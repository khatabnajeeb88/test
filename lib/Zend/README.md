## Synopsis
This library for re-write Zend_Currency to change currency symbol position. 

### Module configuration
1. Package details [composer.json](composer.json).
2. Zend_Currency [Currency.php](lib/Zend/Currency.php).


### Update Composer Dump to update library.
1.  composer dump-autoload