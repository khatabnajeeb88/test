## Synopsis
This library for re-write Zend_Logger to add timezone Asia/Riyad. 

### Module configuration
1. Package details [composer.json](composer.json).
2. Zend Logger [Logger.php](lib/Zend/Log/Logger.php).


### Update Composer Dump to update library.
1.  composer dump-autoload