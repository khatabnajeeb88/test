<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Plugin\Customer\Controller\Adminhtml\Index;

use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Controller\Adminhtml\Index\Validate as CustomerValidateController;

class Validate
{
    public function aroundExecute(CustomerValidateController $subject, \Closure $proceed) 
    {
        $sellerPanel = trim($subject->getRequest()->getParam("seller_panel"));
        $customerId=trim($subject->getRequest()->getParam("customer_id"));
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mpHelper=$objectManager->create("Webkul\Marketplace\Helper\Data");
        $attributeHelper=$objectManager->create("Webkul\MpVendorAttributeManager\Helper\Data");
        $customerData=$objectManager->create("Magento\Customer\Model\CustomerFactory")->create()->load($customerId);
        $response = new \Magento\Framework\DataObject(); 
        $resultJson = $objectManager->create("\Magento\Framework\Controller\Result\JsonFactory");
        $sellerData=$mpHelper->getSellerDataBySellerId($customerId)->getData();
        if ($sellerPanel) {
                if($sellerData[0]['shop_title']==null&& trim($subject->getRequest()->getParam("shop_title"))==null){
                    $response->setError(true);
                    $error[]='Shop Title is a required Field.';
                    $response->setMessages($error);
                    $resultJson = $resultJson->create();
                    $resultJson->setData($response);
                    return $resultJson;
                }
        
            foreach($attributeHelper->getAttributeCollection() as $attribute){
                if($attribute->getData('required_field')==1){
                    $params=$subject->getRequest()->getParams();
                    $attrvalue=null;
                    if(isset($params['customer'][$attribute->getData('attribute_code')])){
                    $attrvalue=$params['customer'][$attribute->getData('attribute_code')];}
                    if($customerData->getData($attribute->getData('attribute_code'))==null&& $attrvalue==null){
                        $frontendLabel=$attribute->getData('frontend_label');
                        $response->setError(true);
                        $error[]= $frontendLabel." is a required Field.";
                        $response->setMessages($error);
                        $resultJson = $resultJson->create();
                        $resultJson->setData($response);
                        return $resultJson;
                    }
                }
            }
        }
        return $proceed();
    }
}
