<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpVendorAttributeManager
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpVendorAttributeManager\Block\Account;

use Magento\Framework\View\Element\Html\Link;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Webkul\MpVendorAttributeManager\Model\Url;
use Magento\Customer\Model\Context as CustomerContext;

class RegisterLink extends Link
{
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    private $httpContext;

    /**
     * @var \Webkul\MpVendorAttributeManager\Model\Url
     */
    private $vendorUrl;

    /**
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param \Magento\Framework\App\Http\Context               $httpContext
     * @param \Webkul\MpVendorAttributeManager\Model\Url        $vendorUrl
     * @param array                                             $data
     */
    public function __construct(
        Context $context,
        HttpContext $httpContext,
        Url $vendorUrl,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->httpContext = $httpContext;
        $this->vendorUrl = $vendorUrl;
    }

    /**
     * Get Vendor URL
     *
     * @return string
     */
    public function getHref()
    {
        return $this->vendorUrl->getVendorUrl();
    }

    /**
     * Convert to HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->httpContext->getValue(CustomerContext::CONTEXT_AUTH)
        ) {
            return '';
        }
        return parent::_toHtml();
    }
}
