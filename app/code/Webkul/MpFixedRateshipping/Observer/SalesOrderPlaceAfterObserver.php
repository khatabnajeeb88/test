<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpFixedRateshipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpFixedRateshipping\Observer;

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;

/**
 * Webkul Marketplace MpFixedRateshipping SalesOrderPlaceAfterObserver Observer Model
 *
 * @author      Webkul Software
 *
 */
class SalesOrderPlaceAfterObserver implements ObserverInterface
{
    /**
     * @var eventManager
     */
    protected $_eventManager;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var Session
     */
    protected $_session;

    /**
     * @var OrdersFactory
     */
    protected $ordersFactory;
    
    /**
     * @param \Magento\Framework\Event\Manager $eventManager
     * @param \Webkul\Marketplace\Model\OrdersFactory $ordersFactory
     * @param SessionManager $session
     */
    public function __construct(
        \Magento\Framework\Event\Manager $eventManager,
        \Webkul\Marketplace\Model\OrdersFactory $ordersFactory,
        SessionManager $session
    ) {
        $this->_eventManager = $eventManager;
        $this->ordersFactory = $ordersFactory;
        $this->_session = $session;
    }

    /**
     * after place order event handler
     * Distribute Shipping Price for sellers
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();
        $shippingmethod=$order->getShippingMethod();
        $lastOrderId = $observer->getOrder()->getId();
        if (strpos($shippingmethod, 'mpfixrate')!==false) {
            $allorderitems=$order->getAllItems();
            $shipmethod=explode('_', $shippingmethod, 2);
            $shippingAll=$this->_session->getShippingInfo();
            foreach ((array)$shippingAll['mpfixrate'] as $shipdata) {
                $collection=$this->ordersFactory->create()
                            ->getCollection()
                            ->addFieldToFilter('order_id', ['eq'=>$lastOrderId])
                            ->addFieldToFilter('seller_id', ['eq'=>$shipdata['seller_id']])
                            ->getLastItem();
                if ($collection->getId()) {
                    $collection->setCarrierName($shipdata['submethod'][0]['method']);
                    $collection->setShippingCharges($shipdata['submethod'][0]['cost']);
                    $this->saveObject($collection);
                }
            }
            $this->_session->unsShippingInfo();
        }
    }

    /**
     * saveObject
     */
    protected function saveObject($object)
    {
        $object->save();
    }
}
