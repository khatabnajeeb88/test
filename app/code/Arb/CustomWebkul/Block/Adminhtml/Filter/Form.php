<?php

/**
 * Custom Filters for Report
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Block\Adminhtml\Filter;

use Magento\Backend\Block\Widget\Form\Element\Dependence;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Block\Adminhtml\Report\Filter\Form as ParentForm;
use Magento\Framework\Data\Form\Element\Fieldset;
use Webkul\MpRmaSystem\Model\ResourceModel\Reasons\CollectionFactory as ReasonsCollection;
use Webkul\MpRmaSystem\Model\ResourceModel\Details\CollectionFactory as RmaCollectionFactory;
use Webkul\MpRmaSystem\Helper\Data as RmaHelper;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Sales\Model\Order\ConfigFactory;

class Form extends ParentForm
{
    /**
     * @var ReasonsCollection
     */
    private $reasonsCollection;

    /**
     * @var RmaCollectionFactory
     */
    private $rmaCollectionFactory;

    /**
     * @var RmaHelper
     */
    private $rmaHelper;

    /**
     * Flag that keep info should we render specific dependent element or not
     *
     * @var bool
     */
    protected $_renderDependentElement = false;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param ConfigFactory $orderConfig
     * @param ReasonsCollection $reasonsCollection
     * @param RmaCollectionFactory $rmaCollectionFactory
     * @param RmaHelper $rmaHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        ConfigFactory $orderConfig,
        ReasonsCollection $reasonsCollection,
        RmaCollectionFactory $rmaCollectionFactory,
        RmaHelper $rmaHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $orderConfig,
            $data
        );

        $this->reasonsCollection = $reasonsCollection;
        $this->rmaCollectionFactory = $rmaCollectionFactory;
        $this->rmaHelper = $rmaHelper;
    }

    /**
     * Add fields to base fieldset which are general to sales reports
     *
     * @return self
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        /** @var Fieldset $fieldset */
        $fieldset = $this->getForm()->getElement('base_fieldset');

        if (is_object($fieldset) && $fieldset instanceof Fieldset) {
            $rmaCollection = $this->rmaCollectionFactory->create();
            $rmaCollection->getSelect()->join(
                ['rma_items' => $rmaCollection->getTable('marketplace_rma_items')],
                'main_table.id = rma_items.rma_id'
            );

            $rmaDataArray = [];
            foreach ($rmaCollection as $rmaItem) {
                $rmaDataArray[$rmaItem->getId()]['customer_name'] = $rmaItem->getCustomerName();
                $rmaDataArray[$rmaItem->getId()]['seller_id'] = $rmaItem->getSellerId();
            }
            $this->addOrderFilter($fieldset);
            $this->addCustomerFilter($fieldset, $rmaDataArray);
            $this->addRmaIdFilter($fieldset);
            $this->addStatusFilter($fieldset);
            $this->addReasonFilter($fieldset);
            $this->addReturnedAmountFilter($fieldset);
            $this->addMerchantFilter($fieldset, $rmaDataArray);

            $this->_renderDependentElement = true;
        }

        return $this;
    }

    /**
     * @return array
     */
    private function getReasons()
    {
        $reasons = [];
        $collection = $this->reasonsCollection->create();

        foreach ($collection as $reason) {
            $reasons[$reason->getId()] = __($reason->getReason());
        }

        return $reasons;
    }

    /**
     * @param Fieldset $fieldset
     *
     * @return void
     */
    private function addRmaIdFilter(Fieldset $fieldset)
    {
        $fieldset->addField(
            'rma_id_list',
            'select',
            [
                'name' => 'rma_id_list',
                'options' => [__('Any'), __('Specified')],
                'label' => __('Rma Id')
            ]
        );

        $fieldset->addField(
            'rma_ids',
            'text',
            [
                'name' => 'rma_ids',
                'label' => '',
                'display' => 'none',
                'class' => 'validate-number validate-zero-or-greater'
            ],
            'rma_id_list'
        );
    }

    /**
     * @param Fieldset $fieldset
     * @param array $rmaDataArray
     *
     * @return void
     */
    private function addMerchantFilter(Fieldset $fieldset, array $rmaDataArray)
    {
        $fieldset->addField(
            'merchant_name_list',
            'select',
            [
                'name' => 'merchant_name_list',
                'options' => [__('Any'), __('Specified')],
                'label' => __('Merchant Name')
            ]
        );

        $sellerIds = [];
        foreach ($rmaDataArray as $key => $value) {
            if (!array_key_exists($value['seller_id'], $sellerIds)) {
                $seller = $this->rmaHelper->getSellerDetails($value['seller_id']);
                if ($seller) {
                    $sellerIds[$value['seller_id']] = $seller->getShopTitle();
                }
            }
        }

        $merchantNamesList = [];
        foreach ($sellerIds as $key => $value) {
            $merchantNamesList[] = ['label' => __($value), 'value' => $value, 'title' => __($value)];
        }

        $fieldset->addField(
            'merchant_names',
            'multiselect',
            [
                'name' => 'merchant_names',
                'label' => '',
                'values' => $merchantNamesList,
                'display' => 'none'
            ],
            'merchant_name_list'
        );
    }

    /**
     * @param Fieldset $fieldset
     * @param array $rmaDataArray
     *
     * @return void
     */
    private function addCustomerFilter(Fieldset $fieldset, array $rmaDataArray)
    {
        $fieldset->addField(
            'customer_name_list',
            'select',
            [
                'name' => 'customer_name_list',
                'options' => [__('Any'), __('Specified')],
                'label' => __('Customer Name')
            ]
        );

        $customerNames = [];
        foreach ($rmaDataArray as $key => $value) {
            if (!in_array($value['customer_name'], $customerNames)) {
                $customerNames[] = $value['customer_name'];
            }
        }

        $customerNamesList = [];
        foreach ($customerNames as $key => $value) {
            $customerNamesList[] = ['label' => __($value), 'value' => $value, 'title' => __($value)];
        }

        $fieldset->addField(
            'customer_names',
            'multiselect',
            [
                'name' => 'customer_names',
                'label' => '',
                'values' => $customerNamesList,
                'display' => 'none'
            ],
            'customer_name_list'
        );
    }

    /**
     * @param Fieldset $fieldset
     *
     * @return void
     */
    private function addOrderFilter(Fieldset $fieldset)
    {
        $fieldset->addField(
            'order_id_list',
            'select',
            [
                'name' => 'order_id_list',
                'options' => [__('Any'), __('Specified')],
                'label' => __('Order Id')
            ]
        );

        $fieldset->addField(
            'order_ids',
            'text',
            [
                'name' => 'order_ids',
                'label' => '',
                'display' => 'none',
                'note' => __('Provide Order Id in a form like this: #100000232')
            ],
            'order_id_list'
        );
    }

    /**
     * @param Fieldset $fieldset
     *
     * @return void
     */
    private function addReturnedAmountFilter(Fieldset $fieldset)
    {
        $fieldset->addField(
            'returned_amount_list',
            'select',
            [
                'name' => 'returned_amount_list',
                'options' => [__('Any'), __('Specified')],
                'label' => __('Returned Amount')
            ]
        );

        $fieldset->addField(
            'returned_amount_field',
            'text',
            [
                'name' => 'returned_amount_field',
                'label' => '',
                'display' => 'none',
                'class' => 'validate-number validate-zero-or-greater'
            ],
            'returned_amount_list'
        );
    }

    /**
     * @param Fieldset $fieldset
     *
     * @return void
     */
    private function addStatusFilter(Fieldset $fieldset)
    {
        $statusArray = ['Processing', 'Solved', 'Declined', 'Canceled', 'Pending'];

        $fieldset->addField(
            'rma_status_list',
            'select',
            [
                'name' => 'rma_status_list',
                'options' => [__('Any'), __('Specified')],
                'label' => __('Rma Status')
            ]
        );

        $rmaStatusList = [];
        foreach ($statusArray as $key => $value) {
            $rmaStatusList[] = ['label' => __($value), 'value' => $value, 'title' => __($value)];
        }

        $fieldset->addField(
            'status_list',
            'multiselect',
            [
                'name' => 'status_list',
                'label' => '',
                'values' => $rmaStatusList,
                'display' => 'none'
            ],
            'rma_status_list'
        );
    }

    /**
     * @param Fieldset $fieldset
     *
     * @return void
     */
    private function addReasonFilter(Fieldset $fieldset)
    {
        $fieldset->addField(
            'rma_reasons_list',
            'select',
            [
                'name' => 'rma_reasons_list',
                'options' => [__('Any'), __('Specified')],
                'label' => __('Rma Reason')
            ]
        );

        $rmaReasonsList = [];
        foreach ($this->getReasons() as $key => $value) {
            $rmaReasonsList[] = ['label' => __($value), 'value' => $value, 'title' => __($value)];
        }

        $fieldset->addField(
            'reasons_list',
            'multiselect',
            [
                'name' => 'reasons_list',
                'label' => '',
                'values' => $rmaReasonsList,
                'display' => 'none'
            ],
            'rma_reasons_list'
        );
    }

    /**
     * Processing block html after rendering
     *
     * @param string $html
     *
     * @return string
     *
     * @throws LocalizedException
     */
    protected function _afterToHtml($html)
    {
        if ($this->_renderDependentElement) {
            $form = $this->getForm();
            $htmlIdPrefix = $form->getHtmlIdPrefix();

            /** @var Dependence $formAfterBlock */
            $formAfterBlock = $this->getLayout()->createBlock(
                Dependence::class,
                'adminhtml.block.widget.form.element.dependence'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'rma_reasons_list',
                'rma_reasons_list'
            )->addFieldMap(
                $htmlIdPrefix . 'reasons_list',
                'reasons_list'
            )->addFieldDependence(
                'reasons_list',
                'rma_reasons_list',
                '1'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'rma_status_list',
                'rma_status_list'
            )->addFieldMap(
                $htmlIdPrefix . 'status_list',
                'status_list'
            )->addFieldDependence(
                'status_list',
                'rma_status_list',
                '1'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'order_id_list',
                'order_id_list'
            )->addFieldMap(
                $htmlIdPrefix . 'order_ids',
                'order_ids'
            )->addFieldDependence(
                'order_ids',
                'order_id_list',
                '1'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'rma_id_list',
                'rma_id_list'
            )->addFieldMap(
                $htmlIdPrefix . 'rma_ids',
                'rma_ids'
            )->addFieldDependence(
                'rma_ids',
                'rma_id_list',
                '1'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'customer_name_list',
                'customer_name_list'
            )->addFieldMap(
                $htmlIdPrefix . 'customer_names',
                'customer_names'
            )->addFieldDependence(
                'customer_names',
                'customer_name_list',
                '1'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'merchant_name_list',
                'merchant_name_list'
            )->addFieldMap(
                $htmlIdPrefix . 'merchant_names',
                'merchant_names'
            )->addFieldDependence(
                'merchant_names',
                'merchant_name_list',
                '1'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'returned_amount_list',
                'returned_amount_list'
            )->addFieldMap(
                $htmlIdPrefix . 'returned_amount_field',
                'returned_amount_field'
            )->addFieldDependence(
                'returned_amount_field',
                'returned_amount_list',
                '1'
            );

            $html = $html . $formAfterBlock->toHtml();
        }

        return $html;
    }
}
