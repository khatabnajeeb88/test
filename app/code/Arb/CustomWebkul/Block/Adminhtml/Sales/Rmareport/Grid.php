<?php
/**
 * Sales RMA report GRID
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Block\Adminhtml\Sales\Rmareport;

use Magento\Reports\Block\Adminhtml\Grid\AbstractGrid;
use Arb\CustomWebkul\Model\ResourceModel\Report\RmaReport\Collection;
use Magento\Reports\Model\ResourceModel\Report\Collection\AbstractCollection;
use Magento\Framework\DataObject;
use Magento\Reports\Block\Adminhtml\Sales\Grid\Column\Renderer\Date;

class Grid extends AbstractGrid
{
    /**
     * GROUP BY criteria
     *
     * @var string
     */
    protected $_columnGroupBy = 'period';

    protected function _construct()
    {
        parent::_construct();

        $this->setCountTotals(false);
    }

    /**
     * @return string
     */
    public function getResourceCollectionName()
    {
        return Collection::class;
    }

    /**
     * @return AbstractGrid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'period',
            [
                'header' => __('RMA Creation Time'),
                'index' => 'period',
                'sortable' => true,
                'period_type' => $this->getPeriodType(),
                'renderer' => Date::class,
                'totals_label' => __('Total Count'),
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'order_id',
            [
                'header' => __('Order Id'),
                'index' => 'order_id',
                'type' => 'string',
                'sortable' => false,
                'translate' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'customer_name',
            [
                'header' => __('Customer Name'),
                'index' => 'customer_name',
                'type' => 'string',
                'sortable' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'rma_id',
            [
                'header' => __('Rma Id'),
                'index' => 'rma_id',
                'type' => 'string',
                'sortable' => false,
                'translate' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'rma_status',
            [
                'header' => __('Rma Status'),
                'index' => 'rma_status',
                'type' => 'string',
                'sortable' => false,
                'translate' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'rma_reason',
            [
                'header' => __('Rma Reason'),
                'index' => 'rma_reason',
                'type' => 'string',
                'sortable' => false,
                'translate' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'rma_returned_amount',
            [
                'header' => __('RMA Returned Amount'),
                'index' => 'rma_returned_amount',
                'type' => 'currency',
                'sortable' => false,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'rma_merchant_name',
            [
                'header' => __('RMA Merchant Name'),
                'index' => 'rma_merchant_name',
                'type' => 'string',
                'sortable' => false,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        if ($this->getFilterData()->getStoreIds()) {
            $this->setStoreIds(explode(',', $this->getFilterData()->getStoreIds()));
        }

        $this->addExportType('*/*/exportRmaReportCsv', __('CSV'));
        $this->addExportType('*/*/exportRmaReportExcel', __('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * @param AbstractCollection $collection
     * @param DataObject $filterData
     *
     * @return AbstractGrid
     */
    protected function _addCustomFilter($collection, $filterData)
    {
        if ($filterData->getOrderIdList()) {
            $orderId = $filterData->getData('order_ids');
            if ($orderId) {
                $collection->addOrderIdFilter($orderId);
            }
        }

        if ($filterData->getReturnedAmountList()) {
            $returnedAmount = $filterData->getData('returned_amount_field');
            if ($returnedAmount) {
                $collection->addReturnedAmountFilter($returnedAmount);
            }
        }

        if ($filterData->getRmaIdList()) {
            $rmaId = $filterData->getData('rma_ids');
            if ($rmaId) {
                $collection->addRmaIdFilter($rmaId);
            }
        }

        if ($filterData->getRmaReasonsList()) {
            $reasonsList = $filterData->getData('reasons_list');
            if (isset($reasonsList[0])) {
                $reasonsIds = explode(',', $reasonsList[0]);
                $collection->addRmaReasonFilter($reasonsIds);
            }
        }

        if ($filterData->getRmaStatusList()) {
            $statusList = $filterData->getData('status_list');
            if (isset($statusList[0])) {
                $statusIds = explode(',', $statusList[0]);
                $collection->addRmaStatusFilter($statusIds);
            }
        }

        if ($filterData->getCustomerNameList()) {
            $customersList = $filterData->getData('customer_names');
            if (isset($customersList[0])) {
                $customerIds = explode(',', $customersList[0]);
                $collection->addCustomerFilter($customerIds);
            }
        }

        if ($filterData->getMerchantNameList()) {
            $merchantList = $filterData->getData('merchant_names');
            if (isset($merchantList[0])) {
                $merchantIds = explode(',', $merchantList[0]);
                $collection->addMerchantFilter($merchantIds);
            }
        }

        return parent::_addCustomFilter($filterData, $collection);
    }
}
