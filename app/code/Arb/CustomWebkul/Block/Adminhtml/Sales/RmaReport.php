<?php
/**
 * Sales RMA report GRID
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Block\Adminhtml\Sales;

use Magento\Backend\Block\Widget\Grid\Container;

class RmaReport extends Container
{
    protected $_template = 'Magento_Reports::report/grid/container.phtml';

    protected function _construct()
    {
        $this->_blockGroup = 'Arb_CustomWebkul';
        $this->_controller = 'adminhtml_sales_rmareport';
        $this->_headerText = __('RMA Report');
        parent::_construct();

        $this->buttonList->remove('add');
        $this->addButton(
            'filter_form_submit',
            ['label' => __('Show Report'), 'onclick' => 'filterFormSubmit()', 'class' => 'primary']
        );
    }

    /**
     * Get filter URL
     *
     * @return string
     */
    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/rmareport', ['_current' => true]);
    }
}
