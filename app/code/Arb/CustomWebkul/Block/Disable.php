<?php
/**
 * Class for disable qty for products
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Block;

class Disable extends \Magento\Framework\View\Element\Template
{
    protected $_registry;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {        
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }
    /**
    * get current product object
    */
    public function getCurrentProduct()
    {        
        return $this->_registry->registry('current_product');
    }   
    
}
?>