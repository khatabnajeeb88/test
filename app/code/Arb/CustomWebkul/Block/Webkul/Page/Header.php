<?php
/**
 * Overriden template file
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Block\Webkul\Page;

class Header extends \Webkul\Marketplace\Block\Page\Header
{

    /**
     * @var string
     * template defination path is defined for webkul merchant header
     */
    protected $_template = 'Arb_CustomWebkul::layout2/page/header.phtml';
}
