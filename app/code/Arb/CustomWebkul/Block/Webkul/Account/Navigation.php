<?php
/**
 * Overridden Block file
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */
namespace Arb\CustomWebkul\Block\Webkul\Account;

use Webkul\Marketplace\Block\Account\Navigation as ParentNavigation;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection as OrdersCollection;

class Navigation extends ParentNavigation
{
    /**
     * Override original method to remove order_approval filter from collection
     *
     * @return OrdersCollection
     */
    public function getMarketplaceOrderCollection()
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $sellerId = $this->mpHelper->getCustomerId();
        $mpOrderCollection = $this->ordersFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )->addFieldToFilter(
                'seller_pending_notification',
                1
            );

        $salesOrder = $this->productCollection->create()->getTable('sales_order');

        $mpOrderCollection->getSelect()->join(
            $salesOrder.' as so',
            'main_table.order_id = so.entity_id'
        );

        return $mpOrderCollection;
        //@codeCoverageIgnoreEnd
    }
}
