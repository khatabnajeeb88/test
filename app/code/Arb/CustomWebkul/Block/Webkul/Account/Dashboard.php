<?php
/**
 * Overridden Block file
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */
namespace Arb\CustomWebkul\Block\Webkul\Account;

use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Model\ResourceModel\Saleperpartner\CollectionFactory as SalesPartnerCollection;
use Webkul\Marketplace\Block\Account\Dashboard as ParentDashboard;

class Dashboard extends ParentDashboard
{
   /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Magento\Sales\Model\Order\ItemRepository
     */
    protected $orderItemRepository;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var \Webkul\Marketplace\Helper\Orders
     */
    protected $orderHelper;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory
     */
    protected $mpSaleslistCollectionFactory;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory
     */
    protected $mpOrderCollectionFactory;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory
     */
    protected $mpProductCollectionFactory;

    /**
     * @var SalesPartnerCollection
     */
    protected $mpSalePerPartnerCollectionFactory;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory
     */
    protected $mpFeedbackCollectionFactory;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /** @var SaleslistFactory */
    public $saleslistModel;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Sales\Model\Order\ItemRepository $orderItemRepository
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Catalog\Model\CategoryRepository $categoryRepository
     * @param \Webkul\Marketplace\Helper\Orders $orderHelper
     * @param \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $mpSaleslistCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory $mpOrderCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollectionFactory
     * @param SalesPartnerCollection $mpSalePerPartnerCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory $mpFeedbackCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Sales\Model\Order\ItemRepository $orderItemRepository,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Webkul\Marketplace\Helper\Orders $orderHelper,
        \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $mpSaleslistCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory $mpOrderCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollectionFactory,
        SalesPartnerCollection $mpSalePerPartnerCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory $mpFeedbackCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        SaleslistFactory $saleslistModel = null,
        array $data = []
    ) {

        $this->saleslistModel = $saleslistModel ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->create(SaleslistFactory::class);
       
        parent::__construct($context, $customer, $customerSession, $orderRepository,
        $orderItemRepository, $productRepository, $categoryRepository, $orderHelper,
        $mpSaleslistCollectionFactory,$mpOrderCollectionFactory, $mpProductCollectionFactory,
        $mpSalePerPartnerCollectionFactory, $mpFeedbackCollectionFactory,$orderCollectionFactory,
        $data);
    }

    /**
     * Override original method to remove order_approval filter from collection
     *
     * @return int
     */
    public function getTotalOrders()
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $sellerId = $this->getCustomerId();
        $collection = $this->mpOrderCollectionFactory->create();
        $collection->addFieldToFilter('seller_id', $sellerId);
        $salesOrder = $collection->getTable('sales_order');
        $collection->getSelect()->join(
            $salesOrder . ' as so',
            'main_table.order_id = so.entity_id',
            ["order_approval_status" => "order_approval_status"]
        );

        return count($collection);
        //@codeCoverageIgnoreEnd
    }

    public function getPricebyorder($orderId)
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $sellerId = $this->getCustomerId();
        $collection = $this->saleslistModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $orderId
            )->getPricebyorderData();
        $name = '';
        $actualSellerAmount = 0;
        foreach ($collection as $coll) {
            // calculate order actual_seller_amount in base currency
            $appliedCouponAmount = $coll['applied_coupon_amount']*1;
            $shippingAmount = $coll['shipping_charges']*1;
            $refundedShippingAmount = $coll['refunded_shipping_charges']*1;
            $totalshipping = $shippingAmount - $refundedShippingAmount;
            if ($coll['tax_to_seller']) {
                $vendorTaxAmount = $coll['total_tax']*1;
            } else {
                $vendorTaxAmount = 0;
            }
            if ($coll['actual_seller_amount'] * 1) {
                $taxShippingTotal = $vendorTaxAmount + $totalshipping - $appliedCouponAmount;
                $actualSellerAmount += $coll['actual_seller_amount'] + $taxShippingTotal;
            } else {
                if ($totalshipping * 1) {
                    $actualSellerAmount += $totalshipping - $appliedCouponAmount;
                }
            }
        }
        return $actualSellerAmount;
        //@codeCoverageIgnoreEnd
    }

    /**
     * @return \Webkul\Marketplace\Model\ResourceModel\Saleslist\Collection
     */
    public function getCollection()
    {
        if (!($customerId = $this->getCustomerId())) {
            return false;
        }

        $paramData = $this->getRequest()->getParams();
        $filterOrderid = '';
        $filterOrderstatus = '';
        $filterDataTo = '';
        $filterDataFrom = '';
        $from = null;
        $to = null;

        if (isset($paramData['s'])) {
            $filterOrderid = $paramData['s'] != '' ? $paramData['s'] : '';
        }
        if (isset($paramData['orderstatus'])) {
            $filterOrderstatus = $paramData['orderstatus'] != '' ? $paramData['orderstatus'] : '';
        }
        if (isset($paramData['from_date'])) {
            $filterDataFrom = $paramData['from_date'] != '' ? $paramData['from_date'] : '';
        }
        if (isset($paramData['to_date'])) {
            $filterDataTo = $paramData['to_date'] != '' ? $paramData['to_date'] : '';
        }

        $orderids = $this->getOrderIdsArray($customerId, $filterOrderstatus);

        $ids = $this->getEntityIdsArray($orderids);

        $collection = $this->mpSaleslistCollectionFactory->create()
        ->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'entity_id',
            ['in' => $ids]
        );

        if ($filterDataTo) {
            $todate = date_create($filterDataTo);
            $to = date_format($todate, 'Y-m-d 23:59:59');
        }
        if ($filterDataFrom) {
            $fromdate = date_create($filterDataFrom);
            $from = date_format($fromdate, 'Y-m-d H:i:s');
        }

        if ($filterOrderid) {
            $collection->addFieldToFilter(
                'magerealorder_id',
                ['eq' => $filterOrderid]
            );
        }

        $collection->addFieldToFilter(
            'created_at',
            ['datetime' => true, 'from' => $from, 'to' => $to]
        );

        $collection->setOrder(
            'created_at',
            'desc'
        );
        $collection->getSellerOrderCollection();
        $collection->setPageSize(5);
        //get only selected order status
        $orderStatusArray = ["canceled","complete","processing","shipped","closed"];
        $collection->addFieldToFilter("so.status",array("in"=>$orderStatusArray));
        return $collection;
    }
}
