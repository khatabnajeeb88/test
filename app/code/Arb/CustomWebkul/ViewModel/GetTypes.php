<?php
/**
 * ViewModel for Create Product Block
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class GetTypes implements ArgumentInterface
{
    /**
     * @return array
     */
    public function getAllowedProductTypes()
    {
        $alloweds = ['simple', 'downloadable', 'configurable'];
        $data = [
            'simple' => __('Simple'),
            'downloadable' => __('Downloadable'),
            'virtual' => __('Virtual'),
            'configurable' => __('Configurable'),
            'grouped' => __('Grouped Product'),
            'bundle' => __('Bundle Product'),
        ];
        $allowedproducts = [];
        if (isset($alloweds)) {
            foreach ($alloweds as $allowed) {
                if (!empty($data[$allowed])) {
                    array_push(
                        $allowedproducts,
                        ['value' => $allowed, 'label' => $data[$allowed]]
                    );
                }
            }
        }

        return $allowedproducts;
    }
}
