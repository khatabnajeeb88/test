<?php
/**
 * Overridden Observer file
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Observer\MpFixedRateshipping;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\Marketplace\Model\Orders;
use Webkul\MpFixedRateshipping\Observer\SalesOrderPlaceAfterObserver as ParentObserver;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Event\Manager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Session\SessionManager;
use Magento\Sales\Model\Order;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\OrdersFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Webkul\Marketplace\Model\SaleslistFactory;
use Arb\CustomWebkul\Helper\Webkul\Orders as OrdersHelper;

/**
 * Fully customized method to assign Shipping Fees as the Webkul one didnt work properly on application
 */
class SalesOrderPlaceAfterObserver extends ParentObserver
{
    const XML_PATH_SHIPPING_TITLE = 'carriers/mpfixrate/title';
    const XML_PATH_DEFAULT_SHIPPING_FEE = 'carriers/mpfixrate/default_amount';
    const XML_PATH_DEFAULT_SHIPPING_FREE_UPTO = 'carriers/mpfixrate/shipping_up_to';

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $config;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SaleslistFactory
     */
    private $saleslistFactory;

    /**
     * @var OrdersHelper
     */
    private $orders;

    /**
     * @param Manager $eventManager
     * @param OrdersFactory $ordersFactory
     * @param SessionManager $session
     * @param CustomerRepositoryInterface $customerRepository
     * @param ScopeConfigInterface $config
     * @param ProductRepositoryInterface $productRepository
     * @param SaleslistFactory $saleslistFactory
     * @param OrdersHelper $orders
     */
    public function __construct(
        Manager $eventManager,
        OrdersFactory $ordersFactory,
        SessionManager $session,
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface $config,
        ProductRepositoryInterface $productRepository,
        SaleslistFactory $saleslistFactory,
        OrdersHelper $orders
    ) {
        parent::__construct($eventManager, $ordersFactory, $session);

        $this->customerRepository = $customerRepository;
        $this->config = $config;
        $this->productRepository = $productRepository;
        $this->saleslistFactory = $saleslistFactory;
        $this->orders = $orders;
    }

    /**
     * Distribute Shipping Fees for Sellers
     *
     * @param Observer $observer
     *
     * @return void
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();
        $shippingMethod = $order->getShippingMethod();

        if (!$shippingMethod || $order->getIsVirtual()) {
            return;
        }

        if (strpos($shippingMethod, 'mpfixrate') !== false) {
            $merchantOrders = $this->ordersFactory->create()
                ->getCollection()
                ->addFieldToFilter('order_id', ['eq' => $order->getId()]);

            foreach ($merchantOrders->getItems() as $merchantOrder) {
                if (!$this->checkIfContainsPhysical($merchantOrder)) {
                    continue;
                }

                try {

                    $sellerId = $merchantOrder->getSellerId();
                    $itemIds = explode(',',$merchantOrder->getProductIds());
                    $customer = $this->customerRepository->getById($sellerId);

                    $shippingFee = $customer->getCustomAttribute('mpshipping_fixrate');
                    if ($shippingFee) {
                        $fixrateAmount = $shippingFee->getValue();
                    } else {
                        $fixrateAmount = (int)$this->config->getValue(self::XML_PATH_DEFAULT_SHIPPING_FEE, 'store');
                    }
                    
                    $freeShipping = $customer->getCustomAttribute('mpshipping_fixrate_upto');
                    if ($freeShipping) {
                        $freeShippingFrom = $freeShipping->getValue();
                    } else {
                        $freeShippingFrom = (int)$this->config->getValue(self::XML_PATH_DEFAULT_SHIPPING_FREE_UPTO, 'store');
                    }

                    $allItems = $order->getAllItems();                    
                    $totalPrice = 0;
                    foreach ($allItems as $item) {                        
                        if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                            continue;
                        }
                        if (in_array($item->getProductId(),$itemIds)) {
                            $qty = $item->getQtyOrdered();
                            $productPrice = $item->getPrice() * $qty;                        
                            $totalPrice = $totalPrice + $productPrice;
                        }
                    }

                    if ($freeShippingFrom != '' && $freeShippingFrom <= $totalPrice) {
                        $shipFee = 0;
                    } else {
                        $shipFee = $fixrateAmount;
                    }                    
                } catch (NoSuchEntityException $e) {
                    continue;
                } catch (LocalizedException $e) {
                    continue;
                }               

                $merchantOrder->setShippingCharges($shipFee);
                $merchantOrder->setCarrierName($this->config->getValue(self::XML_PATH_SHIPPING_TITLE, 'store'));
                $merchantOrder->save();
            }
            $this->_session->unsShippingInfo();
        }
    }

    /**
     * Check if Merchant Order has Shipping Fee by checking if it is a physical/virtual Order
     *
     * @param Orders $merchantOrder
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    private function checkIfContainsPhysical(Orders $merchantOrder)
    {
        $orderId = $merchantOrder->getData('order_id');
        $sellerId = $merchantOrder->getData('seller_id');

        $salesCollection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $orderId
            );

        foreach ($salesCollection as $sale) {
            try {
                $product = $this->productRepository->getById($sale->getData('mageproduct_id'));
            } catch (NoSuchEntityException $e) {
                continue;
            }

            if (($product->getTypeId() === Configurable::TYPE_CODE)
                && $this->orders->isConfigurablePhysical($orderId, $sale)) {
                return true;
            }

            if ($product->getTypeId() === Type::TYPE_SIMPLE) {
                return true;
            }
        }

        return false;
    }
}
