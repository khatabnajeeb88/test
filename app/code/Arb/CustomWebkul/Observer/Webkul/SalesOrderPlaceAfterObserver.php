<?php
/**
 * Overridden Observer file
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Observer\Webkul;

use Arb\CustomWebkul\Helper\Email;
use Magento\Customer\Model\CustomerFactory;
use Magento\Sales\Model\Order;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Marketplace\Observer\SalesOrderPlaceAfterObserver as ObserverParent;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Magento\Sales\Model\Order\AddressFactory;
use Webkul\Marketplace\Model\SaleslistFactory;
use Magento\Directory\Model\CountryFactory;
use Webkul\Marketplace\Helper\Email as MpEmailHelper;
use Webkul\Marketplace\Helper\Orders as OrdersHelper;
use Webkul\Marketplace\Model\ProductFactory;
use Webkul\Marketplace\Model\OrdersFactory;
use Webkul\Marketplace\Model\OrderPendingMailsFactory;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\SaleperpartnerFactory;
use Arb\Sla\Helper\SlaEventManagement;
use Arb\Sla\Model\SlaData;
use Arb\Sla\Api\Data\SlaDataInterface;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory;
use Magento\Backend\Model\UrlInterface;

class SalesOrderPlaceAfterObserver extends ObserverParent
{
    /**
     * @var eventManager
     */
    protected $_eventManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * [$_coreSession description].
     *
     * @var SessionManager
     */
    protected $_coreSession;

    /**
     * @var QuoteRepository
     */
    protected $_quoteRepository;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var AddressFactory
     */
    protected $orderAddressFactory;

    /**
     * @var SaleslistFactory
     */
    protected $saleslistFactory;

    /**
     * @var CountryFactory
     */
    protected $countryModel;

    /**
     * @var MpEmailHelper
     */
    protected $mpEmailHelper;

    /**
     * @var OrdersHelper
     */
    protected $ordersHelper;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var OrdersFactory
     */
    protected $ordersFactory;

    /**
     * @var OrderPendingMailsFactory
     */
    protected $orderPendingMailsFactory;

    /**
     * @var NotificationHelper
     */
    protected $notificationHelper;

    /**
     * @var SaleperpartnerFactory
     */
    protected $saleperpartnerFactory;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\Item
     */
    protected $stockItem;

    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var Email
     */
    private $email;
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;


    /**
     * @param \Magento\Framework\Event\Manager $eventManager
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param SessionManager $coreSession
     * @param QuoteRepository $quoteRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param ProductRepositoryInterface $productRepository
     * @param MarketplaceHelper $marketplaceHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\CatalogInventory\Model\Stock\Item $stockItem
     * @param SaleslistFactory $saleslistFactory
     * @param AddressFactory $orderAddressFactory
     * @param CountryFactory $countryModel
     * @param MpEmailHelper $mpEmailHelper
     * @param OrdersHelper $ordersHelper
     * @param ProductFactory $productFactory
     * @param OrdersFactory $ordersFactory
     * @param OrderPendingMailsFactory $orderPendingMailsFactory
     * @param NotificationHelper $notificationHelper
     * @param SaleperpartnerFactory $saleperpartnerFactory
     * @param SlaEventManagement $slaEventManagement
     * @param CollectionFactory $collectionFactory
     * @param CustomerFactory $customerFactory
     * @param UrlInterface $url
     * @param Email $email
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        SessionManager $coreSession,
        QuoteRepository $quoteRepository,
        OrderRepositoryInterface $orderRepository,
        CustomerRepositoryInterface $customerRepository,
        ProductRepositoryInterface $productRepository,
        MarketplaceHelper $marketplaceHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\CatalogInventory\Model\Stock\Item $stockItem,
        SaleslistFactory $saleslistFactory = null,
        AddressFactory $orderAddressFactory = null,
        CountryFactory $countryModel = null,
        MpEmailHelper $mpEmailHelper = null,
        OrdersHelper $ordersHelper = null,
        ProductFactory $productFactory = null,
        OrdersFactory $ordersFactory = null,
        OrderPendingMailsFactory $orderPendingMailsFactory = null,
        NotificationHelper $notificationHelper = null,
        SaleperpartnerFactory $saleperpartnerFactory = null,
        SlaEventManagement $slaEventManagement,
        CollectionFactory $collectionFactory,
        CustomerFactory $customerFactory,
        UrlInterface $url,
        Email $email,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct(
            $eventManager,
            $objectManager,
            $customerSession,
            $checkoutSession,
            $coreSession,
            $quoteRepository,
            $orderRepository,
            $customerRepository,
            $productRepository,
            $marketplaceHelper,
            $date,
            $saleslistFactory,
            $orderAddressFactory,
            $countryModel,
            $mpEmailHelper,
            $ordersHelper,
            $productFactory,
            $ordersFactory,
            $orderPendingMailsFactory,
            $notificationHelper,
            $saleperpartnerFactory
        );

        $this->stockItem = $stockItem;
        $this->slaEventManagement = $slaEventManagement;
        $this->collectionFactory = $collectionFactory;
        $this->customerFactory = $customerFactory;
        $this->url = $url;
        $this->email = $email;
        $this->storeManager = $storeManager;
    }

    /**
     * Override original method to adjust order approval status and send email to merchant on order creation
     *
     * @param Order $order
     * @param int $lastOrderId
     *
     * @return void
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function orderPlacedOperations($order, $lastOrderId)
    {
        // skip code coverage as this is webkul method with adjustments
        // @codeCoverageIgnoreStart
        $this->productSalesCalculation($order);

        /*send placed order mail notification to seller*/
        $paymentCode = '';
        if ($order->getPayment()) {
            $paymentCode = $order->getPayment()->getMethod();
        }

        $shippingInfo = '';
        $shippingDes = '';

        $billingId = $order->getBillingAddress()->getId();

        $billaddress = $this->orderAddressFactory->create()->load($billingId);
        $billinginfo = $billaddress['firstname'] . '<br/>' .
            $billaddress['street'] . '<br/>' .
            $billaddress['city'] . ' ' .
            $billaddress['region'] . ' ' .
            $billaddress['postcode'] . '<br/>' .
            $this->countryModel->create()->load($billaddress['country_id'])->getName() . '<br/>T:' .
            $billaddress['telephone'];

        $order->setOrderApprovalStatus(1)->save();

        $payment = $order->getPayment()->getMethodInstance()->getTitle();

        if ($order->getShippingAddress()) {
            $shippingId = $order->getShippingAddress()->getId();
            $address = $this->orderAddressFactory->create()->load($shippingId);
            $shippingInfo = $address['firstname'] . '<br/>' .
                $address['street'] . '<br/>' .
                $address['city'] . ' ' .
                $address['region'] . ' ' .
                $address['postcode'] . '<br/>' .
                $this->countryModel->create()->load($address['country_id'])->getName() . '<br/>T:' .
                $address['telephone'];
            $shippingDes = $order->getShippingDescription();
        }

        $adminStoremail = $this->_marketplaceHelper->getAdminEmailId();
        $defaultTransEmailId = $this->_marketplaceHelper->getDefaultTransEmailId();
        $adminEmail = $adminStoremail ? $adminStoremail : $defaultTransEmailId;
        $adminUsername = $this->_marketplaceHelper->getAdminName();

        $sellerOrder = $this->ordersFactory->create()
            ->getCollection()
            ->addFieldToFilter('order_id', $lastOrderId)
            ->addFieldToFilter('seller_id', ['neq' => 0]);
        foreach ($sellerOrder as $info) {
            $userdata = $this->_customerRepository->getById($info['seller_id']);
            $username = $userdata->getFirstname();
            $useremail = $userdata->getEmail();

            $senderInfo = [];
            $receiverInfo = [];

            $receiverInfo = [
                'name' => $username,
                'email' => $useremail,
            ];
            $senderInfo = [
                'name' => $adminUsername,
                'email' => $adminEmail,
            ];
            $totalprice = 0;
            $totalTaxAmount = 0;
            $codCharges = 0;
            $shippingCharges = 0;
            $orderinfo = '';

            $saleslistIds = [];
            $collection1 = $this->saleslistFactory->create()
                ->getCollection()
                ->addFieldToFilter('order_id', $lastOrderId)
                ->addFieldToFilter('seller_id', $info['seller_id'])
                ->addFieldToFilter('parent_item_id', ['null' => 'true'])
                ->addFieldToFilter('magerealorder_id', ['neq' => 0])
                ->addFieldToSelect('entity_id');

            $saleslistIds = $collection1->getData();

            $fetchsale = $this->saleslistFactory->create()
                ->getCollection()
                ->addFieldToFilter(
                    'entity_id',
                    ['in' => $saleslistIds]
                );
            $fetchsale->getSellerOrderCollection();

            $emailTempVariables = [];
            $emailTempVariables['seller_id'] = $info['seller_id'];
            $emailTempVariables['order_details'] = '';
            $alignment = 'left';

            $sellerStore = $this->storeManager->getStore($userdata->getStoreId());

            if ($sellerStore->getCode() === 'ar_SA') {
                $alignment = 'right';
            }

            foreach ($fetchsale as $res) {
                $product = $this->_productRepository->getById($res['mageproduct_id']);

                /* product name */
                $productName = $res->getMageproName();

                $emailTempVariables['order_details'] .=
                    '<tr>' .
                    '<td align="' . $alignment . '">' . $productName . '</td>' .
                    '<td align="' . $alignment . '">' . ($res['magequantity'] * 1) . '</td>' .
                    '</tr>';
                /* end */

                $merchantOrder = $this->collectionFactory->create()
                    ->addFieldToFilter('order_id', $order->getEntityId())
                    ->addFieldToFilter('seller_id', $info['seller_id']);

                if ($merchantOrder->getFirstItem()) {
                    /** @var SlaData $sla */
                    $sla = $this->slaEventManagement->getSla(
                        SlaDataInterface::SLA_TYPE_NEW_ORDER,
                        (int)$merchantOrder->getFirstItem()->getId()
                    );
                    $emailTempVariables['sla_time'] = $sla->getDeadlineTime();
                } else {
                    $emailTempVariables['sla_time'] = '';
                }

                if ($res->getProductType() == 'configurable') {
                    $configurableSalesItem = $this->saleslistFactory->create()
                        ->getCollection()
                        ->addFieldToFilter('order_id', $lastOrderId)
                        ->addFieldToFilter('seller_id', $info['seller_id'])
                        ->addFieldToFilter('parent_item_id', $res->getOrderItemId());
                    $configurableItemArr = $configurableSalesItem->getOrderedProductId();
                    $configurableItemId = $res['mageproduct_id'];
                    if (!empty($configurableItemArr)) {
                        $configurableItemId = $configurableItemArr[0];
                    }
                    $product = $this->_productRepository->getById($configurableItemId);
                    $stockRepository = $this->stockItem->load($configurableItemId,'product_id');
                } else {
                    $product = $this->_productRepository->getById($res['mageproduct_id']);
                    $stockRepository = $this->stockItem->load($res['mageproduct_id'],'product_id');
                }

                $sku = $product->getSku();
                $orderinfo = $orderinfo . "<tbody><tr>
                                <td class='item-info'>" . $productName . "</td>
                                <td class='item-info'>" . $sku . "</td>
                                <td class='item-qty'>" . ($res['magequantity'] * 1) . "</td>
                                <td class='item-price'>" .
                    $order->formatPrice(
                        $res['magepro_price'] * $res['magequantity']
                    ) .
                    '</td>
                             </tr></tbody>';
                $totalTaxAmount = $totalTaxAmount + $res['total_tax'];
                $totalprice = $totalprice + ($res['magepro_price'] * $res['magequantity']);

                /*
                * Low Stock Notification mail to seller
                */
                if ($this->_marketplaceHelper->getlowStockNotification()) {
                    $productStock = (int)$stockRepository->getNotifyStockQty();
                    $seller = $this->customerFactory->create()->load($info['seller_id']);
                    if (!empty($product['quantity_and_stock_status']['qty'])) {
                        $stockItemQty = $product['quantity_and_stock_status']['qty'];
                    } else {
                        $stockItemQty = $product->getQty();
                    }

                    $lowStockNumber = max($productStock, (int)$this->_marketplaceHelper->getlowStockQty());

                    if ($stockItemQty <= $lowStockNumber) {

                        $emailTemplateVariables = [];
                        $emailTemplateVariables['seller_name'] = $seller->getFirstname() . ' ' . $seller->getLastname();
                        $emailTemplateVariables['product_name'] = $productName;
                        $emailTemplateVariables['stockItemQty'] = $stockItemQty * 1;
                        $emailTemplateVariables['store_id'] = $seller->getData('store_id');
                        $emailTemplateVariables['email_content_alignment'] = 'left';
                        $emailTemplateVariables['email_content_direction'] = 'ltr';

                        $store = $this->storeManager->getStore($emailTemplateVariables['store_id']);
                        if ($store->getCode() === 'ar_SA') {
                            $emailTemplateVariables['email_content_alignment'] = 'right';
                            $emailTemplateVariables['email_content_direction'] = 'rtl';
                        }

                        $baseUrl = $this->url->getBaseUrl();
                        $emailTemplateVariables['my_products_list_url'] = $baseUrl . 'marketplace/product/productlist/';

                        $this->email->sendLowStockNotificationMail(
                            $emailTemplateVariables,
                            $senderInfo,
                            $receiverInfo
                        );
                    }
                }
            }
            $shippingCharges = $info->getShippingCharges();
            $couponAmount = $info->getCouponAmount();
            $totalCod = 0;

            if ($paymentCode == 'mpcashondelivery') {
                $totalCod = $info->getCodCharges();
                $codRow = "<tr class='subtotal'>
                            <th colspan='3'>" . __('Cash On Delivery Charges') . "</th>
                            <td colspan='3'><span>" .
                    $order->formatPrice($totalCod) .
                    '</span></td>
                            </tr>';
            } else {
                $codRow = '';
            }

            $orderinfo = $orderinfo . "<tfoot class='order-totals'>
                                <tr class='subtotal'>
                                    <th colspan='3'>" . __('Shipping & Handling Charges') . "</th>
                                    <td colspan='3'><span>" .
                $order->formatPrice($shippingCharges) . "</span></td>
                                </tr>
                                <tr class='subtotal'>
                                    <th colspan='3'>" . __('Discount') . "</th>
                                    <td colspan='3'><span> -" .
                $order->formatPrice($couponAmount) .
                "</span></td>
                                </tr>
                                <tr class='subtotal'>
                                    <th colspan='3'>" . __('Tax Amount') . "</th>
                                    <td colspan='3'><span>" .
                $order->formatPrice($totalTaxAmount) . '</span></td>
                                </tr>' . $codRow . "
                                <tr class='subtotal'>
                                    <th colspan='3'>" . __('Grandtotal') . "</th>
                                    <td colspan='3'><span>" .
                $order->formatPrice(
                    $totalprice +
                    $totalTaxAmount +
                    $shippingCharges +
                    $totalCod -
                    $couponAmount
                ) . '</span></td>
                                </tr></tfoot>';

            if ($shippingInfo != '') {
                $isNotVirtual = 1;
            } else {
                $isNotVirtual = 0;
            }
            $emailTempVariables['myvar1'] = $order->getRealOrderId();
            $emailTempVariables['myvar2'] = $order['created_at'];
            $emailTempVariables['myvar4'] = $billinginfo;
            $emailTempVariables['myvar5'] = $payment;
            $emailTempVariables['myvar6'] = $shippingInfo;
            $emailTempVariables['isNotVirtual'] = $isNotVirtual;
            $emailTempVariables['myvar9'] = $shippingDes;
            $emailTempVariables['myvar8'] = $orderinfo;
            $emailTempVariables['myvar3'] = $username;

            if ($this->_marketplaceHelper->getOrderApprovalRequired()) {
                $order->setOrderApprovalStatus(0)->save();
            }

            $this->mpEmailHelper->sendPlacedOrderEmail(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo
            );
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * Get Seller's Product Data.
     *
     * @param \Magento\Sales\Model\Order $order
     * @param int                        $ratesPerCurrency
     *
     * @return array
     */
    public function getSellerProductData($order, $ratesPerCurrency)
    {
        $lastOrderId = $order->getId();
        /*
        * Get Global Commission Rate for Admin
        */
        $percent = $this->_marketplaceHelper->getConfigCommissionRate();

        $sellerProArr = [];
        $sellerTaxArr = [];
        $sellerCouponArr = [];
        $isShippingFlag = [];
        /*
        * Marketplace Credit discount data
        */
        $discountDetails = [];
        $discountDetails = $this->_coreSession->getData('salelistdata');

        foreach ($order->getAllItems() as $item) {
            $itemData = $item->getData();
            $sellerId = $this->getSellerIdPerProduct($item);
            $calculationStatus = true;
            if ($itemData['product_type'] == 'bundle') {
                $productOptions = $item->getProductOptions();
                $calculationStatus = $productOptions['product_calculations'] ? true : false;
            }
            if ($calculationStatus) {
                $isShippingFlag = $this->getShippingFlag($item, $sellerId, $isShippingFlag);

                $price = $itemData['base_price'];

                $taxamount = $itemData['base_tax_amount'];
                $qty = $item->getQtyOrdered();

                $totalamount = $qty * $price;

                $advanceCommissionRule = $this->_customerSession->getData(
                    'advancecommissionrule'
                );

                //Calculate commission after discount from promocode
                $baseDiscountAmount = $itemData['base_discount_amount'];
                $totalAmountForCommission = $totalamount - $baseDiscountAmount;
 
                $commission = $this->getCommission($sellerId, $totalAmountForCommission, $item, $advanceCommissionRule);                

                $actparterprocost = $totalAmountForCommission - $commission;
            } else {
                if (empty($isShippingFlag[$sellerId])) {
                    $isShippingFlag[$sellerId] = 0;
                }
                $price = 0;
                $taxamount = 0;
                $qty = $item->getQtyOrdered();
                $totalamount = 0;
                $commission = 0;
                $actparterprocost = 0;
            }

            $collectionsave = $this->saleslistFactory->create();
            $collectionsave->setMageproductId($item->getProductId());
            $collectionsave->setOrderItemId($item->getItemId());
            $collectionsave->setParentItemId($item->getParentItemId());
            $collectionsave->setOrderId($lastOrderId);
            $collectionsave->setMagerealorderId($order->getIncrementId());
            $collectionsave->setMagequantity($qty);
            $collectionsave->setSellerId($sellerId);
            $collectionsave->setCpprostatus(\Webkul\Marketplace\Model\Saleslist::PAID_STATUS_PENDING);
            $collectionsave->setMagebuyerId($this->_customerSession->getCustomerId());
            $collectionsave->setMageproPrice($price);
            $collectionsave->setMageproName($item->getName());
            if ($totalamount != 0) {
                $collectionsave->setTotalAmount($totalamount);
                $commissionRate = ($commission * 100) / $totalAmountForCommission;
            } else {
                $collectionsave->setTotalAmount($price);
                $commissionRate = $percent;
            }
            $collectionsave->setTotalTax($taxamount);
            if (!$this->_marketplaceHelper->isSellerCouponModuleInstalled()) {
                if (isset($itemData['base_discount_amount'])) {
                    $baseDiscountAmount = $itemData['base_discount_amount'];
                    $collectionsave->setIsCoupon(1);
                    $collectionsave->setAppliedCouponAmount($baseDiscountAmount);

                    if (!isset($sellerCouponArr[$sellerId])) {
                        $sellerCouponArr[$sellerId] = 0;
                    }
                    $sellerCouponArr[$sellerId] = $sellerCouponArr[$sellerId] + $baseDiscountAmount;
                }
            }
            $collectionsave->setTotalCommission($commission);
            $collectionsave->setActualSellerAmount($actparterprocost);
            $collectionsave->setCommissionRate($commissionRate);
            $collectionsave->setCurrencyRate($ratesPerCurrency);
            if (isset($isShippingFlag[$sellerId])) {
                $collectionsave->setIsShipping($isShippingFlag[$sellerId]);
            }
            if ($item->getProductType() == 'virtual') {
                $collectionsave->setIsShipping(0);
            }
            $collectionsave->setCreatedAt($this->_date->gmtDate());
            $collectionsave->setUpdatedAt($this->_date->gmtDate());
            $collectionsave->save();
            $qty = 0;
            if (!isset($sellerTaxArr[$sellerId])) {
                $sellerTaxArr[$sellerId] = 0;
            }
            $sellerTaxArr[$sellerId] = $sellerTaxArr[$sellerId] + $taxamount;
            if ($price != 0.0000) {
                if (!isset($sellerProArr[$sellerId])) {
                    $sellerProArr[$sellerId] = [];
                }
                array_push($sellerProArr[$sellerId], $item->getProductId());
            } else {
                if (!$item->getParentItemId()) {
                    if (!isset($sellerProArr[$sellerId])) {
                        $sellerProArr[$sellerId] = [];
                    }
                    array_push($sellerProArr[$sellerId], $item->getProductId());
                }
            }
        }

        return [
            'seller_pro_arr' => $sellerProArr,
            'seller_tax_arr' => $sellerTaxArr,
            'seller_coupon_arr' => $sellerCouponArr
        ];
    }
}
