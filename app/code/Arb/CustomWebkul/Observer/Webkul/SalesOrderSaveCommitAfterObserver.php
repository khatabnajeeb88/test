<?php
/**
 * Adjust status change after Order gets completed
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Observer\Webkul;

use Webkul\Marketplace\Observer\SalesOrderSaveCommitAfterObserver as ObserverParent;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Saleslist;

/**
 * Remove forced changing of order status to 'complete' if it's 'shipped' already
 */
class SalesOrderSaveCommitAfterObserver extends ObserverParent
{
    /**
     * Removed forced changing of order status to 'complete' if it's already 'shipped'
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        // skip code coverage as this is webkul method with adjustments
        // @codeCoverageIgnoreStart
        $order = $observer->getOrder();

        $lastOrderId = $observer->getOrder()->getId();
        $helper = $this->marketplaceHelper;

        if ($order->getState() == 'complete') {
            /*
            * Calculate cod and shipping charges if applied
            */
            $ordercollection = $this->saleslistFactory->create()
                ->getCollection()
                ->addFieldToFilter('order_id', $lastOrderId)
                ->addFieldToFilter(
                    'cpprostatus',
                    Saleslist::PAID_STATUS_PENDING
                );

            foreach ($ordercollection as $item) {
                $sellerId = $item->getSellerId();
                $taxAmount = $item['total_tax'];

                $taxToSeller = $helper->getConfigTaxManage();
                $marketplaceOrders = $this->ordersFactory->create()
                    ->getCollection()
                    ->addFieldToFilter('order_id', $lastOrderId)
                    ->addFieldToFilter('seller_id', $item['seller_id']);

                foreach ($marketplaceOrders as $tracking) {
                    $taxToSeller = $tracking['tax_to_seller'];

                    $status = $tracking->getOrderStatus();
                    if ($status != 'closed' || $status != 'canceled') {
                        // making this a separate IF on purpose.
                        if ($status !== 'shipped') {
                            $tracking->setOrderStatus('complete')->save();
                        }
                    }
                }

                if (!$taxToSeller) {
                    $taxAmount = 0;
                }

                $shippingCharges = 0;
                $codCharges = $item->getCodCharges();
                /*
                 * Calculate cod and shipping charges if applied
                 */
                if ($item->getIsShipping() == 1) {
                    $marketplaceOrders = $this->ordersFactory->create()
                        ->getCollection()
                        ->addFieldToFilter('order_id', $lastOrderId)
                        ->addFieldToFilter('seller_id', $item['seller_id']);
                    foreach ($marketplaceOrders as $tracking) {
                        $shippingamount = $tracking->getShippingCharges();
                        $refundedShippingAmount = $tracking->getRefundedShippingCharges();
                        $shippingCharges = $shippingamount - $refundedShippingAmount;
                    }
                }
                $totalTaxShipping = $taxAmount + $codCharges + $shippingCharges - $item['applied_coupon_amount'];
                $actparterprocost = $item->getActualSellerAmount() + $totalTaxShipping;
                $totalamount = $item->getTotalAmount() + $totalTaxShipping;
                $codCharges = 0;

                $collectionverifyread = $this->saleperpartnerFactory->create()->getCollection();
                $collectionverifyread->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );

                if ($collectionverifyread->getSize() >= 1) {
                    foreach ($collectionverifyread as $verifyrow) {
                        $totalsale = $verifyrow->getTotalSale() + $totalamount;
                        $totalremain = $verifyrow->getAmountRemain() + $actparterprocost;
                        $verifyrow->setTotalSale($totalsale);
                        $verifyrow->setAmountRemain($totalremain);
                        $verifyrow->setCommissionRate($item->getCommissionRate());
                        $totalcommission = $verifyrow->getTotalCommission() +
                            ($totalamount - $actparterprocost);
                        $verifyrow->setTotalCommission($totalcommission);
                        $verifyrow->setUpdatedAt($this->_date->gmtDate());
                        $verifyrow->save();
                    }
                } else {
                    $collectionf = $this->saleperpartnerFactory->create();
                    $collectionf->setSellerId($sellerId);
                    $collectionf->setTotalSale($totalamount);
                    $collectionf->setAmountRemain($actparterprocost);
                    $collectionf->setCommissionRate($item->getCommissionRate());
                    $totalcommission = $totalamount - $actparterprocost;
                    $collectionf->setTotalCommission($totalcommission);
                    $collectionf->setCreatedAt($this->_date->gmtDate());
                    $collectionf->setUpdatedAt($this->_date->gmtDate());
                    $collectionf->save();
                }

                if ($sellerId) {
                    $ordercount = 0;
                    $feedbackcount = 0;
                    $feedcountid = 0;
                    $collectionfeed = $this->feedbackcountFactory->create()
                        ->getCollection()
                        ->addFieldToFilter(
                            'seller_id',
                            $sellerId
                        )->addFieldToFilter(
                            'buyer_id',
                            $order->getCustomerId()
                        );
                    foreach ($collectionfeed as $value) {
                        $feedcountid = $value->getEntityId();
                        $ordercount = $value->getOrderCount();
                        $feedbackcount = $value->getFeedbackCount();
                    }
                    $collectionfeed = $this->feedbackcountFactory->create()->load($feedcountid);
                    $collectionfeed->setBuyerId($order->getCustomerId());
                    $collectionfeed->setSellerId($sellerId);
                    $collectionfeed->setOrderCount($ordercount + 1);
                    $collectionfeed->setFeedbackCount($feedbackcount);
                    $collectionfeed->setCreatedAt($this->_date->gmtDate());
                    $collectionfeed->setUpdatedAt($this->_date->gmtDate());
                    $collectionfeed->save();
                }
                $item->setUpdatedAt($this->_date->gmtDate());
                $item->setCpprostatus(Saleslist::PAID_STATUS_COMPLETE)->save();
            }
        }
        // @codeCoverageIgnoreEnd
    }
}
