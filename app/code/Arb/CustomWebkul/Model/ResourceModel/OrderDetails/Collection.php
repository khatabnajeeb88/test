<?php
/**
 * Collection for Order Extended Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel\OrderDetails;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Arb\CustomWebkul\Model\ResourceModel\OrderDetailsResource;
use Arb\CustomWebkul\Model\OrderDetails;

class Collection extends AbstractCollection
{
    /**
     * Define Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(OrderDetails::class, OrderDetailsResource::class);
    }
}
