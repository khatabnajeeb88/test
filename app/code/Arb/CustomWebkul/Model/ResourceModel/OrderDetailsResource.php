<?php
/**
 * Resource Model for Extended Order Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Arb\CustomWebkul\Api\Data\OrderDetailsInterface;

class OrderDetailsResource extends AbstractDb
{
    /**
     * Initialize Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(OrderDetailsInterface::ORDER_TABLE_NAME, OrderDetailsInterface::ORDER_ENTITY_ID);
    }
}
