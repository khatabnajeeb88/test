<?php 
/**
 * Resource Model for Extended Rma Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Arb\CustomWebkul\Api\Data\RmaDetailsInterface;

class RmaDetailsResource extends AbstractDb
{
    /**
     * Initialize Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(RmaDetailsInterface::RMA_TABLE_NAME, RmaDetailsInterface::RMA_ENTITY_ID);
    }
}
