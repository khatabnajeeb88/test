<?php
/**
 * Collection for Rma Extended Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel\RmaDetails;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetailsResource;
use Arb\CustomWebkul\Model\RmaDetails;

class Collection extends AbstractCollection
{
    /**
     * Define Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(RmaDetails::class, RmaDetailsResource::class);
    }
}
