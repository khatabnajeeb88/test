<?php 
/**
 * Resource Model for Extended Rma Item Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Arb\CustomWebkul\Api\Data\RmaItemInterface;

class RmaItemResource extends AbstractDb
{
    /**
     * Initialize Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(RmaItemInterface::RMA_ITEM_TABLE_NAME, RmaItemInterface::RMA_ITEM_ENTITY_ID);
    }
}
