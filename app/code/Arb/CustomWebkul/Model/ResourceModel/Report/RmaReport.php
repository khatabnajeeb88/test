<?php
/**
 * Custom Filters collection for RMA Report
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel\Report;

use Magento\Sales\Model\ResourceModel\Report\AbstractReport;
use Arb\CustomWebkul\Model\Flag;
use Exception;
use Webkul\MpRmaSystem\Model\Details;
use Webkul\MpRmaSystem\Model\ResourceModel\Details\CollectionFactory;
use Webkul\MpRmaSystem\Model\ItemsFactory;
use Webkul\MpRmaSystem\Model\ReasonsFactory;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\Timezone\Validator;
use Magento\Reports\Model\FlagFactory;
use Webkul\MpRmaSystem\Helper\Data as WkRMAHelper;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

class RmaReport extends AbstractReport
{
    private const RMA_REPORT_TABLE = 'arb_rma_report';
    private const RMA_REPORT_ENTITY_ID = 'id';

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var DetailsFactory
     */
    private $detailsFactory;

    /**
     * @var WkRMAHelper
     */
    private $wkRMAHelper;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepositoryInterface;

    /**
     * @param Context $context
     * @param LoggerInterface $logger
     * @param TimezoneInterface $localeDate
     * @param FlagFactory $reportsFlagFactory
     * @param Validator $timezoneValidator
     * @param DateTime $dateTime
     * @param ResourceConnection $resource
     * @param TimezoneInterface $timezone
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SlaReportRepositoryInterface $slaReportRepository
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        TimezoneInterface $localeDate,
        FlagFactory $reportsFlagFactory,
        Validator $timezoneValidator,
        DateTime $dateTime,
        ResourceConnection $resource,
        TimezoneInterface $timezone,
        CollectionFactory $detailsFactory,
        WkRMAHelper $wkRMAHelper,
        OrderRepositoryInterface $orderRepositoryInterface,

        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $logger,
            $localeDate,
            $reportsFlagFactory,
            $timezoneValidator,
            $dateTime,
            $connectionName
        );

        $this->resource = $resource;
        $this->timezone = $timezone;
        $this->detailsFactory = $detailsFactory;
        $this->wkRMAHelper = $wkRMAHelper;
        $this->orderRepositoryInterface = $orderRepositoryInterface;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::RMA_REPORT_TABLE, self::RMA_REPORT_ENTITY_ID);
    }

    /**
     * @param string|int|DateTime|array|null $from
     * @param string|int|DateTime|array|null $to
     *
     * @return self
     */
    public function aggregate($from = null, $to = null)
    {
        $connection = $this->getConnection();

        if ($from === null) {
            $from = date('Y-m-d H:i:s');
        }else{
            /**
             * @var \DateTime
             */
            $from = $from->format('Y-m-d H:i:s');
        }

        $this->truncateTable();
        $rmaData = $this->filterRmaDataByDate($from);

        if (!empty($rmaData)) {
            $tableName = $this->resource->getTableName(self::RMA_REPORT_TABLE);
            foreach (array_chunk($rmaData, 100) as $batch) {
                $connection->insertMultiple($tableName, $batch);
            }

            $this->_setFlagData(Flag::REPORT_RMAREPORT_FLAG_CODE);
        }

        return $this;
    }

    /**
     * @return void
     */
    public function truncateTable()
    {
        $table = $this->resource->getTableName(self::RMA_REPORT_TABLE);
        $connection = $this->resource->getConnection();
        $connection->truncateTable($table);
    }

    /**
     * Prepares RMA data in array 
     *
     * @param  string  $date
     * @return array
     */
    private function filterRmaDataByDate($date){
        $wkRMADeatails = $this->detailsFactory->create();

        $wkRMADeatails->getSelect()->join(
            ['rma_items'=>$wkRMADeatails->getTable('marketplace_rma_items')],
            'main_table.id = rma_items.rma_id'
        );

        $wkRMADeatails->addFieldToFilter('created_date', ['lteq' => $date]);

        $data = [];
        if($wkRMADeatails->getSize() > 0){
            foreach ($wkRMADeatails as $rmaDetails) {
                $data[$rmaDetails->getId()]['period'] = $rmaDetails->getCreatedDate();
                $data[$rmaDetails->getId()]['store_id'] = $this->getStoreIdFromOrder($rmaDetails->getOrderId());
                $data[$rmaDetails->getId()]['order_id'] = $rmaDetails->getOrderRef();
                $data[$rmaDetails->getId()]['customer_name'] = $rmaDetails->getCustomerName();
                $data[$rmaDetails->getId()]['rma_id'] = $rmaDetails->getId();
                $data[$rmaDetails->getId()]['rma_status'] = $this->getRmaStatus($rmaDetails);
                $data[$rmaDetails->getId()]['rma_reason'] = $this->getRmaReason($rmaDetails->getReasonId());
                $data[$rmaDetails->getId()]['rma_returned_amount'] = $rmaDetails->getPrice();
                $data[$rmaDetails->getId()]['rma_merchant_name'] = $this->getSellerTitleById($rmaDetails->getSellerId());
            }
        }
        return $data;
    }

    /**
     * Returns RMA status
     *
     * @param Details $rmaDetails
     *
     * @return string
     */
    private function getRmaStatus($rmaDetails)
    {
        return $this->wkRMAHelper->getRmaStatusTitle($rmaDetails->getStatusId(), $rmaDetails->getFinalStatus());
    }

    /**
     * Returns RMA reason by id
     *
     * @param  int $resonId
     * @return string
     */
    private function getRmaReason($reasonId)
    {
        return $this->wkRMAHelper->getReasonById($reasonId);
    }

    /**
     * Returns seller data by id
     *
     * @param  int $sellerId
     * @return string
     */
    private function getSellerTitleById($sellerId)
    {
        $seller = $this->wkRMAHelper->getSellerDetails($sellerId);
        if ($seller) {
            $shopTitle = $seller->getShopTitle();
        } else {
            $shopTitle = '';
        }

        return $shopTitle;
    }

    /**
     * Get store id from order
     * 
     * @param int $orderId
     * @return int
     * @throws NoSuchEntityException
     */
    private function getStoreIdFromOrder($orderId){
        try{
           $order =  $this->orderRepositoryInterface->get($orderId);
           return $order->getStoreId();
        }catch(NoSuchEntityException $e){
            return false;
        }
        
    }
}
