<?php
/**
 * Custom Filters collection for RMA Report
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel\Report\RmaReport;

use Magento\Sales\Model\ResourceModel\Report\Collection\AbstractCollection;
use Zend_Db_Expr;
use Magento\Sales\Model\ResourceModel\Report;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\EntityFactory;

class Collection extends AbstractCollection
{
    /**
     * @var array
     */
    protected $_selectedColumns = [];

    /**
     * @var Zend_Db_Expr
     */
    private $_periodFormat;

    /**
     * @var string
     */
    protected $orderId;

    /**
     * @var array
     */
    private $rmaReason;

    /**
     * @var array
     */
    private $rmaStatus;

    /**
     * @var int
     */
    private $rmaId;

    /**
     * @var array
     */
    private $customers;

    /**
     * @var array
     */
    private $merchants;

    /**
     * @var float
     */
    private $returnedAmount;

    /**
     * @param EntityFactory $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param Report $resource
     * @param AdapterInterface $connection
     */
    public function __construct(
        EntityFactory $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        Report $resource,
        AdapterInterface $connection = null
    ) {
        $resource->init('arb_rma_report');
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $resource, $connection);
    }
    
    /**
     * Retrieve selected columns
     *
     * @return array
     */
    protected function _getSelectedColumns()
    {
        $connection = $this->getConnection();
        $this->_periodFormat = $connection->getDateFormatSql('period', '%Y-%m-%d %H:%i:%s');

        if (!$this->_selectedColumns) {
            if ($this->isTotals()) {
                $this->_selectedColumns = ['period'];
            } else {
                $this->_selectedColumns = [
                    'period' => sprintf('MAX(%s)', $connection->getDateFormatSql('period', '%Y-%m-%d %H:%i:%s')),
                    'order_id' => 'order_id',
                    'customer_name' => 'customer_name',
                    'rma_status' => 'rma_status',
                    'rma_reason' => 'rma_reason',
                    'rma_id' => 'rma_id',
                    'rma_returned_amount' => 'rma_returned_amount',
                    'rma_merchant_name' => 'rma_merchant_name',
                ];
            }
        }

        return $this->_selectedColumns;
    }

    /**
     * @return AbstractCollection
     *
     * @throws LocalizedException
     */
    protected function _applyAggregatedTable()
    {
        $this->getSelect()->from($this->getResource()->getMainTable(), $this->_getSelectedColumns());
        if ($this->isSubTotals()) {
            $this->getSelect()->group('period');
        } elseif (!$this->isTotals()) {
            $this->getSelect()->group(
                [
                    'period'
                ]
            );
        }

        return $this;
    }

    /**
     * Add filtering by order id
     *
     * @param string $orderId
     *
     * @return self
     */
    public function addOrderIdFilter(string $orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Add filtering by rma reason
     *
     * @param array $rmaReasons
     *
     * @return self
     */
    public function addRmaReasonFilter(array $rmaReasons)
    {
        $this->rmaReason = $rmaReasons;

        return $this;
    }

    /**
     * Add filtering by rma status
     *
     * @param array $rmaStatus
     *
     * @return self
     */
    public function addRmaStatusFilter(array $rmaStatus)
    {
        $this->rmaStatus = $rmaStatus;

        return $this;
    }

    /**
     * Add filtering by merchant name
     *
     * @param array $merchants
     *
     * @return self
     */
    public function addMerchantFilter(array $merchants)
    {
        $this->merchants = $merchants;

        return $this;
    }

    /**
     * Add filtering by customer name
     *
     * @param array $customers
     *
     * @return self
     */
    public function addCustomerFilter(array $customers)
    {
        $this->customers = $customers;

        return $this;
    }

    /**
     * Add filtering by rmaId
     *
     * @param int $rmaIds
     *
     * @return self
     */
    public function addRmaIdFilter(int $rmaIds)
    {
        $this->rmaId = $rmaIds;

        return $this;
    }

    /**
     * Add filtering by returned amount
     *
     * @param float $amount
     *
     * @return self
     */
    public function addReturnedAmountFilter(float $amount)
    {
        $this->returnedAmount = $amount;

        return $this;
    }

    /**
     * Adds order id condition filter
     *
     * @return self
     */
    protected function applyOrderIdFilter()
    {
        if (!$this->orderId) {
            return $this;
        }

        $rmaIdFilterSqlParts[] = $this->getConnection()->quoteInto('order_id = ?', $this->orderId);

        if (!empty($rmaIdFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $rmaIdFilterSqlParts));
        }

        return $this;
    }

    /**
     * Adds rma status condition filter
     *
     * @return self
     */
    protected function applyStatusFilter()
    {
        if (empty($this->rmaStatus) || !is_array($this->rmaStatus)) {
            return $this;
        }

        $rmaStatusFilterSqlParts = [];
        foreach ($this->rmaStatus as $status) {
            $rmaStatusFilterSqlParts[] = $this->getConnection()->quoteInto('rma_status = ?', $status);
        }

        if (!empty($rmaStatusFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $rmaStatusFilterSqlParts));
        }

        return $this;
    }

    /**
     * Adds merchant name condition filter
     *
     * @return self
     */
    protected function applyMerchantFilter()
    {
        if (empty($this->merchants) || !is_array($this->merchants)) {
            return $this;
        }

        $merchantFilterSqlParts = [];
        foreach ($this->merchants as $merchant) {
            $merchantFilterSqlParts[] = $this->getConnection()->quoteInto('rma_merchant_name = ?', $merchant);
        }

        if (!empty($merchantFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $merchantFilterSqlParts));
        }

        return $this;
    }

    /**
     * Adds customer name condition filter
     *
     * @return self
     */
    protected function applyCustomerFilter()
    {
        if (empty($this->customers) || !is_array($this->customers)) {
            return $this;
        }

        $customerFilterSqlParts = [];
        foreach ($this->customers as $customer) {
            $customerFilterSqlParts[] = $this->getConnection()->quoteInto('customer_name = ?', $customer);
        }

        if (!empty($customerFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $customerFilterSqlParts));
        }

        return $this;
    }

    /**
     * Adds Rma Id condition filter
     *
     * @return self
     */
    protected function applyRmaIdFilter()
    {
        if (!$this->rmaId) {
            return $this;
        }

        $rmaIdFilterSqlParts[] = $this->getConnection()->quoteInto('rma_id = ?', $this->rmaId);

        if (!empty($rmaIdFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $rmaIdFilterSqlParts));
        }

        return $this;
    }

    /**
     * Adds rma reason condition filter
     *
     * @return self
     */
    protected function applyReasonFilter()
    {
        if (empty($this->rmaReason) || !is_array($this->rmaReason)) {
            return $this;
        }

        $rmaReasonFilterSqlParts = [];
        foreach ($this->rmaReason as $reason) {
            $rmaReasonFilterSqlParts[] = $this->getConnection()->quoteInto('rma_reason = ?', $reason);
        }

        if (!empty($rmaReasonFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $rmaReasonFilterSqlParts));
        }

        return $this;
    }

    /**
     * Adds returned amount condition filter
     *
     * @return self
     */
    protected function applyAmountFilter()
    {
        if (!$this->returnedAmount) {
            return $this;
        }

        $rmaAmountFilterSqlParts[] = $this->getConnection()
            ->quoteInto('rma_returned_amount = ?', $this->returnedAmount);

        if (!empty($rmaAmountFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $rmaAmountFilterSqlParts));
        }

        return $this;
    }


    /**
     * Apply collection custom filter
     *
     * @return AbstractCollection
     */
    protected function _applyCustomFilter()
    {
        $this->applyOrderIdFilter();
        $this->applyStatusFilter();
        $this->applyReasonFilter();
        $this->applyMerchantFilter();
        $this->applyCustomerFilter();
        $this->applyRmaIdFilter();
        $this->applyAmountFilter();

        return parent::_applyCustomFilter();
    }
}
