<?php
/**
 * Collection for Rma Extended Item Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\ResourceModel\RmaItem;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Arb\CustomWebkul\Model\ResourceModel\RmaItemResource;
use Arb\CustomWebkul\Model\RmaItem;

class Collection extends AbstractCollection
{
    /**
     * Define Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(RmaItem::class, RmaItemResource::class);
    }
}
