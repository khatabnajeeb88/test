<?php
/**
 * Repository for Extended Order Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model;

use Arb\CustomWebkul\Api\Data\OrderDetailsInterface;
use Arb\CustomWebkul\Api\OrderDetailsRepositoryInterface;
use Arb\CustomWebkul\Model\ResourceModel\OrderDetailsResource;
use Arb\CustomWebkul\Model\ResourceModel\OrderDetails\Collection;
use Arb\CustomWebkul\Model\ResourceModel\OrderDetails\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Arb\CustomWebkul\Model\OrderDetailsFactory;
use Arb\CustomWebkul\Api\Data\RmaDetailsInterface;

class OrderDetailsRepository implements OrderDetailsRepositoryInterface
{
    /**
     * @var OrderDetailsResource
     */
    private $resource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param OrderDetailsResource $resource
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        OrderDetailsResource $resource,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param OrderDetailsInterface $orderDetails
     *
     * @return OrderDetailsInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(OrderDetailsInterface $orderDetails)
    {
        try {
            $this->resource->save($orderDetails);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $orderDetails;
    }
    
    /**
     * @param int $id
     *
     * @return OrderDetailsInterface|null
     */
    public function getByOrderId(int $id)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            'order_id',
            $id
        )->create();

        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        return $collection->getSize() > 0 ? $collection->getFirstItem() : null;
    }
}
