<?php
/**
 * Model class file for sending order email for physical products
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model;

use Arb\CustomWebkul\Model\Webkul\ResourceModel\Saleslist\Collection;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\Marketplace\Model\Saleslist;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Helper\Orders as OrdersHelper;

/**
 * Order email transaction
 */
class OrderCompleteEmailSender
{   
    /**
     * Seller image store location
     */
    const SELLER_PROFILE_IMAGE_LOCATION = 'avatar/';

    /**
     * seller image placehoder
     */
    const SELLER_NO_IMAGE = 'noimage.png';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Webkul\Marketplace\Model\ProductFactory
     */
    protected $wkProductFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $wkHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Helper\Image $imageHelper
     */
    protected $imageHelper;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $pricingHelper;
    
    /**
     * @var string
     */
    protected $templateId;

    /**
     * @var \Webkul\Marketplace\Model\OrdersFactory
     */
    protected $wkOrders;

    /**
     * @var \Webkul\Marketplace\Model\SaleslistFactory
     */
    protected $salesList;

    /**
     * @var Arb\CustomWebkul\Helper\Email
     */
    protected $wkEmail;

    /**
     * @var SaleslistFactory
     */
    private $saleslistFactory;
    /**
     * @var OrdersHelper
     */
    private $ordersHelper;

    /**
     * Order complete email class constructor
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Webkul\Marketplace\Helper\Data $wkHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Sales\Api\Data\OrderInterface $orderRepository
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param \Webkul\Marketplace\Model\SaleslistFactory $salesList
     * @param \Arb\CustomWebkul\Helper\Email $wkEmail
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Webkul\Marketplace\Helper\Data $wkHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Sales\Api\Data\OrderInterface $orderRepository,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Webkul\Marketplace\Model\SaleslistFactory $salesList,
        \Arb\CustomWebkul\Helper\Email $wkEmail,
        SaleslistFactory $saleslistFactory,
        OrdersHelper $ordersHelper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->wkHelper = $wkHelper;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->orderRepository = $orderRepository;
        $this->pricingHelper = $pricingHelper;
        $this->salesList = $salesList;
        $this->wkEmail = $wkEmail;
        $this->saleslistFactory = $saleslistFactory;
        $this->ordersHelper = $ordersHelper;
    }

    /**
     * Prepares order data for physical order email
     *
     * @param  int $orderId
     * @param \Webkul\Marketplace\Model\Orders $salesList
     * @param int $sellerId
     *
     * @return void
     */
    public function prepareCompleteOrderEmail($orderId, $salesList, $sellerId, $senderInfo)
    {
        $emailTemplateVariables = [];
        try {
            $order = $this->orderRepository->load($orderId);
        } catch (NoSuchEntityException $e) {
            return false;
        }

        $emailTemplateVariables['increment_id'] = $order->getIncrementId();
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = __($method->getTitle());
        $emailTemplateVariables['payment_method'] = $methodTitle;

        $shipments = $salesList->getItems();

        $allProducts = '';
        foreach ($shipments as $shipment) {
            $allProducts .= $shipment->getData('product_ids');
        }

        $emailTemplateVariables['item_count'] = count(explode(',',$allProducts));
        $emailTemplateVariables['shipments_count'] = count($salesList->getItems());

        $emailTemplateVariables['seller_data'] = $this->prepareMerchantData($sellerId);
        $shippingAddress = $order->getShippingAddress();

        $address = [
            $shippingAddress->getFirstname().' '. $shippingAddress->getLastname(),
            implode(' ', $shippingAddress->getStreet()),
            $shippingAddress->getCity(),
            $shippingAddress->getPostCode(),
            $shippingAddress->getCountryId()
        ];

        $addressLine = implode(', ', $address);
        $emailTemplateVariables['formattedShippingAddress'] = $addressLine;
        $emailTemplateVariables['customer_mobile'] = $order->getShippingAddress()->getTelephone();
        $emailTemplateVariables['total_amount'] =
            $this->pricingHelper->currency($this->getTotalOrderedAmount($sellerId, $orderId), true, false);
        $emailTemplateVariables['customer_name'] = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();
        $emailTemplateVariables['store_id'] = $order->getStoreId();
        $emailTemplateVariables['email_content_alignment'] = 'left';
        $emailTemplateVariables['email_content_direction'] = 'ltr';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $emailTemplateVariables['email_content_alignment'] = 'right';
            $emailTemplateVariables['email_content_direction'] = 'rtl';
        }

        $senderInfo = [
            'name' => $senderInfo['name'],
            'email' => $senderInfo['email'],
        ];

        $receiverInfo = [
            'name' => $order->getCustomerFirstname(),
            'email' => $order->getCustomerEmail(),
        ];

        $this->wkEmail->sendOrderCompleteMail($emailTemplateVariables, $senderInfo, $receiverInfo);
    }

    /**
     * @param $sellerId
     * @param $orderId
     *
     * @return string
     */
    private function getTotalOrderedAmount($sellerId, $orderId)
    {
        $salesCollection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $orderId
            );
        $totalAmount = $this->getSaleCollectionData($salesCollection, 'total_amount');

        $tracking = $this->ordersHelper->getOrderinfo($orderId);
        $couponAmount = $tracking->getCouponAmount();
        $totalTax = $tracking->getTotalTax();
        $shippingCharges = $tracking->getShippingCharges();
        $totalAmount += $shippingCharges;
        $totalAmount -= $couponAmount;
        $totalAmount += $totalTax;

        $totalAmount = sprintf('%0.2f', $totalAmount);

        return $totalAmount;
    }

    /**
     * Getting specific sale data from collection
     *
     * @param Collection $collection
     * @param string $columnName
     *
     * @return float
     */
    private function getSaleCollectionData(Collection $collection, string $columnName)
    {
        $total = 0;

        $items = $collection->getItems();

        /** @var Saleslist $coll */
        foreach ($items as $coll) {
            $total += (float)$coll->getData($columnName);
        }

        return $total;
    }

    /**
     * Returns order confirmed product data in array
     *
     * @param  $sellerId
     *
     * @return array
     */
    protected function prepareMerchantData($sellerId)
    {
        $sellerData = [
            'merchant_name' => null,
            'merchant_contact' => null,
            'merchant_logo' => null
        ];
        $seller = $this->wkHelper->getSellerDataBySellerId($sellerId)->getData();
        if (!empty($seller[0])) {
            $sellerInfo = $seller[0];
            $sellerData['merchant_name'] = $sellerInfo['shop_title'];
            $sellerData['merchant_contact'] = $sellerInfo['contact_number'];
            $sellerLogo = !empty($sellerInfo['logo_pic']) ? $sellerInfo['logo_pic'] : self::SELLER_NO_IMAGE;
            $sellerData['merchant_logo'] =
            $this->wkHelper->getMediaUrl() . '/' . self::SELLER_PROFILE_IMAGE_LOCATION . $sellerLogo;
        }
        return $sellerData;
    }
}
