<?php

/**
 * This file consist of class that provides list of merchants
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model\Merchant;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Option\ArrayInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Listing merchants
 */
class MerchantList implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $sellerlistCollectionFactory;

    /**
     * MerchantList constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        SellerCollection $sellerlistCollectionFactory,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->sellerlistCollectionFactory = $sellerlistCollectionFactory;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Getting list of merchants
     *
     * @param bool $addEmpty
     *
     * @return array
     * @throws LocalizedException
     */
    public function toOptionArray($addEmpty = true)
    {
        $sellerCollection = $this->sellerlistCollectionFactory->create();
        $sellerCollection->addFieldToFilter(
            'is_seller',
            ['eq' => 1]
        )->addFieldToFilter(
            'store_id',
            0
        );
        $options = [];
        if ($addEmpty) {
            $options[] = ['label' => __('-- Please Select a Merchant --'), 'value' => ''];
        }
        foreach ($sellerCollection->getItems() as $merchant) {
            $customer = $this->customerRepository->getById($merchant->getData('seller_id'));
            $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
            $options[] = ['label' => __($customerName), 'value' => $merchant->getData('seller_id')];
        }
        return $options;
    }
}
