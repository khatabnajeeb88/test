<?php
/**
 * This file consist of class repository for email template
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model;

use Arb\CustomWebkul\Api\EmailTemplateRepositoryInterface;
use Magento\Email\Model\Template;
use Magento\Email\Model\ResourceModel\Template as TemplateResource;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Email\Model\TemplateFactory;

/**
 * EmailTemplateRepository class for saving and loading email templates
 */
class EmailTemplateRepository implements EmailTemplateRepositoryInterface
{
    /**
     * @var TemplateResource
     */
    private $templateResource;

    /**
     * @var TemplateFactory
     */
    private $templateFactory;

    /**
     * EmailTemplateRepository constructor.
     *
     * @param TemplateResource $templateResource
     * @param TemplateFactory $templateFactory
     */
    public function __construct(
        TemplateResource $templateResource,
        TemplateFactory $templateFactory
    ) {
        $this->templateResource = $templateResource;
        $this->templateFactory = $templateFactory;
    }

    /**
     * @param Template $template
     *
     * @return Template
     *
     * @throws AlreadyExistsException
     */
    public function save(Template $template)
    {
        $this->templateResource->save($template);

        return $template;
    }

    /**
     * @param string $templateCode
     *
     * @return Template
     */
    public function load(string $templateCode)
    {
        $template = $this->templateFactory->create();
        $this->templateResource->load($template, $templateCode, 'template_code');

        return $template;
    }

    /**
     * @param string $templateCode
     *
     * @return Template
     */
    public function getByTemplateCode(string $templateCode)
    {
        $template = $this->templateFactory->create();
        $templates = $template->getCollection()
            ->addFieldToFilter(
                'orig_template_code',
                $templateCode
            );
        return $templates->getFirstItem();
    }
}
