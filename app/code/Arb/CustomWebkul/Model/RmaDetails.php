<?php
/**
 * Extended RMA Data Class with getters/setters
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Model;

use Magento\Framework\Model\AbstractModel;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetailsResource;
use Arb\CustomWebkul\Api\Data\RmaDetailsInterface;

class RmaDetails extends AbstractModel implements RmaDetailsInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init(RmaDetailsResource::class);
    }
}
