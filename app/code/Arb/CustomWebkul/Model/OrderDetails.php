<?php
/**
 * Extended Order Data Class with getters/setters
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Model;

use Arb\CustomWebkul\Api\Data\OrderDetailsInterface;
use Magento\Framework\Model\AbstractModel;
use Arb\CustomWebkul\Model\ResourceModel\OrderDetailsResource;

class OrderDetails extends AbstractModel implements OrderDetailsInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init(OrderDetailsResource::class);
    }
}
