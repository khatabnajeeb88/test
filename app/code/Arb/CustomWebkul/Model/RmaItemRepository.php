<?php
/**
 * Repository for Extended RMA Item
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model;

use Arb\CustomWebkul\Api\Data\RmaItemInterface;
use Arb\CustomWebkul\Api\RmaItemRepositoryInterface;
use Arb\CustomWebkul\Model\ResourceModel\RmaItemResource;
use Arb\CustomWebkul\Model\ResourceModel\RmaItem\Collection;
use Arb\CustomWebkul\Model\ResourceModel\RmaItem\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Arb\CustomWebkul\Model\RmaItemFactory;

class RmaItemRepository implements RmaItemRepositoryInterface
{
    /**
     * @var RmaItemResource
     */
    private $resource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param RmaItemResource $resource
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        RmaItemResource $resource,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param RmaItemInterface $rmaItem
     *
     * @return mixed
     * @throws CouldNotSaveException
     */
    public function save(RmaItemInterface $rmaItem)
    {
        try {
            $this->resource->save($rmaItem);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $rmaItem;
    }
    
    /**
     * @param int $id
     *
     * @return RmaItemInterface|null
     */
    public function getByRmaId(int $id)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            'rma_id',
            $id
        )->create();

        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        return $collection->getSize() > 0 ? $collection->getFirstItem() : null;
    }
}
