<?php
/**
 * Repository for Extended RMA Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model;

use Arb\CustomWebkul\Model\ResourceModel\RmaDetailsResource;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetails\Collection;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetails\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Arb\CustomWebkul\Model\RmaDetailsFactory;
use Arb\CustomWebkul\Api\RmaDetailsRepositoryInterface;
use Arb\CustomWebkul\Api\Data\RmaDetailsInterface;

class RmaDetailsRepository implements RmaDetailsRepositoryInterface
{
    /**
     * @var RmaDetailsResource
     */
    private $resource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param RmaDetailsResource $resource
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        RmaDetailsResource $resource,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param RmaDetailsInterface $rmaDetails
     *
     * @return mixed
     * @throws CouldNotSaveException
     */
    public function save(RmaDetailsInterface $rmaDetails)
    {
        try {
            $this->resource->save($rmaDetails);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $rmaDetails;
    }
    
    /**
     * @param int $id
     *
     * @return RmaDetailsInterface|null
     */
    public function getByRmaId(int $id)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            'rma_id',
            $id
        )->create();

        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        return $collection->getSize() > 0 ? $collection->getFirstItem() : null;
    }
}
