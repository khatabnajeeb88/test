<?php
/**
 * This file consist of class Merchant which is used to define listing for merchant.
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Model;

use \Webkul\Marketplace\Model\ResourceModel\Seller\Collection as sellerCollection;
use \Magento\Eav\Model\Entity\Attribute as eavAttribute;
use \Webkul\Marketplace\Block\Sellerlist as sellerBlock;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * class to get merchant product data
 */
class MerchantData
{
    const XML_PATH_DEFAULT_SHIPPING_FEE = 'carriers/mpfixrate/default_amount';

    /**
     * @var SellerCollection
     */
    protected $sellerCollection;

    /**
     * topbrand attribute
     *
     * @var string
     */
    protected $topbrandAttr = 'topbrand';

    /**
     * customer phone no. attribute
     *
     * @var string
     */
    protected $phoneAttr = 'phone_number';

    /**
     * filter indexer
     *
     * @var string
     */
    protected $filterIndex = '0';

    /**
     * Webkul seller block class
     *
     * @var [type]
     */
    protected $sellerBlock;

    /**
     * attribute class object
     *
     * @var [type]
     */
    protected $eavAttribute;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Merchant Construct
     *
     * @param sellerCollection $sellerCollection
     * @param sellerBlock $sellerBlock
     * @param eavAttribute $eavAttribute
     * @param CollectionProcessorInterface $collectionProcessor
     * @param CustomerRepositoryInterface $customerRepository
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        sellerCollection $sellerCollection,
        sellerBlock $sellerBlock,
        eavAttribute $eavAttribute,
        CollectionProcessorInterface $collectionProcessor,
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->sellerCollection = $sellerCollection;
        $this->sellerBlock = $sellerBlock;
        $this->eavAttribute = $eavAttribute;
        $this->collectionProcessor = $collectionProcessor;
        $this->customerRepository = $customerRepository;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * get all the merchant lost with filters and topseller
     * the seller lisitng is used from webkul module. This function adds the topseller filter,
     * pagination and sorting details for the listing.
     * The listing will be used as merchant lising api as well
     *
     * @param Object $searchCriteria
     * @return void
     */
    public function getMerchantList($searchCriteria)
    {
        //@codeCoverageIgnoreStart
        //code coverage is skipped as webkul dont provide the php-unit test cases
        $merchants = $this->sellerBlock->getSellerCollection();

        // apply searchCriteria to Seller Collection (if it is 'topbrand' - skip)
        $isTopBrand = false;
        $filterGroups = $searchCriteria->getFilterGroups();
        foreach ($filterGroups as $filterGroup) {
            $filters = $filterGroup->getFilters();
            foreach ($filters as $filter) {
                $field = $filter->getField();
                if ($field === $this->topbrandAttr) {
                    $isTopBrand = true;
                    break;
                }
            }
        }

        if (!$isTopBrand) {
            $this->collectionProcessor->process($searchCriteria, $merchants);
        }

        $customerAttribute = $this->sellerCollection->getTable('customer_entity_varchar');
        $attributeData = $this->eavAttribute->loadByCode('customer', $this->phoneAttr);

        //get customer phone number value
        $merchants->getSelect()->joinLeft(
            $customerAttribute.' as get_attr_val',
            'main_table.seller_id = get_attr_val.entity_id 
            AND get_attr_val.attribute_id = '.$attributeData->getAttributeId(),
            [
                $this->phoneAttr => 'get_attr_val.value',
            ]
        );

        // set topbrand filter
        if ($filters = $searchCriteria->getFilterGroups()) {
            $attrData = $filters[$this->filterIndex]->getFilters()[$this->filterIndex];
            $customerAttribute = $this->sellerCollection->getTable('customer_entity_int');
            if ($attrData->getField() == $this->topbrandAttr) {
                $attributeData = $this->eavAttribute->loadByCode('customer', 'wkv_'.$attrData->getField());
                if ($attributeData) {
                    $merchants->getSelect()->join(
                        $customerAttribute.' as topbrand',
                        'main_table.seller_id = topbrand.entity_id 
                        AND topbrand.attribute_id = '.$attributeData->getAttributeId().'
                        AND topbrand.value = '.$attrData->getValue(),
                        [
                            $this->topbrandAttr => 'topbrand.value', // at frontend top seller is used as label
                        ]
                    );
                }
            }
        }

        // set product list sort order
        if ($searchCriteria->getSortOrders()) {
            foreach ($searchCriteria->getSortOrders() as $sortSet) {
                $merchants->getSelect()->order($sortSet->getField().' '.$sortSet->getDirection());
            }
        }
   
        //set product list page size
        if ($searchCriteria->getPageSize()) {
            $merchants->setPageSize($searchCriteria->getPageSize());
        }

        //set product list to get current page
        if ($searchCriteria->getCurrentPage()) {
            $merchants->setCurPage($searchCriteria->getCurrentPage());
        }

        // Set Merchant Delivery Fee
        foreach ($merchants as $merchant) {
            $customer = $this->customerRepository->getById($merchant->getId());
            $shippingFee = $customer->getCustomAttribute('mpshipping_fixrate');

            if ($shippingFee) {
                $merchant->setData('delivery_fee', $shippingFee->getValue());
            } else {
                $merchant->setData(
                    'delivery_fee',
                    $this->scopeConfig->getValue(self::XML_PATH_DEFAULT_SHIPPING_FEE, 'store')
                );
            }
        }

        $merchants = $merchants->toArray();

        return  ["results" => $merchants];
        //@codeCoverageIgnoreEnd
    }
}
