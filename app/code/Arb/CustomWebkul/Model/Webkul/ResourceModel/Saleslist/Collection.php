<?php
/**
 * Overridden Collection file
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Model\Webkul\ResourceModel\Saleslist;

use Webkul\Marketplace\Model\ResourceModel\Saleslist\Collection as ParentCollection;

class Collection extends ParentCollection
{
    /**
     * Override original method to remove order_approval filter from collection
     *
     * @return self
     */
    public function getSellerOrderCollection()
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $salesOrder = $this->getTable('sales_order');
        $salesOrderItem = $this->getTable('sales_order_item');

        $this->getSelect()->join(
            $salesOrder . ' as so',
            'main_table.order_id = so.entity_id',
            ['status' => 'status']
        );

        $this->getSelect()->join(
            $salesOrderItem . ' as soi',
            'main_table.order_item_id = soi.item_id AND main_table.order_id = soi.order_id',
            [
                'item_id' => 'item_id',
                'qty_canceled' => 'qty_canceled',
                'qty_invoiced' => 'qty_invoiced',
                'qty_ordered' => 'qty_ordered',
                'qty_refunded' => 'qty_refunded',
                'qty_shipped' => 'qty_shipped',
                'product_options' => 'product_options',
                'mage_parent_item_id' => 'parent_item_id',
                'product_type' => 'product_type'
            ]
        );

        return $this;
        //@codeCoverageIgnoreEnd
    }
}
