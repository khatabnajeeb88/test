<?php
/**
 * Order Status Translation
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */
namespace Arb\CustomWebkul\Model\Product\Source;

use Webkul\Marketplace\Model\Product\Source\OrderListStatus as WebkulOrderListStatus;

/**
 * Class OrderListStatus
 */
class OrderListStatus extends WebkulOrderListStatus
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $availableOptions = parent::toOptionArray();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $colorClass = $value['value'];
            if($value['value'] == 'shipped') {
                $colorClass = 'processing';
            }
            $options[] = [
                'label' =>  __($value['label']),//Adding Translation as per standard.
                'row_label' =>  "<span class='wk-mp-grid-status wk-mp-grid-status-".
                    $colorClass."'>".__($value['label'])."</span>",
                'value' => $value['value'],
            ];
        }
        return $options;
        //@codeCoverageIgnoreEnd
    }
}
