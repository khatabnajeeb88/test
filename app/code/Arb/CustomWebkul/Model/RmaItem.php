<?php
/**
 * Extended RMA Item Data Class with getters/setters
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Model;

use Magento\Framework\Model\AbstractModel;
use Arb\CustomWebkul\Model\ResourceModel\RmaItemResource;
use Arb\CustomWebkul\Api\Data\RmaItemInterface;

class RmaItem extends AbstractModel implements RmaItemInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init(RmaItemResource::class);
    }
}
