<?php
/**
 * SLA Report Flag
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Model;

use Magento\Reports\Model\Flag as ParentFlag;

class Flag extends ParentFlag
{
    const REPORT_RMAREPORT_FLAG_CODE = 'report_rmareport_aggregated';
}
