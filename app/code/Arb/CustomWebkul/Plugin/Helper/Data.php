<?php
/**
 * RMA Email fallback plugin
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Plugin\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Webkul\MpRmaSystem\Helper\Data as WebkulData;

class Data
{
    private const CONFIG_PATH_IS_ENABLED = 'mprmasystem/settings/admin_notification';
    private const CONFIG_PATH_EMAIL = 'marketplace/general_settings/adminemail';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * If no Email provided in RMA Admin Setting, use the Default Marketplace one
     *
     * @param WebkulData $subject
     * @param string|null $returnValue
     *
     * @return string|null
     */
    public function afterGetAdminEmail(WebkulData $subject, ?string $returnValue)
    {
        $isRmaAdminEmailEnabled = $this->scopeConfig->getValue(
            self::CONFIG_PATH_IS_ENABLED,
            ScopeInterface::SCOPE_STORE
        );

        if ($returnValue && $isRmaAdminEmailEnabled) {
            return $returnValue;
        }

        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_EMAIL,
            ScopeInterface::SCOPE_STORE
        );
    }
}
