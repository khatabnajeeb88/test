<?php
/**
 * Save Extended Data for Rma Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Plugin\Webkul\MpRmaSystem\Model;

use Arb\CustomWebkul\Api\RmaDetailsRepositoryInterface;
use Arb\CustomWebkul\Model\RmaDetailsFactory;
use Webkul\MpRmaSystem\Model\Details as WebkulDetails;

class Details
{
    /**
     * @var RmaDetailsRepositoryInterface
     */
    private $detailsRepository;

    /**
     * @var RmaDetailsFactory
     */
    private $rmaDetailsFactory;

    /**
     * @param RmaDetailsRepositoryInterface $detailsRepository
     * @param RmaDetailsFactory $rmaDetailsFactory
     */
    public function __construct(
        RmaDetailsRepositoryInterface $detailsRepository,
        RmaDetailsFactory $rmaDetailsFactory
    ) {
        $this->detailsRepository = $detailsRepository;
        $this->rmaDetailsFactory = $rmaDetailsFactory;
    }

    /**
     * @param WebkulDetails $subject
     * @param WebkulDetails $result
     *
     * @return WebkulDetails
     */
    public function afterSave(WebkulDetails $subject, WebkulDetails $result)
    {
        $rmaDetails = $this->detailsRepository->getByRmaId($result->getId());

        if (!$rmaDetails) {
            $rmaDetails = $this->rmaDetailsFactory->create();
            $rmaDetails->setRmaId($result->getId());
        }

        if ($result->getDeclineReason()) {
            $rmaDetails->setDeclineReason($result->getDeclineReason());
        }

        if ($result->getTelephone()) {
            $rmaDetails->setTelephone($result->getTelephone());
        }

        $this->detailsRepository->save($rmaDetails);

        return $result;
    }

    /**
     * @param WebkulDetails $subject
     * @param WebkulDetails $result
     *
     * @return WebkulDetails
     */
    public function afterLoad(WebkulDetails $subject, WebkulDetails $result)
    {
        $rmaDetails = $this->detailsRepository->getByRmaId($result->getId());

        if ($rmaDetails) {
            $result->setDeclineReason($rmaDetails->getDeclineReason());
            $result->setTelephone($rmaDetails->getTelephone());
        }

        return $result;
    }
}
