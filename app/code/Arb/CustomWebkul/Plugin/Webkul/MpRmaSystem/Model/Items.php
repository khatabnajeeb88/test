<?php
/**
 * Save Extended Data for Rma Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Plugin\Webkul\MpRmaSystem\Model;

use Arb\CustomWebkul\Api\RmaItemRepositoryInterface;
use Arb\CustomWebkul\Model\RmaItemFactory;
use Webkul\MpRmaSystem\Model\Items as WebkulItems;

class Items
{
    /**
     * @var RmaItemFactory
     */
    private $rmaItemFactory;

    /**
     * @var RmaItemRepositoryInterface
     */
    private $rmaItemRepository;

    /**
     * @param RmaItemRepositoryInterface $rmaItemRepository
     * @param RmaItemFactory $rmaItemFactory
     */
    public function __construct(
        RmaItemRepositoryInterface $rmaItemRepository,
        RmaItemFactory $rmaItemFactory
    ) {
        $this->rmaItemRepository = $rmaItemRepository;
        $this->rmaItemFactory = $rmaItemFactory;
    }

    /**
     * @param WebkulItems $subject
     * @param WebkulItems $result
     *
     * @return WebkulItems
     */
    public function afterSave(WebkulItems $subject, WebkulItems $result)
    {
        $rmaItem = $this->rmaItemRepository->getByRmaId($result->getId());

        if (!$rmaItem) {
            $rmaItem = $this->rmaItemFactory->create();
            $rmaItem->setRmaId($result->getId());
        }

        if ($result->getItemCondition()) {
            $rmaItem->setItemCondition($result->getItemCondition());
        }
        $this->rmaItemRepository->save($rmaItem);

        return $result;
    }

    /**
     * @param WebkulItems $subject
     * @param WebkulItems $result
     *
     * @return WebkulItems
     */
    public function afterLoad(WebkulItems $subject, WebkulItems $result)
    {
        $rmaItem = $this->rmaItemRepository->getByRmaId($result->getId());

        if ($rmaItem) {
            $result->setItemCondition($rmaItem->getItemCondition());
        }

        return $result;
    }
}
