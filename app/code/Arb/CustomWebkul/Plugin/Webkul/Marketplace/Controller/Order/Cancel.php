<?php
/**
 * Plugin for updating cancel reason and Merchant Order Cancellation email sending
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Plugin\Webkul\Marketplace\Controller\Order;

use Arb\CustomWebkul\Helper\EmailVariables;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Sales\Api\Data\OrderInterface;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersModel;
use Webkul\Marketplace\Controller\Order\Cancel as WebkulCancel;
use Webkul\Marketplace\Helper\Orders;
use Magento\Customer\Model\Session;
use Arb\CustomWebkul\Helper\Email;
use Webkul\Marketplace\Model\SellerFactory;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Updating cancel reason and sending cancellation email to customer
 */
class Cancel
{
    /**
     * @var Orders
     */
    private $webkulOrders;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var MpOrdersModel
     */
    private $mpOrdersModel;

    /**
     * @var EmailVariables
     */
    private $emailVariables;

    /**
     * @var Data
     */
    private $pricingHelper;

    /**
     * Cancel constructor.
     *
     * @param Orders $webkulOrders
     * @param Email $email
     * @param Session $customerSession
     * @param OrderRepositoryInterface $orderRepository
     * @param MpOrdersModel $mpOrdersModel
     * @param EmailVariables $emailVariables
     * @param Data $pricingHelper
     */
    public function __construct(
        Orders $webkulOrders,
        Email $email,
        Session $customerSession,
        OrderRepositoryInterface $orderRepository,
        MpOrdersModel $mpOrdersModel,
        EmailVariables $emailVariables,
        Data $pricingHelper
    ) {
        $this->webkulOrders = $webkulOrders;
        $this->email = $email;
        $this->customerSession = $customerSession;
        $this->orderRepository = $orderRepository;
        $this->mpOrdersModel = $mpOrdersModel;
        $this->emailVariables = $emailVariables;
        $this->pricingHelper = $pricingHelper;
    }

    /**
     * Updating cancel reason and sending cancellation email to customer
     *
     * @param WebkulCancel $subject
     * @param Redirect $result
     *
     * @return Redirect
     * @throws LocalizedException
     */
    public function afterExecute(WebkulCancel $subject, Redirect $result)
    {
        $webkulOrderId = $subject->getRequest()->getParam('id');
        $merchantOrder = $this->webkulOrders->getOrderinfo($webkulOrderId);
        $status = $merchantOrder->getOrderStatus();
        $cancelReason = '';

        if ($status === 'canceled') {
            $cancelReason = __($subject->getRequest()->getParam('cancel_reason'));
            $merchantOrder->setData('cancel_reason', $cancelReason);
            $merchantOrder->save();
        }

        $order = $this->orderRepository->get($webkulOrderId);
        $sellerId = $this->customerSession->getCustomerId();

        $emailTemplateVariables =
            $this->emailVariables->prepareEmailTemplateOrderVariables($order, (int)$sellerId, (int)$webkulOrderId, 'cancel');
        $emailTemplateVariables['cancel_reason'] = $cancelReason;
        $emailTemplateVariables['total_amount'] =
            $this->pricingHelper->currency($emailTemplateVariables['total_amount'], true, false);

        $this->sendCancelEmail($emailTemplateVariables, $order);
        $this->updateMagentoOrderStatus((int)$webkulOrderId);

        return $result;
    }

    /**
     * Sending cancel email
     *
     * @param $emailTemplateVariables
     * @param $order
     */
    private function sendCancelEmail(array $emailTemplateVariables, OrderInterface $order)
    {
        $senderInfo = [
            'name' => $this->customerSession->getName(),
            'email' => $this->customerSession->getCustomer()->getEmail(),
        ];

        $receiverInfo = [
            'name' => $order->getCustomerFirstname(),
            'email' => $order->getCustomerEmail(),
        ];

        $emailTemplateVariables['store_id'] = $order->getStoreId();
        $emailTemplateVariables['email_content_alignment'] = 'left';
        $emailTemplateVariables['email_content_direction'] = 'ltr';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $emailTemplateVariables['email_content_alignment'] = 'right';
            $emailTemplateVariables['email_content_direction'] = 'rtl';
        }

        $this->email->sendOrderCancelMail($emailTemplateVariables, $senderInfo, $receiverInfo);
    }

    /**
     * Check if all Merchants consignments are canceled then mark Order as Canceled
     *
     * @param int $orderId
     *
     * @return void
     */
    private function updateMagentoOrderStatus(int $orderId)
    {
        $orderCollection = $this->mpOrdersModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            );

        foreach ($orderCollection->getItems() as $order) {
            if ($order->getOrderStatus() !== 'canceled') {
                return;
            }
        }

        $order = $this->orderRepository->get($orderId);
        $order->setState('canceled');
        $order->setStatus('canceled');
        $this->orderRepository->save($order);
    }
}
