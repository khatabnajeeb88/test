<?php
/**
 * Change/update/add email template variables which are not OOTB Webkul
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Plugin\Webkul\Marketplace\Helper;

use Magento\Backend\Model\UrlInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;
use Webkul\Marketplace\Model\OrdersFactory;
use Webkul\Marketplace\Helper\Email as WebkulEmail;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Api\StoreRepositoryInterface;

/**
 * Responsible for completing email template variables which are not provided by OOTB Webkul
 */
class Email
{
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var OrdersFactory
     */
    private $ordersFactory;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * Email constructor.
     *
     * @param UrlInterface $url
     * @param OrdersFactory $ordersFactory
     * @param Order $order
     * @param ProductRepositoryInterface $productRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        UrlInterface $url,
        OrdersFactory $ordersFactory,
        Order $order,
        ProductRepositoryInterface $productRepository,
        CustomerRepositoryInterface $customerRepository,
        StoreRepositoryInterface $storeRepository
    ) {
        $this->url = $url;
        $this->ordersFactory = $ordersFactory;
        $this->order = $order;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Plugin setting deny email template variables
     *
     * @param WebkulEmail $subject
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function beforeSendProductDenyMail(
        WebkulEmail $subject,
        array $emailTemplateVariables,
        array $senderInfo,
        array $receiverInfo
    ) {
        $emailTemplateVariables =
            $this->assignTemplateVariables($emailTemplateVariables, $emailTemplateVariables['product']);

        return [$emailTemplateVariables, $senderInfo, $receiverInfo];
    }

    /**
     * Plugin setting disapproval email template variables
     *
     * @param WebkulEmail $subject
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function beforeSendProductUnapproveMail(
        WebkulEmail $subject,
        array $emailTemplateVariables,
        array $senderInfo,
        array $receiverInfo
    ) {
        $product = $this->productRepository->get($emailTemplateVariables['sku']);
        $emailTemplateVariables = $this->assignTemplateVariables($emailTemplateVariables, $product);

        return [$emailTemplateVariables, $senderInfo, $receiverInfo];
    }

    /**
     * Plugin setting approval email template variables
     *
     * @param WebkulEmail $subject
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function beforeSendProductStatusMail(
        WebkulEmail $subject,
        array $emailTemplateVariables,
        array $senderInfo,
        array $receiverInfo
    ) {
        $product = $this->productRepository->get($emailTemplateVariables['sku']);
        $emailTemplateVariables = $this->assignTemplateVariables($emailTemplateVariables, $product);

        return [$emailTemplateVariables, $senderInfo, $receiverInfo];
    }

    /**
     * Method which is assigning necessary product properties to email template variables array
     *
     * @param array $emailTemplateVariables
     * @param ProductInterface $product
     *
     * @return array
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    private function assignTemplateVariables(
        array $emailTemplateVariables,
        ProductInterface $product
    ) {
        $merchantId = null;
        $storeId = null;
        $storeCode = '';
        $merchantIdAttribute = $product->getCustomAttribute('merchant_id');
        if ($merchantIdAttribute) {
            $merchantId = $merchantIdAttribute->getValue();
            $customer = $this->customerRepository->getById($merchantId);
            $storeId = $customer->getStoreId();
            $store = $this->storeRepository->getById($storeId);
            $storeCode = $store->getCode();
        }

        $emailTemplateVariables['product_status'] = 'Disabled';
        if ((int)$product->getStatus() === Status::STATUS_ENABLED) {
            $emailTemplateVariables['product_status'] = 'Enabled';
        }

        $emailTemplateVariables['email_content_alignment'] = 'left';
        $emailTemplateVariables['email_content_direction'] = 'ltr';
        if ($storeCode === 'ar_SA') {
            $emailTemplateVariables['email_content_alignment'] = 'right';
            $emailTemplateVariables['email_content_direction'] = 'rtl';
            $emailTemplateVariables['product_status'] =
                $this->translateProductStatusValue($emailTemplateVariables['product_status']);
        }

        $emailTemplateVariables['product_name'] = $product->getName();
        $emailTemplateVariables['product_sku'] = $product->getSku();

        $baseUrl = $this->url->getBaseUrl();
        $emailTemplateVariables['my_products_list_url'] = $baseUrl . 'marketplace/product/productlist/';

        return $emailTemplateVariables;
    }

    /**
     * @param string $status
     *
     * @return string
     */
    private function translateProductStatusValue(string $status)
    {
        switch ($status) {
            case 'Enabled':
                $status = 'تمكين';
                break;
            case 'Disabled':
                $status = 'معلق';
                break;
            default:
                $status = '';
                break;
        }

        return $status;
    }

    /**
     * Plugin setting order placed email template variables
     *
     * @param WebkulEmail $subject
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     *
     * @return array
     */
    public function beforeSendPlacedOrderEmail(
        WebkulEmail $subject,
        array $emailTemplateVariables,
        array $senderInfo,
        array $receiverInfo
    ) {
        $order = $this->order->loadByIncrementId($emailTemplateVariables['myvar1']);
        $orderId = $order->getId();

        $sellerOrders = $this->ordersFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            )
            ->addFieldToFilter(
                'seller_id',
                $emailTemplateVariables['seller_id']
            );

        $sellerOrderId = $sellerOrders->getFirstItem()->getData('order_id');

        $customer = $this->customerRepository->getById($emailTemplateVariables['seller_id']);
        $storeId = $customer->getStoreId();
        $store = $this->storeRepository->getById($storeId);

        $baseUrl = $this->url->getBaseUrl();
        $orderUrl = $baseUrl . 'marketplace/order/view/id/' . $sellerOrderId;
        $emailTemplateVariables['order_url'] = $orderUrl;
        $emailTemplateVariables['store_id'] = $storeId;
        $emailTemplateVariables['email_content_alignment'] = 'left';
        $emailTemplateVariables['email_content_direction'] = 'ltr';
        if ($store->getCode() === 'ar_SA') {
            $emailTemplateVariables['email_content_alignment'] = 'right';
            $emailTemplateVariables['email_content_direction'] = 'rtl';
        }

        return [$emailTemplateVariables, $senderInfo, $receiverInfo];
    }
}
