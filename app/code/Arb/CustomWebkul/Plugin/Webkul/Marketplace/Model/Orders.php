<?php
/**
 * Save Extended Data for Order Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Plugin\Webkul\Marketplace\Model;

use Arb\CustomWebkul\Api\OrderDetailsRepositoryInterface;
use Arb\CustomWebkul\Model\OrderDetailsFactory;
use Webkul\Marketplace\Model\Orders as WebkulOrders;

class Orders
{
    /**
     * @var OrderDetailsRepositoryInterface
     */
    private $orderDetailsRepository;

    /**
     * @var OrderDetailsFactory
     */
    private $orderDetailsFactory;

    /**
     * @param OrderDetailsRepositoryInterface $orderDetailsRepository
     * @param OrderDetailsFactory $orderDetailsFactory
     */
    public function __construct(
        OrderDetailsRepositoryInterface $orderDetailsRepository,
        OrderDetailsFactory $orderDetailsFactory
    ) {
        $this->orderDetailsRepository = $orderDetailsRepository;
        $this->orderDetailsFactory = $orderDetailsFactory;
    }

    /**
     * @param WebkulOrders $subject
     * @param WebkulOrders $result
     *
     * @return WebkulOrders
     */
    public function afterSave(WebkulOrders $subject, WebkulOrders $result)
    {
        $orderDetails = $this->orderDetailsRepository->getByOrderId($result->getId());

        if (!$orderDetails) {
            $orderDetails = $this->orderDetailsFactory->create();
            $orderDetails->setOrderId($result->getId());
        }

        if ($result->getIsApproved()) {
            $orderDetails->setIsApproved($result->getIsApproved());
        }

        if ($result->getCancelReason()) {
            $orderDetails->setCancelReason($result->getCancelReason());
        }

        $this->orderDetailsRepository->save($orderDetails);

        return $result;
    }

    /**
     * @param WebkulOrders $subject
     * @param WebkulOrders $result
     *
     * @return WebkulOrders
     */
    public function afterLoad(WebkulOrders $subject, WebkulOrders $result)
    {
        $orderDetails = $this->orderDetailsRepository->getByOrderId($result->getId());

        if ($orderDetails) {
            $result->setIsApproved($orderDetails->getIsApproved());
            $result->setCancelReason($orderDetails->getCancelReason());
        }

        return $result;
    }
}
