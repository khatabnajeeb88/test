/**
 * Overriding webkul template and js files
 *  
 * @category    Arb
 * @package     Arb_CustomWebkul
 * @author Arb Magento Team
 */
var config = {
    paths: {
        'Magento_Ui/templates/grid/cells/thumbnail/preview': 'Arb_CustomWebkul/templates/grid/cells/thumbnail/preview'
    },
    map: {
        '*': {
            sellerEditProduct:'Arb_CustomWebkul/js/product/seller-edit-product',
            WKreportsystem: 'Arb_CustomWebkul/js/WKreportsystem',
            sellerAddProduct : 'Arb_CustomWebkul/js/product/seller-add-product'
            }
        }
};