<?php
/**
 * This file consist of interface for Extended Order Details Repository
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api;

interface OrderDetailsRepositoryInterface
{
    /**
     * @param \Arb\CustomWebkul\Api\Data\OrderDetailsInterface $orderDetails
     *
     * @return \Arb\CustomWebkul\Api\Data\OrderDetailsInterface
     */
    public function save(\Arb\CustomWebkul\Api\Data\OrderDetailsInterface $orderDetails);

    /**
     * @param int $orderId
     *
     * @return \Arb\CustomWebkul\Api\Data\OrderDetailsInterface|null
     */
    public function getByOrderId(int $orderId);
}
