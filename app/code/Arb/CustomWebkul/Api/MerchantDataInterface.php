<?php
/**
 * This file consist of interface for merchant listing
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api;

/**
 * Product API interface
 */
interface MerchantDataInterface
{
  /**
   * function to get the merchant data by stores
   *
   * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
   * @return void
   */
    public function getMerchantList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
