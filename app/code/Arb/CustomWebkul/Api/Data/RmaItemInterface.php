<?php
/**
 * This file consist of interface for Extended Rma Item Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api\Data;

interface RmaItemInterface
{
    const RMA_ITEM_TABLE_NAME = 'arb_rma_item_details';
    const RMA_ITEM_ENTITY_ID = 'entity_id';
}
