<?php
/**
 * This file consist of interface for Extended Rma Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api\Data;

interface RmaDetailsInterface
{
    const RMA_TABLE_NAME = 'arb_rma_details';
    const RMA_ENTITY_ID = 'entity_id';
}
