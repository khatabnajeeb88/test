<?php
/**
 * This file consist of interface for Extended Order Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api\Data;

interface OrderDetailsInterface
{
    const ORDER_TABLE_NAME = 'arb_orders_details';
    const ORDER_ENTITY_ID = 'entity_id';
}
