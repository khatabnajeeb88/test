<?php
/**
 * This file consist of interface for Extended Rma Item Details Repository
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api;

interface RmaItemRepositoryInterface
{
    /**
     * @param \Arb\CustomWebkul\Api\Data\RmaItemInterface $rmaItem
     *
     * @return \Arb\CustomWebkul\Api\Data\RmaItemInterface
     */
    public function save(\Arb\CustomWebkul\Api\Data\RmaItemInterface $rmaItem);

    /**
     * @param int $rmaId
     *
     * @return \Arb\CustomWebkul\Api\Data\RmaItemInterface|null
     */
    public function getByRmaId(int $rmaId);
}
