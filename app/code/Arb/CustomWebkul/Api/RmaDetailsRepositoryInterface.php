<?php
/**
 * This file consist of interface for Extended Rma Details Repository
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api;

interface RmaDetailsRepositoryInterface
{
    /**
     * @param \Arb\CustomWebkul\Api\Data\RmaDetailsInterface $rmaData
     *
     * @return \Arb\CustomWebkul\Api\Data\RmaDetailsInterface
     */
    public function save(\Arb\CustomWebkul\Api\Data\RmaDetailsInterface $rmaData);

    /**
     * @param int $rmaId
     *
     * @return \Arb\CustomWebkul\Api\Data\RmaDetailsInterface|null
     */
    public function getByRmaId(int $rmaId);
}
