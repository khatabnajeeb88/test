<?php
/**
 * This file consist of interface for email template repository
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Api;

use Magento\Email\Model\Template;
use Magento\Framework\Exception\AlreadyExistsException;

/**
 * interface for saving and loading email templates
 */
interface EmailTemplateRepositoryInterface
{
    /**
     * @param Template $template
     *
     * @return Template
     *
     * @throws AlreadyExistsException
     */
    public function save(Template $template);

    /**
     * @param string $templateCode
     *
     * @return Template
     */
    public function load(string $templateCode);

    /**
     * @param string $templateCode
     *
     * @return Template
     */
    public function getByTemplateCode(string $templateCode);
}
