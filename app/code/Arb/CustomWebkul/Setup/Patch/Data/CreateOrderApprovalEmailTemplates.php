<?php
/**
 * This file consist of data patch for order approval email template
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Arb\CustomWebkul\Helper\EmailTemplate;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Email\Model\TemplateFactory;

/**
 * Creating email templates and assigning to proper configuration
 */
class CreateOrderApprovalEmailTemplates implements DataPatchInterface
{
    /**
     * Order approve email id for English
     */
    private const ORDER_APPROVE_EMAIL_ID = 'marketplace_email_order_approve_template';

    /**
     * Order approve email id for Arabic
     */
    private const ORDER_APPROVE_EMAIL_ID_AR = 'marketplace_email_order_approve_template_ar';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EmailTemplate
     */
    private $emailTemplate;

    /**
     * CreateEmailTemplates constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EmailTemplate $emailTemplate
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EmailTemplate $emailTemplate
    ) {
        $this->moduleDataSetup = $moduleDataSetup;

        $this->emailTemplate = $emailTemplate;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @throws LocalizedException
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->emailTemplate->createEmailTemplate(self::ORDER_APPROVE_EMAIL_ID, null, 'en');
        $this->emailTemplate->createEmailTemplate(self::ORDER_APPROVE_EMAIL_ID_AR, null, 'ar_SA');

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
