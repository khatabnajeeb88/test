<?php

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UpdateAllowedProductType implements DataPatchInterface
{
    private const MARKETPLACE_SHIPPING_ALLOWED_PRODUCT_PATH = 'marketplace/product_settings/allow_for_seller';
    private const MARKETPLACE_SHIPPING_ALLOWED_PRODUCT_TYPE = 'simple,downloadable,configurable';
   
     /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::MARKETPLACE_SHIPPING_ALLOWED_PRODUCT_PATH, self::MARKETPLACE_SHIPPING_ALLOWED_PRODUCT_TYPE);
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param string|int $value
     *
     * @return void
     */
    private function updateConfiguration(string $path, $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
