<?php

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class SetSellerConsents implements DataPatchInterface
{
    private const MARKETPLACE_SELLER_RELATED_PRODUCTS_PATH = 'marketplace/product_settings/allow_related_product';
    private const MARKETPLACE_SELLER_CROSS_SELL_PATH = 'marketplace/product_settings/allow_crosssell_product';
    private const MARKETPLACE_SELLER_UP_SELL_PATH = 'marketplace/product_settings/allow_upsell_product';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * SetSellerConsents constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::MARKETPLACE_SELLER_CROSS_SELL_PATH, 0);
        $this->updateConfiguration(self::MARKETPLACE_SELLER_UP_SELL_PATH, 0);
        $this->updateConfiguration(self::MARKETPLACE_SELLER_RELATED_PRODUCTS_PATH, 0);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param $path
     * @param $value
     */
    private function updateConfiguration($path, $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
