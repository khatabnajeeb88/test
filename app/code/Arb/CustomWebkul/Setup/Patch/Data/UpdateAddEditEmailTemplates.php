<?php

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpdateAddEditEmailTemplates implements DataPatchInterface
{
    private const MARKETPLACE_ADD_EMAIL_PATH = 'marketplace/email/new_product_notification_template';
    private const CUSTOM_ADD_TEMPLATE_ID = 'custom_marketplace_email_new_product_notification_template';
    private const MARKETPLACE_EDIT_EMAIL_PATH = 'marketplace/email/edit_product_notification_template';
    private const CUSTOM_EDIT_TEMPLATE_ID = 'custom_marketplace_email_edit_product_notification_template';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * UpdateAddEditEmailTemplates constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::MARKETPLACE_ADD_EMAIL_PATH, self::CUSTOM_ADD_TEMPLATE_ID);
        $this->updateConfiguration(self::MARKETPLACE_EDIT_EMAIL_PATH, self::CUSTOM_EDIT_TEMPLATE_ID);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param string $value
     */
    private function updateConfiguration(string $path, string $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
