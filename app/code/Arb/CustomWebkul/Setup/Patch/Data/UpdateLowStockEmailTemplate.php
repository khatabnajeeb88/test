<?php
/**
 * This file consist of data patch for email templates
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Arb\CustomWebkul\Helper\EmailTemplate;

/**
 * Update low Stock email template and assigning to proper configuration
 */
class UpdateLowStockEmailTemplate implements DataPatchInterface
{
    private const MARKETPLACE_LOW_STOCK_EMAIL_PATH = 'marketplace/email/low_stock_template';
    private const CUSTOM_LOW_STOCK_TEMPLATE_ID = 'custom_marketplace_email_low_stock_template';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EmailTemplate
     */
    private $emailTemplate;

    /**
     * CreateLowStockEmailTemplate constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EmailTemplate $emailTemplate
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EmailTemplate $emailTemplate
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->emailTemplate = $emailTemplate;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [CreateLowStockEmailTemplate::class];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->emailTemplate->createEmailTemplate(
            self::CUSTOM_LOW_STOCK_TEMPLATE_ID,
            self::MARKETPLACE_LOW_STOCK_EMAIL_PATH
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
