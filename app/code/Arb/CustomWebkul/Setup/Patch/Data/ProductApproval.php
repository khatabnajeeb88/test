<?php

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class ProductApproval implements DataPatchInterface
{
    private const MARKETPLACE_PRODUCT_APPROVAL_PATH = 'marketplace/product_settings/product_approval';
    private const MARKETPLACE_PRODUCT_EDIT_APPROVAL_PATH = 'marketplace/product_settings/product_edit_approval';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * ProductApproval constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::MARKETPLACE_PRODUCT_APPROVAL_PATH, 1);
        $this->updateConfiguration(self::MARKETPLACE_PRODUCT_EDIT_APPROVAL_PATH, 1);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param int $value
     */
    private function updateConfiguration(string $path, int $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
