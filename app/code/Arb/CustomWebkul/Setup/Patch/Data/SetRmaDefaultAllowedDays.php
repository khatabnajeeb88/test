<?php

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class SetRmaDefaultAllowedDays implements DataPatchInterface
{
    private const MARKETPLACE_RMA_DEFAULT_ALLOWED_DAYS_PATH = 'mprmasystem/settings/default_days';
    private const MARKETPLACE_RMA_DEFAULT_ALLOWED_DAYS_VALUE = 14;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * SetSellerConsents constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(
            self::MARKETPLACE_RMA_DEFAULT_ALLOWED_DAYS_PATH,
            self::MARKETPLACE_RMA_DEFAULT_ALLOWED_DAYS_VALUE
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param int $value
     */
    private function updateConfiguration(string $path, int $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
