<?php

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Webkul\MpRmaSystem\Model\ReasonsFactory;

class SetRmaReturnReasons implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var ReasonsFactory
     */
    private $reasonsFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ReasonsFactory $reasonsFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ReasonsFactory $reasonsFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->reasonsFactory = $reasonsFactory;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $reasonDataArray = [[
            'reason' => 'Product Missing warranty.',
            'status' => 1
        ], [
            'reason' => 'Product Does Not Match Description.',
            'status' => 1
        ], [
            'reason' => 'Product No Longer Needed.',
            'status' => 1
        ], [
            'reason' => 'Incorrect Product or Size Ordered.',
            'status' => 1
        ]];

        foreach ($reasonDataArray as $reasonData) {
            $reason = $this->reasonsFactory->create();
            $reason->setData($reasonData)->save();
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
