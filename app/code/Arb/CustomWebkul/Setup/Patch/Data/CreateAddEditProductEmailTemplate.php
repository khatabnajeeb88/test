<?php
/**
 * This file consist of data patch for email templates (add / edit product)
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Arb\CustomWebkul\Helper\EmailTemplate;

/**
 * Creating product added/edited email template and assigning to proper configuration
 */
class CreateAddEditProductEmailTemplate implements DataPatchInterface
{
    private const EMAILS_ARRAY = [
        'custom_marketplace_email_new_product_notification_template' =>
            'marketplace/email/new_product_notification_template',
        'custom_marketplace_email_edit_product_notification_template' =>
            'marketplace/email/edit_product_notification_template',
    ];

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EmailTemplate
     */
    private $emailTemplate;

    /**
     * CreateOrderPlacedEmailTemplate constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EmailTemplate $emailTemplate
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EmailTemplate $emailTemplate
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->emailTemplate = $emailTemplate;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [UpdateAddEditEmailTemplates::class];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     * @throws AlreadyExistsException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        foreach (self::EMAILS_ARRAY as $code => $path) {
            $this->emailTemplate->createEmailTemplate($code, $path);
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
