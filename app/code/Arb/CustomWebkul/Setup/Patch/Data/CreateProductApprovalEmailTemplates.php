<?php
/**
 * This file consist of data patch for email templates
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Setup\Patch\Data;

use Arb\CustomWebkul\Helper\EmailTemplate;
use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Email\Model\TemplateFactory;
use Arb\CustomWebkul\Api\EmailTemplateRepositoryInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\State;
use Magento\Framework\App\Cache\TypeListInterface;

/**
 * Creating email templates and assigning to proper configuration
 */
class CreateProductApprovalEmailTemplates implements DataPatchInterface
{
    private const EMAILS_ARRAY = [
        'custom_marketplace_email_product_deny_notification_template' =>
            'marketplace/email/product_deny_notification_template',
        'custom_marketplace_email_product_approve_notification_template' =>
            'marketplace/email/product_approve_notification_template',
        'custom_marketplace_email_product_disapprove_notification_template' =>
            'marketplace/email/product_disapprove_notification_template'
    ];

    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var EmailTemplateRepositoryInterface
     */
    protected $emailTemplateRepository;

    /**
     * @var WriterInterface
     */
    protected $writer;

    /**
     * @var State
     */
    protected $state;

    /**
     * @var TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * @var EmailTemplate
     */
    private $emailTemplate;

    /**
     * CreateEmailTemplates constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param StoreManagerInterface $storeManager
     * @param EmailTemplateRepositoryInterface $emailTemplateRepository
     * @param WriterInterface $writer
     * @param State $state
     * @param TypeListInterface $cacheTypeList
     * @param EmailTemplate $emailTemplate
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        StoreManagerInterface $storeManager,
        EmailTemplateRepositoryInterface $emailTemplateRepository,
        WriterInterface $writer,
        State $state,
        TypeListInterface $cacheTypeList,
        EmailTemplate $emailTemplate
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->storeManager = $storeManager;
        $this->emailTemplateRepository = $emailTemplateRepository;
        $this->writer = $writer;
        $this->state = $state;
        $this->cacheTypeList = $cacheTypeList;
        $this->emailTemplate = $emailTemplate;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @throws LocalizedException
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        // phpcs:disable Generic.CodeAnalysis.EmptyStatement, Magento2.CodeAnalysis.EmptyBlock.DetectedCatch
        try {
            $this->state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (LocalizedException $e) {
            // Intentionally nothing happens here.
        }
        // phpcs:enable Generic.CodeAnalysis.EmptyStatement, Magento2.CodeAnalysis.EmptyBlock.DetectedCatch

        foreach (self::EMAILS_ARRAY as $code => $path) {
            $this->emailTemplate->createEmailTemplate($code, $path);
        }

        $this->moduleDataSetup->getConnection()->endSetup();

        $this->cacheTypeList->cleanType('config');
    }
}
