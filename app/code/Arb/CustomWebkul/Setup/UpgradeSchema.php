<?php
/**
 * RMA table installer
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * This class upgrade Rma Reasons database schema class
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        if (version_compare($context->getVersion(), '1.0.6', '<=')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('marketplace_rma_reasons'),
                'store_id',
                [
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Increment Store Id'
                ]
            );
        }
        
        $setup->endSetup();
    }
}
