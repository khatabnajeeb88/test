<?php
/**
 * This file consist of class Merchant which is used to define listing for merchant.
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Ui;

class MassAction extends \Magento\Ui\Component\MassAction
{
    /**
     * Return Empty Mass Action Dropdown
     */
    public function prepare()
    {
        parent::prepare();
        $this->setData('config', []);
    }
}
