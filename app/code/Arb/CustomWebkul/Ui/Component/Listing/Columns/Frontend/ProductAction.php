<?php
/**
 * Product listing - override to hide edit button for virtuals
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ProductAction.
 */
class ProductAction extends \Webkul\Marketplace\Ui\Component\Listing\Columns\Frontend\ProductAction
{
    /**
     * Config path to allow selected product type to edit
     */
    const MARKETPLACE_ALLOWED_PRODUCT_PATH = 'marketplace/product_settings/allow_for_seller';
    const ALLOWED_PRODUCT_TYPES = 'simple,virtual,downloadable,configurable';

    /**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
   protected $scopeConfig;
   

    /**
     * Product listing UI component
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param ScopeConfigInterface $scopeConfig
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        ScopeConfigInterface $scopeConfig,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory,$urlBuilder, $components, $data);
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $allowedProductType = self::ALLOWED_PRODUCT_TYPES;
        $allowedProductType = explode(',',$allowedProductType);
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['entity_id']) && in_array($item['type_id'],$allowedProductType)) {
                    $item[$fieldName.'_html'] = '<div class="wk-row-action-icons"><span class="wk-action-wrapper"><span title="'.__('Edit').'" class="mp-edit" data-url="'.$this->urlBuilder->getUrl('marketplace/product/edit/', ['id'=>$item['entity_id']]).'"></span></span></div>';
                }
            }
        }

        return $dataSource;
    }
}
