<?php
/**
 * Product listing - override to hide edit button for virtuals
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Catalog\Model\ProductFactory;
use Webkul\Marketplace\Ui\Component\Listing\Columns\Frontend\Thumbnail as ParentThumbnail;

class Thumbnail extends ParentThumbnail
{
    const NAME = 'thumbnail';

    const ALT_FIELD = 'name';

    const ALLOWED_PRODUCT_TYPES = 'simple,downloadable,configurable';

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $imageHelper;

    /**
     * UI Thumbnail constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param ProductFactory $productModel
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\UrlInterface $urlBuilder,
        ProductFactory $productModel = null,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $imageHelper,$urlBuilder,$productModel, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $allowedProductType = self::ALLOWED_PRODUCT_TYPES;
        $allowedProductType = explode(',',$allowedProductType);
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $productData = new \Magento\Framework\DataObject($item);
                $product = $this->productModel->create()->load($productData->getEntityId());
                $imageHelper = $this->imageHelper->init($product, 'product_thumbnail_image');
                $item[$fieldName . '_src'] = $imageHelper->getUrl();
                $item[$fieldName . '_alt'] = $this->getAlt($item) ?: $imageHelper->getLabel();
                if (in_array($item['type_id'],$allowedProductType)) {
                    $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                        'marketplace/product/edit',
                        ['id' => $product->getEntityId()]
                    );
                }else{
                    $item[$fieldName . '_linktext'] = '';
                }
                $origImageHelper = $this->imageHelper->init($product, 'product_base_image');
                $item[$fieldName . '_orig_src'] = $origImageHelper->getUrl();
            }
        }
        return $dataSource;
    }

    /**
     * @param array $row
     *
     * @return null|string
     */
    protected function getAlt($row)
    {
        $altField = $this->getData('config/altField') ?: self::ALT_FIELD;

        return isset($row[$altField]) ? $row[$altField] : null;
    }
}
