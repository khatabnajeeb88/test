<?php
namespace Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Sales\Model\OrderRepository;
use Webkul\Marketplace\Ui\Component\Listing\Columns\Frontend\OrderHistoryProDetails as WebkulOrderHistoryProDetails;

class OrderHistoryProDetails extends WebkulOrderHistoryProDetails
{

    public function getpronamebyorder($orderId, $sellerId)
    {
        $collection = $this->_collectionFactory->create()
        ->addFieldToFilter(
            'seller_id',
            $sellerId
        )
        ->addFieldToFilter(
            'order_id',
            $orderId
        );
        $productName = '';
        foreach ($collection as $res) {
            if ($res->getParentItemId()) {
                continue;
            }
            $item = $this->orderItemRepository->get($res->getOrderItemId());
            $url = '';
            // Updated product name
            $result = [];
            if ($options = $item['product_options']) {
                if (isset($options['options'])) {
                    $result = array_merge($result, $options['options']);
                }
                if (isset($options['additional_options'])) {
                    $result = array_merge($result, $options['additional_options']);
                }
                if (isset($options['attributes_info'])) {
                    $result = array_merge($result, $options['attributes_info']);
                }
            }
            $productName = $productName.$item['name'];
            $productName = $this->getProductNameHtml($result, $productName);
            /*prepare product quantity status*/
            $isForItemPay = 0;
            if ($item['qty_ordered'] > 0) {
                $productName = $productName.__('Ordered').
                ': <strong>'.($item['qty_ordered'] * 1).'</strong><br />';
            }
            if ($item['qty_invoiced'] > 0) {
                ++$isForItemPay;
                $productName = $productName.
                __('Invoiced').
                ': <strong>'.
                ($item['qty_invoiced'] * 1).
                '</strong><br />';
            }
            if ($item['qty_shipped'] > 0) {
                ++$isForItemPay;
                $productName = $productName.__('Shipped').
                ': <strong>'.($item['qty_shipped'] * 1).'</strong><br />';
            }
            if ($item['qty_canceled'] > 0) {
                $isForItemPay = 4;
                $productName = $productName.
                __('Canceled').
                ': <strong>'.
                ($item['qty_canceled'] * 1).
                '</strong><br />';
            }
            if ($item['qty_refunded'] > 0) {
                $isForItemPay = 3;
                $productName = $productName.
                __('Refunded').
                ': <strong>'.
                ($item['qty_refunded'] * 1).
                '</strong><br />';
            }
        }

        return $productName;
    }
}
