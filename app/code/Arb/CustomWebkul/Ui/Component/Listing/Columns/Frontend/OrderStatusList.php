<?php
/**
 * Order listing - override status column
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend;



use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Webkul\Marketplace\Helper\Orders as WebkulOrderHelper;
/**
 * Class OrderStatus.
 */
class OrderStatusList extends Column
{
    protected $webkulOrderHelper;

    /**
     * 
     * @param ContextInterface   $context           
     * @param UiComponentFactory $uiComponentFactory   
     * @param array              $components        
     * @param array              $data              
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        WebkulOrderHelper $webkulOrderHelper,
        array $components = [],
        array $data = []
    ) {
        $this->webkulOrderHelper = $webkulOrderHelper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
             $fieldName = $this->getData('name'); 
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['order_id'])) {
                    $status = $item[$fieldName];
                    $tracking = $this->webkulOrderHelper->getOrderinfo($item['order_id']);
                    if ($tracking->getIsCanceled()) {
                        $status = 'canceled';
                    } else if ($tracking->getShipmentId() && $item[$fieldName] !== 'complete') {
                        $status = 'shipped';
                    }

                   $item[$fieldName] = $status;
                }
            }
        }

        return $dataSource;
    }
}
