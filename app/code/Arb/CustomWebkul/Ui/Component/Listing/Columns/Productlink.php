<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Arb\CustomWebkul\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Productlink extends \Webkul\Marketplace\Ui\Component\Listing\Columns\Productlink
{
     /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['mageproduct_id'])) {
                    $item[$fieldName] = "<a href='".$this->urlBuilder->getUrl('catalog/product/edit', ['id' => $item['mageproduct_id']])."' target='blank' title='".__('View Product')."'>View</a>";
                }
            }
        }

        return $dataSource;
    }
}
