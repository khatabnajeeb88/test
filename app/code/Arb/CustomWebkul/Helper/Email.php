<?php
/**
 * Extending Webkul class that is responsible for sending emails
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Helper;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\Store;
use Webkul\Marketplace\Helper\Email as WebkulEmail;

/**
 * Sending emails
 */
class Email extends WebkulEmail
{
    /**
     * Order cancel email template id
     */
    private const ORDER_APPROVE_EMAIL_TEMPLATE = 'marketplace_email_order_approve_template';

    /**
     * Order cancel email template id
     */
    private const ORDER_CANCEL_EMAIL_TEMPLATE = 'marketplace_email_order_cancel_template';

    /**
     * Order reject email template id
     */
    private const ORDER_REJECT_EMAIL_TEMPLATE = 'marketplace_email_order_reject_template';

    /**
     * order complete email template id
     */
    private const ORDER_COMPLETE_EMAIL_TEMPLATE = 'marketplace_email_order_complete_template';

    /**
     * low stock email template id
     */
    const LOW_STOCK_EMAIL_TEMPLATE = 'custom_marketplace_email_low_stock_template';

    /**
     * Sending order approve email
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendOrderApproveMail(array $emailTemplateVariables, array $senderInfo, array $receiverInfo)
    {
        $this->_template = self::ORDER_APPROVE_EMAIL_TEMPLATE;
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * Sending order cancellation email
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendOrderCancelMail(array $emailTemplateVariables, array $senderInfo, array $receiverInfo)
    {
        $this->_template = self::ORDER_CANCEL_EMAIL_TEMPLATE;
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * Sending order rejection email
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendOrderRejectMail(array $emailTemplateVariables, array $senderInfo, array $receiverInfo)
    {
        $this->_template = self::ORDER_REJECT_EMAIL_TEMPLATE;
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * Sending order complete email
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendOrderCompleteMail(array $emailTemplateVariables, array $senderInfo, array $receiverInfo)
    {
        $this->_template = self::ORDER_COMPLETE_EMAIL_TEMPLATE;
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * sends the email to seller when product reached to its low stock
     *
     * @param Mixed $emailTemplateVariables
     * @param Mixed $senderInfo
     * @param Mixed $receiverInfo
     */
    public function sendLowStockNotificationMail($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->_template = self::LOW_STOCK_EMAIL_TEMPLATE;
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * @param $emailTemplateVariables
     * @param $senderInfo
     * @param $receiverInfo
     */
    public function sendRmaNotificationMail($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->_template = $emailTemplateVariables['template'];
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            if (isset($emailTemplateVariables['confirmation_email'])) {
                $this->_messageManager->addSuccessMessage('Confirmation email sent to customer.');
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }


    /**
     * @param mixed $emailTemplateVariables
     * @param mixed $senderInfo
     * @param mixed $receiverInfo
     *
     * @return $this|WebkulEmail
     */
    public function generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $senderEmail = $senderInfo['email'];
        $adminEmail = $this->getConfigValue(
            'trans_email/ident_general/email',
            $this->getStore()->getStoreId()
        );

        $storeId = $emailTemplateVariables['store_id'] ?? 1;

        $senderInfo['email'] = $adminEmail;
        $template = $this->_transportBuilder->setTemplateIdentifier($this->_template)
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $storeId,
                ]
            )
            ->setTemplateVars($emailTemplateVariables)
            ->setFrom($senderInfo)
            ->addTo($receiverInfo['email'], $receiverInfo['name'])
            ->setReplyTo($senderEmail, $senderInfo['name']);
        return $this;
    }
}
