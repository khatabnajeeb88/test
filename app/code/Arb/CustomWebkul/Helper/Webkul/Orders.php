<?php
/**
 * Overridden Helper file
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Helper\Webkul;

use Magento\Catalog\Model\Product\Type;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Sales\Model\Order\Status as OrderStatus;
use Webkul\Marketplace\Helper\Orders as ParentOrders;
use Webkul\Marketplace\Helper\Email;
use Webkul\Marketplace\Helper\Notification;
use Webkul\Marketplace\Api\Data\OrdersInterface;
use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\Marketplace\Model\FeedbackcountFactory;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersFactory;
use Webkul\Marketplace\Model\SaleperpartnerFactory;
use Webkul\Marketplace\Model\Saleslist;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Model\SellertransactionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Framework\Unserialize\Unserialize;
use Magento\Framework\Json\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Arb\Creditmemo\Helper\Data as ArbCreditMemoHelper;
use Webkul\Marketplace\Model\OrdersRepository;

/**
 * Helper class containing methods used in template
 */
class Orders extends ParentOrders
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ArbCreditMemoHelper
     */
    protected $arbCreditMemoHelper;
    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param DateTime $date
     * @param Session $customerSession
     * @param Data $jsonHelper
     * @param Unserialize $unserializer
     * @param ItemRepository $orderItemRepository
     * @param OrderStatus $orderStatus
     * @param MpOrdersFactory $mpOrdersFactory
     * @param MpHelper $mpHelper
     * @param SaleslistFactory $saleslistFactory
     * @param SaleperpartnerFactory $saleperpartnerFactory
     * @param SellertransactionFactory $sellertransactionFactory
     * @param FeedbackcountFactory $feedbackcountFactory
     * @param Email $mpEmailHelper
     * @param Notification $notificationHelper
     * @param CustomerFactory $customerModel
     * @param OrderFactory $orderModel
     * @param ProductRepositoryInterface $productRepository
     * @param ArbCreditMemoHelper $arbCreditMemoHelper
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        DateTime $date,
        Session $customerSession,
        Data $jsonHelper,
        Unserialize $unserializer,
        ItemRepository $orderItemRepository,
        OrderStatus $orderStatus = null,
        MpOrdersFactory $mpOrdersFactory = null,
        MpHelper $mpHelper = null,
        SaleslistFactory $saleslistFactory = null,
        SaleperpartnerFactory $saleperpartnerFactory = null,
        SellertransactionFactory $sellertransactionFactory = null,
        FeedbackcountFactory $feedbackcountFactory = null,
        Email $mpEmailHelper = null,
        Notification $notificationHelper = null,
        CustomerFactory $customerModel = null,
        OrderFactory $orderModel = null,
        ProductRepositoryInterface $productRepository,
        ArbCreditMemoHelper $arbCreditMemoHelper,
        OrdersRepository $ordersRepository
    ) {
        parent::__construct(
            $context,
            $objectManager,
            $date,
            $customerSession,
            $jsonHelper,
            $unserializer,
            $orderItemRepository,
            $orderStatus,
            $mpOrdersFactory,
            $mpHelper,
            $saleslistFactory,
            $saleperpartnerFactory,
            $sellertransactionFactory,
            $feedbackcountFactory,
            $mpEmailHelper,
            $notificationHelper,
            $customerModel,
            $orderModel
        );

        $this->productRepository = $productRepository;
        $this->arbCreditMemoHelper = $arbCreditMemoHelper;
        $this->ordersRepository = $ordersRepository;
    }


    /**
     * Check if Merchant Consignment contains any Physical Products
     *
     * @param int $orderId
     *
     * @return bool
     *
     * @throws NoSuchEntityException
     */
    public function getIsPhysicalOrder(int $orderId)
    {
        $sellerId = $this->_customerSession->getCustomerId();
        $order = $this->mpOrdersFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )->addFieldToFilter(
                'order_id',
                $orderId
            )->getFirstItem();

        if (!$order) {
            return false;
        }

        $salesCollection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $orderId
            );

        foreach ($salesCollection as $sale) {
            try {
                $product = $this->productRepository->getById($sale->getData('mageproduct_id'));
            } catch (NoSuchEntityException $e) {
                continue;
            }

            if (($product->getTypeId() === Configurable::TYPE_CODE) && $this->isConfigurablePhysical($orderId, $sale)) {
                return true;
            }

            if ($product->getTypeId() === Type::TYPE_SIMPLE) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int $orderId
     * @param Saleslist $sale
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isConfigurablePhysical(int $orderId, Saleslist $sale)
    {
        $orderItemId = $sale->getData('order_item_id');
        $collection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.order_id',
                $orderId
            )->addFieldToFilter(
                'main_table.parent_item_id',
                $orderItemId
            );

        if ($collection->getSize() > 0) {
            foreach ($collection as $configuration) {
                $product = $this->productRepository->getById($configuration->getData('mageproduct_id'));
                if ($product->getTypeId() === Type::TYPE_SIMPLE) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Override original method to remove order_approval filter from select
     *
     * @param string $orderId
     *
     * @return OrdersInterface|bool
     */
    public function getOrderinfo($orderId = '')
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $model = $this->mpOrdersFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'seller_id',
                $this->_customerSession->getCustomerId()
            )
            ->addFieldToFilter(
                'order_id',
                $orderId
            );
        $salesOrder = $this->mpOrdersFactory->create()->getCollection()->getTable('sales_order');

        $model->getSelect()->join(
            $salesOrder . ' as so',
            'main_table.order_id = so.entity_id',
            ["order_approval_status" => "order_approval_status"]
        );
        foreach ($model as $tracking) {
            $webkuLOrder = $this->ordersRepository->getById($tracking->getEntityId());
            return $webkuLOrder;
        }

        return false;
        //@codeCoverageIgnoreEnd
    }

    /**
     * Override original method to remove order_approval filter from collection
     *
     * @param $sellerId
     *
     * @return int
     */
    public function getSellerOrders($sellerId)
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        try {
            $collection = $this->mpOrdersFactory->create()
                ->getCollection()
                ->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
            $salesOrder = $collection->getTable('sales_order');
            $collection->getSelect()->join(
                $salesOrder . ' as so',
                'main_table.order_id = so.entity_id',
                ["order_approval_status" => "order_approval_status"]
            );
            return count($collection);
        } catch (Exception $e) {
            return 0;
        }
        //@codeCoverageIgnoreEnd
    }

    /**
     * Check if credit memo allowed
     *
     * @param \Magento\Sales\Model\Order $order
     * @return boolean
     */
    public function isAllowedCreditMemo($order){
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        return $this->arbCreditMemoHelper->isCreditMemoAllowed($order);
        //@codeCoverageIgnoreEnd
    }
    
    /*
     * Get Order Product Option Data Method.
     *
     * @param \Webkul\Marketplace\Model\Saleslist $res
     *
     * @return array
     */
    public function getOrderedProductName($res, $productName, $wrap = false)
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $item = $this->orderItemRepository->get($res->getOrderItemId());
        $url = '';
        // Updated product name
        $result = [];
        $result = $this->getProductOptionData($item, $result);
        //Remove Link from Product on Dashboard
        $productName = $productName.$item['name'];
        $productName = $this->getProductNameHtml($result, $productName, $wrap);
        /*prepare product quantity status*/
        $isForItemPay = 0;
        if ($item['qty_ordered'] > 0) {
            $content = __('Ordered').': <strong>'.($item['qty_ordered'] * 1).'</strong>';
            $content = $this->getWrappedHtml($content, $wrap);
            $productName = $productName.$content;
        }
        if ($item['qty_invoiced'] > 0) {
            ++$isForItemPay;
            $content = __('Invoiced').': <strong>'.($item['qty_invoiced'] * 1).'</strong>';
            $content = $this->getWrappedHtml($content, $wrap);
            $productName = $productName.$content;
        }
        if ($item['qty_shipped'] > 0) {
            ++$isForItemPay;
            $content = __('Shipped').': <strong>'.($item['qty_shipped'] * 1).'</strong>';
            $content = $this->getWrappedHtml($content, $wrap);
            $productName = $productName.$content;
        }
        if ($item['qty_canceled'] > 0) {
            $isForItemPay = 4;
            $content = __('Canceled').': <strong>'.($item['qty_canceled'] * 1).'</strong>';
            $content = $this->getWrappedHtml($content, $wrap);
            $productName = $productName.$content;
        }
        if ($item['qty_refunded'] > 0) {
            $isForItemPay = 3;
            $content = __('Refunded').': <strong>'.($item['qty_refunded'] * 1).'</strong>';
            $content = $this->getWrappedHtml($content, $wrap);
            $productName = $productName.$content;
        }
        return $productName;
        //@codeCoverageIgnoreEnd
    }

    /**
     * @param string $order
     * @param string $sellerId
     * @param string $comment
     *
     * @return bool
     */
    public function mpregisterCancellation($order, $sellerId, $comment = '')
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $flag = 0;
        if ($order->canCancel()) {
            $cancelState = 'canceled';
            $items = [];
            $orderId = $order->getId();
            $trackingsdata = $this->mpOrdersFactory->create()
                ->getCollection()
                ->addFieldToFilter(
                    'order_id',
                    $orderId
                )
                ->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
            foreach ($trackingsdata as $tracking) {
                $items = explode(',', $tracking->getProductIds());
                foreach ($order->getAllItems() as $item) {
                    if ($item->getProductType() == 'virtual') {
                        continue;
                    }

                    if (in_array($item->getProductId(), $items)) {
                        $flag = 1;
                        $item->cancel();
                    }

                    if ($cancelState != 'processing' && $item->getQtyToRefund()) {
                        if ($item->getQtyToShip() > $item->getQtyToCancel()) {
                            $cancelState = 'processing';
                        } else {
                            $cancelState = 'complete';
                        }
                    } elseif ($item->getQtyToInvoice()) {
                        $cancelState = 'processing';
                    }
                }
                $order->setState($cancelState, true, $comment)
                    ->setStatus($cancelState)
                    ->save();
            }
        }

        return $flag;
        //@codeCoverageIgnoreEnd
    }
}
