<?php
/**
 * Customize Webkul massupload methods
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Helper\MpMassUpload;

use Webkul\MpMassUpload\Helper\Data as ParentHelper;

class Data extends ParentHelper
{

    /**
     * checks whether the csv sheet
     * contains the column for
     * required type custom attribute
     *
     * @param array $csvAttributeList
     * @param array $wholeData
     * @return array
     */
    public function validateCsvForRequiredCustomAttributes($csvAttributeList, $wholeData)
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        $result = ['error' => 0];
        $customAttributeList = $this->getCustomAttributeList();
        $attributeSetId = $wholeData['set'];//your_attributeSetId
        $productAttributesManagement = $this->_objectManager->create('Magento\Catalog\Api\ProductAttributeManagementInterface');
        $productAttributes = $productAttributesManagement->getAttributes($attributeSetId);
        $attributesForSet = [];
        foreach($productAttributes as $data) {
            $attributesForSet[] = $data->getAttributeId();
        }
        foreach ($customAttributeList as $code => $value) {
            $attributeId = $value;
            $attribute = $this->getAttributeDataById($attributeId);
            if ($attribute['is_required'] == 1 && in_array($attributeId, $attributesForSet)) {
                if (!array_key_exists($attribute['attribute_code'], $csvAttributeList)) {
                    $wholeData['error'] = 1;
                    $wholeData['msg'] = __(
                        'Required "%1" Attribute column is missing in sheet',
                        $attribute['attribute_code']
                    );
                }
            }
        }
        return $wholeData;
        // @codeCoverageIgnoreEnd
    }

    /**
     * check attribute data
     *
     * @param string $attribute
     * @param string $code
     * @param string $value
     * @param array $notAllowedAttr
     * @param array $wholeData
     * @return void
     */
    public function checkAttributeData($attribute, $code, $value, $notAllowedAttr, $wholeData, $row)
    {
        // skip code coverage as this is webkul method with adjustments
        //@codeCoverageIgnoreStart
        if ($this->isAttributeAllowed($attribute) && !in_array($code, $notAllowedAttr)) {
            if ($code == "tier_price") {
                $value = $this->processTierPrice($value);
                if (!empty($value)) {
                    foreach ($value as $key => $vl) {
                        if (empty($vl['website_id'])) {
                            $value[$key]['website_id'] = 0;
                        }
                    }
                    $wholeData['product'][$code] = $value;
                }
            } else {
                $wholeData = $this->isRequiredAttributeEmpty($attribute, $value, $wholeData, $row);
                if (!isset($wholeData['error'])) {
                    if ((($attribute["frontend_input"] == "multiselect") ||
                        ($attribute["frontend_input"] == "select"))
                        ) {
                        $valueArray = explode(",", $value);
                        $optionId = $this->getOptionIdByLabel($code, $valueArray);
                        if($attribute["frontend_input"] == "select" && in_array($attribute["backend_type"], array("varchar", "int"))) {
                            $optionId = $optionId[0];
                        }
                    } else {
                        if ($attribute["frontend_input"] == "boolean" && (strcasecmp($value, 'yes') == 0)) {
                            $value = 1;
                        } elseif ($attribute["frontend_input"] == "boolean" && (strcasecmp($value, 'no') == 0)) {
                            $value = 0;
                        }
                        $optionId = $value;
                    }
                    $wholeData['product'][$code] = $optionId;
                }
            }
        }
        return $wholeData;
        // @codeCoverageIgnoreEnd
    }
}
