<?php
/**
 * Class that is responsible for managing order email template variables
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\UrlInterface;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Webkul\Marketplace\Helper\Orders as OrdersHelper;
use Webkul\Marketplace\Model\Saleslist;
use Arb\CustomWebkul\Model\Webkul\ResourceModel\Saleslist\Collection;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory;
use Webkul\Marketplace\Model\SaleslistFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Email order variables managing
 */
class EmailVariables
{
    /**
     * @var SaleslistFactory
     */
    private $saleslistFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var MpHelper
     */
    private $mpHelper;

    /**
     * @var OrdersHelper
     */
    private $orderHelper;

    protected $_productloader;

    /**
     * EmailVariables constructor.
     *
     * @param SaleslistFactory $saleslistFactory
     * @param StoreManagerInterface $storeManager
     * @param Session $customerSession
     * @param MpHelper $mpHelper
     * @param OrdersHelper $orderHelper
     */
    public function __construct(
        SaleslistFactory $saleslistFactory,
        StoreManagerInterface $storeManager,
        Session $customerSession,
        MpHelper $mpHelper,
        OrdersHelper $orderHelper,
        CreditmemoRepositoryInterface $creditmemoRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Catalog\Model\ProductFactory $_productloader
    ) {
        $this->saleslistFactory = $saleslistFactory;
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->mpHelper = $mpHelper;
        $this->orderHelper = $orderHelper;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_productloader = $_productloader;
    }

    /**
     * Preparing variables used in email template
     *
     * @param OrderInterface $order
     * @param int $sellerId
     * @param int $orderId
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function prepareEmailTemplateOrderVariables(OrderInterface $order, int $sellerId, int $orderId, $type = NULL)
    {
        $salesCollection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $orderId
            );

        $totalQuantity = $this->getSaleCollectionData($salesCollection, 'magequantity');
        $totalAmount = $this->getSaleCollectionData($salesCollection, 'total_amount');
        $orderItems = $order->getAllItems();
                 
        $tracking = $this->orderHelper->getOrderinfo($orderId);
        $shippingCharges = $tracking->getShippingCharges();
        $couponAmount = $tracking->getCouponAmount();
        $totalTax = $tracking->getTotalTax();
        $totalAmount += $shippingCharges;
        $totalAmount -= $couponAmount;
        $totalAmount += $totalTax;

        if($type == 'cancel') {
            $totalQuantity = 0;
            $tracking = $this->orderHelper->getOrderinfo($orderId);
            $creditmemoIds = $tracking->getCreditmemoId();
            $creditMemoIdArray = explode(',', $creditmemoIds);
            $totalRefundedAmount = 0;
            foreach ($creditMemoIdArray as $creditMemoId) {
                $searchCriteria = $this->searchCriteriaBuilder->addFilter('entity_id', $creditMemoId)->create();
                $creditmemos = $this->creditmemoRepository->getList($searchCriteria);
                $creditmemoRecords = $creditmemos->getItems();
                foreach ($creditmemoRecords as $creditMemoData) {
                    $items = $creditMemoData->getAllItems();
                    foreach($items as $item){
                        $itemType = $this->getProductType($item->getProductId());
                        if($itemType == 'configurable'){
                            continue;
                        }
                        $totalQuantity +=  $item->getQty();
                    }
                    $refundAmount = $creditMemoData->getBaseGrandTotal();
                    $totalRefundedAmount += number_format($refundAmount, 2, ".", "");
                }
            }
            $totalAmount = $totalRefundedAmount;
        }

        $webkulMerchant = $this->mpHelper->getSellerCollectionObj($sellerId);
        $logopic = null;
        $contact = '';
        foreach ($webkulMerchant->getItems() as $merchant) {
            $logopic = (string)$merchant->getLogoPic();
            $contact = (string)$merchant->getContactNumber();
            if (strlen($logopic) <= 0) {
                $logopic = 'noimage.png';
            }
            if (strlen($contact) <= 0) {
                $contact = '';
            }
        }

        $sellerLogo = $this->getBaseMediaUrl() . '/avatar/' . $logopic ?? 'noimage.png';

        $merchant = $this->customerSession->getCustomer();

        $contactNumber = $contact ?: 'not provided';

        $payment = null;
        $paymentMethod = $order->getPayment()->getMethod();

        if ($paymentMethod === 'arbpayment_gateway') {
            $payment = __('Card payment');
        } else {
            $payment = __('Promo code');
        }

        $emailTemplateVariables = [
            'order_increment_id' => $order->getIncrementId(),
            'merchant_name' => $merchant->getName(),
            'customer_name' => $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname(),
            'payment_method' => $payment,
            'items_number' => $totalQuantity,
            'total_amount' => $totalAmount,
            'merchant_logo' => $sellerLogo,
            'merchant_service_number' => $contactNumber,
        ];

        return $emailTemplateVariables;
    }

    /**
     * Getting specific sale data from collection
     *
     * @param Collection $collection
     * @param string $columnName
     *
     * @return float
     */
    private function getSaleCollectionData(Collection $collection, string $columnName)
    {
        $total = 0;

        $items = $collection->getItems();

        /** @var Saleslist $coll */
        foreach ($items as $coll) {
            $total += (float)$coll->getData($columnName);
        }

        return $total;
    }

    /**
     * Getting base media url
     *
     * @return string
     * @throws NoSuchEntityException
     */
    private function getBaseMediaUrl()
    {
        return rtrim($this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA), '/');
    }

    public function getProductType($product_id)
    {
        $product = $this->_productloader->create()->load($product_id);
        return $product->getTypeId();
    }
}
