<?php
/**
 * File with helper class for emails
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Helper;

use Arb\CustomWebkul\Api\EmailTemplateRepositoryInterface;
use Magento\Email\Model\Template;
use Magento\Email\Model\Template\Config as TemplateConfig;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Email\Model\TemplateFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Creating and saving editable email templates
 */
class EmailTemplate
{
    /**
     * @var TemplateConfig
     */
    private $templateConfig;

    /**
     * @var TemplateFactory
     */
    private $templateFactory;

    /**
     * @var EmailTemplateRepositoryInterface
     */
    private $emailTemplateRepository;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * EmailTemplate constructor.
     *
     * @param TemplateConfig $templateConfig
     * @param TemplateFactory $templateFactory
     * @param EmailTemplateRepositoryInterface $emailTemplateRepository
     * @param WriterInterface $writer
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        TemplateConfig $templateConfig,
        TemplateFactory $templateFactory,
        EmailTemplateRepositoryInterface $emailTemplateRepository,
        WriterInterface $writer,
        StoreRepositoryInterface $storeRepository
    ) {
        $this->templateConfig = $templateConfig;
        $this->templateFactory = $templateFactory;
        $this->emailTemplateRepository = $emailTemplateRepository;
        $this->writer = $writer;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @param string $templateCode
     * @param string|null $path
     * @param string $storeCode
     *
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    public function createEmailTemplate(string $templateCode, ?string $path, string $storeCode = 'en')
    {
        /** @var Template $parentTemplate */
        $parentTemplate = $this->templateFactory->create();
        $parentTemplate->setTemplateCode($templateCode);
        $parentTemplate->setForcedArea($parentTemplate->getTemplateCode());
        $parentTemplate->loadDefault($parentTemplate->getTemplateCode());

        /** @var Template $template */
        $template = $this->emailTemplateRepository->getByTemplateCode($templateCode);
        
        if (is_null($template->getId())) {
            $template = $this->templateFactory->create();
            $label = $this->templateConfig->getTemplateLabel($parentTemplate->getTemplateCode());
            $template->setTemplateCode($label->getText());
        }

        $this->setTemplateData($template, $parentTemplate);
        $this->emailTemplateRepository->save($template);

        if ($path !== null) {
            $this->saveToConfig($path, $template);
        }
    }

    /**
     * Preparing email template data
     *
     * @param Template $template
     * @param Template $parentTemplate
     *
     * @return void
     */
    private function setTemplateData(Template $template, Template $parentTemplate)
    {
        $template->setTemplateText($parentTemplate->getTemplateText())
            ->setTemplateType(Template::TYPE_HTML)
            ->setTemplateSubject(
                $parentTemplate->getTemplateSubject()
            )
            ->setOrigTemplateCode($parentTemplate->getTemplateCode())
            ->setOrigTemplateVariables(
                $parentTemplate->getOrigTemplateVariables()
            );

        if ($parentTemplate->getTemplateStyles()) {
            $template->setTemplateStyles($parentTemplate->getTemplateStyles());
        }
    }

    /**
     * Saving configuration
     *
     * @param string $path
     * @param Template $template
     *
     * @return void
     */
    protected function saveToConfig(string $path, Template $template)
    {
        $this->writer->save(
            $path,
            $template->getId()
        );
    }
}
