<?php
/**
 * This file consist of PHPUnit test case for plugin class Data
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Plugin\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\CustomWebkul\Plugin\Helper\Data as DataPlugin;
use Webkul\MpRmaSystem\Helper\Data as WebkulData;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * @covers \Arb\CustomWebkul\Plugin\Helper\Data
 */
class DataTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Object to test
     *
     * @var DataPlugin
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->createMock(ScopeConfigInterface::class);

        $this->testObject = $objectManager->getObject(DataPlugin::class, [
            'scopeConfig' => $this->scopeConfig
        ]);
    }

    /**
     * @return array
     */
    public function afterGetAdminEmailProvider()
    {
        return [
            [   // Scenario 1: RMA Admin email is enabled and email is provided -> use RMA email
                1,                      // number of times method getValue gets called
                'some@email.com',       // value returned by RMA Email method
                true,                   // is RMA Admin Email checked to be used
                'some@email.com'        // return Value of method
            ],
            [   // Scenario 2: RMA Admin email is disabled and email is provided -> use default marketplace email
                2,                      // number of times method getValue gets called
                'some@email.com',       // value returned by RMA Email method
                false,                  // is RMA Admin Email checked to be used
                'some@email.com'        // return Value of method
            ],
            [   // Scenario 3: RMA Admin email is disabled and email is not provided -> use default marketplace email
                2,                      // number of times method getValue gets called
                null,                   // value returned by RMA Email method
                false,                  // is RMA Admin Email checked to be used
                'some@email.com'        // return Value of method
            ],
            [   // Scenario 4A: RMA Admin email is enabled and email is not provided -> use default marketplace email
                2,                      // number of times method getValue gets called
                null,                   // value returned by RMA Email method
                true,                   // is RMA Admin Email checked to be used
                'some@email.com'        // return Value of method
            ],
            [   // Scenario 4B: RMA Admin email is enabled and email is not provided -> use default marketplace email
                2,                      // number of times method getValue gets called
                null,                   // value returned by RMA Email method
                true,                   // is RMA Admin Email checked to be used
                null                    // return Value of method
            ]
        ];
    }

    /**
     * @dataProvider afterGetAdminEmailProvider
     *
     * @param int $callCount
     * @param string|null $rmaEmail
     * @param bool $isRmaAdminEmailEnabled
     * @param string|null $returnValue
     *
     * @return void
     */
    public function testAfterGetAdminEmail(
        int $callCount,
        ?string $rmaEmail,
        bool $isRmaAdminEmailEnabled,
        ?string $returnValue
    ) {
        $this->scopeConfig->expects($this->exactly($callCount))
            ->method('getValue')
            ->willReturnOnConsecutiveCalls($isRmaAdminEmailEnabled, $returnValue);

        /** @var PHPUnit_Framework_MockObject_MockObject|WebkulData $webkulData */
        $webkulData = $this->createMock(WebkulData::class);

        $afterGetAdminEmail = $this->testObject->afterGetAdminEmail($webkulData, $rmaEmail);
        $this->assertSame($returnValue, $afterGetAdminEmail);
    }
}
