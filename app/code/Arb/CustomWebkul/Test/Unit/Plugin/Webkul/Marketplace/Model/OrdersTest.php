<?php
/**
 * This file consist of PHPUnit test case for plugin class Orders
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Plugin\Webkul\Marketplace\Model;

use Arb\CustomWebkul\Model\OrderDetails;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Arb\CustomWebkul\Api\OrderDetailsRepositoryInterface;
use Arb\CustomWebkul\Model\OrderDetailsFactory;
use Webkul\Marketplace\Model\Orders as WebkulOrders;
use Arb\CustomWebkul\Plugin\Webkul\Marketplace\Model\Orders;

/**
 * @covers \Arb\CustomWebkul\Plugin\Webkul\Marketplace\Model\Orders
 */
class OrdersTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderDetailsRepositoryInterface
     */
    private $orderDetailsRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderDetailsFactory
     */
    private $orderDetailsFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulOrders
     */
    private $subjectMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulOrders
     */
    private $resultMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Orders
     */
    private $testObject;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->orderDetailsRepositoryMock = $this->createMock(OrderDetailsRepositoryInterface::class);
        $this->orderDetailsFactoryMock = $this->createMock(OrderDetailsFactory::class);

        $this->subjectMock = $this->createMock(WebkulOrders::class);
        $this->resultMock = $this->getMockBuilder(WebkulOrders::class)
            ->setMethods(['getId', 'getIsApproved', 'getCancelReason'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->testObject = $objectManager->getObject(Orders::class, [
            'orderDetailsRepository' => $this->orderDetailsRepositoryMock,
            'orderDetailsFactory' => $this->orderDetailsFactoryMock
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testAfterSave()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderDetails $orderDetailsMock */
        $orderDetailsMock = $this->createMock(OrderDetails::class);

        $this->orderDetailsRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn(null);

        $this->orderDetailsFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($orderDetailsMock);

        $this->resultMock
            ->method('getIsApproved')
            ->willReturn(1);

        $this->resultMock
            ->method('getCancelReason')
            ->willReturn('some reason');

        $afterSave = $this->testObject->afterSave($this->subjectMock, $this->resultMock);
        $this->assertInstanceOf(WebkulOrders::class, $afterSave);
    }

    /**
     * @return void
     */
    public function testAfterLoad()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderDetails $orderDetailsMock */
        $orderDetailsMock = $this->getMockBuilder(OrderDetails::class)
            ->setMethods(['getIsApproved', 'getCancelReason'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderDetailsRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn($orderDetailsMock);

        $orderDetailsMock
            ->method('getIsApproved')
            ->willReturn(1);

        $orderDetailsMock
            ->method('getCancelReason')
            ->willReturn('some reason');

        $afterLoad = $this->testObject->afterLoad($this->subjectMock, $this->resultMock);
        $this->assertInstanceOf(WebkulOrders::class, $afterLoad);
    }
}
