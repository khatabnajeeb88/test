<?php
/**
 * This file consist of PHPUnit test case for plugin class Items
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Plugin\Webkul\MpRmaSystem\Model;

use Arb\CustomWebkul\Model\RmaItem;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Arb\CustomWebkul\Api\RmaItemRepositoryInterface;
use Arb\CustomWebkul\Model\RmaItemFactory;
use Webkul\MpRmaSystem\Model\Items as WebkulItems;
use Arb\CustomWebkul\Plugin\Webkul\MpRmaSystem\Model\Items;

/**
 * @covers \Arb\CustomWebkul\Plugin\Webkul\MpRmaSystem\Model\Items
 */
class ItemsTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RmaItemRepositoryInterface
     */
    private $rmaItemRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RmaItemFactory
     */
    private $rmaItemFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulItems
     */
    private $subjectMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulItems
     */
    private $resultMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Items
     */
    private $testObject;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->rmaItemRepositoryMock = $this->createMock(RmaItemRepositoryInterface::class);
        $this->rmaItemFactoryMock = $this->createMock(RmaItemFactory::class);

        $this->subjectMock = $this->createMock(WebkulItems::class);
        $this->resultMock = $this->getMockBuilder(WebkulItems::class)
            ->setMethods(['getId', 'getItemCondition'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->testObject = $objectManager->getObject(Items::class, [
            'rmaItemRepository' => $this->rmaItemRepositoryMock,
            'rmaItemFactory' => $this->rmaItemFactoryMock
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testAfterSave()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|RmaItem $rmaItemMock */
        $rmaItemMock = $this->createMock(RmaItem::class);

        $this->rmaItemRepositoryMock
            ->expects($this->once())
            ->method('getByRmaId')
            ->willReturn(null);

        $this->rmaItemFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($rmaItemMock);

        $this->resultMock
            ->method('getItemCondition')
            ->willReturn(5);

        $afterSave = $this->testObject->afterSave($this->subjectMock, $this->resultMock);
        $this->assertInstanceOf(WebkulItems::class, $afterSave);
    }

    /**
     * @return void
     */
    public function testAfterLoad()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|RmaItem $rmaItemMock */
        $rmaItemMock = $this->getMockBuilder(RmaItem::class)
            ->setMethods(['getItemCondition'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->rmaItemRepositoryMock
            ->expects($this->once())
            ->method('getByRmaId')
            ->willReturn($rmaItemMock);

        $rmaItemMock
            ->method('getItemCondition')
            ->willReturn(7);

        $afterLoad = $this->testObject->afterLoad($this->subjectMock, $this->resultMock);
        $this->assertInstanceOf(WebkulItems::class, $afterLoad);
    }
}
