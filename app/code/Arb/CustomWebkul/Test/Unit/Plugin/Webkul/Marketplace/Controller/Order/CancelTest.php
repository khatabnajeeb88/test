<?php
/**
 * This file consist of PHPUnit test case for plugin class Cancel
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Plugin\Webkul\Marketplace\Controller\Order;

use Arb\CustomWebkul\Helper\Email;
use Arb\CustomWebkul\Helper\EmailVariables;
use Arb\CustomWebkul\Plugin\Webkul\Marketplace\Controller\Order\Cancel;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\Store;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Webkul\Marketplace\Api\Data\OrdersInterface;
use Webkul\Marketplace\Controller\Order\Cancel as WebkulCancel;
use Webkul\Marketplace\Helper\Orders;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersModel;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection;

/**
 * test class CancelTest
 */
class CancelTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Orders
     */
    private $webkulOrdersMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Email
     */
    private $emailMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Session
     */
    private $customerSessionMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderRepositoryInterface
     */
    private $orderRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|MpOrdersModel
     */
    private $mpOrdersModelMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|EmailVariables
     */
    private $emailVariablesMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulCancel
     */
    private $subjectMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Redirect
     */
    private $resultMock;

    /**
     * Object to test
     *
     * @var Cancel
     */
    private $testObject;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->webkulOrdersMock = $this->createMock(Orders::class);
        $this->emailMock = $this->createMock(Email::class);
        $this->customerSessionMock = $this->createMock(Session::class);
        $this->orderRepositoryMock = $this->createMock(OrderRepositoryInterface::class);
        $this->mpOrdersModelMock = $this->createMock(MpOrdersModel::class);
        $this->emailVariablesMock = $this->createMock(EmailVariables::class);

        $this->subjectMock = $this->createMock(WebkulCancel::class);
        $this->resultMock = $this->createMock(Redirect::class);

        $this->testObject = $objectManager->getObject(Cancel::class, [
            'webkulOrders' => $this->webkulOrdersMock,
            'email' => $this->emailMock,
            'customerSession' => $this->customerSessionMock,
            'orderRepository' => $this->orderRepositoryMock,
            'mpOrdersModel' => $this->mpOrdersModelMock,
            'emailVariables' => $this->emailVariablesMock,
        ]);
    }

    /**
     * @return array
     */
    public function afterExecuteProvider()
    {
        return [
          [     // Scenario 0: status different than 'canceled', data should not be saved
              'some status',    // status
              0                 // number of 'save' method execution
          ],
          [     // Scenario 1: status 'canceled', data should be saved
              'canceled',       // status
              1                 // number of 'save' method execution
          ],
        ];
    }

    /**
     * @dataProvider afterExecuteProvider
     *
     * @param string $status
     * @param int $expectedOrderSaving
     *
     * @throws LocalizedException
     * @throws ReflectionException
     */
    public function testAfterExecute(string $status, int $expectedOrderSaving)
    {
        $requestMock = $this->createMock(RequestInterface::class);
        $this->subjectMock->expects($this->atLeastOnce())->method('getRequest')->willReturn($requestMock);
        $requestMock->method('getParam')->willReturn(1);

        $merchantOrder = $this->getMockBuilder(OrdersInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getId',
                'setId',
                'getCreatedAt',
                'setCreatedAt',
                'getUpdatedAt',
                'setUpdatedAt',
                'getOrderStatus',
                'setData',
                'save',
                'getCollection',
            ])
            ->getMock();
        
        $this->webkulOrdersMock->expects($this->once())->method('getOrderinfo')->with(1)->willReturn($merchantOrder);
        $emailVariables = ['total_amount' => 100];

        $this->emailVariablesMock->method('prepareEmailTemplateOrderVariables')->willReturn($emailVariables);

        $merchantOrder->method('getOrderStatus')->willReturn($status);

        $merchantOrder->expects($this->exactly($expectedOrderSaving))->method('setData');
        $merchantOrder->expects($this->exactly($expectedOrderSaving))->method('save');

        $orderMock = $this->createMock(Order::class);
        $this->orderRepositoryMock->expects($this->atLeastOnce())->method('get')->willReturn($orderMock);

        $storeMock = $this->createMock(Store::class);
        $orderMock->method('getStore')->willReturn($storeMock);

        $customerMock = $this->createMock(Customer::class);
        $this->customerSessionMock->expects($this->once())->method('getCustomer')->willReturn($customerMock);

        $this->mpOrdersModelMock->method('create')->willReturn($merchantOrder);

        $webkulOrderCollection = $this->createMock(Collection::class);

        $merchantOrder->method('getCollection')->willReturn($webkulOrderCollection);
        $webkulOrderCollection->method('addFieldToFilter')->willReturnSelf();
        $webkulOrderCollection->method('getItems')->willReturn([$merchantOrder]);

        $this->testObject->afterExecute($this->subjectMock, $this->resultMock);
    }
}
