<?php
/**
 * This file consist of PHPUnit test case for plugin class Details
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Plugin\Webkul\MpRmaSystem\Model;

use Arb\CustomWebkul\Model\RmaDetails;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Arb\CustomWebkul\Api\RmaDetailsRepositoryInterface;
use Arb\CustomWebkul\Model\RmaDetailsFactory;
use Webkul\MpRmaSystem\Model\Details as WebkulDetails;
use Arb\CustomWebkul\Plugin\Webkul\MpRmaSystem\Model\Details;

/**
 * @covers \Arb\CustomWebkul\Plugin\Webkul\MpRmaSystem\Model\Details
 */
class DetailsTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RmaDetailsRepositoryInterface
     */
    private $rmaDetailsRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RmaDetailsFactory
     */
    private $rmaDetailFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulDetails
     */
    private $subjectMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulDetails
     */
    private $resultMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Details
     */
    private $testObject;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->rmaDetailsRepositoryMock = $this->createMock(RmaDetailsRepositoryInterface::class);
        $this->rmaDetailFactoryMock = $this->createMock(RmaDetailsFactory::class);

        $this->subjectMock = $this->createMock(WebkulDetails::class);
        $this->resultMock = $this->getMockBuilder(WebkulDetails::class)
            ->setMethods(['getId', 'getDeclineReason', 'getTelephone'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->testObject = $objectManager->getObject(Details::class, [
            'detailsRepository' => $this->rmaDetailsRepositoryMock,
            'rmaDetailsFactory' => $this->rmaDetailFactoryMock
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testAfterSave()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|RmaDetails $rmaDetailsMock */
        $rmaDetailsMock = $this->createMock(RmaDetails::class);

        $this->rmaDetailsRepositoryMock
            ->expects($this->once())
            ->method('getByRmaId')
            ->willReturn(null);

        $this->rmaDetailFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($rmaDetailsMock);

        $this->resultMock
            ->method('getDeclineReason')
            ->willReturn('some reason');

        $this->resultMock
            ->method('getTelephone')
            ->willReturn('12345');

        $afterSave = $this->testObject->afterSave($this->subjectMock, $this->resultMock);
        $this->assertInstanceOf(WebkulDetails::class, $afterSave);
    }

    /**
     * @return void
     */
    public function testAfterLoad()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|RmaDetails $rmaDetailsMock */
        $rmaDetailsMock = $this->getMockBuilder(RmaDetails::class)
            ->setMethods(['getDeclineReason', 'getTelephone'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->rmaDetailsRepositoryMock
            ->expects($this->once())
            ->method('getByRmaId')
            ->willReturn($rmaDetailsMock);

        $rmaDetailsMock
            ->method('getDeclineReason')
            ->willReturn('reason');

        $rmaDetailsMock
            ->method('getTelephone')
            ->willReturn('123145');

        $afterLoad = $this->testObject->afterLoad($this->subjectMock, $this->resultMock);
        $this->assertInstanceOf(WebkulDetails::class, $afterLoad);
    }
}
