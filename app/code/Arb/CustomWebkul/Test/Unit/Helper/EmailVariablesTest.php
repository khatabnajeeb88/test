<?php
/**
 * This file consist of PHPUnit test case for helper class EmailVariables
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Helper;

use Arb\CustomWebkul\Helper\EmailVariables;
use Arb\CustomWebkul\Model\Webkul\ResourceModel\Saleslist\Collection;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Store\Api\Data\StoreInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Webkul\Marketplace\Api\Data\OrdersInterface;
use Webkul\Marketplace\Helper\Orders as OrdersHelper;
use Webkul\Marketplace\Model\ResourceModel\Seller\Collection as SellerCollection;
use Webkul\Marketplace\Model\Saleslist;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Marketplace\Model\Seller;

/**
 * Testing EmailVariables class methods
 */
class EmailVariablesTest extends TestCase
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SaleslistFactory
     */
    private $saleslistFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StoreManagerInterface
     */
    private $storeManagerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Session
     */
    private $customerSessionMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|MpHelper
     */
    private $mpHelperMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersHelper
     */
    private $orderHelperMock;

    /**
     * Object to test
     *
     * @var EmailVariables
     */
    private $testObject;

    /**
     * Setting up all necessary mocks
     *
     * @throws ReflectionException
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->saleslistFactoryMock = $this->createMock(SaleslistFactory::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->customerSessionMock = $this->createMock(Session::class);
        $this->mpHelperMock = $this->createMock(MpHelper::class);
        $this->orderHelperMock = $this->createMock(OrdersHelper::class);

        $this->testObject = $this->objectManager->getObject(EmailVariables::class, [
            'saleslistFactory' => $this->saleslistFactoryMock,
            'storeManager' => $this->storeManagerMock,
            'customerSession' => $this->customerSessionMock,
            'mpHelper' => $this->mpHelperMock,
            'orderHelper' => $this->orderHelperMock,
        ]);
    }

    /**
     * Testing prepareEmailTemplateOrderVariables method
     *
     * @throws ReflectionException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testPrepareEmailTemplateOrderVariables()
    {
        $salesList = $this->createMock(Saleslist::class);
        $this->saleslistFactoryMock->expects($this->once())->method('create')->willReturn($salesList);

        $salesListCollection = $this->createMock(Collection::class);
        $salesList->expects($this->once())->method('getCollection')->willReturn($salesListCollection);
        $salesListCollection->method('addFieldToFilter')->willReturnSelf();
        $salesListCollection->method('addFieldToFilter')->willReturnSelf();

        $trackingMock = $this->getMockBuilder(OrdersInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getShippingCharges',
                'getCouponAmount',
                'getTotalTax',
                'getId',
                'setId',
                'getCreatedAt',
                'setCreatedAt',
                'getUpdatedAt',
                'setUpdatedAt',
            ])
            ->getMock();
        $this->orderHelperMock->expects($this->once())->method('getOrderinfo')->willReturn($trackingMock);
        $trackingMock->expects($this->once())->method('getShippingCharges')->willReturn('123');

        $salesListCollection->expects($this->exactly(2))->method('getItems')->willReturn([$salesList]);

        $salesList->method('getData')->willReturn(5);

        $sellerCollectionMock = $this->createMock(SellerCollection::class);

        $this->mpHelperMock->method('getSellerCollectionObj')->willReturn($sellerCollectionMock);

        $sellerMock = $this->createMock(Seller::class);
        $sellerCollectionMock->method('getItems')->willReturn($sellerMock);

        $storeMock = $this->getMockBuilder(StoreInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getId',
                'setId',
                'getCode',
                'setCode',
                'getName',
                'setName',
                'getWebsiteId',
                'setWebsiteId',
                'getStoreGroupId',
                'setStoreGroupId',
                'setIsActive',
                'getIsActive',
                'getExtensionAttributes',
                'setExtensionAttributes',
                'getBaseUrl'
            ])
            ->getMock();

        $this->storeManagerMock->expects($this->once())->method('getStore')->willReturn($storeMock);
        $storeMock->expects($this->once())->method('getBaseUrl')->willReturn('some url');

        $customerMock = $this->createMock(Customer::class);
        $this->customerSessionMock->method('getCustomer')->willReturn($customerMock);

        $orderPaymentMock = $this->createMock(OrderPaymentInterface::class);

        $orderMock = $this->getMockBuilder(\Magento\Sales\Model\Order::class)
                        ->disableOriginalConstructor()
                        ->setMethods(["getIncrementId","getAllItems","getPayment"])
                        ->getMock();
        
        $orderMock->method('getPayment')->willReturn($orderPaymentMock);
        $orderMock->method('getAllItems')->willReturnSelf();

        $sellerId = 9;
        $orderId = 2;
        $this->assertInternalType(
            'array',
            $this->testObject->prepareEmailTemplateOrderVariables($orderMock, $sellerId, $orderId)
        );
    }
}
