<?php
/**
 * This file consist of PHPUnit test case for helper class  EmailTemplate
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Helper;

use Arb\CustomWebkul\Helper\EmailTemplate;
use Magento\Framework\Phrase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Store\Api\StoreRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Arb\CustomWebkul\Api\EmailTemplateRepositoryInterface;
use Magento\Email\Model\Template;
use Magento\Email\Model\Template\Config as TemplateConfig;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Email\Model\TemplateFactory;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;

/**
 * Testing EmailTemplate class methods
 */
class EmailTemplateTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|TemplateConfig
     */
    private $templateConfigMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|TemplateFactory
     */
    private $templateFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|EmailTemplateRepositoryInterface
     */
    private $emailTemplateRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WriterInterface
     */
    private $writerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StoreRepositoryInterface
     */
    private $storeRepositoryMock;

    /**
     * Object to test
     *
     * @var EmailTemplate
     */
    private $testObject;

    /**
     * Setting up all necessary mocks
     *
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->templateConfigMock = $this->createMock(TemplateConfig::class);
        $this->templateFactoryMock = $this->getMockBuilder(TemplateFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['setTemplateCode',"setForcedArea","loadDefault","create","getTemplateCode","getId"])
            ->getMock();

        $this->emailTemplateRepositoryMock = $this->getMockBuilder(EmailTemplateRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getByTemplateCode'])
            ->getMockForAbstractClass();;

        $this->writerMock = $this->createMock(WriterInterface::class);
        $this->storeRepositoryMock = $this->createMock(StoreRepositoryInterface::class);
        $this->templateFactoryMock->method("create")->willReturnSelf();
        $this->testObject = $objectManager->getObject(EmailTemplate::class, [
            'templateConfig' => $this->templateConfigMock,
            'templateFactory' => $this->templateFactoryMock,
            'emailTemplateRepository' => $this->emailTemplateRepositoryMock,
            'writer' => $this->writerMock,
            'storeRepository' => $this->storeRepositoryMock
        ]);
    }

    /**
     * Test for CreateEmailTemplate method
     *
     * @throws AlreadyExistsException
     * @throws ReflectionException
     *
     * @return void
     */
    public function testCreateEmailTemplate()
    {

        $labelMock = $this->createMock(Phrase::class);
        $this->templateConfigMock->expects($this->any())->method('getTemplateLabel')->willReturn($labelMock);
        $this->templateFactoryMock->method("setTemplateCode")->willReturnSelf();
        $this->templateFactoryMock->method("setForcedArea")->willReturnSelf();
        $this->templateFactoryMock->method("getTemplateCode")->willReturn("test");
        $this->templateMock =  $this->createMock(\Magento\Email\Model\Template::class);
        $this->templateFactoryMock->method("loadDefault")->willReturn($this->templateMock);
        $this->emailTemplateRepositoryMock->method("getByTemplateCode")->willReturn($this->templateMock);
        $this->templateFactoryMock->method("getId")->willReturn(10);
        $storeMock = $this->createMock(\Magento\Store\Api\Data\StoreInterface::class);
        $this->storeRepositoryMock->expects($this->any())->method('get')->willReturn($storeMock);
        $storeMock->expects($this->any())->method('getId')->willReturn(1);

        //$this->testObject->createEmailTemplate('template code', 'some/config/path');
    }
}
