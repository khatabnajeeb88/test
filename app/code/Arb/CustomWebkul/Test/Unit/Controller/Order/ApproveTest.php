<?php
/**
 * This file consist of PHPUnit test case for controller class Approve
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Test\Unit\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Arb\CustomWebkul\Controller\Order\Approve;
use Magento\Sales\Model\Order;
use Webkul\Marketplace\Helper\Data;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Controller\Result\Redirect;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection as WebkulOrderCollection;
use Webkul\Marketplace\Model\Orders as WebkulOrder;
use Webkul\Marketplace\Model\OrdersFactory;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\ObjectManagerInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use \Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Model\Saleslist;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\Collection as SalesListCollection;

/**
 * @covers \Arb\CustomWebkul\Controller\Order\Approve
 */
class ApproveTest extends TestCase
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Context
     */
    private $context;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ResultFactory
     */
    private $resultFactory;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RedirectInterface
     */
    private $redirect;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Data
     */
    private $sellerHelper;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersFactory
     */
    private $mpOrderFactory;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ManagerInterface
     */
    private $messageManager;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RequestInterface
     */
    private $request;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SaleslistFactory
     */
    private $saleslistFactory;

    /**
     * Object to test
     *
     * @var Approve
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->context = $this->createMock(Context::class);
        $this->resultFactory = $this->createMock(ResultFactory::class);
        $this->redirect = $this->createMock(RedirectInterface::class);
        $this->sellerHelper = $this->createMock(Data::class);
        $this->mpOrderFactory = $this->createMock(OrdersFactory::class);
        $this->messageManager = $this->createMock(ManagerInterface::class);
        $this->orderRepository = $this->createMock(OrderRepositoryInterface::class);
        $this->saleslistFactory = $this->createMock(SaleslistFactory::class);

        $this->request = $this->createMock(RequestInterface::class);
        $this->context->method('getRequest')->willReturn($this->request);

        $this->testObject = $this->objectManager->getObject(Approve::class, [
            'context' => $this->context,
            'resultFactory' => $this->resultFactory,
            'helper' => $this->sellerHelper,
            'mpOrdersModel' => $this->mpOrderFactory,
            'messageManager' => $this->messageManager,
            'orderRepository' => $this->orderRepository,
            'saleslistFactory' => $this->saleslistFactory,
            '_redirect' => $this->redirect
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|WebkulOrder $order */
        $order = $this->createMock(WebkulOrder::class);

        return [
            [ // Scenario 1: User is not a merchant, redirection occurs
                false,                      // is user a Merchant
                true,                       // not relevant
                []                          // not relevant
            ],
            [ // Scenario 2: User is a merchant but it's not his order, redirection occurs
                true,                       // is user a Merchant
                true,                       // is View Order Page
                []                          // Order Entity in Collection
            ],
            [   // Scenario 3: User is a merchant, Controller redirects to previous page
                true,                       // is user a Merchant
                true,                       // is View Order Page
                [$order]                    // Order Entity in Collection
            ],
            [   // Scenario 4: User is a merchant, no redirection to previous page
                true,                       // is user a Merchant
                false,                      // is View Order Page
                [$order]                    // Order Entity in Collection
            ],
        ];
    }

    /**
     * Test function for Controller's execute
     *
     * @dataProvider executeProvider
     *
     * @param bool $isMerchant
     * @param bool $isOrderPage
     * @param array $order
     *
     * @return void
     */
    public function testExecute(bool $isMerchant, bool $isOrderPage, array $order)
    {
        $this->sellerHelper->method('isSeller')->willReturn($isMerchant);

        /** @var PHPUnit_Framework_MockObject_MockObject|Redirect $result */
        $redirect = $this->createMock(Redirect::class);

        /** @var PHPUnit_Framework_MockObject_MockObject|Json $json */
        $json = $this->createMock(Json::class);

        $this->resultFactory->method('create')->willReturnOnConsecutiveCalls($redirect, $json);
        $this->redirect->method('getRefererUrl')->willReturn('www.previous.url.com');

        $this->request->method('getParam')->willReturnOnConsecutiveCalls($isOrderPage, 25);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $webkulOrder */
        $webkulOrder = $this->createMock(WebkulOrder::class);

        $this->mpOrderFactory->method('create')->willReturn($webkulOrder);

        /** @var PHPUnit_Framework_MockObject_MockObject|WebkulOrderCollection $webkulOrderCollection */
        $webkulOrderCollection = $this->createMock(WebkulOrderCollection::class);

        $webkulOrder->method('getCollection')->willReturn($webkulOrderCollection);
        $webkulOrderCollection->method('addFieldToFilter')->willReturnSelf();
        $webkulOrderCollection->method('getSize')->willReturn(count($order));
        $webkulOrderCollection->method('getItems')->willReturn($order);

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $mageOrder */
        $mageOrder = $this->createMock(Order::class);

        $this->orderRepository->method('get')->willReturn($mageOrder);

        /** @var PHPUnit_Framework_MockObject_MockObject|Saleslist $saleslist */
        $saleslist = $this->createMock(Saleslist::class);

        $this->saleslistFactory->method('create')->willReturn($saleslist);

        /** @var PHPUnit_Framework_MockObject_MockObject|SalesListCollection $saleslistCollection */
        $saleslistCollection = $this->createMock(SalesListCollection::class);

        $saleslist->method('getCollection')->willReturn($saleslistCollection);
        $saleslistCollection->method('addFieldToFilter')->willReturnSelf();

        $json->method('setData')->willReturnSelf();

        $execute = $this->testObject->execute();
        $this->assertInstanceOf(ResultInterface::class, $execute);
    }
}
