<?php
/**
 * This file consist of PHPUnit test case for controller class Reject
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Controller\Order;

use Arb\CustomWebkul\Controller\Order\Reject;
use Magento\Customer\Model\Customer;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\Data\OrderInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersFactory;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection;
use Arb\CustomWebkul\Helper\EmailVariables;
use Arb\CustomWebkul\Helper\Email;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Customer\Model\Session;

/**
 * Testing Reject class methods
 */
class RejectTest extends TestCase
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|\Magento\Framework\Controller\ResultFactory
     */
    private $resultFactory;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersFactory
     */
    private $mpOrderFactory;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderRepositoryInterface
     */
    private $orderRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Email
     */
    private $emailMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|EmailVariables
     */
    private $emailVariablesMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|HelperData
     */
    private $helperMock;

    /**
     * @var PricingHelper|Session
     */
    private $pricingHelperMock;

    /**
     * Object to test
     *
     * @var Reject
     */
    private $testObject;

    /**
     * Setting up all necessary mocks
     *
     * @throws ReflectionException
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->mpOrderFactory = $this->createMock(OrdersFactory::class);
        $this->emailMock = $this->createMock(Email::class);
        $this->emailVariablesMock = $this->createMock(EmailVariables::class);
        $this->orderRepositoryMock = $this->createMock(OrderRepositoryInterface::class);
        $this->customerSession = $this->createMock(Session::class);

        $this->helperMock = $this->createMock(HelperData::class);
        $this->pricingHelperMock = $this->createMock(PricingHelper::class);

        $this->resultFactory = $this->getMockBuilder(\Magento\Framework\Controller\ResultFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->testObject = $this->objectManager->getObject(Reject::class, [
            'resultFactory' => $this->resultFactory,
            'mpOrdersModel' => $this->mpOrderFactory,
            'orderRepository' => $this->orderRepositoryMock,
            'helper' => $this->helperMock,
            'email' => $this->emailMock,
            'emailVariables' => $this->emailVariablesMock,
            'pricingHelper' => $this->pricingHelperMock
        ]);
    }

    /**
     * data provider for execute method
     *
     * @return array
     */
    public function executeProvider()
    {
        return [
            [ // Scenario 0: User is not a merchant - emails not sent
                false,                      // is user a Merchant
                0,                          // emails sent
            ],
            [ // Scenario 1: User is a merchant - emails sent
                true,                       // is user a Merchant
                2,                          // emails sent
            ]
        ];
    }

    /**
     * Testing execute method
     *
     * @dataProvider executeProvider
     *
     * @param $isMerchant
     * @param $numberOfEmails
     *
     * @throws ReflectionException
     */
    public function testExecute($isMerchant, $numberOfEmails)
    {
        $this->helperMock->expects($this->once())->method('isSeller')->willReturn($isMerchant);

        $redirect = $this->createMock(Redirect::class);

        $this->resultFactory->expects($this->once())->method('create')->willReturn($redirect);

        $redirect->expects($this->once())->method('setUrl')->willReturnSelf();

        $webkulOrder = $this->createMock(Orders::class);
        $this->mpOrderFactory->method('create')->willReturn($webkulOrder);

        $webkulOrderCollection = $this->createMock(Collection::class);

        $webkulOrder->method('getCollection')->willReturn($webkulOrderCollection);
        $webkulOrderCollection->method('addFieldToFilter')->willReturnSelf();
        $webkulOrderCollection->method('getSize')->willReturn(count([$webkulOrder]));
        $webkulOrderCollection->method('getItems')->willReturn([$webkulOrder]);

        $orderMock = $this->createMock(OrderInterface::class);
        $this->orderRepositoryMock->method('get')->willReturn($orderMock);

        $this->emailVariablesMock->method('prepareEmailTemplateOrderVariables')->willReturn([]);
        $this->pricingHelperMock->method('currency')->willReturn('SAR5');

        $customerMock = $this->createMock(Customer::class);
        $this->customerSession->method('getCustomer')->willReturn($customerMock);

        $this->assertInstanceOf(ResultInterface::class, $this->testObject->execute());
    }
}
