<?php
/**
 * This file consist of PHPUnit test case for class OrderDetailsResource
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Test\Unit\Model\ResourceModel;

use Arb\CustomWebkul\Api\Data\OrderDetailsInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\CustomWebkul\Model\ResourceModel\OrderDetailsResource;

/**
 * @covers \Arb\CustomWebkul\Model\ResourceModel\OrderDetailsResource
 */
class OrderDetailsResourceTest extends TestCase
{
    /**
     * @var OrderDetailsResource
     */
    private $resourceModel;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->resourceModel = $objectManager->getObject(OrderDetailsResource::class);
    }

    /**
     * Test getting table
     *
     * @return void
     */
    public function testGetMainTable(): void
    {
        $this->assertSame(
            $this->resourceModel->getMainTable(),
            $this->resourceModel->getTable(OrderDetailsInterface::ORDER_TABLE_NAME)
        );
    }
}
