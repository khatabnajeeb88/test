<?php
/**
 * This file consist of PHPUnit test case for class RmaItemResource
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Test\Unit\Model\ResourceModel;

use Arb\CustomWebkul\Api\Data\RmaItemInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\CustomWebkul\Model\ResourceModel\RmaItemResource;

/**
 * @covers \Arb\CustomWebkul\Model\ResourceModel\RmaItemResource
 */
class RmaItemResourceTest extends TestCase
{
    /**
     * @var RmaItemResource
     */
    private $resourceModel;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->resourceModel = $objectManager->getObject(RmaItemResource::class);
    }

    /**
     * Test getting table
     *
     * @return void
     */
    public function testGetMainTable(): void
    {
        $this->assertSame(
            $this->resourceModel->getMainTable(),
            $this->resourceModel->getTable(RmaItemInterface::RMA_ITEM_TABLE_NAME)
        );
    }
}
