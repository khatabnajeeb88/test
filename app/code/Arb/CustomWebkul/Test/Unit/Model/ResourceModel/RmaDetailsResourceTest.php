<?php
/**
 * This file consist of PHPUnit test case for class RmaDetailsResource
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Test\Unit\Model\ResourceModel;

use Arb\CustomWebkul\Api\Data\RmaDetailsInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetailsResource;

/**
 * @covers \Arb\CustomWebkul\Model\ResourceModel\RmaDetailsResource
 */
class RmaDetailsResourceTest extends TestCase
{
    /**
     * @var RmaDetailsResource
     */
    private $resourceModel;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->resourceModel = $objectManager->getObject(RmaDetailsResource::class);
    }

    /**
     * Test getting table
     *
     * @return void
     */
    public function testGetMainTable(): void
    {
        $this->assertSame(
            $this->resourceModel->getMainTable(),
            $this->resourceModel->getTable(RmaDetailsInterface::RMA_TABLE_NAME)
        );
    }
}
