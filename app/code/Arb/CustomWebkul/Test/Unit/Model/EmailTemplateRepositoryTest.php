<?php
/**
 * This file consist of PHPUnit test case for repository class EmailTemplateRepository
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Model;

use Arb\CustomWebkul\Model\EmailTemplateRepository;
use PHPUnit\Framework\TestCase;
use Magento\Email\Model\ResourceModel\Template;
use Magento\Email\Model\Template as TemplateModel;
use Magento\Email\Model\TemplateFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;

/**
 * Testing EmailTemplateRepository class methods
 */
class EmailTemplateRepositoryTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Template
     */
    private $templateResourceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|TemplateFactory
     */
    private $templateFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|EmailTemplateRepository
     */
    private $testObject;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->templateResourceMock = $this->createMock(Template::class);
        $this->templateFactoryMock = $this->createMock(TemplateFactory::class);

        $this->testObject = $objectManager->getObject(EmailTemplateRepository::class, [
            'templateResource' => $this->templateResourceMock,
            'templateFactory' => $this->templateFactoryMock
        ]);
    }

    /**
     * Test for load method
     *
     * @throws ReflectionException
     *
     * @return void
     */
    public function testLoad()
    {
        $template = $this->createMock(TemplateModel::class);
        $this->templateFactoryMock->expects($this->once())->method('create')->willReturn($template);

        $this->testObject->load('asd');
    }

    /**
     * Test for save method
     *
     * @return void
     *
     * @throws AlreadyExistsException
     * @throws ReflectionException
     */
    public function testSave()
    {
        $template = $this->createMock(TemplateModel::class);

        $this->templateResourceMock->method('save')->willReturn($template);

        $this->assertSame($template, $this->testObject->save($template));
    }
}
