<?php
/**
 * This file consist of PHPUnit test case for RmaDetailsRepository
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Model;

use Arb\CustomWebkul\Model\RmaDetails;
use Arb\CustomWebkul\Model\RmaDetailsFactory;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetailsResource;
use Arb\CustomWebkul\Model\RmaDetailsRepository;
use Magento\CatalogRule\Model\ResourceModel\Product\CollectionProcessor;
use Magento\Framework\Api\Search\SearchCriteria;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetails\CollectionFactory;
use Arb\CustomWebkul\Model\ResourceModel\RmaDetails\Collection;
use Exception;
use ReflectionException;

/**
 * @covers \Arb\CustomWebkul\Model\RmaDetailsRepository
 */
class RmaDetailsRepositoryTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RmaDetailsResource
     */
    private $resourceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionProcessor
     */
    private $collectionProcessorMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|RmaDetailsRepository
     */
    private $testedObject;

    /**
     * Setting up tested object
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->resourceMock = $this->createMock(RmaDetailsResource::class);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->collectionProcessorMock = $this->createMock(CollectionProcessorInterface::class);

        $this->testedObject = $objectManager->getObject(RmaDetailsRepository::class, [
            'resource' => $this->resourceMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'collectionProcessor' => $this->collectionProcessorMock
        ]);
    }

    /**
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws ReflectionException
     */
    public function testSave()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|RmaDetails $rmaDetailsMock */
        $rmaDetailsMock = $this->createMock(RmaDetails::class);

        $this->resourceMock
            ->expects($this->once())
            ->method('save')
            ->willReturn($rmaDetailsMock);

        $save = $this->testedObject->save($rmaDetailsMock);
        $this->assertEquals($rmaDetailsMock, $save);
    }

    /**
     * Test save() method for throwing an exception
     *
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws ReflectionException
     */
    public function testSaveExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|RmaDetails $rmaDetailsMock */
        $rmaDetailsMock = $this->createMock(RmaDetails::class);

        $this->resourceMock
            ->expects($this->once())
            ->method('save')
            ->willThrowException(new Exception('Save error'));

        $this->expectException(CouldNotSaveException::class);

        $this->testedObject->save($rmaDetailsMock);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetByRmaId()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('addFilter')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        $this->collectionProcessorMock
            ->expects($this->once())
            ->method('process');

        $collectionMock
            ->expects($this->once())
            ->method('getSize')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|RmaDetails $rmaDetailsMock */
        $rmaDetailsMock = $this->createMock(RmaDetails::class);

        $collectionMock
            ->expects($this->once())
            ->method('getFirstItem')
            ->willReturn($rmaDetailsMock);

        $getByRmaId = $this->testedObject->getByRmaId(1);
        $this->assertInstanceOf(RmaDetails::class, $getByRmaId);
    }
}
