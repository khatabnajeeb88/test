<?php
/**
 * This file consist of PHPUnit test case for class MerchantData
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */
namespace Arb\CustomWebkul\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\CustomWebkul\Model\MerchantData
 */
class MerchantDataTest extends TestCase
{
    /**
     * @var SellerCollection
     */
    protected $sellerCollection;

   /**
    * Webkul seller block class
    *
    * @var [type]
    */
    protected $sellerBlock;

   /**
    * attribute class object
    *
    * @var [type]
    */
    protected $eavAttribute;

   /**
    * Object Manager instance
    *
    * @var \Magento\Framework\ObjectManagerInterface
    */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\CustomWebkul\Model\MerchantData
     */
    private $testObject;

    /**
     * searchcriteria variable
     *
     * @var [type]
     */
    private $searchCriteriaMock;

    /**
     * merchant mock
     *
     * @var [type]
     */
    private $merchantsBlockMock;

    /**
     * select query
     *
     * @var [type]
     */
    private $selectQueryMock;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->sellerCollection = $this->createMock(
            \Webkul\Marketplace\Model\ResourceModel\Seller\Collection::class
        );

        $this->sellerBlock = $this->createMock(
            \Webkul\Marketplace\Block\Sellerlist::class
        );

        $this->eavAttribute = $this->createMock(
            \Magento\Eav\Model\Entity\Attribute::class
        );

        $this->testObject = $this->objectManager->getObject(
            \Arb\CustomWebkul\Model\MerchantData::class,
            [
                'sellerCollection' => $this->sellerCollection,
                'sellerBlock' => $this->sellerBlock,
                'eavAttribute' => $this->eavAttribute
            ]
        );
    }

    /**
     * test function for getMerchant list
     *
     * @return void
     */
    public function testGetMerchantList()
    {
        $this->searchCriteriaMock = $this->createMock(\Magento\Framework\Api\SearchCriteriaInterface::class);
        $this->merchantsBlockMock =  $this->sellerBlock;

        $this->selectQueryMock = $this->getMockBuilder(\Magento\Framework\DB\Select::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals([], []);
    }
}
