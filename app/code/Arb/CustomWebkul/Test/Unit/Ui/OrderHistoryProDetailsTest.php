<?php
namespace Arb\CustomWebkul\Test\Unit\Ui;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Sales\Model\OrderRepository;
use Webkul\Marketplace\Ui\Component\Listing\Columns\Frontend\OrderHistoryProDetails as WebkulOrderHistoryProDetails;
use Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend\OrderHistoryProDetails;

/**
 * @covers \Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend\OrderHistoryProDetails
 */
class OrderHistoryProDetailsTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->contextMock = $this->createMock(ContextInterface::class);
        $this->uiComponentMock = $this->createMock(UiComponentFactory::class);
        $this->columnMock = $this->createMock(Column::class);
        $this->helperDataMock = $this->createMock(HelperData::class);
        $this->collectionFactoryMock = $this->getMockBuilder(CollectionFactory::class)
            ->setMethods(['create',"addFieldToFilter"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->itemRepositoryMock = $this->getMockBuilder(ItemRepository::class)
            ->setMethods(["getParentItemId","getOrderItemId","get"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->orderItemRepositoryMock = $this->getMockBuilder(ItemRepository::class)
            ->setMethods(["get"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->orderRepositoryMock = $this->createMock(OrderRepository::class);
        $this->webkulOrderHistoryProDetailsMock = $this->createMock(WebkulOrderHistoryProDetails::class);
        $this->collectionFactoryMock->expects($this->any())->method("create")->willReturnSelf();
        $this->collectionFactoryMock->expects($this->at(1))->method("addFieldToFilter")->willReturnSelf();
        $items = [$this->itemRepositoryMock];
        $this->collectionFactoryMock->expects($this->at(2))->method("addFieldToFilter")->willReturn($items);
        $this->collectionFactoryMock->expects($this->any())->method("create")->willReturn($items);

        $this->orderHistoryProDetails = $this->objectManager->getObject(
            OrderHistoryProDetails::class,
            [
                "contextInterface"=>$this->contextMock,
                "uiComponentFactory"=>$this->uiComponentMock,
                "helperData"=>$this->helperDataMock,
                "collectionFactory"=>$this->collectionFactoryMock,
                "itemRepository"=>$this->itemRepositoryMock,
                "orderRepository"=>$this->orderRepositoryMock,
                "orderItemRepository"=>$this->orderItemRepositoryMock
            ]
        );
    }

    public function testGetpronamebyorder()
    {
        $this->itemRepositoryMock->expects($this->any())->method("getParentItemId")->willReturn(true);
        $this->orderHistoryProDetails->getpronamebyorder("30", "2");
    }

    public function testGetpronamebyorderWithData()
    {
        $this->itemRepositoryMock->expects($this->any())->method("getOrderItemId")->willReturn(31);
        $item["product_options"]["options"][] = ["label"=>"teste","value"=>10];
        $item["product_options"]["additional_options"][] = ["label"=>"additonal Test","value"=>10];
        $item["product_options"]["attributes_info"][] = ["label"=>"teste","value"=>10];
        $item["name"] ="testeproduct";
        $item["qty_ordered"] ="5";
        $item["qty_invoiced"] ="5";
        $item["qty_shipped"] ="5";
        $item["qty_canceled"] ="5";
        $item["qty_refunded"] ="5";
        $this->orderItemRepositoryMock->expects($this->any())->method("get")->willReturn($item);
        $this->orderHistoryProDetails->getpronamebyorder("30", "2");
    }
}
