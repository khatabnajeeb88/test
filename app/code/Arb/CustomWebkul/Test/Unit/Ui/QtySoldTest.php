<?php
namespace Arb\CustomWebkul\Test\Unit\Ui;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory;
use Webkul\Marketplace\Helper\Data as HelperData;
use Magento\Framework\UrlInterface;
use Webkul\Marketplace\Ui\Component\Listing\Columns\Frontend\QtySold as WebkulQtySold;
use Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend\QtySold;

/**
 * @covers \Arb\CustomWebkul\Ui\Component\Listing\Columns\Frontend\QtySold
 */
class QtySoldTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->contextMock = $this->createMock(ContextInterface::class);
        $this->uiComponentMock = $this->createMock(UiComponentFactory::class);
        $this->columnMock = $this->createMock(Column::class);
        $this->helperDataMock = $this->createMock(HelperData::class);
        $this->urlInterfaceMock = $this->createMock(UrlInterface::class);
        $this->collectionFactoryMock = $this->getMockBuilder(CollectionFactory::class)
            ->setMethods(['create',"addFieldToFilter","getAllSoldQty"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->collectionFactoryMock->expects($this->any())->method("create")->willReturnSelf();
        $this->collectionFactoryMock->expects($this->any())->method("addFieldToFilter")->willReturnSelf();
        $items[] = ["entity_id"=>10];
        $this->dataSource['data']['items'] = $items;
        $this->qtySold = $this->objectManager->getObject(
            QtySold::class,
            [
                "contextInterface"=>$this->contextMock,
                "uiComponentFactory"=>$this->uiComponentMock,
                "collectionFactory"=>$this->collectionFactoryMock,
                "helperData"=>$this->helperDataMock,
                "urlBuilder"=>$this->urlInterfaceMock
            ]
        );
        $this->qtySold->setData(["name"=>"teste"]);
    }

    public function testPrepareDataSource()
    {
        $data["0"] = ["qty"=>10];
        $this->collectionFactoryMock->expects($this->any())->method("getAllSoldQty")->willReturn($data);
        
        $this->qtySold->prepareDataSource($this->dataSource);
    }

    public function testPrepareDataSourceNull()
    {
        $this->qtySold->prepareDataSource($this->dataSource);
    }
}
