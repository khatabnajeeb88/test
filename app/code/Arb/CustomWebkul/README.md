## Synopsis
This is module used for customize webkul marketplace module functionality and overriden layout and view files.

### functions
The Webkul Merchant Dashboard view is modified as per the first release, It can be modified in future for enabling the more features for Merchant like Customers, Transactions, Reviews, Profile edit, Product edits, Orders Edit etc. for now following features are enabled:
1. The merchant can only view product list, add product and edit option removed.
2. Updated label "Catalog" to "Catalogue".
3. Removed navigation menu other than Orders, Catalogue, and Mass upload.

### API List
1. Merchant list - It provides the list of all active merchants with there data and ability to add filter like topseller, pagination with page size and page number, and sorting capability

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).
4. Web API configuration details in [webapi.xml](etc/webapi.xml).

### Override Webkul Marketplace Obeservers to remove TAX from seller email.
1.  rewrite Webkul\Marketplace\Observer\SalesOrderPlaceAfterObserver by Arb\CustomWebkul\Observer\SalesOrderPlaceAfterObserver.
2.  rewrite Webkul\Marketplace\Observer\SalesOrderSuccessObserver by Arb\CustomWebkul\Observer\SalesOrderSuccessObserver.
3.  rewrite Webkul\Marketplace\Observer\SalesOrderInvoiceSaveAfterObserver by Arb\CustomWebkul\Observer\SalesOrderInvoiceSaveAfterObserver.

### Override module-catalog to disable qty field for Virtual product
1. Adding a block Arb\CustomWebkul\Block\Disable.php
2. Rewrite xml app\code\Arb\CustomWebkul\view\adminhtml\layout\catalog_product_new.xml
3. Adding a new js for disable qty app\code\Arb\CustomWebkul\view\adminhtml\templates\product\stock\disabler.phtml