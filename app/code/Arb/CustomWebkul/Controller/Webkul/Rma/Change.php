<?php
/**
 * Overridden Change Controller
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Controller\Webkul\Rma;

use Webkul\MpRmaSystem\Helper\Data;
use Webkul\MpRmaSystem\Controller\Rma\Change as WebkulChange;
use Magento\Framework\Controller\Result\Redirect;

/**
 * Overridden Change Controller
 */
class Change extends WebkulChange
{
    /**
     * Insert Decline Reason into data saved for RMA
     *
     * @return Redirect
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory
                ->create()
                ->setPath('*/seller/allrma');
        }

        $helper = $this->mpRmaHelper;
        $data = $this->getRequest()->getParams();
        $rmaData = [];
        $returnQty = false;
        $rmaId = $data['rma_id'];
        $sellerStatus = $data['seller_status'];
        
        if ($sellerStatus == Data::SELLER_STATUS_PENDING) {
            $rmaData['status'] = Data::RMA_STATUS_PENDING;
        } elseif ($sellerStatus == Data::SELLER_STATUS_PACKAGE_NOT_RECEIVED) {
            $rmaData['status'] = Data::RMA_STATUS_PENDING;
            $helper->sendRmaConfirmationEmail($data['rma_id']);
        } elseif ($sellerStatus == Data::SELLER_STATUS_PACKAGE_RECEIVED) {
            $rmaData['status'] = Data::RMA_STATUS_PROCESSING;
            $helper->sendRmaConfirmationEmail($data['rma_id']);
        } elseif ($sellerStatus == Data::SELLER_STATUS_PACKAGE_DISPATCHED) {
            $rmaData['status'] = Data::RMA_STATUS_PROCESSING;
            $helper->sendRmaConfirmationEmail($data['rma_id']);
        } elseif ($sellerStatus == Data::SELLER_STATUS_SOLVED) {
            $rmaData['status'] = Data::RMA_STATUS_SOLVED;
        } elseif ($sellerStatus == Data::SELLER_STATUS_ITEM_CANCELED) {
            $rmaData['status'] = Data::RMA_STATUS_SOLVED;
            $rmaData['final_status'] = Data::FINAL_STATUS_SOLVED;
            $returnQty = true;
        } else {
            $rmaData['status'] = Data::RMA_STATUS_DECLINED;
            $rmaData['final_status'] = Data::FINAL_STATUS_DECLINED;
            $rmaData['decline_reason'] = $data['decline-rma-reason'];
        }
        $rmaData['seller_status'] = $sellerStatus;
        
        $rma = $this->details->create()->load($rmaId);
        $rma->addData($rmaData)->setId($rmaId)->save();
        $helper->sendUpdateRmaEmail($data);

        if ($returnQty) {
            $helper->processCancellation($rmaId);
            $helper->updateRmaItemQtyStatus($rmaId);
        }

        return $this->resultRedirectFactory->create()
            ->setPath(
                '*/seller/rma',
                ['id' => $rmaId, 'back' => null, '_current' => true]
            );
    }
}
