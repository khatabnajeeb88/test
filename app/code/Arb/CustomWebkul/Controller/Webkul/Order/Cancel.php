<?php
/**
 * Customize 'Cancel' behaviour
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Controller\Webkul\Order;

use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Email\Sender\ShipmentSender;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;
use Webkul\Marketplace\Controller\Order\Cancel as ParentCancel;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersModel;
use Webkul\Marketplace\Model\Saleslist;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Model\SellerFactory as MpSellerModel;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Exception;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Sales\Api\InvoiceManagementInterface;
use Webkul\Marketplace\Model\Order\Pdf\Invoice;
use Webkul\Marketplace\Model\Order\Pdf\Creditmemo;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Magento\Framework\Registry;
use Magento\Customer\Model\Session as CustomerSession;
use Webkul\Marketplace\Helper\Orders;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;

class Cancel extends ParentCancel
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /** @var OrderStatusHistoryRepositoryInterface */
    private $orderStatusRepository;
 
    /** @var OrderRepositoryInterface */
    private $orderRepository;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param InvoiceSender $invoiceSender
     * @param ShipmentSender $shipmentSender
     * @param ShipmentFactory $shipmentFactory
     * @param Shipment $shipment
     * @param CreditmemoSender $creditmemoSender
     * @param CreditmemoRepositoryInterface $creditmemoRepository
     * @param CreditmemoFactory $creditmemoFactory
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param StockConfigurationInterface $stockConfiguration
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderManagementInterface $orderManagement
     * @param Registry $coreRegistry
     * @param CustomerSession $customerSession
     * @param Orders $orderHelper
     * @param NotificationHelper $notificationHelper
     * @param HelperData|null $helper
     * @param CreditmemoManagementInterface|null $creditmemoManagement
     * @param SaleslistFactory|null $saleslistFactory
     * @param CustomerUrl|null $customerUrl
     * @param DateTime|null $date
     * @param FileFactory|null $fileFactory
     * @param Creditmemo|null $creditmemoPdf
     * @param Invoice|null $invoicePdf
     * @param MpOrdersModel|null $mpOrdersModel
     * @param InvoiceCollection|null $invoiceCollection
     * @param InvoiceManagementInterface|null $invoiceManagement
     * @param ProductFactory|null $productModel
     * @param MpSellerModel|null $mpSellerModel
     * @param LoggerInterface|null $logger
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        InvoiceSender $invoiceSender,
        ShipmentSender $shipmentSender,
        ShipmentFactory $shipmentFactory,
        Shipment $shipment,
        CreditmemoSender $creditmemoSender,
        CreditmemoRepositoryInterface $creditmemoRepository,
        CreditmemoFactory $creditmemoFactory,
        InvoiceRepositoryInterface $invoiceRepository,
        StockConfigurationInterface $stockConfiguration,
        OrderRepositoryInterface $orderRepository,
        OrderManagementInterface $orderManagement,
        Registry $coreRegistry,
        CustomerSession $customerSession,
        Orders $orderHelper,
        NotificationHelper $notificationHelper,
        HelperData $helper = null,
        CreditmemoManagementInterface $creditmemoManagement = null,
        SaleslistFactory $saleslistFactory = null,
        CustomerUrl $customerUrl = null,
        DateTime $date = null,
        FileFactory $fileFactory = null,
        Creditmemo $creditmemoPdf = null,
        Invoice $invoicePdf = null,
        MpOrdersModel $mpOrdersModel = null,
        InvoiceCollection $invoiceCollection = null,
        InvoiceManagementInterface $invoiceManagement = null,
        ProductFactory $productModel = null,
        MpSellerModel $mpSellerModel = null,
        LoggerInterface $logger = null,
        ProductRepositoryInterface $productRepository,
        OrderStatusHistoryRepositoryInterface $orderStatusRepository
    ) {
        parent::__construct(
            $context,
            $resultPageFactory,
            $invoiceSender,
            $shipmentSender,
            $shipmentFactory,
            $shipment,
            $creditmemoSender,
            $creditmemoRepository,
            $creditmemoFactory,
            $invoiceRepository,
            $stockConfiguration,
            $orderRepository,
            $orderManagement,
            $coreRegistry,
            $customerSession,
            $orderHelper,
            $notificationHelper,
            $helper,
            $creditmemoManagement,
            $saleslistFactory,
            $customerUrl,
            $date,
            $fileFactory,
            $creditmemoPdf,
            $invoicePdf,
            $mpOrdersModel,
            $invoiceCollection,
            $invoiceManagement,
            $productModel,
            $mpSellerModel,
            $logger
        );
        $this->productRepository = $productRepository;
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * Overridden Cancel method to exclude virtual products from being canceled
     *
     * @return Redirect
     */
    public function execute()
    {
        $helper = $this->helper;
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            if ($order = $this->_initOrder()) {
                try {
                    $sellerId = $this->_customerSession->getCustomerId();
                    $flag = $this->orderHelper->cancelorder($order, $sellerId);
                    if ($flag) {
                        $orderId = $this->getRequest()->getParam('id');

                        $collection = $this->saleslistFactory->create()
                            ->getCollection()
                            ->addFieldToFilter(
                                'order_id',
                                ['eq' => $orderId]
                            )
                            ->addFieldToFilter(
                                'seller_id',
                                ['eq' => $sellerId]
                            );

                        $paidCanceledStatus = Saleslist::PAID_STATUS_CANCELED;
                        $paymentCode = '';
                        if ($order->getPayment()) {
                            $paymentCode = $order->getPayment()->getMethod();
                        }
                        $orderId = $this->getRequest()->getParam('id');

                        foreach ($collection as $saleproduct) {
                            $product = $this->productRepository->getById($saleproduct->getMageproductId());
                            if ($product->getTypeId() == 'virtual') {
                                continue;
                            }

                            $saleproduct->setCpprostatus(
                                $paidCanceledStatus
                            );
                            $saleproduct->setPaidStatus(
                                $paidCanceledStatus
                            );
                            if ($paymentCode == 'mpcashondelivery') {
                                $saleproduct->setCollectCodStatus(
                                    $paidCanceledStatus
                                );
                                $saleproduct->setAdminPayStatus(
                                    $paidCanceledStatus
                                );
                            }
                            $saleproduct->save();
                        }
                        $trackingcoll = $this->mpOrdersModel->create()
                            ->getCollection()
                            ->addFieldToFilter(
                                'order_id',
                                $orderId
                            )
                            ->addFieldToFilter(
                                'seller_id',
                                $sellerId
                            );
                        foreach ($trackingcoll as $tracking) {
                            $tracking->setTrackingNumber('canceled');
                            $tracking->setCarrierName('canceled');
                            $tracking->setIsCanceled(1);
                            $tracking->setOrderStatus('canceled');
                            $tracking->save();
                        }
                        $this->messageManager->addSuccess(
                            __('The order has been cancelled.')
                        );
                        $this->_eventManager->dispatch(
                            'mp_order_cancel_after',
                            ['seller_id' => $sellerId, 'order' => $order]
                        );
                    } else {
                        //For refund/rejected orders.
                        $orderId = $this->getRequest()->getParam('id');
                        $sellerOrderCollection = $this->mpOrdersModel->create()
                            ->getCollection()
                            ->addFieldToFilter(
                                'order_id',
                                $orderId
                            )
                            ->addFieldToFilter(
                                'seller_id',
                                $sellerId
                            );
                        /** @var Orders $mpOrder */
                foreach ($sellerOrderCollection->getItems() as $mpOrder) {
                    $mpOrder->setOrderStatus('canceled');
                    $mpOrder->setData('is_canceled', '1');
                    $mpOrder->setData('cancel_reason', $this->getRequest()->getParam('cancel_reason'));
                    $mpOrder->setData('carrier_name', 'canceled');
                    $mpOrder->setData('tracking_number', 'canceled');
                    try {
                        $message = __('The order has been cancelled.');
                        $this->messageManager->addSuccessMessage($message);
                        $mpOrder->save();
                        $mpOrderObj = $this->_orderRepository->get($mpOrder->getOrderId());
                        $this->_eventManager->dispatch(
                            'mp_order_cancel_after',
                            ['seller_id' => $sellerId, 'order' => $mpOrderObj]
                        );
                    } catch (Exception $e) {
                        $this->logger->error($e->getMessage());
                        $message = __('There has been an error while saving the Order.');
                        $this->messageManager->addErrorMessage($message);
                    }
                }
                $this->updateMagentoOrderStatus((int)$orderId, (int)$sellerId);
                    }
                } catch (LocalizedException $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (Exception $e) {
                    $this->helper->logDataInLogger(
                        "Controller_Order_Cancel execute : " . $e->getMessage()
                    );
                    $this->messageManager->addError(
                        __('We can\'t send the email order right now.')
                    );
                }
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/view',
                    [
                        'id' => $order->getEntityId(),
                        '_secure' => $this->getRequest()->isSecure(),
                    ]
                );
            } else {
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/history',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

     /**
     * Updating magento order status
     *
     * @param int $orderId
     * @param int $sellerId
     *
     * @return void
     */
    private function updateMagentoOrderStatus(int $orderId, int $sellerId)
    {
        $otherOrdersCollection = $this->mpOrdersModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            )->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            );
        if (empty($otherOrdersCollection->getItems())) {
            $order = $this->_orderRepository->get($orderId);
            $order->setState('canceled');
            $order->setStatus('canceled');
            $this->_orderRepository->save($order);
            return;
        }
        foreach ($otherOrdersCollection->getItems() as $order) {
            if ($order->getOrderStatus() !== 'canceled') {
                return;
            }
        }
        $order = $this->_orderRepository->get($orderId);
        $order->setState('canceled');
        $order->setStatus('canceled');
        $this->_orderRepository->save($order);
        $this->addCommentToOrder($orderId);
    }    
    /**
     * addCommentToOrder
     *
     * @param  mixed $orderId
     * @return void
     */
    public function addCommentToOrder(int $orderId)
    {
        $order = null;
        try {
            $order = $this->_orderRepository->get($orderId);
        } catch (NoSuchEntityException $exception) {
            $this->_logger->error($exception->getMessage());
        }
        $orderHistory = null;
        if ($order) {
            $comment = $order->addStatusHistoryComment(
                date('F d y h:i:s A').' Order Canceled #'.$orderId
            );
            try {
                $orderHistory = $this->orderStatusRepository->save($comment);
            } catch (\Exception $exception) {
                $this->_logger->critical($exception->getMessage());
            }
        }
        return $orderHistory;
    }
}
