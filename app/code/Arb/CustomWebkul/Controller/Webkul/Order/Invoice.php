<?php
/**
 * Customize 'Invoice' button status
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Controller\Webkul\Order;

use Magento\Framework\DB\Transaction;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Service\InvoiceService;
use Webkul\Marketplace\Controller\Order\Invoice as ParentController;
use Webkul\Customattribute\Plugin\Invoice as ParentInvoice;

/**
 * Insert custom 'shipped' status to be set on 'Invoice' button in Webkul Orders table
 * if the Order has already been shipped
 */
class Invoice extends ParentInvoice
{
    /**
     * update marketplace COD table
     * Changed status to be set after 'invoice' button if Order is shipped
     *
     *
     * @param [type] $invoiceId
     * @param [type] $orderId
     * @param [type] $sellerId
     * @param [type] $paymentCode
     * @return void
     */
    public function updateMpcodTable($invoiceId, $orderId, $sellerId, $paymentCode)
    {
        if ($invoiceId != '') {
            if ($paymentCode == 'mpcashondelivery') {
                $saleslistColl = $this->saleslistFactory->create()
                    ->getCollection()
                    ->addFieldToFilter(
                        'order_id',
                        $orderId
                    )
                    ->addFieldToFilter(
                        'seller_id',
                        $sellerId
                    );
                foreach ($saleslistColl as $saleslist) {
                    $saleslist->setCollectCodStatus(1);
                    $saleslist->save();
                }
            }

            $trackingcol1 = $this->mpOrdersModel->create()
                ->getCollection()
                ->addFieldToFilter(
                    'order_id',
                    $orderId
                )
                ->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
            foreach ($trackingcol1 as $row) {
                $row->setInvoiceId($invoiceId);
                if ($row->getShipmentId()) {
                    $row->setOrderStatus('shipped');
                } else {
                    $row->setOrderStatus('processing');
                }
                $row->save();
            }
        }
    }
}
