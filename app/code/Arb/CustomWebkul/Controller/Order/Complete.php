<?php
/**
 * Complete Order if User is a Merchant
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Controller\Order;

use Exception;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\Order;
use Webkul\Marketplace\Controller\Order as WebkulOrderController;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Email\Sender\ShipmentSender;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\Notification;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Model\SaleslistFactory;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Response\Http\FileFactory;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersModel;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;
use Webkul\Marketplace\Model\SellerFactory as MpSellerModel;
use Arb\CustomWebkul\Model\OrderCompleteEmailSender;
/**
 * Set Merchant Order status to Complete
 * Check if all Merchants Completed their Orders and if so then set Magento's Order Status to Complete
 */
class Complete extends WebkulOrderController
{
    private const COMPLETE_STATUS = 'complete';

    /**
     * @var InvoiceSender
     */
    protected $_invoiceSender;

    /**
     * @var ShipmentSender
     */
    protected $_shipmentSender;

    /**
     * @var ShipmentFactory
     */
    protected $_shipmentFactory;

    /**
     * @var Shipment
     */
    protected $_shipment;

    /**
     * @var CreditmemoSender
     */
    protected $_creditmemoSender;

    /**
     * @var CreditmemoRepositoryInterface;
     */
    protected $_creditmemoRepository;

    /**
     * @var CreditmemoFactory;
     */
    protected $_creditmemoFactory;

    /**
     * @var \Magento\Sales\Api\InvoiceRepositoryInterface
     */
    protected $_invoiceRepository;

    /**
     * @var StockConfigurationInterface
     */
    protected $_stockConfiguration;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var OrderManagementInterface
     */
    protected $_orderManagement;

    /**
     * @var \Webkul\Marketplace\Helper\Orders
     */
    protected $orderHelper;

    /**
     * @var NotificationHelper
     */
    protected $notificationHelper;

    /**
     * @var HelperData
     */
    protected $helper;
     
    /**
     * @var \Magento\Sales\Api\CreditmemoManagementInterface
     */
    protected $creditmemoManagement;
    
    /**
     * @var SaleslistFactory
     */
    protected $saleslistFactory;
    
    /**
     * @var CustomerUrl
     */
    protected $customerUrl;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var \Webkul\Marketplace\Model\Order\Pdf\Creditmemo
     */
    protected $creditmemoPdf;

    /**
     * @var \Webkul\Marketplace\Model\Order\Pdf\Invoice
     */
    protected $invoicePdf;

    /**
     * @var MpOrdersModel
     */
    protected $mpOrdersModel;

    /**
     * @var InvoiceCollection
     */
    protected $invoiceCollection;

    /**
     * @var \Magento\Sales\Api\InvoiceManagementInterface
     */
    protected $invoiceManagement;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productModel;

    /**
     * @var MpSellerModel
     */
    protected $mpSellerModel;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var OrderCompleteEmailSender
     */
    protected $completeOrderSender;

    public function __construct(
        OrderCompleteEmailSender $completeOrderSender,
        Context $context,
        PageFactory $resultPageFactory,
        InvoiceSender $invoiceSender,
        ShipmentSender $shipmentSender,
        ShipmentFactory $shipmentFactory,
        Shipment $shipment,
        CreditmemoSender $creditmemoSender,
        CreditmemoRepositoryInterface $creditmemoRepository,
        CreditmemoFactory $creditmemoFactory,
        \Magento\Sales\Api\InvoiceRepositoryInterface $invoiceRepository,
        StockConfigurationInterface $stockConfiguration,
        OrderRepositoryInterface $orderRepository,
        OrderManagementInterface $orderManagement,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Helper\Orders $orderHelper,
        NotificationHelper $notificationHelper,
        HelperData $helper = null,
        \Magento\Sales\Api\CreditmemoManagementInterface $creditmemoManagement = null,
        SaleslistFactory $saleslistFactory = null,
        CustomerUrl $customerUrl = null,
        DateTime $date = null,
        FileFactory $fileFactory = null,
        \Webkul\Marketplace\Model\Order\Pdf\Creditmemo $creditmemoPdf = null,
        \Webkul\Marketplace\Model\Order\Pdf\Invoice $invoicePdf = null,
        MpOrdersModel $mpOrdersModel = null,
        InvoiceCollection $invoiceCollection = null,
        \Magento\Sales\Api\InvoiceManagementInterface $invoiceManagement = null,
        \Magento\Catalog\Model\ProductFactory $productModel = null,
        MpSellerModel $mpSellerModel = null,
        \Psr\Log\LoggerInterface $logger = null
    ) {
        parent::__construct(
            $this->context = $context,
            $this->resultPageFactory = $resultPageFactory,
            $this->invoiceSender = $invoiceSender,
            $this->shipmentSender = $shipmentSender,
            $this->shipmentFactory = $shipmentFactory,
            $this->shipment = $shipment,
            $this->creditmemoSender = $creditmemoSender,
            $this->creditmemoRepository = $creditmemoRepository,
            $this->creditmemoFactory = $creditmemoFactory,
            $this->invoiceRepository = $invoiceRepository,
            $this->stockConfiguration = $stockConfiguration,
            $this->orderRepository = $orderRepository,
            $this->orderManagement = $orderManagement,
            $this->coreRegistry = $coreRegistry,
            $this->customerSession = $customerSession,
            $this->orderHelper = $orderHelper,
            $this->notificationHelper = $notificationHelper,
            $this->helper = $helper,
            $this->creditmemoManagement = $creditmemoManagement,
            $this->saleslistFactory = $saleslistFactory,
            $this->customerUrl = $customerUrl,
            $this->date = $date,
            $this->fileFactory = $fileFactory,
            $this->creditmemoPdf = $creditmemoPdf,
            $this->invoicePdf = $invoicePdf,
            $this->mpOrdersModel = $mpOrdersModel,
            $this->invoiceCollection = $invoiceCollection,
            $this->invoiceManagement = $invoiceManagement,
            $this->productModel = $productModel,
            $this->mpSellerModel = $mpSellerModel,
            $this->logger = $logger
        );
        
        $this->completeOrderSender = $completeOrderSender;
    }
     /*
     * @return ResultInterface
     */
    public function execute()
    {
        $isMerchant = $this->helper->isSeller();
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        if (!$isMerchant) {
            return $resultRedirect;
        }

        $orderId = $this->getRequest()->getParam('order_id');
        $sellerId = $this->_customerSession->getCustomerId();

        $sellerOrderCollection = $this->mpOrdersModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            )
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            );

        if ($sellerOrderCollection->getSize() < 1) {
            return $resultRedirect;
        }
        foreach ($sellerOrderCollection->getItems() as $mpOrder) {
            $mpOrder->setOrderStatus('complete');
            try {
                $mpOrder->save();
                $message = __('Order has been Completed.');
                $this->_eventManager->dispatch(
                    'arb_merchant_complete_order_success',
                    [
                        'order' => $mpOrder,
                        'seller' => $sellerId
                    ]
                );
                $this->messageManager->addSuccessMessage($message);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $message = __('There has been an error while saving the Order.');
                $this->messageManager->addErrorMessage($message);
            }
        }
        
        $senderInfo = [
            'name' => $this->_customerSession->getName(),
            'email' => $this->_customerSession->getCustomer()->getEmail(),
        ];

        
        $this->completeOrderSender->prepareCompleteOrderEmail($orderId, $sellerOrderCollection, $sellerId, $senderInfo);
        $this->updateMagentoOrderStatus((int)$orderId);

        return $resultRedirect;
    }

    /**
     * Check if all Merchants approved their Consignments then mark Order as Completed
     *
     * @param int $orderId
     *
     * @return void
     */
    private function updateMagentoOrderStatus(int $orderId)
    {
        $orderCollection = $this->mpOrdersModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            );

        foreach ($orderCollection->getItems() as $order) {
            if ($order->getOrderStatus() !== 'complete') {
                return;
            }
        }

        $order = $this->_orderRepository->get($orderId);
        $order->setState(Order::STATE_COMPLETE);
        $order->setStatus(self::COMPLETE_STATUS);
        $order->setCompletedAt(new \DateTime);
        $this->_orderRepository->save($order);
    }
}
