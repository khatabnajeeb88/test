<?php
/**
 * Custom Controller for Merchant Order Approval
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Controller\Order;

use Arb\CustomWebkul\Api\EmailTemplateRepositoryInterface;
use Arb\CustomWebkul\Helper\Email;
use Exception;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Email\Sender\ShipmentSender;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;
use Psr\Log\LoggerInterface;
use Webkul\Marketplace\Controller\Order as WebkulOrderController;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\Order\Pdf\Creditmemo;
use Webkul\Marketplace\Model\Order\Pdf\Invoice;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersModel;
use Webkul\Marketplace\Model\SellerFactory as MpSellerModel;
use Arb\CustomWebkul\Helper\EmailVariables;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Merchant Order Approval
 */
class Approve extends WebkulOrderController
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var InvoiceSender
     */
    private $invoiceSender;

    /**
     * @var ShipmentSender
     */
    private $shipmentSender;

    /**
     * @var ShipmentFactory
     */
    private $shipmentFactory;

    /**
     * @var Shipment
     */
    private $shipment;

    /**
     * @var CreditmemoSender
     */
    private $creditmemoSender;

    /**
     * @var CreditmemoRepositoryInterface
     */
    private $creditmemoRepository;

    /**
     * @var CreditmemoFactory
     */
    private $creditmemoFactory;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var StockConfigurationInterface
     */
    private $stockConfiguration;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderManagementInterface
     */
    private $orderManagement;

    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var EmailVariables
     */
    private $emailVariables;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CountryFactory
     */
    private $countryFactory;

    /**
     * @var Image
     */
    private $helperImage;

    /**
     * @var Data
     */
    private $pricingHelper;

    /**
     * Approve constructor.
     *
     * @param Email $email
     * @param EmailVariables $emailVariables
     * @param ProductRepositoryInterface $productRepository
     * @param StoreManagerInterface $storeManager
     * @param CountryFactory $countryFactory
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param InvoiceSender $invoiceSender
     * @param ShipmentSender $shipmentSender
     * @param ShipmentFactory $shipmentFactory
     * @param Shipment $shipment
     * @param CreditmemoSender $creditmemoSender
     * @param CreditmemoRepositoryInterface $creditmemoRepository
     * @param CreditmemoFactory $creditmemoFactory
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param StockConfigurationInterface $stockConfiguration
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderManagementInterface $orderManagement
     * @param Registry $coreRegistry
     * @param Session $customerSession
     * @param \Webkul\Marketplace\Helper\Orders $orderHelper
     * @param NotificationHelper $notificationHelper
     * @param Image $helperImage
     * @param Data $pricingHelper
     * @param HelperData|null $helper
     * @param CreditmemoManagementInterface|null $creditmemoManagement
     * @param SaleslistFactory|null $saleslistFactory
     * @param CustomerUrl|null $customerUrl
     * @param DateTime|null $date
     * @param FileFactory|null $fileFactory
     * @param Creditmemo|null $creditmemoPdf
     * @param Invoice|null $invoicePdf
     * @param MpOrdersModel|null $mpOrdersModel
     * @param InvoiceCollection|null $invoiceCollection
     * @param InvoiceManagementInterface|null $invoiceManagement
     * @param ProductFactory|null $productModel
     * @param MpSellerModel|null $mpSellerModel
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        Email $email,
        EmailVariables $emailVariables,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        CountryFactory $countryFactory,
        Context $context,
        PageFactory $resultPageFactory,
        InvoiceSender $invoiceSender,
        ShipmentSender $shipmentSender,
        ShipmentFactory $shipmentFactory,
        Shipment $shipment,
        CreditmemoSender $creditmemoSender,
        CreditmemoRepositoryInterface $creditmemoRepository,
        CreditmemoFactory $creditmemoFactory,
        InvoiceRepositoryInterface $invoiceRepository,
        StockConfigurationInterface $stockConfiguration,
        OrderRepositoryInterface $orderRepository,
        OrderManagementInterface $orderManagement,
        Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Helper\Orders $orderHelper,
        NotificationHelper $notificationHelper,
        Image $helperImage,
        Data $pricingHelper,
        HelperData $helper = null,
        CreditmemoManagementInterface $creditmemoManagement = null,
        SaleslistFactory $saleslistFactory = null,
        CustomerUrl $customerUrl = null,
        DateTime $date = null,
        FileFactory $fileFactory = null,
        Creditmemo $creditmemoPdf = null,
        Invoice $invoicePdf = null,
        MpOrdersModel $mpOrdersModel = null,
        InvoiceCollection $invoiceCollection = null,
        InvoiceManagementInterface $invoiceManagement = null,
        ProductFactory $productModel = null,
        MpSellerModel $mpSellerModel = null,
        LoggerInterface $logger = null
    ) {
        parent::__construct(
            $this->context = $context,
            $this->resultPageFactory = $resultPageFactory,
            $this->invoiceSender = $invoiceSender,
            $this->shipmentSender = $shipmentSender,
            $this->shipmentFactory = $shipmentFactory,
            $this->shipment = $shipment,
            $this->creditmemoSender = $creditmemoSender,
            $this->creditmemoRepository = $creditmemoRepository,
            $this->creditmemoFactory = $creditmemoFactory,
            $this->invoiceRepository = $invoiceRepository,
            $this->stockConfiguration = $stockConfiguration,
            $this->orderRepository = $orderRepository,
            $this->orderManagement = $orderManagement,
            $this->coreRegistry = $coreRegistry,
            $this->customerSession = $customerSession,
            $this->orderHelper = $orderHelper,
            $this->notificationHelper = $notificationHelper,
            $this->helper = $helper,
            $this->creditmemoManagement = $creditmemoManagement,
            $this->saleslistFactory = $saleslistFactory,
            $this->customerUrl = $customerUrl,
            $this->date = $date,
            $this->fileFactory = $fileFactory,
            $this->creditmemoPdf = $creditmemoPdf,
            $this->invoicePdf = $invoicePdf,
            $this->mpOrdersModel = $mpOrdersModel,
            $this->invoiceCollection = $invoiceCollection,
            $this->invoiceManagement = $invoiceManagement,
            $this->productModel = $productModel,
            $this->mpSellerModel = $mpSellerModel,
            $this->logger = $logger
        );

        $this->email = $email;
        $this->emailVariables = $emailVariables;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->countryFactory = $countryFactory;
        $this->helperImage = $helperImage;
        $this->pricingHelper = $pricingHelper;
    }

    /**
     * Approve Order if User is Merchant
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $message = '';
        $isMerchant = $this->helper->isSeller();
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        if (!$isMerchant) {
            return $resultRedirect;
        }

        $isViewOrderPage = $this->getRequest()->getParam('view_page');
        $orderId = $this->getRequest()->getParam('order_id');
        $sellerId = $this->_customerSession->getCustomerId();

        $mpOrderCollectionForSeller = $this->mpOrdersModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            )
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            );

        if ($mpOrderCollectionForSeller->getSize() < 1) {
            return $resultRedirect;
        }

        /** @var Orders $mpOrder */
        foreach ($mpOrderCollectionForSeller->getItems() as $mpOrder) {
            $mpOrder->setIsApproved(1);
            $mpOrder->setOrderStatus('processing');

            try {
                $order = $this->orderRepository->get($orderId);
                $sellerId = $this->customerSession->getCustomerId();

                $emailTemplateOrderVariables =
                    $this->emailVariables->prepareEmailTemplateOrderVariables($order, (int)$sellerId, (int)$orderId);

                $emailTemplateApproveVariables = $this->prepareEmailTemplateApproveVariables($order);

                $emailTemplateVariables = array_merge($emailTemplateOrderVariables, $emailTemplateApproveVariables);

                $emailTemplateVariables['order_details'] = $this->prepareOrderDetails($sellerId, $order);
                $emailTemplateVariables['shipping_fees'] =
                    $this->pricingHelper->currency($mpOrder->getData('shipping_charges'), true, false);
                $emailTemplateVariables['grand_total'] =
                    $this->pricingHelper->currency($emailTemplateVariables['total_amount'], true, false);

                $emailTemplateVariables['total_amount'] -= $mpOrder->getData('shipping_charges');
                $emailTemplateVariables['total_amount'] =
                    $this->pricingHelper->currency($emailTemplateVariables['total_amount'], true, false);

                // send email
                $this->sendApproveEmail($emailTemplateVariables, $order);

                $mpOrder->save();
                $message = __('Order has been Approved.');
                $this->_eventManager->dispatch(
                    'arb_merchant_approve_order_success',
                    [
                        'order' => $mpOrder,
                        'seller' => $sellerId
                    ]
                );
                $this->messageManager->addSuccessMessage($message);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $message = __('There has been an error while saving the Order.');
                $this->messageManager->addErrorMessage($message);
            }
        }

        if ($isViewOrderPage) {
            return $resultRedirect;
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        return $resultJson->setData(['message' => $message]);
    }

    /**
     * Preparing custom variables for email template
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    private function prepareEmailTemplateApproveVariables(OrderInterface $order)
    {
        $deliveryAddress = 'not provided';
        $customerMobile = 'not provided';

        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== null) {
            $countryId = $shippingAddress->getCountryId();
            $country = $this->countryFactory->create()->loadByCode($countryId);

            $countryName = $country->getName() ?: '';
            $region = $shippingAddress->getRegion() ?: '';
            $postCode = $shippingAddress->getPostCode() ?: '';
            $city = $shippingAddress->getCity() ?: '';
            $street = $shippingAddress->getData('street') ?: '';

            $address = [
                $countryName,
                $region,
                $postCode,
                $city,
                $street
            ];

            $deliveryAddress = implode(', ', array_filter($address));
            $customerMobile = $shippingAddress->getTelephone() ?? 'not provided';
        }

        $approveVariables = [
            'delivery_address' => $deliveryAddress,
            'customer_mobile' => $customerMobile,
        ];

        return $approveVariables;
    }

    /**
     * Setting receiver and sander, sending email
     *
     * @param array $emailTemplateVariables
     * @param OrderInterface $order
     *
     * @throws NoSuchEntityException
     */
    private function sendApproveEmail(array $emailTemplateVariables, OrderInterface $order)
    {
        $senderInfo = [
            'name' => $this->customerSession->getName(),
            'email' => $this->customerSession->getCustomer()->getEmail(),
        ];

        $receiverInfo = [
            'name' => $order->getCustomerFirstname(),
            'email' => $order->getCustomerEmail(),
        ];

        $emailTemplateVariables['store_id'] = $order->getStoreId();
        $emailTemplateVariables['email_content_alignment'] = 'left';
        $emailTemplateVariables['email_content_direction'] = 'ltr';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $emailTemplateVariables['email_content_alignment'] = 'right';
            $emailTemplateVariables['email_content_direction'] = 'rtl';
        }

        $this->email->sendOrderApproveMail($emailTemplateVariables, $senderInfo, $receiverInfo);
    }

    /**
     * Preparing order details table for email template
     *
     * @param $sellerId
     * @param $orderId
     *
     * @return string
     * @throws NoSuchEntityException
     */
    private function prepareOrderDetails($sellerId, $order)
    {
        $html = '';
        $orderId = $order->getId();
        $salesCollection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $orderId
            );

        $items = $salesCollection->getItems();
        $alignment = 'left';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $alignment = 'right';
        }

        foreach ($items as $item) {
            $parentItem = $item->getData('parent_item_id');
            if ($parentItem !== null) {
                continue;
            }

            $productName = $item->getData('magepro_name');
            $productPrice = $item->getData('magequantity') . ' x ' . round($item->getData('magepro_price'), 2);

            $product = $this->productRepository->getById($item->getData('mageproduct_id'));

            $img = $this->helperImage->init(
                $product,
                'product_thumbnail_image'
            )->getUrl();

            $html .=
                '<tr>' .
                '<td align="' . $alignment . '"><img style="max-width:200px;" alt="product_image" src="' . $img . '"/></td>' .
                '<td align="' . $alignment . '">' . $productName . '</td>' .
                '<td align="' . $alignment . '" dir="ltr">' . $productPrice . '</td>' .
                '</tr>';
        }

        return $html;
    }
}
