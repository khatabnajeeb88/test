<?php
/**
 * Custom Controller for Merchant Order Rejection
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Controller\Order;

use Exception;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\Registry;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Psr\Log\LoggerInterface;
use Webkul\Marketplace\Controller\Order as WebkulOrderController;
use Webkul\Marketplace\Helper\Orders as HelperOrders;
use Webkul\Marketplace\Model\Order\Pdf\Creditmemo;
use Webkul\Marketplace\Model\Order\Pdf\Invoice;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\Saleslist;
use Webkul\Marketplace\Model\SaleslistFactory;
use Arb\CustomWebkul\Helper\EmailVariables;
use Arb\CustomWebkul\Helper\Email;
use Webkul\Marketplace\Helper\Data as HelperData;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Email\Sender\ShipmentSender;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersModel;
use Webkul\Marketplace\Model\SellerFactory as MpSellerModel;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;

/**
 * Setting Merchant Order status to Closed
 * Setting Magento Order status if this is only consignment in order
 * Sending an email to customer and admin
 */
class Reject extends WebkulOrderController
{
    /**
     * @var Email
     */
    private $email;

    /**
     * @var EmailVariables
     */
    private $emailVariables;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var InvoiceSender
     */
    private $invoiceSender;

    /**
     * @var ShipmentSender
     */
    private $shipmentSender;

    /**
     * @var ShipmentFactory
     */
    private $shipmentFactory;

    /**
     * @var Shipment
     */
    private $shipment;

    /**
     * @var CreditmemoSender
     */
    private $creditmemoSender;

    /**
     * @var CreditmemoRepositoryInterface
     */
    private $creditmemoRepository;

    /**
     * @var CreditmemoFactory
     */
    private $creditmemoFactory;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var StockConfigurationInterface
     */
    private $stockConfiguration;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderManagementInterface
     */
    private $orderManagement;

    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var Data
     */
    private $pricingHelper;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Reject constructor.
     *
     * @param Email $email
     * @param EmailVariables $emailVariables
     * @param HelperData $helperData
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param InvoiceSender $invoiceSender
     * @param ShipmentSender $shipmentSender
     * @param ShipmentFactory $shipmentFactory
     * @param Shipment $shipment
     * @param CreditmemoSender $creditmemoSender
     * @param CreditmemoRepositoryInterface $creditmemoRepository
     * @param CreditmemoFactory $creditmemoFactory
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param StockConfigurationInterface $stockConfiguration
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderManagementInterface $orderManagement
     * @param Registry $coreRegistry
     * @param Session $customerSession
     * @param HelperOrders $orderHelper
     * @param NotificationHelper $notificationHelper
     * @param Data $pricingHelper
     * @param HelperData|null $helper
     * @param CreditmemoManagementInterface|null $creditmemoManagement
     * @param SaleslistFactory|null $saleslistFactory
     * @param CustomerUrl|null $customerUrl
     * @param DateTime|null $date
     * @param FileFactory|null $fileFactory
     * @param Creditmemo|null $creditmemoPdf
     * @param Invoice|null $invoicePdf
     * @param MpOrdersModel|null $mpOrdersModel
     * @param InvoiceCollection|null $invoiceCollection
     * @param InvoiceManagementInterface|null $invoiceManagement
     * @param ProductFactory|null $productModel
     * @param MpSellerModel|null $mpSellerModel
     * @param LoggerInterface|null $logger
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Email $email,
        EmailVariables $emailVariables,
        HelperData $helperData,
        Context $context,
        PageFactory $resultPageFactory,
        InvoiceSender $invoiceSender,
        ShipmentSender $shipmentSender,
        ShipmentFactory $shipmentFactory,
        Shipment $shipment,
        CreditmemoSender $creditmemoSender,
        CreditmemoRepositoryInterface $creditmemoRepository,
        CreditmemoFactory $creditmemoFactory,
        InvoiceRepositoryInterface $invoiceRepository,
        StockConfigurationInterface $stockConfiguration,
        OrderRepositoryInterface $orderRepository,
        OrderManagementInterface $orderManagement,
        Registry $coreRegistry,
        Session $customerSession,
        HelperOrders $orderHelper,
        NotificationHelper $notificationHelper,
        Data $pricingHelper,
        HelperData $helper = null,
        CreditmemoManagementInterface $creditmemoManagement = null,
        SaleslistFactory $saleslistFactory = null,
        CustomerUrl $customerUrl = null,
        DateTime $date = null,
        FileFactory $fileFactory = null,
        Creditmemo $creditmemoPdf = null,
        Invoice $invoicePdf = null,
        MpOrdersModel $mpOrdersModel = null,
        InvoiceCollection $invoiceCollection = null,
        InvoiceManagementInterface $invoiceManagement = null,
        ProductFactory $productModel = null,
        MpSellerModel $mpSellerModel = null,
        LoggerInterface $logger = null,
        ProductRepositoryInterface $productRepository,
        OrderStatusHistoryRepositoryInterface $orderStatusRepository
    ) {
        parent::__construct(
            $this->context = $context,
            $this->resultPageFactory = $resultPageFactory,
            $this->invoiceSender = $invoiceSender,
            $this->shipmentSender = $shipmentSender,
            $this->shipmentFactory = $shipmentFactory,
            $this->shipment = $shipment,
            $this->creditmemoSender = $creditmemoSender,
            $this->creditmemoRepository = $creditmemoRepository,
            $this->creditmemoFactory = $creditmemoFactory,
            $this->invoiceRepository = $invoiceRepository,
            $this->stockConfiguration = $stockConfiguration,
            $this->orderRepository = $orderRepository,
            $this->orderManagement = $orderManagement,
            $this->coreRegistry = $coreRegistry,
            $this->customerSession = $customerSession,
            $this->orderHelper = $orderHelper,
            $this->notificationHelper = $notificationHelper,
            $this->helper = $helper,
            $this->creditmemoManagement = $creditmemoManagement,
            $this->saleslistFactory = $saleslistFactory,
            $this->customerUrl = $customerUrl,
            $this->date = $date,
            $this->fileFactory = $fileFactory,
            $this->creditmemoPdf = $creditmemoPdf,
            $this->invoicePdf = $invoicePdf,
            $this->mpOrdersModel = $mpOrdersModel,
            $this->invoiceCollection = $invoiceCollection,
            $this->invoiceManagement = $invoiceManagement,
            $this->productModel = $productModel,
            $this->mpSellerModel = $mpSellerModel,
            $this->logger = $logger
        );

        $this->email = $email;
        $this->emailVariables = $emailVariables;
        $this->helperData = $helperData;
        $this->pricingHelper = $pricingHelper;
        $this->productRepository = $productRepository;
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * Reject Order if User is a Merchant
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $isMerchant = $this->helper->isSeller();
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        if (!$isMerchant) {
            return $resultRedirect;
        }

        $orderId = $this->getRequest()->getParam('id');
        $sellerId = $this->_customerSession->getCustomerId();
        $order = $this->_initOrder();

        $sellerOrderCollection = $this->mpOrdersModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            )
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            );

        if (!$order || $sellerOrderCollection->getSize() < 1) {
            return $resultRedirect;
        }

        $flag = $this->orderHelper->cancelorder($order, $sellerId);
        if ($flag) {
            $collection = $this->saleslistFactory->create()
                ->getCollection()
                ->addFieldToFilter(
                    'order_id',
                    ['eq' => $orderId]
                )
                ->addFieldToFilter(
                    'seller_id',
                    ['eq' => $sellerId]
                );

            $paidCanceledStatus = Saleslist::PAID_STATUS_CANCELED;
            $paymentCode = '';
            if ($order->getPayment()) {
                $paymentCode = $order->getPayment()->getMethod();
            }
            $orderId = $this->getRequest()->getParam('id');

            foreach ($collection as $saleproduct) {
                $product = $this->productRepository->getById($saleproduct->getMageproductId());
                if ($product->getTypeId() == 'virtual') {
                    continue;
                }

                $saleproduct->setCpprostatus(
                    $paidCanceledStatus
                );
                $saleproduct->setPaidStatus(
                    $paidCanceledStatus
                );
                if ($paymentCode == 'mpcashondelivery') {
                    $saleproduct->setCollectCodStatus(
                        $paidCanceledStatus
                    );
                    $saleproduct->setAdminPayStatus(
                        $paidCanceledStatus
                    );
                }
                $saleproduct->save();
            }
        }

        /** @var Orders $mpOrder */
        foreach ($sellerOrderCollection->getItems() as $mpOrder) {
            $mpOrder->setOrderStatus('canceled');
            $mpOrder->setData('is_canceled', '1');
            $mpOrder->setData('cancel_reason', 'Rejected by Merchant');
            $mpOrder->setData('carrier_name', 'canceled');
            $mpOrder->setData('tracking_number', 'canceled');
            try {
                $order = $this->orderRepository->get($orderId);
                $emailTemplateVariables =
                    $this->emailVariables->prepareEmailTemplateOrderVariables($order, (int)$sellerId, (int)$orderId);
                $emailTemplateVariables['total_amount'] =
                    $this->pricingHelper->currency($emailTemplateVariables['total_amount'], true, false);

                // send email
                $this->sendRejectEmails($emailTemplateVariables, $order);
                $message = __('Order has been Rejected.');
                $this->messageManager->addSuccessMessage($message);
                $mpOrder->save();
                $this->_eventManager->dispatch(
                    'arb_order_rejected',
                    [
                        'order' => $mpOrder,
                        'seller_id' => $sellerId
                    ]
                );
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $message = __('There has been an error while saving the Order.');
                $this->messageManager->addErrorMessage($message);
            }
        }

        $this->updateMagentoOrderStatus((int)$orderId, (int)$sellerId);

        return $resultRedirect;
    }

    /**
     * Updating magento order status
     *
     * @param int $orderId
     * @param int $sellerId
     *
     * @return void
     */
    private function updateMagentoOrderStatus(int $orderId, int $sellerId)
    {
        $otherOrdersCollection = $this->mpOrdersModel->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            )->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            );

        if (empty($otherOrdersCollection->getItems())) {
            $order = $this->_orderRepository->get($orderId);
            $order->setState('canceled');
            $order->setStatus('canceled');
            $this->_orderRepository->save($order);

            return;
        }

        foreach ($otherOrdersCollection->getItems() as $order) {
            if ($order->getOrderStatus() !== 'canceled') {
                return;
            }
        }

        $order = $this->_orderRepository->get($orderId);
        $order->setState('canceled');
        $order->setStatus('canceled');
        $this->_orderRepository->save($order);
        $this->addCommentToOrder($orderId);
    }    
  
    /**
     * addCommentToOrder
     *
     * @param  mixed $orderId
     * @return void
     */
    public function addCommentToOrder(int $orderId)
    {
        $order = null;
        try {
            $order = $this->_orderRepository->get($orderId);
        } catch (NoSuchEntityException $exception) {
            $this->_logger->error($exception->getMessage());
        }
        $orderHistory = null;
        if ($order) {
            $comment = $order->addStatusHistoryComment(
                date('F d y h:i:s A').' Order Canceled #'.$orderId
            );
            try {
                $orderHistory = $this->orderStatusRepository->save($comment);
            } catch (\Exception $exception) {
                $this->_logger->critical($exception->getMessage());
            }
        }
        return $orderHistory;
    }

    /**
     * Sending an email to customer and admin
     *
     * @param array $emailTemplateVariables
     * @param OrderInterface $order
     */
    private function sendRejectEmails(array $emailTemplateVariables, OrderInterface $order)
    {
        $senderInfo = [
            'name' => $this->customerSession->getName(),
            'email' => $this->customerSession->getCustomer()->getEmail(),
        ];

        // send email to customer
        $receiverInfo = [
            'name' => $order->getCustomerFirstname(),
            'email' => $order->getCustomerEmail(),
        ];

        $emailTemplateVariables['store_id'] = $order->getStoreId();
        $emailTemplateVariables['email_content_alignment'] = 'left';
        $emailTemplateVariables['email_content_direction'] = 'ltr';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $emailTemplateVariables['email_content_alignment'] = 'right';
            $emailTemplateVariables['email_content_direction'] = 'rtl';
        }

        $this->email->sendOrderRejectMail($emailTemplateVariables, $senderInfo, $receiverInfo);

        // send email to admin
        $receiverInfo = [
            'name' => $this->helperData->getAdminName(),
            'email' => $this->helperData->getAdminEmailId(),
        ];

        $this->email->sendOrderRejectMail($emailTemplateVariables, $senderInfo, $receiverInfo);
    }
}
