<?php

namespace Arb\CustomWebkul\Controller\Export;

use Magento\Framework\App\Action\Action;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Index extends Action
{
    
    protected $uploaderFactory;

    protected $_locationFactory; 

    public function __construct(
        Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Webkul\Mpreportsystem\Block\Mpreport $locationFactory // This is returns Collaction of Data

    ) {
       parent::__construct($context);
       $this->_fileFactory = $fileFactory;
       $this->_locationFactory = $locationFactory;
       $this->directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA); // VAR Directory Path
       parent::__construct($context);
    }

    public function execute()
    {   
        $data    = $_POST["data"];
        $data    = json_decode("$data", true);
        
        if (empty($data['orderstatus'])) {
            unset($data['orderstatus']);
        }
        if (empty($data['categories'])) {
            unset($data['categories']);
        }
        
        $name = date('m-d-Y-H-i-s');
        $filepath = 'export/export-data-' .$name. '.csv'; // at Directory path Create a Folder Export and FIle
        $this->directory->create('export');

        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();

        //column name dispay in your CSV 

        $columns = ['Date','Total Orders','Total Items Sold','Revenues',];

            foreach ($columns as $column) 
            {
                $header[] = $column; //storecolumn in Header array
            }

        $stream->writeCsv($header);

        $salesCollection = $this->_locationFactory->getSalesCollection($data);
         
        foreach($salesCollection as $salesData){
            
            $itemData = [];

            // column name must same as in your Database Table 
            $itemData[] = $salesData->getOrderDate();
            $itemData[] = $salesData->getTotalOrderId();
            $itemData[] = $salesData->getTotalItemQty();
            $itemData[] = $salesData->getTotalSellerAmount();

            $stream->writeCsv($itemData);

        }

        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder
        print_r('pub/'.DirectoryList::MEDIA.'/'.$filepath);
        die();
    }
}
