<?php

namespace Arb\CustomWebkul\Controller\Adminhtml\Reasons;

use Magento\Backend\App\Action;

class Save extends \Webkul\MpRmaSystem\Controller\Adminhtml\Reasons\Save
{
    /**
     * @var \Webkul\MpRmaSystem\Model\ReasonsFactory
     */
    protected $reasons;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Webkul\MpRmaSystem\Model\ReasonsFactory $reasons
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Webkul\MpRmaSystem\Model\ReasonsFactory $reasons
    ) {
        $this->reasons = $reasons;
        parent::__construct($context,$reasons);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpRmaSystem::reasons');
    }

    /**
     * Save action.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            $data['store_id'] = implode(',', $this->getRequest()->getParam('store_id'));
            $id = (int) $this->getRequest()->getParam('id');
            //$storeId = implode(',', $this->getRequest()->getParam('store_id'));

            $resultRedirect = $this->resultRedirectFactory->create();
            $model = $this->reasons->create();
            if ($id) {
                $model->addData($data)->setId($id)->save();
                $this->messageManager->addSuccess(__('Reason edited successfully.'));
            } else {
                $model->setData($data)->save();
                $this->messageManager->addSuccess(__('Reason saved successfully.'));
            }
        } else {
            $error = 'There was some error while processing your request.';
            $this->messageManager->addError(__($error));
            return $resultRedirect->setPath('*/*/');
        }

        return $resultRedirect->setPath('*/*/');
    }
}
