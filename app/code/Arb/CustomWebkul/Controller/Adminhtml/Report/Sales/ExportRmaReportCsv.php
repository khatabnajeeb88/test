<?php
/**
 * Export RMA CSV Controller
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Controller\Adminhtml\Report\Sales;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Reports\Controller\Adminhtml\Report\Sales;
use Arb\CustomWebkul\Block\Adminhtml\Sales\Rmareport\Grid;
use Exception;
use Magento\Framework\Controller\ResultInterface;

class ExportRMAReportCsv extends Sales
{
    /**
     * @return ResponseInterface|ResultInterface
     *
     * @throws Exception
     */
    public function execute()
    {
        $fileName = 'merchant_rma_report.csv';
        $grid = $this->_view->getLayout()->createBlock(Grid::class);
        $this->_initReportAction($grid);

        return $this->_fileFactory->create($fileName, $grid->getCsvFile(), DirectoryList::VAR_DIR);
    }
}
