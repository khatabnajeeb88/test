<?php

/**
 * Sales RMA report 
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Controller\Adminhtml\Report\Sales;

use Arb\CustomWebkul\Model\Flag;
use Magento\Reports\Controller\Adminhtml\Report\Sales;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Backend\Helper\Data as BackendHelper;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class RmaReport extends Sales
{
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Stdlib\DateTime\Filter\DateTime $dateFilter,
        TimezoneInterface $timezone,
        BackendHelper $backendHelperData
    ) {

        $this->_fileFactory = $fileFactory;
        $this->_dateFilter = $dateFilter;
        $this->timezone = $timezone;
        $this->backendHelper = $backendHelperData ?: $this->_objectManager->get(\Magento\Backend\Helper\Data::class);
        parent::__construct($context, $fileFactory, $dateFilter, $timezone);
    }
    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $this->_showLastExecutionTime(Flag::REPORT_RMAREPORT_FLAG_CODE, 'rmareport');

        $this->_initAction()->_setActiveMenu(
            'Arb_CustomWebkul::report_rma'
        )->_addBreadcrumb(
            __('Merchant RMA Report'),
            __('Merchant RMA Report')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('RMA Report'));

        $gridBlock = $this->_view->getLayout()->getBlock('adminhtml_sales_rmareport.grid');
        $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form');

        $this->_initReportAction([$gridBlock, $filterFormBlock]);

        $this->_view->renderLayout();
    }
        
    /**
     * _initReportAction
     *
     * @param  mixed $blocks
     * @return void
     */
    public function _initReportAction($blocks)
    {
        if (!is_array($blocks)) {
            $blocks = [$blocks];
        }

        $params = $this->initFilterData();

        foreach ($blocks as $block) {
            if ($block) {
                $block->setPeriodType($params->getData('period_type'));
                $block->setFilterData($params);
            }
        }

        return $this;
    }
        
    /**
     * initFilterData
     *
     * @return Magento\Framework\DataObject
     */
    private function initFilterData(): \Magento\Framework\DataObject
    {
        $requestData = $this->backendHelper
            ->prepareFilterString(
                $this->getRequest()->getParam('filter')
            );

        $filterRules = ['from' => $this->_dateFilter, 'to' => $this->_dateFilter];

        $inputFilter = new \Zend_Filter_Input($filterRules, [], $requestData);

        $requestData = $inputFilter->getUnescaped();
        $requestData['store_ids'] = $this->getRequest()->getParam('store_ids');
        $requestData['group'] = $this->getRequest()->getParam('group');
        $requestData['website'] = $this->getRequest()->getParam('website');

        $params = new \Magento\Framework\DataObject();

        foreach ($requestData as $key => $value) {
            if (!empty($value)) {
                $params->setData($key, $value);
            }
        }
        return $params;
    }
}
