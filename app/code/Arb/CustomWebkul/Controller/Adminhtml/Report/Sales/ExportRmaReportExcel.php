<?php
/**
 * Export RMA Excel Controller
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Controller\Adminhtml\Report\Sales;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultInterface;
use Magento\Reports\Controller\Adminhtml\Report\Sales;
use Arb\CustomWebkul\Block\Adminhtml\Sales\Rmareport\Grid;
use Exception;

class ExportRmaReportExcel extends Sales
{
    /**
     * @return ResponseInterface|ResultInterface
     *
     * @throws Exception
     */
    public function execute()
    {
        $fileName = 'merchant_rma_report.xml';
        $grid = $this->_view->getLayout()->createBlock(Grid::class);
        $this->_initReportAction($grid);

        return $this->_fileFactory->create($fileName, $grid->getExcelFile($fileName), DirectoryList::VAR_DIR);
    }
}
