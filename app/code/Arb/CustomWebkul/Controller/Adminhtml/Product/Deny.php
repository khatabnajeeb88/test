<?php
/**
 * Override Controller for sending deny email
 *
 * @category Arb
 * @package Arb_CustomWebkul
 * @author Arb Magento Team
 */

namespace Arb\CustomWebkul\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;
use Webkul\Marketplace\Controller\Adminhtml\Product\Deny as ParentDeny;
use Magento\Backend\Model\View\Result\Redirect;
use Exception;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Webkul\Marketplace\Model\Product;
use Webkul\Marketplace\Model\Notification;

class Deny extends ParentDeny
{
    /**
     * Inserted Product model into email variables array
     *
     * @return Redirect
     *
     * @throws Exception
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $collection = $this->productModel->create()
            ->getCollection()
            ->addFieldToFilter('mageproduct_id', $data['mageproduct_id'])
            ->addFieldToFilter('seller_id', $data['seller_id']);

        if ($collection->getSize()) {
            $productIds = [$data['mageproduct_id']];
            $allStores = $this->_storeManager->getStores();
            $status = Status::STATUS_DISABLED;
            $sellerProductStatus = Product::STATUS_DENIED;
            $sellerProduct = $this->productModel->create()->getCollection();
            $coditionData = "`mageproduct_id`=" . $data['mageproduct_id'];

            $sellerProduct->setProductData(
                $coditionData,
                ['status' => $sellerProductStatus, 'seller_pending_notification' => 1]
            );

            foreach ($allStores as $eachStoreId => $storeId) {
                $this->productAction->updateAttributes($productIds, ['status' => $status], $storeId);
            }

            $this->productAction->updateAttributes($productIds, ['status' => $status], 0);
            $this->_productPriceIndexerProcessor->reindexList($productIds);
            $catagoryModel = $this->categoryFactory->create();
            $id = 0;

            foreach ($collection as $item) {
                $id = $item->getId();
                $this->notificationHelper->saveNotification(
                    Notification::TYPE_PRODUCT,
                    $id,
                    $data['mageproduct_id']
                );
            }

            $model = $this->productFactory->create()->load($data['mageproduct_id']);
            $catarray = $model->getCategoryIds();
            $categoryname = '';

            foreach ($catarray as $keycat) {
                $categoriesy = $catagoryModel->load($keycat);
                if ($categoryname == '') {
                    $categoryname = $categoriesy->getName();
                } else {
                    $categoryname = $categoryname . ',' . $categoriesy->getName();
                }
            }

            $allStores = $this->_storeManager->getStores();
            $pro = $this->productModel->create()->load($id);
            $seller = $this->customerModel->create()->load($data['seller_id']);

            if (isset($data['notify_seller']) && $data['notify_seller'] == 1) {
                $helper = $this->mpHelper;
                $adminStoreEmail = $helper->getAdminEmailId();
                $adminEmail = $adminStoreEmail ? $adminStoreEmail : $helper->getDefaultTransEmailId();
                $adminUsername = $helper->getAdminName();

                $emailTemplateVariables['myvar1'] = $seller->getName();
                $emailTemplateVariables['myvar2'] = $data['product_deny_reason'];
                $emailTemplateVariables['myvar3'] = $model->getName();
                $emailTemplateVariables['myvar4'] = $categoryname;
                $emailTemplateVariables['myvar5'] = $model->getDescription();
                $emailTemplateVariables['myvar6'] = $model->getPrice();
                $emailTemplateVariables['merchant_store_id'] = $seller->getData('store_id');
                $emailTemplateVariables['product'] = $model;
                $senderInfo = [
                    'name' => $adminUsername,
                    'email' => $adminEmail,
                ];

                $receiverInfo = [
                    'name' => $seller->getName(),
                    'email' => $seller->getEmail(),
                ];

                $this->mpEmailHelper->sendProductDenyMail(
                    $emailTemplateVariables,
                    $senderInfo,
                    $receiverInfo
                );
            }

            $this->_eventManager->dispatch(
                'mp_deny_product',
                ['product' => $pro, 'seller' => $seller]
            );

            $this->messageManager->addSuccess(__('Product has been Denied.'));
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
