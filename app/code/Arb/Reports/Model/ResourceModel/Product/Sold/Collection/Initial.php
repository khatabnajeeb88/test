<?php
/**
 * @category Arb
 * @package Arb_Reports
 * @author Arb Magento Team
 *
 */

/**
 * Report Reviews collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
namespace Arb\Reports\Model\ResourceModel\Product\Sold\Collection;

/**
 * @api
 * @since 100.0.2
 */

class Initial extends \Arb\Reports\Model\ResourceModel\Report\Collection
{
    /**
     * Report sub-collection class name
     *
     * @var string
     */
    protected $_reportCollection = \Magento\Reports\Model\ResourceModel\Product\Sold\Collection::class;
}
