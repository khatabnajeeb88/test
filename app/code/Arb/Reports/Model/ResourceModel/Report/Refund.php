<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Report Reviews collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */

namespace Arb\Reports\Model\ResourceModel\Report;

/**
 * Class Collection
 *
 * @api
 * @since 100.0.2
 */
class Refund extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('marketplace_saleslist', 'entity_id');
    }
}
