<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */


namespace Arb\Reports\Model\ResourceModel;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Data\Collection;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DataObject;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Inventory\Model\ResourceModel\Source;
use Magento\Inventory\Model\ResourceModel\SourceItem as SourceItemResourceModel;
use Magento\Inventory\Model\SourceItem as SourceItemModel;
use Magento\InventoryApi\Api\Data\SourceInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryConfigurationApi\Model\GetAllowedProductTypesForSourceItemManagementInterface;
use Magento\InventoryLowQuantityNotificationApi\Api\Data\SourceItemConfigurationInterface;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class LowQuantityCollection extends AbstractCollection
{
    /**
     * @var StockConfigurationInterface
     */
    private $stockConfiguration;

    /**
     * @var GetAllowedProductTypesForSourceItemManagementInterface
     */
    private $getAllowedProductTypesForSourceItemManagement;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * @var int
     */
    private $filterStoreId;

    /**
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param AttributeRepositoryInterface $attributeRepository
     * @param StockConfigurationInterface $stockConfiguration
     * @param GetAllowedProductTypesForSourceItemManagementInterface $getAllowedProductTypesForSourceItemManagement
     * @param MetadataPool $metadataPool
     * @param AdapterInterface|null $connection
     * @param AbstractDb|null $resource
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        AttributeRepositoryInterface $attributeRepository,
        StockConfigurationInterface $stockConfiguration,
        GetAllowedProductTypesForSourceItemManagementInterface $getAllowedProductTypesForSourceItemManagement,
        MetadataPool $metadataPool,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $connection,
            $resource
        );

        $this->attributeRepository = $attributeRepository;
        $this->stockConfiguration = $stockConfiguration;
        $this->getAllowedProductTypesForSourceItemManagement = $getAllowedProductTypesForSourceItemManagement;
        $this->metadataPool = $metadataPool;
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(SourceItemModel::class, SourceItemResourceModel::class);

        $this->addFilterToMap('source_code', 'main_table.source_code');
        $this->addFilterToMap('sku', 'main_table.sku');
        $this->addFilterToMap('product_name', 'product_entity_varchar.value');
        $this->addFilterToMap('seller_id', 'mp_product_entity.seller_id');            // custom
        $this->addFilterToMap('category', 'category_entity_varchar.row_id');    // custom
    }

    /**
     * @param int $storeId
     * @return void
     */
    public function addStoreFilter(int $storeId)
    {
        $this->filterStoreId = $storeId;
    }

    /**
     * @codeCoverageIgnore
     *
     * @inheritdoc
     */
    protected function _renderFilters()
    {
        if (false === $this->_isFiltersRendered) {
            $this->joinInventoryConfiguration();
            $this->joinCatalogProduct();
            $this->joinMarketplaceProduct();    // custom
            $this->joinCategory();              // custom

            $this->addProductTypeFilter();
            $this->addNotifyStockQtyFilter();
            $this->addEnabledSourceFilter();
            $this->addSourceItemInStockFilter();
        }
        return parent::_renderFilters();
    }

    /**
     * @codeCoverageIgnore
     *
     * @inheritdoc
     */
    protected function _renderOrders()
    {
        if (false === $this->_isOrdersRendered) {
            $this->setOrder(SourceItemInterface::QUANTITY, static::SORT_ORDER_ASC);
        }
        return parent::_renderOrders();
    }

    /**
     * joinCatalogProduct depends on dynamic condition 'filterStoreId'
     *
     * @return void
     * @throws NoSuchEntityException
     */
    private function joinCatalogProduct()
    {
        $productEntityTable = $this->getTable('catalog_product_entity');
        $productEavVarcharTable = $this->getTable('catalog_product_entity_varchar');
        $nameAttribute = $this->attributeRepository->get('catalog_product', 'name');

        $metadata = $this->metadataPool->getMetadata(ProductInterface::class);
        $linkField = $metadata->getLinkField();

        $this->getSelect()->join(
            ['product_entity' => $productEntityTable],
            'main_table.' . SourceItemInterface::SKU . ' = product_entity.' . ProductInterface::SKU,
            []
        );

        $this->getSelect()->joinInner(
            ['product_entity_varchar' => $productEavVarcharTable],
            'product_entity_varchar.' . $linkField . ' = product_entity.' . $linkField . ' ' .
            'AND product_entity_varchar.store_id = ' . Store::DEFAULT_STORE_ID. ' ' .
            'AND product_entity_varchar.attribute_id = ' . (int)$nameAttribute->getAttributeId(),
            []
        );

        if (null !== $this->filterStoreId) {
            $this->getSelect()->joinLeft(
                ['product_entity_varchar_store' => $productEavVarcharTable],
                'product_entity_varchar_store.' . $linkField . ' = product_entity.' . $linkField . ' ' .
                'AND product_entity_varchar_store.store_id = ' . (int)$this->filterStoreId . ' ' .
                'AND product_entity_varchar_store.attribute_id = ' . (int)$nameAttribute->getAttributeId(),
                [
                    'product_name' => $this->getConnection()->getIfNullSql(
                        'product_entity_varchar_store.value',
                        'product_entity_varchar.value'
                    )
                ]
            );
        } else {
            $this->getSelect()->columns(['product_name' => 'product_entity_varchar.value']);
        }
    }

    /**
     * @return void
     */
    private function joinInventoryConfiguration()
    {
        $sourceItemConfigurationTable = $this->getTable('inventory_low_stock_notification_configuration');

        $this->getSelect()->joinInner(
            ['notification_configuration' => $sourceItemConfigurationTable],
            sprintf(
                'main_table.%s = notification_configuration.%s AND main_table.%s = notification_configuration.%s',
                SourceItemInterface::SKU,
                SourceItemConfigurationInterface::SKU,
                SourceItemInterface::SOURCE_CODE,
                SourceItemConfigurationInterface::SOURCE_CODE
            ),
            []
        );
    }

    /**
     * @return void
     */
    private function addProductTypeFilter()
    {
        $this->addFieldToFilter(
            'product_entity.type_id',
            $this->getAllowedProductTypesForSourceItemManagement->execute()
        );
    }

    /**
     * @return void
     */
    private function addNotifyStockQtyFilter()
    {
        $notifyStockExpression = $this->getConnection()->getIfNullSql(
            'notification_configuration.' . SourceItemConfigurationInterface::INVENTORY_NOTIFY_QTY,
            (float)$this->stockConfiguration->getNotifyStockQty()
        );

        $this->getSelect()->where(
            SourceItemInterface::QUANTITY . ' < ?',
            $notifyStockExpression
        );
    }

    /**
     * @return void
     */
    private function addEnabledSourceFilter()
    {
        $this->getSelect()->joinInner(
            ['inventory_source' => $this->getTable(Source::TABLE_NAME_SOURCE)],
            sprintf(
                'inventory_source.%s = 1 AND inventory_source.%s = main_table.%s',
                SourceInterface::ENABLED,
                SourceInterface::SOURCE_CODE,
                SourceItemInterface::SOURCE_CODE
            ),
            []
        );
    }

    /**
     * @return void
     */
    private function addSourceItemInStockFilter()
    {
        $this->addFieldToFilter('main_table.status', SourceItemInterface::STATUS_IN_STOCK);
    }

    /**
     * joining marketplace product
     */
    private function joinMarketplaceProduct()
    {
        $mpProductEntityTable = $this->getTable('marketplace_product');
        $customerEntityTable = $this->getTable('customer_entity');

        $this->getSelect()->join(
            [
                'mp_product_entity' => $mpProductEntityTable
            ],
            'mp_product_entity.mageproduct_id = product_entity.entity_id',
            []
        );

        $this->getSelect()->joinInner(
            [
                'customer_entity' => $customerEntityTable
            ],
            'mp_product_entity.seller_id = customer_entity.entity_id',
            [
                'seller_id' => "CONCAT(customer_entity.firstname, ' ', customer_entity.lastname)"
            ]
        );
    }

    /**
     * joining product category
     */
    private function joinCategory()
    {
        $categoryProductTable = $this->getTable('catalog_category_product');
        $catalogEntityVarcharTable = $this->getTable('catalog_category_entity_varchar');
        $categoryNameAttribute = $this->attributeRepository->get('catalog_category', 'name');

        $this->getSelect()->joinInner(
            [
                'category_product' => $categoryProductTable,
            ],
            'category_product.product_id = product_entity.entity_id ',
            [
                'category' => 'category_entity_varchar.value',
            ]
        )->join(
            ['category_entity_varchar' => $catalogEntityVarcharTable],
            'category_entity_varchar.row_id = category_product.category_id ' .
            'AND category_entity_varchar.store_id = ' . Store::DEFAULT_STORE_ID . ' ' .
            'AND category_entity_varchar.attribute_id = ' . $categoryNameAttribute->getAttributeId()
        );
    }

    /**
     * Adding item to item array
     * overridden parent method because Item (Magento\Inventory\Model\SourceItem) with the same ID  already exists
     * is thrown when one product has more than one category assigned
     *
     * @param DataObject $item
     * @return $this
     * @throws Exception
     */
    public function addItem(DataObject $item)
    {
        $itemId = $this->_getItemId($item);
        if ($itemId !== null) {
            $this->_items[] = $item;
        } else {
            $this->_addItem($item);
        }
        return $this;
    }
}
