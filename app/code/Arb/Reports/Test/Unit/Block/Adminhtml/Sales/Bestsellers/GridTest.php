<?php
/**
 * This file consist of PHPUnit test case for class Grid
 *
 * @category Arb
 * @package Arb_Reports
 * @author Arb Magento Team
 *
 */

namespace Arb\Reports\Test\Unit\Block\Adminhtml\Sales\Bestsellers;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\Reports\Block\Adminhtml\Sales\Bestsellers\Grid;
use \Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection;

/**
 * @covers Arb\Reports\Block\Adminhtml\Sales\Bestsellers\Grid
 */
class GridTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Grid
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->testObject = $objectManager->getObject(Grid::class, []);
    }
  
    /**
     * testGetResourceCollectionName
     *
     * @return void
     */
    public function testGetResourceCollectionName()
    {
        $getResourceCollectionName = $this->testObject->getResourceCollectionName();
        $this->assertEquals(Collection::class, $getResourceCollectionName);
        $this->assertInternalType('string', $getResourceCollectionName);
    }
}