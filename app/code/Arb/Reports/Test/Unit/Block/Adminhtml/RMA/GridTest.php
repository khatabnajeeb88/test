<?php
/**
 * This file consist of PHPUnit test case for class Grid
 *
 * @category Arb
 * @package Arb_Reports
 * @author Arb Magento Team
 *
 */

namespace Arb\Reports\Test\Unit\Block\Adminhtml\RMA;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\Reports\Block\Adminhtml\RMA\Grid;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\DataObject;
/**
 * @covers Arb\Reports\Block\Adminhtml\RMA\Grid
 */
class GridTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Grid
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->request = $this->createMock(\Magento\Framework\App\RequestInterface::class);

        $writeInterface = $this->createMock(\Magento\Framework\Filesystem\Directory\WriteInterface::class);
        $this->filesystem = $this->createMock(\Magento\Framework\Filesystem::class);
        $this->filesystem
            ->method('getDirectoryWrite')
            ->with($this->equalTo(DirectoryList::VAR_DIR))
            ->will($this->returnValue($writeInterface));

        $this->urlBuilder = $this->getMockForAbstractClass(\Magento\Framework\UrlInterface::class, [], '', false);

        //$this->context = $this->createMock(\Magento\Backend\Block\Template\Context::class);
        $this->context = $this->getMockBuilder(\Magento\Backend\Block\Template\Context::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->context
            ->expects($this->once())
            ->method('getUrlBuilder')
            ->will($this->returnValue($this->urlBuilder));
        $this->context
            ->expects($this->once())
            ->method('getFilesystem')
            ->will($this->returnValue($this->filesystem));
        $this->context
            ->expects($this->once())
            ->method('getRequest')
            ->will($this->returnValue($this->request));

        $this->helper = $this->createMock(\Magento\Backend\Helper\Data::class);
        $this->collectionFactoryMock = $this->createMock(\Magento\Rma\Model\ResourceModel\Rma\Grid\CollectionFactory::class);
        $this->rmaFactoryMock = $this->createMock(\Magento\Rma\Model\RmaFactory::class);
        // $this->rmaFactoryMock = $this->createPartialMock(
        //     \Magento\Rma\Model\RmaFactory::class,
        //     ['create']
        // );
        $this->testObject = $objectManager->getObject(Grid::class, [
            'context' => $this->context,
            'backendHelper' => $this->helper,
            'collectionFactory' => $this->collectionFactoryMock,
            'rmaFactory' => $this->rmaFactoryMock
        ]);

        // $this->model = new \Arb\Reports\Block\Adminhtml\RMA\Grid(
        //     $this->context,
        //     $this->helper,
        //     $this->collectionFactoryMock,
        //     $this->rmaFactoryMock
        // );
    }

  
    /**
     * testGetRowUrl
     *
     * 
     */
    public function testGetRowUrl()
    {
        $this->testObject->setId(1);

        $object = new DataObject(['id' => 1]);

        $this->urlBuilder
            ->expects($this->any())
            ->method('getUrl')
            ->with('*/*/edit', ['id' => $this->testObject->getId()])
            ->willReturn('http://some_url');
        $this->testObject->_construct();
        $this->assertContains('http://some_url', (string)$this->testObject->getRowUrl($object));
        $this->assertEquals('http://some_url', (string)$this->testObject->getRowUrl($object));
    }
}