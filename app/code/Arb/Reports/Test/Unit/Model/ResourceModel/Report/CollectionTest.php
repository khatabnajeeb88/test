<?php

/**
 * This file consist of PHPUnit test case for Collection
 *
 * @category Arb
 * @package Arb_Reports
 * @author Arb Magento Team
 *
 */

namespace Arb\Reports\Test\Unit\Model\ResourceModel\Report;

use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Reports\Model\ResourceModel\Report\Collection;
use BadMethodCallException;
use Magento\Framework\Data\Collection\EntityFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Reports\Model\ResourceModel\Report\Collection\Factory;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;

/**
 * @covers Arb\Reports\Model\ResourceModel\Report\Collection
 */
class CollectionTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|EntityFactory
     */
    private $entityFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Collection
     */
    private $testedObject;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $this->entityFactoryMock = $this->createMock(EntityFactory::class);
        $this->_localeDate = $this->createMock(TimezoneInterface::class);
        $this->_collectionFactory = $this->createMock(Factory::class);
        $this->mpCollectionFactory = $this->createMock(CollectionFactory::class);
        $this->productCollectionFactory = $this->createMock(ProductCollection::class);
        $this->testedObject = new Collection(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
    }

    /**
     * testSetPeriod
     *
     * @return void
     */
    public function testSetPeriod()
    {
        $period = 1;
        $this->period = $this->testedObject->setPeriod($period);
        $this->assertInstanceOf(Collection::class, $this->period);
    }

    /**
     * testSetInterval
     *
     * @return void
     */
    public function testSetInterval()
    {
        $fromDate = new \DateTime('now');
        $toDate = new \DateTime('now');
        $this->duration = $this->testedObject->setInterval($fromDate, $toDate);
        $this->assertInstanceOf(Collection::class, $this->duration);
    }
    /**
     * testGetIntervalEmpty
     *
     * @return void
     */
    public function testGetIntervalEmpty()
    {
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $this->duration = $testObject->_getIntervals();
        $this->assertEmpty($this->duration);
    }
    /**
     * testGetIntervalNoPeriod
     *
     * @return void
     */
    public function testGetIntervalNoPeriod()
    {
        $fromDate = new \DateTime('now');
        $toDate = new \DateTime('now');
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $reflectionClass = new \ReflectionClass('Arb\Reports\Model\ResourceModel\Report\Collection');
        $reflectionProperty = $reflectionClass->getProperty('_from');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, $fromDate);
        $reflectionProperty = $reflectionClass->getProperty('_to');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, $toDate);
        $this->duration = $testObject->_getIntervals();
        $this->assertEmpty($this->duration);
    }
    /**
     * testGetIntervalDay
     *
     * @return void
     */
    // public function testGetIntervalDay()
    // {
    //     $fromDate = new \DateTime('now');
    //     $toDate = new \DateTime('now');
    //     $testObject = new ExampleExposed(
    //         $this->entityFactoryMock,
    //         $this->_localeDate,
    //         $this->_collectionFactory,
    //         $this->mpCollectionFactory,
    //         $this->productCollectionFactory
    //     );
    //     $reflectionClass = new \ReflectionClass('Arb\Reports\Model\ResourceModel\Report\Collection');
    //     $reflectionProperty = $reflectionClass->getProperty('_from');
    //     $reflectionProperty->setAccessible(true);
    //     $reflectionProperty->setValue($testObject, $fromDate);
    //     $reflectionProperty = $reflectionClass->getProperty('_to');
    //     $reflectionProperty->setAccessible(true);
    //     $reflectionProperty->setValue($testObject, $toDate);
    //     $reflectionProperty = $reflectionClass->getProperty('_period');
    //     $reflectionProperty->setAccessible(true);
    //     $reflectionProperty->setValue($testObject, 'day');
    //     $this->duration = $testObject->_getIntervals();
    //     $this->assertArrayHasKey('', $this->duration);
    // }
    /**
     * testGetIntervalMonth
     *
     * @return void
     */
    public function testGetIntervalMonth()
    {
        $fromDate = new \DateTime('now');
        $toDate = new \DateTime('now');
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $reflectionClass = new \ReflectionClass('Arb\Reports\Model\ResourceModel\Report\Collection');
        $reflectionProperty = $reflectionClass->getProperty('_from');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, $fromDate);
        $reflectionProperty = $reflectionClass->getProperty('_to');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, $toDate);
        $reflectionProperty = $reflectionClass->getProperty('_period');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, 'month');
        $this->duration = $testObject->_getIntervals();
        $this->assertArrayHasKey($fromDate->format('m') . '/' . $fromDate->format('Y'), $this->duration);
    }
    /**
     * testGetIntervalYear
     *
     * @return void
     */
    public function testGetIntervalYear()
    {
        $fromDate = new \DateTime('now');
        $toDate = new \DateTime('now');
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $reflectionClass = new \ReflectionClass('Arb\Reports\Model\ResourceModel\Report\Collection');
        $reflectionProperty = $reflectionClass->getProperty('_from');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, $fromDate);
        $reflectionProperty = $reflectionClass->getProperty('_to');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, $toDate);
        $reflectionProperty = $reflectionClass->getProperty('_period');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, 'year');
        $this->duration = $testObject->_getIntervals();
        $this->assertArrayHasKey($fromDate->format('Y'), $this->duration);
    }
    /**
     * testGetDayInterval
     *
     * @return void
     */
    // public function testGetDayInterval()
    // {
    //     $dateStart = new \DateTime('now');
    //     $testObject = new ExampleExposed(
    //         $this->entityFactoryMock,
    //         $this->_localeDate,
    //         $this->_collectionFactory,
    //         $this->mpCollectionFactory,
    //         $this->productCollectionFactory
    //     );
    //     $this->interval = $testObject->_getDayInterval($dateStart);
    //     $this->assertArrayHasKey('period', $this->interval);
    //     $this->assertArrayHasKey('start', $this->interval);
    //     $this->assertArrayHasKey('end', $this->interval);
    // }
    /**
     * testGetMonthInterval
     *
     * @return void
     */
    public function testGetMonthInterval()
    {
        $dateStart = new \DateTime('now');
        $dateEnd = new \DateTime('now');
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $this->interval = $testObject->_getMonthInterval($dateStart, $dateEnd, 1);
        // var_dump($this->interval);die;
        $this->assertArrayHasKey('period', $this->interval);
        $this->assertArrayHasKey('start', $this->interval);
        $this->assertArrayHasKey('end', $this->interval);
        $dateAssert = new \DateTime('now');
        $this->assertEquals($dateAssert->format('m') . '/' . $dateAssert->format('Y'), $this->interval['period']);
    }
    /**
     * testGetMonthIntervalNoInterval
     *
     * @return void
     */
    public function testGetMonthIntervalNoInterval()
    {
        $dateStart = new \DateTime('now');
        $dateEnd = new \DateTime('now');
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $this->interval = $testObject->_getMonthInterval($dateStart, $dateEnd, 0);
        $this->assertArrayHasKey('period', $this->interval);
        $this->assertArrayHasKey('start', $this->interval);
        $this->assertArrayHasKey('end', $this->interval);
        $dateAssert = new \DateTime('now');
        $this->assertEquals($dateAssert->format('m') . '/' . $dateAssert->format('Y'), $this->interval['period']);
    }
    /**
     * testGetMonthIntervalIntervalNotZero
     *
     * @return void
     */
    public function testGetMonthIntervalIntervalNotZero()
    {
        $dateStart = new \DateTime('now');
        $dateEnd = new \DateTime('now');
        $dateEnd->modify('+1 month');
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $this->interval = $testObject->_getMonthInterval($dateStart, $dateEnd, 0);
        $this->assertArrayHasKey('period', $this->interval);
        $this->assertArrayHasKey('start', $this->interval);
        $this->assertArrayHasKey('end', $this->interval);
        $dateAssert = new \DateTime('now');
        $this->assertEquals($dateAssert->format('m') . '/' . $dateAssert->format('Y'), $this->interval['period']);
    }
    /**
     * testGetYearInterval
     *
     * @return void
     */
    public function testGetYearInterval()
    {
        $dateStart = new \DateTime('now');
        $dateEnd = new \DateTime('now');
        $testObject = new ExampleExposed(
            $this->entityFactoryMock,
            $this->_localeDate,
            $this->_collectionFactory,
            $this->mpCollectionFactory,
            $this->productCollectionFactory
        );
        $this->interval = $testObject->_getYearInterval($dateStart, $dateEnd, 1);
        $this->assertArrayHasKey('period', $this->interval);
        $this->assertArrayHasKey('start', $this->interval);
        $this->assertArrayHasKey('end', $this->interval);
        $dateAssert = new \DateTime('now');
        $this->assertEquals($dateAssert->format('Y'), $this->interval['period']);
    }
    /**
     * testGetPeriods
     *
     * @return void
     */
    public function testGetPeriods()
    {
        $this->period = $this->testedObject->getPeriods();
        $this->assertArrayHasKey('day', $this->period);
        $this->assertArrayHasKey('month', $this->period);
        $this->assertArrayHasKey('year', $this->period);
    }
        
    /**
     * testSetStoreIds
     *
     * @return void
     */
    public function testSetStoreIds()
    {
        $ids = [1,2];
        $this->stores = $this->testedObject->setStoreIds($ids);
        $this->assertInstanceOf(Collection::class, $this->stores);
    }
    
    /**
     * testGetStoreIds
     *
     * @return void
     */
    public function testGetStoreIds()
    {
        $reflectionClass = new \ReflectionClass('Arb\Reports\Model\ResourceModel\Report\Collection');
        $reflectionProperty = $reflectionClass->getProperty('_storeIds');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->testedObject, [1,2,3]);
        $this->_storeIds = $this->testedObject->getStoreIds();
        $this->assertEquals([1,2,3], $this->_storeIds);
    }    
    /**
     * testGetSize
     *
     * @return void
     */
    public function testGetSize()
    {
        $this->size = $this->testedObject->getSize();
        $this->assertEquals(0, $this->size);
    }    
    /**
     * testSetPageSize
     *
     * @return void
     */
    public function testSetPageSize()
    {
        $this->pageSize = $this->testedObject->setPageSize(2);
        $this->assertInstanceOf(Collection::class, $this->pageSize);
    }    
    /**
     * testGetPageSize
     *
     * @return void
     */
    public function testGetPageSize()
    {
        $this->size = $this->testedObject->getPageSize();
        $this->assertEmpty($this->size);
    }    
}
/**
 * ExampleExposed
 */
class ExampleExposed extends Collection
{
    public function __call($method, array $args = array())
    {
        if (!method_exists($this, $method))
            throw new BadMethodCallException("method '$method' does not exist");
        return call_user_func_array(array($this, $method), $args);
    }
}
