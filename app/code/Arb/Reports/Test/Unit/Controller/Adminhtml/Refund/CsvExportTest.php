<?php
/**
 * IndexTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Reports
 * @author Arb Magento Team
 *
 */
namespace Arb\Reports\Test\Unit\Controller\Adminhtml\Refund;

use Arb\Reports\Controller\Adminhtml\Refund\CsvExport;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Model\Export\ConvertToCsv;
use Magento\Framework\App\Response\Http\FileFactory;
use Arb\Reports\Model\ResourceModel\Report\Refund\CollectionFactory;
use Magento\Framework\Api\Search\DocumentInterface;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Filesystem\Directory\WriteInterface as DirectoryWriteInterface;
use Magento\Framework\Filesystem\File\WriteInterface as FileWriteInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Ui\Model\Export\MetadataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Magento\Framework\DataObject;
/**
 * Class CsvExportTest for testing CsvExport class
 * @covers \Arb\Reports\Controller\Adminhtml\Refund\CsvExport
 */
class CsvExportTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var CsvExport
     */
    protected $controller;

    /**
     * @var \Magento\Backend\App\Action\Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Backend\Model\View\Result\Page|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultPage;

    /**
     * Main setup method
     * @SuppressWarnings(PHPMD.LongVariable)
     */
    protected function setUp()
    {
        $this->context = $this->getMockBuilder(\Magento\Backend\App\Action\Context::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->forwardFactory = $this->getMockBuilder(\Magento\Backend\Model\View\Result\ForwardFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->filter = $this->getMockBuilder(Filter::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->filesystem = $this->getMockBuilder(Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->convertToCsv = $this->getMockBuilder(ConvertToCsv::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->fileFactory = $this->getMockBuilder(FileFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->metadataProvider = $this->getMockBuilder(\Magento\Ui\Model\Export\MetadataProvider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->refund = $this->getMockBuilder(\Arb\Reports\Model\ResourceModel\Report\Refund::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->collectionFactory =  $this->createPartialMock(
            CollectionFactory::class,
            ['create','getIterator']
        );
        $this->refundCollectionMock = $this->createMock(\Arb\Reports\Model\ResourceModel\Report\Refund\Collection::class, ['getIterator']);

        $this->directory = $this->getMockBuilder(\Magento\Framework\Filesystem\Directory\WriteInterface::class)
            ->getMockForAbstractClass();

        $this->filesystem = $this->getMockBuilder(Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->filesystem->expects($this->any())
            ->method('getDirectoryWrite')
            ->with(DirectoryList::VAR_DIR)
            ->willReturn($this->directory);

        $this->metadataProvider = $this->getMockBuilder(MetadataProvider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->component = $this->getMockBuilder(UiComponentInterface::class)
            ->getMockForAbstractClass();
            $this->filter->expects($this->once())
            ->method('getComponent')
            ->willReturn($this->component);
        $this->filter->expects($this->once())
            ->method('prepareComponent')
            ->with($this->component)
            ->willReturnSelf();
        $this->stream = $this->getMockBuilder(\Magento\Framework\Filesystem\File\WriteInterface::class)
            ->setMethods([
                'lock',
                'unlock',
                'close',
            ])
            ->getMockForAbstractClass();

        $this->controller = new CsvExport(
            $this->context,
            $this->forwardFactory,
            $this->filter,
            $this->filesystem,
            $this->convertToCsv,
            $this->fileFactory,
            $this->metadataProvider,
            $this->refund,
            $this->collectionFactory
        );
    }

    
    /**
     * testExecute method
     */
    public function testExecute()
    {
        $object = new DataObject(['id' => 1]);
        $this->collectionFactory->expects($this->once())->method('create')->willReturn($this->refundCollectionMock);

        $this->filter->expects($this->once())
            ->method('getCollection')
            ->with($this->refundCollectionMock)
            ->willReturn($this->refundCollectionMock);
       $this->refundCollectionMock->expects($this->any())->method('getIterator')->willReturn(new \ArrayIterator($object));
       $componentName = 'component_name';
        $data = ['data_value'];

        $document = $this->getMockBuilder(DocumentInterface::class)
            ->getMockForAbstractClass();

        $this->mockComponent($componentName, [$document]);
        $this->mockFilter();
        $this->mockDirectory();

        $this->stream->expects($this->once())
            ->method('lock')
            ->willReturnSelf();
        $this->stream->expects($this->once())
            ->method('unlock')
            ->willReturnSelf();
        $this->stream->expects($this->once())
            ->method('close')
            ->willReturnSelf();
        $this->stream->expects($this->any())
            ->method('writeCsv')
            ->with($data)
            ->willReturnSelf();

        $this->metadataProvider->expects($this->any())
            ->method('getOptions')
            ->willReturn([]);
        $this->metadataProvider->expects($this->any())
            ->method('getHeaders')
            ->with($this->component)
            ->willReturn($data);
        $this->metadataProvider->expects($this->any())
            ->method('getFields')
            ->with($this->component)
            ->willReturn([]);
        $this->metadataProvider->expects($this->any())
            ->method('getRowData')
            ->with($document, [], [])
            ->willReturn($data);
        $this->metadataProvider->expects($this->any())
            ->method('convertDate')
            ->with($document, $componentName);

       // $this->assertEquals($componentName, $this->controller->execute());
         $result = $this->controller->execute();
        // $this->assertIsArray($result);
        // $this->assertArrayHasKey('type', $result);
        // $this->assertArrayHasKey('value', $result);
        // $this->assertArrayHasKey('rm', $result);
        // $this->assertStringContainsString($componentName, $result['value']);
        // $this->assertStringContainsString('.csv', $result['value']);
    }

    /**
     * @param string $componentName
     * @param array $items
     */
    protected function mockComponent($componentName, $items)
    {
        $context = $this->getMockBuilder(ContextInterface::class)
            ->setMethods(['getDataProvider'])
            ->getMockForAbstractClass();

        $dataProvider = $this->getMockBuilder(
            DataProviderInterface::class
        )->setMethods(['getSearchResult','getData'])
            ->getMockForAbstractClass();

        $searchResult = $this->getMockBuilder(SearchResultInterface::class)
            ->setMethods(['getItems'])
            ->getMockForAbstractClass();

        $searchCriteria = $this->getMockBuilder(SearchCriteriaInterface::class)
            ->setMethods(['setPageSize', 'setCurrentPage'])
            ->getMockForAbstractClass();
        $this->component->expects($this->any())
            ->method('getName')
            ->willReturn($componentName);
        $this->component->method('getContext')
            ->willReturn($context);

        $context->method('getDataProvider')
            ->willReturn($dataProvider);

        $dataProvider->expects($this->any())
            ->method('getData')
            ->willReturn([]);

        $dataProvider->expects($this->any())
            ->method('getSearchResult')
            ->willReturn([$searchResult]);

        $dataProvider->expects($this->any())
            ->method('getSearchCriteria')
            ->willReturn($searchCriteria);

        $searchResult->expects($this->any())
            ->method('getItems')
            ->willReturn($items);

        $searchResult->expects($this->any())
            ->method('getTotalCount')
            ->willReturn(1);

        $searchCriteria->expects($this->any())
            ->method('setCurrentPage')
            ->willReturnSelf();

        $searchCriteria->expects($this->any())
            ->method('setPageSize')
            ->with(200)
            ->willReturnSelf();
    }

    protected function mockFilter()
    {
        $this->filter->expects($this->any())
            ->method('getComponent')
            ->willReturn($this->component);
        $this->filter->expects($this->any())
            ->method('prepareComponent')
            ->with($this->component)
            ->willReturnSelf();
        $this->filter->expects($this->any())
            ->method('applySelectionOnTargetProvider')
            ->willReturnSelf();
    }

    protected function mockDirectory()
    {
        $this->directory->expects($this->once())
            ->method('create')
            ->with('export')
            ->willReturnSelf();
        $this->directory->expects($this->once())
            ->method('openFile')
            ->willReturn($this->stream);
    }
}
