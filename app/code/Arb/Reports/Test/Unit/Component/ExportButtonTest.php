<?php
/**
 * ExportButtonTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Reports
 * @author Arb Magento Team
 *
 */
namespace Arb\Reports\Test\Unit\Component;

use Arb\Reports\Component\ExportButton;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
/**
 * Class ExportButtonTest for testing  ExportButton class
 * @covers \Arb\Reports\Component\ExportButton
 */
class ExportButtonTest extends \PHPUnit\Framework\TestCase
{
	protected function setUp()
    {
        $this->context = $this->getMockBuilder(\Magento\Framework\View\Element\UiComponent\ContextInterface::class)
            ->getMockForAbstractClass();
        $this->objectManager = new ObjectManager($this);

        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->model = $this->objectManager->getObject(
            ExportButton::class,
            [
                'urlBuilder' => $this->urlBuilderMock,
                'context' => $this->context,
            ]
        );
    }


    public function testPrepare()
    {

        
        $processor = $this->getMockBuilder(\Magento\Framework\View\Element\UiComponent\Processor::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->context->expects($this->atLeastOnce())->method('getProcessor')->willReturn($processor);
        $this->context->expects($this->any())
            ->method('getRequestParam')
            ->with('test_asterisk')
            ->willReturn('test_asterisk_value');
        $option = ['label' => 'test label', 'value' => 'test value', 'url' => 'test_url'];
        $data = [
            'config' => [
                'options' => [
                    $option
                ],
                'additionalParams' => [
                    'test_key' => 'test_value',
                    'test_asterisk' => '*'
                ]
            ],
        ];
        $expected = $data;
        $expected['config']['options'][0]['url'] = [
            'test_key' => 'test_value',
            'test_asterisk' => 'test_asterisk_value',
        ];
        $this->model->setData($data);
        
        self::assertNull($this->model->prepare());
    }
}

?>