<?php

declare(strict_types=1);

namespace Arb\Reports\Ui\Component\Listing;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;

/**
 * Class RefundDataProvider
 */
class RefundDataProvider extends DataProvider
{
    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        //Getting refund and reversal are required to get data from multiple tables with different joins and subqueries 
        //which is required to do using raw query because of complexity of the query. This query is getting the data 
        // for refund and reversal amount, ref num, datetiime, comission, promocode value.

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $request =  $objectManager->get('Magento\Framework\App\Request\Http');
        $connection = $resource->getConnection();
        $sql = "select derive_table.* ,(select sum(sc.customer_bal_total_refunded)from sales_creditmemo sc  
        where  sc.entity_id in (derive_table.creditmemo_id )) as 'Total_Refund',
        (derive_table.merchant_revenue-(select sum(sc.customer_bal_total_refunded)from sales_creditmemo sc  
        where  sc.entity_id in (derive_table.creditmemo_id ))) as 'Calculation'
        from  (select mc.entity_id,mo.creditmemo_id,mo.seller_id ,mo.order_id,mc.applied_coupon_amount, 
        mc.total_amount,so.increment_id as ince_order_id,mc.created_at as order_datetime,
        re.return_ref_id as pg_tras_ref_num,
        mo.updated_at as transaction_date,
        so.coupon_code as promocode,mc.applied_coupon_amount as promocode_value,re.seller_id as merchant_id,
        CONCAT(ce.firstname,' ',ce.lastname) as merchant_name ,mc.commission_rate as commission_percentage,
        mc.total_commission as commission_amount,mc.actual_seller_amount as merchant_revenue,
        re.return_auth_code as authorization_number,re.return_type as transaction_type,
        (mc.applied_coupon_amount /mc.total_amount *100) as 'Promocode Percentage'
        from arb_cortex_refunds_mapping re
        Join marketplace_saleslist mc
        ON mc.order_id = re.mage_order_id and mc.seller_id = re.seller_id
        Join sales_order so
        ON mc.order_id = so.entity_id
        Join customer_entity ce
        ON mc.seller_id = ce.entity_id
        Join marketplace_orders mo
        ON mo.order_id = re.mage_order_id and mo.seller_id = re.seller_id";
        if (!empty($request->getParams()['filters']) && count($request->getParams()['filters'])>1) {
            $sql .= " where";
        }
        if (!empty($request->getParams()['filters']['pg_tras_ref_num'])) {
            $sql .= " re.return_ref_id between " . $request->getParams()['filters']['pg_tras_ref_num']['from'] . " AND " . $request->getParams()['filters']['pg_tras_ref_num']['to'];
        }
        if (!empty($request->getParams()['filters']['transaction_date'])) {
            if (!empty($request->getParams()['filters']['pg_tras_ref_num'])) {
                $sql .= " and";
            }
            $timezone = $objectManager->create(\Magento\Framework\Stdlib\DateTime\TimezoneInterface::class);
            $from = $timezone->formatDateTime(
                $request->getParams()['filters']['transaction_date']['from'],
                null,
                null,
                null,
                $timezone->getConfigTimezone(),
                'Y-MM-dd HH:mm:ss'
            );
            $to = $timezone->formatDateTime(
                $request->getParams()['filters']['transaction_date']['to'],
                null,
                null,
                null,
                $timezone->getConfigTimezone(),
                'Y-MM-dd HH:mm:ss'
            );
            $sql .= " mo.updated_at between '" . $from . "' AND '" . $to . "'";
        }
        if (!empty($request->getParams()['filters']['order_id'])) {
            if (!empty($request->getParams()['filters']['pg_tras_ref_num']) || !empty($request->getParams()['filters']['transaction_date'])) {
                $sql .= " and";
            }
            $sql .= " mo.order_id=" . $request->getParams()['filters']['order_id'];
        }
        if (!empty($request->getParams()['filters']['promocode'])) {
            if (!empty($request->getParams()['filters']['pg_tras_ref_num']) || !empty($request->getParams()['filters']['transaction_date'])
            || !empty($request->getParams()['filters']['order_id'])) {
                $sql .= " and";
            }
            $sql .= " so.coupon_code like '%" . $request->getParams()['filters']['promocode'] . "%'";
        }

        if (!empty($request->getParams()['filters']['merchant_name'])) {
            if (count($request->getParams()['filters'])>4){
                $sql .= " and";
            }
            $sql .= " CONCAT(ce.firstname,' ',ce.lastname) like '%" . $request->getParams()['filters']['merchant_name'] . "%'";
        }

        if (!empty($request->getParams()['filters']['authorization_number'])) {
            if (count($request->getParams()['filters'])>3){
                $sql .= " and";
            }
            $sql .= " re.return_auth_code=" . $request->getParams()['filters']['authorization_number'];
        }
        if (!empty($request->getParams()['filters']['transaction_type'])) {
            if (count($request->getParams()['filters'])>2){
                $sql .= " and";
            }
            $sql .= " re.return_type='" . $request->getParams()['filters']['transaction_type'] . "'";
        }
        $sql .= ") as derive_table";
        $result = $connection->fetchAll($sql);
        return [
            'items' => $result,
            'totalRecords' => count($result)
        ];
    }
}
