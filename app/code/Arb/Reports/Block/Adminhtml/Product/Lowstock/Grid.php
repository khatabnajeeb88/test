<?php

namespace Arb\Reports\Block\Adminhtml\Product\Lowstock;

/**
 * @category Arb
 * @package Arb_Reports
 * @author Arb Magento Team
 *
 */

class Grid extends \Magento\Backend\Block\Widget\Grid
{
    /**
     * @var \Magento\Reports\Model\ResourceModel\Product\Lowstock\CollectionFactory
     */
    protected $_lowstocksFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Reports\Model\ResourceModel\Product\Lowstock\CollectionFactory $lowstocksFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Reports\Model\ResourceModel\Product\Lowstock\CollectionFactory $lowstocksFactory,
        array $data = []
    ) {
        $this->_lowstocksFactory = $lowstocksFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @codeCoverageIgnore
     *
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $website = $this->getRequest()->getParam('website');
        $group = $this->getRequest()->getParam('group');
        $store = $this->getRequest()->getParam('store');

        if ($website) {
            $storeIds = $this->_storeManager->getWebsite($website)->getStoreIds();
            $storeId = array_pop($storeIds);
        } elseif ($group) {
            $storeIds = $this->_storeManager->getGroup($group)->getStoreIds();
            $storeId = array_pop($storeIds);
        } elseif ($store) {
            $storeId = (int)$store;
        } else {
            $storeId = null;
        }

        $collection = $this->_lowstocksFactory->create()->addAttributeToSelect(
            '*'
        )->filterByIsQtyProductTypes()->joinInventoryItem(
            'qty'
        )->useManageStockFilter(
            $storeId
        )->useNotifyStockQtyFilter(
            $storeId
        )->filterByMerchant()->setOrder(
            'qty',
            \Magento\Framework\Data\Collection::SORT_ORDER_ASC
        );

        if ($storeId) {
            $collection->addStoreFilter($storeId);
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Method responsible for adding custom filters
     *
     * @param $collection
     * @param $filterData
     *
     * @return mixed
     */
    protected function _addCustomFilter($collection, $filterData)
    {
        $merchantsList = $filterData->getData('merchants');
        if (isset($merchantsList[0])) {
            $merchantsIds = explode(',', $merchantsList[0]);
            $collection->addMerchantsFilter($merchantsIds);
        }

        $categoriesList = $filterData->getData('categories');
        if (isset($categoriesList[0])) {
            $categoriesIds = explode(',', $categoriesList[0]);
            $collection->addCategoriesFilter($categoriesIds);
        }
        return parent::_addCustomFilter($filterData, $collection);
    }
}
