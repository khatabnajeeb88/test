<?php

namespace Arb\Reports\Block\Adminhtml\Report\Filter\Form;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Data\Form\Element\Fieldset;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;

class Bestseller extends \Magento\Sales\Block\Adminhtml\Report\Filter\Form
{
    /**
     * @var SellerCollection
     */
    private $sellerlistCollectionFactory;

    /**
     * @var CollectionFactory
     */
    private $categoryCollection;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Bestseller constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Sales\Model\Order\ConfigFactory $orderConfig
     * @param SellerCollection $sellerlistCollectionFactory
     * @param CollectionFactory $categoryCollection
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Sales\Model\Order\ConfigFactory $orderConfig,
        SellerCollection $sellerlistCollectionFactory,
        CollectionFactory $categoryCollection,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $orderConfig, $data);
        $this->sellerlistCollectionFactory = $sellerlistCollectionFactory;
        $this->categoryCollection = $categoryCollection;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Preparing form
     *
     *
     * @return $this
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        /** @var Fieldset $fieldset */
        $fieldset = $this->getForm()->getElement('base_fieldset');

        if (is_object($fieldset) && $fieldset instanceof Fieldset) {
            $sellerCollection = $this->sellerlistCollectionFactory->create();
            $sellerCollection->addFieldToFilter(
                'is_seller',
                ['eq' => 1]
            )->addFieldToFilter(
                'store_id',
                0
            );

            $merchants = [
                [
                    'label' => 'Any',
                    'value' => '',
                ],
            ];

            foreach ($sellerCollection->getItems() as $merchant) {
                $customer = $this->customerRepository->getById($merchant->getData('seller_id'));
                $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
                $merchants[] = ['label' => __($customerName), 'value' => $merchant->getData('seller_id')];
            }

            $fieldset->addField(
                'merchants',
                'select',
                [
                    'name' => 'merchants',
                    'label' => 'Merchant',
                    'values' => $merchants
                ]
            );

            $categoryCollection = $this->categoryCollection->create();
            $categoryCollection->addAttributeToSelect('*');

            $categories = [
                [
                    'label' => 'Any',
                    'value' => '',
                ],
            ];

            /** @var \Magento\Catalog\Model\Category $category */
            foreach ($categoryCollection->getItems() as $category) {
                    $categories[] = ['label' => $category->getName(), 'value' => $category->getId()];
            }

            $fieldset->addField(
                'categories',
                'select',
                [
                    'name' => 'categories',
                    'label' => 'Category',
                    'values' => $categories
                ]
            );
        }

        return $this;
    }
}
