<?php
/**
 * This file consist of class Cron Setup
 *
 * @category Arb
 * @package Arb_OrderCleanUp
 * @author Arb Magento Team
 *
 */
namespace Arb\OrderCleanUp\Test\Unit\Model\Config;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\OrderCleanUp\Model\Config\CronConfig;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class CronConfigTest for testing CronConfig class
 */
class CronConfigTest extends TestCase
{
    public function setUp()
    {
        $this->contextMock = $this->createMock(Context::class);
        $this->registryMock = $this->createMock(Registry::class);
        $this->scopeConfigInterfaceMock = $this->createMock(ScopeConfigInterface::class);
        $this->typeListInterfaceMock = $this->createMock(TypeListInterface::class);
        $this->valueFactoryMock = $this->getMockBuilder(\Magento\Framework\App\Config\ValueFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create',"load","save","setValue","setPath"])
            ->getMock();
        $this->abstractResourceMock = $this->createMock(AbstractResource::class);
        $this->abstractDbMock = $this->createMock(AbstractDb::class);
        $this->cronConfigMock = new CronConfig(
            $this->contextMock,
            $this->registryMock,
            $this->scopeConfigInterfaceMock,
            $this->typeListInterfaceMock,
            $this->valueFactoryMock,
            $this->abstractResourceMock,
            $this->abstractDbMock
        );
    }

    public function testAfterSave()
    {
        $this->assertInstanceOf(
            CronConfig::class,
            $this->cronConfigMock
        );
        /*$this->valueFactoryMock->expects($this->once())->method("create")->willReturnSelf();
        $this->valueFactoryMock->expects($this->once())->method("load")->willReturnSelf();
        $this->valueFactoryMock->expects($this->once())->method("setValue")->willReturnSelf();
        $this->valueFactoryMock->expects($this->once())->method("setPath")->willReturnSelf();
        $this->valueFactoryMock->expects($this->once())->method("save")->willReturnSelf();
        $this->cronConfigMock->afterSave();*/
    }
}
