<?php
/**
 * This file consist of class OrderCleanUp for cleaning pending/pending payment orders
 *
 * @category Arb
 * @package Arb_OrderCleanUp
 * @author Arb Magento Team
 *
 */
namespace Arb\OrderCleanUp\Test\Unit\Cron;

/**
 * Consist of order data funtions
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
//phpcs:disable
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use \Magento\Sales\Model\OrderFactory;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use \Arb\ArbPayment\Model\PaymentInquiry;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Framework\Exception\LocalizedException;
use Arb\OrderCleanUp\Cron\OrderCleanUp;
use Magento\Framework\DB\Select;
/**
 * Class OrderCleanUpTest for testing OrderCleanUp class
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class OrderCleanUpTest extends TestCase
{
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->_orderFactoryMock = $this->getMockBuilder(OrderFactory::class)
            ->setMethods(["create","getCollection","addFieldToFilter",
                "setOrder","getIncrementId","getAllItems","getSelect","limit"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->_dateTimeMock = $this->createMock(DateTime::class);
        $this->_paymentInquiryMock = $this->createMock(PaymentInquiry::class);

        $this->_orderFactoryMock->expects($this->any())->method('create')->willReturnSelf();
        $this->_orderFactoryMock->expects($this->any())->method('getCollection')->willReturnSelf();
        $this->_orderFactoryMock->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        
        $this->itemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
                        ->disableOriginalConstructor()
                        ->setMethods([ 'create',"getProductType"])
                        ->getMock();
        $this->itemFactory = $this->getMockBuilder(\Magento\Sales\Model\Order\ItemFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([ 'create',"getProductType"])
            ->getMock();
        $this->itemFactory->method('create')->willReturn(
            $this->itemMock
        );
        $this->orderMock = $this->getMockBuilder(\Magento\Sales\Model\Order::class)
                        ->disableOriginalConstructor()
                        ->setMethods(["getIncrementId","getAllItems","getPayment"])
                        ->getMock();
        
        $this->_orderCleanUpMock = new OrderCleanUp(
            $this->_orderFactoryMock,
            $this->_dateTimeMock,
            $this->_paymentInquiryMock,
            $this->_scopeConfigMock
        );
    }

    public function testExecutewithRealProducts()
    {
        $this->_scopeConfigMock->expects($this->at(0))->method('getValue')->with("ordercleanup/general/is_clean_enable")->willReturn(1);
        $this->_scopeConfigMock->expects($this->at(1))->method('getValue')->with("ordercleanup/general/thresold")->willReturn(100);
        $this->orderMock->expects($this->any())->method('getIncrementId')->willReturn("00000021");
        $orders = [$this->orderMock];
        $this->_orderFactoryMock->expects($this->any())->method('setOrder')->willReturnSelf();
         /** @var Select|MockObject $selectMock */
        $selectQueryMock = $this->getMockBuilder(Select::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_orderFactoryMock->method('getSelect')->willReturn($selectQueryMock);
        $this->_orderFactoryMock->method('limit')->with(100)->willReturn($orders);

        $items = [$this->itemMock];
        
        $this->orderMock->method('getAllItems')->willReturn(
            $items
        );        
        $this->_orderCleanUpMock->execute();
    }

    public function testExecute()
    {
        $this->_scopeConfigMock->expects($this->at(0))->method('getValue')->with("ordercleanup/general/is_clean_enable")->willReturn(1);
        $this->_scopeConfigMock->expects($this->at(1))->method('getValue')->with("ordercleanup/general/thresold")->willReturn(100);
        $this->orderMock->expects($this->any())->method('getIncrementId')->willReturn("00000021");
        $this->orderMock->expects($this->any())->method('getPayment')->willReturnSelf();
        $orders = [$this->orderMock];
       $this->_orderFactoryMock->expects($this->any())->method('setOrder')->willReturnSelf();
         /** @var Select|MockObject $selectMock */
        $selectQueryMock = $this->getMockBuilder(Select::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_orderFactoryMock->method('getSelect')->willReturn($selectQueryMock);
        $this->_orderFactoryMock->method('limit')->with(100)->willReturn($orders);
        $items = [$this->itemMock];
        
        $this->orderMock->method('getAllItems')->willReturn(
            $items
        );
        $this->itemMock->method('getProductType')->willReturn("virtual");        
        // $this->_scopeConfigMock->expects($this->at(2))->method('getValue')->with("payment/arbpayment_gateway/vector_init")->willReturn("215785278");
        // $this->_scopeConfigMock->expects($this->at(3))->method('getValue')->with("payment/arbpayment_gateway/merchant_id")->willReturn("215785278");
        // $this->_scopeConfigMock->expects($this->at(4))->method('getValue')->with("payment/arbpayment_gateway/merchant_password")->willReturn("215785278");        
        $this->_orderCleanUpMock->execute();
    }

    public function testExecuteNullPayment()
    {
        $this->_scopeConfigMock->expects($this->at(0))->method('getValue')->with("ordercleanup/general/is_clean_enable")->willReturn(1);
        $this->_scopeConfigMock->expects($this->at(1))->method('getValue')->with("ordercleanup/general/thresold")->willReturn(100);
        $this->orderMock->expects($this->any())->method('getIncrementId')->willReturn("00000021");
        $this->orderMock->expects($this->any())->method('getPayment')->willReturn(null);
        $orders = [$this->orderMock];
        $this->_orderFactoryMock->expects($this->any())->method('setOrder')->willReturnSelf();
         /** @var Select|MockObject $selectMock */
        $selectQueryMock = $this->getMockBuilder(Select::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_orderFactoryMock->method('getSelect')->willReturn($selectQueryMock);
        $this->_orderFactoryMock->method('limit')->with(100)->willReturn($orders);
        $items = [$this->itemMock];
        $this->orderMock->method('getAllItems')->willReturn(
            $items
        );
        $this->itemMock->method('getProductType')->willReturn("virtual");
        $this->_orderCleanUpMock->execute();
    }

    
    public function testExecuteException()
    {
        $this->_scopeConfigMock->expects($this->at(0))->method('getValue')->with("ordercleanup/general/is_clean_enable")->willReturn(1);
        $this->_scopeConfigMock->expects($this->at(1))->method('getValue')->with("ordercleanup/general/thresold")->willReturn(100);
        $this->orderMock->expects($this->any())->method('getIncrementId')->willReturn("00000021");
        $this->orderMock->expects($this->any())->method('getPayment')->willReturnSelf();
        $orders = [$this->orderMock];
        $this->_orderFactoryMock->expects($this->any())->method('setOrder')->willReturnSelf();
         /** @var Select|MockObject $selectMock */
        $selectQueryMock = $this->getMockBuilder(Select::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_orderFactoryMock->method('getSelect')->willReturn($selectQueryMock);
        $this->_orderFactoryMock->method('limit')->with(100)->willReturn($orders);
        $items = [$this->itemMock];
        $this->orderMock->method('getAllItems')->willReturn(
            $items
        );
        $this->itemMock->method('getProductType')->willReturn("virtual");
        $this->_orderCleanUpMock->execute();
    }

    public function testExecuteDisableCron()
    {
        $this->_scopeConfigMock->expects($this->once())->method('getValue')->with("ordercleanup/general/is_clean_enable")->willReturn("0");
        $this->_orderCleanUpMock->execute();
    }
}
