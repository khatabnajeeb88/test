<?php
/**
 * This file consist of class Cron Setup
 * @category Arb
 * @package Arb_OrderCleanUp
 * @author Arb Magento Team
 *
 */
namespace Arb\OrderCleanUp\Model\Config;

class CronConfig extends \Magento\Framework\App\Config\Value
{
    /**
     * CRON Value/Path for Order Clean Up Frequency
     */
    const CRON_STRING_PATH = "crontab/default/jobs/arb_ordercleanup/schedule/cron_expr";

    /**
     * @var \Magento\Framework\App\Config\ValueFactory
     */
    protected $_configValueFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Config\ValueFactory $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\ValueFactory $configValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_configValueFactory = $configValueFactory;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     * @return $this
     * @throws \Exception
     * @codeCoverageIgnore
     */
    public function afterSave()
    {
        //Get Time from Configuration in Minutes
        $frequency = $this->getData('groups/general/fields/frequency/value');
        //Default 0 Hours
        $hours = "*/1";
        if ($frequency > 60) {
            //If time is more than an hour
            $hours = "*/".floor($frequency / 60);
            $minutes = ($frequency % 60);
        } else {
            //get minutes
            $minutes = $frequency;
        }
        // Set Frequncy as per CRON standards
        $cronString = "*/{$minutes} {$hours} * * *";
        /** @var $configValue \Magento\Framework\App\Config\ValueInterface */
        $configValue = $this->_configValueFactory->create();
        $configValue->load(self::CRON_STRING_PATH, 'path');
        $configValue->setValue($cronString)->setPath(self::CRON_STRING_PATH);
        ///Save configuration
        $configValue->save();
        return parent::afterSave();
    }
}
