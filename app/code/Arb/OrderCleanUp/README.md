## Synopsis
An extension to update order status either complete or cancled.

## Technical feature

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration available through Stores->Configuration [system.xml](etc/adminhtml/system.xml)
3. ACL configuration [acl.xml](etc/acl.xml)
4. Module configuration [module.xml](etc/module.xml)

##### Let's look into configuration attributes:
 * <code>is_clean_enable</code> is order clean up active? By Defualt/No
 * <code>frequency</code> CRON frequency
 * <code>thresold</code> Pick Up Time

## Tests
Unit tests could be found in the [Test/Unit](Test/Unit) directory.