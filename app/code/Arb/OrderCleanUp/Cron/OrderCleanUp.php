<?php

/**
 * This file consist of class OrderCleanUp for cleaning pending/pending payment orders
 *
 * @category Arb
 * @package Arb_OrderCleanUp
 * @author Arb Magento Team
 *
 */

namespace Arb\OrderCleanUp\Cron;

/**
 * Consist of order data funtions
 *
 */
use \Magento\Sales\Model\OrderFactory;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use \Arb\ArbPayment\Model\PaymentInquiry;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Framework\Exception\LocalizedException;

class OrderCleanUp
{
    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var PaymentInquiry
     */
    protected $_paymentInquiry;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Order Status as Array
     */
    const ORDER_STATUS = ["pending", "pending_payment"];

    /**
     * Virtual Product types
     */
    const PRODUCT_TYPE = "virtual";

    /**
     * 1 Minute TimeStamp
     */
    const MINUTE = "60";

    /**
     * Inquiry UDF array mapping
     *
     * @var array
     */
    protected $inquiryUDF =[
        PaymentInquiry::PAYMENT_ID => 'PaymentID',
        PaymentInquiry::TRACKING_ID => 'TrackID'
    ];

    /**
     * @param OrderFactory $_orderFactory
     * @param DateTime $_dateTime
     * @param PaymentInquiry $_paymentInquiry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        OrderFactory $_orderFactory,
        DateTime $_dateTime,
        PaymentInquiry $_paymentInquiry,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_orderFactory = $_orderFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_dateTime = $_dateTime;
        $this->_paymentInquiry = $_paymentInquiry;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Ordercleanup_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * @return void
     * generating CSV file, logging in cortex_log
     */
    public function execute()
    {
        $this->logger->info(__("Order Clean Up CRON Started"));

        //Check If Order Clean Up CRON is Enabled...
        $isOrderCleanUp = $this->scopeConfig->getValue(
            'ordercleanup/general/is_clean_enable',
            ScopeInterface::SCOPE_STORE
        );
        if (empty($isOrderCleanUp)) {
            $this->logger->info(__("Order Clean Up CRON is Disabled"));
            return;
        }
        //Get Current Time
        $currentDateTime = strtotime($this->_dateTime->date("Y-m-d H:i:s"));
        $pickUpTime = $this->scopeConfig->getValue(
            'ordercleanup/general/thresold',
            ScopeInterface::SCOPE_STORE
        );
        //convert Pick Up Time in Seconds
        $pickUpTime = (int) $pickUpTime * (int) self::MINUTE;
        
        $pastDateTime = $currentDateTime - $pickUpTime;
        //Get Order Collection Pending/Pending Payment for more than 10 minutes
        $orderCollection = $this->_orderFactory
            ->create()
            ->getCollection()
            ->addFieldToFilter('status', ['in' => self::ORDER_STATUS])
            ->addFieldToFilter('updated_at', ['lteq' => $this->_dateTime->date("Y-m-d H:i:s", $pastDateTime)])
            ->setOrder('entity_id', 'DESC');

        //Added limit for restrict the whole orders at once
        $orderCollection->getSelect()->limit(100);

        foreach ($orderCollection as $order) {
            if ($order->getIncrementId()) {
                //Payment Inquery logic
                $paymentData = $this->getPaymentData($order);
                if (!empty($paymentData)) {
                    //If payment purchange has called then Call payment Inquiry else cancle an order
                    // Re-Using Payment Inquiry Model from ArbPayment Module
                    $response = $this->_paymentInquiry->getInquiryData(
                        $paymentData,
                        PaymentInquiry::INITIAL_INQUIRY_COUNT,
                        true
                    );
                    $this->logger->info("Response");
                    $this->logger->info($response);
                    unset($response['orderStatus']);
                }
            }
        }
    }

    /**
     * @param OrderFactory $order
     * @return array
     */
    private function getPaymentData($order = '')
    {   
        //Added check if payment data available or not for the order
        $amt = $order->getGrandTotal();
        if (null != $order->getPayment()) {
            $payment = $order->getPayment();
            $amt = $payment->getAmountOrdered();
        }
        $paymentConfig = $this->getPaymentConfiguration();
        if (isset($paymentConfig['merchant_password']) && isset($paymentConfig['merchant_id'])) {
            $paymentData[] = [
            'amt' => $amt,
            'action' => PaymentInquiry::PAYMENT_ACTION,
            'password' => $paymentConfig['merchant_password'],
            'id' => $paymentConfig['merchant_id'],
            'currencyCode' => $paymentConfig['currency_code'],
            'trackId' => $order->getIncrementId(),
            'udf5' => $this->inquiryUDF[PaymentInquiry::TRACKING_ID],
            'transId' => $order->getIncrementId(),
            "orderId" => $order->getIncrementId()
            ];
        } else {
            throw new LocalizedException(
                __("Configuration problem exist")
            );
        }

        //Payment Data Array logged
        $this->logger->info("paymentData");
        $this->logger->info("Order Track ID -- ". $order->getIncrementId());
        return $paymentData;        
    }

    /**
     * Returns payment configuration
     * @return array
     */
    private function getPaymentConfiguration()
    {
        $this->paymentConfig['iv_config'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/vector_init',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_id'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_id',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_password'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_password',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['resource_key'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_terminal_resourcekey',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['currency_code'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_currency_code',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['inquiry_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_inquiry_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['response_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_response_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['error_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_error_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['timeout'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_timeout',
            ScopeInterface::SCOPE_STORE
        );
        return $this->paymentConfig;
    }
}
