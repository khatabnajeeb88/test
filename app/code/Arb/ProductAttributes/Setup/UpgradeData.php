<?php
/**
 * Product attributes setup files
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */
namespace Arb\ProductAttributes\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\TypeFactory as eavTypeFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Eav\Api\Data\AttributeFrontendLabelInterfaceFactory;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Eav\Model\AttributeSetManagement;
use Magento\Eav\Model\AttributeManagement;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * Default parent category
     */
    const DEFAULT_CATEGORY_ID = 2;

   /**
    * Arabic store code
    */
    const ARABIC_STORE_CODE = 'ar_SA';

   /**
    * English store code
    */
    const ENGLISH_STORE_CODE = 'en';

   /**
    * @var AttributeSetFactory
    */
    private $attributeSetFactory;
  
   /**
    * @var AttributeFrontendLabelInterfaceFactory
    */
    private $attributeFrontendLabelFactory;

   /**
    * @var ProductAttributeRepositoryInterface
    */
    private $productAttributeRepository;

   /**
    * @var StoreRepositoryInterface
    */
    private $storeRepository;

   /**
    * @var EavSetupFactory
    */
    private $eavTypeFactory;
   
   /**
    * @var AttributeSetManagement
    */
    private $attributeManagement;
   
   /**
    * @var \Magento\Eav\Setup\EavSetupFactory
    */
    private $eavSetupFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * Undocumented function
     *
     * @param AttributeSetFactory $attributeSetFactory
     * @param CategorySetupFactory $categorySetupFactory
     * @param AttributeFrontendLabelInterfaceFactory $attributeFrontendLabelFactory
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     * @param StoreRepositoryInterface $storeRepository
     * @param EavSetupFactory $eavSetupFactory
     * @param eavTypeFactory $eavTypeFactory
     * @param AttributeSetManagement $attributeSetManagement
     * @param AttributeManagement $attributeManagement
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $repository
     */
    public function __construct(
        AttributeSetFactory $attributeSetFactory,
        CategorySetupFactory $categorySetupFactory,
        AttributeFrontendLabelInterfaceFactory $attributeFrontendLabelFactory,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        StoreRepositoryInterface $storeRepository,
        EavSetupFactory $eavSetupFactory,
        eavTypeFactory $eavTypeFactory,
        AttributeSetManagement $attributeSetManagement,
        AttributeManagement $attributeManagement,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Serialize\Serializer\Json $json
    ) {
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeFrontendLabelFactory = $attributeFrontendLabelFactory;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->storeRepository= $storeRepository;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavTypeFactory = $eavTypeFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->attributeManagement = $attributeManagement;
        $this->json = $json;
        $this->categoryFactory = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        //get store Id for arabic store to set
        $arabicStoreId = $this->storeRepository->get(self::ARABIC_STORE_CODE)->getId();
        $engStoreId = $this->storeRepository->get(self::ENGLISH_STORE_CODE)->getId();

        //set Attribute data
        $attributesData = [
            ['label' => 'Brand','required'=>true,'arabic_label' =>'العلامة التجارية','attribute_set'=>'Default'],
            ['label' => 'Colour','required'=>true,'arabic_label' =>'اللون','attribute_set'=>'Default', 'options' => [
                'en' => ['White', 'Black', 'Violet', 'Blue', 'Green', 'Yellow', 'Orange', 'Red', 'Pink', 'Brown', 'Maroon', 'Gold', 'Silver',
                    'Gray', 'Light (as in lighter shade)', 'Dark (as in darker shade)', 'Light yellow', 'Deep red', 'Colorful'],
                'ar' => ['أبيض', 'اسود', 'بنفسجي', 'أزرق', 'أخضر', 'أصفر', 'برتقالي', 'أحمر', 'وردي', 'بنّي', 'كستنائي'
                , 'ذهبي', 'فضي', 'رصاصي', 'فاتح', 'غامق', 'أصفر فاتح', 'أحمر غامق', 'ملون']
            ]],
            ['label' => 'Model Number','required'=>true,'arabic_label' =>'رقم الموديل','attribute_set'=>'Default'],
            ['label' => 'Model Name','required'=>true,'arabic_label' =>'الموديل','attribute_set'=>'Default'],
            ['label' => 'Size','required'=>true,'arabic_label' =>'الحجم','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Display Type','required'=>false,'arabic_label' =>'طريقة العرض','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Screen Size','required'=>true,'arabic_label' =>'حجم الشاشة','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Internal Memory','required'=>true,'arabic_label' =>'الذاكرة الداخلية','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Battery Capacity','required'=>true,'arabic_label' =>'حجم البطارية','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'RAM','required'=>true,'arabic_label' =>'الذاكرة','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Operating System','required'=>true,'arabic_label' =>'نظام التشغيل','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Processor Speed','required'=>true,'arabic_label' =>'سرعه المعالج','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'SIM Card','required'=>true,'arabic_label' =>'بطاقه الاتصال','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Sim Type','required'=>true,'arabic_label' =>'نوع البطاقه','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Display Resolution Type','required'=>true,'arabic_label' =>'دقه العرض','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Charger Compatibility','required'=>false,'arabic_label' =>'طريقه الشحن','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Secondary Camera','required'=>true,'arabic_label' =>'الكاميرا الاضافيه','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Network Type','required'=>true,'arabic_label' =>'نوع الشبكه','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Expandable Memory','required'=>true,'arabic_label' =>'الذاكره الاضافيه','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Primary Camera','required'=>true,'arabic_label' =>'الاكاميرا الاساسيه','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Display Resolution','required'=>true,'arabic_label' =>'دقه العرض','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Operating System Version','required'=>false,'arabic_label' =>'اصدار نظام التشغيل','attribute_set'=>'Mobile & Tablets'],
            ['label' => 'Processor Type','required'=>true,'arabic_label' =>'نوع المعالج','attribute_set'=>'Computer & Network'],
            ['label' => 'Primary Camera','required'=>true,'arabic_label' =>'الكاميرا الاساسيه','attribute_set'=>'Computer & Network'],
            ['label' => 'Display Resolution','required'=>true,'arabic_label' =>'دقه العرض','attribute_set'=>'Computer & Network'],
            ['label' => 'Screen Size','required'=>true,'arabic_label' =>'حجم الشاشة','attribute_set'=>'Computer & Network'],
            ['label' => 'Display Type','required'=>false,'arabic_label' =>'دقه العرض','attribute_set'=>'Computer & Network'],
            ['label' => 'Graphics Memory','required'=>true,'arabic_label' =>'كرت الشاشه','attribute_set'=>'Computer & Network'],
            ['label' => 'Internal Memory','required'=>true,'arabic_label' =>'الذاكرة الداخلية','attribute_set'=>'Computer & Network'],
            ['label' => 'Storage','required'=>true,'arabic_label' =>'سعه التخزين','attribute_set'=>'Computer & Network'],
            ['label' => 'Battery Capacity','required'=>true,'arabic_label' =>'حجم البطاريه','attribute_set'=>'Computer & Network'],
            ['label' => 'RAM','required'=>true,'arabic_label' =>'الذاكره ','attribute_set'=>'Computer & Network'],
            ['label' => 'Operating System','required'=>true,'arabic_label' =>'نظام التشغيل','attribute_set'=>'Computer & Network'],
            ['label' => 'Country of Origin','required'=>true,'arabic_label' =>'بلد الصنع','attribute_set'=>'Computer & Network'],
            ['label' => 'Display Resolution Type','required'=>false,'arabic_label' =>'دقه العرض','attribute_set'=>'Computer & Network'],
            ['label' => 'Operating System Version','required'=>false,'arabic_label' =>'اصدار نظام التشغيل','attribute_set'=>'Computer & Network'],
            ['label' => 'Product Length','required'=>false,'arabic_label' =>'عرض المنتج','attribute_set'=>'Computer & Network'],
            ['label' => 'Product Height','required'=>false,'arabic_label' =>'طول المنتج','attribute_set'=>'Computer & Network'],
            ['label' => 'Product Width/Depth','required'=>false,'arabic_label' =>'سعه المنتج','attribute_set'=>'Computer & Network'],
            ['label' => 'Operating System Number','required'=>false,'arabic_label' =>'اصدار نظام التشغيل','attribute_set'=>'Computer & Network'],
            ['label' => 'Number of Cores','required'=>false,'arabic_label' =>'','attribute_set'=>'Computer & Network'],
            ['label' => 'Camera Type','required'=>true,'arabic_label' =>'نوع الكاميرا','attribute_set'=>'Computer & Network'],
            ['label' => 'Wireless','required'=>true,'arabic_label' =>'لاسلكي','attribute_set'=>'Computer & Network'],
            ['label' => 'Audio Jack','required'=>false,'arabic_label' =>'الصوتيات','attribute_set'=>'Computer & Network'],
            ['label' => 'Battery Type','required'=>false,'arabic_label' =>'نوع البطاريه','attribute_set'=>'Computer & Network'],
            ['label' => 'Screen Size','required'=>true,'arabic_label' =>'حجم الشاشه','attribute_set'=>'Cameras'],
            ['label' => 'Expandable Memory','required'=>false,'arabic_label' =>'الذاكره الاضافيه','attribute_set'=>'Cameras'],
            ['label' => 'Screen Resolution','required'=>false,'arabic_label' =>'دقه العرض','attribute_set'=>'Cameras'],
            ['label' => 'Input','required'=>true,'arabic_label' =>'المدخلات','attribute_set'=>'Cameras'],
            ['label' => 'Waterproof Depth','required'=>true,'arabic_label' =>'الحمايه ضد الماء','attribute_set'=>'Cameras'],
            ['label' => 'Slo-Mo','required'=>true,'arabic_label' =>'الحركه البطيئه','attribute_set'=>'Cameras'],
            ['label' => 'Output','required'=>true,'arabic_label' =>'المخرجات','attribute_set'=>'Cameras'],
            ['label' => 'Video Recording Resolution Type','required'=>true,'arabic_label' =>'دقه تصوير الفديو','attribute_set'=>'Cameras'],
            ['label' => 'Water/ Dust Properties','required'=>true,'arabic_label' =>'ضد الماء والاتربه','attribute_set'=>'Cameras'],
            ['label' => 'Sensor','required'=>true,'arabic_label' =>'حساس','attribute_set'=>'Cameras'],
            ['label' => 'Touch Screen','required'=>true,'arabic_label' =>'شاشه لمس','attribute_set'=>'Cameras'],
            ['label' => 'Wireless Controller','required'=>true,'arabic_label' =>'جهاز تحكم لا سلكي','attribute_set'=>'Video Games'],
            ['label' => '4K Display Capability','required'=>true,'arabic_label' =>'العرض بتقنيه 4k','attribute_set'=>'Video Games'],
            ['label' => 'Hard Drive Size','required'=>true,'arabic_label' =>'حجم القرص الصلب','attribute_set'=>'Video Games'],
            ['label' => 'Installed RAM','required'=>true,'arabic_label' =>'الذاكره','attribute_set'=>'Video Games'],
            ['label' => 'USB','required'=>true,'arabic_label' =>'مدخل USB','attribute_set'=>'Video Games'],
            ['label' => 'Platform','required'=>false,'arabic_label' =>'','attribute_set'=>'Video Games'],
            ['label' => 'Installation','required'=>true,'arabic_label' =>'التركيب','attribute_set'=>'Home Appliances'],
            ['label' => 'Filter Material','required'=>false,'arabic_label' =>'أدوات الصنع','attribute_set'=>'Home Appliances'],
            ['label' => 'Power','required'=>true,'arabic_label' =>'الطاقه','attribute_set'=>'Home Appliances'],
            ['label' => 'Voltage','required'=>true,'arabic_label' =>'فولت','attribute_set'=>'Home Appliances'],
            ['label' => 'Includes','required'=>false,'arabic_label' =>'الإضافات','attribute_set'=>'Home Appliances'],
            ['label' => 'Over Pressure Safety Function','required'=>false,'arabic_label' =>'جهاز تحمل الضغط','attribute_set'=>'Home Appliances'],
            ['label' => 'Energy Used','required'=>false,'arabic_label' =>'الاستهلاك','attribute_set'=>'Home Appliances'],
        ];

        //create attributes if not present
        $attributeSets = array_column($attributesData, 'attribute_set');
        $attributeSets = array_unique($attributeSets);

        $eavSetup = $this->eavSetupFactory->create();
        $entityType     = $this->eavTypeFactory->create()->loadByCode(Product::ENTITY);
        
        $defaultSetId   = $eavSetup->getDefaultAttributeSetId(Product::ENTITY);
        $defaultGroupId = $eavSetup->getDefaultAttributeGroupId(Product::ENTITY);
       
        //create products for the attribute
        $iCount = 100;
        foreach ($attributesData as $data) {
            $attributeCode = $this->getCodeName($data['label']);
            $attributeSetName = $data['attribute_set'];
            $attributeId = $eavSetup->getAttributeId(Product::ENTITY, $attributeCode);
            if (!$attributeId) {
                $eavSetup->addAttribute(
                    Product::ENTITY,
                    $attributeCode,
                    [
                        'type' => isset($data['options'])? 'text' : 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => $data['label'],
                        'input' => isset($data['options'])? 'select' : 'text',
                        'class' => '',
                        'source' => '',
                        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => \true,
                        'required' => $data['required'],
                        'user_defined' => \true,
                        'default' => '',
                        'searchable' => \false,
                        'filterable' => \false,
                        'comparable' => \false,
                        'visible_on_front' => \true,
                        'used_in_product_listing' => \true,
                        'unique' => \false,
                        'apply_to' => 'simple,configurable',
                        'sort_order' => $iCount,
                    ]
                );

                $attributeSet = $this->attributeSetFactory->create();
                $attribute = $this->productAttributeRepository->get($attributeCode);
                $checkAttrSet = $attributeSet->setEntityTypeId(Product::ENTITY)->load($attributeSetName, 'attribute_set_name');
    
                if (!$checkAttrSet->getId()) {
                    $dataSet = [
                        'attribute_set_name'    => $attributeSetName,
                        'entity_type_id'        => $entityType->getId(),
                        'sort_order'            => $iCount,
                    ];
                    $attributeSet->setData($dataSet);
                    $attributeSet->validate();
                    $attributeSet->save();
                    $attributeSet->initFromSkeleton($defaultSetId)->save();
                  
                    $checkAttrSet = $attributeSet->setEntityTypeId(Product::ENTITY)->load($attributeSetName, 'attribute_set_name');
                }
                $checkAttrSet->getId(). ' - '.  $checkAttrSet->getDefaultGroupId();
                
                $attributeSetId = $checkAttrSet->getId();
                $groupId = $checkAttrSet->getDefaultGroupId();
                if ($attributeSetName == 'Default') {
                    $attributeSetId = $defaultSetId;
                    $groupId = $defaultGroupId;
                }

                $this->attributeManagement->assign(
                    'catalog_product',
                    $attributeSetId,
                    $groupId,
                    $attributeCode,
                    $iCount
                );
                $iCount = $iCount+10;
                
                if (isset($data['options'])) {
                    $engValues = $data['options']['en'];
                    $arValues = $data['options']['ar'];
                    for ($i = 0; $i < count($engValues); $i++) {
                        $options['value'][$engValues[$i]][0] = $engValues[$i];
                        $options['value'][$engValues[$i]][$engStoreId] = $engValues[$i];
                        $options['value'][$engValues[$i]][$arabicStoreId] = $arValues[$i];
                    }

                    $options['attribute_id'] = $attribute->getId();
                    $eavSetup->addAttributeOption($options);
                }

                $frontendLabels = [
                    $this->attributeFrontendLabelFactory->create()
                        ->setStoreId($engStoreId)
                        ->setLabel($data['label']),
                    $this->attributeFrontendLabelFactory->create()
                        ->setStoreId($arabicStoreId)
                        ->setLabel($data['arabic_label'])
                ];

                $attribute->setFrontendLabels($frontendLabels);
                $this->productAttributeRepository->save($attribute);
            }
        }
        if (version_compare($context->getVersion(), '2.0.1', '<=')) {
            $this->createCategories();
        }
    }

    /**
     * Returns clean attribute string
     *
     * @param  string $string
     * @return string
     */
    public function getCodeName($string)
    {
        if (is_numeric(substr($string, 0, 1))) {
            $string = preg_replace("/^(\w+\s)/", "", $string);
        }
        $code =  str_replace(' ', '_', $string);
        $code = preg_replace('/[^A-Za-z0-9\_]/', '', $code);
        $code = strtolower($code);
        
        return trim($code);
    }

    /**
     * Create physical product categories
     *
     * @return void
     */
    public function createCategories()
    {
        //get store Id for arabic store to set
        $arabicStoreId = $this->storeRepository->get(self::ARABIC_STORE_CODE)->getId();
        $engStoreId = $this->storeRepository->get(self::ENGLISH_STORE_CODE)->getId();
        $jsonCategories = $this->getCategoriesData();
        $categoryData  = $this->json->unserialize($jsonCategories);
        
        foreach ($categoryData as $key => $value) {
            $parentId = self::DEFAULT_CATEGORY_ID;
            if (is_string($value['parent_category'])) {
                $categoryTitle = $value['parent_category']; // Category Name
                $collection = $this->categoryFactory->create()->getCollection()->addFieldToFilter('name', ['in' => $categoryTitle]);
                if ($collection->getSize()) {
                    $parentId = $collection->getFirstItem()->getId();
                }
            }
           
            $data = ['data' => [
                "parent_id" => $parentId,
                'name' => $value['en'],
                "is_active" => true,
                "position" => 10,
                "include_in_menu" => true,
                "store_id" => $engStoreId
            ]];

            $checkCategory = $this->categoryFactory->create()->getCollection()->addFieldToFilter('name', ['in' => $value['en']])->addFieldToFilter('parent_id', ['in' => $parentId]);
            if (!$checkCategory->getData()) {
                $category = $this->categoryFactory->create($data)->setStoreId($engStoreId);
                $categoryId = $this->categoryRepository->save($category)->getId();
                $categoryArabic = $this->categoryFactory->create()->setStoreId($arabicStoreId)->load($categoryId);
                if ($categoryId) {
                    $dataArabic["entity_id"] = $categoryId;
                    $dataArabic["is_active"] = true;
                    $dataArabic["name"] = $value['ar_SA'];
                    $dataArabic["url_key"] = $value['en'];
                    $dataArabic["store_id"] = $arabicStoreId;
                    $categoryArabic->setData($dataArabic);
                    $categoryArabic->save();
                    $this->categoryRepository->save($categoryArabic);

                    $categoryEnglish = $this->categoryFactory->create()->setStoreId($engStoreId)->load($categoryId);
                    $dataEn["entity_id"] = $categoryId;
                    $dataEn["name"] = $value['en'];
                    $dataEn["store_id"] = $engStoreId;
                    $categoryEnglish->setData($dataEn);
                    $categoryEnglish->save();
                    $this->categoryRepository->save($categoryEnglish);
                }
            }
        }
    }

    /**
     * Returns category data in JSON format
     *
     * @return string
     */
    private function getCategoriesData()
    {
        $categoryData = '[{"parent_category":"2","en":"Electronics","ar_SA":"الالكترونيات"},{"parent_category":"Electronics","en":"Mobile & Tablets","ar_SA":"الجوال & الاجهزه اللوحيه"},{"parent_category":"Mobile & Tablets","en":"Mobiles","ar_SA":"الجوالات"},{"parent_category":"Mobile & Tablets","en":"Mobiles Accessories","ar_SA":"اكسسوارات الجوال"},{"parent_category":"Mobile & Tablets","en":"Tablets","ar_SA":"الاجهزه اللوحيه"},{"parent_category":"Electronics","en":"Computer & network","ar_SA":"الكمبيوتر والشبكات"},{"parent_category":"Computer & network","en":"LapTop","ar_SA":"لاب توب"},{"parent_category":"Computer & network","en":"Monitors","ar_SA":"الشاشات"},{"parent_category":"Computer & network","en":"Printers","ar_SA":"الطابعات"},{"parent_category":"Computer & network","en":"Routers","ar_SA":"اجهزه الاتصال بالشبكات"},{"parent_category":"Electronics","en":"Cameras","ar_SA":"الكاميرات"},{"parent_category":"Cameras","en":"Lenses","ar_SA":"العدسات"},{"parent_category":"Cameras","en":"Cameras Accessories","ar_SA":"اكسسوارات الكاميرات"},{"parent_category":"Electronics","en":"Television & Video","ar_SA":"التلفزيونات والصوتيات"},{"parent_category":"Television & Video","en":"Television","ar_SA":"التلفزيونات"},{"parent_category":"Television & Video","en":"Projectors","ar_SA":"بروجيكتور"},{"parent_category":"Electronics","en":"Video Games","ar_SA":"العاب الفديو"},{"parent_category":"Video Games","en":"Consoles","ar_SA":"اجهزه التحكم"},{"parent_category":"Video Games","en":"Accessories","ar_SA":"الاكسسوارات"},{"parent_category":"Video Games","en":"Games","ar_SA":"الألعاب "},{"parent_category":"Electronics","en":"Home Appliances","ar_SA":"الاجهزه المنزليه"},{"parent_category":"Home Appliances","en":"Coffee Makers","ar_SA":"مكائن القهوه"},{"parent_category":"Home Appliances","en":"Juicers","ar_SA":"العصارات"},{"parent_category":"Home Appliances","en":"Mixers","ar_SA":"العجانات"},{"parent_category":"Home Appliances","en":"Ovens & Toasters","ar_SA":"الافران الصغيره"},{"parent_category":"Home Appliances","en":"Blenders","ar_SA":"الخلاطات"},{"parent_category":"Home Appliances","en":"Irons & Steamers","ar_SA":"اجهزه الكي والتبخير"},{"parent_category":"Home Appliances","en":"Cooktops","ar_SA":"الافران "},{"parent_category":"Home Appliances","en":"Food Processors","ar_SA":"الات تحضير الطعام"},{"parent_category":"Home Appliances","en":"Microwave Ovens","ar_SA":"المايكرويف"},{"parent_category":"Home Appliances","en":"Contact Grills","ar_SA":"الشوايات"},{"parent_category":"Home Appliances","en":"Fryers","ar_SA":"القلايات"},{"parent_category":"Home Appliances","en":"Electric Cookers","ar_SA":"القدور الالكترونيه"},{"parent_category":"Home Appliances","en":"Sewing Machines","ar_SA":"اجهزه الخياطه"}]';
        return $categoryData;
    }
}
