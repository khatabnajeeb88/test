<?php
/**
 * DataPatch for assigning values to new custom attribute merchant_id
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Setup\Patch\Data;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as MpProductCollection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Create assign values to new custom product attribute
 */
class AssignMerchantIdAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var MpProductCollection
     */
    private $collectionFactory;

    /**
     * @var State
     */
    private $state;

    /**
     * AssignMerchantIdAttribute constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ProductRepositoryInterface $productRepository
     * @param MpProductCollection $collectionFactory
     * @param State $state
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ProductRepositoryInterface $productRepository,
        MpProductCollection $collectionFactory,
        State $state
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->productRepository = $productRepository;
        $this->collectionFactory = $collectionFactory;
        $this->state = $state;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [CreateMerchantIdAttribute::class];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Function responsible for running code inside patch
     *
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws StateException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function apply()
    {
        $this->state->setAreaCode(Area::AREA_FRONTEND);
        $this->moduleDataSetup->getConnection()->startSetup();

        $attributes = [
            'model_number',
            'brand',
            'colour',
            'model_name',
            'size',
            'screen_size',
            'internal_memory',
            'battery_capacity',
            'ram',
            'operating_system',
            'processor_speed',
            'sim_card',
            'sim_type',
            'display_resolution_type',
            'secondary_camera',
            'network_type',
            'expandable_memory',
            'primary_camera',
            'display_resolution',
            'processor_type',
            'graphics_memory',
            'storage',
            'country_of_origin',
            'camera_type',
            'wireless',
            'input',
            'waterproof_depth',
            'slomo',
            'output',
            'video_recording_resolution_type',
            'water_dust_properties',
            'sensor',
            'touch_screen',
            'wireless_controller',
            'display_capability',
            'hard_drive_size',
            'installed_ram',
            'usb',
            'installation',
            'power',
            'voltage',
        ];

        // $mpProducts = $this->collectionFactory->create()->getItems();
        // foreach ($mpProducts as $mpProduct) {
        //     $merchantId = $mpProduct->getData('seller_id');
        //     $productId = $mpProduct->getData('mageproduct_id');

        //     $product = $this->productRepository->getById($productId);

        //     if (($product->getTypeId() === 'simple') || ($product->getTypeId() === 'configurable')) {
        //         foreach ($attributes as $attribute) {
        //             $product->getCustomAttribute($attribute) ?: $product->setCustomAttribute($attribute, ' ');
        //         }
        //     }

        //     $product->setCustomAttribute('merchant_id', $merchantId);
        //     $this->productRepository->save($product);
        // }

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
