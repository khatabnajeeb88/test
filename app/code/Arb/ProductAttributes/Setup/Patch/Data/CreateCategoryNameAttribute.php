<?php
/**
 * DataPatch for creating new custom attribute category_name
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Create new custom product attribute
 */
class CreateCategoryNameAttribute implements DataPatchInterface
{
    private const CATEGORY_NAME_ATTRIBUTE = 'categories';
    private const CATEGORY_NAME_ATTRIBUTE_LABEL = 'Category Name';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreatePhysicalProductAttributes constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Function responsible for running code inside patch
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup
            ->addAttribute(
                Product::ENTITY,
                self::CATEGORY_NAME_ATTRIBUTE,
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => self::CATEGORY_NAME_ATTRIBUTE_LABEL,
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_STORE,
                    'visible' => false,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => true,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'note' => 'This attribute is used for searching product name by category'
                ]
            );

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
