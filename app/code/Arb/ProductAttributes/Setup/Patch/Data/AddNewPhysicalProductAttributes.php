<?php
/**
 * DataPatch for adding/editing new custom attributes
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Zend_Validate_Exception;

/**
 * Install new custom attributes and update existing ones
 */
class AddNewPhysicalProductAttributes implements DataPatchInterface
{
    const MERCHANT_NAME_ATTRIBUTE = 'merchant_name';
    const MERCHANT_NAME_ATTRIBUTE_LABEL = 'Merchant Name';
    const BRAND_ATTRIBUTE = 'brand';
    const MODEL_NUMBER_ATTRIBUTE = 'model_number';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [
            CreatePhysicalProductAttributes::class
        ];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Install new custom attribute and update existing ones
     *
     * @return void
     * @throws LocalizedException|Zend_Validate_Exception
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create();

        $eavSetup->updateAttribute(
            Product::ENTITY,
            self::BRAND_ATTRIBUTE,
            'is_global',
            ScopedAttributeInterface::SCOPE_STORE
        )->updateAttribute(
            Product::ENTITY,
            self::BRAND_ATTRIBUTE,
            'apply_to',
            'simple,configurable'
        );

        $eavSetup->updateAttribute(
            Product::ENTITY,
            self::MODEL_NUMBER_ATTRIBUTE,
            'is_global',
            ScopedAttributeInterface::SCOPE_STORE
        )->updateAttribute(
            Product::ENTITY,
            self::MODEL_NUMBER_ATTRIBUTE,
            'apply_to',
            'simple,configurable'
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            self::MERCHANT_NAME_ATTRIBUTE,
            [
                'type' => 'text',
                'label' => self::MERCHANT_NAME_ATTRIBUTE_LABEL,
                'input' => 'text',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'apply_to' => 'simple,configurable'
            ]
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
