<?php
/**
 * DataPatch for assigning values to new custom attribute category_name
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Setup\Patch\Data;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollection;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ResourceModel\Product\Action;
/**
 * Create assign values to new custom product attribute
 */
class AssignCategoryNameAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var ProductCollection
     */
    private $collectionFactory;

    /**
     * @var CategoryCollection
     */
    private $categoryCollectionFactory;

    /**
     * @var State
     */
    private $state;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * Assign category name
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ProductCollection $collectionFactory
     * @param CategoryCollection $categoryCollectionFactory
     * @param State $state
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ProductCollection $collectionFactory,
        CategoryCollection $categoryCollectionFactory,
        State $state,
        StoreManagerInterface $storeManager,
        Action $productAction
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->collectionFactory = $collectionFactory;
        $this->state = $state;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->storeManager = $storeManager;
        $this->productAction = $productAction;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_CategoryAssign_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);

    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [CreateCategoryNameAttribute::class];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Function responsible for running code inside patch
     *
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws StateException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function apply()
    {
        $this->state->setAreaCode(Area::AREA_FRONTEND);
        $this->moduleDataSetup->getConnection()->startSetup();

        $storeIds = array_keys($this->storeManager->getStores());

        foreach ($storeIds as $store) {
            $collection = $this->collectionFactory->create();
            $productCollection = $collection->addAttributeToSelect('*')->addStoreFilter($store);
            $productCategory = [];
            foreach ($productCollection as $product) {
                if(!empty($product->getCategoryIds())){
                $categories = $this->categoryCollectionFactory->create()->setStoreId($store)->addAttributeToSelect(
                    'name'
                )
                ->addAttributeToFilter(
                    'entity_id',
                    $product->getCategoryIds()
                );

                $categoryNames = [];
                foreach ($categories as $category) {
                    $categoryNames[] = strtolower($category->getName());
                }
                $this->logger->info($product->getSku()." - Product has assigned category.- ".json_encode($product->getCategoryIds())."store - ".$store);
                $this->productAction->updateAttributes([$product->getId()], ['categories' => implode(' ',$categoryNames)], $store);
                }else{
                    $this->logger->info($product->getSku()." - Product does not have assigned category.");
                }
            }
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
