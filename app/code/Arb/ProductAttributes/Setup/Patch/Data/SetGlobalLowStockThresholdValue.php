<?php 
/**
 * DataPatch to set Global Low Stock Value
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class SetGlobalLowStockThresholdValue implements DataPatchInterface
{
    private const XML_CONFIG_MAGENTO_PATH_GLOBAL_LOW_STOCK_THRESHOLD = 'cataloginventory/item_options/notify_stock_qty';
    private const XML_CONFIG_WEBKUL_PATH_GLOBAL_LOW_STOCK_THRESHOLD = 'marketplace/inventory_settings/low_stock_amount';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::XML_CONFIG_MAGENTO_PATH_GLOBAL_LOW_STOCK_THRESHOLD, 5);
        $this->updateConfiguration(self::XML_CONFIG_WEBKUL_PATH_GLOBAL_LOW_STOCK_THRESHOLD, 5);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param int $value
     *
     * @return void
     */
    private function updateConfiguration(string $path, int $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
