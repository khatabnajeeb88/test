<?php

namespace Arb\ProductAttributes\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;

class CreatePhysicalProductAttributes implements DataPatchInterface
{
    private const BRAND_ATTRIBUTE = 'brand';
    private const BRAND_ATTRIBUTE_LABEL = 'Brand';
    private const MODEL_NUMBER_ATTRIBUTE = 'model_number';
    private const MODEL_NUMBER_ATTRIBUTE_LABEL = 'Model number';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreatePhysicalProductAttributes constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup
            ->addAttribute(
                Product::ENTITY,
                self::BRAND_ATTRIBUTE,
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => self::BRAND_ATTRIBUTE_LABEL,
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => \true,
                    'required' => \false,
                    'user_defined' => \false,
                    'default' => '',
                    'searchable' => \false,
                    'filterable' => \false,
                    'comparable' => \false,
                    'visible_on_front' => \true,
                    'used_in_product_listing' => \true,
                    'unique' => \false,
                    'apply_to' => 'simple,configurable',
                ]
            )
            ->addAttribute(
                Product::ENTITY,
                self::MODEL_NUMBER_ATTRIBUTE,
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => self::MODEL_NUMBER_ATTRIBUTE_LABEL,
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => \true,
                    'required' => \false,
                    'user_defined' => \false,
                    'default' => '',
                    'searchable' => \false,
                    'filterable' => \false,
                    'comparable' => \false,
                    'visible_on_front' => \true,
                    'used_in_product_listing' => \true,
                    'unique' => \false,
                    'apply_to' => 'simple,configurable',
                ]
            );

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
