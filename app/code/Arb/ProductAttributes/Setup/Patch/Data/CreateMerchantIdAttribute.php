<?php 
/**
 * DataPatch for creating new custom attribute merchant_id
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Create new custom product attribute
 */
class CreateMerchantIdAttribute implements DataPatchInterface
{
    private const MERCHANT_ID_ATTRIBUTE = 'merchant_id';
    private const MERCHANT_ID_ATTRIBUTE_LABEL = 'Merchant Id';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreatePhysicalProductAttributes constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Function responsible for running code inside patch
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup
            ->addAttribute(
                Product::ENTITY,
                self::MERCHANT_ID_ATTRIBUTE,
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => self::MERCHANT_ID_ATTRIBUTE_LABEL,
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => \false,
                    'required' => \false,
                    'user_defined' => \false,
                    'default' => '',
                    'searchable' => \false,
                    'filterable' => \true,
                    'comparable' => \false,
                    'visible_on_front' => \false,
                    'used_in_product_listing' => \false,
                    'unique' => \false
                ]
            );

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
