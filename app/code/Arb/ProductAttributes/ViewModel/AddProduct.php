<?php
/**
 * ViewModel for New Product Block
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Arb\ProductAttributes\Helper\CustomAttribute;

/**
 * View model containing helper methods for custom attributes
 */
class AddProduct implements ArgumentInterface
{
    /**
     * @var CustomAttribute
     */
    private $customAttributeHelper;

    /**
     * @param CustomAttribute $customAttributeHelper
     */
    public function __construct(CustomAttribute $customAttributeHelper)
    {
        $this->customAttributeHelper = $customAttributeHelper;
    }

    /**
     * @return string
     */
    public function getProductHintBrand()
    {
        return (string)$this->customAttributeHelper->getProductHintBrand();
    }

    /**
     * @return string
     */
    public function getProductHintModelNumber()
    {
        return (string)$this->customAttributeHelper->getProductHintModelNumber();
    }

    /**
     * @return string
     */
    public function getProductHintMerchantName()
    {
        return (string)$this->customAttributeHelper->getProductHintMerchantName();
    }
}
