<?php 
/**
 * Custom Attributes for Product Form
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Plugin\Block\Product;

use Magento\Framework\App\Request\DataPersistorInterface;
use Webkul\Marketplace\Block\Product\Create as WebkulCreate;

class Create
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(DataPersistorInterface $dataPersistor)
    {
        $this->dataPersistor = $dataPersistor;
    }

    /**
     * Added Custom Attributes to Product structure in Add/Edit form
     *
     * @param WebkulCreate $subject
     * @param callable $proceed
     *
     * @return array
     */
    public function aroundGetPersistentData(WebkulCreate $subject, callable $proceed)
    {
        $persistentData = (array)$this->dataPersistor->get('seller_catalog_product');

        $fields = [
            "set" => "",
            "type" => "",
            "product" => [
                "name" => "",
                "category_ids" => [],
                "description" => "",
                "short_description" => "",
                "sku" => "",
                "price" => "",
                "special_price" => "",
                "special_from_date" => "",
                "special_to_date" => "",
                "product_has_weight" => 1,
                "brand" => "",
                "model_number" => "",
                "merchant_name" => "",
                "weight" => "",
                "mp_product_cart_limit" => "",
                "visibility" => "",
                "tax_class_id" => "",
                "meta_title" => "",
                "meta_keyword" => "",
                "meta_description" => "",
                "quantity_and_stock_status" => [
                    "is_in_stock" => 1,
                    "qty" => ""
                ],
                "image" => "",
                "small_image" => "",
                "thumbnail" => ""
            ],
        ];

        $this->dataPersistor->clear('seller_catalog_product');

        return $subject->setFieldsValue($persistentData, $fields);
    }
}
