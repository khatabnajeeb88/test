<?php
/**
 * Custom Attributes insertion into Product structure
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Plugin\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Webkul\MpMassUpload\Helper\Data as WebkulData;

class Data
{
    private const CONFIG_PATH = 'marketplace/massupload_customAttribute/validate_custom_attribute';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Insert Custom Attributes into Product Data Array
     *
     * @param WebkulData $subject
     * @param callable $proceed
     *
     * @param int $sellerId
     * @param int $profileId
     * @param int $mainRow
     * @param string $profileType
     *
     * @return array
     */
    public function aroundCalculateProductRowData(
        WebkulData $subject,
        callable $proceed,
        $sellerId,
        $profileId,
        $mainRow,
        $profileType
    ) {
        $uploadedFileRowData = $subject->getUploadedFileRowData($profileId);

        $data = [];
        if (!empty($uploadedFileRowData[$mainRow])) {
            $data = $uploadedFileRowData[$mainRow];
        }

        $i = 0;
        $customData = [];
        $customData['product'] = [];
        $csvAttributeList = [];

        foreach ($uploadedFileRowData[0] as $value) {
            $csvAttributeList[$value] = $value;
            if (!empty($data[$i])) {
                $customData['product'][$value] = $data[$i];
            } else {
                $customData['product'][$value] = '';
            }
            $i++;
        }

        $data = $customData;
        $validate = $subject->validateFields($data, $profileType, $mainRow);

        if ($validate['error']) {
            $wholeData['error'] = $validate['error'];
            $wholeData['msg'] = $validate['msg'];

            return $wholeData;
        }

        $data = $validate['data'];
        $returnValue = $proceed($sellerId, $profileId, $mainRow, $profileType);

        if ($returnValue) {
            $returnValue['product']['brand'] = $data['product']['brand'];
            $returnValue['product']['model_number'] = $data['product']['model_number'];
            $returnValue['product']['merchant_name'] = $data['product']['merchant_name'];
        }

        $returnValue = $subject->utf8Converter($returnValue);

        return $returnValue;
    }

    /**
     * Validate Custom Attribute
     *
     * @param WebkulData $subject
     * @param callable $proceed
     *
     * @param array $data
     * @param string $profileType
     * @param int $row
     *
     * @return array
     */
    public function aroundValidateFields(WebkulData $subject, callable $proceed, $data, $profileType, $row)
    {
        $hasWeight = array_key_exists('weight', $data['product']);
        $data = $subject->prepareProductDataIfNotSet($data, $profileType);

        // proceed only if physical products
        if (!(($profileType === 'configurable' && $hasWeight) || $profileType === 'simple')) {
            return $proceed($data, $profileType, $row);
        }

        // original attributes to be validated, insert new ones here
        $attributesToValidate = [
            'images' => $data['product']['images'],
            'price' => $data['product']['price']
        ];

        foreach ($attributesToValidate as $label => $attribute) {
            $validateData = $this->validateField($data, $row, $attribute, $label);
            if ($validateData) {
                return $validateData;
            }
        }

        $isEnabled = $this->scopeConfig->getValue(
            self::CONFIG_PATH,
            ScopeInterface::SCOPE_STORE
        );

        // if custom attributes validation is disabled, plugin finishes here
        if (!$isEnabled) {
            return $proceed($data, $profileType, $row);
        }

        // custom attributes to be validated, insert new ones here
        $customAttributesToValidate = [
            'brand' => $data['product']['brand'],
            'merchant_name' => $data['product']['merchant_name']
        ];

        foreach ($customAttributesToValidate as $label => $attribute) {
            $validateData = $this->validateField($data, $row, $attribute, $label);
            if ($validateData) {
                return $validateData;
            }
        }

        return $proceed($data, $profileType, $row);
    }

    /**
     * @param array $data
     * @param int $row
     * @param string $attribute
     * @param string $label
     *
     * @return array|null
     */
    private function validateField(
        array $data,
        int $row,
        string $attribute,
        string $label
    ) {
        if (strlen($attribute) <= 0) {
            $result['error'] = 1;
            $result['data'] = $data;
            $result['msg'] = __('Skipped row %1. product %2 can not be empty.', $row, $label);

            return $result;
        }

        return null;
    }

    /**
     * Inserted Custom Attributes into Product Array
     *
     * @param WebkulData $subject
     * @param array $data
     *
     * @return array
     */
    public function afterPrepareProductDataIfNotSet(WebkulData $subject, array $data)
    {
        $fields = [
            "product" => [
                "name" => "",
                "category" => "",
                "description" => "",
                "short_description" => "",
                "sku" => "",
                "stock" => "",
                "price" => "",
                "brand" => "",
                "model_number" => "",
                "merchant_name" => "",
                "special_price" => "",
                "special_from_date" => "",
                "special_to_date" => "",
                "weight" => 0,
                "mp_product_cart_limit" => "",
                "visibility" => "",
                "tax_class_id" => "",
                "meta_title" => "",
                "meta_keyword" => "",
                "meta_description" => "",
                "images" => "",
                "downloadable_link_file" => "",
                "links_title" => "",
                "links_purchased_separately" => "",
                "samples_title" => "",
                "downloadable_link_file" => "",
                "downloadable_link_price" => "",
                "downloadable_link_title" => "",
                "downloadable_link_type" => "",
                "downloadable_link_sample" => "",
                "downloadable_link_is_sharable" => "",
                "downloadable_link_is_unlimited" => "",
                "downloadable_link_number_of_downloads" => "",
                "downloadable_link_sample_type" => "",
                "custom_option" => "",
                "_super_attribute_option" => "",
                "_super_attribute_option" => ""
            ],
        ];

        $data = $subject->setFieldsValue($data, $fields);

        return $data;
    }
}
