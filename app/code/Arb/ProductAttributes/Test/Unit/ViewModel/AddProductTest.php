<?php
/**
 * This file consist of PHPUnit test case for viewModel class AddProduct
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Test\Unit\ViewModel;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\ProductAttributes\ViewModel\AddProduct;
use Arb\ProductAttributes\Helper\CustomAttribute;

/**
 * @covers \Arb\ProductAttributes\ViewModel\AddProduct
 */
class AddProductTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CustomAttribute
     */
    private $customAttributeHelper;

    /**
     * Object to test
     *
     * @var AddProduct
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->customAttributeHelper = $this->createMock(CustomAttribute::class);

        $this->testObject = $objectManager->getObject(AddProduct::class, [
            'customAttributeHelper' => $this->customAttributeHelper
        ]);
    }

    /**
     * @return array
     */
    public function getProductHintProvider()
    {
        return [
            [
                'Product hint'  // return value from config
            ],
            [
                null            // return value from config
            ]
        ];
    }

    /**
     * @dataProvider getProductHintProvider
     *
     * @param string|null $hint
     *
     * @return void
     */
    public function testGetProductHintBrand(?string $hint)
    {
        $this->customAttributeHelper->method('getProductHintBrand')->willReturn($hint);

        $getProductHintBrand = $this->testObject->getProductHintBrand();
        $this->assertInternalType('string', $getProductHintBrand);
        $this->assertSame($getProductHintBrand, (string)$hint);
    }

    /**
     * @dataProvider getProductHintProvider
     *
     * @param string|null $hint
     *
     * @return void
     */
    public function testGetProductHintModelNumber(?string $hint)
    {
        $this->customAttributeHelper->method('getProductHintModelNumber')->willReturn($hint);

        $getProductHintModelNumber = $this->testObject->getProductHintModelNumber();
        $this->assertInternalType('string', $getProductHintModelNumber);
        $this->assertSame($getProductHintModelNumber, (string)$hint);
    }

    /**
     * @dataProvider getProductHintProvider
     *
     * @param string|null $hint
     *
     * @return void
     */
    public function testGetProductHintMerchantName(?string $hint)
    {
        $this->customAttributeHelper->method('getProductHintMerchantName')->willReturn($hint);

        $getProductHintMerchantName = $this->testObject->getProductHintMerchantName();
        $this->assertInternalType('string', $getProductHintMerchantName);
        $this->assertSame($getProductHintMerchantName, (string)$hint);
    }
}
