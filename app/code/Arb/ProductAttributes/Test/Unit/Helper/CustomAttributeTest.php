<?php
/**
 * Helper for new Custom Attributes
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Test\Unit\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Helper methods with admin configuration for custom attributes
 */
class CustomAttributeTest extends TestCase
{

    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock storeManager
     *
     * @var \Magento\Store\Model\StoreManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManager;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\Config\Settings
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->getMockBuilder(\Magento\Framework\App\Config\ScopeConfigInterface::class)
                                  ->disableOriginalConstructor()
                                  ->setMethods([
                                      'getValue',
                                      'isSetFlag'
                                  ])
                                  ->getMock();
        
        $this->StoreInterface = $this->getMockBuilder(\Magento\Store\Api\Data\StoreInterface::class)
                                            ->disableOriginalConstructor()
                                            ->setMethods([
                                                'getId',
                                                'setId',
                                                'getCode',
                                                'setCode',
                                                'getName',
                                                'setName',
                                                'getWebsiteId',
                                                'setWebsiteId',
                                                'getStoreGroupId',
                                                'setStoreGroupId',
                                                'setIsActive',
                                                'getIsActive',
                                                'getExtensionAttributes',
                                                'setExtensionAttributes',
                                                "getBaseUrl"
                                            ])
                                            ->getMock();
        $this->testObject = $this->objectManager->getObject(
            \Arb\ProductAttributes\Helper\CustomAttribute::class,
            [
                'scopeConfig' => $this->scopeConfig
            ]
        );
    }


    /**
     * @return string|null
     */
    public function testGetProductHintBrand()
    {
        $xmlConfigPath = "marketplace/producthint_settings/product_brand";
        $this->scopeConfig->method('getValue')->with($xmlConfigPath)->willReturn(
            "foo"
        );
        $result = $this->testObject->getProductHintBrand();
    }

    /**
     * @return string|null
     */
    public function testGetProductHintModelNumber()
    {
        $xmlConfigPath = "marketplace/producthint_settings/product_model_number";
        $this->scopeConfig->method('getValue')->with($xmlConfigPath)->willReturn(
            "foo"
        );
        $result = $this->testObject->getProductHintModelNumber();
    }

    /**
     * @return string|null
     */
    public function testGetProductHintMerchantName()
    {
        $xmlConfigPath = "marketplace/producthint_settings/product_merchant_name";
        $this->scopeConfig->method('getValue')->with($xmlConfigPath)->willReturn(
            "foo"
        );
        $result = $this->testObject->getProductHintMerchantName();
    }
}
