<?php
/**
 * Event Observer for Product Attributes Test file
 * @category    Arb
 * @package     Arb_ProductAttributes
 */


namespace Arb\ProductAttributes\Test\Unit\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use Magento\Store\Model\StoreManagerInterface;
use Arb\ProductAttributes\Observer\SetCategoryName;
use Magento\Catalog\Model\ResourceModel\Product\Action;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class SetCategoryNameTest for testing SetCategoryName class
 * @covers \Arb\ProductAttributes\Observer\SetCategoryName
 */
class SetCategoryNameTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test setUp
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->storeManager = $this->getMockBuilder(StoreManagerInterface::class)
        ->setMethods([
                "getStores"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->productActionMock = $this->getMockBuilder(Action::class)
        ->setMethods([
                "updateAttributes"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->collectionFactory = $this->getMockBuilder(CollectionFactory::class)
        ->setMethods([
                "create"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->eventMock = $this->getMockBuilder(Observer::class)
            ->setMethods(["getProduct","setStoreId","getCategoryIds","setCategories","getCategoryCollection"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->categoryCollectionMock = $this->objectManager->getCollectionMock(
            \Magento\Catalog\Model\ResourceModel\Category\Collection::class,
            [$this->createMock(\Magento\Catalog\Model\Category::class)]
        );
        
        $this->storeManager->method("getStores")->willReturn([1,2]);
        $this->setCategoryNameMock = $this->objectManager->getObject(
            SetCategoryName::class,
            [
                "storeManager"=>$this->storeManager,
                "productAction"=>$this->productActionMock,
                "categoryCollection"=>$this->collectionFactory
            ]
        );
    }
    /**
     * test execute function
     */
    public function testExecute()
    {
        $this->eventMock->method("getProduct")->willReturnSelf();
        $this->collectionFactory->method("create")->willReturn($this->categoryCollectionMock);
        $this->categoryCollectionMock->method("addAttributeToSelect")->willReturnSelf();
        $this->categoryCollectionMock->method("addAttributeToFilter")->willReturnSelf();
        $this->categoryCollectionMock->method("setStore")->willReturnSelf();
        $this->categoryCollectionMock->method("getColumnValues")->willReturn(["test1","test2","test3"]);
        $this->eventMock->method("setStoreId")->willReturnSelf();
        $this->eventMock->method("setCategories")->willReturnSelf();
        $this->eventMock->method("getCategoryIds")->willReturn([2,3,4]);
        $this->setCategoryNameMock->execute($this->eventMock);
    }

    /**
     * test execute function
     */
    public function testExecuteNullCategory()
    {
        $this->eventMock->method("getProduct")->willReturnSelf();
        $this->collectionFactory->method("create")->willReturn($this->categoryCollectionMock);
        $this->categoryCollectionMock->method("addAttributeToSelect")->willReturn($this->categoryCollectionMock);
        $this->categoryCollectionMock->method("addAttributeToFilter")->willReturn($this->categoryCollectionMock);
        $this->categoryCollectionMock->method("setStoreId")->willReturn($this->categoryCollectionMock);
        $this->categoryCollectionMock->method("getColumnValues")->willReturn(["test1","test2","test3"]);
        $this->eventMock->method("setStoreId")->willReturnSelf();
        $this->eventMock->method("setCategories")->willReturnSelf();
        $this->eventMock->method("getCategoryIds")->willReturn([]);
        $this->setCategoryNameMock->execute($this->eventMock);
    }
}
