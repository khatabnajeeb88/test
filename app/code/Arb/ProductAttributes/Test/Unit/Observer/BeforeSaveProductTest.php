<?php
/**
 * Test class BeforeSaveProduct
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Test\Unit\Observer;

use Arb\ProductAttributes\Observer\BeforeSaveProduct;
use Magento\Catalog\Model\Product;
use Magento\Framework\Api\AttributeInterface;
use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;

/**
 * Testing if methods inside a class works correctly
 */
class BeforeSaveProductTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Observer
     */
    private $observerMock;

    /**
     * Object to test
     *
     * @var BeforeSaveProduct
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->observerMock = $this->createMock(Observer::class);

        $this->requestMock =$this->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
                                  ->disableOriginalConstructor()
                                  ->setMethods([
                                  ])
                                  ->getMock();

        $this->sessionMock = $this->getMockBuilder(\Magento\Customer\Model\Session::class)
        ->disableOriginalConstructor()
        ->setMethods([
            'getCustomerId'
        ])->getMock();

        $this->testObject = $objectManager->getObject(BeforeSaveProduct::class,
            [
                "session" => $this->sessionMock,
                "request" => $this->requestMock
            ]
        );
    }

    /**
     * @dataProvider dataProviderForTestExecute
     *
     * @param string|null $attribute
     * @param int $expected
     *
     * @throws ReflectionException
     */
    public function testExecute(?string $attribute, int $expected)
    {
        $eventMock = $this->getMockBuilder(Event::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProduct','getData'])
            ->getMock();
        $this->observerMock->expects($this->once())->method('getEvent')->willReturn($eventMock);

        $this->sessionMock->expects($this->any())
            ->method('getCustomerId')
            ->willReturn(2);

        $productMock = $this->createMock(Product::class);
        $eventMock->expects($this->once())->method('getProduct')->willReturn($productMock);

        $productMock->expects($this->once())->method('getCustomAttribute')->willReturn($attribute);

        $productMock->expects($this->exactly($expected))->method('getData');
        $productMock->expects($this->exactly($expected))->method('setCustomAttribute');

        $this->testObject->execute($this->observerMock);
    }

    /**
     * @dataProvider dataProviderForTestExecute
     *
     * @param string|null $attribute
     * @param int $expected
     *
     * @throws ReflectionException
     */
    public function testExecuteElse(?string $attribute, int $expected)
    {
        $eventMock = $this->getMockBuilder(Event::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProduct','getData'])
            ->getMock();
        $this->observerMock->expects($this->once())->method('getEvent')->willReturn($eventMock);

        $this->sessionMock->expects($this->any())
            ->method('getCustomerId')
            ->willReturn(null);

        $productMock = $this->createMock(Product::class);
        $eventMock->expects($this->once())->method('getProduct')->willReturn($productMock);

        $productMock->expects($this->once())->method('getCustomAttribute')->willReturn($attribute);

        $productMock->expects($this->exactly($expected))->method('getData');
        $productMock->expects($this->exactly($expected))->method('setCustomAttribute');

        $this->testObject->execute($this->observerMock);
    }

    /**
     * @dataProvider dataProviderForTestExecute
     *
     * @param string|null $attribute
     * @param int $expected
     *
     * @throws ReflectionException
     */
    public function testExecuteSeller(?string $attribute, int $expected)
    {
        $eventMock = $this->getMockBuilder(Event::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProduct','getData'])
            ->getMock();
        $this->observerMock->expects($this->once())->method('getEvent')->willReturn($eventMock);

        $this->sessionMock->expects($this->any())
            ->method('getCustomerId')
            ->willReturn(null);

        $productMock = $this->createMock(Product::class);
        $eventMock->expects($this->once())->method('getProduct')->willReturn($productMock);

        $productMock->expects($this->once())->method('getCustomAttribute')->willReturn($attribute);

        $productMock->expects($this->exactly($expected))->method('getData')->willReturn([
            "assign_seller"=>"2"]);
        $productMock->expects($this->exactly($expected))->method('setCustomAttribute');

        $this->testObject->execute($this->observerMock);
    }

    /**
     * @return array
     */
    public function dataProviderForTestExecute()
    {
        return [
            [                   // Scenario 0: attribute not assigned
                null,           // attribute
                1               // expected method execution number
            ],
            [                   // Scenario 1: attribute assigned
                'something',    // attribute
                0               // expected method execution number
            ]
        ];
    }
}
