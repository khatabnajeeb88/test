<?php
/**
 * This file consist of PHPUnit test case for plugin class Data
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Test\Unit\Plugin\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Webkul\MpMassUpload\Helper\Data as WebkulData;
use Arb\ProductAttributes\Plugin\Helper\Data as DataPlugin;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * @covers \Arb\ProductAttributes\Plugin\Helper\Data
 */
class DataTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulData
     */
    private $webkulData;

    /**
     * Object to test
     *
     * @var DataPlugin
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->createMock(ScopeConfigInterface::class);

        $this->webkulData = $this->createMock(WebkulData::class);

        $this->testObject = $objectManager->getObject(DataPlugin::class, [
            'scopeConfig' => $this->scopeConfig
        ]);
    }

    /**
     * @return array
     */
    public function aroundCalculateProductRowDataProvider()
    {
        return [
            [   // Scenario 1: There is error thrown
                ['error' => 1, 'msg' => 'ERROR', 'data' => 'empty']
            ],
            [   // Scenario 2: There is no error thrown
                ['error' => 0, 'data' => ['product' => ['brand' => '', 'model_number' => '', 'merchant_name' => '']]]
            ]
        ];
    }

    /**
     * @dataProvider aroundCalculateProductRowDataProvider
     *
     * @param array $dataArray
     *
     * @return void
     */
    public function testAroundCalculateProductRowData(array $dataArray)
    {
        $stubArray[0] = [0 => '', 1 => 'brand'];

        $this->webkulData->expects($this->once())->method('getUploadedFileRowData')->willReturn($stubArray);
        $this->webkulData->expects($this->once())->method('validateFields')->willReturn($dataArray);

        $proceed = function () {
            return [
                'product' => [
                    'brand' => '',
                    'model_number' => '',
                    'merchant_name' => ''
                ]
            ];
        };

        $this->webkulData->method('utf8Converter')->willReturn([]);

        $aroundCalculateProductRowData = $this->testObject->aroundCalculateProductRowData(
            $this->webkulData,
            $proceed,
            1,
            1,
            0,
            'simple'
        );
        $this->assertInternalType('array', $aroundCalculateProductRowData);
    }

    /**
     * @return array
     */
    public function aroundValidateFieldsProvider()
    {
        return [
            [   // Scenario 1: Simple Product import with properly set attributes -> validate
                1,                                                  // is Custom Attribute Validation enabled
                'simple',                                           // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data with weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => 'some name',
                        'images' => 'image.jpg',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 2A: Simple Product import with incorrectly set Brand -> Error
                1,                                                  // is Custom Attribute Validation enabled
                'simple',                                           // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => '',
                        'merchant_name' => 'some name',
                        'images' => 'image.jpg',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 2B: Simple Product import with incorrectly set Merchant Name -> Error
                1,                                                  // is Custom Attribute Validation enabled
                'simple',                                           // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => '',
                        'images' => 'image.jpg',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 2C: Simple Product import with incorrectly set Price -> Error
                0,                                                  // irrelevant
                'simple',                                           // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => 'name',
                        'images' => 'image.jpg',
                        'price' => ''
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 2D: Simple Product import with incorrectly set Images -> Error
                0,                                                  // irrelevant
                'simple',                                           // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => 'name',
                        'images' => '',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 3: Configurable Product import with Virtual Products -> no validation
                1,                                                  // irrelevant
                'configurable',                                     // Profile Type
                ['product' => ['some data' => '']],                 // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => '',
                        'merchant_name' => '',
                        'images' => '',
                        'price' => ''
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 4: Configurable Product import with Simple Products and properly set attributes -> validate
                1,                                                  // is Custom Attribute Validation enabled
                'configurable',                                     // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => 'some name',
                        'images' => 'image.jpg',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 5A: Configurable Product import with Simple Products and incorrectly set Brand -> Error
                1,                                                  // is Custom Attribute Validation enabled
                'configurable',                                     // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => '',
                        'merchant_name' => 'some name',
                        'images' => 'image.jpg',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 5B: Configurable Product import with Simple Products and incorrectly set Merchant -> Error
                1,                                                  // is Custom Attribute Validation enabled
                'configurable',                                     // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => '',
                        'images' => 'image.jpg',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 5C: Configurable Product import with Simple Products and incorrectly set Images -> Error
                1,                                                  // irrelevant
                'configurable',                                     // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => 'some name',
                        'images' => '',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 5D: Configurable Product import with Simple Products and incorrectly set Price -> Error
                1,                                                  // irrelevant
                'configurable',                                     // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => 'some brand',
                        'merchant_name' => 'some name',
                        'images' => 'something.jpg',
                        'price' => ''
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 6: Product Type other than Simple/Configurable -> no validation
                1,                                                  // irrelevant
                'downloadable',                                     // Profile Type
                ['product' => ['some data' => '']],                 // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => '',
                        'merchant_name' => '',
                        'images' => '',
                        'price' => ''
                    ]
                ]                                                   // Processed Product Data array
            ],
            [   // Scenario 7: Custom Attributes Validation is disabled -> no validation for custom attributes
                0,                                                  // is Custom Attribute Validation enabled
                'simple',                                           // Profile Type
                ['product' => ['weight' => '']],                    // Pre-Processed Product Data without weight
                ['product' =>
                    [
                        'brand' => '',
                        'merchant_name' => '',
                        'images' => 'something.jpg',
                        'price' => '15'
                    ]
                ]                                                   // Processed Product Data array
            ]
        ];
    }

    /**
     * @dataProvider aroundValidateFieldsProvider
     *
     * @param int $isEnabled
     * @param string $profileType
     * @param array $preProcessedData
     * @param array $processedData
     *
     * @return void
     */
    public function testAroundValidateFields(
        int $isEnabled,
        string $profileType,
        array $preProcessedData,
        array $processedData
    ) {
        $this->scopeConfig->method('getValue')->willReturn($isEnabled);

        $proceed = function () {
            return [];
        };

        $this->webkulData->method('prepareProductDataIfNotSet')->willReturn($processedData);

        $aroundValidateFields = $this->testObject->aroundValidateFields(
            $this->webkulData,
            $proceed,
            $preProcessedData,
            $profileType,
            0
        );
        $this->assertInternalType('array', $aroundValidateFields);
    }

    /**
     * @return void
     */
    public function testAfterPrepareProductDataIfNotSet()
    {
        $this->webkulData->expects($this->once())->method('setFieldsValue')->willReturn([]);

        $afterPrepareProductDataIfNotSet = $this->testObject->afterPrepareProductDataIfNotSet($this->webkulData, []);
        $this->assertInternalType('array', $afterPrepareProductDataIfNotSet);
    }
}
