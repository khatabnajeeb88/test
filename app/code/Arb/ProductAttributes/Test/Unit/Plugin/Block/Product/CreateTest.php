<?php
/**
 * This file consist of PHPUnit test case for plugin class Create
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Test\Unit\Plugin\Block\Product;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Webkul\Marketplace\Block\Product\Create as WebkulCreate;
use Arb\ProductAttributes\Plugin\Block\Product\Create as CreatePlugin;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * @covers \Arb\ProductAttributes\Plugin\Block\Product\Create
 */
class CreateTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * Object to test
     *
     * @var CreatePlugin
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->dataPersistor = $this->createMock(DataPersistorInterface::class);

        $this->testObject = $objectManager->getObject(CreatePlugin::class, [
            'dataPersistor' => $this->dataPersistor
        ]);
    }

    /**
     * @return void
     */
    public function testAroundGetPersistentData()
    {
        $this->dataPersistor->expects($this->once())->method('get');
        $this->dataPersistor->expects($this->once())->method('clear');

        /** @var PHPUnit_Framework_MockObject_MockObject|WebkulCreate $subjectMock */
        $subjectMock = $this->createMock(WebkulCreate::class);

        $subjectMock->expects($this->once())->method('setFieldsValue')->willReturn([]);

        $proceed = function () use ($subjectMock) {
            return true;
        };

        $aroundGetPersistentData = $this->testObject->aroundGetPersistentData($subjectMock, $proceed);
        $this->assertInternalType('array', $aroundGetPersistentData);
    }
}
