<?php
/**
 * Helper for new Custom Attributes
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Helper methods with admin configuration for custom attributes
 */
class CustomAttribute extends AbstractHelper
{
    /**
     * @return string|null
     */
    public function getProductHintBrand()
    {
        return $this->scopeConfig->getValue(
            'marketplace/producthint_settings/product_brand',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string|null
     */
    public function getProductHintModelNumber()
    {
        return $this->scopeConfig->getValue(
            'marketplace/producthint_settings/product_model_number',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string|null
     */
    public function getProductHintMerchantName()
    {
        return $this->scopeConfig->getValue(
            'marketplace/producthint_settings/product_merchant_name',
            ScopeInterface::SCOPE_STORE
        );
    }
}
