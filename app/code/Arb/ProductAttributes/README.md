## Synopsis
An extension to install product attributes via setup scripts.

### Following Attributes are created
1. features - To add features of each product.
2. vendor_country - To Assign country to product.
3. bestseller - Attribute to assign product as top product and these top products can be configured by Magento admin manually.
4. arb_subcategory - Add subcategory to product at attribute level.
5. New product attributes are created with attribute set for Physical Products

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).


