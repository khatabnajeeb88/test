<?php
/**
 * Observer which checks if custom attribute merchant_id is set
 *
 * @category Arb
 * @package Arb_ProductAttributes
 * @author Arb Magento Team
 *
 */

namespace Arb\ProductAttributes\Observer;

use Magento\Catalog\Api\Data\ProductExtensionFactory;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Assigning attribute value if it is empty
 */
class BeforeSaveProduct implements ObserverInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * BeforeSaveProduct constructor.
     *
     * @param Session $session
     * @param RequestInterface $request
     */
    public function __construct(
        Session $session,
        RequestInterface $request
    ) {
        $this->session = $session;
        $this->request = $request;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();
        $attribute = $product->getCustomAttribute('merchant_id');

        if ($attribute === null) {
            // saving merchant_id custom attribute
            $customerId = $this->session->getCustomerId();
            $assignedSeller = $product->getData('assign_seller');
            if ($customerId !== null) { // saving on marketplace (add / edit product)
                $product->setCustomAttribute('merchant_id', $customerId);
            } elseif ($assignedSeller !== null) { // saving on magento admin (OOTB magento add/edit)
                $product->setCustomAttribute('merchant_id', reset($assignedSeller));
            } else { // saving on marketplace mass upload
                $product->setCustomAttribute('merchant_id', $this->request->getParam('seller_id'));
            }
        }
    }
}
