<?php
/**
 * Event Observer for Product Attributes
 * @category    Arb
 * @package     Arb_ProductAttributes
 */

namespace Arb\ProductAttributes\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ResourceModel\Product\Action;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

class SetCategoryName implements ObserverInterface
{
    
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Action
     */
    private $productAction;
    
    /**
     * @var CollectionFactory
     */
    private $categoryCollection;

    /**
     * Assign category name
     *
     * @param StoreManagerInterface $storeManager
     * @param Action $productAction
     * @param CollectionFactory $categoryCollection
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        Action $productAction,
        CollectionFactory $categoryCollection
    ) {
        $this->storeManager = $storeManager;
        $this->productAction = $productAction;
        $this->categoryCollection = $categoryCollection;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_CategoryAssign_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }
    public function execute(Observer $observer)
    {
        //Get Store IDs
        $storeIds = array_keys($this->storeManager->getStores());
        $categoryIds = [];
        //Get current Product
        $product = $observer->getProduct();
        $productNameId = $product->getName()." - ".$product->getId();
        foreach ($storeIds as $storeId) {
            $categoryIds = $product->setStoreId($storeId)->getCategoryIds();
            if (count($categoryIds)) {
                //Filter by current store id.
                $categoryCollection = $this->categoryCollection->create()
                    ->addAttributeToSelect('name')
                    ->addAttributeToFilter("entity_id", ["in"=>$categoryIds])
                    ->setStore($storeId);
                //Setting up category name as per language.
                $categoryList = implode(" ", $categoryCollection->getColumnValues("name"));
                //Assigning categories to the product.
                $this->productAction->updateAttributes([$product->getId()], ['categories' => $categoryList], $storeId);
                $this->logger->info($productNameId." - Product has assigned category.- ".$categoryList);
            } else {
                //Unassigning categories to the product.
                $this->productAction->updateAttributes([$product->getId()], ['categories' => ""], $storeId);
                $this->logger->info($productNameId." - Product does not have assigned category.");
            }
        }
    }
}
