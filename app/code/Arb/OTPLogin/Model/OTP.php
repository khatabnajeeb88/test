<?php
/**
 * Sends and validates an OTP
 *
 * @category Arb
 * @package Arb_OTPLogin
 * @author Arb Magento Team
 *
 */

namespace  Arb\OTPLogin\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;

/**
 * OTP class to send and validate OTP
 */
class OTP extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Default code for successful validation
     */
    const DEFAULT_SUCCESS_CODE = 'I000000';

    /**
     * Input string for generating random string
     */
    const RANDOM_STRING = 'abcdef0123456789';

    /**
     * Get OTP request content type
     */
    const REQUEST_CONTENT_TYPE = 'application/json';

    /**
     * Get OTP request channel id
     */
    const REQUEST_CHANNEL_ID = 'MAGENTO';

    /**
     * Get OTP request sub channel id
     */
    const REQUEST_SUBCHANNEL_ID = 'MAGENTO';

    /**
     * Get OTP request purpose
     */
    const REQUEST_PURPOSE = 'MAK';

    /**
     * Saudi ISD code
     */
    const SAUDI_ISD_CDOE = '+966';

    /**
     * OTP end point url
     */
    const XML_PATH_OTPLOGIN_URL = 'esbotp/otplogin/url';

    /**
     * OTP login IBM client id
     */
    const XML_PATH_IBM_CLIENT_ID = 'esbotp/otplogin/ibm_client_id';

    /**
     * OTP login IBM client secret
     */
    const XML_PATH_IBM_CLIENT_SECRET = 'esbotp/otplogin/ibm_client_secret';

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curl;

    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $random;

    /**
     * @var \Magento\Customer\Model\Authentication
     */
    protected $authenticateModel;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManagerInterface;

   /**
    * @var \Magento\Framework\Serialize\Serializer\Json
    */
    protected $json;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

   /**
    * @var \Magento\Framework\Encryption\EncryptorInterface
    */
    protected $encryptor;

    /**
     * Send OTP class constructor function
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Framework\Math\Random $random
     * @param \Magento\Customer\Model\Authentication $authenticateModel,
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Math\Random $random,
        \Magento\Customer\Model\Authentication $authenticateModel,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->customerRepository = $customerRepository;
        $this->curl = $curl;
        $this->random = $random;
        $this->authenticateModel = $authenticateModel;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->scopeConfig = $scopeConfig;
        $this->json = $json;
        $this->encryptor = $encryptor;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/ESB_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }
    /**
     * Sends OTP to merchant
     *
     * @param string $emailId
     * @param string $password
     * @return array
     */
    public function sendOTPToMerchant($emailId, $password)
    {
        $mobileData = $this->getCustomerPhone($emailId, $password);
        if ($mobileData['mobile_number'] != null) {
            return $this->generateOTP($mobileData['mobile_number']);
        } else {
            return $mobileData;
        }
    }

    /**
     * Returns customer mobile number or error details
     *
     * @param  string $emailId
     * @param string $password
     * @return array
     */
    protected function getCustomerPhone($emailId, $password)
    {
        $return = [
            'mobile_number' => null,
            'message' => null,
            'status' => false,
            'merchant' => false,
            "log_message"=>null
        ];
        $this->logger->info("User Email - ".$emailId);
        try {
            $customer = $this->customerRepository->get($emailId);
            $customerId = $customer->getId();
            $this->authenticateModel->authenticate($customerId, $password);
            
            if (null != $customer->getCustomAttribute('phone_number')) {
                $return['mobile_number'] = $customer->getCustomAttribute('phone_number')->getValue();
                $return['status'] = true;
                $return['merchant'] = true;
            } else {
                $return['message'] = __('You do not have mobile number registered with us');
                $return['log_message'] = 'You do not have mobile number registered with us';
            }
        } catch (NoSuchEntityException $e) {
            $return['message'] = __('Invalid email or password.');
            $return['log_message'] = 'Invalid email or password.';
        } catch (LocalizedException $e) {
            $return['message'] = __($e->getMessage());
            $return['log_message'] = $e->getMessage();
        }
        $this->logger->info("generateOTP - ".$return['log_message']);
        return $return;
    }

    /**
     * Sends OTP to merchant
     *
     * @param string $mobileNumber
     * @return void
     */
    public function generateOTP($mobileNumber)
    {
        $return = [
            'status' => false,
            'message' => null,
            'merchant' => true,
            "log_message"=>null
        ];
        $data = $this->getConfiguration();
        $headers = $this->getRequestHeaders($data, $mobileNumber);
        $esbEndPoint = $data['url'];
        $data = [
            'purpose' => self::REQUEST_PURPOSE,
            'mobileNumber' => self::SAUDI_ISD_CDOE.$mobileNumber
        ];
        $postData = $this->json->serialize($data);
        try {
            $this->curl->setHeaders($headers);
            $this->curl->post($esbEndPoint, $postData);
            $response = $this->curl->getBody();
            $responseData = $this->json->unserialize($response);
            if (isset($responseData['header']['statusCode'])) {
                $status = $responseData['header']['statusCode'];
                if ($status == self::DEFAULT_SUCCESS_CODE) {
                    $return['status'] = true;
                    $return['message'] = __('OTP send to your registered mobile number');
                    $return['log_message'] = 'OTP send to your registered mobile number';
                } else {
                    $return['message'] =
                    isset($responseData['header']['statusDescription'])
                    ?$status." - ".__($responseData['header']['statusDescription']):$status;
                    $return['log_message'] = $return['message'];
                }
            } else {
                $return['message'] = __('Unable to send an OTP, Please try after sometime.');
                $return['log_message'] = 'Unable to send an OTP, Please try after sometime.';
            }
        } catch (\Exception $e) {
            $return['message'] = __($e->getMessage());
            $return['log_message'] = $e->getMessage();
        }
        $this->logger->info("generateOTP - ".$return['log_message']);
        return $return;
    }

    /**
     * Get OTP service headers
     *
     * @param array $config
     * @param string $mobileNumber
     * @return array
     */
    private function getRequestHeaders($config, $mobileNumber = null)
    {
        $storeCode = $this->storeManagerInterface->getStore()->getCode();
        $headers = [
            'X-IBM-Client-Id' => $config['client_id'],
            'requestID' => $this->getRandomString(),
            'sessionLanguage' => $this->getStoreCode($storeCode),
            'X-IBM-Client-Secret'=> $config['client_secret'],
            'userMobile' => self::SAUDI_ISD_CDOE.$mobileNumber,
            'Content-Type' => self::REQUEST_CONTENT_TYPE,
            'sessionID' => $this->getRandomString(),
            'IDNumber' => $mobileNumber,
            'channelID' => self::REQUEST_CHANNEL_ID,
            'subChannelID' => self::REQUEST_SUBCHANNEL_ID
        ];
        
        return $headers;
    }

    /**
     * Get Store code
     *
     * @param string $code
     * @return string
     */
    private function getStoreCode($code)
    {
        switch ($code) {
            case 'ar_SA':
                $storeCode = "AR";
                break;
            default:
                $storeCode = "EN";
                break;
        }

        return $storeCode;
    }

    /**
     * Get Random String
     *
     * @param void
     * @return string
     */
    private function getRandomString()
    {
        $id = $this->random->getRandomString(8, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(12, self::RANDOM_STRING);
        return $id;
    }

    /**
     * Returns configuration
     *
     * @return array
     */
    protected function getConfiguration()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;
        $data['url'] = $this->scopeConfig->getValue(self::XML_PATH_OTPLOGIN_URL, $storeScope);
        $clientId = $this->scopeConfig->getValue(self::XML_PATH_IBM_CLIENT_ID, $storeScope);
        $clientSecret = $this->scopeConfig->getValue(self::XML_PATH_IBM_CLIENT_SECRET, $storeScope);
        $data['client_secret'] = $this->encryptor->decrypt($clientSecret);
        $data['client_id'] = $this->encryptor->decrypt($clientId);
        return $data;
    }

    /**
     * Validate merchant OTP
     *
     * @param string $otp
     * @param string $email
     * @param string $telephone
     * @return array
     */
    public function validateOTP($otp, $email = null, $telephone = null)
    {
        return $this->sendValidationOTPRequest($otp, $email, $telephone);
    }

    /**
     * Send OTP validation request to ESB
     *
     * @param  string $otp
     * @param  string $email
     * @param  string $telephone
     * @return array
     */
    private function sendValidationOTPRequest($otp, $email = null, $mobileNumber = null)
    {
        $return = [
            'status' => false,
            'message' => null,
            "log_message"=>null
        ];
        try {
            if (empty($mobileNumber)) {
                $customer = $this->customerRepository->get($email);
                $mobileNumber = $customer->getCustomAttribute('phone_number')->getValue();
            }
        } catch (NoSuchEntityException $e) {
            $return['message'] = __('Merchant does not exist with provided email id');
            $return['log_message'] = 'Merchant does not exist with provided email id';
            return $return;
        }
        $data = $this->getConfiguration();
        $headers = $this->getRequestHeaders($data, $mobileNumber);
        $esbEndPoint = $data['url'].'/'.$otp.'?purpose='.self::REQUEST_PURPOSE;
        try {
            $this->curl->setHeaders($headers);
            $this->curl->get($esbEndPoint);
            $response = $this->curl->getBody();
            $responseData = $this->json->unserialize($response);
            if (isset($responseData['header']['statusCode'])) {
                $status = $responseData['header']['statusCode'];
                if ($status == self::DEFAULT_SUCCESS_CODE) {
                    $return['status'] = true;
                } else {
                    $return['message'] =
                    isset($responseData['header']['statusDescription'])
                    ? $status." - ".__($responseData['header']['statusDescription']):$status;
                    $return['log_message'] = $return['message'];
                }
            } else {
                $return['message'] = __('Unable to validate OTP, Please try after sometime.');
                $return['log_message'] = 'Unable to validate OTP, Please try after sometime.';
            }
        } catch (\Exception $e) {
            $return['message'] = $e->getMessage();
        }
        $this->logger->info("sendValidationOTPRequest - ".$return['log_message']);
        return $return;
    }
}
