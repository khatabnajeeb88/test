<?php

/**
 * Ajax controller to request OTP
 *
 * @category Arb
 * @package Arb_OTPLogin
 * @author Arb Magento Team
 *
 */

namespace Arb\OTPLogin\Controller\GetOtp;

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    const CRYPTO_PARAPHRSAE = "this is a secret key";
    /**
     * @var \Arb\OTPLogin\Model\SendOTP
     */
    protected $sendOTP;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;
     /**
     * @var \Arb\OTPLogin\Helper\CryptoJsAes
     */
    protected $cryptoHelper;

    /**
     * Ajax class controller constructor
     *
     * @param Context $context
     * @param \Arb\OTPLogin\Model\OTP $sendOTP
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        \Arb\OTPLogin\Model\OTP $sendOTP,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Arb\OTPLogin\Helper\CryptoJsAes $cryptoHelper
    ) {
        parent::__construct($context);
        $this->sendOTP = $sendOTP;
        $this->formKeyValidator = $formKeyValidator;
        $this->cryptoHelper = $cryptoHelper;
    }
    public function execute()
    {
        $email = $this->getRequest()->getParam('email');
        $password = $this->cryptoHelper->decrypt($this->getRequest()->getParam('password'),self::CRYPTO_PARAPHRSAE);
        $formKey = $this->getRequest()->getParam('form_key');
        if ($this->formKeyValidator->validate($this->getRequest())) {
            if (!\Zend_Validate::is(trim($email), 'EmailAddress')) {
                $response = $this->resultFactory
                    ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                    ->setData([
                        'merchant' => false,
                        'status'  => false,
                        'message' => ''
                    ]);
                return $response;
            }
            if ((null == $password) || (!$password)) {
                $response = $this->resultFactory
                    ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                    ->setData([
                        'merchant' => false,
                        'status'  => false,
                        'message' => ''
                    ]);
                return $response;
            }
            $otp = $this->sendOTP->sendOTPToMerchant($email, $password);
            if ($otp['status']) {
                $merchant = $otp['merchant'];
                $status = true;
                $message = $otp['message'];
            } else {
                $status = false;
                $merchant = $otp['merchant'];
                $message = $otp['message'];
            }
            $response = $this->resultFactory
                ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                ->setData([
                    'merchant' => $merchant,
                    'status'  => $status,
                    'message' => __($message)
                ]);
            return $response;
        } else {
            $response = $this->resultFactory
                ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                ->setData([
                    'merchant' => false,
                    'status'  => false,
                    'message' => __('Invalid Form Key')
                ]);
            return $response;
        }
    }
}
