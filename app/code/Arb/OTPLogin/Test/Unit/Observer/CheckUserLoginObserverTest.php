<?php
/**
 * Captcha validation check
 *
 * @category Arb
 * @package Arb_OTPLogin
 * @author Arb Magento Team
 *
 */

namespace Arb\OTPLogin\Test\Unit\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Arb\OTPLogin\Observer\CheckUserLoginObserver;
use Magento\Captcha\Helper\Data;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Captcha\Observer\CaptchaStringResolver;
use Magento\Customer\Model\Url;
use Magento\Framework\UrlInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class CheckUserLoginObserverTest for testing CheckUserLoginObserver class
 * @covers \Arb\OTPLogin\Observer\CheckUserLoginObserver
 */
class CheckUserLoginObserverTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test setUp
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->captchaMock = $this->getMockBuilder(Data::class)
            ->setMethods([
                "getCaptcha",
                "isRequired",
                "isCorrect",
                "logAttempt"
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $this->actionFlagMock = $this->getMockBuilder(ActionFlag::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->managerInterfaceMock = $this->getMockBuilder(ManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->sessionManagerInterfaceMock = $this->getMockBuilder(SessionManagerInterface::class)
        ->setMethods([
                "setUsername"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->captchaStringResolverMock = $this->getMockBuilder(CaptchaStringResolver::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlMock = $this->getMockBuilder(Url::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlInterfaceMock = $this->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventMock = $this->getMockBuilder(\Magento\Framework\Event\Observer::class)
            ->setMethods(["getControllerAction","getRequest","getResponse","setRedirect"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock = $this
            ->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
            ->setMethods(["getPost"])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
            ->setMethods(["getId","get"])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->customerRepositoryMock->method("get")->willReturnSelf();
        $this->checkUserLoginObserverMock = $this->objectManager->getObject(
            CheckUserLoginObserver::class,
            [
                "_helper"=>$this->captchaMock,
                "_actionFlag"=>$this->actionFlagMock,
                "messageManager"=>$this->managerInterfaceMock,
                "_session"=>$this->sessionManagerInterfaceMock,
                "captchaStringResolver"=>$this->captchaStringResolverMock,
                "_customerUrl"=>$this->urlMock,
                "_urlInterface"=>$this->urlInterfaceMock,
                "customerRepository"=>$this->customerRepositoryMock
            ]
        );
    }
    /**
     * testExecute method
     */
    public function testExecute()
    {
        $this->eventMock->method("getControllerAction")->willReturnSelf();
        $this->eventMock->method("getResponse")->willReturnSelf();
        $this->eventMock->method("setRedirect")->willReturnSelf();
        $this->eventMock->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $this->requestMock->method("getPost")->willReturn(["username"=>"teste"]);
        $this->captchaMock->method("getCaptcha")->willReturnSelf();
        $this->captchaMock->method("isRequired")->willReturn(true);
        $this->captchaMock->method("isCorrect")->willReturn(false);
        $this->captchaStringResolverMock->method("resolve")->willReturnSelf();
        $this->checkUserLoginObserverMock->execute($this->eventMock);
    }
}
