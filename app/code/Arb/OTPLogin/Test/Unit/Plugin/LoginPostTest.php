<?php
/**
 * @category Arb
 * @package Arb_OTPLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\OTPLogin\Test\Unit\Plugin;

use Magento\Framework\App\Action\Context;
use Magento\Framework\UrlInterface;
use Arb\OTPLogin\Model\OTP;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Controller\ResultFactory;
use Arb\OTPLogin\Plugin\LoginPost;
use Magento\Customer\Controller\Account\LoginPost as LPController;

/**
 * Class LoginPostTest for testing LoginPost class
 * @covers \Arb\OTPLogin\Plugin\LoginPost
 */
class LoginPostTest extends \PHPUnit\Framework\TestCase
{
     /**
      * Test setUp
      */
    protected function setUp()
    {
        $this->contextMock = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->otpModelMock = $this->getMockBuilder(OTP::class)
            ->setMethods(["validateOTP"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlMock = $this->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->messageMock = $this->getMockBuilder(ManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->resultFactoryMock = $this->createPartialMock(
            ResultFactory::class,
            ['create',"setUrl"]
        );
        $this->requestMock = $this
            ->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
            ->setMethods(["getPost"])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->contextMock->expects($this->once())->method('getRequest')->willReturn($this->requestMock);
        $this->resultFactoryMock->expects($this->any())
            ->method('create')
            ->willReturnSelf();
        $this->loginPostMock = new LoginPost(
            $this->contextMock,
            $this->urlMock,
            $this->otpModelMock,
            $this->messageMock,
            $this->resultFactoryMock
        );
    }

    /**
     * testAroundExecute method
     */
    public function testAroundExecute()
    {
        $this->lpController = $this->createMock(LPController::class);
        $closureMock = $this->lpController;
        $this->proceed = function () use ($closureMock) {
            return $closureMock;
        };
        $this->loginPostMock->aroundExecute($this->lpController, $this->proceed);
    }

    /**
     * testAroundExecuteSuccess method
     */
    public function testAroundExecuteSuccess()
    {
        $this->lpController = $this->createMock(LPController::class);
        $closureMock = $this->lpController;
        $this->proceed = function () use ($closureMock) {
            return $closureMock;
        };
        $this->requestMock->expects($this->any())
        ->method('getPost')
        ->willReturn(["otp"=>"1234",
            "username"=>"test@example.com"]);
        $this->otpModelMock->expects($this->any())
        ->method('validateOTP')
        ->willReturn(["message"=>"success","status"=>true]);
        $this->loginPostMock->aroundExecute($this->lpController, $this->proceed);
    }

    /**
     * testAroundExecuteFailer method
     */
    public function testAroundExecuteFailer()
    {
        $this->lpController = $this->createMock(LPController::class);
        $closureMock = $this->lpController;
        $this->proceed = function () use ($closureMock) {
            return $closureMock;
        };
        $this->requestMock->expects($this->any())
        ->method('getPost')
        ->willReturn(
            ["otp"=>"1234",
            "username"=>"test@example.com"]
        );
        $this->otpModelMock->expects($this->any())
        ->method('validateOTP')
        ->willReturn(["message"=>"","status"=>false]);
        $this->loginPostMock->aroundExecute($this->lpController, $this->proceed);
    }
}
