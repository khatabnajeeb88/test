<?php
/**
 * Ajax controller to request OTP
 *
 * @category Arb
 * @package Arb_OTPLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\OTPLogin\Test\Unit\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Arb\OTPLogin\Model\OTP;
use \Magento\Framework\Model\Context;
use \Magento\Framework\Model\ResourceModel\AbstractResource;
use \Magento\Framework\Data\Collection\AbstractDb;
use \Magento\Framework\Registry;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Framework\HTTP\Client\Curl;
use \Magento\Framework\Math\Random;
use \Magento\Customer\Model\Authentication;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class OTPTest for testing OTP class
 * @covers \Arb\OTPLogin\Model\OTP
 */
class OTPTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test setUp
     */
    protected function setUp()
    {
        $this->contextMock = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->_scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);
        $this->_curlMock = $this->getMockBuilder(Curl::class)->getMock();
        $this->_registryMock = $this->getMockBuilder(Registry::class)->getMock();
        $this->_randomMock = $this->createMock(Random::class);
        $this->_abstractDbMock = $this->createMock(AbstractDb::class);
        $this->_abstractResourceMock = $this->createMock(AbstractResource::class);
        $this->_authenticateMock = $this->createMock(Authentication::class);
        $this->_customerRepositoryMock = $this->getMockBuilder(\Magento\Customer\Api\CustomerRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["get","getId","getCustomAttribute","getValue"])
            ->getMockForAbstractClass();
        $this->storeManager = $this->getMockBuilder(StoreManagerInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["getStore","getCode","getId","getWebsiteId"])
            ->getMockForAbstractClass();
        $this->storeManager->expects($this->any())->method('getStore')->willReturnSelf();
        $this->scopeConfig = $this->getMockBuilder(ScopeConfigInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->encryptor = $this->createMock(\Magento\Framework\Encryption\EncryptorInterface::class);

        $this->_serializationMock = $this->getMockBuilder(Json::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'unserialize'
                            ])
                            ->getMock();

        $this->otpMock = new OTP(
            $this->contextMock,
            $this->_registryMock,
            $this->_customerRepositoryMock,
            $this->_curlMock,
            $this->_randomMock,
            $this->_authenticateMock,
            $this->storeManager,
            $this->_serializationMock,
            $this->scopeConfig,
            $this->encryptor,
            $this->_abstractResourceMock,
            $this->_abstractDbMock,
            []
        );
    }

    public function testSendOTPToMerchant()
    {
        $this->_authenticateMock->method("authenticate")->willReturn(true);
        $this->_customerRepositoryMock->expects($this->any())->method("get")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getId")->willReturn("10");
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("989829128");
        $this->storeManager->expects($this->any())->method('getCode')->willReturn("ar_SA");
        $this->encryptor->expects($this->at(0))->method("decrypt")->willReturn("cliet_secret");
        $this->encryptor->expects($this->at(1))->method("decrypt")->willReturn("cliet_id");
        $this->_serializationMock->method("unserialize")->willReturn(["header"=>["statusCode"=>"I000000"]]);
        $this->otpMock->sendOTPToMerchant("test@example.com", "passowrd");
    }

    public function testSendOTPToMerchantNullMessage()
    {
        $this->_authenticateMock->method("authenticate")->willReturn(true);
        $this->_customerRepositoryMock->expects($this->any())->method("get")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getId")->willReturn("10");
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("989829128");
        $this->storeManager->expects($this->any())->method('getCode')->willReturn("ar_SA");
        $this->encryptor->expects($this->at(0))->method("decrypt")->willReturn("cliet_secret");
        $this->encryptor->expects($this->at(1))->method("decrypt")->willReturn("cliet_id");
        $this->_serializationMock->method("unserialize")->willReturn(["header"=>["statusCode"=>"Error"]]);
        $this->otpMock->sendOTPToMerchant("test@example.com", "passowrd");
    }

    public function testSendOTPToMerchantNullMobile()
    {
        $this->_customerRepositoryMock->expects($this->any())->method("get")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getId")->willReturn("10");
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("");
        $this->otpMock->sendOTPToMerchant("test@example.com", "passowrd");
    }

    public function testValidateOTP()
    {
        $this->_serializationMock->method("unserialize")->willReturn(["header"=>["statusCode"=>"I000000"]]);
        $this->_customerRepositoryMock->expects($this->any())->method("get")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("989829128");
        $this->otpMock->validateOTP("1111", "test@example.com");
    }

    public function testLogMessage()
    {
        $this->_customerRepositoryMock->expects($this->any())->method("get")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getId")->willReturn("1");
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn(null);
        $this->otpMock->sendOTPToMerchant("test1@example.com", "passowrd");
    }
}
