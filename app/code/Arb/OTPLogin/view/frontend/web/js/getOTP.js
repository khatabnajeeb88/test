/**
 * GET OTP call from ajax request
 *  
 * @category    Arb
 * @package     Arb_OTPLogin
 * @author Arb Magento Team
 */
require([
    'jquery',
    'mage/url',
    'mage/validation'
], function ($, url) {
    'use strict';
    //Hiding sign in button before OTP validation
    $( document ).ready(function() {
        $('.form-actions').css('display','none');
        $('.captcha').css('display','none');
        //Prevent Submit form on Enter
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
				if($('#otp-field').css('display') == 'none'){
					event.preventDefault();
					$("#generate-otp").click();
					return false;
				}
			}
		});
    });

    //OTP trigger ajax
    $(document).on("click", "#generate-otp", function () {
        var dataForm = $('#login-form');
        dataForm.validation('isValid');
        var paraphrase="this is a secret key";
        var ajaxUrl = url.build('otplogin/getotp/index');
        $.ajax({
            url: ajaxUrl,
            type: 'POST',
            dataType: 'json',
            data: {
                email: $('#email').val(),
                password: CryptoJSAesJson.encrypt($("#pass").val(), paraphrase),
                form_key: $("input[name=form_key]").val()
            },
            beforeSend: function() { 
                $('#generate-otp').css('display','none');
                $('#loading-text').css('display','inline-block');
                $('#otp-result').css('display', 'none');
                $('.form-actions').css('display','none');
                $('#otp-error').css('display','none');
                $('.captcha').css('display','none');
              },
            complete: function (response) {
                $('#otp-result').css('display', 'inline-block');
                $('#loading-text').css('display','none');
                if(response.responseJSON.merchant){
                    $('.captcha').css('display','block');
                    $('#otp-field').css('display','block');
                    $('#otp-trigger').css('display','none');
                    $('.form-actions').css('display','block');
                }else{
                    $('#generate-otp').css('display','inline-block');
                    $('#otp-field').css('display','none');
                    $('#otp-result').css('color','#e02b27');
                    $('#otp-result').text(response.responseJSON.message);
                }
                if(response.responseJSON.status === false){
                    $('#otp-error').css('display','block');
                    $('#otp-error').css('color','#e02b27');
                    $('#otp-error').text(response.responseJSON.message);
                }
                console.log(response.responseJSON);
            },
            error: function (xhr, status, errorThrown) {
                console.log(xhr+' '+status+' '+errorThrown);
                $('#loading-text').css('display','inline-block');               
            }
        });

    });
});