<?php
/**
 * Ajax controller to request OTP
 *
 * @category Arb
 * @package Arb_OTPLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\OTPLogin\Plugin;

use Magento\Framework\Controller\ResultFactory;

/**
 * Check OTP before login
 */
class LoginPost
{
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * @var \Magento\Framework\App\Action\Context::getRequest()
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Action\Context::getResponse()
     */
    protected $response;

    /**
     * @var \Arb\OTPLogin\Model\OTP
     */
    protected $otpModel;

    /**
     * @var \Magento\Framework\Message\ManagerInterface $messageManager
     */
    protected $messageManager;

    /**
     * LoginPost class constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\Controller\ResultFactory $resultFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\UrlInterface $url,
        \Arb\OTPLogin\Model\OTP $otpModel,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        ResultFactory $resultFactory
    ) {
        $this->request = $context->getRequest();
        $this->response = $context->getResponse();
        $this->url = $url;
        $this->resultFactory = $resultFactory;
        $this->otpModel = $otpModel;
        $this->messageManager = $messageManager;
    }

    /**
     * Before login check OTP
     *
     * @param \Magento\Customer\Controller\Account\LoginPost $subject
     * @param \Closure $proceed
     * @return void
     */
    public function aroundExecute(
        \Magento\Customer\Controller\Account\LoginPost $subject,
        \Closure
        $proceed
    ) {
        
        $loginData =  $this->request->getPost('login');

        //Validate OTP with ESB before setting session
        if (null == $loginData['otp']) {
            $this->messageManager->addErrorMessage(__('OTP is required field'));
            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $result->setUrl($this->url->getUrl('marketplace/account/login/'));
            return $result;
        }

        $checkOtp = $this->otpModel->validateOTP($loginData['otp'], $loginData['username']);
        if (!$checkOtp['status']) {
            $message = (null != $checkOtp['message'])?$checkOtp['message']:'Invalid OTP!';
            $this->messageManager->addErrorMessage(__($message));
            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $result->setUrl($this->url->getUrl('marketplace/account/login/'));
            return $result;
        }
       
        //Original function executes if OTP is valid
        $resultProceed = $proceed();
    
        return $resultProceed;
    }
}
