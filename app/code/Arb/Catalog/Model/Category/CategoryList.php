<?php
/**
 * This file consist of class that provides list of categories
 *
 * @category Arb
 * @package Arb_Catalog
 * @author Arb Magento Team
 *
 */

namespace Arb\Catalog\Model\Category;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Option\ArrayInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

/**
 * Listing product categories
 */
class CategoryList implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * CategoryList constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->categoryCollectionFactory = $collectionFactory;
    }

    /**
     * Getting list of categories
     *
     * @param bool $addEmpty
     *
     * @return array
     * @throws LocalizedException
     */
    public function toOptionArray($addEmpty = true)
    {
        $categoryCollection = $this->categoryCollectionFactory->create();
        $categoryCollection->addAttributeToSelect('name');

        $options = [];
        if ($addEmpty) {
            $options[] = ['label' => __('-- Please Select a Category --'), 'value' => ''];
        }

        foreach ($categoryCollection as $category) {
            $options[] = ['label' => $category->getName(), 'value' => $category->getId()];
        }

        return $options;
    }
}
