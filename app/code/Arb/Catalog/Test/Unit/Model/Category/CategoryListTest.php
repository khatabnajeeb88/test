<?php

namespace Arb\Catalog\Test\Unit\Model\Category;

use Arb\Catalog\Model\Category\CategoryList;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;

class CategoryListTest extends TestCase
{
    /**
     * Object to test
     *
     * @var CategoryList
     */
    private $testObject;

    /**
     * @var CollectionFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $collectionFactoryMock;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);

        $this->testObject = $objectManager->getObject(CategoryList::class, [
            'categoryCollectionFactory' => $this->collectionFactoryMock
        ]);
    }

    /**
     * @throws ReflectionException
     * @throws LocalizedException
     */
    public function testToOptionArray()
    {
        $collectionMock = $this->createMock(Collection::class);
        $collectionMock->expects($this->once())
            ->method('getIterator')
            ->willReturn(new \ArrayObject([]));
        $this->collectionFactoryMock->expects($this->once())->method('create')->willReturn($collectionMock);
        $collectionMock->expects($this->once())->method('addAttributeToSelect')->willReturnSelf();

        $this->assertInternalType('array', $this->testObject->toOptionArray());
    }
}
