<?php

namespace Arb\Catalog\Test\Unit\Ui\Component\Listing\Column;

use Arb\Catalog\Ui\Component\Listing\Column\Category;
use Magento\Catalog\Model\Category as CategoryModel;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;

class CategoryTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Category
     */
    private $testObject;

    /**
     * @var ContextInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var UiComponentFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $uiComponentFactoryMock;

    /**
     * @var ProductFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $productFactoryMock;

    /**
     * @var CategoryFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $categoryFactoryMock;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->contextMock = $this->createMock(ContextInterface::class);
        $this->uiComponentFactoryMock = $this->createMock(UiComponentFactory::class);
        $this->productFactoryMock = $this->createMock(ProductFactory::class);
        $this->categoryFactoryMock = $this->createMock(CategoryFactory::class);

        $this->testObject = $objectManager->getObject(Category::class, [
            'productFactory' => $this->productFactoryMock,
            'categoryFactory' => $this->categoryFactoryMock
        ]);
    }

    /**
     * @throws ReflectionException
     */
    public function testPrepareDataSource()
    {
        $productMock = $this->createMock(Product::class);
        $this->productFactoryMock->expects($this->atLeastOnce())->method('create')->willReturn($productMock);
        $productMock->expects($this->atLeastOnce())->method('load')->willReturnSelf();
        $productMock->expects($this->atLeastOnce())->method('getCategoryIds')->willReturn([1,2]);
        $categoryMock = $this->createMock(CategoryModel::class);
        $this->categoryFactoryMock->expects($this->atLeastOnce())->method('create')->willReturn($categoryMock);
        $categoryMock->expects($this->atLeastOnce())->method('load')->willReturnSelf();
        $categoryMock->expects($this->atLeastOnce())->method('getName')->willReturn('category name');

        $dataSource = [
            'data' => [
                'items' => [
                    [
                        'entity_id' => 1,
                    ],
                    [
                        'entity_id' => 2,
                    ]
                ],
            ],
        ];
        $this->testObject->prepareDataSource($dataSource);
    }
}
