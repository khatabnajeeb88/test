## Synopsis
This module used for export sales data and send to CORTEX.

### Module configuration
1. Module configuration details [module.xml](etc/module.xml).
2. Module system configuration details in [config.xml](etc/config.xml).
3. Admin ACL configuration [acl.xml](etc/acl.xml).
4. Admin menu routing [routes.xml](etc/adminhtml/routes.xml).
5. Admin system configuration [system.xml](etc/adminhtml/routes.xml)

## Tests
Unit tests could be found in the [Test/Unit](Test/Unit) directory.
