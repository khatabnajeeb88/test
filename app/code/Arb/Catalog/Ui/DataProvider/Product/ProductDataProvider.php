<?php
/**
 * This file consist of class that provides product filter
 *
 * @category Arb
 * @package Arb_Catalog
 * @author Arb Magento Team
 *
 */

namespace Arb\Catalog\Ui\DataProvider\Product;

use Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider as MagentoProductDataProvider;
use Magento\Framework\Api\Filter;

/**
 * Providing data for product filtering
 */
class ProductDataProvider extends MagentoProductDataProvider
{
    /**
     * @codeCoverageIgnore
     *
     * @param Filter $filter
     *
     * @return mixed|void
     */
    public function addFilter(Filter $filter)
    {
        if ($filter->getField() === 'category_id') {
            $this->getCollection()->addCategoriesFilter(array('in' => $filter->getValue()));
        } elseif (isset($this->addFilterStrategies[$filter->getField()])) {
            $this->addFilterStrategies[$filter->getField()]
                ->addFilter(
                    $this->getCollection(),
                    $filter->getField(),
                    [$filter->getConditionType() => $filter->getValue()]
                );
        } else {
            parent::addFilter($filter);
        }
    }
}
