<?php
/**
 * Payment gateway REST api return data modification plugin
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Plugin\Model;

/**
 * Payment gateway REST api return data modification
 */
class QuoteManagement
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * Plugin constructor
     *
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\Serialize\Serializer\Json $json
    ) {
        $this->orderRepository = $orderRepository;
        $this->json = $json;
    }
    /**
     * Function modification by plugin
     *
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param array $result
     * @param string $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @return array
     */
    public function afterPlaceOrder(
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        $result,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
    ) {
        $order = $this->orderRepository->get($result);
        return $order->getIncrementId();
    }
}
