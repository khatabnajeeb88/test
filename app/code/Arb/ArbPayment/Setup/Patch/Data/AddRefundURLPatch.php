<?php
/**
 * Payment gateway refund url setup
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddRefundURLPatch implements DataPatchInterface
{
    private const ARB_PAYMENT_REFUND_PATH = 'payment/arbpayment_gateway/pg_refund_url';
    private const ARB_PAYMENT_REFUND_URL = 'https://securepayments.alrajhibank.com.sa/pg/payment/tranportal.htm';
   
     /**
      * @var ModuleDataSetupInterface
      */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::ARB_PAYMENT_REFUND_PATH, self::ARB_PAYMENT_REFUND_URL);
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param string|int $value
     *
     * @return void
     */
    private function updateConfiguration(string $path, $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
