<?php
/**
 * Payment gateway install schema update
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Setup;

use \Magento\Framework\DB\Ddl\Table;

/**
 * InstallSchema class to add payment gateway related data
 * @codeCoverageIgnore
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * DB setup code
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $salesDb = $setup->getConnection('sales');
        $salesDb->addColumn(
            $setup->getTable('sales_order_payment'),
            'arb_track_id',
            [
                'type' => Table::TYPE_TEXT,
                'length'=> 16,
                ['nullable' => false, 'default' => '0','unsigned' => true],
                'comment' => 'ARB Payment track id',
            ]
        );
        $salesDb->addColumn(
            $setup->getTable('sales_order_payment'),
            'arb_payment_id',
            [
                'type' => Table::TYPE_TEXT,
                'length'=> 64,
                ['nullable' => false, 'default' => '0','unsigned' => true],
                'comment' => 'ARB Payment id for 3ds',
            ]
        );

        $setup->endSetup();
    }
}
