<?php
/**
 * Payment gateway install schema update
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Log table addition
 * @codeCoverageIgnore
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '2.0.0', '<')) {
            $connection = $installer->getConnection();
            $connection->addColumn($installer->getTable('quote'), 'CICNumber', [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'ARB customer CIC Number'
            ]);
            $connection->addColumn($installer->getTable('sales_order'), 'CICNumber', [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'ARB customer CIC Number'
            ]);
        }
        if (version_compare($context->getVersion(), '2.0.1', '<=')) {
            $table = $installer->getConnection()
            ->newTable($installer->getTable('arb_payment_log'))
            ->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'auto increment primary key'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_TEXT,
                16,
                [
                    'unsigned' => true
                ],
                'Order Id'
            )
            ->addColumn(
                'timestamp',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT
                ],
                'system generated timestamp'
            )
            ->addColumn(
                'api_type',
                Table::TYPE_TEXT,
                8,
                [
                    'unsigned' => true
                ],
                'purchase or inquiry'
            )
            ->addColumn(
                'inquiry_count',
                Table::TYPE_INTEGER,
                12,
                [
                    'unsigned' => true
                ]
            )
            ->addColumn(
                'request',
                Table::TYPE_TEXT,
                '2M',
                [],
                'NON PCI request data'
            )
            ->addColumn(
                'response',
                Table::TYPE_TEXT,
                '2M',
                [],
                'NON PCI response data'
            )
            ->setComment('payment log');
            $installer->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '2.0.3', '<=')) {
            $connection = $installer->getConnection();
            $connection->addColumn($installer->getTable('sales_order_payment'), 'arb_ref_id', [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'Payment Inquiry Ref Id'
            ]);
        }

        if (version_compare($context->getVersion(), '2.0.4', '<=')) {
            $connection = $installer->getConnection();
            $connection->dropColumn($installer->getTable('quote'), 'CICNumber');
            $connection->dropColumn($installer->getTable('sales_order'), 'CICNumber');
        }

        if (version_compare($context->getVersion(), '2.0.5', '<=')) {
            $connection = $installer->getConnection();
            $connection->addColumn($installer->getTable('sales_order_payment'), 'arb_auth_code', [
                'type'     => Table::TYPE_TEXT,
                'length'   => 64,
                'nullable' => true,
                'comment'  => 'Payment Inquiry Auth code'
            ]);
        }

        $installer->endSetup();
    }
}
