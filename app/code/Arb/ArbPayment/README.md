## Synopsis
An extension to add integration with ARB Payment Gateway.

## Technical feature

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module configuration available through Stores->Configuration [system.xml](etc/adminhtml/system.xml)
4. ACL configuration [acl.xml](etc/acl.xml)
5. Dependency injection configuration [di.xml](etc/di.xml)
6. Module configuration [module.xml](etc/module.xml)
7. Payment detail configuration [payment.xml](etc/payment.xml)
8. Webapi configuration [webapi.xml](etc/webapi.xml)

Payment gateway module depends on `Sales`, `Payment`, `Checkout`, `Directory` and `Config` Magento modules.

### Gateway configuration
On the next step, we might specify gateway domain configuration in [config.xml](etc/config.xml).

##### Let's look into configuration attributes:
 * <code>debug</code> enables debug mode by default, e.g log for request/response
 * <code>active</code> is payment active by default
 * <code>model</code> `Payment Method Facade` used for integration with `Sales` and `Checkout` modules
 * <code>merchant_id</code> Payment Gateway ID
 * <code>merchant_password</code> Payment Gateway Password
 * <code>vector_init</code> Initialization Vector for Payment Gateway encryption.
 * <code>pg_terminal_resourcekey</code> Payment Gateway terminal resource key
 * <code>order_status</code> default order status
 * <code>payment_action</code> default action of payment
 * <code>title</code> default title for a payment method
 * <code>currency</code> supported currency

## Tests
Unit tests could be found in the [Test/Unit](Test/Unit) directory.


