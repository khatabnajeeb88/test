<?php
/**
 * Payment method module registration
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
// @codeCoverageIgnoreStart
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_ArbPayment',
    __DIR__
);
// @codeCoverageIgnoreEnd
