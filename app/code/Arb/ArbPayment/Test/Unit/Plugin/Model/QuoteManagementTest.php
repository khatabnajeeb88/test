<?php
namespace Arb\ArbPayment\Test\Unit\Plugin\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\ArbPayment\Plugin\Model\QuoteManagement
 */
class QuoteManagementTest extends TestCase
{
    /**
     * Mock order
     *
     * @var \Magento\Sales\Model\Order|PHPUnit_Framework_MockObject_MockObject
     */
    private $order;

    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $json;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Plugin\Model\QuoteManagement
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->order = $this->getMockBuilder(\Magento\Sales\Api\OrderRepositoryInterface::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'getIncrementId',
                                'getList',
                                'get',
                                'delete',
                                'save'
                            ])
                            ->getMock();
        $this->orderRepository = $this->createMock(\Magento\Sales\Api\OrderRepositoryInterface::class);
        $this->json = $this->createMock(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->quoteManagement = $this->createMock(\Magento\Quote\Model\QuoteManagement::class);
        $this->paymentMethod = $this->createMock(\Magento\Quote\Api\Data\PaymentInterface::class);
        $this->testObject = $this->objectManager->getObject(
            \Arb\ArbPayment\Plugin\Model\QuoteManagement::class,
            [
                'orderRepository' => $this->orderRepository,
                'json' => $this->json,
            ]
        );
    }

    /**
     * @return array
     */
    public function dataProviderForTestAfterPlaceOrder()
    {
        return [
            'Testcase 1' => [
                'result' => 10,
                'cartId' => 21,
                'expectedResult' => 10000012
            ]
        ];
    }

    /**
     * @dataProvider dataProviderForTestAfterPlaceOrder
     */
    public function testAfterPlaceOrder($result, $cartId, $expectedResult)
    {
        $this->orderRepository->method('get')->willReturn($this->order);
        $this->order->method('getIncrementId')->willReturn($expectedResult);
        $result = $this->testObject->afterPlaceOrder($this->quoteManagement, $result, $cartId, $this->paymentMethod);
        $this->assertEquals($result, $expectedResult);
    }
}
