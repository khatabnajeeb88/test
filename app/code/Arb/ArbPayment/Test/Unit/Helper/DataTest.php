<?php
namespace Arb\ArbPayment\Test\Unit\Helper;

use BadMethodCallException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use \Arb\ArbPayment\Helper\Data;
use \Magento\Framework\Serialize\Serializer\Json;
/**
 * @covers \Arb\ArbPayment\Helper\Data
 */
// phpcs:disable
class DataTest extends TestCase
{
    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock payment
     *
     * @var \Magento\Sales\Model\Order\Payment|PHPUnit_Framework_MockObject_MockObject
     */
    private $payment;

    /**
     * Mock encryptor
     *
     * @var \Arb\ArbPayment\Model\Encryption|PHPUnit_Framework_MockObject_MockObject
     */
    private $encryptor;

    /**
     * Mock curl
     *
     * @var \Magento\Framework\HTTP\Client\Curl|PHPUnit_Framework_MockObject_MockObject
     */
    private $curl;

    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $json;

    /**
     * Mock paymentLog
     *
     * @var \Arb\ArbPayment\Model\PaymentLog|PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentLog;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\PaymentInquiry
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->jsonObj =  $this->objectManager->getObject(Json::class);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->voucher = $this->getMockBuilder(\Arb\Vouchers\Model\VoucherOrders::class)
                              ->disableOriginalConstructor()
                              ->setMethods([
                                  'assignVouchers',
                                  'unBlockVouchers'
                              ])
                              ->getMock();
        $this->esbNotification = $this->getMockBuilder(\Arb\ArbPayment\Model\ESBNotifications::class)
                              ->disableOriginalConstructor()
                              ->setMethods([
                                  'prepareOrderData'
                              ])
                              ->getMock();
        $this->order = $this->createMock(\Magento\Sales\Model\Order::class);
        $this->invoiceService = $this->getMockBuilder(\Magento\Sales\Model\Service\InvoiceService::class)
                                ->disableOriginalConstructor()
                              ->setMethods([
                                  'prepareInvoice'
                              ])
                              ->getMock();  

        $this->invoiceServiceInterface = $this->getMockBuilder(\Magento\Sales\Api\Data\InvoiceInterface::class)
                                ->disableOriginalConstructor()
                              ->setMethods([
                                  'setRequestedCaptureCase',
                                  'register',
                                  "getOrder"
                              ])
                              ->getMockForAbstractClass();      

        $this->order->method('setState')->willReturnSelf();
        $this->order->method('setStatus')->willReturnSelf();
        $this->payment = $this->getMockBuilder(\Magento\Sales\Model\Order\Payment::class)
                              ->disableOriginalConstructor()
                              ->setMethods([
                                  'load',
                                  'getArbTrackId',
                                  'getParentId',
                                  'getAmountOrdered',
                                  'getOrder',
                                  'setTransactionId',
                                  'setParentTransactionId',
                                  'save',
                                  'setData',
                                  'getLastTransId'
                              ])
                              ->getMock();
        $this->payment->method('load')->will($this->returnSelf());
        $this->payment->method('getOrder')->willReturn($this->order);
        $this->encryptor = $this->createMock(\Arb\ArbPayment\Model\Encryption::class);
        $this->transactionFactory = $this->getMockBuilder(\Magento\Framework\DB\TransactionFactory::class)
                            ->disableOriginalConstructor()
                              ->setMethods([
                                  'create',
                                  "addObject",
                                    "save"
                              ])
                              ->getMock();
        $this->curl = $this->createMock(\Magento\Framework\HTTP\Client\Curl::class);
        $this->json = $this->getMockBuilder(\Magento\Framework\Serialize\Serializer\Json::class)
                            ->disableOriginalConstructor()
                              ->setMethods([
                                  'unserialize'
                              ])
                              ->getMock();

        $this->paymentLog = $this->createMock(\Arb\ArbPayment\Model\PaymentLog::class);
        $getValueReturnMap = [
            [ "payment/arbpayment_gateway/vector_init" , ScopeInterface::SCOPE_STORE , null , "PGKEYENCDECIVSPC"],
            [ "payment/arbpayment_gateway/merchant_id", ScopeInterface::SCOPE_STORE , null , 10],
            [ "payment/arbpayment_gateway/merchant_password", ScopeInterface::SCOPE_STORE , null , "password"],
            [ "payment/arbpayment_gateway/pg_terminal_resourcekey",
            ScopeInterface::SCOPE_STORE ,
            null ,
            "92978803765192978803765192978803"
            ],
            [ "payment/arbpayment_gateway/pg_currency_code", ScopeInterface::SCOPE_STORE , null , "USD"],
            [ "payment/arbpayment_gateway/pg_inquiry_url", ScopeInterface::SCOPE_STORE , null , "http://test.com"]
        ];
        $this->scopeConfig->expects($this->any())
             ->method('getValue')
             ->will($this->returnValueMap($getValueReturnMap));

        $this->encryptionModel = $this->getMockBuilder(\Arb\ArbPayment\Model\Encryption::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                  'encryptAES',
                                  "decryptData",
                                  "decryptAES"
                              ])
                            ->getMock();
        $this->encryptionModel->method('decryptData')->willReturn("abc");
        $this->transactionBuilder = $this->getMockBuilder(BuilderInterface::class)
                            ->disableOriginalConstructor()->getMockForAbstractClass();
       
        $this->transactionBuilder->method('setPayment')->will($this->returnSelf());
        $this->transactionBuilder->method('setOrder')->will($this->returnSelf());
        $this->transactionBuilder->method('setTransactionId')->will($this->returnSelf());
        $this->transactionBuilder->method('setAdditionalInformation')->will($this->returnSelf());
        $this->transactionBuilder->method('setFailSafe')->will($this->returnSelf());
        $this->transactionBuilder->method('build')->will($this->returnSelf());
        $this->testObject = $this->objectManager->getObject(
            Data::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
    }


    /**
     * testProcessPaymentInquiry
     *
     * @return void
     */
    public function testGetPaymentConfiguration()
    {
        
        $result = $this->testObject->getPaymentConfiguration("refund");
    }    


     /**
     * testProcessPaymentInquiry
     *
     * @return void
     */
    public function testGetPaymentConfigurationPurchase()
    {
        
        $result = $this->testObject->getPaymentConfiguration("purchase");
    }    

}