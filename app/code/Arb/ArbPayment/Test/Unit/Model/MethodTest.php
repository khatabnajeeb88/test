<?php
namespace Arb\ArbPayment\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use \Magento\Store\Model\ScopeInterface;

/**
 * @covers \Arb\ArbPayment\Model\Method
 */
class MethodTest extends TestCase
{
    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock order
     *
     * @var \Magento\Sales\Model\Order|PHPUnit_Framework_MockObject_MockObject
     */
    private $order;

    /**
     * Mock curl
     *
     * @var \Magento\Framework\HTTP\Client\Curl|PHPUnit_Framework_MockObject_MockObject
     */
    private $curl;

    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $json;

    /**
     * Mock responseChecker
     *
     * @var \Arb\ArbPayment\Model\ResponseChecker|PHPUnit_Framework_MockObject_MockObject
     */
    private $responseChecker;

    /**
     * Mock paymentLog
     *
     * @var \Arb\ArbPayment\Model\PaymentLog|PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentLog;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\PaymentPurchase
     */
    private $testObject;

    /**
     * Main set up method
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        
        $this->context = $this->createMock(\Magento\Framework\Model\Context::class);
        $this->resource = $this->createMock(\Magento\Framework\Model\ResourceModel\AbstractResource::class);
        $this->resourceCollection = $this->createMock(\Magento\Framework\Data\Collection\AbstractDb::class);
        $this->registry = $this->createMock(\Magento\Framework\Registry::class);

        $this->extensionFactory = $this->createMock(\Magento\Framework\Api\ExtensionAttributesFactory::class);
        $this->customAttributeFactory = $this->createMock(\Magento\Framework\Api\AttributeValueFactory::class);
        $this->paymentData = $this->createMock(\Magento\Payment\Helper\Data::class);
        
        $this->settings = $this->createMock(\Arb\ArbPayment\Model\Config\Settings::class);

        $this->logger = $this->createMock(\Magento\Payment\Model\Method\Logger::class);

        $this->testObject = $this->getMockForAbstractClass(
            \Arb\ArbPayment\Model\Method::class,
            [
                'settings' => $this->settings,
                'context' => $this->context,
                'registry' => $this->registry,
                'extensionFactory' => $this->extensionFactory,
                'customAttributeFactory' => $this->customAttributeFactory,
                'paymentData' => $this->paymentData,
                'scopeConfig' => $this->scopeConfig,
                'logger' => $this->logger,
                'resource' => $this->resource,
                'resourceCollection' => $this->resourceCollection
            ]
        );
    }

   /**
     * @expectedException Magento\Framework\Exception\LocalizedException
     */
   public function testIsAvailable()
   {
        $quote = $this->createMock(\Magento\Quote\Api\Data\CartInterface::class);
        $this->settings->method('getCurrentStoreCode')->willReturn('en');
        $this->settings->method('getEnabledForStores')->willReturn(['en']);
        $this->testObject->isAvailable($quote);
   }

   public function testIsAvailableFalse()
   {
        $quote = $this->createMock(\Magento\Quote\Api\Data\CartInterface::class);
        $this->settings->method('getCurrentStoreCode')->willReturn('ar');
        $this->settings->method('getEnabledForStores')->willReturn(['en']);
        $this->testObject->isAvailable($quote);
   }
}
