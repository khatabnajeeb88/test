<?php
namespace Arb\ArbPayment\Test\Unit\Model\Config;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Store\Model\ScopeInterface;

/**
 * @covers \Arb\ArbPayment\Model\Config\Settings
 */
class SettingsTest extends TestCase
{
    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock storeManager
     *
     * @var \Magento\Store\Model\StoreManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManager;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\Config\Settings
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->getMockBuilder(\Magento\Framework\App\Config\ScopeConfigInterface::class)
                                  ->disableOriginalConstructor()
                                  ->setMethods([
                                      'getValue',
                                      'isSetFlag'
                                  ])
                                  ->getMock();
        $this->storeManager = $this->getMockBuilder(\Magento\Store\Model\StoreManagerInterface::class)
                                       ->disableOriginalConstructor()
                                       ->setMethods([
                                           'getStore',
                                           'setIsSingleStoreModeAllowed',
                                           'hasSingleStore',
                                           'isSingleStoreMode',
                                           'getStores',
                                           'getWebsite',
                                           'getWebsites',
                                           'reinitStores',
                                           'getDefaultStoreView',
                                           'getGroup',
                                           'getGroups',
                                           'setCurrentStore'
                                           
                                       ])
                                       ->getMock();
        $this->StoreInterface = $this->getMockBuilder(\Magento\Store\Api\Data\StoreInterface::class)
                                            ->disableOriginalConstructor()
                                            ->setMethods([
                                                'getId',
                                                'setId',
                                                'getCode',
                                                'setCode',
                                                'getName',
                                                'setName',
                                                'getWebsiteId',
                                                'setWebsiteId',
                                                'getStoreGroupId',
                                                'setStoreGroupId',
                                                'setIsActive',
                                                'getIsActive',
                                                'getExtensionAttributes',
                                                'setExtensionAttributes',
                                                "getBaseUrl"
                                            ])
                                            ->getMock();
        $this->testObject = $this->objectManager->getObject(
            \Arb\ArbPayment\Model\Config\Settings::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'storeManager' => $this->storeManager,
            ]
        );
        $this->storeManager->method('getStore')->willReturn(
            $this->StoreInterface
        );
    }

    public function testGetCurrentStoreCode()
    {
        $storeCode = "SA";
        $this->StoreInterface->method('getCode')->willReturn(
            $storeCode
        );
        $this->assertEquals($this->testObject->getCurrentStoreCode(), $storeCode);
    }

    public function testGetEnabledForStores()
    {
        $paymentMethodCode = "arb";
        $xmlConfigPath = "payment/{$paymentMethodCode}/active";
        $this->scopeConfig->method('getValue')->with($xmlConfigPath)->willReturn(
            "foo"
        );
        $storeCode = "SA";
        $this->StoreInterface->method('getCode')->willReturn(
            $storeCode
        );
        $result = $this->testObject->getEnabledForStores($paymentMethodCode);
        $this->assertContains($storeCode, $result);
    }

    public function testGetEnabledForStoresEmpty()
    {
        $paymentMethodCode = "arb";
        $xmlConfigPath = "payment/{$paymentMethodCode}/active";
        $this->scopeConfig->method('getValue')->with($xmlConfigPath)->willReturn(
            null
        );
        $result = $this->testObject->getEnabledForStores($paymentMethodCode);
        $this->assertEmpty($result);
    }
}
