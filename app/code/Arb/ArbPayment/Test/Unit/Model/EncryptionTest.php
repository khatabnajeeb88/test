<?php
namespace Arb\ArbPayment\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\ArbPayment\Model\Encryption
 */
class EncryptionTest extends TestCase
{
    /**
     * Mock encryptor
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $encryptor;

    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\Encryption
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->encryptor = $this->createMock(\Magento\Framework\Encryption\EncryptorInterface::class);
        $this->scopeConfig = $this->getMockBuilder(\Magento\Framework\App\Config\ScopeConfigInterface::class)
                                  ->disableOriginalConstructor()
                                  ->setMethods([
                                      'getValue',
                                      'isSetFlag'
                                  ])
                                  ->getMock();

        $this->scopeConfig->method('getValue')->willReturn('5382213802222451');
        
        $this->testObject = $this->objectManager->getObject(
            \Arb\ArbPayment\Model\Encryption::class,
            [
                'encryptor' => $this->encryptor,
                'scopeConfig' => $this->scopeConfig,
            ]
        );
    }

    public function testEncryptAES()
    {
        $encrypt_text = "hello world";
        $encrypted = $this->testObject->encryptAES($encrypt_text);
        $encrypted2 = $this->testObject->encryptAES($encrypt_text,'', '', 'order');
        $decrypted = $this->testObject->decryptAES($encrypted);
        $decrypted2 = $this->testObject->decryptAES($encrypted, '', '', 'order');
        $decryptData = $this->testObject->decryptData($encrypted, null);
        $this->assertEquals($encrypt_text, $decrypted);
        $this->assertEquals($encrypt_text, $decryptData);
    }

    public function testGetPkcs5Unpad()
    {   
        $this->testObject->getPkcs5Unpad('This is a test string This is a test string This is a test string This is a test string This is a test string This is a test string');
    }
}
