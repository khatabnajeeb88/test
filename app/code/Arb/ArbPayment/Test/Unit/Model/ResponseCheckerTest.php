<?php
namespace Arb\ArbPayment\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use \Magento\Sales\Model\Order\Payment;
use \Arb\Vouchers\Model\VoucherOrders;
use \Magento\Sales\Model\Order;
use \Arb\ArbPayment\Model\ESBNotifications;
use \Arb\ArbPayment\Model\PaymentInquiry;
use \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;

/**
 * @covers \Arb\ArbPayment\Model\ResponseChecker
 */
// phpcs:disable
class ResponseCheckerTest extends TestCase
{
    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $jsonMock;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\ResponseChecker
     */
    private $responseChecker;
   
    /**
     * setUp Main set up method
     *
     * @return void
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->jsonMock = $this->getMockBuilder(\Magento\Framework\Serialize\Serializer\Json::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'unserialize'
                            ])
                            ->getMock();        
        $this->encryptionMock = $this->getMockBuilder(\Arb\ArbPayment\Model\Encryption::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                  'encryptAES',
                                  "decryptData",
                                  "decryptAES"
                              ])
                            ->getMock();
        $this->paymentMock = $this->getMockBuilder(Payment::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                              ])
                            ->getMock();
        $this->voucherOrdersMock = $this->getMockBuilder(VoucherOrders::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                              ])
                            ->getMock();
        $this->orderMock = $this->getMockBuilder(Order::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                "loadByIncrementId",
                                "getPayment",
                                "setPayment",
                                "cancel",
                                "save",
                                "canInvoice"
                              ])
                            ->getMock();
        $this->esbNotificationsMock = $this->getMockBuilder(ESBNotifications::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                              ])
                            ->getMock();
        $this->paymentInquiryMock = $this->getMockBuilder(PaymentInquiry::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                              ])
                            ->getMock();
        $this->transactionBuilder = $this->getMockBuilder(BuilderInterface::class)
                            ->disableOriginalConstructor()->getMockForAbstractClass();
       
        $this->transactionBuilder->method('setPayment')->will($this->returnSelf());
        $this->transactionBuilder->method('setOrder')->will($this->returnSelf());
        $this->transactionBuilder->method('setTransactionId')->will($this->returnSelf());
        $this->transactionBuilder->method('setAdditionalInformation')->will($this->returnSelf());
        $this->transactionBuilder->method('setFailSafe')->will($this->returnSelf());
        $this->transactionBuilder->method('build')->will($this->returnSelf());
        $this->responseCheckerMock = $this->objectManager->getObject(
            \Arb\ArbPayment\Model\ResponseChecker::class,
            [
                "encryption" => $this->encryptionMock,
                "json" => $this->jsonMock,
                "payment"=> $this->paymentMock,
                "vouchers"=>$this->voucherOrdersMock,
                "orderObject"=>$this->orderMock,
                "esbNotification"=>$this->esbNotificationsMock,
                "paymentInquiry"=>$this->paymentInquiryMock,
                "transactionBuilder"=>$this->transactionBuilder                
            ]
        );
    }
       
    /**
     * testGetTransactionDetails
     *
     * @return void
     */
    public function testGetTransactionDetails()
    {   
        $returnData = [
            'isValid' => true,
            '3ds' => "abls",
            'status' => true,
            'error' => null,
            'errorText' => null,
            'paymentId' => "21021862016",
            '3dsUrl' => "etste.com",
            'txnId' => "21706201",
            'trackId' => "1000000012",
            'ref' => "ref1y2",
            "status"=>true
        ];
        $returnData[0] =  [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                        "status"=>true
                    ];       
        $this->orderMock->method("loadByIncrementId")->willReturnSelf(); 
        $this->orderMock->method("getPayment")->willReturn($this->paymentMock); 
        $this->orderMock->method("setPayment")->willReturnSelf(); 
        $this->encryptionMock->method('decryptAES')->willReturn("decryptAES");
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn($returnData);
        $this->responseCheckerMock->getTransactionDetails($returnData);
        $this->responseCheckerMock->getTransactionDetails(0);
    }
        
    /**
     * testGetTransactionDetailsstatus
     *
     * @return void
     */
    public function testGetTransactionDetailsstatus()
    {   
        $returnData = [
            'isValid' => true,
            '3ds' => "abls",
            'status' => true,
            'error' => null,
            'errorText' => null,
            'paymentId' => "21021862016",
            '3dsUrl' => "etste.com",
            'txnId' => "21706201",
            'trackId' => "1000000012",
            'ref' => "ref1y2",
            "status"=>true
        ];
        $returnData[0] =  [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                        "status"=>true
                    ]; 
        $returnDataTrans[0] =  [
                        "trackId" => 100201934525118923,
                        "ref" => "trandata",
                        "Error" => "ErrorText",
                        "status"=>true
                    ];        
        $this->orderMock->method("loadByIncrementId")->willReturnSelf(); 
        $this->orderMock->method("getPayment")->willReturn($this->paymentMock); 
        $this->orderMock->method("setPayment")->willReturnSelf(); 
        $this->encryptionMock->method('decryptAES')->willReturn("decryptAES");
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn($returnData);
        $this->jsonMock->expects($this->at(1))->method('unserialize')->willReturn($returnDataTrans);
        $this->responseCheckerMock->getTransactionDetails($returnData);
        $this->responseCheckerMock->getTransactionDetails(0);
    }
    
    /**
     * testIsResponseValid
     *
     * @return void
     */
    public function testIsResponseValid()
    {   
        $returnData[0] = [
            'isValid' => true,
            '3ds' => "abls",
            'status' => true,
            'error' => null,
            'errorText' => null,
            'paymentId' => "21021862016",
            '3dsUrl' => "etste.com",
            'txnId' => "21706201",
            'trackId' => "1000000012",
            'ref' => "ref1y2"
        ];   
        $this->orderMock->method("loadByIncrementId")->willReturnSelf(); 
        $this->encryptionMock->method('decryptAES')->willReturn("decryptAES");
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn($returnData);
        $this->responseCheckerMock->getTransactionDetails($returnData);
    }
    
    /**
     * testIsThreeDTransaction
     *
     * @return void
     */
    public function testIsThreeDTransaction()
    {   
        $resposeData[0] = ["status"=>true,"result"=>"https://securepayments.alrajhibank.com.sa/pg/TranportalVbv.htm?paymentId=600202015638403311&id=gd97TJ7K3xKq8eQ"
        ];
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn($resposeData);
        $this->responseCheckerMock->getTransactionDetails("respose");
    }
     
     /**
      * testIsThreeDTransactionError
      *
      * @return void
      */
     public function testIsThreeDTransactionError()
    {   
        $resposeData[0] = ["orderId"=> "2000000269",
        "result"=> "error:teste",
        "error"=> "IPAY0100263",
        "errorText"=> "!ERROR!-IPAY0100263-Transaction not found.",
        "status"=> false
        ];
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn($resposeData);
        $this->responseCheckerMock->getTransactionDetails("respose");
    }
    
    /**
     * testIsThreeDTransactionNonArray
     *
     * @return void
     */
    public function testIsThreeDTransactionNonArray()
    {   
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn("respose");
        $this->responseCheckerMock->getTransactionDetails("respose");
    }    
    /**
     * testcreateOrderTrancation
     *
     * @return void
     */
    public function testcreateOrderTrancation()
    {   
        $data['transId']="001";
        $this->paymentMock->expects($this->at(0))->method('setTransactionId')->willReturnSelf();
        $this->paymentMock->expects($this->at(0))->method('setAdditionalInformation')->willReturnSelf();
        $this->paymentMock->method('setParentTransactionId')->will($this->returnSelf());
        $this->paymentMock->method('save')->will($this->returnSelf());
        $result=$this->responseCheckerMock->createOrderTrancation($this->paymentMock,$this->orderMock,$data);
        $this->assertEmpty($result);
    }    
    /**
     * testGetTransactionDetailsOrder
     *
     * @return void
     */
    public function testGetTransactionDetailsOrder()
    {   
        $returnData = [
            'isValid' => true,
            '3ds' => "abls",
            'status' => true,
            'error' => null,
            'errorText' => null,
            'paymentId' => "21021862016",
            '3dsUrl' => "etste.com",
            'txnId' => "21706201",
            'trackId' => "1000000012",
            'ref' => "ref1y2",
            "status"=>true
        ];
        $returnData[0] =  [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                        "status"=>true
                    ];       
        $this->orderMock->method("loadByIncrementId")->willReturnSelf(); 
        $this->orderMock->method("getPayment")->willReturn($this->paymentMock); 
        $this->orderMock->method("setPayment")->willReturnSelf(); 
        $this->encryptionMock->method('decryptAES')->willReturn("decryptAES");
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn($returnData);
        $this->responseCheckerMock->getTransactionDetails($returnData,'',null);
    }
    public function testGetTransactionDetailsOrderStatusFalse()
    {   
        $returnData = [
            'isValid' => true,
            '3ds' => "abls",
            'status' => false,
            'error' => null,
            'errorText' => null,
            'paymentId' => "21021862016",
            '3dsUrl' => "etste.com",
            'txnId' => "21706201",
            'trackId' => "1000000012",
            'ref' => "ref1y2"
        ];
        $returnData[0] =  [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                        "status"=>false
                    ];     
        $this->orderMock->method("loadByIncrementId")->willReturnSelf(); 
        $this->orderMock->method("getPayment")->willReturn($this->paymentMock); 
        $this->orderMock->method("setPayment")->willReturnSelf(); 
        $this->encryptionMock->method('decryptAES')->willReturn("decryptAES");
        $this->jsonMock->expects($this->at(0))->method('unserialize')->willReturn($returnData);
        $result=$this->responseCheckerMock->getTransactionDetails($returnData);
        $this->assertFalse($result['status']);
        $this->assertEquals("Cancelled",$result['orderStatus']);
    }
}