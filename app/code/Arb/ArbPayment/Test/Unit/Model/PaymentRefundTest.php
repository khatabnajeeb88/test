<?php

namespace Arb\ArbPayment\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Store\Model\ScopeInterface;
use Arb\ArbPayment\Model\PaymentRefund;
use BadMethodCallException;

/**
 * @covers \Arb\ArbPayment\Model\PaymentRefund
 */
class PaymentRefundTest extends TestCase
{
    /**
     * Mock paymentLog
     *
     * @var \Arb\ArbPayment\Model\PaymentLog|PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentLog;

    /**
     * Mock encryptor
     *
     * @var \Arb\ArbPayment\Model\Encryption|PHPUnit_Framework_MockObject_MockObject
     */
    private $encryptor;

    /**
     * Mock curl
     *
     * @var \Magento\Framework\HTTP\Client\Curl|PHPUnit_Framework_MockObject_MockObject
     */
    private $curl;

    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $json;

    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\PaymentRefund
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->paymentLog = $this->createMock(\Arb\ArbPayment\Model\PaymentLog::class);
        $getValueReturnMap = [
            ["payment/arbpayment_gateway/vector_init", ScopeInterface::SCOPE_STORE, null, "PGKEYENCDECIVSPC"],
            ["payment/arbpayment_gateway/merchant_id", ScopeInterface::SCOPE_STORE, null, 10],
            ["payment/arbpayment_gateway/merchant_password", ScopeInterface::SCOPE_STORE, null, "password"],
            [
                "payment/arbpayment_gateway/pg_terminal_resourcekey",
                ScopeInterface::SCOPE_STORE,
                null,
                "92978803765192978803765192978803"
            ],
            ["payment/arbpayment_gateway/pg_currency_code", ScopeInterface::SCOPE_STORE, null, "USD"],
            ["payment/arbpayment_gateway/pg_inquiry_url", ScopeInterface::SCOPE_STORE, null, "http://test.com"]
        ];
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->scopeConfig->expects($this->any())
            ->method('getValue')
            ->will($this->returnValueMap($getValueReturnMap));

        $this->encryptionModel = $this->getMockBuilder(\Arb\ArbPayment\Model\Encryption::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'encryptAES',
                "decryptData",
                "decryptAES"
            ])
            ->getMock();
        $this->curl = $this->createMock(\Magento\Framework\HTTP\Client\Curl::class);
        $this->json = $this->getMockBuilder(\Magento\Framework\Serialize\Serializer\Json::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'unserialize'
            ])
            ->getMock();
        $this->testObject = $this->objectManager->getObject(
            PaymentRefund::class,
            [
                'paymentLog' => $this->paymentLog,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'scopeConfig' => $this->scopeConfig,
            ]
        );
    }
    /**
     * invokeMethod
     *
     * @param  mixed $object
     * @param  mixed $methodName
     * @param  mixed $parameters
     * @return void
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * testRefundAPIRequest
     *
     * @return void
     */
    public function testRefundAPIRequest()
    {
        $curlResponse = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $curlResponse[0] = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] = ["result" => "success", "ref" => 12312312];

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($decodedTransData);
        $data[0] = ["paymentId" => "-1"];
        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($data);
        $this->encryptionModel->method('decryptAES')->willReturn("decryptAES");
        $paymentData[] =
            [
                'amt' => '42.00',
                'action' => '2',
                'password' => 'w3j$1$I3wM6WcM@',
                'id' => 'KUmW2iI41n3sY8q',
                'currencyCode' => '682',
                'trackId' => '855291596524096',
                'udf5' => 'TrackID',
                'transId' => '2000000323',
            ];
        $this->testObject->refundAPIRequest($paymentData, \Arb\ArbPayment\Model\PaymentLog::REFUND);
    }
    /**
     * testRefundAPIRequest
     *
     * @return void
     */
    public function testRefundAPIRequestURL()
    {
        $curlResponse = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $curlResponse[0] = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] = ["result" => "success", "ref" => 12312312];

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($decodedTransData);
        $data[0] = ["paymentId" => "-1"];
        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($data);
        $this->encryptionModel->method('decryptAES')->willReturn("decryptAES");
        $paymentData[] =
            [
                'amt' => '42.00',
                'action' => '2',
                'password' => 'w3j$1$I3wM6WcM@',
                'id' => 'KUmW2iI41n3sY8q',
                'currencyCode' => '682',
                'trackId' => '855291596524096',
                'udf5' => 'TrackID',
                'transId' => '2000000323',
            ];
        $this->testObject->refundAPIRequest($paymentData, \Arb\ArbPayment\Model\PaymentLog::REFUND_INQUIRY);
    }

    /**
     * testgetRefundAPIData
     *
     * @return void
     */
    public function testgetRefundAPIData()
    {
        $refundData = ['amt' => '42.00', 'transId' => '1000001'];
        $this->testObject->getRefundAPIData($refundData, 2);
    }

    /**
     * testprocessRefundInquiry
     *
     * @return void
     */
    public function testprocessRefundInquiry()
    {
        $paymentData[] =
            [
                'amt' => '42.00',
                'action' => '2',
                'password' => 'w3j$1$I3wM6WcM@',
                'id' => 'KUmW2iI41n3sY8q',
                'currencyCode' => '682',
                'trackId' => '855291596524096',
                'udf5' => 'TrackID',
                'transId' => '2000000323',
            ];

        $this->testObject->processRefundInquiry($paymentData);
    }

    /**
     * testprocessRefundInquiryInvalidParam
     *
     * @return void
     */
    public function testprocessRefundInquiryInvalidParam()
    {
        $paymentData =
            [
                'transId' => '2000000323',
            ];

        $this->testObject->processRefundInquiry($paymentData);
    }
    /**
     * testprocessRefundResponse
     *
     * @return void
     */
    public function testprocessRefundResponse()
    {
        $responseData[0]['status'] = 0;
        $orderId = '001';
        $this->json->method('unserialize')->willReturn($responseData);
        $result = $this->invokeMethod($this->testObject, 'processRefundResponse', array($responseData, $orderId));
        $this->assertFalse($result['status']);
    }
    /**
     * testprocessRefundStatus
     *
     * @return void
     */
    public function testprocessRefundStatus()
    {
        $curlResponse = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $curlResponse[0] = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] = ["result" => "success", "ref" => 12312312];

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($decodedTransData);
        $orderId = '001';
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentRefund');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->testObject, ['resource_key' => 'abc']);
        $result = $this->invokeMethod($this->testObject, 'processRefundResponse', array($curlResponse, $orderId));
        $this->assertFalse($result['status']);
    }
    /**
     * testprocessRefundRef
     *
     * @return void
     */
    public function testprocessRefundRef()
    {
        $curlResponse = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $curlResponse[0] = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] = ["result" => "success", "ref" => 12312312];

        $this->json->expects($this->at(1))->method('unserialize')->willReturn($decodedTransData);
        $orderId = '001';
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentRefund');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->testObject, ['resource_key' => 'abc']);
        $result = $this->invokeMethod($this->testObject, 'processRefundResponse', array($curlResponse, $orderId));
        $this->assertTrue($result['status']);
        $this->assertEquals('success', $result['transData']['result']);
        $this->assertEquals($decodedTransData[0]['ref'], $result['transData']['ref']);
    }    
    /**
     * testprocessRefundStatusFalse
     *
     * @return void
     */
    public function testprocessRefundStatusFalse()
    {
        $curlResponse = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "1"
        ];

        $curlResponse[0] = [
            "paymentId" => 100201934525118923,
            "trandata" => "trandata",
            "Error" => "ErrorText",
            "status" => "0"
        ];

        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));

        $this->json->expects($this->at('any'))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] = ["result" => "success", "ref" => 12312312];
        $orderId = '001';
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentRefund');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->testObject, ['resource_key' => 'abc']);
        $result = $this->invokeMethod($this->testObject, 'processRefundResponse', array($curlResponse, $orderId));
        $this->assertFalse($result['status']);
    }    
    /**
     * testprocessRefundStatusResponse
     *
     * @return void
     */
    public function testprocessRefundStatusResponse()
    {
        $result = $this->invokeMethod($this->testObject, 'processRefundResponse', array(null, null));
        $this->assertFalse($result['status']);
    }

}
