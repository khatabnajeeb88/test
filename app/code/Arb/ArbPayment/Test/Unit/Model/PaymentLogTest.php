<?php
namespace Arb\ArbPayment\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Zend\Log\Logger;

/**
 * @covers \Arb\ArbPayment\Model\PaymentLog
 */
class PaymentLogTest extends TestCase
{
    /**
     * Mock context
     *
     * @var \Magento\Framework\Model\Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * Mock registry
     *
     * @var \Magento\Framework\Registry|PHPUnit_Framework_MockObject_MockObject
     */
    private $registry;

    /**
     * Mock resource
     *
     * @var \Magento\Framework\Model\ResourceModel\AbstractResource|PHPUnit_Framework_MockObject_MockObject
     */
    private $resource;

    /**
     * Mock resourceCollection
     *
     * @var \Magento\Framework\Data\Collection\AbstractDb|PHPUnit_Framework_MockObject_MockObject
     */
    private $resourceCollection;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\PaymentLog
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\Model\Context::class);
        $this->registry = $this->createMock(\Magento\Framework\Registry::class);
        $this->resource = $this->getMockBuilder(\Magento\Framework\Model\ResourceModel\AbstractResource::class)
                                ->setMethods([
                                    'getIdFieldName',
                                    'getConnection',
                                    '_construct',
                                    'load',
                                    'save',
                                    'getId'
                                ])
                                ->getMock();
        $this->resourceCollection = $this->getMockBuilder(\Magento\Framework\Data\Collection\AbstractDb::class)
                                        ->disableOriginalConstructor()
                                        ->getMock();
        $this->_loggerMock = $this->getMockBuilder(Logger::class)
                                ->setMethods([
                                    'addWriter',
                                    'info'
                                ])
                                ->getMock();

        $this->_loggerMock->expects($this->any())->method("addWriter")->willReturnSelf();
        $this->_loggerMock->expects($this->any())->method("info")->willReturnSelf();
        $this->context->expects($this->any())->method("getLogger")->willReturn($this->_loggerMock);
        
        $this->testObject = $this->objectManager->getObject(
            \Arb\ArbPayment\Model\PaymentLog::class,
            [
                'context' => $this->context,
                'registry' => $this->registry,
                'resource' => $this->resource,
                'resourceCollection' => $this->resourceCollection,
                'data' => []
            ]
        );

        $this->testObject->getIdentities();
    }

    /**
     * @dataProvider response
     */
    public function testSave($data)
    {
        $this->resource->method('save')->will(
            $this->returnSelf()
        );

        $result = $this->testObject->setPaymentLog($data);
    }

   /**
    * @dataProvider response
    * @expectedException \Magento\Framework\Exception\LocalizedException
    */
    public function testUpdateLogException($data)
    {
        $this->resource->method('save')->willThrowException(new \Magento\Framework\Exception\LocalizedException(
            __("Unable to add payment logs")
        ));
        $this->testObject->setPaymentLog($data);
    }

    /**
     * @dataProvider response
     *
     */
    public function testUpdatePaymentLog($data)
    {
        $id = 1;
        $this->testObject->updatePaymentLog($id, $data);
    }

    /**
     * @dataProvider response
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testUpdatePaymentLogException($data)
    {
        $id = 1;
        $this->resource->method('load')->willThrowException(new \Magento\Framework\Exception\LocalizedException(
            __("Unable to add payment logs")
        ));
        $this->testObject->updatePaymentLog($id, $data);
    }

    public function response()
    {
        return [
            "data" => [ 'order_id' => 1,
            'api_type' => 'purchase',
            'inquiry_count' => 1
            ]
        ];
    }

    public function testLogPaymentException()
    {
        $this->testObject->logPaymentException("error message");
    }
}
