<?php
namespace Arb\ArbPayment\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use \Magento\Store\Model\ScopeInterface;

/**
 * @covers \Arb\ArbPayment\Model\PaymentPurchase
 */
class PaymentPurchaseTest extends TestCase
{
    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock order
     *
     * @var \Magento\Sales\Model\Order|PHPUnit_Framework_MockObject_MockObject
     */
    private $order;

    /**
     * Mock curl
     *
     * @var \Magento\Framework\HTTP\Client\Curl|PHPUnit_Framework_MockObject_MockObject
     */
    private $curl;

    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $json;

    /**
     * Mock responseChecker
     *
     * @var \Arb\ArbPayment\Model\ResponseChecker|PHPUnit_Framework_MockObject_MockObject
     */
    private $responseChecker;

    /**
     * Mock paymentLog
     *
     * @var \Arb\ArbPayment\Model\PaymentLog|PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentLog;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\PaymentPurchase
     */
    private $testObject;

    /**
     * Main set up method
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->payment = $this->getMockBuilder(\Magento\Sales\Api\Data\OrderPaymentInterface::class)
                              ->disableOriginalConstructor()
                              ->setMethods([
                                  'setArbPaymentId',
                                  'setArbTrackId',
                                  'getAdditionalData',
                                  'getAdditionalInformation',
                                  'getAddressStatus',
                                  'getAmountAuthorized',
                                  'getAmountCanceled',
                                  'getAmountOrdered',
                                  'getAmountPaid',
                                  'getAmountRefunded',
                                  'getAnetTransMethod',
                                  'getBaseAmountAuthorized',
                                  'getBaseAmountCanceled',
                                  'getBaseAmountOrdered',
                                  'getBaseAmountPaid',
                                  'getBaseAmountPaidOnline',
                                  'getAccountStatus',
                                  'getBaseAmountRefunded',
                                  'getBaseAmountRefundedOnline',
                                  'getBaseShippingAmount',
                                  'getBaseShippingCaptured',
                                  'getBaseShippingRefunded',
                                  'getCcApproval',
                                  'getCcAvsStatus',
                                  'getCcCidStatus',
                                  'getCcDebugRequestBody',
                                  'getCcDebugResponseBody',
                                  'getCcDebugResponseSerialized',
                                  'getCcExpMonth',
                                  'getCcExpYear',
                                  'getCcLast4',
                                  'getCcNumberEnc',
                                  'getCcOwner',
                                  'getCcSecureVerify',
                                  'getCcSsIssue',
                                  'getCcSsStartMonth',
                                  'getCcSsStartYear',
                                  'getCcStatus',
                                  'getCcStatusDescription',
                                  'getCcTransId',
                                  'getCcType',
                                  'getEcheckAccountName',
                                  'getEcheckAccountType',
                                  'getEcheckBankName',
                                  'getEcheckRoutingNumber',
                                  'getEcheckType',
                                  'getEntityId',
                                  'setEntityId',
                                  'getLastTransId',
                                  'getMethod',
                                  'getParentId',
                                  'getPoNumber',
                                  'getProtectionEligibility',
                                  'getQuotePaymentId',
                                  'getShippingAmount',
                                  'getShippingCaptured',
                                  'getShippingRefunded',
                                  'setParentId',
                                  'setBaseShippingCaptured',
                                  'setShippingCaptured',
                                  'setAmountRefunded',
                                  'setBaseAmountPaid',
                                  'setAmountCanceled',
                                  'setBaseAmountAuthorized',
                                  'setBaseAmountPaidOnline',
                                  'setBaseAmountRefundedOnline',
                                  'setBaseShippingAmount',
                                  'setShippingAmount',
                                  'setAmountPaid',
                                  'setAmountAuthorized',
                                  'setBaseAmountOrdered',
                                  'setBaseShippingRefunded',
                                  'setShippingRefunded',
                                  'setBaseAmountRefunded',
                                  'setAmountOrdered',
                                  'setBaseAmountCanceled',
                                  'setQuotePaymentId',
                                  'setAdditionalData',
                                  'setCcExpMonth',
                                  'setCcSsStartYear',
                                  'setEcheckBankName',
                                  'setMethod',
                                  'setCcDebugRequestBody',
                                  'setCcSecureVerify',
                                  'setProtectionEligibility',
                                  'setCcApproval',
                                  'setCcLast4',
                                  'setCcStatusDescription',
                                  'setEcheckType',
                                  'setCcDebugResponseSerialized',
                                  'setCcSsStartMonth',
                                  'setEcheckAccountType',
                                  'setLastTransId',
                                  'setCcCidStatus',
                                  'setCcOwner',
                                  'setCcType',
                                  'setPoNumber',
                                  'setCcExpYear',
                                  'setCcStatus',
                                  'setEcheckRoutingNumber',
                                  'setAccountStatus',
                                  'setAnetTransMethod',
                                  'setCcDebugResponseBody',
                                  'setCcSsIssue',
                                  'setEcheckAccountName',
                                  'setCcAvsStatus',
                                  'setCcNumberEnc',
                                  'setCcTransId',
                                  'setAddressStatus',
                                  'getExtensionAttributes',
                                  'setExtensionAttributes',
                                  'save',
                                  "setArbRefId"
                              ])
                              ->getMock();
        $this->order = $this->createMock(\Magento\Sales\Model\Order::class);

        $this->order->method('loadByIncrementId')->will($this->returnSelf());
        $this->order->method('getPayment')->willReturn($this->payment);
        $this->order->method('setStatus')->will($this->returnSelf());
        $this->order->method('setState')->will($this->returnSelf());
        $this->curl =  $this->createMock(\Magento\Framework\HTTP\Client\Curl::class);
        $this->json = $this->objectManager->getObject(\Magento\Framework\Serialize\Serializer\Json::class, []);
        $this->responseChecker = $this->createMock(\Arb\ArbPayment\Model\ResponseChecker::class);
        $this->paymentLog = $this->createMock(\Arb\ArbPayment\Model\PaymentLog::class);
        $this->testObject = $this->objectManager->getObject(
            \Arb\ArbPayment\Model\PaymentPurchase::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'order' => $this->order,
                'curl' => $this->curl,
                'json' => $this->json,
                'responseCheck' => $this->responseChecker,
                'paymentLog' => $this->paymentLog,
            ]
        );
    }

    /**
     * @return array
     */
    public function dataProviderForTestProcessPaymentPurchase()
    {
        return [
            'Testcase 1' => [
                'orderId' => 1,
                'trandata' => null,
                "responseURL" => "localhost",
                "errorURL" =>  "localhost"
            ]
        ];
    }

    /**
     * @dataProvider dataProviderForTestProcessPaymentPurchase
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testProcessPaymentPurchaseException($orderId, $transdata, $responseUrl, $errorUrl)
    {
        $getValueReturnMap = [
            [ "payment/arbpayment_gateway/vector_init" , ScopeInterface::SCOPE_STORE , null , "nlkqneqe"],
            [ "payment/arbpayment_gateway/merchant_id", ScopeInterface::SCOPE_STORE , null , "lneqle"],
            [ "payment/arbpayment_gateway/merchant_password", ScopeInterface::SCOPE_STORE , null , "password"],
            [ "payment/arbpayment_gateway/pg_terminal_resourcekey",
                ScopeInterface::SCOPE_STORE , null ,
                "929788037651929"
            ],
            [ "payment/arbpayment_gateway/pg_currency_code",
             ScopeInterface::SCOPE_STORE ,
             null ,
             "682"
            ],
            [ "payment/arbpayment_gateway/pg_inquiry_url",
            ScopeInterface::SCOPE_STORE ,
            null ,
            "https://securepayments.alrajhibank.com.sa/pg/payment/tranportal.htm"
            ]
        ];
        $this->order->method('loadByIncrementId')->willThrowException(
            new \Magento\Framework\Exception\LocalizedException(
                __("Invalid Order Id")
            )
        );

        $this->scopeConfig->expects($this->any())
             ->method('getValue')
             ->will($this->returnValueMap($getValueReturnMap));
        $this->curl->method('getBody')->willThrowException(
            new \Magento\Framework\Exception\LocalizedException(
                __("Payment Gateway Error")
            )
        );
        $this->testObject->processPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl);
    }

    /**
     * @dataProvider dataProviderForTestProcessPaymentPurchase
     *
     */
    public function testProcessPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl)
    {
        $this->testObject->processPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl);
        $this->responseChecker->method('getTransactionDetails')->willReturn(
            [
                '3ds' => true,
                '3dsUrl' => '',
                'isValid' => false,
                'paymentId' => 100,
                "ref"=>"ref_26892"
            ]
        );

        $this->assertInternalType('array', $this->testObject->processPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl));
    }

     /**
      * @dataProvider dataProviderForTestProcessPaymentPurchase
      * @expectedException \Magento\Framework\Exception\LocalizedException
      */
    public function testProcessPaymentPurchaseNonThree($orderId, $transdata, $responseUrl, $errorUrl)
    {
        $this->testObject->processPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl);
        $this->responseChecker->method('getTransactionDetails')->willReturn(
            ["0"=>[
                'orderStatus' => "complete",
                '3dsUrl' => '',
                'status' => true,
                'paymentId' => 100,
                "ref"=>"ref_26892"
            ]
            ]
        );
        
        $this->order->method('getId')->willReturn(1);
        $this->order->method('getStatus')->willReturn('pending');
        $this->order->method('getPayment')->willReturn($this->payment);
        $this->payment->method('setArbPaymentId')->willReturn($this->returnSelf());
        $this->payment->method('setArbTrackId')->willReturn($this->returnSelf());
        $this->testObject->processPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl);
    }
    /**
     * @dataProvider dataProviderForTestProcessPaymentPurchase
     */
    public function testProcessPaymentPurchaseInvalid($orderId, $transdata, $responseUrl, $errorUrl)
    {
        $this->testObject->processPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl);
        $this->responseChecker->method('getTransactionDetails')->willReturn(
            [
                '3ds' => true,
                '3dsUrl' => '',
                'isValid' => true,
                'paymentId' => 100,
                "ref"=>"ref_26892"
            ]
        );
        
        $this->order->method('getId')->willReturn(1);
        $this->order->method('getStatus')->willReturn('pending');
        $this->order->method('getPayment')->willReturn($this->payment);
        $this->payment->method('setArbPaymentId')->willReturn($this->returnSelf());
        $this->payment->method('setArbTrackId')->willReturn($this->returnSelf());
        $this->testObject->processPaymentPurchase($orderId, $transdata, $responseUrl, $errorUrl);
    }
}
