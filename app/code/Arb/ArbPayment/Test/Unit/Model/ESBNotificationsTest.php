<?php
namespace Arb\ArbPayment\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * @covers \Arb\ArbPayment\Model\ESBNotifications
 */
class ESBNotificationsTest extends TestCase
{
    /**
     * Mock context
     *
     * @var \Magento\Framework\Model\Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * Mock resource
     *
     * @var \Magento\Framework\Model\ResourceModel\AbstractResource|PHPUnit_Framework_MockObject_MockObject
     */
    private $resource;

    /**
     * Mock resourceCollection
     *
     * @var \Magento\Framework\Data\Collection\AbstractDb|PHPUnit_Framework_MockObject_MockObject
     */
    private $resourceCollection;

    /**
     * Mock registry
     *
     * @var \Magento\Framework\Registry|PHPUnit_Framework_MockObject_MockObject
     */
    private $registry;

    /**
     * Mock orderFactoryInstance
     *
     * @var \Magento\Sales\Model\Order|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderFactoryInstance;

    /**
     * Mock orderFactory
     *
     * @var \Magento\Sales\Model\OrderFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderFactory;

    /**
     * Mock itemMock
     *
     * @var \Magento\Sales\Model\Order\Item|PHPUnit_Framework_MockObject_MockObject
     */
    private $itemMock;

    /**
     * Mock itemFactory
     *
     * @var \Magento\Sales\Model\Order\ItemFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $itemFactory;

    /**
     * Mock esbHelper
     *
     * @var \Arb\EsbNotifications\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $esbHelper;

    /**
     * Mock mpHelper
     *
     * @var \Webkul\MpAdvancedCommission\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $mpHelper;

    /**
     * Mock wpHelper
     *
     * @var \Webkul\Marketplace\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $wpHelper;

    /**
     * Mock serializer
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $serializer;

    /**
     * Mock encryptor
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $encryptor;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\ESBNotifications
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\Model\Context::class);
        $this->resource = $this->createMock(\Magento\Framework\Model\ResourceModel\AbstractResource::class);
        $this->resourceCollection = $this->createMock(\Magento\Framework\Data\Collection\AbstractDb::class);
        $this->registry = $this->createMock(\Magento\Framework\Registry::class);
        $this->orderFactory = $this->getMockBuilder(\Magento\Sales\Model\OrderFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create','getCollection','addAddressFields','addAttributeToFilter',
                'getFirstItem','setBaseTotalDue','setCustomerEmail','setGrandTotal',
                'setCreatedAt','setIncrementId','setBaseGrandTotal','setItems',
                "setTotalItemCount","load","getId","getBaseGrandTotal","getPayment",
                "getBaseTotalDue","getCustomerEmail","getGrandTotal","getCreatedAt",
                "getIncrementId","getTotalItemCount","getItems","getData",
                "getCustomerFirstname","getCustomerLastname","getAllItems",
                "getStore","getCode","getBillingAddress","getTelephone"
                ])
            ->getMock();
        
        $this->itemFactory = $this->getMockBuilder(\Magento\Sales\Model\Order\ItemFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([ 'create','getVouchers',"getData","getId","getProductType"])
            ->getMock();
        $this->itemFactory->method('create')->willReturnSelf();
        $this->itemFactory->method("getVouchers")->willReturn("teste");
        $this->itemFactory->method("getProductType")->willReturn("virtual");
        $this->esbHelper = $this->getMockBuilder(\Arb\EsbNotifications\Helper\Data::class)
            ->disableOriginalConstructor()
            ->setMethods(['sendEmailNotification',"sendSmsNotification"])
            ->getMock();
       
        $this->wpHelper = $this->getMockBuilder(\Webkul\Marketplace\Helper\Data::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'getSellerDataBySellerId','getData'
                            ])
                            ->getMock();
        $this->wpHelper->expects($this->any())->method("getSellerDataBySellerId")->willReturnSelf();
        $this->wpHelper->expects($this->any())->method("getData")->willReturn([
            "0"=>[
                "shop_title"=>"test"
                ]
        ]);
        $this->_customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                "getById",
                                "getCustomAttribute",
                                "getValue"
                            ])
                            ->getMockForAbstractClass();
        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("2168963286");
       
        $this->serializer = $this->getMockBuilder(\Magento\Framework\Serialize\Serializer\Json::class)
                             ->disableOriginalConstructor()
                            ->setMethods([
                                'unserialize'
                            ])
                            ->getMock();
        $this->mpHelper = $this->getMockBuilder(\Webkul\MpAdvancedCommission\Helper\Data::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSellerIdByItem'])
            ->getMock();
        $this->mpHelper->method("getSellerIdByItem")->willReturn("2");
        
        $this->encryptor = $this->createMock(\Magento\Framework\Encryption\EncryptorInterface::class);
        $this->testObject = $this->objectManager->getObject(
            \Arb\ArbPayment\Model\ESBNotifications::class,
            [
                'context' => $this->context,
                'resource' => $this->resource,
                'resourceCollection' => $this->resourceCollection,
                'registry' => $this->registry,
                'orderFactory' => $this->orderFactory,
                'itemFactory' => $this->itemFactory,
                'esbHelper' => $this->esbHelper,
                'mpHelper' => $this->mpHelper,
                'wkHelper' => $this->wpHelper,
                'serializer' => $this->serializer,
                'encryptor' => $this->encryptor,
                "_customerRepository"=>$this->_customerRepositoryMock
            ]
        );

        $this->orderFactory->method('create')->will(
            $this->returnSelf()
        );
        $this->orderFactory->method('load')->will(
            $this->returnSelf()
        );
        $this->orderFactory->expects($this->any())->method('getStore')->willReturnSelf();
        $this->orderFactory->expects($this->any())->method('getBillingAddress')->willReturnSelf();
        $this->orderFactory->expects($this->any())->method('getTelephone')->willReturn("2709260960");
    }

    /**
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testOrderIdNotExist()
    {
        $increment_id = "000000005";
        $this->testObject->prepareOrderData($increment_id);
    }

    /**
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testPrepareOrderDataException()
    {
        $this->orderFactory->expects($this->any())->method('getCode')->willReturn("ar_SA");
        $items = [$this->itemFactory];
        $this->itemFactory->method("getData")->willReturn([
            "row_total"=>"120",
            "name"=>"Teste",
            "vouchers"=>"teste_vouchers"
        ]);
        $this->orderFactory->method('getAllItems')->willReturn(
            $items
        );
        $this->orderFactory->method('getId')->willReturn("5");
        $this->orderFactory->method('getPayment')->willReturnSelf();
        $this->orderFactory->expects($this->any())->method('getCode')->willReturn("EN");
        $this->orderFactory->method('getData')->willReturn(["last_trans_id"=>"dslgase",
                                                        "arb_payment_id"=>"26986986",
                                                        "arb_ref_id"=>"sagdjgwe"
                                                        ]);
        $increment_id = "000000005";
        $this->testObject->prepareOrderData($increment_id);
    }

    public function testPrepareOrderData()
    {
        $this->esbHelper->expects($this->any())->method("sendEmailNotification")
        ->willReturn(json_encode(["success"=>true]));
        $this->esbHelper->expects($this->any())->method("sendSmsNotification")
        ->willReturn(json_encode(["success"=>true]));
        $this->serializer->method('unserialize')->willReturn(["voucher1"]);
        $items = [$this->itemFactory];
        $this->itemFactory->method("getData")->willReturn([
            "row_total"=>"120",
            "name"=>"Teste",
            "vouchers"=>"teste_vouchers"
        ]);
        $this->itemFactory->method('getId')->willReturn(12);
        $this->orderFactory->method('getAllItems')->willReturn(
            $items
        );
        $this->orderFactory->method('getId')->willReturn("5");
        $this->orderFactory->method('getPayment')->willReturnSelf();
        $this->orderFactory->expects($this->any())->method('getCode')->willReturn("EN");
        $this->orderFactory->method('getData')->willReturn(["last_trans_id"=>"dslgase",
                                                        "arb_payment_id"=>"26986986",
                                                        "arb_ref_id"=>"sagdjgwe"
                                                        ]);
        $increment_id = "000000005";
        $this->testObject->prepareOrderData($increment_id);
    }

    public function testPrepareOrderDataName()
    {
        $this->esbHelper->expects($this->any())->method("sendEmailNotification")
        ->willReturn(json_encode(["success"=>true]));
        $this->esbHelper->expects($this->any())->method("sendSmsNotification")
        ->willReturn(json_encode(["success"=>true]));
        $this->serializer->method('unserialize')->willReturn(["voucher1"]);
        $items = [$this->itemFactory];
        $this->itemFactory->method("getData")->willReturn([
            "row_total"=>"120",
            "name"=>"Teste",
            "vouchers"=>"teste_vouchers"
        ]);
        $this->itemFactory->method('getId')->willReturn(12);
        $this->orderFactory->method('getAllItems')->willReturn(
            $items
        );
        $this->orderFactory->method('getId')->willReturn("5");
        $this->orderFactory->method('getPayment')->willReturnSelf();
        $this->orderFactory->expects($this->any())->method('getCode')->willReturn("EN");
        $this->orderFactory->expects($this->any())->method('getCustomerLastname')->willReturn("arb");
        $this->orderFactory->method('getData')->willReturn(["last_trans_id"=>"dslgase",
                                                        "arb_payment_id"=>"26986986",
                                                        "arb_ref_id"=>"sagdjgwe"
                                                        ]);
        $increment_id = "000000005";
        $this->testObject->prepareOrderData($increment_id);
    }

    public function testPrepareOrderDataError()
    {
        $this->esbHelper->expects($this->any())
        ->method('sendEmailNotification')
        ->willThrowException(new \Exception("invalid"));
        $this->serializer->method('unserialize')->willReturn(["voucher1"]);
        $items = [$this->itemFactory];
        $this->itemFactory->method("getData")->willReturn([
            "row_total"=>"120",
            "name"=>"Teste",
            "vouchers"=>"teste_vouchers"
        ]);
        $this->itemFactory->method('getId')->willReturn(12);
        $this->orderFactory->method('getAllItems')->willReturn(
            $items
        );
        $this->orderFactory->method('getId')->willReturn("5");
        $this->orderFactory->method('getPayment')->willReturnSelf();
        $this->orderFactory->expects($this->any())->method('getCode')->willReturn("EN");
        $this->orderFactory->method('getData')->willReturn(["last_trans_id"=>"dslgase",
                                                        "arb_payment_id"=>"26986986",
                                                        "arb_ref_id"=>"sagdjgwe"
                                                        ]);
        $increment_id = "000000005";
        $this->testObject->prepareOrderData($increment_id);
    }
}
