<?php
namespace Arb\ArbPayment\Test\Unit\Model;

use BadMethodCallException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use \Arb\ArbPayment\Model\PaymentInquiry;
use \Magento\Framework\Serialize\Serializer\Json;
/**
 * @covers \Arb\ArbPayment\Model\PaymentInquiry
 */
// phpcs:disable
class PaymentInquiryTest extends TestCase
{
    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock payment
     *
     * @var \Magento\Sales\Model\Order\Payment|PHPUnit_Framework_MockObject_MockObject
     */
    private $payment;

    /**
     * Mock encryptor
     *
     * @var \Arb\ArbPayment\Model\Encryption|PHPUnit_Framework_MockObject_MockObject
     */
    private $encryptor;

    /**
     * Mock curl
     *
     * @var \Magento\Framework\HTTP\Client\Curl|PHPUnit_Framework_MockObject_MockObject
     */
    private $curl;

    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $json;

    /**
     * Mock paymentLog
     *
     * @var \Arb\ArbPayment\Model\PaymentLog|PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentLog;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\ArbPayment\Model\PaymentInquiry
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->jsonObj =  $this->objectManager->getObject(Json::class);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->voucher = $this->getMockBuilder(\Arb\Vouchers\Model\VoucherOrders::class)
                              ->disableOriginalConstructor()
                              ->setMethods([
                                  'assignVouchers',
                                  'unBlockVouchers'
                              ])
                              ->getMock();
        $this->esbNotification = $this->getMockBuilder(\Arb\ArbPayment\Model\ESBNotifications::class)
                              ->disableOriginalConstructor()
                              ->setMethods([
                                  'prepareOrderData'
                              ])
                              ->getMock();
        $this->order = $this->createMock(\Magento\Sales\Model\Order::class);
        $this->invoiceService = $this->getMockBuilder(\Magento\Sales\Model\Service\InvoiceService::class)
                                ->disableOriginalConstructor()
                              ->setMethods([
                                  'prepareInvoice'
                              ])
                              ->getMock();  

        $this->invoiceServiceInterface = $this->getMockBuilder(\Magento\Sales\Api\Data\InvoiceInterface::class)
                                ->disableOriginalConstructor()
                              ->setMethods([
                                  'setRequestedCaptureCase',
                                  'register',
                                  "getOrder"
                              ])
                              ->getMockForAbstractClass();      

        $this->order->method('setState')->willReturnSelf();
        $this->order->method('setStatus')->willReturnSelf();
        $this->payment = $this->getMockBuilder(\Magento\Sales\Model\Order\Payment::class)
                              ->disableOriginalConstructor()
                              ->setMethods([
                                  'load',
                                  'getArbTrackId',
                                  'getParentId',
                                  'getAmountOrdered',
                                  'getOrder',
                                  'setTransactionId',
                                  'setParentTransactionId',
                                  'save',
                                  'getLastTransId',
                                  'setData'
                              ])
                              ->getMock();
        $this->payment->method('load')->will($this->returnSelf());
        $this->payment->method('getOrder')->willReturn($this->order);
        $this->encryptor = $this->createMock(\Arb\ArbPayment\Model\Encryption::class);
        $this->transactionFactory = $this->getMockBuilder(\Magento\Framework\DB\TransactionFactory::class)
                            ->disableOriginalConstructor()
                              ->setMethods([
                                  'create',
                                  "addObject",
                                    "save"
                              ])
                              ->getMock();
        $this->curl = $this->createMock(\Magento\Framework\HTTP\Client\Curl::class);
        $this->json = $this->getMockBuilder(\Magento\Framework\Serialize\Serializer\Json::class)
                            ->disableOriginalConstructor()
                              ->setMethods([
                                  'unserialize'
                              ])
                              ->getMock();

        $this->paymentLog = $this->createMock(\Arb\ArbPayment\Model\PaymentLog::class);
        $getValueReturnMap = [
            [ "payment/arbpayment_gateway/vector_init" , ScopeInterface::SCOPE_STORE , null , "PGKEYENCDECIVSPC"],
            [ "payment/arbpayment_gateway/merchant_id", ScopeInterface::SCOPE_STORE , null , 10],
            [ "payment/arbpayment_gateway/merchant_password", ScopeInterface::SCOPE_STORE , null , "password"],
            [ "payment/arbpayment_gateway/pg_terminal_resourcekey",
            ScopeInterface::SCOPE_STORE ,
            null ,
            "92978803765192978803765192978803"
            ],
            [ "payment/arbpayment_gateway/pg_currency_code", ScopeInterface::SCOPE_STORE , null , "USD"],
            [ "payment/arbpayment_gateway/pg_inquiry_url", ScopeInterface::SCOPE_STORE , null , "http://test.com"]
        ];
        $this->scopeConfig->expects($this->any())
             ->method('getValue')
             ->will($this->returnValueMap($getValueReturnMap));

        $this->encryptionModel = $this->getMockBuilder(\Arb\ArbPayment\Model\Encryption::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                  'encryptAES',
                                  "decryptData",
                                  "decryptAES"
                              ])
                            ->getMock();
        $this->encryptionModel->method('decryptData')->willReturn("abc");
        $this->transactionBuilder = $this->getMockBuilder(BuilderInterface::class)
                            ->disableOriginalConstructor()->getMockForAbstractClass();
       
        $this->transactionBuilder->method('setPayment')->will($this->returnSelf());
        $this->transactionBuilder->method('setOrder')->will($this->returnSelf());
        $this->transactionBuilder->method('setTransactionId')->will($this->returnSelf());
        $this->transactionBuilder->method('setAdditionalInformation')->will($this->returnSelf());
        $this->transactionBuilder->method('setFailSafe')->will($this->returnSelf());
        $this->transactionBuilder->method('build')->will($this->returnSelf());
        $this->testObject = $this->objectManager->getObject(
            PaymentInquiry::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
    }
        
    /**
     * testProcessPaymentInquiry
     *
     * @return void
     */
    public function testProcessPaymentInquiry()
    {
        $this->order->method('getIncrementId')->willReturn("00001");
        $this->payment->method('getArbTrackId')->willReturn("00001");
        $this->payment->method('getAmountOrdered')->willReturn("2000");
        $this->payment->method('getParentId')->willReturn("10");
        $this->order->method('getStatus')->willReturn("complete");
        $this->order->method('canInvoice')->willReturn(1);
        $this->order->method('load')->willReturnSelf();
        $this->voucher->method('assignVouchers')->willReturn(true);
        $this->esbNotification->method('prepareOrderData')->willReturn(true);
        $this->invoiceService->method("prepareInvoice")->willReturn($this->invoiceServiceInterface);
        $this->invoiceServiceInterface->method("setRequestedCaptureCase")->willReturnSelf();
        $this->invoiceServiceInterface->method("register")->willReturnSelf();
        $this->transactionFactory->method("create")->willReturnSelf();
        $this->transactionFactory->method("addObject")->willReturnSelf();
        $this->transactionFactory->method("save")->willReturnSelf();
        $curlResponse= [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata" ,
                        "Error" => "ErrorText",
                    ];
        $curlResponse[0] = [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                    ];        
        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] =["result"=>"success"]; 
        $this->json->expects($this->at(1))->method('unserialize')->willReturn($decodedTransData);
        $data = array (
            0 => 
            array (
            'date' => 419,
            'authRespCode' => '00',
            'authCode' => '000000',
            'trackId' => '000000107',
            'transId' => 202011039594173,
            'udf5' => 'TrackID',
            'amt' => '76.19',
            'udf3' => 'null',
            'udf4' => 'null',
            'udf1' => 'null',
            'udf2' => 'null',
            'result' => 'CAPTURED',
            'ref' => '011022000000',
            'paymentId' => -1,
            ),
        ); 
        $this->json->expects($this->at(2))->method('unserialize')->willReturn($data);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->encryptionModel->method('decryptAES')->willReturn("decryptAES");
        $result = $this->testObject->processPaymentInquiry("PY20-2797");
    }    
    /**
     * testProcessPaymentInquiryCancel
     *
     * @return void
     */
    public function testProcessPaymentInquiryCancel()
    {
        $this->order->method('getIncrementId')->willReturn("00001");
        $this->payment->method('getArbTrackId')->willReturn("00001");
        $this->payment->method('getAmountOrdered')->willReturn("2000");
        $this->payment->method('getParentId')->willReturn("10");        
        $curlResponse= [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata" ,
                        "Error" => "ErrorText",
                    ];
        $curlResponse[0] = [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                    ];        
        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] =["result"=>"success"]; 
        $this->json->expects($this->at(1))->method('unserialize')->willReturn($decodedTransData);
        $data[0]=["paymentId"=>"-1"]; 
        $this->json->expects($this->at(2))->method('unserialize')->willReturn($data);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->encryptionModel->method('decryptAES')->willReturn("decryptAES");
        $result = $this->testObject->processPaymentInquiry("PY20-2797");
    }
    
    /**
     * testProcessPaymentInquiryStatus
     *
     * @return void
     */
    public function testProcessPaymentInquiryStatus()
    {
        $this->order->method('getIncrementId')->willReturn("00001");
        $this->payment->method('getArbTrackId')->willReturn("00001");
        $this->payment->method('getAmountOrdered')->willReturn("2000");
        $this->payment->method('getParentId')->willReturn("10");        
        $curlResponse= [
                        "paymentId" => 100201934525118923,
                        "status" => "2" ,
                        "Error" => "ErrorText",
                    ];
        $curlResponse[0] = [
                        "paymentId" => 100201934525118923,
                        "status" => "2",
                        "Error" => "ErrorText",
                    ];        
        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));
        $decodedTransData[0] =["error"=>"error"]; 
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->encryptionModel->method('decryptAES')->willReturn("decryptAES");
        $result = $this->testObject->processPaymentInquiry("PY20-2797");
    }
   
    /**
     * testProcessPaymentInquiryException
     *
     * @return void
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testProcessPaymentInquiryException()
    {
        $this->order->method('getIncrementId')->willReturn("00001");
        $this->payment->method('getArbTrackId')->willReturn("00001");
        $this->payment->method('getAmountOrdered')->willReturn("2000");
        $this->payment->method('getParentId')->willReturn("10");        
        $curlResponse= [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata" ,
                        "Error" => "ErrorText",
                    ];
        $curlResponse[0] = [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                    ];        
        $this->curl->method('getBody')->willThrowException(new \Exception("invalida"));       
        $result = $this->testObject->processPaymentInquiry("PY20-2797");
    }
    public function testProcessPaymentInquiryResponse()
    {
        $this->order->method('getIncrementId')->willReturn("00001");
        $this->payment->method('getArbTrackId')->willReturn("00001");
        $this->payment->method('getAmountOrdered')->willReturn("2000");
        $this->payment->method('getParentId')->willReturn("10");
        $this->order->method('getStatus')->willReturn("complete");
        $this->order->method('canInvoice')->willReturn(1);
        $this->order->method('load')->willReturnSelf();
        $this->voucher->method('assignVouchers')->willReturn(true);
        $this->esbNotification->method('prepareOrderData')->willReturn(true);
        $this->invoiceService->method("prepareInvoice")->willReturn($this->invoiceServiceInterface);
        $this->invoiceServiceInterface->method("setRequestedCaptureCase")->willReturnSelf();
        $this->invoiceServiceInterface->method("register")->willReturnSelf();
        $this->transactionFactory->method("create")->willReturnSelf();
        $this->transactionFactory->method("addObject")->willReturnSelf();
        $this->transactionFactory->method("save")->willReturnSelf();
        $curlResponse= [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata" ,
                        "Error" => "ErrorText",
                    ];
        $curlResponse[0] = [
                        "paymentId" => 100201934525118923,
                        "trandata" => "trandata",
                        "Error" => "ErrorText",
                    ];        
        $this->curl->method('getBody')->willReturn(json_encode($curlResponse));
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($curlResponse);
        $decodedTransData[0] =["result"=>"success"]; 
        $this->json->expects($this->at(1))->method('unserialize')->willReturn($decodedTransData);
        $data = array (
            0 => 
            array (
            'date' => 419,
            'authRespCode' => '00',
            'authCode' => '000000',
            'trackId' => '000000107',
            'transId' => 202011039594173,
            'udf5' => 'TrackID',
            'amt' => '76.19',
            'udf3' => 'null',
            'udf4' => 'null',
            'udf1' => 'null',
            'udf2' => 'null',
            'result' => 'CAPTURED',
            'ref' => '011022000000',
            'paymentId' => -1,
            ),
        ); 
        $this->json->expects($this->at(2))->method('unserialize')->willReturn($data);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->encryptionModel->method('decryptAES')->willReturn("decryptAES");
        $result = $this->testObject->processPaymentInquiry("PY20-2797");
    }
     /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     *
     */
    public function testGetArbTrackId()
    {        
        $result = $this->testObject->processPaymentInquiry("PY20-2797");
    }    
    /**
     * testCreateInvoice
     *
     * @return void
     */
    public function testCreateInvoice()
    {   
        /*$orderid=123;
        $this->order->method('load')->willReturnSelf();
        $this->invoiceService->method("prepareInvoice")->willReturn($this->invoiceServiceInterface);
        $this->invoiceServiceInterface->method("setRequestedCaptureCase")->willReturnSelf();
        $this->invoiceServiceInterface->method("register")->willReturnSelf();
        $this->transactionFactory->method("create")->willReturnSelf();
        $this->transactionFactory->method("addObject")->willReturnSelf();
        $result = $this->testObject->createInvoice($orderid);
        $this->assertEmpty($result); */
    }    
    /**
     * testCreateOrderTrancationEmpty
     *
     * @return void
     */
    public function testCreateOrderTrancationEmpty()
    { 
      $this->payment->method('setTransactionId')->will($this->returnSelf());
        $this->payment->method('setParentTransactionId')->will($this->returnSelf());
        $this->payment->method('save')->will($this->returnSelf()); 
      $data['transId']= '';
      $this->order->method('getIncrementId')->willReturn("00001");
        $result = $this->testObject->createOrderTrancation($this->payment,$this->order,$data);
        $this->assertEmpty($result);
    }    
    /**
     * testCreateOrderTrancation
     *
     * @return void
     */
    public function testCreateOrderTrancation()
    {  
        $this->payment->method('setTransactionId')->will($this->returnSelf());
        $this->payment->method('setParentTransactionId')->will($this->returnSelf());
        $this->payment->method('save')->will($this->returnSelf());
        $data['transId']= 123;
        $result = $this->testObject->createOrderTrancation($this->payment,$this->order,$data);
        $this->assertEmpty($result);
    }    
    /**
     * testprocessInquiryResponse
     *
     * @return void
     */
    public function testprocessInquiryResponse()
    {  
        $responseData[0]['status']=1;
        $responseData[0]['trandata']=1;
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($responseData);
        $this->objectManager = new ObjectManager($this);
        $testObject = $this->objectManager->getObject(
            ExampleExposed::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentInquiry');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, ['resource_key'=>'abc']);
        $result = $testObject->processInquiryResponse('abc',null,null, null);
        // var_dump($result);die;
        $this->assertTrue($result['status']);
    }    
    /**
     * testprocessInquiryResponseCron
     *
     * @return void
     */
    public function testprocessInquiryResponseCron()
    {  
        $responseData[0]['status']=1;
        $responseData[0]['trandata']=1;
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($responseData);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->objectManager = new ObjectManager($this);
        $testObject = $this->objectManager->getObject(
            ExampleExposed::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
       
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentInquiry');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, ['resource_key'=>'abc']);
        $result = $testObject->processInquiryResponse('abc',null,null, 1);
        $this->assertTrue($result['status']);
    }    
    /**
     * testprocessInquiryResponseTrandata
     *
     * @return void
     */
    public function testprocessInquiryResponseTrandata()
    {  
        $responseData[0]['status']=1;
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($responseData);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->objectManager = new ObjectManager($this);
        $testObject = $this->objectManager->getObject(
            ExampleExposed::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
       
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentInquiry');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, ['resource_key'=>'abc']);
        $result = $testObject->processInquiryResponse('abc',null,null, 1);
        $this->assertEquals('Cancelled',$result['orderStatus']);
        $this->assertEmpty($result['orderId']);
        $this->assertEquals('error',$result['result']);
    }    
    /**
     * testprocessInquiryResponseStatus
     *
     * @return void
     */
    public function testprocessInquiryResponseStatus()
    {  
        $responseData[0]['status']=2;
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($responseData);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("save")->willReturnSelf();
        $this->order->method('cancel')->willReturnSelf();
        $this->voucher->method('unBlockVouchers')->willReturnSelf();
        $this->objectManager = new ObjectManager($this);
        $testObject = $this->objectManager->getObject(
            ExampleExposed::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
       
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentInquiry');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, ['resource_key'=>'abc']);
        $result = $testObject->processInquiryResponse('abc',null,null, 1);
        $this->assertEquals('Cancelled',$result['orderStatus']);
        $this->assertEmpty($result['orderId']);
        $this->assertEquals('error',$result['result']);
    }    
    /**
     * testprocessInquiryResponseTrans
     *
     * @return void
     */
    public function testprocessInquiryResponseTrans()
    {  
        $responseData[0]['trandata']=1;
        $responseData[0]['status']=2;
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($responseData);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("save")->willReturnSelf();
        $this->order->method('cancel')->willReturnSelf();
        $this->voucher->method('unBlockVouchers')->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->payment->method('setData')->will($this->returnSelf());
        $this->objectManager = new ObjectManager($this);
        $testObject = $this->objectManager->getObject(
            ExampleExposed::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
       
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentInquiry');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, ['resource_key'=>'abc']);
        $result = $testObject->processInquiryResponse('abc',null,null, 1);
        // var_dump($result);die;
        $this->assertEquals('Cancelled',$result['orderStatus']);
        $this->assertEmpty($result['orderId']);
        $this->assertEquals('error',$result['result']);
    }    
    /**
     * testprocessInquiryResponseTransCron
     *
     * @return void
     */
    public function testprocessInquiryResponseTransCron()
    {  
        $responseData[0]['trandata']=1;
        $responseData[0]['status']=2;
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($responseData);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("save")->willReturnSelf();
        $this->order->method('cancel')->willReturnSelf();
        $this->voucher->method('unBlockVouchers')->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->payment->method('setData')->will($this->returnSelf());
        $this->objectManager = new ObjectManager($this);
        $testObject = $this->objectManager->getObject(
            ExampleExposed::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
       
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentInquiry');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, ['resource_key'=>'abc']);
        $result = $testObject->processInquiryResponse('abc',null,null, null);
        $this->assertEquals('Cancelled',$result['orderStatus']);
        $this->assertEmpty($result['orderId']);
        $this->assertEquals('error',$result['result']);
    }
    public function testprocessInquiryResponseVouchers()
    {  
        $responseData[0]['trandata']=true;
        $responseData[0]['status']=2;
        $this->json->expects($this->at(0))->method('unserialize')->willReturn($responseData);
        $this->order->method("loadByIncrementId")->willReturnSelf();
        $this->order->method("save")->willReturnSelf();
        $this->order->method('cancel')->willReturnSelf();
        $this->voucher->method('unBlockVouchers')->willReturnSelf();
        $this->order->method("getPayment")->willReturn($this->payment);
        $this->payment->method('load')->will($this->returnSelf());
        $this->payment->method('setData')->will($this->returnSelf());
        $this->order->method('getIncrementId')->willReturn("00001");
        $this->voucher->method('assignVouchers')->willReturn(true);
        $this->payment->method('getOrder')->willReturn($this->order);
        $this->objectManager = new ObjectManager($this);
        $testObject = $this->objectManager->getObject(
            ExampleExposed::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'payment' => $this->payment,
                'encryptor' => $this->encryptionModel,
                'curl' => $this->curl,
                'json' => $this->json,
                'paymentLog' => $this->paymentLog,
                "orderObject"=>$this->order,
                "vouchers"=>$this->voucher,
                "esbNotification"=>$this->esbNotification,
                "invoiceService"=>$this->invoiceService,
                "transactionFactory"=>$this->transactionFactory,
                "transactionBuilder"=>$this->transactionBuilder
            ]
        );
       
        $reflectionClass = new \ReflectionClass('Arb\ArbPayment\Model\PaymentInquiry');
        $reflectionProperty = $reflectionClass->getProperty('paymentConfig');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($testObject, ['resource_key'=>'abc']);
        $result = $testObject->processInquiryResponse('abc',null,null, 1);
        $this->assertEquals('Cancelled',$result['orderStatus']);
        $this->assertEquals('00001',$result['orderId']);
        $this->assertEquals('error',$result['result']);
    }
}
/**
 * ExampleExposed
 */
class ExampleExposed extends PaymentInquiry
{
    public function __call($method, array $args = array())
    {
        if (!method_exists($this, $method))
            throw new BadMethodCallException("method '$method' does not exist");
        return call_user_func_array(array($this, $method), $args);
    }
}
