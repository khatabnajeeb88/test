<?php

namespace Arb\ArbPayment\Api;

interface PaymentInquiryInterface
{
    /**
     * Returns payment inquiry details
     *
     * @api
     * @param string $paymentId
     * @return array
     */
    public function processPaymentInquiry($paymentId);
}
