<?php

namespace Arb\ArbPayment\Api;

interface PaymentPurchaseInterface
{
    /**
     * Returns purchase API transaction
     *
     * @api
     * @param string $orderId
     * @param string $trandata
     * @param string $responseURL
     * @param string $errorURL
     * @return array
     */
    public function processPaymentPurchase($orderId, $trandata, $responseURL, $errorURL);
}
