<?php
/**
 * Payment gateway code configuration
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */

namespace Arb\ArbPayment\Model\Gateway;

use Arb\ArbPayment\Model\Method;

/**
 * ArbPaymentGateway class for payment method configuration
 */
// Magento extended property requires underscore property
// phpcs:disable
class ArbPaymentGateway extends Method
{
    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;
    
    /**
     * @var string
     */
    public $_code = 'arbpayment_gateway';

    /**
     * @var string
     */
    public $_gateWayCode = 'arbpayment_gateway';
}
