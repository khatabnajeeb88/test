<?php
/**
 * Payment gateway transaction purchase API
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Arb\ArbPayment\Model\PaymentLog;
use Arb\ArbPayment\Model\PaymentInquiry;

/**
 * PaymentPurchase class to perform purchase call
 */
class PaymentPurchase implements \Arb\ArbPayment\Api\PaymentPurchaseInterface
{
    /**
     *  Purchase Payment Action
     */
    const PAYMENT_ACTION = '1';
    
    /**
     * Intial inquiry count to be saved in log table
     */
    const INQUIRY_COUNT = 1;

    /**
     * Before purchase API call order status
     */
    const BEFORE_PAYMENT_ORDER_STATUS = "pending_payment";

    /**
     * Pending order status value
     */
    const PENDING_ORDER_STATUS = "pending";

    /**
     * Payment configuration
     *
     * @var array
     */
    protected $paymentConfig = [];

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curlClient;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $order;
 
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     *
     * @var \Arb\ArbPayment\Model\PaymentLog
     */
    protected $paymentLog;

    /**
     * @var \Arb\ArbPayment\Model\ResponseChecker
     */
    protected $responseCheck;
    
    /**
     * @param \Arb\ArbPayment\Model\PaymentInquiry
     */
    private $paymentInquiry;

    /**
     * @var int
     */
    private $maxRequestCount;

    /**
     * Payment Purchase class constructor
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     * @param \Arb\ArbPayment\Model\ResponseChecker $responseChecker
     * @param PaymentLog $paymentLog
     * @param PaymentInquiry $paymentInquiry
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Arb\ArbPayment\Model\ResponseChecker $responseChecker,
        PaymentLog $paymentLog,
        PaymentInquiry $paymentInquiry
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->order = $order;
        $this->curlClient = $curl;
        $this->json = $json;
        $this->paymentLog = $paymentLog;
        $this->paymentInquiry = $paymentInquiry;
        $this->responseCheck = $responseChecker;
        $this->maxRequestCount = 3 ;
        $this->getPaymentConfiguration();
    }

    /**
     * Returns purchase API transaction
     *
     * @param int $orderId
     * @param string $trandata
     * @param string $responseURL
     * @param string $errorURL
     * @return array
     */
    public function processPaymentPurchase($orderId, $trandata, $responseURL, $errorURL)
    {
        $this->paymentLog->logPaymentException($orderId . "- purchase request initiated.");
        if ($this->isValidOrderId($orderId)) {
            $apiInputData[] = [
                'trandata' => $trandata,
                'id' => $this->paymentConfig['merchant_id'],
                'responseURL' => $responseURL,
                'errorURL' => $errorURL
            ];
            $data = $this->json->serialize($apiInputData);
            $result = $this->processPurchaseRequest($orderId, $data);
            $this->paymentLog->logPaymentException('processPaymentPurchase, 
                payment response to mobile for valid Order ID');
            $this->paymentLog->logPaymentException($result);
            return $result;
        } else {
                $returnData['orderId'] = $orderId;
                $returnData['3ds'] = false;
                $returnData['url'] = "";
                $returnData['status'] = false;
                $this->paymentLog->logPaymentException($orderId . ' - processPaymentPurchase, payment response send to mobile.');
                $this->paymentLog->logPaymentException($returnData);
                return [$returnData];
        }
    }

    /**
     * Checks if order id exist
     *
     * @param  int $orderId
     * @return boolean
     * @throws LocalizedException
     */
    public function isValidOrderId($orderId)
    {
        try {
            $order = $this->order->loadByIncrementId($orderId);
            if ($order->getId() && ($order->getStatus() == self::PENDING_ORDER_STATUS)) {
                $order->setState(self::BEFORE_PAYMENT_ORDER_STATUS)->setStatus(self::BEFORE_PAYMENT_ORDER_STATUS);
                $order->save();
                $this->paymentLog->logPaymentException($orderId . " - Order is valid and order status set to Pending Payment.");
                return true;
            } else {
                $this->paymentLog->logPaymentException($orderId . " - Order id or status is not valid.");
                return false;
            }
        } catch (\Exception $e) {
            $this->paymentLog->logPaymentException($orderId . ' - isValidOrderId : '.$e->getMessage());
            throw new LocalizedException(
                __("Invalid Order Id")
            );
        }
    }

      /**
       * Process purchase API
       *
       * @param  int $orderId
       * @param  string $apiData
       * @return array
       */
    private function processPurchaseRequest($orderId, $apiData)
    {
       //logging request and response data in table
        $logData = [
          'order_id' => $orderId,
          'api_type' => PaymentLog::PURCHASE,
          'inquiry_count' => self::INQUIRY_COUNT,
          'request' => null,
          'response' => null
        ];
        $response = null;
        $this->paymentLog->logPaymentException($orderId . " - Purchase Request");
        $logTableId = $this->paymentLog->setPaymentLog($logData);
        try {
            $this->curlClient->addHeader("Content-Type", "application/json");
            $this->curlClient->setOption(CURLOPT_SSL_VERIFYHOST, 0);
            $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, 0);
            $this->curlClient->setOption(CURLOPT_RETURNTRANSFER, 1);
            $this->curlClient->setOption(CURLOPT_FOLLOWLOCATION, 1);
            $this->curlClient->setOption(CURLOPT_TIMEOUT, $this->paymentConfig['timeout']);
            $this->curlClient->post($this->paymentConfig['purchase_url'], $apiData);
            $response = $this->curlClient->getBody();
            
            $this->paymentLog->logPaymentException($orderId . " - Purchase Response encrypted");
            $this->paymentLog->updatePaymentLog($logTableId, $this->json->serialize([$response]));
            $responseData =  $this->validateResponse($response, $orderId);

            $returnData['orderId'] = $orderId;
            $returnData['3ds'] = isset($responseData['3ds']) ? $responseData['3ds']  : false;
            $returnData['url'] = isset($responseData['3dsUrl']) ? $responseData['3dsUrl'] : "";
            $returnData['status'] = isset($responseData['status']) ? $responseData['status'] : false;
            return [$returnData];
        } catch (\Exception $e) {
            $this->paymentLog->logPaymentException($orderId . " - Exception ". $e->getMessage().' : '.$e->getTraceAsString());
            $logData['response'] = $e->getMessage().' : '.$e->getTraceAsString();
            $this->paymentLog->updatePaymentLog($logTableId, $this->json->serialize($logData));
            throw new LocalizedException(
                __("Payment Gateway Error")
            );
        }
    }
    
    /**
     * Returns payment configuration
     *
     * @return array
     */
    private function getPaymentConfiguration()
    {
        $this->paymentConfig['iv_config'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/vector_init',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_id'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_id',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_password'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_password',
            ScopeInterface::SCOPE_STORE
        );
        ;
        $this->paymentConfig['resource_key'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_terminal_resourcekey',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['currency_code'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_currency_code',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['inquiry_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_inquiry_url',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['purchase_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_purchase_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['timeout'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_timeout',
            ScopeInterface::SCOPE_STORE
        );
        
        return $this->paymentConfig;
    }

    /**
     * Return response validation
     *
     * @param  string $response
     * @param  string $orderId
     * @return array
     */
    protected function validateResponse($response, $orderId)
    {
        $this->paymentLog->logPaymentException($orderId . " - Purchase Response plain text");
        $responseData =  $this->responseCheck->getTransactionDetails($response, $this->paymentConfig['resource_key'], $orderId);
        $this->paymentLog->logPaymentException($responseData);
        $this->setPaymentDetails($orderId, $responseData['paymentId'], $responseData['ref']);

        //Intiate inquiry if response data is null or empty
        if (!$responseData['isValid'] && !$responseData['3ds']) {
            $this->paymentLog->logPaymentException($orderId." - Inquiry for non-3d payment for no/invalid response from Purchase.");
            $result = $this->getInquiryResponse($responseData, $orderId);
            return $result;
        }

        return $responseData;
    }

    /**
     * Inquiry initiate if response is empty for purchase API
     *
     * @param array $responseData
     * @param int $orderId
     * @return void
     */
    protected function getInquiryResponse($responseData, $orderId)
    {
        $responseData = $this->paymentInquiry->getPaymentDataByField(PaymentInquiry::TRACKING_ID, $orderId);
        for ($i = self::INQUIRY_COUNT; $i <= $this->maxRequestCount; $i++) {
            $response = $this->paymentInquiry->getInquiryData($responseData, $i);
            if (isset($response['orderStatus'])) {
                break;
            } else {
                $this->paymentLog->logPaymentException($orderId." - Payment Inquiry is not successfull in attempt".$i);
            }
        }
        return [$response];
    }

    /**
     * Sets payment details
     *
     * @param  string $orderId
     * @param  string $paymentId
     * @return string $orderStatus
     */
    protected function setPaymentDetails($orderId, $paymentId, $refId = null)
    {
        try {
            $order = $this->order->loadByIncrementId($orderId);
            if ($order->getId()) {
                $payment = $order->getPayment();
                $payment->setArbPaymentId($paymentId);
                $payment->setArbTrackId($orderId);
                if ($refId) {
                    $payment->setArbRefId($refId);
                }
                $payment->save();
                $order->save();
            }
        } catch (\Exception $e) {
            $this->paymentLog->logPaymentException('setPaymentDetails : '.$e->getMessage());
            throw new LocalizedException(
                __("Unable to save payment data")
            );
        }
        return $order->getStatus();
    }
}
