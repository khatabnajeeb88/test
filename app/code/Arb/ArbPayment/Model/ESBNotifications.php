<?php
/**
 * Model class file for sending SMS and emails
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */

namespace Arb\ArbPayment\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order\ItemFactory;
use Arb\EsbNotifications\Helper\Data as EsbHelper;
use Webkul\MpAdvancedCommission\Helper\Data as MpHelper;
use Webkul\Marketplace\Helper\Data as WebkulHelper;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Catalog\Model\Product\Type as ProductType;

/**
 * Arb Payment and ESB notification Orders integration class
 */
class ESBNotifications extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @param \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @param \Magento\Sales\Model\Order\ItemFactory
     */
    protected $itemFactory;

    /**
     * @param \EsbHelper
     */
    protected $esbHelper;

    /**
     * @param \MpHelper
     */
    protected $mpHelper;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var \Zend\Log\Writer\Stream
     */
    protected $_writer;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     *
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var PriceHelper
     */
    protected $priceHelper;

    /*
    * Default Not available
    */
    const NOT_AVAILABLE = "NA";

    /*
    * Dummy LastName
    */
    const LASTNAME = "arb";

    /**
     * Saudi ISD code
     */
    const SAUDI_ISD_CDOE = '+966';

    /**
     * Voucher order constructor function
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param OrderFactory $orderFactory
     * @param ItemFactory $itemFactory
     * @param EsbHelper $esbHelper
     * @param MpHelper $mpHelper
     * @param WebkulHelper $wkHelper
     * @param Json $serializer
     * @param EncryptorInterface $encryptor
     * @param CustomerRepositoryInterface $_customerRepository
     * @param PriceHelper $priceHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        OrderFactory $orderFactory,
        ItemFactory $itemFactory,
        EsbHelper $esbHelper,
        MpHelper $mpHelper,
        WebkulHelper $wkHelper,
        Json $serializer,
        EncryptorInterface $encryptor,
        CustomerRepositoryInterface $_customerRepository,
        PriceHelper $priceHelper,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->orderFactory = $orderFactory;
        $this->itemFactory = $itemFactory;
        $this->esbHelper = $esbHelper;
        $this->mpHelper = $mpHelper;
        $this->wkHelper = $wkHelper;
        $this->serializer = $serializer;
        $this->encryptor = $encryptor;
        $this->_customerRepository = $_customerRepository;
        $this->priceHelper = $priceHelper;
    }

    /**
     * Returns log object
     *
     * @return \Zend\Log\Logger
     */
    protected function getLoggerFunction()
    {
        try {
            $this->_writer = new \Zend\Log\Writer\Stream(BP . '/var/log/ESB_Log-'.date("Ymd").'.log');
            $this->_logger = new \Zend\Log\Logger();
            return $this->_logger->addWriter($this->_writer);
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Error in log file creation')
            );
        }
    }

    /**
     * Prepares order data for sending email/SMS
     *
     * @param  int $orderId
     * @return array
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function prepareOrderData($orderId)
    {
        $return = [
            'sms' => false,
            'email' => false
        ];
        $order = $this->orderFactory->create()->load($orderId, 'increment_id');
        //Logging data in file
        $logger = $this->getLoggerFunction();
        if ($order->getId()) {
            $payment = $order->getPayment()->getData();
            $orderData['email'] = $order->getCustomerEmail();

            $customerFirstName = !empty($order->getCustomerFirstname()) ? $order->getCustomerFirstname() : "";
            $customerLastName = !empty($order->getCustomerLastname()) ? $order->getCustomerLastname() : "";
            //Removed ARB as last name when last name is empty
            if (strtolower($customerLastName) == self::LASTNAME) {
                $customerLastName = "";
            }
            $customerName = $customerFirstName." ".$customerLastName;
            $orderData['customer_name'] = $customerName;
            
            $orderData['order_number'] = $order->getIncrementId();
            $storeCode = $order->getStore()->getCode();
            $orderData['store'] = $this->getStoreCode($storeCode);

            $orderData['total_amount'] = $this->priceHelper->currency($order->getGrandTotal(), true, false);
            //$orderData['ref_no'] = isset($payment['arb_ref_id']) ? $payment['arb_ref_id'] : 0;
            $orderData['tran_id'] = isset($payment['arb_payment_id']) ? $payment['arb_payment_id'] : 0;
            $orderData['payment_id'] = isset($payment['arb_ref_id']) ? $payment['arb_ref_id'] : 0;

            $billingAddress = $order->getBillingAddress();
            $orderData['phonenumber']=!empty($billingAddress->getTelephone()) ? self::SAUDI_ISD_CDOE.$billingAddress->getTelephone():'';

            foreach ($order->getAllItems() as $item) {
                $itemData = $item->getData();
                $vouchers = isset($itemData['vouchers']) ? $this->serializer->unserialize($itemData['vouchers']) : [];

                if (empty($vouchers) && $item->getProductType() === ProductType::TYPE_VIRTUAL) {
                    $logger->info("processInquiryResponse voucher are not assigned to order");
                    throw new NoSuchEntityException(__('Vouchers are not assigned to order.'));
                }

                $decryptedVouchers = [];
                foreach ($vouchers as $key => $voucher) {
                    $decryptedVouchers[$key] =  $this->encryptor->decrypt($voucher);
                }

                $voucherAmount = isset($itemData['price_incl_tax']) ? $itemData['price_incl_tax'] : 0;
                $productName = isset($itemData['name']) ? $itemData['name'] : '';
                $orderData['voucher_amount'] = $this->priceHelper->currency($voucherAmount, true, false);
                $orderData['product_name'] = $productName;
                $sellerId = $this->mpHelper->getSellerIdByItem($item);
                $orderData["seller_phone_no"] = self::NOT_AVAILABLE;
                $orderData["seller_name"] = self::NOT_AVAILABLE;
                if (!empty($sellerId)) {
                    //get Seller Phone Number
                    $seller = $this->_customerRepository->getById($sellerId);
                    $sellerPhone = (null != $seller->getCustomAttribute("wkv_customer_care_number"))
                    ?$seller->getCustomAttribute("wkv_customer_care_number")->getValue():'';
                    $orderData["seller_phone_no"] = $sellerPhone;
                    //Get Seller Shop Title
                    $sellerData = $this->wkHelper->getSellerDataBySellerId($sellerId)->getData();
                    $shopTitle=!empty($sellerData[0]["shop_title"])?$sellerData[0]["shop_title"]:"";
                    $orderData["seller_name"] = $shopTitle;
                }
                try {
                    foreach ($decryptedVouchers as $key => $vocuher) {
                        $orderData["voucher_name"] =   $vocuher;
                        $orderData["ref_no"] =   $key;
                        $emailResult = $this->esbHelper->sendEmailNotification($orderData);
                        $emailResultData = json_decode($emailResult, true);
                        if ($emailResultData['success']) {
                            $return['email'][$item->getId()][] = true;
                        }

                        $smsResult = $this->esbHelper->sendSmsNotification($orderData);
                        $smsResultData = json_decode($smsResult, true);
                        if ($smsResultData['success']) {
                            $return['sms'][$item->getId()][] = true;
                        }
                    }
                } catch (\Exception $e) {
                    $return = ["error_message"=>__($e->getMessage())];
                }
            }
            $logger->info('prepareOrderData : '.$orderId.' --- '.json_encode($return, true));
            return $return;
        } else {
            $logger->info('prepareOrderData : '.$orderId.' --- Requested order doesn\'t exist ');
            throw new NoSuchEntityException(__('Requested order doesn\'t exist'));
        }
    }

    /**
     * Get Store code
     *
     * @param string $code
     * @return string
     */
    private function getStoreCode($code)
    {
        switch ($code) {
            case 'ar_SA':
                $storeCode = "AR";
                break;
            default:
                $storeCode = "EN";
                break;
        }

        return $storeCode;
    }
}
