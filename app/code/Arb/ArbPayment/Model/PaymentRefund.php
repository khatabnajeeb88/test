<?php
/**
 * Payment gateway refund and void API call
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model;

use Arb\ArbPayment\Helper\Data;
use Arb\ArbPayment\Model\PaymentLog;

/**
 * PaymentInquiry class to perform inquiry
 */
class PaymentRefund
{
    /**
     *  Refund inquiry Payment Action code
     */
    const REFUND_INQUIRY_ACTION = '8';

    /**
     * Payment Gateway UDF track parameter
     */
    const UDF_TRACK_ID = 'TrackID';

    /**
     * Payment gateway failure response code
     */
    const PAYMNET_FAILURE = '2';

    /**
     * Payment gateway success response code
     */
    const PAYMENT_SUCCESS = '1';
    
    /**
     * Payment configuration
     *
     * @var array
     */
    protected $paymentConfig = [];

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curlClient;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * @var \Arb\ArbPayment\Model\PaymentLog
     */
    protected $paymentLog;

    /**
     * @var \Arb\ArbPayment\Model\Encryption
     */
    private $encryptor;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     * variable helperData
     *
     * @var Arb\ArbPayment\Helper\Data
     */
    protected $helperData;

    /**
     * Payment refund constructor
     *
     * @param PaymentLog $paymentLog
     * @param \Arb\ArbPayment\Model\Encryption $encryptor
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Arb\ArbPayment\Helper\Data $helperData
     */
    public function __construct(
        PaymentLog $paymentLog,
        \Arb\ArbPayment\Model\Encryption $encryptor,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Data $helperData
    ) {
        $this->curlClient = $curl;
        $this->json = $json;
        $this->paymentLog = $paymentLog;
        $this->encryptor = $encryptor;
        $this->scopeConfig = $scopeConfig;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Refund_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->helperData=$helperData;
    }

    /**
     * Returns Payment Gateway Request Data
     *
     * @param  array $refundData
     * @param string $action (either refund or void)
     * @return array
     */
    public function getRefundAPIData($refundData, $action)
    {
        $uniqueTrackId = $this->getUniqueTrackId();
        $paymentConfig = $this->getPaymentConfiguration();
        $paymentData[] = [
            'amt' => $refundData['amt'],
            'action' => $action,
            'password' => $paymentConfig['merchant_password'],
            'id' => $paymentConfig['merchant_id'],
            'currencyCode' => $paymentConfig['currency_code'],
            'trackId' => $uniqueTrackId,
            'udf5' => self::UDF_TRACK_ID,
            'transId' => $refundData['transId']
            ];
        $this->paymentLog->setPaymentLog('Payment gateway return data set successfully for transaction Id'. $refundData['transId']);
        return $paymentData;
    }

    /**
     * Returns Payment Gateway Refund/Refund inquiry API result
     *
     * @param  array $refundData
     * @param string $type (API type Refund/Void/Inquiry)
     * @return array
     */
    public function refundAPIRequest($refundData, $type)
    {
        $paymentConfig = $this->getPaymentConfiguration();
        
        if ($type === PaymentLog::REFUND_INQUIRY) {
            $url = $paymentConfig['inquiry_url'];
        } else {
            $url = $paymentConfig['refund_url'];
        }
        $this->paymentLog->setPaymentLog('Start Refund process for '.$refundData[0]['transId']);
        $responseUrl = $paymentConfig['response_url'];
        $errorUrl = $paymentConfig['error_url'];
        $jsonRefundData = $this->json->serialize($refundData);
        $urlEncodeData = urlencode($jsonRefundData);
        $transactionData = $this->encryptor->encryptAES($urlEncodeData, $paymentConfig['resource_key']);

        $data[]=[
            'id' => $paymentConfig['merchant_id'],
            'trandata' => $transactionData,
            'responseURL' => $responseUrl,
            'errorURL' => $errorUrl
        ];

        $encodeDetails = json_encode($data, JSON_UNESCAPED_SLASHES);
        $this->paymentLog->setPaymentLog('Refund Request data for Order ID '.$refundData[0]['transId'].'is '.$encodeDetails);
        try {
            /**
            * We don't save plain merchant id and password in table,
            * therefore we mask before saving in DB.
            */

            $logRequest = $refundData[0];
            $logRequest['id'] = '*****';
            $logRequest['password'] = '*****';
            $data = [
                'order_id' => $logRequest['transId'],
                'api_type' => $type,
                'inquiry_count' => 0,
                'request' => $this->json->serialize($logRequest),
                'response' => null
            ];

            $this->curlClient->addHeader("Content-Type", "application/json");
            $this->curlClient->setOption(CURLOPT_SSL_VERIFYHOST, 0);
            $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, 0);
            $this->curlClient->setOption(CURLOPT_RETURNTRANSFER, 1);
            $this->curlClient->setOption(CURLOPT_FOLLOWLOCATION, 1);
            $this->curlClient->setOption(CURLOPT_TIMEOUT, $paymentConfig['timeout']);
            $this->curlClient->post($url, $encodeDetails);
            $response = $this->curlClient->getBody();
            $data['response'] = $this->json->serialize([$response]);
            $this->paymentLog->setPaymentLog('Refund response data for Order ID '.$data['order_id']. 'from payment gateway '.$data['response']);
            return $this->processRefundResponse($response,$data['order_id']);
        } catch (\Exception $e) {
            $data['response'] = $e->getMessage().' : '.$e->getTraceAsString();
            $this->paymentLog->setPaymentLog('There is no response from PG, Error code is '.$e->getCode().' and Error Message is '.$e->getMessage());
        }
        $this->paymentLog->setPaymentLog('End refund process for Order ID '.$refundData[0]['transId']);
    }

    /**
     * Validates refund API response
     *
     * @param  array $response
     * @return array
     */
    private function processRefundResponse($response,$orderId)
    {
        $return = [];
        if ($response) {
            $this->paymentLog->setPaymentLog('Start Refund Response process for order ID '.$orderId);
            $responseData = $this->json->unserialize($response);
            $paymentConfig = $this->getPaymentConfiguration();
            // check if response is valid or not
            if (isset($responseData[0]['status'])) {
                //If response is successful
                if ($responseData[0]['status'] === self::PAYMENT_SUCCESS) {
                    if (isset($responseData[0]['trandata'])) {
                        $encodedTransData = $responseData[0]['trandata'];
                        $decodedTransData= urldecode(
                            $this->encryptor->decryptAES(
                                $encodedTransData,
                                $paymentConfig['resource_key']
                            )
                        );
                        $transData = $this->json->unserialize($decodedTransData);
                        if (isset($transData[0]['ref'])) {
                            $return['status'] = true;
                            $return['message'] = __('Refund Process Completed');
                            $return['transData'] = $transData[0];
                            $this->paymentLog->setPaymentLog('Refund Process Completed for order ID '.$orderId);
                        } else {
                            $return['status'] = false;
                            $return['message'] = __('Refund Process Failed');
                            $this->paymentLog->setPaymentLog('Refund Process Failed for order ID '.$orderId);
                        }
                    }
                } else {
                    //If response is not successful
                    $return['status'] = false;
                    $return['message'] = isset($responseData[0]['errorText'])
                    ?__($responseData[0]['errorText']):__('Unable to process Refund for order ID '.$orderId);
                }
            } else {
                $return['status'] = false;
                $return['message'] = __('Invalid Refund Transaction Response');
                $this->paymentLog->setPaymentLog('Invalid Refund Transaction Response for order ID '.$orderId);
            }
        } else {
            $return['status'] = false;
            $return['message'] = __('Payment Gateway Error');
            $this->paymentLog->setPaymentLog('Payment Gateway Error for order ID '.$orderId);
        }
        return $return;
    }
  
    /**
     * getPaymentConfiguration
     * Returns payment configuration
     *
     * @return array
     */
    private function getPaymentConfiguration()
    { 
        return $this->helperData->getPaymentConfiguration('refund');
    }

    /**
     * Returns unique numeric track id for refund API
     *
     * @return string
     */
    protected function getUniqueTrackId()
    {
        $randomNumber =  random_int(100, 100000);
        //combining random number and time() stamp
        $uniqueTrackId = $randomNumber.time();
        return $uniqueTrackId;
    }

    /**
     * Returns Refund inquiry data
     *
     * @param array $refundData
     * @return array
     */
    public function processRefundInquiry($refundData)
    {
        if (isset($refundData[0]) && !empty($refundData[0])) {
            $inquiryData = $refundData[0];
            $paymentConfig = $this->getPaymentConfiguration();
            $paymentData[] = [
            'amt' => $inquiryData['amt'],
            'action' => self::REFUND_INQUIRY_ACTION,
            'password' => $paymentConfig['merchant_password'],
            'id' => $paymentConfig['merchant_id'],
            'currencyCode' => $paymentConfig['currency_code'],
            'trackId' => $inquiryData['trackId'],
            'udf5' => self::UDF_TRACK_ID,
            'transId' => $inquiryData['transId']
            ];
            $this->paymentLog->setPaymentLog('Payment data set successfully for tansaction ID'.$inquiryData['transId']);
        } else {
            $return['status'] = false;
            $return['message'] = __('Invalid Refund API data for inquiry');
            $this->paymentLog->setPaymentLog('Invalid Refund API data for inquiry');
            return $return;
        }
        return $paymentData;
    }
}
