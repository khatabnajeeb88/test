<?php
/**
 * Payment gateway encryption and decryption
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model;

/**
 * Payment gateway encryption and decryption class
 */
// PHPCS disabled as payment gateway provided functions
// Throwing warning about inbuilt PHP functions
// phpcs:disable
class Encryption
{
    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    private $cipher;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     * Encryption class constructor
     *
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->cipher = $encryptor;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Returns intialized vector value
     *
     * @param void
     * @return string
     */
    private function getVectorInit()
    {
        $ivConfig = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/vector_init',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $ivConfig;
    }

    /**
     * Returns resource key
     *
     * @param void
     * @return string
     */
    private function getResourceKey()
    {
        $resourceKey = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_terminal_resourcekey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $resourceKey;
    }

    /**
     * encrypts data
     *
     * @param string $str
     * @param string $key
     * @return string
     */
    public function encryptAES($str, $key = null, $iv= null, $from=null)
    {
        if (null == $key) {
            $key = $this->getResourceKey();
        }
        $str = $this->getPkcs5Pad($str);
		
		if(null == $from) {
			$ivlen = openssl_cipher_iv_length($cipher = "aes-256-cbc");
		} else {
			$ivlen = openssl_cipher_iv_length($cipher = "aes-128-cbc");
		}
		
		if (null == $iv) {
            $iv = $this->getVectorInit();
        }
		if(null == $from) {
			$encrypted = openssl_encrypt($str, "aes-256-cbc", $key, OPENSSL_ZERO_PADDING, $iv);
		} else {
			$encrypted = openssl_encrypt($str, "aes-128-cbc", $key, OPENSSL_ZERO_PADDING, $iv);
		}
        $encrypted = base64_decode($encrypted);
        $encrypted = unpack('C*', ($encrypted));
        $encrypted = $this->byteArray2Hex($encrypted);
        $encrypted = urlencode($encrypted);
        return $encrypted;
    }

    /**
     * Decrypts data
     *
     * @param string $code
     * @param string $key
     * @param string $iv
     * @return string
     */
    public function decryptAES($code, $key = null, $iv= null, $from=null)
    {
        if (null == $key) {
            $key = $this->getResourceKey();
        }
        $code = $this->hex2ByteArray(trim($code));
        $code = $this->byteArray2String($code);
        if (null == $iv) {
            $iv = $this->getVectorInit();
        }
        $code = base64_encode($code);
		if(null == $from) {
			$decrypted = openssl_decrypt($code, 'AES-256-CBC', $key, OPENSSL_ZERO_PADDING, $iv);
		} else {
			$decrypted = openssl_decrypt($code, 'AES-128-CBC', $key, OPENSSL_ZERO_PADDING, $iv);
		}
        return $this->getPkcs5Unpad($decrypted);
    }

    /**
     * Returns padding
     *
     * @param  string $text
     * @return void
     */
    public function getPkcs5Pad($text)
    {
        $blocksize = 16;
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    public function byteArray2Hex($byteArray)
    {
        $chars = array_map("chr", $byteArray);
        $bin = join($chars);
        return bin2hex($bin);
    }

    public function decryptData($code, $key)
    {
        if (null == $key) {
            $key = $this->getResourceKey();
        }
        $code = $this->hex2ByteArray(trim($code));
        $code = $this->byteArray2String($code);
        $iv = $this->getVectorInit();
        $code = base64_encode($code);
        $decrypted = openssl_decrypt($code, 'AES-256-CBC', $key, OPENSSL_ZERO_PADDING, $iv);
        return $this->getPkcs5Unpad($decrypted);
    }

    public function hex2ByteArray($hexString)
    {
        $string = hex2bin($hexString);
        return unpack('C*', $string);
    }

    public function byteArray2String($byteArray)
    {
        $chars = array_map("chr", $byteArray);
        return join($chars);
    }

    public function getPkcs5Unpad($text)
    {
        $pad = ord($text{strlen($text) - 1});
        if ($pad > strlen($text)) {
            return false;
        }
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) {
            return false;
        }
        return substr($text, 0, -1 * $pad);
    }
}
