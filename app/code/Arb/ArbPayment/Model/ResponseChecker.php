<?php
/**
 * Payment gateway purchase API response checker
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model;

use Arb\Order\Model\Order\Email\OrderEmailSender as PhysicalProductEmail;
/**
 * ResponseChecker class purchase API class
 */
class ResponseChecker
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * @param \Arb\ArbPayment\Model\Encryption
     */
    private $encryption;

    /**
     * @var PhysicalProductEmail
     */
    private $physicalProductEmail;

    /**
     * ResponseChecker class constructor
     *
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     */
    public function __construct(
        \Arb\ArbPayment\Model\Encryption $encryption,
        \Magento\Sales\Model\Order\Payment $payment,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Arb\Vouchers\Model\VoucherOrders $vouchers,
        \Magento\Sales\Model\Order $orderObject,
        \Arb\ArbPayment\Model\ESBNotifications $esbNotification,
        \Arb\ArbPayment\Model\PaymentInquiry $paymentInquiry,
        PhysicalProductEmail $physicalProductEmail,
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder
    ) {
        $this->encryption = $encryption;
        $this->json = $json;
        $this->payment = $payment;
        $this->vouchers = $vouchers;
        $this->orderObject = $orderObject;
        $this->esbNotification = $esbNotification;
        $this->paymentInquiry = $paymentInquiry;
        $this->physicalProductEmail = $physicalProductEmail;
        $this->transactionBuilder = $transactionBuilder;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Payment_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Check if response is valid
     *
     * @param array $response
     * @return boolean
     */
    public function isResponseValid(array $response)
    {
        if ($this->getCommonArrayKey($response, 'status')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if common key exist in response
     *
     * @param  array $data
     * @param string $keySearch
     * @return boolean
     */
    public function getCommonArrayKey($array, $keySearch)
    {
        foreach ($array as $key => $item) {
            $key = strtolower($key);
            if ($key == $keySearch) {
                return true;
            } elseif (is_array($item) && $this->getCommonArrayKey($item, $keySearch)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Differentiate 3d or non 3d based on array format
     *
     * @param  array $response
     * @return boolean
     */
    public function isThreeDTransaction($response)
    {
        if (isset($response[0]['result'])) {
            return true;
        }
        return false;
    }

    /**
     * Payment Gateway Response validation
     *
     * @param  array $response
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getTransactionDetails($response, $key = '', $orderId = "")
    {
        $this->logger->info("Enter in response check");
        $returnData = [
            'isValid' => false,
            '3ds' => false,
            'status' => false,
            'error' => null,
            'errorText' => null,
            'paymentId' => null,
            '3dsUrl' => null,
            'txnId' => null,
            'trackId' => null,
            'ref' => null
        ];
        if (!$response) {
            return $returnData;
        }
        $responseData = $this->json->unserialize($response);
        if (!is_array($responseData)) {
            return $returnData;
        }
        if (!isset($orderId)) {
            $returnData['status'] = false;
            $returnData['error'] = "error";
            $returnData['errorText'] = __("invalid order id");
            return $returnData;
        }

        if ($this->isResponseValid($responseData)) {
            $returnData['isValid'] = true;
            if ($responseData[0]['status'] == 1) {
                $returnData['status'] = true;
            } else {
                $returnData['status'] = false;
            }
            if ($this->isThreeDTransaction($responseData)) {
                // 3d transaction details
                $returnData['3ds'] = true;
                $returnData['error'] = isset($responseData[0]['error']) ? $responseData[0]['error'] : null;
                $returnData['errorText'] = isset($responseData[0]['errorText']) ? $responseData[0]['errorText'] : null;
                $result = isset($responseData[0]['result']) ? $responseData[0]['result'] : null;
                if ($result != null) {
                    $threeDData = $this->getPaymentIdUrl($result);
                    $returnData['paymentId'] = $threeDData['paymentId'];
                    $returnData['3dsUrl'] = $threeDData['3dsUrl'];
                }
            } else {
                // Non 3d transaction details
                if ($responseData[0]['status'] == 1) {
                    $this->logger->info($orderId. " - non 3d payment status is successful");
                    if (isset($responseData[0]['trandata'])) {
                        $this->logger->info($orderId. " - non 3d payment success trandata available");
                        $decodedTransData = $this->encryption->decryptData(
                            $responseData[0]['trandata'],
                            $key
                        );

                        $data = $this->json->unserialize(urldecode($decodedTransData));
                        $this->logger->info($data);
                        $order = $this->orderObject->loadByIncrementId($orderId);
                        $payment = $order->getPayment();
                        //Setting ref in payment object
                        $returnData['ref'] = isset($data[0]['ref']) ? $data[0]['ref'] : "";
                        $payment->setData('arb_ref_id', $returnData['ref']);
                        
                        $returnData['trackId'] = isset($data[0]['trackId']) ? $data[0]['trackId'] : null;

                        //Setting authcode in payment object
                        $authCode = isset($data[0]['authCode']) ? $data[0]['authCode'] : 0;
                        $payment->setData('arb_auth_code', $authCode);

                        $returnData['orderId'] = $order->getIncrementId();
                        $order->setPayment($payment);

                        //Create order transaction 
                        $this->createOrderTrancation($payment, $order, $data[0]);
                        
                        // Then changing status to processing from pending payment
                        $order
                        ->setState(PaymentInquiry::POST_PAYMENT_ORDER_STATUS)
                        ->setStatus(PaymentInquiry::POST_PAYMENT_ORDER_STATUS);
                        $order->save();
                        //Email Sent For Physical products
                        if (!$order->getIsVirtual()) {
                            $this->physicalProductEmail->preparePhysicalProductEmail($order->getId());
                        }
                        $assignVouchers = $this->vouchers->assignVouchers($order->getIncrementId());
                        if ($assignVouchers === true) {
                            $this->logger->info($orderId . " - non 3d payment, voucher assigned");
                            $this->esbNotification->prepareOrderData($order->getIncrementId());
                        } else {
                            $this->logger->info($orderId . " - non 3d payment, voucher not assigned");
                        }

                        if (($order->canInvoice()) && ($assignVouchers === true)) {                            
                            // Changing status to complete for virtual after successfull voucher send and invoice generate
							if ($order->getIsVirtual()) {
                                $this->paymentInquiry->createInvoice($order->getId());
                                $this->logger->info($orderId . " - non 3d payment, invoice generated");
								$order->setState(PaymentInquiry::POST_INVOICE_ORDER_STATUS)->setStatus(PaymentInquiry::POST_INVOICE_ORDER_STATUS);
                                $order->save();
                                $this->logger->info($orderId . " - non 3d payment, Order status set to Complete for virtual");
							}
                        } else {
                            $this->logger->info($orderId . " - non 3d payment, invoice generated failed");
                        }
                        $returnData['status'] = true;
                        $returnData['orderStatus'] = $order->getStatus();
                        $returnData['error'] = isset($responseData[0]['error']) ? $responseData[0]['error']: null;
                        $returnData['errorText']=isset($responseData[0]['errorText'])?$responseData[0]['errorText']:null;
                    } else {
                        $this->logger->info($orderId. " - non 3d payment success but no transdata available");
                        $order = $this->orderObject->loadByIncrementId($orderId);
                        $order->cancel();
                        $order->save();
                        $this->logger->info($orderId. " - non 3d payment cancelled");
                        $this->vouchers->unBlockVouchers($orderId);
                        $this->logger->info($orderId." - non 3d payment success but no transdata available, after voucher unblocked");
                        $returnData['result'] = 'error';
                        $returnData['status'] = false;
                        $returnData['orderStatus'] = "Cancelled";
                        $returnData['error'] = isset($responseData[0]['error']) ? $responseData[0]['error'] : null;
                        $returnData['errorText']=isset($responseData[0]['errorText'])?$responseData[0]['errorText']:null;
                    }
                } else {
                    $this->logger->info($orderId. " - non 3d payment status is failed");
                    $order = $this->orderObject->loadByIncrementId($orderId);
                    $order->cancel();
                    $order->save();
                    $this->logger->info($orderId. " - non 3d payment cancelled");
                    $this->vouchers->unBlockVouchers($orderId);
                    $this->logger->info($orderId. " - non 3d payment failure so voucher unblocked");

                    if (isset($responseData[0]['trandata'])) {
                        $this->logger->info($orderId. " - non 3d payment failure trandata available");
                        $decodedTransData = $this->encryption->decryptData(
                            $responseData[0]['trandata'],
                            $key
                        );
                        
                        $data = $this->json->unserialize(urldecode($decodedTransData));
                        $this->logger->info($data);

                        $payment = $order->getPayment();

                        //Setting ref in payment object
                        $returnData['ref'] = isset($data[0]['ref']) ? $data[0]['ref'] : "";
                        $payment->setData('arb_ref_id', $returnData['ref']);
                        
                        $returnData['trackId'] = isset($data[0]['trackId']) ? $data[0]['trackId'] : null;

                        //Setting authcode in payment object
                        $authCode = isset($data[0]['authCode']) ? $data[0]['authCode'] : 0;
                        $payment->setData('arb_auth_code', $authCode);

                        $returnData['orderId'] = $order->getIncrementId();
                        $order->setPayment($payment);
                        
                        //Create order transaction 
                        $this->createOrderTrancation($payment, $order, $data[0]);

                        $order->save();
                    }
                    $returnData['status'] = false;
                    $returnData['orderStatus'] = "Cancelled";
                    $returnData['result'] = 'error';
                    $returnData['error'] = isset($responseData[0]['error']) ? $responseData[0]['error'] : null;
                    $returnData['errorText']=isset($responseData[0]['errorText'])?$responseData[0]['errorText']:null;
                }
            }
        }
        return $returnData;
    }

    /**
     * Returns payment Id and URL for 3D secure payment
     *
     * @param string $threeDresult
     * @return array
     */
    private function getPaymentIdUrl($threeDresult)
    {
        list($paymentId, $url) = explode(':', $threeDresult, 2);
        $data = [
            'paymentId' => $paymentId,
            '3dsUrl' => $url
        ];
        return $data;
    }

    /**
     * Create Order trancation for payment
     *
     * @param object $payment
     * @param object $order
     * @param array $data
     * @return void
     */
    public function createOrderTrancation($payment, $order, $data)
    {
        if(!empty($data['transId'])) { 
            $payment->setTransactionId($data['transId']);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $data]
            );

            $trans = $this->transactionBuilder;

            $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($data['transId'])
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $data]
                )
                ->setFailSafe(true)         
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->setParentTransactionId(null);
            $payment->save();
        } else {
            $orderId = $order->getIncrementId();
            $this->logger->info($orderId." - payment transId not available, transaction not created.");
        }
    }
}
