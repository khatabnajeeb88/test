<?php
/**
 * Payment gateway transaction purchase API
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */

namespace Arb\ArbPayment\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Settings class configuration
 */
class Settings
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * class settings constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Enabled payment method store in array
     *
     * @param string $paymentMethodCode
     * @return array
     */
    public function getEnabledForStores($paymentMethodCode)
    {
        $xmlConfigPath = "payment/{$paymentMethodCode}/active";
        $values = $this->getValue($xmlConfigPath);
        if ($values) {
            return [$this->getCurrentStoreCode()];
        }

        return [];
    }

    /**
     * Returns current store code
     *
     * @param void
     * @return string
     */
    public function getCurrentStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }

    /**
     * Returns configuration
     *
     * @param  string $xmlPath
     * @return string
     */
    protected function getValue($xmlPath)
    {
        return $this->scopeConfig->getValue($xmlPath, ScopeInterface::SCOPE_STORE);
    }
}
