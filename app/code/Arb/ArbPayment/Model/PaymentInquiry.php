<?php
/**
 * Payment gateway transaction inquiry
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model;

use Arb\Order\Model\Order\Email\OrderEmailSender as PhysicalProductEmail;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Arb\ArbPayment\Model\PaymentLog;
use \Magento\Sales\Model\Order\Invoice;
use \Magento\Framework\Event\ManagerInterface;

/**
 * PaymentInquiry class to perform inquiry
 */
class PaymentInquiry implements \Arb\ArbPayment\Api\PaymentInquiryInterface
{
    /**
     *  Inquiry Payment Action
     */
    const PAYMENT_ACTION = '8';

    /**
     * Intial inquriry count
     */
    const INITIAL_INQUIRY_COUNT = 1;

    /**
     * 3D transaction payment id
     */
    const PAYMENT_ID = 'arb_payment_id';

    /**
     * Non 3D tracking id
     */
    const TRACKING_ID = 'arb_track_id';

    /**
     * Order id field in payment table
     */
    const PARENT_ID = 'parent_id';

    /**
     * Default payment inquiry auth code
     */
    const DEFAULT_AUTH_CODE = '0';

    /**
     * Payment gateway failure response code
     */
    const PAYMNET_FAILURE = 2;

    /**
     * Payment gateway success response code
     */
    const PAYMENT_SUCCESS = 1;

    /**
     * NON 3D response returns payment id as -1
     */
    const NON_THREED_PAYMENT_ID = -1;

    /**
     * Before purchase API call order status
     */
    const BEFORE_PAYMENT_ORDER_STATUS = "pending_payment";

    /**
     * After purchase API call order status
     */
    const POST_PAYMENT_ORDER_STATUS = "processing";

    /**
     * After invoice call order status
     */
    const POST_INVOICE_ORDER_STATUS = "complete";

    /**
     * Inquiry UDF array mapping
     *
     * @var array
     */
    public $inquiryUDF =[
        self::PAYMENT_ID => 'PaymentID',
        self::TRACKING_ID => 'TrackID'
    ];

    /**
     * Payment configuration
     *
     * @var array
     */
    protected $paymentConfig = [];

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curlClient;

    /**
     * @var \Arb\ArbPayment\Model\Encryption
     */
    private $encryptor;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Sales\Model\Order\Payment
     */
    protected $payment;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     *
     * @var \Arb\ArbPayment\Model\PaymentLog
     */
    protected $paymentLog;

    /**
     * @var int
     */
    private $maxRequestCount;

    /**
     * @param \Magento\Sales\Model\Service\InvoiceService
     */
    private $invoiceService;

    /**
     * @param \Magento\Framework\DB\TransactionFactory
     */
    private $transactionFactory;

    /**
     * @var \Arb\Vouchers\Model\VoucherOrders
     */
    protected $vouchers;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $orderObject;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var PhysicalProductEmail
     */
    private $physicalProductEmail;

    /**
     * Payment Inquiry class constructor
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Sales\Model\Order\Payment $payment
     * @param \Arb\ArbPayment\Model\Encryption $encryptor
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     * @param PaymentLog $paymentLog
     * @param \Magento\Sales\Model\Service\InvoiceService $invoiceService
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     * @param \Arb\Vouchers\Model\VoucherOrders $vouchers
     * @param \Magento\Sales\Model\Order $orderObject
     * @param ESBNotifications $esbNotification
     * @param ManagerInterface $eventManager
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order\Payment $payment,
        \Arb\ArbPayment\Model\Encryption $encryptor,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Serialize\Serializer\Json $json,
        PaymentLog $paymentLog,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Arb\Vouchers\Model\VoucherOrders $vouchers,
        \Magento\Sales\Model\Order $orderObject,
        \Arb\ArbPayment\Model\ESBNotifications $esbNotification,
        ManagerInterface $eventManager,
        PhysicalProductEmail $physicalProductEmail,
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder
    ) {
        $this->encryptor = $encryptor;
        $this->scopeConfig = $scopeConfig;
        $this->payment = $payment;
        $this->curlClient = $curl;
        $this->json = $json;
        $this->paymentLog = $paymentLog;
        $this->invoiceService = $invoiceService;
        $this->transactionFactory = $transactionFactory;
        $this->maxRequestCount = 3;
        $this->vouchers = $vouchers;
        $this->orderObject = $orderObject;
        $this->esbNotification = $esbNotification;
        $this->transactionBuilder = $transactionBuilder;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Payment_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->eventManager = $eventManager;
        $this->physicalProductEmail = $physicalProductEmail;
    }

    /**
     * Returns payment inquiry details
     *
     * @param  string $paymentId
     * @return mixed
     */
    public function processPaymentInquiry($paymentId)
    {
        $this->logger->info("Payment inquiry Initiated for payment/Track ID ". $paymentId);

        $paymentData = $this->getPaymentDataByField(self::PAYMENT_ID, $paymentId);

        for ($i = self::INITIAL_INQUIRY_COUNT; $i <= $this->maxRequestCount; $i++) {
            $response = $this->getInquiryData($paymentData, $i);
            if (isset($response['orderStatus'])) {
                break;
            } else {
                $this->logger->info("Payment Inquiry is not successfull in attempt".$i." for payment/Track ID ". $paymentId);
            }
        }
        unset($response['orderStatus']);
        return [$response];
    }

    /**
     * Returns payment data by field
     *
     * @param  string $field
     * @param  string $value
     * @return array
     */
    public function getPaymentDataByField($field, $value)
    {
        try {
            $payment = $this->payment->load($value, $field);
            if (!($payment->getArbTrackId())) {
                $this->logger->info("Inquiry, wrong value - ".$value. " for field - ". $field);
                throw new LocalizedException(
                    __("Wrong track id shared")
                );
            }
            $paymentConfig = $this->getPaymentConfiguration();
            if (isset($paymentConfig['merchant_password']) && isset($paymentConfig['merchant_id'])) {
                $paymentData[] = [
                'amt' => $payment->getAmountOrdered(),
                'action' => self::PAYMENT_ACTION,
                'password' => $paymentConfig['merchant_password'],
                'id' => $paymentConfig['merchant_id'],
                'currencyCode' => $paymentConfig['currency_code'],
                'trackId' => $payment->getOrder()->getIncrementId(),
                'udf5' => $this->inquiryUDF[$field],
                'transId' => $value,
                'orderId' => $payment->getOrder()->getIncrementId()
                ];
            } else {
                $this->logger->info("Inquiry, wrong configuration for merchant password and id in backend");
                throw new LocalizedException(
                    __("Configuration problem exist")
                );
            }
        } catch (\Exception $e) {
            $this->paymentLog->logPaymentException('getPaymentDataByField : '.$e->getMessage());
            throw new LocalizedException(
                __('Error in track id loading')
            );
        }
        return $paymentData;
    }

    /**
     * Returns payment configuration
     *
     * @return array
     */
    private function getPaymentConfiguration()
    {
        $this->paymentConfig['iv_config'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/vector_init',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_id'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_id',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_password'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_password',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['resource_key'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_terminal_resourcekey',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['currency_code'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_currency_code',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['inquiry_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_inquiry_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['response_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_response_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['error_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_error_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['timeout'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_timeout',
            ScopeInterface::SCOPE_STORE
        );

        return $this->paymentConfig;
    }

    /**
     * Returns Inquiry Response from Payment Gateway
     *
     * @param array
     * @param int $inquiryCount
     * @param bool $orderCron Executing paramter for order cron
     * @return void
     */
    public function getInquiryData($paymentData, $inquiryCount, $orderCron = false)
    {
        $orderId = 0;
        if (isset($paymentData[0]['orderId'])) {
            $orderId = $paymentData[0]['orderId'];
            unset($paymentData[0]['orderId']);
        }
        $this->logger->info($orderId . " - Inquiry");

        $paymentConfig = $this->getPaymentConfiguration();
        $inquiryURL = $paymentConfig['inquiry_url'];
        $responseUrl = $paymentConfig['response_url'];
        $errorUrl = $paymentConfig['error_url'];
        $jsonPaymentData = $this->json->serialize($paymentData);
        $urlEncodeData = urlencode($jsonPaymentData);
        $transactionData = $this->encryptor->encryptAES($urlEncodeData, $paymentConfig['resource_key']);

        $data[]=[
            'id' => $paymentConfig['merchant_id'],
            'trandata' => $transactionData,
            'responseURL' => $responseUrl,
            'errorURL' => $errorUrl
        ];

        $encodeDetails = json_encode($data, JSON_UNESCAPED_SLASHES);
        $returnResponse = null ;
        $response = null;
        try {

            /**
            * We don't save plain merchant id and password in table,
            * therefore we mask before saving in DB.
            */

            $logRequest = $paymentData[0];
            $logRequest['id'] = '*****';
            $logRequest['password'] = '*****';
            $data = [
                'order_id' => $orderId,
                'api_type' => PaymentLog::INQUIRY,
                'inquiry_count' => $inquiryCount,
                'request' => $this->json->serialize($logRequest),
                'response' => null
            ];
            $this->logger->info($orderId . " - Inquiry Request, inquiry count - ". $inquiryCount);
            $logTableId = $this->paymentLog->setPaymentLog($data);
            $this->curlClient->addHeader("Content-Type", "application/json");
            $this->curlClient->setOption(CURLOPT_SSL_VERIFYHOST, 0);
            $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, 0);
            $this->curlClient->setOption(CURLOPT_RETURNTRANSFER, 1);
            $this->curlClient->setOption(CURLOPT_FOLLOWLOCATION, 1);
            $this->curlClient->setOption(CURLOPT_TIMEOUT, $paymentConfig['timeout']);
            $this->curlClient->post($inquiryURL, $encodeDetails);
            $response = $this->curlClient->getBody();
        } catch (\Exception $e) {
            $logData['response'] = $e->getMessage().' : '.$e->getTraceAsString();
            $this->paymentLog->updatePaymentLog($logTableId, $this->json->serialize($logData));
            if ($inquiryCount == $this->maxRequestCount) {
                throw new LocalizedException(
                    __("Payment Gateway Error")
                );
            }
        } finally {
            $paymentId = $paymentData[0]['transId'];
            $this->logger->info("Inquiry Response");
            $this->paymentLog->updatePaymentLog($logTableId, $this->json->serialize([$response]));
            $returnResponse = $this->processInquiryResponse($response, $orderId, $paymentId, $orderCron);
        }

        return $returnResponse;
    }

    /**
     * Process inquiry response
     *
     * @param  string $response
     * @param int $orderId
     * @param int $paymentId
     * @param bool $orderCron
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function processInquiryResponse($response, $orderId, $paymentId, $orderCron)
    {
        $return = [];
        $return['orderId'] = $orderId;
        if (null != $response) {
            $responseData = $this->json->unserialize($response);
            $this->logger->info($orderId. " - processInquiryResponse IN, orderCron - ". $orderCron);
            $this->logger->info($responseData);
            if (isset($responseData[0]['status'])) {
                if ($responseData[0]['status'] == self::PAYMENT_SUCCESS) {
                    $this->logger->info($orderId. " - processInquiryResponse payment success status IN");
                    if (isset($responseData[0]['trandata'])) {
                        $this->logger->info($orderId. " - processInquiryResponse payment success trandata available");
                        $decodedTransData = $this->encryptor->decryptData(
                            $responseData[0]['trandata'],
                            $this->paymentConfig['resource_key']
                        );

                        $data = $this->json->unserialize(urldecode($decodedTransData));
                        $this->logger->info($data);

                        // if non 3D or order cleanup cron. For non 3D, paymentId value will be -1
                        if (($data[0]['paymentId'] == self::NON_THREED_PAYMENT_ID) || $orderCron) {
                            $this->logger->info($orderId . " - processInquiryResponse non 3D or order cleanup cron IN");
                            $order = $this->orderObject->loadByIncrementId($orderId);
                            $payment = $order->getPayment();
                        } else {
                            $this->logger->info($orderId . " - processInquiryResponse 3D IN");
                            $payment = $this->payment->load($paymentId, self::PAYMENT_ID);
                            $order = $payment->getOrder();
                        }

                        //Setting ref in payment object
                        $ref = isset($data[0]['ref']) ? $data[0]['ref'] : "";
                        $payment->setData('arb_ref_id', $ref);

                        //Setting authcode in payment object
                        $authCode = isset($data[0]['authCode']) ? $data[0]['authCode'] : self::DEFAULT_AUTH_CODE;
                        $payment->setData('arb_auth_code', $authCode);

                        $return['orderId'] = $order->getIncrementId();
                        $order->setPayment($payment);
                        
                        //Create order transaction 
                        $this->createOrderTrancation($payment, $order, $data[0]);

                        // Then changing status to processing from pending payment
                        $order->setState(self::POST_PAYMENT_ORDER_STATUS)->setStatus(self::POST_PAYMENT_ORDER_STATUS);
                        $order->save();
                        //Email Sent For Physical products
                        if (!$order->getIsVirtual()) {
                            $this->physicalProductEmail->preparePhysicalProductEmail($order->getId());
                        }

                        $assignVouchers = $this->vouchers->assignVouchers($order->getIncrementId());
                        if ($assignVouchers === true) {
                            $this->logger->info($orderId . " - processInquiryResponse voucher assigned");
                            $this->esbNotification->prepareOrderData($order->getIncrementId());
                        } else {
                            $this->logger->info($orderId . " - processInquiryResponse voucher not assigned");
                        }
                        if (($order->canInvoice()) && ($assignVouchers === true)) {
                            // Changing status to complete for virtual after successfull voucher send and invoice generate
							if ($order->getIsVirtual()) {
                                $this->createInvoice($order->getId());
                                $this->logger->info($orderId . " - processInquiryResponse invoice generated");
								$order->setState(self::POST_INVOICE_ORDER_STATUS)->setStatus(self::POST_INVOICE_ORDER_STATUS);
                                $order->save();
                                $this->logger->info($orderId . " - processInquiryResponse, Order status set to Complete for virtual");
							}
                        } else {
                            $this->logger->info($orderId . " - processInquiryResponse invoice generated failed");
                        }
                        $return['status'] = true;
                        $return['orderStatus'] = $order->getStatus();
                        $return['error'] = isset($responseData[0]['error']) ? $responseData[0]['error']: null;
                        $return['errorText'] = isset($responseData[0]['errorText']) ? $responseData[0]['errorText']: null;
                    } else {
                        $this->logger->info($orderId. " - processInquiryResponse payment success but no transdata available");
                        $order = $this->orderObject->loadByIncrementId($orderId);
                        $order->cancel();
                        $order->save();
                        $this->logger->info($orderId. " - cancelled");
                        $this->vouchers->unBlockVouchers($orderId);
                        $this->logger->info($orderId. " - processInquiryResponse payment success but no transdata available, after voucher unblocked");

                        $return['result'] = 'error';
                        $return['status'] = false;
                        $return['orderStatus'] = "Cancelled";
                        $return['error'] = isset($responseData[0]['error']) ? $responseData[0]['error'] : null;
                        $return['errorText'] = isset($responseData[0]['errorText']) ? $responseData[0]['errorText'] : null;
                    }
                } else {
                    $this->logger->info($orderId. " - processInquiryResponse payment failure status IN");
                    $order = $this->orderObject->loadByIncrementId($orderId);
                    $order->cancel();
                    $order->save();
                    $this->logger->info($orderId. " - cancelled");
                    $this->vouchers->unBlockVouchers($orderId);
                    $this->logger->info($orderId. " - processInquiryResponse  payment failure so voucher unblocked");

                    if (isset($responseData[0]['trandata'])) {
                        $this->logger->info($orderId. " - processInquiryResponse  payment failure trandata available");
                        $decodedTransData = $this->encryptor->decryptData(
                            $responseData[0]['trandata'],
                            $this->paymentConfig['resource_key']
                        );

                        $data = $this->json->unserialize(urldecode($decodedTransData));
                        $this->logger->info($data);

                        // if non 3D or order cleanup cron. For non 3D payment Id will be -1
                        if (($data[0]['paymentId'] == self::NON_THREED_PAYMENT_ID) || $orderCron) {
                            $this->logger->info($orderId . " - processInquiryResponse payment failure non 3D or order cleanup cron IN");
                            $order = $this->orderObject->loadByIncrementId($orderId);
                            $payment = $order->getPayment();
                        } else {
                            $this->logger->info($orderId . " - processInquiryResponse payment failure 3D IN");
                            $payment = $this->payment->load($paymentId, self::PAYMENT_ID);
                            $order = $payment->getOrder();
                        }

                        //Setting ref in payment object
                        $ref = isset($data[0]['ref']) ? $data[0]['ref'] : "";
                        $payment->setData('arb_ref_id', $ref);

                        //Setting authcode in payment object
                        $authCode = isset($data[0]['authCode']) ? $data[0]['authCode'] : self::DEFAULT_AUTH_CODE;
                        $payment->setData('arb_auth_code', $authCode);

                        $return['orderId'] = $order->getIncrementId();
                        $order->setPayment($payment);
                        
                        //Added condition for order clean up job to not create transaction again
                        if(!$orderCron){
                            //Create order transaction 
                            $this->createOrderTrancation($payment, $order, $data[0]);
                        }
                        $order->save();
                    }
                    $return['status'] = false;
                    $return['orderStatus'] = "Cancelled";
                    $return['result'] = 'error';
                    $return['error'] = isset($responseData[0]['error'])?$responseData[0]['error']:null;
                    $return['errorText'] = isset($responseData[0]['errorText'])?$responseData[0]['errorText']:null;
                }
            } else {
                $this->logger->info("processInquiryResponse Payment Gateway No status available.");
                $return['status'] = false;
                $return['errorText'] = __('Payment Gateway Error- No status available in response');
            }
        } else {
            $this->logger->info("processInquiryResponse Payment Gateway Error, null response");
            $return['status'] = false;
            $return['errorText'] = __('Payment Gateway Error');
        }

        $this->logger->info($return);
        return $return;
    }

    /**
     * create order invoice to complete order
     * @param  int $orderId
     * @return void
     */
    public function createInvoice($orderId)
    {
        $order = $this->orderObject->load($orderId);
        $data = [];
        foreach ($order->getAllItems() as $item) {
            if ($item->getProductType() == 'virtual') {
                $data[$item->getItemId()] = intval($item->getQtyOrdered() - $item->getQtyInvoiced());
            }
        }

        if (is_array($data) && !empty($data)) {
            $this->logger->info($orderId . " - processInquiryResponse create Invoice for virtual items in order");
            $invoice = $this->invoiceService->prepareInvoice($order, $data);
            $invoice->setRequestedCaptureCase(Invoice::CAPTURE_OFFLINE);
            $invoice->register();
            $transaction = $this->transactionFactory->create()
                ->addObject($invoice)
                ->addObject($invoice->getOrder());
            $transaction->save();
        } else {
            $this->logger->info($orderId . " - processInquiryResponse no virtual items in order for create invoice");
        }
    }

    /**
     * Create Order trancation for payment
     *
     * @param object $payment
     * @param object $order
     * @param array $data
     * @return void
     */
    public function createOrderTrancation($payment, $order, $data)
    {
        //Check for existing transaction id
        $existingTransId = $payment->getLastTransId();
        if(isset($data['transId']) && !empty($data['transId']) && !$existingTransId) { 
            $payment->setTransactionId($data['transId']);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $data]
            );

            $trans = $this->transactionBuilder;

            $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($data['transId'])
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $data]
                )
                ->setFailSafe(true)         
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->setParentTransactionId(null);
            $payment->save();
        } else {
            $orderId = $order->getIncrementId();
            $this->logger->info($orderId." - payment transId not available, transaction not created.");
        }
    }
}
