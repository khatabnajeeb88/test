<?php
/**
 * Payment Log Model.
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model;

use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Model\AbstractModel;

/**
 * PaymentLog class for logging transaction
 */
// Magento extended property requires underscore property
// phpcs:disable
class PaymentLog extends AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    /**
     *  Magento model cache tag
     */
    const CACHE_TAG = 'arb_arbpayment_payment_log';

    /**
     * Payment gateway inquiry constant
     */
    const INQUIRY = 'inquiry';

    /**
     * Payment gateway purchase constant
     */
    const PURCHASE = 'purchase';

    /**
     * Payment gateway refund constant
     */
    const REFUND = 'refund';

    /**
     * Payment gateway refund inquiry
     */
    const REFUND_INQUIRY = 'refund_inquiry';

    /**
     * Payment gateway void request
     */
    const VOID = 'void';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'payment_log';

    /**
     * @var \Zend\Log\Writer\Stream
     */
    protected $_writer;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\ArbPayment\Model\ResourceModel\PaymentLog::class);
    }

    /**
     * Return a unique id for the model.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Returns log object
     *
     * @return \Zend\Log\Logger
     */
    protected function getLoggerFunction($file = null)
    {
        try {
            $fileName = BP .'/var/log/Arb_Payment_Log-'.date("Ymd").'.log';

            if($file !== null){
                $fileName = BP.$file;
            }

            $this->_writer = new \Zend\Log\Writer\Stream($fileName);
            $this->_logger = new \Zend\Log\Logger();
            return $this->_logger->addWriter($this->_writer);
        } catch (\Exception $th) {
            throw new LocalizedException(
                __('Error in log file creation')
            );
        }
    }

    /**
     * Sets Payment logs in table
     *
     * @param  array $data
     * @param string $file
     * @return int
     * @throws \Exception
     */
    public function setPaymentLog($data,$file = null)
    {
        try {
            $logger = $this->getLoggerFunction();
            $logger->info("set Payment Log in database");
            $logger->info(print_r($data, true));
            return $this->setData($data)->save()->getId();
        } catch (\Exception $e) {
            throw new LocalizedException(
                __("Unable to add payment database logs")
            );
        }
    }

    /**
     * Updates log table
     *
     * @param int $logTableId
     * @param string $response
     * @param string $file
     * @return void
     */
    public function updatePaymentLog($logTableId, $response, $file = null)
    {
        try {
            $logger = $this->getLoggerFunction();
            $logger->info("update Payment Log in database");
            $logger->info(print_r($response, true));
            $paymentLogObject = $this->load($logTableId);
            if ($paymentLogObject->getId()) {
                $paymentLogObject->setResponse($response)->save();
            }
        } catch (\Exception $e) {
            throw new LocalizedException(
                __("Unable to update payment database logs")
            );
        }
    }

    /**
     * Logs exception messages in log files
     *
     * @param  string $message
     * @return void
     */
    public function logPaymentException($message)
    {
        $logger = $this->getLoggerFunction();
        $logger->crit($message);
    }
}
