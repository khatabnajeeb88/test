<?php
/**
 * Payment Log Collection.
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model\ResourceModel\PaymentLog;

// Magento extended property requires underscore property
// phpcs:disable
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'arb_arbpayment_payment_log_collection';
    protected $_eventObject = 'payment_log_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\ArbPayment\Model\PaymentLog::class, \Arb\ArbPayment\Model\ResourceModel\PaymentLog::class);
    }
}
