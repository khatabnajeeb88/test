<?php
/**
 * Payment Log ResourceModel.
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Model\ResourceModel;

class PaymentLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * PaymentLog Resource Model Constructor
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    // phpcs:disable
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('arb_payment_log', 'entity_id');
    }
}
