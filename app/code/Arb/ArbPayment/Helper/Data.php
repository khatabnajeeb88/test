<?php
/**
 * ARB Payment
 *
 * @category Arb
 * @package Arb_ArbPayment
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbPayment\Helper;

use Magento\Framework\App\Helper\Context;
use \Magento\Store\Model\ScopeInterface;

/**
 * ARBPayemt data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * 
     * @param Context $context
     */
    public function __construct(Context $context) {
        parent::__construct($context);
    }

    /**
     * getPaymentConfiguration
     *
     * @param  mixed $paymentType
     * @return void
     */
    public function getPaymentConfiguration($paymentType)
    {
        $this->paymentConfig['iv_config'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/vector_init',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_id'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_id',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['merchant_password'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/merchant_password',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['resource_key'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_terminal_resourcekey',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['currency_code'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_currency_code',
            ScopeInterface::SCOPE_STORE
        );
        $this->paymentConfig['inquiry_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_inquiry_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['response_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_response_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['error_url'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_error_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->paymentConfig['timeout'] = $this->scopeConfig->getValue(
            'payment/arbpayment_gateway/pg_timeout',
            ScopeInterface::SCOPE_STORE
        );
        if($paymentType=='purchase'){
            $this->paymentConfig['purchase_url'] = $this->scopeConfig->getValue(
                'payment/arbpayment_gateway/pg_purchase_url',
                ScopeInterface::SCOPE_STORE
            );
    
        }
        if($paymentType=='refund'){
            $this->paymentConfig['refund_url'] = $this->scopeConfig->getValue(
                'payment/arbpayment_gateway/pg_refund_url',
                ScopeInterface::SCOPE_STORE
            );
            $this->paymentConfig['purchase_url'] = $this->scopeConfig->getValue(
                'payment/arbpayment_gateway/pg_purchase_url',
                ScopeInterface::SCOPE_STORE
            );    
        }

        return $this->paymentConfig;
    }
}
