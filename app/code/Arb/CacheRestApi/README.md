## Synopsis
This is module  depends on module Magento_PageCache, Arb_Banners, Arb_ElasticSearch

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).