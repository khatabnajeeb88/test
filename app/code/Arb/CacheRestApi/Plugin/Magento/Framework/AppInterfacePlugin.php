<?php
/**
 * Process the API request or load it from cache
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Plugin\Magento\Framework;

use Arb\CacheRestApi\Model\RestApiCacheManagement;

class AppInterfacePlugin
{
    /**
     * @var RestApiCacheManagement
     */
    private $restApiCacheManagement;

    /**
     * AppInterfacePlugin constructor.
     * @param RestApiCacheManagement $restApiCacheManagement
     */
    public function __construct(RestApiCacheManagement $restApiCacheManagement)
    {
        $this->restApiCacheManagement = $restApiCacheManagement;
    }

    /**
     * @param \Magento\Framework\AppInterface $subject
     * @param \Closure $proceed
     * @return bool|false|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Webapi\Rest\Response|mixed
     */
    public function aroundLaunch(\Magento\Framework\AppInterface $subject, \Closure $proceed)
    {
        if (!$this->restApiCacheManagement->canProcessRequest()) {
            return $proceed();
        }
        $response = $this->restApiCacheManagement->loadCacheResult();
        if (!$response) {
            /** @var \Magento\Framework\App\ResponseInterface $response */
            $response = $proceed();
            $this->restApiCacheManagement->saveCacheResult($response);
        }

        return $response;
    }
}
