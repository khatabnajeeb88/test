<?php
/**
 *  Add banners tags to tag manager
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Plugin\Arb\Banners\Model;

use Arb\Banners\Model\BannerManagement;
use Arb\Banners\Model\Banners;
use Arb\CacheRestApi\Model\CacheTagEntityManager;

/**
 * Class BannerManagementPlugin
 */
class BannerManagementPlugin
{
    /**
     * @var CacheTagEntityManager
     */
    private $cacheTagEntityManager;

    /**
     * CatalogCategoryCollectionLoadAfter constructor.
     * @param CacheTagEntityManager $cacheTagEntityManager
     */
    public function __construct(
        CacheTagEntityManager $cacheTagEntityManager
    ) {
        $this->cacheTagEntityManager = $cacheTagEntityManager;
    }

    /**
     * @param BannerManagement $subject
     * @param $result
     * @return mixed
     */
    public function afterGetBanners(BannerManagement $subject, $result)
    {
        $tags = [];
        foreach ($result as $bannerData) {
            $tags[] = Banners::CACHE_TAG . '_' . $bannerData['banners_id'];
        }

        if (!empty($tags)) {
            $this->cacheTagEntityManager->addTags($tags);
        }

        return $result;
    }
}
