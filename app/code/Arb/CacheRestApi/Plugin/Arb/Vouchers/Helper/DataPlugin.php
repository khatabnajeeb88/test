<?php
/**
 * Plugin for update cache for vocuher upload
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */

namespace Arb\CacheRestApi\Plugin\Arb\Vouchers\Helper;

use Arb\Vouchers\Helper\Data;
use Arb\CacheRestApi\Model\CacheType;

/**
 * Arb Vouchers Helper Email.
 * 
 */
class DataPlugin
{

    /**
     * cacheType constructor.
     * @param CacheType $cacheType
     */
    public function __construct(
        CacheType $cacheType
    ) {
        $this->restApiCache = $cacheType;
    }
    
    /**
     * updateProductStock
     *
     * @param  array $data
     * @return void
     */
    public function afterUpdateProductStock(Data $subject, $result)
    {
        $this->restApiCache->clean(
            \Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
            [\Magento\Catalog\Model\Product::CACHE_TAG]
        );

        return $result;
    }

}
