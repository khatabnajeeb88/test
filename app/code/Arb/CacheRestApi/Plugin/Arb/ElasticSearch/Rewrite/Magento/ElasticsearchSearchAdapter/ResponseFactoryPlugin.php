<?php
/**
 *  Add search result entity tag to tag manager
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Plugin\Arb\ElasticSearch\Rewrite\Magento\ElasticsearchSearchAdapter;

use Magento\Catalog\Model\Product;
use Magento\Framework\Search\Response\QueryResponse;

use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Arb\ElasticSearch\Rewrite\Magento\Elasticsearch\SearchAdapter\ResponseFactory;

/**
 * Class ResponseFactoryPlugin
 */
class ResponseFactoryPlugin
{
    /**
     * @var CacheTagEntityManager
     */
    private $cacheTagEntityManager;

    /**
     * CatalogCategoryCollectionLoadAfter constructor.
     * @param CacheTagEntityManager $cacheTagEntityManager
     */
    public function __construct(
        CacheTagEntityManager $cacheTagEntityManager
    ) {
        $this->cacheTagEntityManager = $cacheTagEntityManager;
    }

    /**
     * @param ResponseFactory $subject
     * @param QueryResponse $result
     * @return mixed
     */
    public function afterCreate(ResponseFactory $subject, $result)
    {
        $tags = [Product::CACHE_TAG];
        foreach ($result as $searchResultItem) {
            $tags[] = Product::CACHE_TAG . '_' . $searchResultItem['id'];
        }

        if (!empty($tags)) {
            $this->cacheTagEntityManager->addTags($tags);
        }

        return $result;
    }
}
