<?php
/**
 *  Flush cache by tags
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
declare(strict_types=1);

namespace Arb\CacheRestApi\Observer\CleanCache;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Cache\Tag\Resolver as TagResolver;

use Arb\CacheRestApi\Model\CacheType;
use Arb\CacheRestApi\Model\RestApiCacheManagement;

/**
 * Class FlushCacheByTags
 */
class FlushCacheByTags implements ObserverInterface
{
    /**
     * @var CacheType
     */
    private $restApiCache;

    /**
     * @var RestApiCacheManagement
     */
    private $restApiCacheManagement;

    /**
     * Invalidation tags resolver
     *
     * @var \Magento\Framework\App\Cache\Tag\Resolver
     */
    private $tagResolver;

    /**
     * FlushCacheByTags constructor.
     * @param CacheType $restApiCache
     * @param RestApiCacheManagement $restApiCacheManagement
     * @param TagResolver $tagResolver
     */
    public function __construct(
        CacheType $restApiCache,
        RestApiCacheManagement $restApiCacheManagement,
        TagResolver $tagResolver
    ) {
        $this->restApiCache = $restApiCache;
        $this->restApiCacheManagement = $restApiCacheManagement;
        $this->tagResolver = $tagResolver;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->restApiCacheManagement->isCacheEnabled()) {
            $object = $observer->getEvent()->getObject();
            if (!is_object($object)) {
                return;
            }
            $tags = $this->tagResolver->getTags($object);
            if (!empty($tags)) {
                $this->restApiCache->clean(\Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array_unique($tags));
            }
        }
    }
}
