<?php
/**
 *  Flush all Rest Api Cache
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
declare(strict_types=1);

namespace Arb\CacheRestApi\Observer\CleanCache;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Arb\CacheRestApi\Model\CacheType;

/**
 * Class FlushAllCache
 */
class FlushAllCache implements ObserverInterface
{
    /**
     * @var CacheType
     */
    private $cache;

    /**
     * FlushAllCache constructor.
     * @param CacheType $cache
     */
    public function __construct(
        CacheType $cache
    ) {
        $this->cache = $cache;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $this->cache->clean();
    }
}
