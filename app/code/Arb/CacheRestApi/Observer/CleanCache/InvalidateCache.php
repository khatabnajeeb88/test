<?php
/**
 *  Invalidate Rest Api Cache
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
declare(strict_types=1);

namespace Arb\CacheRestApi\Observer\CleanCache;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

use Arb\CacheRestApi\Model\CacheType;

/**
 * Class InvalidateCache
 */
class InvalidateCache implements ObserverInterface
{
    /**
     * @var TypeListInterface
     */
    private $typeList;

    /**
     * InvalidateCache constructor.
     * @param TypeListInterface $typeList
     */
    public function __construct(
        TypeListInterface $typeList
    ) {
        $this->typeList = $typeList;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $this->typeList->invalidate(CacheType::TYPE_IDENTIFIER);
    }
}
