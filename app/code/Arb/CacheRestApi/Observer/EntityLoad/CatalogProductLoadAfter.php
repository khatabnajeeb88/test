<?php
/**
 *  Add products tags to tag manager
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Observer\EntityLoad;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product;

use Arb\CacheRestApi\Model\CacheTagEntityManager;

/**
 * Class CatalogProductLoadAfter
 */
class CatalogProductLoadAfter implements ObserverInterface
{
    /**
     * @var CacheTagEntityManager
     */
    private $cacheTagEntityManager;

    /**
     * CatalogProductLoadAfter constructor.
     * @param CacheTagEntityManager $cacheTagEntityManager
     */
    public function __construct(
        CacheTagEntityManager $cacheTagEntityManager
    ) {
        $this->cacheTagEntityManager = $cacheTagEntityManager;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $product = $observer->getProduct();
        $tags = [
            Product::CACHE_TAG,
            Product::CACHE_TAG . '_' . $product->getId()
        ];
        $this->cacheTagEntityManager->addTags($tags);
    }
}
