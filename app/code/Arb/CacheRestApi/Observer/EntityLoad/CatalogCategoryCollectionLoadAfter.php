<?php
/**
 *  Add category tags to tag manager
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
declare(strict_types=1);

namespace Arb\CacheRestApi\Observer\EntityLoad;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Category;

use Arb\CacheRestApi\Model\CacheTagEntityManager;

/**
 * Class CatalogCategoryCollectionLoadAfter
 */
class CatalogCategoryCollectionLoadAfter implements ObserverInterface
{
    /**
     * @var CacheTagEntityManager
     */
    private $cacheTagEntityManager;

    /**
     * CatalogCategoryCollectionLoadAfter constructor.
     * @param CacheTagEntityManager $cacheTagEntityManager
     */
    public function __construct(
        CacheTagEntityManager $cacheTagEntityManager
    ) {
        $this->cacheTagEntityManager = $cacheTagEntityManager;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $collection = $observer->getCategoryCollection();
        $tags = [Category::CACHE_TAG];
        $ids = array_keys($collection->getItems());
        foreach ($ids as $id) {
            $tags[] = Category::CACHE_TAG . '_' . $id;
        }

        $this->cacheTagEntityManager->addTags($tags);
    }
}
