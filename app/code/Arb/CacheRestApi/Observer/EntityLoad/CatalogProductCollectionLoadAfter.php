<?php
/**
 *  Add products tags to tag manager
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Observer\EntityLoad;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product;

use Arb\CacheRestApi\Model\CacheTagEntityManager;

/**
 * Class CatalogProductCollectionLoadAfter
 */
class CatalogProductCollectionLoadAfter implements ObserverInterface
{
    /**
     * @var CacheTagEntityManager
     */
    private $cacheTagEntityManager;

    /**
     * CatalogProductLoadAfter constructor.
     * @param CacheTagEntityManager $cacheTagEntityManager
     */
    public function __construct(
        CacheTagEntityManager $cacheTagEntityManager
    ) {
        $this->cacheTagEntityManager = $cacheTagEntityManager;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $tags = [Product::CACHE_TAG];
        $collection = $observer->getCollection();
        $productIds = array_keys($collection->getItems());
        foreach ($productIds as $product) {
            $tags[] = Product::CACHE_TAG . '_' . $product;
        }

        $this->cacheTagEntityManager->addTags($tags);
    }
}
