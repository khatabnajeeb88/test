<?php

/**
 * This file consist of PHPUnit test case for class CacheIdentifierManager
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

namespace Arb\CacheRestApi\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Model\CacheIdentifierManager;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Framework\App\RequestInterface;
use Arb\CacheRestApi\Model\CustomerAuth;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Customer\Model\Customer;

/**
 * @covers \Arb\CacheRestApi\Model\CacheIdentifierManager
 */
class CacheIdentifierManagerTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->groupInterfaceMock = $this->createMock(RequestInterface::class);
        //$this->_request = $this->createPartialMock(RequestInterface::class, ['getMethod','getHeader', 'getRequestUri']);
        $this->request = $this->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
            ->setMethods(['getMethod','getHeader', 'getRequestUri'])
            ->getMockForAbstractClass();
        $this->customerAuthMock = $this->createMock(CustomerAuth::class);
        $this->testObject = $objectManager->getObject(CacheIdentifierManager::class, [
            'request' =>  $this->request,
            'customerAuthMock' => $this->customerAuthMock
        ]);
    }

    public function testGetRequestIdentifier()
    {
        $customerMock = $this->createMock(Customer::class);
        $this->customerAuthMock->method('getCustomer')->willReturn($customerMock);
        $arary = [
            0 => 0,
            1 =>null,
            2 =>null,
            3 =>null
        ];
        $this->assertEquals($arary, $this->testObject->getRequestIdentifier());
    }
}
