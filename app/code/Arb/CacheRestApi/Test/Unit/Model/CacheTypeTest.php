<?php

/**
 * This file consist of PHPUnit test case for class CacheType
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

namespace Arb\CacheRestApi\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Model\CacheType;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\Cache\FrontendInterface;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\CacheRestApi\Model\CacheType
 */
class CacheTypeTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->frontendPoolMock = $this->createMock(FrontendPool::class);
        $this->_frontend = $this->createMock(\Magento\Framework\Cache\FrontendInterface::class);
        $this->_object = new \Magento\Framework\Cache\Frontend\Decorator\TagScope($this->_frontend, 'enforced_tag');
        // $this->testObject = $objectManager->getObject(CacheType::class, [
        //     'frontendPoolMock' => $this->frontendPoolMock,
        //     'frontend' => $this->_object
        // ]);
    }

    public function testGetTag()
    {
        $this->assertEquals('enforced_tag', $this->_object->getTag());
    }
}
