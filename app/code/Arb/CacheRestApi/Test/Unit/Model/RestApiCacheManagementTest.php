<?php

/**
 * This file consist of PHPUnit test case for class RestApiCacheManagement
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

namespace Arb\CacheRestApi\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Model\RestApiCacheManagement;
use Arb\CacheRestApi\Model\CacheType;
use Arb\CacheRestApi\Model\CacheIdentifierManager;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\Request\Http as RestRequest;
use Magento\Framework\Webapi\Rest\Response;
use Magento\Webapi\Controller\PathProcessor;
use Zend\Http\Headers;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\CacheRestApi\Model\RestApiCacheManagement
 */
class RestApiCacheManagementTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        //$this->cacheTypeMock = $this->createMock(CacheType::class);
        $this->restRequestMock = $this->createMock(RestRequest::class);
        $this->stateInterfaceMock = $this->createMock(StateInterface::class);
        $this->pathProcessorMock = $this->createMock(PathProcessor::class);
        $this->cacheIdentifierManagerMock = $this->createMock(CacheIdentifierManager::class);
        $this->responseMock = $this->createMock(Response::class);
        $this->cacheTagEntityManagerMock = $this->createMock(CacheTagEntityManager::class);
        $this->cacheTypeMock = $this->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
            ->setMethods(['load'])
            ->getMockForAbstractClass();
        // $this->_customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
        //                     ->disableOriginalConstructor()
        //                     ->setMethods([
        //                         "getById",
        //                         "getCustomAttribute",
        //                         "getValue"
        //                     ])
        //                     ->getMockForAbstractClass();
        // $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->testObject = $objectManager->getObject(RestApiCacheManagement::class, [
            'cacheTypeMock' =>  $this->cacheTypeMock,
            'restRequestMock' => $this->restRequestMock,
            'stateInterfaceMock' => $this->stateInterfaceMock,
            'pathProcessorMock' => $this->pathProcessorMock,
            'cacheIdentifierManagerMock' => $this->cacheIdentifierManagerMock,
            'responseMock' => $this->responseMock,
            'cacheTagEntityManagerMock' => $this->cacheTagEntityManagerMock
        ]);
    }

    public function testLoadCacheResult()
    {
        $cacheKey = '8739602554c7f3241958e3cc9b57fdecb474d508';
        $this->cacheIdentifierManagerMock->method('getRequestIdentifier')->willReturn($cacheKey);
        $this->cacheTypeMock->method('load')->with($cacheKey)->willReturn('');
        $this->assertEquals(null, $this->testObject->loadCacheResult());
    }

    public function testLoadCacheResultEmpty()
    {
       $this->assertEquals(null, $this->testObject->loadCacheResult());
    }

    // public function testIsCacheEnabled()
    // {
    //     //$this->stateInterfaceMock->method('load')->with($cacheKey)->willReturn('');
    //     $this->stateInterfaceMock->setEnabled('arb_cache_rest_api', false);
    //     //$this->assertEquals(null,$this->testObject->isCacheEnabled());
    //     $result = $this->testObject->isCacheEnabled();
    //     $this->assertInternalType('boolean', $result);
    // }
    
    public function testSaveCacheResult()
    {
        // $customerMock = $this->createMock(Customer::class);
         $cacheKey = '8739602554c7f3241958e3cc9b57fdecb474d508';
        $this->cacheIdentifierManagerMock->method('getRequestIdentifier')->willReturn($cacheKey);
        $this->cacheTagEntityManagerMock->method("getTags")->willReturn([1,2]);
        // //$this->customerAuthMock->method('getCustomer')->willReturn($customerMock);
        
        $this->assertEquals(null, $this->testObject->saveCacheResult($this->responseMock));
    }
}
