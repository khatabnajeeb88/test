<?php

/**
 * This file consist of PHPUnit test case for class CustomerAuth
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

namespace Arb\CacheRestApi\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Model\CustomerAuth;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Integration\Model\Oauth\TokenFactory;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Customer\Model\Customer;

/**
 * @covers \Arb\CacheRestApi\Model\CustomerAuth
 */
class CustomerAuthTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->request = $this->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
            ->setMethods(['getMethod','getHeader', 'getRequestUri'])
            ->getMockForAbstractClass();
        $this->_customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                "getById",
                                "getCustomAttribute",
                                "getValue"
                            ])
                            ->getMockForAbstractClass();
        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->tokenFactoryMock = $this->createMock(TokenFactory::class);
        $this->testObject = $objectManager->getObject(CustomerAuth::class, [
            'request' =>  $this->request,
            'customerRepository' => $this->_customerRepositoryMock,
            'tokenFactory' => $this->tokenFactoryMock
        ]);
    }

    public function testGetCustomer()
    {
        $customerMock = $this->createMock(Customer::class);

        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturn("");
        $this->request->method("getHeader")->willReturn("Bearer testetoken");
        //$this->customerAuthMock->method('getCustomer')->willReturn($customerMock);
        
        $this->assertEquals(null, $this->testObject->getCustomer());
    }
}
