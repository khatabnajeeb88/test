<?php

/**
 * This file consist of PHPUnit test case for class CacheTagEntityManager
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

namespace Arb\CacheRestApi\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\CacheRestApi\Model\CacheTagEntityManager
 */
class CacheTagEntityManagerTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        
        $this->testObject = $objectManager->getObject(CacheTagEntityManager::class, []);
    }

    public function testAddTags()
    {
        $arary = [];
        $this->assertEquals(null, $this->testObject->addTags($arary));
    }
    
    public function testGetTags()
    {
        $arary = [];
        $this->assertEquals($arary, $this->testObject->getTags());
    }
}
