<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Observer\CleanCache;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\CacheRestApi\Model\CacheType;
use Arb\CacheRestApi\Observer\CleanCache\FlushAllCache;

/**
 * Class FlushAllCacheTest for testing  FlushAllCache class
 * @covers \Arb\CacheRestApi\Observer\CleanCache\FlushAllCache
 */
class FlushAllCacheTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheType = $this->createMock(CacheType::class);
        $this->observerMock = $this->createPartialMock(
            Observer::class,
            ['getEvent',"getControllerAction","getResponse"]
        );
        $this->observerMock->method("getControllerAction")->willReturnSelf();
        $this->flushAllCache = $this->objectManager->getObject(FlushAllCache::class, [
            "cacheType"=>$this->cacheType
        ]);
    }

    public function testExecute()
    {
        $this->flushAllCache->execute($this->observerMock);
    }
}
