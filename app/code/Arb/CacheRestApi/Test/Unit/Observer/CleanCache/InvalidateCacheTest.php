<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Observer\CleanCache;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\CacheRestApi\Model\CacheType;
use Arb\CacheRestApi\Observer\CleanCache\InvalidateCache;
use Magento\Framework\App\Cache\TypeListInterface;
/**
 * Class InvalidateCacheTest for testing  InvalidateCache class
 * @covers \Arb\CacheRestApi\Observer\CleanCache\InvalidateCache
 */
class InvalidateCacheTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->typeListInterface = $this->createMock(TypeListInterface::class);
        $this->observerMock = $this->createPartialMock(
            Observer::class,
            ['getEvent',"getControllerAction","getResponse"]
        );
        $this->observerMock->method("getControllerAction")->willReturnSelf();
        $this->flushAllCache = $this->objectManager->getObject(InvalidateCache::class, [
            "typeListInterface"=>$this->typeListInterface
        ]);
    }

    public function testExecute()
    {
        $this->flushAllCache->execute($this->observerMock);
    }
}
