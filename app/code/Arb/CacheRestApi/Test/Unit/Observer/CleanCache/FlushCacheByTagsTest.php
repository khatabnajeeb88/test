<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Observer\CleanCache;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\CacheRestApi\Model\CacheType;
use Arb\CacheRestApi\Observer\CleanCache\FlushCacheByTags;
use Magento\Framework\App\Cache\Tag\Resolver as TagResolver;
use Arb\CacheRestApi\Model\RestApiCacheManagement;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\Event;
use Magento\Store\Model\Store;
/**
 * Class FlushCacheByTagsTest for testing  FlushCacheByTags class
 * @covers \Arb\CacheRestApi\Observer\CleanCache\FlushCacheByTags
 */
class FlushCacheByTagsTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheType = $this->createMock(CacheType::class);
        $this->restApiCacheManagement = $this->createMock(RestApiCacheManagement::class);
        $this->tagResolver = $this->createMock(TagResolver::class);
        $this->observerMock = $this->createPartialMock(
            Observer::class,
            ['getEvent',"getControllerAction","getResponse"]
        );
        $this->observerMock->method("getControllerAction")->willReturnSelf();
        $this->flushAllCache = $this->objectManager->getObject(FlushCacheByTags::class, [
            "cacheType"=>$this->cacheType,
            "restApiCacheManagement"=>$this->restApiCacheManagement,
            "tagResolver"=>$this->tagResolver
        ]);
    }

    public function testExecute()
    {
        $this->restApiCacheManagement->expects($this->once())->method('isCacheEnabled')->willReturn(1);
        $observedObject = $this->createMock(Store::class);
        $eventMock = $this->getMockBuilder(Event::class)
                ->setMethods(['getObject'])
                ->disableOriginalConstructor()
                ->getMock();
        $eventMock->expects($this->once())->method('getObject')->willReturn($observedObject);
        $this->observerMock->expects($this->once())->method('getEvent')->willReturn($eventMock);
           
        $this->tagResolver->expects($this->once())->method('getTags')->with($observedObject)->willReturn([1,2]);
        $this->flushAllCache->execute($this->observerMock);
    }
    
    public function testExecuteEmptyTag()
    {
        $this->restApiCacheManagement->expects($this->once())->method('isCacheEnabled')->willReturn(1);
        $observedObject = $this->createMock(Store::class);
        $eventMock = $this->getMockBuilder(Event::class)
                ->setMethods(['getObject'])
                ->disableOriginalConstructor()
                ->getMock();
        $eventMock->expects($this->once())->method('getObject')->willReturn($observedObject);
        $this->observerMock->expects($this->once())->method('getEvent')->willReturn($eventMock);
           
        $this->tagResolver->expects($this->once())->method('getTags')->with($observedObject)->willReturn([]);
        $this->flushAllCache->execute($this->observerMock);
    }
    
    public function testExecuteNoObject()
    {
        $this->restApiCacheManagement->expects($this->once())->method('isCacheEnabled')->willReturn(1);
        $observedObject = $this->createMock(Store::class);
        $eventMock = $this->getMockBuilder(Event::class)
                ->setMethods([])
                ->disableOriginalConstructor()
                ->getMock();
        $this->observerMock->expects($this->once())->method('getEvent')->willReturn($eventMock);
        $this->flushAllCache->execute($this->observerMock);
    }
}
