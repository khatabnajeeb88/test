<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Observer\EntityLoad;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Arb\CacheRestApi\Observer\EntityLoad\CatalogProductLoadAfter;
use Magento\Framework\Event;
use Magento\Store\Model\Store;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
/**
 * Class CatalogProductLoadAfterTest for testing  CatalogProductLoadAfter class
 * @covers \Arb\CacheRestApi\Observer\EntityLoad\CatalogProductLoadAfter
 */
class CatalogProductLoadAfterTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheTagEntityManager = $this->createMock(CacheTagEntityManager::class);
        $this->eventMock = $this->getMockBuilder(Observer::class)
            ->setMethods(["getProduct","setStoreId","getCategoryIds","setCategories","getCollection"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->flushAllCache = $this->objectManager->getObject(CatalogProductLoadAfter::class, [
            "cacheTagEntityManager"=>$this->cacheTagEntityManager
        ]);
    }

    public function testExecute()
    {
        $productCollectionMock = $this->getMockBuilder(Collection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productMock = $this->createMock(Product::class);
        $this->eventMock->method("getProduct")->willReturn($productMock);
        $productMock->expects($this->once())->method('getId')->willReturn(1);

        $this->flushAllCache->execute($this->eventMock);
    }
}
