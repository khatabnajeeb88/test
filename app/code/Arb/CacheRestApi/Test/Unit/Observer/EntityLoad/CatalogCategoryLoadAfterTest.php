<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Observer\EntityLoad;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Category;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Arb\CacheRestApi\Observer\EntityLoad\CatalogCategoryLoadAfter;
use Magento\Framework\Event;
use Magento\Store\Model\Store;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
/**
 * Class CatalogCategoryLoadAfterTest for testing  CatalogCategoryLoadAfter class
 * @covers \Arb\CacheRestApi\Observer\EntityLoad\CatalogCategoryLoadAfter
 */
class CatalogCategoryLoadAfterTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheTagEntityManager = $this->createMock(CacheTagEntityManager::class);
        // $this->observerMock = $this->createPartialMock(
        //     Observer::class,
        //     ['getEvent',"getControllerAction","getResponse"]
        // );
        $this->eventMock = $this->getMockBuilder(Observer::class)
            ->setMethods(["getCategory","setStoreId","getCategoryIds","setCategories","getCategoryCollection"])
            ->disableOriginalConstructor()
            ->getMock();
        //$this->observerMock->method("getControllerAction")->willReturnSelf();
        $this->flushAllCache = $this->objectManager->getObject(CatalogCategoryLoadAfter::class, [
            "cacheTagEntityManager"=>$this->cacheTagEntityManager
        ]);
    }

    public function testExecute()
    {
        $categoryCollectionMock = $this->getMockBuilder(Collection::class)
            ->setMethods(["getId"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventMock->method("getCategory")->willReturn($categoryCollectionMock);
        $categoryCollectionMock->method('getId')->willReturn(1);

        $this->flushAllCache->execute($this->eventMock);
    }
}
