<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Observer\EntityLoad;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Arb\CacheRestApi\Observer\EntityLoad\CatalogProductCollectionLoadAfter;
use Magento\Framework\Event;
use Magento\Store\Model\Store;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
/**
 * Class CatalogProductCollectionLoadAfterTest for testing  CatalogProductCollectionLoadAfter class
 * @covers \Arb\CacheRestApi\Observer\EntityLoad\CatalogProductCollectionLoadAfter
 */
class CatalogProductCollectionLoadAfterTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheTagEntityManager = $this->createMock(CacheTagEntityManager::class);
        $this->eventMock = $this->getMockBuilder(Observer::class)
            ->setMethods(["getProduct","setStoreId","getCategoryIds","setCategories","getCollection"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->flushAllCache = $this->objectManager->getObject(CatalogProductCollectionLoadAfter::class, [
            "cacheTagEntityManager"=>$this->cacheTagEntityManager
        ]);
    }

    public function testExecute()
    {
        $productCollectionMock = $this->getMockBuilder(Collection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventMock->method("getCollection")->willReturn($productCollectionMock);
        $productCollectionMock->method('getItems')->willReturn([1,2]);

        $this->flushAllCache->execute($this->eventMock);
    }
}
