<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Observer\EntityLoad;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Category;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Arb\CacheRestApi\Observer\EntityLoad\CatalogCategoryCollectionLoadAfter;
use Magento\Framework\Event;
use Magento\Store\Model\Store;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
/**
 * Class CatalogCategoryCollectionLoadAfterTest for testing  CatalogCategoryCollectionLoadAfter class
 * @covers \Arb\CacheRestApi\Observer\EntityLoad\CatalogCategoryCollectionLoadAfter
 */
class CatalogCategoryCollectionLoadAfterTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheTagEntityManager = $this->createMock(CacheTagEntityManager::class);
        // $this->observerMock = $this->createPartialMock(
        //     Observer::class,
        //     ['getEvent',"getControllerAction","getResponse"]
        // );
        $this->eventMock = $this->getMockBuilder(Observer::class)
            ->setMethods(["getProduct","setStoreId","getCategoryIds","setCategories","getCategoryCollection"])
            ->disableOriginalConstructor()
            ->getMock();
        //$this->observerMock->method("getControllerAction")->willReturnSelf();
        $this->flushAllCache = $this->objectManager->getObject(CatalogCategoryCollectionLoadAfter::class, [
            "cacheTagEntityManager"=>$this->cacheTagEntityManager
        ]);
    }

    public function testExecute()
    {
        $categoryCollectionMock = $this->getMockBuilder(Collection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventMock->method("getCategoryCollection")->willReturn($categoryCollectionMock);
        $categoryCollectionMock->method('getItems')->willReturn([1,2]);

        $this->flushAllCache->execute($this->eventMock);
    }
}
