<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Plugin\Magento\Framework;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Model\RestApiCacheManagement;
use Arb\CacheRestApi\Plugin\Magento\Framework\AppInterfacePlugin;
use Closure;
use Magento\Framework\AppInterface;
use Psr\Log\LoggerInterface;
/**
 * Class AppInterfacePluginTest for testing  AppInterfacePlugin class
 * @covers \Arb\CacheRestApi\Plugin\Magento\Framework\AppInterfacePlugin
 */
class AppInterfacePluginTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->restApiCacheManagement = $this->createMock(RestApiCacheManagement::class);
        $this->subject = $this->createMock(AppInterface::class);
        $this->closureMock = function () {
            return 'Expected';
        };


        $this->testObject = $this->objectManager->getObject(
            AppInterfacePlugin::class,
            [
                'restApiCacheManagement' => $this->restApiCacheManagement
            ]
        );
    }

    public function testAroundLaunchNotResponse()
    {
        $this->restApiCacheManagement->method('canProcessRequest')->willReturn(true);
        $this->restApiCacheManagement->method('loadCacheResult')->willReturn(true);
        $this->testObject->aroundLaunch($this->subject, $this->closureMock);
    }
    
    public function testAroundLaunch()
    {
        $this->restApiCacheManagement->method('canProcessRequest')->willReturn(true);
        $this->restApiCacheManagement->method('loadCacheResult')->willReturn(false);
        $this->restApiCacheManagement->method('saveCacheResult')->willReturnSelf();
        $this->testObject->aroundLaunch($this->subject, $this->closureMock);
    }
    
    public function testAroundLaunchReturnProceed()
    {
        $this->restApiCacheManagement->method('canProcessRequest')->willReturn(false);
        $this->testObject->aroundLaunch($this->subject, $this->closureMock);
    }
}
