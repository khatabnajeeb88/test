<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Plugin\Arb\ElasticSearch\Rewrite\Magento\ElasticsearchSearchAdapter;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Plugin\Arb\ElasticSearch\Rewrite\Magento\ElasticsearchSearchAdapter\ResponseFactoryPlugin;
use Magento\Catalog\Model\Product;
use Magento\Framework\Search\Response\QueryResponse;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
use Arb\ElasticSearch\Rewrite\Magento\Elasticsearch\SearchAdapter\ResponseFactory;
/**
 * Class ResponseFactoryPluginTest for testing  ResponseFactoryPlugin class
 * @covers \Arb\CacheRestApi\Plugin\Arb\ElasticSearch\Rewrite\Magento\ElasticsearchSearchAdapter\ResponseFactoryPlugin
 */
class ResponseFactoryPluginTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheType = $this->createMock(CacheTagEntityManager::class);
        $this->responseFactory = $this->createMock(ResponseFactory::class);
        $this->queryResponse = $this->createMock(QueryResponse::class);


        $this->testObject = $this->objectManager->getObject(
            ResponseFactoryPlugin::class,
            [
                'cacheType' => $this->cacheType
            ]
        );
    }

    public function testAfterCreate()
    {
        $tags = [Product::CACHE_TAG];
        $this->queryResponse->expects($this->any())
            ->method('getIterator')
            ->willReturn(new \ArrayIterator(['id'=>1]));
        $this->cacheType->expects($this->any())->method('addTags')->with($tags)->willReturnSelf();
        $this->testObject->afterCreate($this->responseFactory, ['id'=>1]);
    }
}
