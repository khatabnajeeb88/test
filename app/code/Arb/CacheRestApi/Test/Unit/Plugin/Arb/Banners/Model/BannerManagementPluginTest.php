<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Plugin\Arb\Banners\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Plugin\Arb\Banners\Model\BannerManagementPlugin;
use Arb\Banners\Model\BannerManagement;
use Arb\Banners\Model\Banners;
use Arb\CacheRestApi\Model\CacheTagEntityManager;
/**
 * Class BannerManagementPluginTest for testing  BannerManagementPlugin class
 * @covers \Arb\CacheRestApi\Plugin\Arb\Banners\Model\BannerManagementPlugin
 */
class BannerManagementPluginTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheType = $this->createMock(CacheTagEntityManager::class);
        $this->responseFactory = $this->createMock(BannerManagement::class);


        $this->testObject = $this->objectManager->getObject(
            BannerManagementPlugin::class,
            [
                'cacheType' => $this->cacheType
            ]
        );
    }

    public function testAfterGetBanners()
    {
        $tags = [];
        
        $this->cacheType->expects($this->any())->method('addTags')->with($tags)->willReturnSelf();
        $this->testObject->afterGetBanners($this->responseFactory, ["id"=>1]);
    }
}
