<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */
namespace Arb\CacheRestApi\Test\Unit\Plugin\Arb\Vouchers\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\CacheRestApi\Plugin\Arb\Vouchers\Helper\DataPlugin;
use Arb\CacheRestApi\Model\CacheType;
use Arb\Vouchers\Helper\Data;
/**
 * Class DataPluginTest for testing  DataPlugin class
 * @covers \Arb\CacheRestApi\Plugin\Arb\Vouchers\Helper\DataPlugin
 */
class DataPluginTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->cacheType = $this->createMock(CacheType::class);
        $this->Data = $this->createMock(Data::class);
        $this->closureMock = function () {
            return 'Expected';
        };


        $this->testObject = $this->objectManager->getObject(
            DataPlugin::class,
            [
                'cacheType' => $this->cacheType
            ]
        );
    }

    public function testAfterUpdateProductStock()
    {
        $this->cacheType->expects($this->once())->method('clean')->willReturnSelf();
        $this->testObject->afterUpdateProductStock($this->Data, []);
    }
    
}
