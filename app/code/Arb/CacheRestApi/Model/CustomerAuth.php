<?php
/**
 * Get current customer object
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Integration\Model\Oauth\TokenFactory;

/**
 * Class CustomerAuth
 */
class CustomerAuth
{
    private $customer;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var TokenFactory
     */
    private $tokenFactory;

    /**
     * CustomerAuth constructor.
     * @param RequestInterface $request
     * @param CustomerRepositoryInterface $customerRepository
     * @param TokenFactory $tokenFactory
     */
    public function __construct(
        RequestInterface $request,
        CustomerRepositoryInterface $customerRepository,
        TokenFactory $tokenFactory
    ) {
        $this->request = $request;
        $this->customerRepository = $customerRepository;
        $this->tokenFactory = $tokenFactory;
    }

    /**
     * Get customer by oauth token
     * @return CustomerInterface|false
     */
    public function getCustomer()
    {
        if ($this->customer) {
            $authHeader = $this->request->getHeader('Authorization');
            if (preg_match('/^bearer\s+\"?(.+?)\"?\s*$/i', $authHeader, $matches)) {
                $token = $this->tokenFactory->create()->loadByToken($matches[1]);
                if ($token->getId() && !$token->getRevoked() && $token->getCustomerId()) {
                    $customer = $this->customerRepository->getById($token->getCustomerId());
                    $this->customer = $customer;
                }
            }
        }

        return $this->customer;
    }
}
