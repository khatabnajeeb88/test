<?php
/**
 * Cache identified manager creates unique id for the request
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Model;

use Magento\Customer\Api\Data\GroupInterface;
use Magento\Framework\App\RequestInterface;

/**
 * Class CacheIdentifierManager
 */
class CacheIdentifierManager
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var CustomerAuth
     */
    private $customerAuth;

    /**
     * CacheIdentifierManager constructor.
     * @param RequestInterface $request
     * @param CustomerAuth $customerAuth
     */
    public function __construct(
        RequestInterface $request,
        CustomerAuth $customerAuth
    ) {
        $this->request = $request;
        $this->customerAuth = $customerAuth;
    }

    /**
     * @return array
     */
    public function getRequestIdentifier(): array
    {
        $customer = $this->customerAuth->getCustomer();
        $customerGroupId = $customer ? $customer->getGroupId() : GroupInterface::NOT_LOGGED_IN_ID;
        return [
            $customerGroupId,
            $this->request->getMethod(),
            $this->request->getRequestUri(),
            $this->request->getHeader('Content-Type'),
        ];
    }
}
