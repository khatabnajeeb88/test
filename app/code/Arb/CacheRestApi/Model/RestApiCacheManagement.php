<?php
/**
 * Cache manager responsible fro loading and saving cached API response
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Model;

use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\Request\Http as RestRequest;
use Magento\Framework\Webapi\Rest\Response;
use Magento\Webapi\Controller\PathProcessor;
use Zend\Http\Headers;

/**
 * Class RestApiCacheManagement
 */
class RestApiCacheManagement
{
    private const CACHE_TTL = 86400;

    /**
     * @var StateInterface
     */
    private $state;

    /**
     * @var CacheType
     */
    private $cacheType;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var CacheIdentifierManager
     */
    private $cacheIdentifierManager;

    /**
     * @var PathProcessor
     */
    private $pathProcessor;

    /**
     * @var RestRequest
     */
    private $request;

    /**
     * @var array
     */
    private $allowedCachedRequests;

    /**
     * @var CacheTagEntityManager
     */
    private $cacheTagEntityManager;

    /**
     * RestApiCacheManagement constructor.
     * @param CacheType $cacheType
     * @param RestRequest $request
     * @param StateInterface $state
     * @param PathProcessor $pathProcessor
     * @param CacheIdentifierManager $cacheIdentifierManager
     * @param Response $response
     * @param CacheTagEntityManager $cacheTagEntityManager
     * @param array $allowedCachedRequests
     */
    public function __construct(
        CacheType $cacheType,
        RestRequest $request,
        StateInterface $state,
        PathProcessor $pathProcessor,
        CacheIdentifierManager $cacheIdentifierManager,
        Response $response,
        CacheTagEntityManager $cacheTagEntityManager,
        array $allowedCachedRequests = []
    ) {
        $this->response = $response;
        $this->request = $request;
        $this->state = $state;
        $this->pathProcessor = $pathProcessor;
        $this->cacheIdentifierManager = $cacheIdentifierManager;
        $this->cacheType = $cacheType;
        $this->cacheTagEntityManager = $cacheTagEntityManager;
        $this->allowedCachedRequests = $allowedCachedRequests;
    }

    /**
     * @return bool|Response
     */
    public function loadCacheResult()
    {
        $cacheKey = $this->getCacheKey();
        if (!$this->cacheType->test($cacheKey)) {
            return false;
        }

        $serializedCachedData = $this->cacheType->load($cacheKey);
        if (!$serializedCachedData) {
            return false;
        }
        $cacheData = unserialize($serializedCachedData);

        $this->response->setHttpResponseCode($cacheData['code']);
        $this->response->setHeaders(Headers::fromString($cacheData['headers']));
        $this->response->setBody($cacheData['body']);

        return $this->response;
    }

    /**
     * @param $response
     */
    public function saveCacheResult($response): void
    {
        $cacheKey = $this->getCacheKey();
        $tags = $this->cacheTagEntityManager->getTags();
        if (empty($tags)) {
            return;
        }
        $tags[] = CacheType::CACHE_TAG;

        $responseBody = $response->getBody();
        $responseCode = $response->getStatusCode();
        $responseHeaders = $response->getHeaders()->toString();
        $cacheData = [
            'code'    => $responseCode,
            'headers' => $responseHeaders,
            'body'    => $responseBody,
        ];

        $this->cacheType->save(serialize($cacheData), $cacheKey, $tags, static::CACHE_TTL);
    }

    /**
     * @return bool
     */
    public function canProcessRequest(): bool
    {
        if (!$this->canCacheRequest()) {
            return false;
        }

        if (!$this->isCacheEnabled()) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    private function getCacheKey(): string
    {
        return sha1(
            serialize($this->cacheIdentifierManager->getRequestIdentifier())
        );
    }

    /**
     * @return bool
     */
    public function isCacheEnabled(): bool
    {
        return $this->state->isEnabled(CacheType::TYPE_IDENTIFIER);
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function canCacheRequest(): bool
    {
        if (!$this->request->isGet()) {
            return false;
        }

        $currentRequestPath = $this->pathProcessor->process($this->request->getPathInfo());
        foreach ($this->allowedCachedRequests as $allowedCachedRequests) {
            if (strpos($currentRequestPath, $allowedCachedRequests) === 0) {
                return true;
            }
        }

        return false;
    }
}
