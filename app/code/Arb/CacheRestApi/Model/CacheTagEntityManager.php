<?php
/**
 * Cache tags manager object
 *
 * @category Arb
 * @package Arb_CacheRestApi
 * @author Arb Magento Team
 *
 */

declare(strict_types=1);

namespace Arb\CacheRestApi\Model;

/**
 * Class CacheTagEntityManager
 */
class CacheTagEntityManager
{
    /**
     * @var array
     */
    private $tags = [];

    /**
     * @param array $tags
     */
    public function addTags(array $tags): void
    {
        $this->tags = array_merge($this->tags, $tags);
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return array_flip(array_flip($this->tags));
    }
}
