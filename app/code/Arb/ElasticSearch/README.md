## Synopsis
An extention which overrides ElasticSearch Magento adapters to modify Magento to ElasticSearch indexing and res

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Dependency injection configuration [di.xml](etc/di.xml)
4. Search container configuration [search_request.xml](etc/search_request.xml)


## Tests
Unit tests could be found in the [Test/Unit](Test/Unit) directory.


