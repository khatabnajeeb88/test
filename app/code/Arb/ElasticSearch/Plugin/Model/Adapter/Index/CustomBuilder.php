<?php
/**
 * ElasticSearch query builder
 *
 * @category Arb
 * @package Arb_ElasticSearch
 * @author Arb Magento Team
 *
 */
namespace Arb\ElasticSearch\Plugin\Model\Adapter\Index;

class CustomBuilder
{

    /**
     * After build plugin
     *
     * @param \Magento\Elasticsearch\Model\Adapter\Index\Builder $subject
     * @param array $result
     * @return void
     */
    public function afterBuild(\Magento\Elasticsearch\Model\Adapter\Index\Builder $subject, $result)
    {
        
        $result['analysis']['analyzer']['autocomplete'] = [
            'tokenizer' => 'autocomplete',
            'filter' => ['lowercase','split_on_numerics']
        ];

        $result['analysis']['analyzer']['autocomplete_search'] = [
            'tokenizer' =>'lowercase'
        ];

        $result['analysis']['tokenizer']['autocomplete'] = [
        'type' => 'edge_ngram',
        'min_gram' => 3,
        'max_gram' => 12,
        "token_chars"=> [
            "letter"
          ]
        ];

        $result['analysis']['filter']['split_on_numerics'] = [
            'type' => 'word_delimiter',
            'split_on_numerics' => true,
            'split_on_case_change' => true,
            'generate_word_parts' => true,
            'generate_number_parts' => true,
            'catenate_all' => false
        ];

        return $result;
    }
}
