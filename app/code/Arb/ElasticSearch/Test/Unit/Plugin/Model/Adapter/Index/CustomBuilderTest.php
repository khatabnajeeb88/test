<?php
/**
 * ElasticSearch query builder
 *
 * @category Arb
 * @package Arb_ElasticSearch
 * @author Arb Magento Team
 *
 */
namespace Arb\ElasticSearch\Test\Unit\Plugin\Model\Adapter\Index;


use Arb\ElasticSearch\Plugin\Model\Adapter\Index\CustomBuilder;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Elasticsearch\Model\Adapter\Index\Builder as Builder;
use ReflectionException;

class CustomBuilderTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Item
     */
    private $testObject;


    /**
     * Setting up necessary mocks
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->testObject = $objectManager->getObject(CustomBuilder::class, []);
    }

    /**
     * After build plugin
     *
     * @param \Magento\Elasticsearch\Model\Adapter\Index\Builder $subject
     * @param array $result
     * @return void
     */
    public function testAfterBuild()
    {
        $itemModelMock = $this->createMock(Builder::class);
		$this->testObject->afterBuild($itemModelMock, []);
    }
}
