<?php
/**
 * ResponseFactory php unit test
 *
 * @category Arb
 * @package Arb_ElasticSearch
 * @author Arb Magento Team
 *
 */

namespace Arb\ElasticSearch\Test\Unit\SearchAdapter;

use Magento\Elasticsearch\SearchAdapter\ResponseFactory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Arb\ElasticSearch\Rewrite\Magento\Elasticsearch\SearchAdapter\ResponseFactory as ArbResponseFactory;

class ResponseFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ResponseFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $model;

    /**
     * @var \Magento\Elasticsearch\SearchAdapter\DocumentFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $documentFactory;

    /**
     * @var \Magento\Elasticsearch\SearchAdapter\AggregationFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $aggregationFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $objectManager;

    /**
     * Set up test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        $this->documentFactory = $this->getMockBuilder(\Magento\Elasticsearch\SearchAdapter\DocumentFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->aggregationFactory = $this->getMockBuilder(
            \Magento\Elasticsearch\SearchAdapter\AggregationFactory::class
        )
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->objectManager = $this->createMock(\Magento\Framework\ObjectManagerInterface::class);

        $objectManagerHelper = new ObjectManagerHelper($this);
        $this->model = $objectManagerHelper->getObject(
            ArbResponseFactory::class,
            [
                'objectManager' => $this->objectManager,
                'documentFactory' => $this->documentFactory,
                'aggregationFactory' => $this->aggregationFactory
            ]
        );
    }

    public function testCreate()
    {
        $documents[]["_source"]["product_data"] = [
            ['title' => 'oneTitle', 'description' => 'oneDescription'],
            ['title' => 'twoTitle', 'description' => 'twoDescription'],
        ];
        $aggregations = [
            'aggregation1' => [
                'itemOne' => 10,
                'itemTwo' => 20,
            ],
            'aggregation2' => [
                'itemOne' => 5,
                'itemTwo' => 45,
            ]
        ];
        $rawResponse = ['documents' => $documents, 'aggregations' => $aggregations, 'total' => 2];

        $exceptedResponse = [
            
            'total' => 1
        ];

        
        $this->aggregationFactory->expects($this->at(0))->method('create')
            ->will($this->returnValue('aggregationsData'));

        $this->objectManager->expects($this->any())->method('create')
            ->with(
                $this->equalTo(\Magento\Framework\Search\Response\QueryResponse::class),
                $this->equalTo([
                    'documents' => [[
                ['title' => 'oneTitle', 'description' => 'oneDescription'],
                ['title' => 'twoTitle', 'description' => 'twoDescription'],
                    ]],
                    'aggregations' => 'aggregationsData',
                    'total' => 2
                ])
            )
            ->will($this->returnValue('QueryResponseObject'));

        $result = $this->model->create($rawResponse);
        $this->assertEquals('QueryResponseObject', $result);
    }
}
