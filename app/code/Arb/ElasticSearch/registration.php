<?php
/**
 * ElasticSearch override module registration file
 *
 * @category Arb
 * @package Arb_ElasticSearch
 * @author Arb Magento Team
 *
 */

// @codeCoverageIgnoreStart
/** it is a default magento module registration code so no code coverage
 * is required as default magneto also not providing it
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_ElasticSearch',
    __DIR__
);

// @codeCoverageIgnoreEnd
