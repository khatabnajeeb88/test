<?php

/**
 * ElasticSearch override indexer logic
 *
 * @category Arb
 * @package Arb_ElasticSearch
 * @author Arb Magento Team
 *
 */

namespace Arb\ElasticSearch\Rewrite\Magento\Elasticsearch\Model\Adapter\BatchDataMapper;

use Magento\CatalogSearch\Model\Indexer\Fulltext\Action\DataProvider;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Elasticsearch\Model\Adapter\Document\Builder;
use Magento\Elasticsearch\Model\Adapter\FieldMapperInterface;
use Magento\Elasticsearch\Model\Adapter\BatchDataMapperInterface;
use Magento\Elasticsearch\Model\Adapter\FieldType\Date as DateFieldType;
use Magento\AdvancedSearch\Model\Adapter\DataMapper\AdditionalFieldsProviderInterface;
use Magento\Eav\Api\Data\AttributeOptionInterface;
use Magento\Elasticsearch\Model\Adapter\BatchDataMapper\ProductDataMapper as ParentProductDataMapper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Arb\API\Model\ProductsFactory;
use Magento\Framework\Webapi\ServiceOutputProcessor;
use Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Exception\LocalizedException;

/**
 * Product data mapping class override for indexing
 */
class ProductDataMapper extends ParentProductDataMapper
{
    /**
     *  Product listing getList class name
     */
    const GET_LIST_CLASS_NAME = 'Arb\API\Api\ProductsInterface';

    /**
     * Get list class function name
     */
    const GET_LIST_FUNCTION = 'getList';

    /**
     * @var AttributeOptionInterface[]
     */
    private $attributeOptionsCache;

    /**
     * @var Builder
     */
    private $builder;

    /**
     * @var FieldMapperInterface
     */
    private $fieldMapper;

    /**
     * @var DateFieldType
     */
    private $dateFieldType;

    /**
     * @var array
     */
    private $excludedAttributes;

    /**
     * @var AdditionalFieldsProviderInterface
     */
    private $additionalFieldsProvider;

    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var ProductsFactory
     */
    protected $productsFactory;

    /**
     * @var ServiceOutputProcessor
     */
    protected $serviceOutputProcessor;

    /**
     * @var Json
     */
    protected $json;

    /**
     * List of attributes which will be skipped during mapping
     *
     * @var string[]
     */
    private $defaultExcludedAttributes = [
        'price',
        'media_gallery',
        'tier_price',
        'quantity_and_stock_status',
        'media_gallery',
        'giftcard_amounts',
    ];

    /**
     * @var string[]
     */
    private $attributesExcludedFromMerge = [
        'status',
        'visibility',
        'tax_class_id'
    ];

    /**
     * ProductDataMapper class override
     *
     * @param Builder $builder
     * @param FieldMapperInterface $fieldMapper
     * @param DateFieldType $dateFieldType
     * @param AdditionalFieldsProviderInterface $additionalFieldsProvider
     * @param DataProvider $dataProvider
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductsFactory $productsFactory
     * @param ServiceOutputProcessor $serviceOutputProcessor
     * @param Json $json
     * @param array $excludedAttributes
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Builder $builder,
        FieldMapperInterface $fieldMapper,
        DateFieldType $dateFieldType,
        AdditionalFieldsProviderInterface $additionalFieldsProvider,
        DataProvider $dataProvider,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductsFactory $productsFactory,
        ServiceOutputProcessor $serviceOutputProcessor,
        Json $json,
        array $excludedAttributes = []
    ) {
        $this->builder = $builder;
        $this->fieldMapper = $fieldMapper;
        $this->dateFieldType = $dateFieldType;
        $this->excludedAttributes = array_merge($this->defaultExcludedAttributes, $excludedAttributes);
        $this->additionalFieldsProvider = $additionalFieldsProvider;
        $this->dataProvider = $dataProvider;
        $this->attributeOptionsCache = [];
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->serviceOutputProcessor = $serviceOutputProcessor;
        $this->productsFactory = $productsFactory;
        $this->serviceOutputProcessor = $serviceOutputProcessor;
        $this->json = $json;
    }

    /**
     * Map index data for using in search engine metadata
     *
     * @param array $documentData
     * @param int $storeId
     * @param array $context
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */

    public function map(array $documentData, $storeId, array $context = [])
    {
        $documents = [];
        foreach ($documentData as $productId => $indexData) {
            $this->builder->addField('store_id', $storeId);
            $productIndexData = $this->convertToProductData($productId, $indexData, $storeId);

            $this->searchCriteriaBuilder->addFilter('entity_id', $productId, 'eq');
            $this->searchCriteriaBuilder->addFilter('store_id', $storeId, 'eq');
            $productData = $this->productsFactory->create()->getList($this->searchCriteriaBuilder->create());
            $productDetails = current($productData->getItems());
            $productArrayData = [];
            //Getting category names for searching purpose
            if (!empty($productDetails)) {
                $productArrayData = $this->serviceOutputProcessor->process(
                    $productData,
                    self::GET_LIST_CLASS_NAME,
                    self::GET_LIST_FUNCTION
                );
            }

            if (!empty($productArrayData['items'][0])) {
                /**
                 * Converting value array to json to avoid mapping issue in elastic search
                 */
                if (!empty($productArrayData['items'][0]['custom_attributes'])) {
                    $productArrayData['items'][0]['custom_attributes'] =
                        $this->json->unserialize(
                            $this->json->serialize(
                                $productArrayData['items'][0]['custom_attributes']
                            )
                        );

                    $normalizedCustomAttribute = [];
                    $i = 0;

                    foreach ($productArrayData['items'][0]['custom_attributes'] as $customAttribute) {
                        $normalizedCustomAttribute[$i]['attribute_code'] = $customAttribute['attribute_code'];

                        if (is_array($customAttribute['value'])) {
                            $stringValueData =  $this->json->serialize($customAttribute['value']);
                            $normalizedCustomAttribute[$i]['value'] = $stringValueData;
                        } else {
                            $normalizedCustomAttribute[$i]['value'] = $customAttribute['value'];
                        }
                        $i++;
                    }

                    $productArrayData['items'][0]['custom_attributes'] = $normalizedCustomAttribute;
                }

                foreach ($productIndexData as $attributeCode => $value) {                    
                    if($attributeCode == 'name'){
                        if(is_array($value)) {
                            foreach($value as $key => $valueData) {
                                $value[$key] = strtolower($valueData);  
                            }
                        } else{
                            $value  = strtolower($value);
                        }
                    }

                    // Prepare processing attribute info
                    if (strpos($attributeCode, '_value') !== false) {
                        $this->builder->addField($attributeCode, $value);
                        continue;
                    }

                    $this->builder->addField(
                        $this->fieldMapper->getFieldName(
                            $attributeCode,
                            $context
                        ),
                        $value
                    );
                }

                //Indexing seller id field
                $this->builder->addField('seller_id', $productDetails->getSellerId());
                //Indexing product type id field
                $this->builder->addField('product_type_id', $productDetails->getTypeId());
                $price = $this->getProductPrice($productDetails);
                $this->builder->addField('product_price', $price);
                //Indexing product data as per product getList data
                $this->builder->addField('product_data', $productArrayData['items'][0]);
            }
            $documents[$productId] = $this->builder->build();
        }

        $productIds = array_keys($documentData);
        foreach ($this->additionalFieldsProvider->getFields($productIds, $storeId) as $productId => $fields) {
            $documents[$productId] = array_merge_recursive(
                $documents[$productId],
                $this->builder->addFields($fields)->build()
            );
        }
        return $documents;
    }

    /**
     * Convert raw data retrieved from source tables to human-readable format.
     *
     * @param int $productId
     * @param array $indexData
     * @param int $storeId
     * @return array
     */
    private function convertToProductData(int $productId, array $indexData, int $storeId): array
    {
        $productAttributes = [];

        if (isset($indexData['options'])) {
            // cover case with "options"
            // see \Magento\CatalogSearch\Model\Indexer\Fulltext\Action\DataProvider::prepareProductIndex
            $productAttributes['options'] = $indexData['options'];
            unset($indexData['options']);
        }

        foreach ($indexData as $attributeId => $attributeValues) {
            $attribute = $this->dataProvider->getSearchableAttribute($attributeId);
            if (in_array($attribute->getAttributeCode(), $this->excludedAttributes, true)) {
                continue;
            }

            if (!\is_array($attributeValues)) {
                $attributeValues = [$productId => $attributeValues];
            }
            $attributeValues = $this->prepareAttributeValues($productId, $attribute, $attributeValues, $storeId);
            $productAttributes += $this->convertAttribute($attribute, $attributeValues);
        }

        return $productAttributes;
    }

    /**
     * Convert data for attribute, add {attribute_code}_value for searchable attributes, that contain actual value.
     *
     * @param Attribute $attribute
     * @param array $attributeValues
     * @return array
     */
    private function convertAttribute(Attribute $attribute, array $attributeValues): array
    {
        $productAttributes = [];

        $retrievedValue = $this->retrieveFieldValue($attributeValues);
        if ($retrievedValue) {
            $productAttributes[$attribute->getAttributeCode()] = $retrievedValue;

            if ($attribute->getIsSearchable()) {
                $attributeLabels = $this->getValuesLabels($attribute, $attributeValues);
                $retrievedLabel = $this->retrieveFieldValue($attributeLabels);
                if ($retrievedLabel) {
                    $productAttributes[$attribute->getAttributeCode() . '_value'] = $retrievedLabel;
                }
            }
        }

        return $productAttributes;
    }

    /**
     * Prepare attribute values.
     *
     * @param int $productId
     * @param Attribute $attribute
     * @param array $attributeValues
     * @param int $storeId
     * @return array
     */
    private function prepareAttributeValues(
        int $productId,
        Attribute $attribute,
        array $attributeValues,
        int $storeId
    ): array {
        if (in_array($attribute->getAttributeCode(), $this->attributesExcludedFromMerge, true)) {
            $attributeValues = [
                $productId => $attributeValues[$productId] ?? '',
            ];
        }

        if ($attribute->getFrontendInput() === 'multiselect') {
            $attributeValues = $this->prepareMultiselectValues($attributeValues);
        }

        if ($this->isAttributeDate($attribute)) {
            foreach ($attributeValues as $key => $attributeValue) {
                $attributeValues[$key] = $this->dateFieldType->formatDate($storeId, $attributeValue);
            }
        }

        return $attributeValues;
    }

    /**
     * Prepare multiselect values.
     *
     * @param array $values
     * @return array
     */
    private function prepareMultiselectValues(array $values): array
    {
        return \array_merge(...\array_map(function (string $value) {
            return \explode(',', $value);
        }, $values));
    }

    /**
     * Is attribute date.
     *
     * @param Attribute $attribute
     * @return bool
     */
    private function isAttributeDate(Attribute $attribute): bool
    {
        return $attribute->getFrontendInput() === 'date'
            || in_array($attribute->getBackendType(), ['datetime', 'timestamp'], true);
    }

    /**
     * Get values labels.
     *
     * @param Attribute $attribute
     * @param array $attributeValues
     * @return array
     */
    private function getValuesLabels(Attribute $attribute, array $attributeValues): array
    {
        $attributeLabels = [];

        $options = $this->getAttributeOptions($attribute);
        if (empty($options)) {
            return $attributeLabels;
        }

        foreach ($options as $option) {
            if (\in_array($option->getValue(), $attributeValues)) {
                $attributeLabels[] = $option->getLabel();
            }
        }

        return $attributeLabels;
    }

    /**
     * Retrieve options for attribute
     *
     * @param Attribute $attribute
     * @return array
     */
    private function getAttributeOptions(Attribute $attribute): array
    {
        if (!isset($this->attributeOptionsCache[$attribute->getId()])) {
            $options = $attribute->getOptions() ?? [];
            $this->attributeOptionsCache[$attribute->getId()] = $options;
        }

        return $this->attributeOptionsCache[$attribute->getId()];
    }

    /**
     * Retrieve value for field. If field have only one value this method return it.
     * Otherwise will be returned array of these values.
     * Note: array of values must have index keys, not as associative array.
     *
     * @param array $values
     * @return array|string
     */
    private function retrieveFieldValue(array $values)
    {
        $values = \array_filter(\array_unique($values));

        return count($values) === 1 ? \array_shift($values) : \array_values($values);
    }

    private function getProductPrice($productDetails) {
        $price = 0;
        if($productDetails->getTypeId() == 'configurable'){
            $_children = $productDetails->getTypeInstance()->getUsedProducts($productDetails);
            foreach ($_children as $child){
                $childrenPrices[] = $child->getPrice();
            }             
            return min($childrenPrices);
        } else{
 
            $orgprice = $productDetails->getPrice();
            $specialprice = $productDetails->getSpecialPrice();
            $specialfromdate = $productDetails->getSpecialFromDate();
            $specialtodate = $productDetails->getSpecialToDate();
            $today = time();
            if (!$specialprice) {
                $price = $orgprice;
            }
            if ($specialprice < $orgprice) {
                if (
                    (is_null($specialfromdate) && is_null($specialtodate)) || 
                    ($today >= strtotime($specialfromdate) && is_null($specialtodate)) || 
                    ($today <= strtotime($specialtodate) && is_null($specialfromdate)) || 
                    ($today >= strtotime($specialfromdate) && $today <= strtotime($specialtodate))
                ) {
                    $price = $specialprice;
                }
            }
            return $price;        
        }
    }
}
