<?php
/**
 * ElasticSearch override response logic
 *
 * @category Arb
 * @package Arb_ElasticSearch
 * @author Arb Magento Team
 *
 */
 namespace Arb\ElasticSearch\Rewrite\Magento\Elasticsearch\SearchAdapter;

 use Magento\Elasticsearch\SearchAdapter\ResponseFactory as ParentResponseFactory;

 /**
  * ResponseFactory class to override core responsefactory
  */
class ResponseFactory extends ParentResponseFactory
{

    /**
     * Create Query Response instance
     *
     * @param array $response
     * @return \Magento\Framework\Search\Response\QueryResponse
     * @since 100.1.0
     */
    public function create($response)
    {
        $documents = [];
        foreach ($response['documents'] as $rawDocument) {
            $documents[] = $rawDocument['_source'];
        }
        
        /**
         * Custom data indexed data render logic
         */
        $productsData = [];
        $count = 0;
        foreach ($documents as $productData) {
            if (!empty($productData['product_data'])) {
                $productsData[] = $productData['product_data'];
            } else {
                $count++;
            }
        }

        /** @var \Magento\Framework\Search\Response\Aggregation $aggregations */
        $aggregations = $this->aggregationFactory->create($response['aggregations']);
        return $this->objectManager->create(
            \Magento\Framework\Search\Response\QueryResponse::class,
            [
                'documents' => $productsData,
                'aggregations' => $aggregations,
                'total' => $response['total'] - $count  //To get correct total count 
            ]
        );
    }
}
