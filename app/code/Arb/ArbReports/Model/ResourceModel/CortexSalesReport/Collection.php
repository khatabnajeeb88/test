<?php
/**
 * @category Arb
 * @package Arb_ArbReports
 * @author Arb Magento Team
 */

namespace Arb\ArbReports\Model\ResourceModel\CortexSalesReport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'arb_cortex_sales_report_collection';
    protected $_eventObject = 'cortex_sales_report_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\ArbReports\Model\CortexSalesReport::class,
            \Arb\ArbReports\Model\ResourceModel\CortexSalesReport::class
        );
    }
}
