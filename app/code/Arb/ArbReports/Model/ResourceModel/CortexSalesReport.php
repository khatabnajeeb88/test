<?php
/**
 * @category Arb
 * @package Arb_ArbReports
 * @author Arb Magento Team
 */
namespace Arb\ArbReports\Model\ResourceModel;


class CortexSalesReport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * CortexSalesReport constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     *
     * @return $this
     */
    protected function _construct()
    {
        $this->_init('arb_cortex_sales_report', 'id');
    }

}
