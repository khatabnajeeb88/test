<?php
/**
 * @category Arb
 * @package Arb_ArbReports
 * @author Arb Magento Team
 */


namespace Arb\ArbReports\Model\ResourceModel\MerchantSalesReport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'arb_merchant_sales_report_collection';
    protected $_eventObject = 'merchant_sales_report_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\ArbReports\Model\MerchantSalesReport::class,
            \Arb\ArbReports\Model\ResourceModel\MerchantSalesReport::class);

    }

}
