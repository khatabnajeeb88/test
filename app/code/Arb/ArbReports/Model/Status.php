<?php

namespace Arb\ArbReports\Model;

use Webkul\Marketplace\Block\Sellerlist as sellerBlock;
use Webkul\Marketplace\Model\ResourceModel\Seller\Collection as sellerCollection;

class Status extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var sellerCollection
     */
    private $sellerCollection;
    /**
     * @var sellerBlock
     */
    private $sellerBlock;

    /**
     * @param StoreRepository      $storeRepository
     */
    public function __construct(
       // StoreRepository $storeRepository,
        sellerCollection $sellerCollection,
        sellerBlock $sellerBlock
    ) {
        //  $this->_storeRepository = $storeRepository;
        $this->sellerCollection = $sellerCollection;
        $this->sellerBlock = $sellerBlock;
    }

    public function toOptionArray()
    {
        $merchants = $this->sellerBlock->getSellerCollection();

        foreach ($merchants as $store) {
            $merchantList[] = [
                "value" =>   $store["seller_id"],
                "label"=> $store["shop_title"],
            ];
        }
        return $merchantList;
    }
}
