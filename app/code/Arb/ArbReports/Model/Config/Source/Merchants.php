<?php

namespace Arb\ArbReports\Model\Config\Source;

use Arb\ArbReports\Model\ResourceModel\CortexSalesReport\CollectionFactory as CortexSalesReportCollectionFactory;

class Merchants implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var CortexSalesReportCollectionFactory
     */
    protected $cortexSalesReportCollectionFactory;

    /**
     * Merchants constructor.
     * @param CortexSalesReportCollectionFactory $cortexSalesReportCollectionFactory
     */
    public function __construct(
        CortexSalesReportCollectionFactory $cortexSalesReportCollectionFactory
    ) {
        $this->cortexSalesReportCollectionFactory = $cortexSalesReportCollectionFactory;
    }

    public function toOptionArray()
    {
        $merchants = $this->cortexSalesReportCollectionFactory->create()
            ->distinct(true)
            ->addFieldToSelect('merchant_name');

        $data = [
            ['value' => '', 'label' => __('All Merchants')],
        ];

        foreach ($merchants as $merchant) {
            $data[] = ['value' => $merchant->getData('merchant_name'), 'label' => $merchant->getData('merchant_name')];
        }

        return $data;
    }
}
