<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Arb\ArbReports\Model\Export;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Model\Export\ConvertToCsv as ConvertToCsvParent;
use Magento\Ui\Model\Export\MetadataProvider;

/**
 * Class ConvertToCsv
 */
/**
 * @codeCoverageIgnore
 */
class ConvertToCsv extends ConvertToCsvParent
{

    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var DirectoryList
     */
    protected $directory;

    /**
     * @var MetadataProvider
     */
    protected $metadataProvider;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var \Arb\ArbSalesReport\Helper\Data
     */
    private $helper;

    const MERCHANT_SALES_REPORT = "arb_reports_merchantsalesreport";

    const INVENTORY_REPORT = "arb_reports_inventoryreport";

    /**
     * @var \Arb\ArbSalesReport\Helper\MerchantSalesData
     */
    private $merchantSaleshelper;
    /**
     * @var \Arb\ArbReports\Helper\InventoryData
     */
    private $inventoryData;

    /**
     * @param Filesystem $filesystem
     * @param Filter $filter
     * @param MetadataProvider $metadataProvider
     * @param Http $request
     * @param \Arb\ArbReports\Helper\SalesData $helper
     * @param \Arb\ArbReports\Helper\MerchantSalesData $merchantSaleshelper
     * @param \Arb\ArbReports\Helper\InventoryData $merchantSaleshelper
     * @throws FileSystemException
     */
    public function __construct(
        Filesystem $filesystem,
        Filter $filter,
        MetadataProvider $metadataProvider,
        Http $request,
        \Arb\ArbReports\Helper\SalesData $helper,
        \Arb\ArbReports\Helper\MerchantSalesData $merchantSaleshelper,
        \Arb\ArbReports\Helper\InventoryData $inventoryData
    ) {
        $this->filter = $filter;
        $this->request = $request;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->metadataProvider = $metadataProvider;
        $this->helper = $helper;
        $this->merchantSaleshelper = $merchantSaleshelper;
        $this->inventoryData = $inventoryData;
    }

    /**
     * Returns CSV file
     *
     * @return array
     * @throws LocalizedException
     */
    public function getCsvFile()
    {

        // Preparing filters data//
        $transaction_date_start   = isset($this->request->getParam('filters')['transaction_date_start']) ? $this->request->getParam('filters')['transaction_date_start'] : '';
        $transaction_date_end   = isset($this->request->getParam('filters')['transaction_date_end']) ? $this->request->getParam('filters')['transaction_date_end'] : '';
        $order_id   = isset($this->request->getParam('filters')['order_id']) ? $this->request->getParam('filters')['order_id'] : '';
        $product_name   = isset($this->request->getParam('filters')['product_name']) ? $this->request->getParam('filters')['product_name'] : '';
        $product_id   = isset($this->request->getParam('filters')['product_id']) ? $this->request->getParam('filters')['product_id'] : "";
        $merchant_name   = isset($this->request->getParam('filters')['merchant_name']) ? $this->request->getParam('filters')['merchant_name'] : '';
        $customer_name   = isset($this->request->getParam('filters')['customer_name']) ? $this->request->getParam('filters')['customer_name'] : '';
        $merchant_id   = isset($this->request->getParam('filters')['merchant_id']) ? $this->request->getParam('filters')['merchant_id'] : '';
        $skuFilter   = isset($this->request->getParam('filters')['sku']) ? $this->request->getParam('filters')['sku'] : '';
        $transaction_reference_number   = isset($this->request->getParam('filters')['transaction_reference_number']) ? $this->request->getParam('filters')['transaction_reference_number'] : '';
        $qty   = isset($this->request->getParam('filters')['qty']) ? $this->request->getParam('filters')['qty'] : '';
        $status   = isset($this->request->getParam('filters')['status']) ? $this->request->getParam('filters')['status'] : '';

        $component = $this->filter->getComponent();
        $name = md5(microtime());
        $file = 'export/' . $component->getName() . $name . '.csv';
        $this->directory->create('cortexSalesReport');
        $stream = $this->directory->openFile($file, 'w+');
        $stream->lock();
        $stream->writeCsv(array_reverse($this->metadataProvider->getHeaders($component)));

        /** @var TYPE_NAME $salesData */

        $salesData= [];


        if ($component->getName() == self::MERCHANT_SALES_REPORT) {
          if ($order_id || $product_name || $merchant_name || $customer_name || $merchant_id || $skuFilter || $transaction_reference_number || $transaction_date_start || $transaction_date_end) {
                $salesData = $this->merchantSaleshelper->getSalesData($transaction_date_start, $transaction_date_end,$order_id,$merchant_id);
           }
        } elseif ($component->getName() == self::INVENTORY_REPORT) {
            if ($product_id || $product_name || $skuFilter || $qty || $status ) {
                $salesData = $this->inventoryData->getStockData($product_id, $product_name, $skuFilter, $qty, $status);
            }
        } else {
            if ($order_id || $product_name || $merchant_name || $customer_name || $merchant_id || $skuFilter || $transaction_reference_number || $transaction_date_start || $transaction_date_end) {
                $salesData = $this->helper->getSalesData($transaction_date_start, $transaction_date_end, $order_id, $merchant_id);
            }
        }

        foreach ($salesData as $data) {
            $stream->writeCsv($data);
        }

        $stream->unlock();
        $stream->close();

        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true  // can delete file after use
        ];
    }
}
