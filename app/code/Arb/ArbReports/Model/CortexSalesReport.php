<?php
/**
 * @category Arb
 * @package Arb_ArbReports
 * @author Arb Magento Team
 */

namespace Arb\ArbReports\Model;

class CortexSalesReport extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'arb_cortex_sales_report';

    protected $_cacheTag = 'arb_cortex_sales_report';

    protected $_eventPrefix = 'arb_cortex_sales_report';

    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\ArbReports\Model\ResourceModel\CortexSalesReport::class
        );
    }


    /**
     * get Identities from Cache.
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


}
