<?php
/**
 * Created by PhpStorm.
 * User: aftabaslam
 * Date: 2021-03-24
 * Time: 12:20
 */

namespace Arb\ArbReports\Controller\Inventory;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Csv;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as MarketplaceProductCollectionFactory;

class Export extends Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var Csv
     */
    protected $csvProcessor;

    /**
     * @var DirectoryList
     */
    protected $directoryList;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollection;

    /**
     * @var MarketplaceProductCollectionFactory
     */
    protected $marketplaceProductCollection;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var GetSalableQuantityDataBySku
     */
    protected $getSalableQuantityDataBySku;

    /**
     * @var Http
     */
    private $request;

    /**
     * @param Context $context
     * @param FileFactory $fileFactory
     * @param ProductFactory $productFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param Csv $csvProcessor
     * @param DirectoryList $directoryList
     * @param Http $request
     * @param Session $customerSession
     * @param ProductCollectionFactory $productCollection
     * @param MarketplaceProductCollectionFactory $marketplaceProductCollection
     * @param GetSalableQuantityDataBySku $getSalableQuantityDataBySku
     * @param StockRegistryInterface $stockRegistry
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        ProductFactory $productFactory,
        LayoutFactory $resultLayoutFactory,
        Csv $csvProcessor,
        DirectoryList $directoryList,
        Http $request,
        Session $customerSession,
        ProductCollectionFactory $productCollection,
        MarketplaceProductCollectionFactory $marketplaceProductCollection,
        GetSalableQuantityDataBySku $getSalableQuantityDataBySku,
        StockRegistryInterface $stockRegistry
    ) {
        parent::__construct($context);

        $this->fileFactory = $fileFactory;
        $this->productFactory = $productFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        $this->request = $request;
        $this->customerSession = $customerSession;
        $this->productCollection = $productCollection;
        $this->marketplaceProductCollection = $marketplaceProductCollection;
        $this->getSalableQuantityDataBySku = $getSalableQuantityDataBySku;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * CSV Create and Download
     *
     * @return ResponseInterface
     * @throws FileSystemException|LocalizedException
     */
    public function execute()
    {
        $product_id = $this->request->getParam('filters')['product_id'] ?? "";
        $product_name = $this->request->getParam('filters')['product_name'] ?? '';
        $sku = $this->request->getParam('filters')['product_sku'] ?? '';
        $qty = $this->request->getParam('filters')['product_quantity'] ?? '';
        $status = $this->request->getParam('filters')['product_status'] ?? '';

        $collection = $this->productCollection->create();
        $collection->addAttributeToSelect('*');

        $collection->joinTable(
            ['mpro' => $this->marketplaceProductCollection->create()->getMainTable()],
            'mage_pro_row_id = row_id',
            ['seller_id'],
            '{{table}}.seller_id = ' . (int)$this->customerSession->getCustomer()->getId()
        );

        if (!empty($product_id)) {
            $collection->addFieldToFilter('entity_id', $product_id);
        }

        if (!empty($product_name)) {
            $collection->addAttributeToFilter('name', [
                ['like' => '%' . $product_name . '%']
            ]);
        }

        if (!empty($sku)) {
            $collection->addFieldToFilter('sku', $sku);
        }
        if (!empty($qty)) {
            $collection->joinField(
                'qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            $collection->addAttributeToFilter('qty', ['lteq' => $qty]);
        }
        if (!empty($status)) {
            $collection->addFieldToFilter('status', $status);
        }

        $collection
            ->setPageSize((int)$this->request->getParam('paging')['pageSize'])
            ->setCurPage((int)$this->request->getParam('paging')['current'])
            ->load();

        $productsData = [];
        $productsData[] = [
            "Product ID",
            "Product Name",
            "SKU",
            "Product Price",
            "Quantity",
            "Salable Quantity",
            "Product Status"
        ];
        foreach ($collection as $product) {
            /** @var Product $product */
            $productStock = $this->stockRegistry->getStockItem($product->getId());
            // Get quantity of product.
            $productQty = $productStock->getQty();
            $saleAbleQty = 0;
            if ($productQty > 0) {
                $salable = $this->getSalableQuantityDataBySku->execute($product->getSku());
                $saleAbleQty = $salable[0]['qty'];
            }

            $productsData[] = [
                "product_id" => $product->getId(),
                "product_name" => $product->getName(),
                "product_sku" => $product->getSku(),
                "product_price" => $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount(),
                "product_quantity" => $productQty,
                "product_salable_quantity" => $saleAbleQty,
                "product_status" => __(($product->getStatus()==1) ? 'Enabled' : 'Disabled')
            ];
        }
        $fileName = 'stock_inventory_report.csv'; // Add Your CSV File name
        $filePath =  $this->directoryList->getPath(DirectoryList::MEDIA) . "/" . $fileName;
        $this->csvProcessor->setEnclosure('"')->setDelimiter(',')->saveData($filePath, $productsData);
        return $this->fileFactory->create(
            $fileName,
            [
                'type'  => "filename",
                'value' => $fileName,
                'rm'    => true, // True => File will be remove from directory after download.
            ],
            DirectoryList::MEDIA,
            'text/csv',
            null
        );
    }
}
