<?php
namespace Arb\ArbReports\Controller\Adminhtml\SalesReport;

class Listing extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE='Arb_ArbReports::salesreports_list';

    protected $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {

        //Call page factory to render layout and page content
        $resultPage = $this->resultPageFactory->create();
        //Set the menu which will be active for this page
        $resultPage->setActiveMenu('Arb_ArbReports::salesreports_list');
        //Set the header title of grid
        $resultPage->getConfig()->getTitle()->prepend(__('Cortex Sales Report'));
        return $resultPage;
    }
}
