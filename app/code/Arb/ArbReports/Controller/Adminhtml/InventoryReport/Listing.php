<?php
namespace Arb\ArbReports\Controller\Adminhtml\InventoryReport;
class Listing extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE='Arb_ArbReports::inventoryreports';

    protected $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        //Call page factory to render layout and page content
        $resultPage = $this->resultPageFactory->create();
        //Set the menu which will be active for this page
        $resultPage->setActiveMenu('Arb_ArbReports::inventoryreports');
        //Set the header title of grid
        $resultPage->getConfig()->getTitle()->prepend(__('Stock Inventory Report'));
        return $resultPage;
    }
}


