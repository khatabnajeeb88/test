<?php
/**
 * Created by PhpStorm.
 * User: aftabaslam
 * Date: 2021-03-24
 * Time: 12:20
 */


namespace Arb\ArbReports\Controller\Adminhtml\InventoryReport;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;

class Export extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;
    /**
     * @var \Magento\Framework\App\Action\Context
     */
    private $context;
    /**
     * @var Http
     */
    private $request;
    /**
     * @var \Arb\ArbReports\Helper\InventoryData
     */
    private $inventoryData;

    /**
     * @param \Magento\Framework\App\Action\Context            $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Catalog\Model\ProductFactory            $productFactory
     * @param \Magento\Framework\View\Result\LayoutFactory     $resultLayoutFactory
     * @param \Magento\Framework\File\Csv                      $csvProcessor
     * @param \Magento\Framework\App\Filesystem\DirectoryList  $directoryList
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        Http $request,
        \Arb\ArbReports\Helper\InventoryData $inventoryData
    ) {
        $this->fileFactory = $fileFactory;
        $this->productFactory = $productFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        parent::__construct($context);
        $this->context = $context;
        $this->request = $request;
        $this->inventoryData = $inventoryData;
    }

    /**
     * CSV Create and Download
     *
     * @return ResponseInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function execute()
    {

        // Preparing filters data//
        $product_name   = isset($this->request->getParam('filters')['product_name']) ? $this->request->getParam('filters')['product_name'] : '';
        $product_id   = isset($this->request->getParam('filters')['product_id']) ? $this->request->getParam('filters')['product_id'] : "";
        $skuFilter   = isset($this->request->getParam('filters')['sku']) ? $this->request->getParam('filters')['sku'] : '';
        $qty   = isset($this->request->getParam('filters')['qty']) ? $this->request->getParam('filters')['qty'] : '';
        $status   = isset($this->request->getParam('filters')['status']) ? $this->request->getParam('filters')['status'] : '';

        /** Add yout header name here */
        $salesData[] =  [
            "Product ID",
            "Product Name",
            "Product SKU",
            "Product Price",
            "Product Quantity",
            "Product salable Quantity",
            "Product Status",
            "Merchant Name",
            "Merchant ID"
        ];
        $inventoryData = $this->inventoryData->getStockData($product_id, $product_name, $skuFilter, $qty, $status);
        foreach ($inventoryData as $data) {
            $salesData[] = [
                $data["product_id"],
                $data["product_name"],
                $data["product_sku"],
                $data["product_price"],
                $data["product_quantity"],
                $data["product_salable_quantity"],
                $data["product_status"],
                $data["merchant_name"],
                $data["merchant_id"],
            ];
        }
        $fileName = 'stock_inventory_report.csv'; // Add Your CSV File name
        $filePath =  $this->directoryList->getPath(DirectoryList::MEDIA) . "/" . $fileName;
        $this->csvProcessor->setEnclosure('"')->setDelimiter(',')->saveData($filePath, $salesData);
        return $this->fileFactory->create(
            $fileName,
            [
                'type'  => "filename",
                'value' => $fileName,
                'rm'    => true, // True => File will be remove from directory after download.
            ],
            DirectoryList::MEDIA,
            'text/csv',
            null
        );
    }
}
