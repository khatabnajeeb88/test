<?php
namespace Arb\ArbReports\Controller\Sales;

use Magento\Customer\Controller\AbstractAccount;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use Webkul\Marketplace\Helper\Data as HelperData;

class Index extends AbstractAccount
{
    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * Upload controller constructor
     *
     * @param Context $context
     * @param HelperData $helper
     * @param PageFactory $resultPageFactory
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        HelperData $helper,
        PageFactory $resultPageFactory,
        Session $customerSession
    ) {
        $this->helper = $helper;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $customerSession;
        parent::__construct(
            $context
        );
    }

    public function dispatch(RequestInterface $request)
    {
        $loginUrl =  $this->_url->getUrl(
            'marketplace/account/login/',
            ['_secure' => $this->getRequest()->isSecure()]
        );

        if (!$this->customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        if ($this->helper->isSeller()) {
            if ($this->helper->getIsSeparatePanel()) {
                $resultPage->addHandle('salesreport_layout_index');
                $resultPage->getConfig()->getTitle()->set(__('Marketplace Sales Reports'));
                return $resultPage;
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}
