<?php

namespace Arb\ArbReports\Ui\DataProvider;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Ui\DataProvider\AddFieldToCollectionInterface;
use Magento\Ui\DataProvider\AddFilterToCollectionInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as MarketplaceProductCollectionFactory;

class InventoryReportDataProvider extends AbstractDataProvider
{
    /**
     * @var AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollection;

    /**
     * @var MarketplaceProductCollectionFactory
     */
    protected $marketplaceProductCollection;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var GetSalableQuantityDataBySku
     */
    protected $getSalableQuantityDataBySku;

    /**
     * @var Http
     */
    protected $request;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Http $request,
        Session $customerSession,
        ProductCollectionFactory $productCollection,
        MarketplaceProductCollectionFactory $marketplaceProductCollection,
        GetSalableQuantityDataBySku $getSalableQuantityDataBySku,
        StockRegistryInterface $stockRegistry,
        array $meta = [],
        array $data = []
    ) {
        $this->request = $request;
        $this->name = $name;
        $this->primaryFieldName = $primaryFieldName;
        $this->requestFieldName = $requestFieldName;
        $this->meta = $meta;
        $this->customerSession = $customerSession;
        $this->productCollection = $productCollection;
        $this->marketplaceProductCollection = $marketplaceProductCollection;
        $this->getSalableQuantityDataBySku = $getSalableQuantityDataBySku;
        $this->stockRegistry = $stockRegistry;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        $product_id = $this->request->getParam('filters')['product_id'] ?? "";
        $product_name = $this->request->getParam('filters')['product_name'] ?? '';
        $sku = $this->request->getParam('filters')['product_sku'] ?? '';
        $qty = $this->request->getParam('filters')['product_quantity'] ?? '';
        $status = $this->request->getParam('filters')['product_status'] ?? '';

        $collection = $this->productCollection->create();
        $collection->addAttributeToSelect('*');

        $collection->joinTable(
            ['mpro' => $this->marketplaceProductCollection->create()->getMainTable()],
            'mage_pro_row_id = row_id',
            ['seller_id'],
            '{{table}}.seller_id = ' . (int)$this->customerSession->getCustomer()->getId()
        );

        if (!empty($product_id)) {
            $collection->addFieldToFilter('entity_id', $product_id);
        }

        if (!empty($product_name)) {
            $collection->addAttributeToFilter('name', [
                ['like' => '%' . $product_name . '%']
            ]);
        }

        if (!empty($sku)) {
            $collection->addFieldToFilter('sku', $sku);
        }
        if (!empty($qty)) {
            $collection->joinField(
                'qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            $collection->addAttributeToFilter('qty', ['lteq' => $qty]);
        }
        if (!empty($status)) {
            $collection->addFieldToFilter('status', $status);
        }

        $totalRecords = $collection->count();

        $collection
            ->setPageSize((int)$this->request->getParam('paging')['pageSize'])
            ->setCurPage((int)$this->request->getParam('paging')['current'])
            ->load();

        $productsData = [];
        foreach ($collection as $product) {
            /** @var Product $product */
            $productStock = $this->stockRegistry->getStockItem($product->getId());
            // Get quantity of product.
            $productQty = $productStock->getQty();
            $saleAbleQty = 0;
            if ($productQty > 0) {
                $salable = $this->getSalableQuantityDataBySku->execute($product->getSku());
                $saleAbleQty = $salable[0]['qty'];
            }

            $productsData[] = [
                "product_id" => $product->getId(),
                "product_name" => $product->getName(),
                "product_sku" => $product->getSku(),
                "product_price" => $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount(),
                "product_quantity" => $productQty,
                "product_salable_quantity" => $saleAbleQty,
                "product_status" => $product->getStatus()
            ];
        }
        return [
            'totalRecords' => $totalRecords,
            'items' => $productsData
        ];
    }

    public function setCurrentPage($offset, $size)
    {
    }

    public function setPageSize($offset, $size)
    {
    }

    public function setLimit($offset, $size)
    {
    }

    public function addOrder($field, $direction)
    {
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
    }
}
