<?php
namespace Arb\ArbReports\Ui\Component\Listing\DataProviders\Arb\MerchantSalesReport;

use Magento\Framework\App\Request\Http;

/**
 * Class MerchantSalesDataProvider
 */
 /**
  * @codeCoverageIgnore
  */
class MerchantSalesReport extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var \Arb\ArbCardsListing\Helper\Data
     */
    private $helper;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Http $request,
        array $meta = [],
        array $data = [],
        \Arb\ArbReports\Helper\MerchantSalesData $helper
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->name = $name;
        $this->primaryFieldName = $primaryFieldName;
        $this->requestFieldName = $requestFieldName;
        $this->meta = $meta;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {

        // Preparing filters data//
        $transaction_date_start   = isset($this->request->getParam('filters')['transaction_date_start']) ? $this->request->getParam('filters')['transaction_date_start'] : '';
        $transaction_date_end   = isset($this->request->getParam('filters')['transaction_date_end']) ? $this->request->getParam('filters')['transaction_date_end'] : '';
        $order_id   = isset($this->request->getParam('filters')['order_id']) ? $this->request->getParam('filters')['order_id'] : "";
        $merchant_id   = isset($this->request->getParam('filters')['merchant_id']) ? $this->request->getParam('filters')['merchant_id'] : '';
        $salesData= [];
        if ($order_id  ||   $merchant_id ||  $transaction_date_start || $transaction_date_end) {
            $salesData = $this->helper->getSalesData($transaction_date_start, $transaction_date_end,$order_id, $merchant_id);
        }
        $pagesize = intval($this->request->getParam('paging')['pageSize']);
        $pageCurrent = intval($this->request->getParam('paging')['current']);
        $pageoffset = ($pageCurrent - 1)*$pagesize;

        return [
                'totalRecords' => count($salesData),
                'items'=>array_slice($salesData, $pageoffset, $pageoffset+$pagesize),
        ];
    }

    /**
     * get sales data from Marketplace
     * @return array $salesData
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    // ###########################################
    public function setCurrentPage($offset, $size)
    {
    }
    public function setPageSize($offset, $size)
    {
    }

    public function setLimit($offset, $size)
    {
    }
    public function addOrder($field, $direction)
    {
    }
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
    }
}
