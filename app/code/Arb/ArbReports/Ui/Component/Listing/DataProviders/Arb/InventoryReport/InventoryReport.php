<?php
namespace Arb\ArbReports\Ui\Component\Listing\DataProviders\Arb\InventoryReport;

use Magento\Framework\App\Request\Http;

/**
 * Class InventoryReportsDataProvider
 */
 /**
  * @codeCoverageIgnore
  */
class InventoryReport extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var \Arb\ArbCardsListing\Helper\Data
     */
    private $helper;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Http $request,
        array $meta = [],
        array $data = [],
        \Arb\ArbReports\Helper\InventoryData $helper
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->name = $name;
        $this->primaryFieldName = $primaryFieldName;
        $this->requestFieldName = $requestFieldName;
        $this->meta = $meta;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {

        // Preparing filters data//
        $product_id   = isset($this->request->getParam('filters')['product_id']) ? $this->request->getParam('filters')['product_id'] : "";
        $product_name   = isset($this->request->getParam('filters')['product_name']) ? $this->request->getParam('filters')['product_name'] : '';
        $sku   = isset($this->request->getParam('filters')['sku']) ? $this->request->getParam('filters')['sku'] : '';
        $qty   = isset($this->request->getParam('filters')['qty']) ? $this->request->getParam('filters')['qty'] : '';
        $status   = isset($this->request->getParam('filters')['status']) ? $this->request->getParam('filters')['status'] : '';

        $salesData= [];
        if ($product_id || $product_name  || $sku || $qty || $status) {
            $salesData = $this->helper->getStockData($product_id, $product_name, $sku, $qty, $status);
        }
        $pagesize = intval($this->request->getParam('paging')['pageSize']);
        $pageCurrent = intval($this->request->getParam('paging')['current']);
        $pageoffset = ($pageCurrent - 1)*$pagesize;

        return [
                'totalRecords' => count($salesData),
                'items'=>array_slice($salesData, $pageoffset, $pageoffset+$pagesize),
        ];
    }

    /**
     * get sales data from Marketplace
     * @return array $salesData
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    // ###########################################
    public function setCurrentPage($offset, $size)
    {
    }
    public function setPageSize($offset, $size)
    {
    }

    public function setLimit($offset, $size)
    {
    }
    public function addOrder($field, $direction)
    {
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
    }
}
