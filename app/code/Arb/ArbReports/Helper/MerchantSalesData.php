<?php
/**
 * Created by PhpStorm.
 * User: aftabaslam
 * Date: 2020-12-03
 * Time: 10:06
 */

namespace Arb\ArbReports\Helper;

use Arb\ArbReports\Model\MerchantSalesReportFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\OrderFactory;

use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SalesListCollection;

/**
 * ArbSalesReport data helper
 *
 * @api
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
/**
 * @codeCoverageIgnore
 */
class MerchantSalesData extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var OrderFactory
     */
    private $_orderFactory;
    /**
     * @var SalesListCollection
     */
    private $_salesListCollection;
    /**
     * @var CustomerRepositoryInterface
     */
    private $_customerRepository;
    /**
     * Not Available Option
     */
    const NOT_AVAILABLE = "NA";

    /**
     * Successful file transfer Status for cortex log
     */
    const SUCCESS_STATUS = "1";

    /**
     * Default Status for cortex log
     */
    const DEFAULT_STATUS = "0";

    /**
     * Order Currency SAR
     */
    const ORDER_CURRENCY = "SAR";

    /**
     * Set Loyalty as Zero
     */
    const LOYALTY = 0;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $timezone;
    /**
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory
     */
    private $voucherCollection;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var MerchantSalesReportFactory
     */
    private $merchantSalesReportFactory;

    /**
     * MerchantSalesData constructor.
     * @param Context $context
     * @param DateTime $_dateTime
     * @param OrderFactory $_orderFactory
     * @param SalesListCollection $_salesListCollection
     * @param CustomerRepositoryInterface $_customerRepository
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $voucherCollection
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $collectionFactory
     * @param MerchantSalesReportFactory $merchantSalesReportFactory
     */
    public function __construct(
        Context $context,
        DateTime $_dateTime,
        OrderFactory $_orderFactory,
        SalesListCollection $_salesListCollection,
        CustomerRepositoryInterface $_customerRepository,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $voucherCollection,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $collectionFactory,
        MerchantSalesReportFactory $merchantSalesReportFactory
    ) {
        $this->_dateTime = $_dateTime;
        $this->context = $context;
        $this->_orderFactory = $_orderFactory;
        $this->_salesListCollection = $_salesListCollection;
        $this->_customerRepository = $_customerRepository;
        parent::__construct($context);
        $this->timezone = $timezone;
        $this->voucherCollection = $voucherCollection;
        $this->collectionFactory = $collectionFactory;
        $this->merchantSalesReportFactory = $merchantSalesReportFactory;
    }

    /**
     * @param $transaction_date_start
     * @param $transaction_date_end
     * @param $order_id
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveSalesData($data)
    {
        try {
            $model = $this->merchantSalesReportFactory->create();
            $model->setData($data)->save();

        } catch (\Exception $e) {
            throw new LocalizedException(__('Something went wrong, Please try again later.'));
        }
    }

    public function getSalesData($transaction_date_start, $transaction_date_end, $order_id, $merchant_id)
    {



        $sales_data = $this->merchantSalesReportFactory->create();
        $collection = $sales_data->getCollection();

        if (!empty($order_id)) {
            $collection->addFieldTofilter('order_id', ['eq' => $order_id]);
        }

        if (!empty($merchant_id)) {
            $collection->addFieldTofilter('merchant_id', ['eq' => $merchant_id]);
        }

        if (!empty($transaction_date_start) && !empty($transaction_date_end)) {
            $startDate = date("Y-m-d", strtotime($transaction_date_start)); // start date
            $endDate = date("Y-m-d", strtotime($transaction_date_end)); // end date
            $collection->addFieldToFilter('transaction_date', ['from'=>$startDate, 'to'=>$endDate]);
        }

        $salesData  =[];
        foreach ($collection as $order) {

            $created = $this->timezone->date(new \DateTime($order["transaction_date"]));
            //To print or display this you can use following.
            $transactionDate = $created->format('Y-m-d');
            $transactionTime = $created->format('H:i:s');

            $salesData[] = [
                "order_id"              =>  $order["order_id"],
                "transaction_date"      =>  $transactionDate,
                "transaction_time"      =>  $transactionTime,
                "customer_name"         =>  $order["customer_name"],
                "customer_id"           =>  $order["customer_id"],
                "merchant_name"         =>  $order["merchant_name"],
                "merchant_id"           =>  $order["merchant_id"],
                "product_name"          =>  $order["product_name"],
                "product_price"         => $order["product_price"],
                "authorization_number"  => $order["authorization_number"],
                "cortex_terminal_id"    => $order["cortex_terminal_id"],
                "pg_transaction_reference_number"=>$order["pg_transaction_reference_number"],
                "sku"                   => $order["sku"],
                "product_id"            =>$order["product_id"],
                "voucher_serial_numbers"=> $order["voucher_serial_numbers"]
            ];

        }

        return $salesData;


    }
}
