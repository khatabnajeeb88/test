<?php
/**
 * Created by PhpStorm.
 * User: aftabaslam
 * Date: 2020-12-03
 * Time: 10:06
 */

namespace Arb\ArbReports\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku;

/**
 * ArbStockInventoryReport InventoryData helper
 *
 * @api
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
/**
 * @codeCoverageIgnore
 */
class InventoryData extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var CustomerRepositoryInterface
     */
    private $_customerRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $timezone;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollection;
    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    private $stockItemRepository;
    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    private $stockFilter;
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    private $stockRegistry;
    /**
     * @var GetSalableQuantityDataBySku
     */
    private $getSalableQuantityDataBySku;
    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Product\Collection
     */
    private $sellerCollection;

    /**
     * InventoryData constructor.
     * @param Context $context
     * @param CustomerRepositoryInterface $_customerRepository
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param GetSalableQuantityDataBySku $getSalableQuantityDataBySku
     * @param \Webkul\Marketplace\Model\ResourceModel\Product\Collection $sellerCollection
     */
    public function __construct(
        Context $context,
        CustomerRepositoryInterface $_customerRepository,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        GetSalableQuantityDataBySku $getSalableQuantityDataBySku,
        \Webkul\Marketplace\Model\ResourceModel\Product\Collection $sellerCollection
    ) {
        $this->context = $context;
        $this->_customerRepository = $_customerRepository;
        parent::__construct($context);
        $this->timezone = $timezone;
        $this->productCollection = $productCollection;
        $this->stockRegistry = $stockRegistry;
        $this->getSalableQuantityDataBySku = $getSalableQuantityDataBySku;
        $this->sellerCollection = $sellerCollection;
    }

    /**
     * @param $productIds
     * @param $storeId
     * @return array
     */
    public function getMerchantData($productIds, $storeId)
    {
        //@codeCoverageIgnoreStart
        // code coverage will be get completed after the webkul team provides proper testcases for webkul model
        $productIds = implode(',', $productIds);
        $brandUserTable = $this->sellerCollection->getTable('marketplace_userdata');

        $brandCollection = $this->sellerCollection;

        $brandCollection->addFieldToFilter('main_table.mageproduct_id', [ "in" =>$productIds ]);
        $brandCollection->addFieldToFilter('main_table.status', 1);

        $brandCollection->getSelect()->join(
            $brandUserTable . ' as band_store_default',
            'main_table.seller_id = band_store_default.seller_id
            AND band_store_default.store_id = 0',
            [
                'seller_id_default' => 'band_store_default.seller_id',
            ]
        );

        $brandCollection->getSelect()->joinLeft(
            $brandUserTable . ' as band_store',
            'main_table.seller_id = band_store.seller_id
            AND band_store.store_id = ' . $storeId,
            [
                'shop_title' => 'IF(band_store.shop_title != \'\', band_store.shop_title,
                        band_store_default.shop_title)',
                'store_id' => 'IF(band_store.store_id > 0, band_store.store_id,
                        band_store_default.store_id)'
            ]
        );
        $brandCollection->addFieldToSelect([
            'mageproduct_id',
            'seller_id'
        ]);
        $merchantData = [];
        if (count($brandCollection) >= 1) {
            foreach ($brandCollection->load() as $data) {
                $merchantData[$data['mageproduct_id']] = $data->getData();
            }
        }
        return $merchantData;
        //@codeCoverageIgnoreEnd
    }


    /**
     * @param $product_id
     * @param $product_name
     * @param $sku
     * @param $qty
     * @param $status
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStockData($product_id, $product_name, $sku, $qty, $status)
    {
        $collection = $this->productCollection->create();
        $collection->addAttributeToSelect('*');
        if (!empty($product_id)) {
            $collection->addFieldToFilter('entity_id', $product_id);
        }
        if (!empty($product_name)) {
            $collection->addAttributeToFilter('name', [
                ['like' => '%' . $product_name . '%']
            ]);
        }
        if (!empty($sku)) {
            $collection->addFieldToFilter('sku', $sku);
        }
        if (!empty($qty)) {
            $collection->joinField(
                'qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            $collection->addAttributeToFilter('qty', ['lteq'=>$qty]);
        }
        if (!empty($status)) {
            $collection->addFieldToFilter('status', $status);
        }
        $productsData  =[];
        $itemsId = [];
        foreach ($collection as $product) {
            $itemsId[$product->getId()] = $product->getId();
        }
        $merchants = $this->getMerchantData($itemsId, 1);
        foreach ($collection as $product) {
            $productStock = $this->stockRegistry->getStockItem($product->getId());
            // Get quantity of product.
            $productQty = $productStock->getQty();
            $saleAbleQty = 0;
            if($productQty >0){
                $salable = $this->getSalableQuantityDataBySku->execute($product->getSku());
                $saleAbleQty = $salable[0]['qty'];
            }
            //@codeCoverageIgnoreStart
            $merchantName = isset($merchants[$product->getId()]) ? $merchants[$product->getId()]['shop_title'] : '';
            $merchantId = isset($merchants[$product->getId()]) ? $merchants[$product->getId()]['seller_id'] : 0;
            $productsData[] = [

                    "product_id"                =>  $product->getId(),
                    "product_name"              =>  $product->getName(),
                    "product_sku"               =>  $product->getSku(),
                    "product_price"             =>  $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount(),
                    "product_quantity"          =>  $productQty,
                    "product_salable_quantity"  =>  $saleAbleQty,
                    "product_status"            => ($product->getStatus()==1) ? "Enabled" : "Disabled",
                    "merchant_name"             =>  $merchantName,
                    "merchant_id"               =>  $merchantId,
                     ];
        }
        return $productsData;
    }
}
