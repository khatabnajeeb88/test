<?php
/**
 * SalesexportTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbReports\Helper;

/**
 * Consist of order data funtions
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SalesListCollection;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\Collection as SalesListCollectionMock;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory as OrdersCollection;
use Magento\Framework\File\Csv;
use Magento\Framework\App\Filesystem\DirectoryList;
use Arb\Cortex\Model\CortexFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Filesystem\Driver\File as DirectoryListFile;
use Arb\ArbSalesReport\Helper;
use Magento\Framework\DB\Select;
use Magento\Framework\App\Helper\Context;
use Arb\ArbReports\Helper\SalesData;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Arb\ArbReports\Model\CortexSalesReportFactory;

/**
 * Class SalesexportTest for testing Salesexport class
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class SalesDataTest extends TestCase
{

    /**
     * @var Csv|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_csvProcessorMock;
    /**
     * @var DirectoryList|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_directoryListMock;

    /**
     * @var SaleslistFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_salesListCollectionMock;

    /**
     * @var CortexFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_cortexFactoryMock;

    /**
     * @var DateTime|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_dateTimeMock;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_scopeConfigMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Framework\App\ResourceConnection
     */
    protected $_resourceMock;

    /**
     * @var Data
     */
    protected $_data;

    /**
     * @var File
     */
    protected $_fileIoMock;

    /**
     * @var DirectoryListFile
     */
    protected $_directoryListFileMock;

    /**
     * @var \Arb\Cortex\Model\CortexRefund
     */
    protected $_cortexRefund;

    /**
     *
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->_csvProcessorMock = $this->getMockBuilder(Csv::class)
            ->setMethods([
                'setDelimiter' ,
                'setEnclosure' ,
                'saveData'
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_csvProcessorMock->expects($this->any())->method('setDelimiter')->willReturnSelf();
        $this->_csvProcessorMock->expects($this->any())->method('setEnclosure')->willReturnSelf();
        $this->_directoryListMock = $this->createMock(DirectoryList::class);
        $this->_salesListCollectionMock = $this->getMockBuilder(SalesListCollection::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_salesListMock = $this->getMockBuilder(\Webkul\Marketplace\Model\Saleslist::class)
            ->setMethods(["getSelect","columns",'group',"addFieldToFilter",])
            ->disableOriginalConstructor()
            ->getMock();
        $this->salesListCollectionMock = $this->objectManager->getCollectionMock(
            SalesListCollectionMock::class,
            [$this->_salesListMock]
        );
        $this->_cortexFactoryMock = $this->createPartialMock(CortexFactory::class, ['create',"addData","save"]);

        $this->_dateTimeMock = $this->createMock(DateTime::class);
        $this->cortexSalesReportFactory = $this->createMock(CortexSalesReportFactory::class);
        $this->_scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->_resourceMock = $this->getMockBuilder(ResourceConnection::class)
            ->setMethods(['insertOnDuplicate',"getConnection","getTableName"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->selectMock = $this->createMock(Select::class);
        $this->_customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
            ->setMethods(["getById","__toArray"])
            ->disableOriginalConstructor()->getMockForAbstractClass();
        $this->_fileIoMock = $this->createMock(File::class);
        $this->_directoryListFileMock = $this->createMock(DirectoryListFile::class);
        $this->_orderFactoryMock = $this->getMockBuilder(OrderFactory::class)
            ->setMethods(["create","getCollection","addFieldToFilter","setOrder","getFirstItem"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_resourceMock->expects($this->any())->method('getConnection')->willReturnSelf();
        $this->_resourceMock->expects($this->any())->method('getTableName')->willReturn("sales_order");
        $this->_resourceMock->expects($this->any())->method('insertOnDuplicate')->willReturn(1);
        $this->orderMock = $this->getMockBuilder(\Magento\Sales\Model\Order::class)
            ->disableOriginalConstructor()
            ->getMock();


        $this->orderCollectionMock = $this->getMockBuilder(OrdersCollection::class)
            ->disableOriginalConstructor()
            ->setMethods([
                "addFieldToFilter","getFirstItem","create","getCreditmemoId","getShippingCharges"
            ])
            ->getMock();
        $this->creditmemoRepositoryMock = $this->getMockBuilder(CreditmemoRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                "get","getBaseGrandTotal"
            ])
            ->getMockForAbstractClass();
        $this->_contextMock = $this->createMock(Context::class);
        $this->timezoneMock = $this->createMock(TimezoneInterface::class);
        $this->cortexSalesReportFactoryMock = $this->getMockBuilder(\Arb\ArbReports\Model\CortexSalesReportFactory::class)
            ->disableOriginalConstructor()
            ->getMock();



        $this->_helperMock = new SalesData(
            $this->_contextMock,
            $this->_dateTimeMock,
            $this->_orderFactoryMock,
            $this->_salesListCollectionMock,
            $this->_customerRepositoryMock,
            $this->timezoneMock,
            $this->cortexSalesReportFactoryMock

        );
    }

    public function testExecute()
    {
        $this->_salesListCollectionMock->expects($this->any())
            ->method('create')->willReturn($this->salesListCollectionMock);
        $this->salesListCollectionMock->expects($this->any())->method('addFieldToFilter')
            ->willReturnSelf();
        $this->salesListCollectionMock->expects($this->any())->method('getSelect')->willReturn($this->selectMock);

         $this->_orderFactoryMock->expects($this->any())->method('create')->willReturnSelf();
        $this->_orderFactoryMock->expects($this->any())->method('getCollection')->willReturnSelf();
        $this->_orderFactoryMock->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $orders = [$this->orderMock];
        $this->_orderFactoryMock->expects($this->any())->method('setOrder')->willReturn($orders);
        $this->_customerRepositoryMock->method("getById")->willReturnSelf();
        $customerData["custom_attributes"] = [
            "wkv_cortex_merchants_id"=>["value"=>10],
            "wkv_cortex_terminal_id"=>["value"=>10]
        ];
        $this->_customerRepositoryMock->method("__toArray")->willReturn($customerData);
      }
}
