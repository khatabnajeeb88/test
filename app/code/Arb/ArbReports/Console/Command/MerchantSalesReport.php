<?php

namespace Arb\ArbReports\Console\Command;

use Arb\ArbReports\Cron\MerchantReportData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MerchantSalesReport extends Command
{
    /**
     * @var MerchantReportData
     */
    protected $merchantSalesReportDataCron;

    public function __construct(
        MerchantReportData $merchantSalesReportDataCron,
        string $name = null
    ) {
        parent::__construct($name);

        $this->merchantSalesReportDataCron = $merchantSalesReportDataCron;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('arb:reports:merchant');
        $this->setDescription('Generate Merchant Sales Reports for Admin');

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting Merchant Sales Report Update');
        $time = time();
        $this->merchantSalesReportDataCron->execute();
        $output->writeln(__('End Merchant Sales Report Update, (Elapsed: %1 secs)', time() - $time));
    }
}
