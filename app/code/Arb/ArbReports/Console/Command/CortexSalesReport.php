<?php

namespace Arb\ArbReports\Console\Command;

use Arb\ArbReports\Cron\CortexSalesReportData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CortexSalesReport extends Command
{
    /**
     * @var CortexSalesReportData
     */
    protected $cortexSalesReportDataCron;

    public function __construct(
        CortexSalesReportData $cortexSalesReportDataCron,
        string $name = null
    ) {
        parent::__construct($name);

        $this->cortexSalesReportDataCron = $cortexSalesReportDataCron;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('arb:reports:cortex');
        $this->setDescription('Generate Cortex Sales Reports for Admin');

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting Cortex Sales Report Update');
        $time = time();
        $this->cortexSalesReportDataCron->execute();
        $output->writeln(__('End Cortex Sales Report Update, (Elapsed: %1 secs)', time() - $time));
    }
}
