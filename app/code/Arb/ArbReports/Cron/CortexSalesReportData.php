<?php

namespace Arb\ArbReports\Cron;

use Arb\ArbReports\Model\CortexSalesReportFactory;
use Arb\ArbReports\Model\ResourceModel\CortexSalesReport\CollectionFactory as CortexSalesReportCollectionFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SalesListCollection;

class CortexSalesReportData
{

    /**
     * Not Available Option
     */
    public const NOT_AVAILABLE = "NA";

    /**
     * Successful file transfer Status for cortex log
     */
    public const SUCCESS_STATUS = "1";

    /**
     * Default Status for cortex log
     */
    public const DEFAULT_STATUS = "0";

    /**
     * Order Currency SAR
     */
    public const ORDER_CURRENCY = "SAR";

    /**
     * Set Loyalty as Zero
     */
    public const LOYALTY = 0;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var SalesListCollection
     */
    protected $salesListCollection;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var CortexSalesReportFactory
     */
    protected $cortexSalesReportFactory;

    /**
     * @var CortexSalesReportCollectionFactory
     */
    protected $cortexSalesReportCollectionFactory;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * CortexSalesReportData constructor.
     * @param Context $context
     * @param DateTime $_dateTime
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param SalesListCollection $salesListCollection
     * @param CustomerRepositoryInterface $customerRepository
     * @param TimezoneInterface $timezone
     * @param CortexSalesReportFactory $cortexSalesReportFactory
     * @param ProductRepository $productRepository
     * @param CortexSalesReportCollectionFactory $cortexSalesReportCollectionFactory
     */
    public function __construct(
        Context $context,
        DateTime $_dateTime,
        OrderCollectionFactory $orderCollectionFactory,
        SalesListCollection $salesListCollection,
        CustomerRepositoryInterface $customerRepository,
        TimezoneInterface $timezone,
        CortexSalesReportFactory $cortexSalesReportFactory,
        ProductRepository $productRepository,
        CortexSalesReportCollectionFactory $cortexSalesReportCollectionFactory
    ) {
        $this->dateTime = $_dateTime;
        $this->context = $context;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->salesListCollection = $salesListCollection;
        $this->customerRepository = $customerRepository;
        $this->timezone = $timezone;
        $this->cortexSalesReportFactory = $cortexSalesReportFactory;
        $this->productRepository = $productRepository;
        $this->cortexSalesReportCollectionFactory = $cortexSalesReportCollectionFactory;
    }

    /**
     * @return void
     * @throws LocalizedException
     */
    public function execute()
    {
        $pageSize = 100;
        $lastId = 0;
        do {
            $orderCollection = $this->orderCollectionFactory
                ->create()
                ->addFieldToFilter('entity_id', ['gt' => $lastId])
                ->addFieldToFilter('status', ['eq' => "complete"])
                ->addFieldToFilter('increment_id', [
                    'nin' => new \Zend_Db_Expr(
                        (string)$this->cortexSalesReportCollectionFactory->create()->addFieldToSelect('order_id')->getSelect()
                    )
                ]);

            $orderCollection->setPageSize($pageSize)->setOrder('entity_id', 'asc');
            $orderCollection->load();

            foreach ($orderCollection as $order) {
                /** @var Order $order */
                $lastId = $orderId = $order->getId();

                if (defined('PHP_SAPI') && PHP_SAPI === 'cli') {
                    echo __('Processing Order #%1', $order->getIncrementId()) . "\r\n";
                }

                //get Payment data from Order
                $payment = $order->getPayment();
                $referenceNo = $payment->getData("arb_ref_id") ?: self::NOT_AVAILABLE;
                $authCode = $payment->getData("arb_auth_code") ?: self::NOT_AVAILABLE;

                $items = $order->getAllItems();
                foreach ($items as $item) {
                    $_webkulSalelists = $this->salesListCollection->create()
                        ->addFieldToFilter("seller_id", ["neq" => self::DEFAULT_STATUS])
                        ->addFieldToFilter("order_id", $orderId)
                        ->addFieldToFilter("order_item_id", $item->getItemId());

                    $_webkulSalelists
                        ->getSelect()
                        ->columns([
                            'seller_total_commission' => new \Zend_Db_Expr('SUM(total_commission)')
                        ])
                        ->group('seller_id');

                    $_webkulSalelists->load();

                    if (defined('PHP_SAPI') && PHP_SAPI === 'cli' && count($_webkulSalelists) === 0) {
                        echo "\t" . __('No commission information found for order #', $order->getIncrementId()) . "\r\n";
                    }

                    foreach ($_webkulSalelists as $_webkulSalelist) {
                        $customerId = $_webkulSalelist["seller_id"];
                        //To print or display this you can use following.
                        $commission = number_format($_webkulSalelist["seller_total_commission"], 2, ".", "");

                        $cortexTerminalId = self::NOT_AVAILABLE;

                        try {
                            //Get Seller Attributes to set Data for Cortex Merchant ID/Cortex Terminal ID
                            $customer = $this->customerRepository->getById($customerId);
                            $cortexTerminalId = $customer->getCustomAttribute('wkv_cortex_terminal_id') ?
                                $customer->getCustomAttribute('wkv_cortex_terminal_id')->getValue() : self::NOT_AVAILABLE;
                            $merchant_name = $customer->getFirstname() ? ($customer->getFirstname() . ' ' . $customer->getLastname()) : self::NOT_AVAILABLE;
                        } catch (NoSuchEntityException $e) {
                            $merchant_name = self::NOT_AVAILABLE . ' (removed)';
                        }

                        $productCategoriesNames = [];
                        $family = "N/A";

                        try {
                            $product = $this->productRepository->getById($_webkulSalelist["mageproduct_id"]);
                            if ($product->getResource()->getAttribute('family')->getFrontend()->getValue($product)) {
                                $family = $product->getResource()->getAttribute('family')->getFrontend()->getValue($product);
                            }
                            $productCategoriesNames = $product->getCategoryCollection()
                                ->addFieldToSelect('name')
                                ->getColumnValues('name');
                        } catch (NoSuchEntityException $e) {
                            $family = "(removed)";
                        }

                        $salesData = [
                            "order_id" => $_webkulSalelist["magerealorder_id"],
                            "transaction_date" => $_webkulSalelist["created_at"],
                            "transaction_time" => $_webkulSalelist["created_at"],
                            "customer_name" => $order->getCustomerFirstname() . " " . $order->getCustomerLastName(),
                            "customer_id" => $order->getCustomerId() ?: "N/A",
                            "merchant_name" => $merchant_name,
                            "merchant_id" => $_webkulSalelist["seller_id"],
                            "product_name" => $_webkulSalelist["magepro_name"] ?? "N/A",
                            "product_price" => $_webkulSalelist["magepro_price"] ?? "N/A",
                            "vat" => $_webkulSalelist["total_tax"] ?? "N/A",
                            "price_vat" => (!empty($_webkulSalelist["magepro_price"])) ? $_webkulSalelist["magepro_price"] + $_webkulSalelist["total_tax"] : "N/A",
                            "quantity" => $_webkulSalelist["magequantity"] ?: "N/A",
                            "item_sub_total" => ($_webkulSalelist["magepro_price"] + $_webkulSalelist["total_tax"]) * ($_webkulSalelist["magequantity"]),
                            "commission" => $commission,
                            "seller_revenue" => number_format($_webkulSalelist["actual_seller_amount"], 2, ".", ""),
                            "currency" => self::ORDER_CURRENCY,
                            "authorization_number" => $authCode,
                            "cortex_terminal_id" => $cortexTerminalId,
                            "loyality_usage" => self::LOYALTY,
                            "pg_transaction_reference_number" => $referenceNo,
                            "order_status" => $order->getState(),
                            "sku" => $item->getSku(),
                            "product_id" => $_webkulSalelist["mageproduct_id"],
                            "category_name" => implode(',', $productCategoriesNames) ?: "N/A",
                            "family" => $family
                        ];

                        if (defined('PHP_SAPI') && PHP_SAPI === 'cli') {
                            print_r($salesData);
                        }

                        // saving the sales data in cortex sales table
                        $this->cortexSalesReportFactory->create()
                            ->setData($salesData)
                            ->save();
                    }
                }
            }
        } while ($orderCollection->count());
    }
}
