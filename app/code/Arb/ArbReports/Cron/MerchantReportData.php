<?php

namespace Arb\ArbReports\Cron;

use Arb\ArbReports\Model\MerchantSalesReportFactory;
use Arb\ArbReports\Model\ResourceModel\MerchantSalesReport\CollectionFactory as MerchantSalesReportCollectionFactory;
use Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory as VouchersCollectionFactory;
use Exception;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SalesListCollection;

class MerchantReportData
{

    /**
     * Not Available Option
     */
    public const NOT_AVAILABLE = "NA";

    /**
     * Successful file transfer Status for cortex log
     */
    public const SUCCESS_STATUS = "1";

    /**
     * Default Status for cortex log
     */
    public const DEFAULT_STATUS = "0";

    /**
     * Order Currency SAR
     */
    public const ORDER_CURRENCY = "SAR";

    /**
     * Set Loyalty as Zero
     */
    public const LOYALTY = 0;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var SalesListCollection
     */
    protected $salesListCollection;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var MerchantSalesReportFactory
     */
    protected $merchantSalesReportFactory;

    /**
     * @var MerchantSalesReportCollectionFactory
     */
    protected $merchantSalesReportCollectionFactory;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var VouchersCollectionFactory
     */
    private $voucherCollectionFactory;

    /**
     * MerchantSalesReportData constructor.
     * @param Context $context
     * @param DateTime $_dateTime
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param SalesListCollection $salesListCollection
     * @param CustomerRepositoryInterface $customerRepository
     * @param TimezoneInterface $timezone
     * @param MerchantSalesReportFactory $merchantSalesReportFactory
     * @param ProductRepository $productRepository
     * @param MerchantSalesReportCollectionFactory $merchantSalesReportCollectionFactory
     * @param VouchersCollectionFactory $voucherCollectionFactory
     */
    public function __construct(
        Context $context,
        DateTime $_dateTime,
        OrderCollectionFactory $orderCollectionFactory,
        SalesListCollection $salesListCollection,
        CustomerRepositoryInterface $customerRepository,
        TimezoneInterface $timezone,
        MerchantSalesReportFactory $merchantSalesReportFactory,
        ProductRepository $productRepository,
        MerchantSalesReportCollectionFactory $merchantSalesReportCollectionFactory,
        VouchersCollectionFactory $voucherCollectionFactory
    ) {
        $this->dateTime = $_dateTime;
        $this->context = $context;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->salesListCollection = $salesListCollection;
        $this->customerRepository = $customerRepository;
        $this->timezone = $timezone;
        $this->merchantSalesReportFactory = $merchantSalesReportFactory;
        $this->productRepository = $productRepository;
        $this->merchantSalesReportCollectionFactory = $merchantSalesReportCollectionFactory;
        $this->voucherCollectionFactory = $voucherCollectionFactory;
    }

    /**
     * @return void
     * @throws LocalizedException
     * @throws Exception
     */
    public function execute()
    {
        $pageSize = 10;
        $lastId = 0;
        do {
            $orderCollection = $this->orderCollectionFactory
                ->create()
                ->addFieldToFilter('entity_id', ['gt' => $lastId])
                ->addFieldToFilter('status', ['eq' => "complete"])
                ->addFieldToFilter('increment_id', [
                    'nin' => new \Zend_Db_Expr(
                        (string)$this->merchantSalesReportCollectionFactory->create()->addFieldToSelect('order_id')->getSelect()
                    )
                ]);

            $orderCollection->setPageSize($pageSize)->setOrder('entity_id', 'asc');
            $orderCollection->load();

            foreach ($orderCollection as $order) {
                /** @var Order $order */
                $lastId = $orderId = $order->getId();

                if (defined('PHP_SAPI') && PHP_SAPI === 'cli') {
                    echo __('Processing Order #%1', $order->getIncrementId()) . "\r\n";
                }

                //get Payment data from Order
                $payment = $order->getPayment();
                $referenceNo = $payment->getData("arb_ref_id") ?: self::NOT_AVAILABLE;
                $authCode = $payment->getData("arb_auth_code") ?: self::NOT_AVAILABLE;

                $items = $order->getAllItems();
                foreach ($items as $item) {
                    $_webkulSalelists = $this->salesListCollection->create()
                        ->addFieldToFilter("seller_id", ["neq" => self::DEFAULT_STATUS])
                        ->addFieldToFilter("order_id", $orderId)
                        ->addFieldToFilter("order_item_id", $item->getItemId());

                    $_webkulSalelists
                        ->getSelect()
                        ->columns([
                            'total_actual_seller_amount' => new \Zend_Db_Expr('SUM(total_amount+total_tax)'),
                            'seller_total_commission' => new \Zend_Db_Expr('SUM(total_commission)')
                        ])
                        ->group('seller_id');

                    $_webkulSalelists->load();

                    if (defined('PHP_SAPI') && PHP_SAPI === 'cli' && count($_webkulSalelists) === 0) {
                        echo "\t" . __('No commission information found for order #', $order->getIncrementId()) . "\r\n";
                    }

                    foreach ($_webkulSalelists as $_webkulSalelist) {
                        $customerId = $_webkulSalelist["seller_id"];

                        $cortexTerminalId = self::NOT_AVAILABLE;

                        try {
                            //Get Seller Attributes to set Data for Cortex Merchant ID/Cortex Terminal ID
                            $customer = $this->customerRepository->getById($customerId);
                            $cortexTerminalId = $customer->getCustomAttribute('wkv_cortex_terminal_id') ?
                                $customer->getCustomAttribute('wkv_cortex_terminal_id')->getValue() : self::NOT_AVAILABLE;
                            $merchant_name = $customer->getFirstname() ? ($customer->getFirstname() . ' ' . $customer->getLastname()) : self::NOT_AVAILABLE;
                        } catch (NoSuchEntityException $e) {
                            $merchant_name = self::NOT_AVAILABLE . ' (removed)';
                        }

                        // getting used vouchers against one order //
                        $voucherCollection = $this->voucherCollectionFactory->create()
                            ->addFieldToSelect(['serial_number','sku','order_id'])
                            ->addFieldToFilter('sku', ['eq' => $item->getSku()])
                            ->addFieldToFilter('order_id', ['eq' => $_webkulSalelist["magerealorder_id"]]);

                        $voucherCollection->load();

                        if (defined('PHP_SAPI') && PHP_SAPI === 'cli' && count($_webkulSalelists) === 0) {
                            echo "\t" . __('No Voucher information found for order #', $order->getIncrementId()) . "\r\n";
                        }

                        foreach ($voucherCollection as $voucher) {
                            $salesData = [
                                "order_id" => $_webkulSalelist["magerealorder_id"],
                                "transaction_date" => $_webkulSalelist["created_at"],
                                "transaction_time" => $_webkulSalelist["created_at"],
                                "customer_name" => $order->getCustomerFirstname() . " " . $order->getCustomerLastName(),
                                "customer_id" => $order->getCustomerId() ?: "N/A",
                                "merchant_name" => $merchant_name,
                                "merchant_id" => $_webkulSalelist["seller_id"],
                                "product_name" => $_webkulSalelist["magepro_name"] ?? "N/A",
                                "product_price" => $_webkulSalelist["magepro_price"] ?? "N/A",
                                "authorization_number" => $authCode,
                                "cortex_terminal_id" => $cortexTerminalId,
                                "pg_transaction_reference_number" => $referenceNo,
                                "sku" => $item->getSku(),
                                "product_id" => $_webkulSalelist["mageproduct_id"],
                                "voucher_serial_numbers"=> $voucher['serial_number']
                            ];

                            if (defined('PHP_SAPI') && PHP_SAPI === 'cli') {
                                print_r($salesData);
                            }

                            // saving the sales data in cortex sales table
                            $this->merchantSalesReportFactory->create()
                                ->setData($salesData)
                                ->save();
                        }
                    }
                }
            }
        } while ($orderCollection->count());
    }
}
