<?php
/**
 * Vouchers table installer
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * This class upgrade Voucher database schema class
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        if (version_compare($context->getVersion(), '1.0.1', '<=')) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('arb_vouchers'),
                'order_id',
                'order_id',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' => 'Increment Order Id'
                ]
            );
        }
        
        $setup->endSetup();
    }
}
