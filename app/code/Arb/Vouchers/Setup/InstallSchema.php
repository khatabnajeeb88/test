<?php
/**
 * Vouchers table installer
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Sales\Setup\SalesSetupFactory;

/**
 * This class installs Voucher database schema class
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs table schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'Vouchers'
         */

        $table = $installer->getConnection()->newTable($installer->getTable('arb_vouchers'))
            ->addColumn(
                'vouchers_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Vouchers ID'
            )
            ->addColumn(
                'file_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'FILE ID'
            )
            ->addColumn(
                'sku',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'SKU'
            )
            ->addColumn(
                'serial_number',
                Table::TYPE_TEXT,
                255,
                [],
                'Serial Number'
            )
            ->addColumn(
                'voucher_code',
                Table::TYPE_TEXT,
                '2M',
                [],
                'Voucher Code'
            )
            ->addColumn(
                'voucher_expiry',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Voucher Expiry'
            )
            ->addColumn(
                'vendor_name',
                Table::TYPE_TEXT,
                64,
                [],
                'Vendor Name'
            )
            ->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'unsigned' => true, 'default' => '0'],
                'Status'
            )
            ->addColumn(
                'is_used',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false,'unsigned' => true, 'default'=> '0'],
                'is_used'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_INTEGER,
                null,
                [],
                'Order Increment Id'
            )
            ->addColumn(
                'created_time',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Creation Time'
            );

        $installer->getConnection()->createTable($table);

        $tableName = $installer->getTable('arb_vouchers');
        $fullTextIndex = ['sku', 'serial_number', 'vendor_name'];
        
        $installer->getConnection()->addIndex(
            $tableName,
            $installer->getIdxName(
                $tableName,
                $fullTextIndex,
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            $fullTextIndex,
            AdapterInterface::INDEX_TYPE_FULLTEXT
        );

        /**
         * Create table 'Arb_Vouchers_log'
         */
        $logtable = $installer->getConnection()->newTable($installer->getTable('arb_vouchers_log'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'reference_number',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'Reference Number'
            )
            ->addColumn(
                'filename',
                Table::TYPE_TEXT,
                255,
                [],
                'file Name'
            )
            ->addColumn(
                'total_records',
                Table::TYPE_INTEGER,
                255,
                [],
                'Total Voucher Records'
            )
            ->addColumn(
                'failed_records',
                Table::TYPE_INTEGER,
                255,
                [],
                'Failed Voucher Records'
            )
            ->addColumn(
                'success_records',
                Table::TYPE_INTEGER,
                255,
                [],
                'Success Voucher Records'
            )
            ->addColumn(
                'failed_voucher_data',
                Table::TYPE_TEXT,
                '2048M',
                [],
                'Failed Voucher Data'
            )
            ->addColumn(
                'vendor_email',
                Table::TYPE_TEXT,
                64,
                [],
                'Vendor Email'
            )
            ->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'unsigned' => true, 'default' => '0'],
                'Status'
            )
            ->addColumn(
                'added_by',
                Table::TYPE_TEXT,
                8,
                [],
                'Added By'
            )
            ->addColumn(
                'created_time',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Creation Time'
            );
            
        $installer->getConnection()->createTable($logtable);

        $logTableName = $installer->getTable('arb_vouchers_log');
        $logFullTextIndex = ['filename', 'reference_number', 'added_by','vendor_email'];
        
        $installer->getConnection()->addIndex(
            $logTableName,
            $installer->getIdxName(
                $logTableName,
                $logFullTextIndex,
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            $logFullTextIndex,
            AdapterInterface::INDEX_TYPE_FULLTEXT
        );

        $installer->endSetup();
    }
}
