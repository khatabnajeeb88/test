<?php
/**
 * Voucher check before placing order
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Plugin;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\Product\Type as ProductType;

/**
 * Class OrderManagement block vouchers
 */
class OrderManagement
{
    /**
     * @var \Arb\Vouchers\Model\VoucherOrders
     */
    protected $voucherCollection;

    /**
     * OrderManagement class constructor
     *
     * @param \Arb\Vouchers\Model\VoucherOrders $voucherCollection
     */
    public function __construct(
        \Arb\Vouchers\Model\VoucherOrders $voucherCollection
    ) {
        $this->voucherCollection = $voucherCollection;
    }

    /**
     * @param OrderManagementInterface $subject
     * @param OrderInterface $order
     *
     * @return OrderInterface[]
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws LocalizedException
     */
    public function beforePlace(
        OrderManagementInterface $subject,
        OrderInterface $order
    ) {
        if ($order != null) {
            $skuQtyData = [];
            foreach ($order->getAllItems() as $item) {
                if ($item->getProductType() !== ProductType::TYPE_VIRTUAL) {
                    continue;
                }

                $skuBlockData = [
                    'sku' => $item->getSku(),
                    'count' => $item->getQtyOrdered(),
                ];
                $this->voucherCollection->blockVouchers($skuBlockData, $order->getIncrementId());
            }
        }
        return [$order];
    }
}
