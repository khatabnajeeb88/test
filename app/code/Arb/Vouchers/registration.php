<?php
/**
 * Registration file for Vouchers module
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
// @codeCoverageIgnoreStart
/** it is a default magento module registration code */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_Vouchers',
    __DIR__
);
// @codeCoverageIgnoreEnd
