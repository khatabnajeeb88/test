<?php

/**
 * Model class file for order updates
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */

namespace Arb\Vouchers\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\Product\Type as ProductType;

/**
 * Arb Voucher Orders integration class
 */
class VoucherOrders extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Voucher available flag
     */
    const VOUCHER_AVAILABLE_STATUS = 1;

    /**
     * Voucher block flag
     */
    const VOUCHER_BLOCK = 1;

    /**
     * voucher not used flag
     */
    const VOUCHER_NOT_USED = 0;

    /**
     * Set voucher is used flag after success
     */
    const VOUCHER_USED = 2;

    /**
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory
     */
    protected $voucherCollection;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * @var \Magento\Sales\Model\Order\ItemFactory $itemFactory
     */
    protected $itemFactory;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Zend\Log\Writer\Stream
     */
    protected $_writer;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     * Voucher order constructor function
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $voucherCollection
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Model\OrderRepository $order
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $voucherCollection,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->voucherCollection = $voucherCollection;
        $this->json = $json;
        $this->itemFactory = $itemFactory;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     *  Returns voucher collection by SKU
     *
     * @param  string $skus
     * @return \Arb\Vouchers\Model\ResourceModel\Vouchers\Collection
     */
    protected function getVoucherCollection($skus)
    {
        $voucherCollection = null;
        if (!empty($skus)) {
            $voucherCollection = $this->voucherCollection->create();
            $voucherCollection->addFieldToSelect(['sku', 'status', 'is_used']);
            $voucherCollection->addFieldToFilter('sku', ['in' => $skus]);
            $voucherCollection->addFieldToFilter('status', self::VOUCHER_AVAILABLE_STATUS);
            $voucherCollection->addFieldToFilter('is_used', self::VOUCHER_NOT_USED);
        }
        return $voucherCollection;
    }

    /**
     * Returns Voucher availability data with
     *
     * @param  array $skuData
     * @return array
     */
    public function getVoucherAvailability($skuData)
    {
        $voucherData = [];
        if (!empty($skuData)) {
            $skus = array_keys($skuData);
            $voucherCollection = $this->getVoucherCollection($skus);
            $uniqueSku = $voucherCollection->getColumnValues('sku');
            $voucherData =  array_count_values($uniqueSku);
        }
        return $voucherData;
    }

    /**
     * Blocks voucher and sets order id
     *
     * @param  array $skuData
     * @param string $orderId
     * @return void
     * @throws LocalizedException
     */
    public function blockVouchers($skuData, $orderId)
    {
        if (isset($skuData['sku']) && isset($skuData['count'])) {
            $voucherCollection = $this->voucherCollection->create();
            $voucherCollection
                ->addFieldToSelect(['sku', 'status', 'is_used', 'order_id'])
                ->addFieldToFilter('sku', $skuData['sku'])
                ->addFieldToFilter('status', self::VOUCHER_AVAILABLE_STATUS)
                ->addFieldToFilter('is_used', self::VOUCHER_NOT_USED);

            $voucherCollection->getSelect()->limit($skuData['count']);
            
            $availableVouchers = $voucherCollection->count();
            
            if ($availableVouchers == $skuData['count']) {
                foreach ($voucherCollection as $vouchers) {
                    try {
                        $vouchers->setOrderId($orderId);
                        $vouchers->setIsUsed(self::VOUCHER_BLOCK);
                        $vouchers->save();
                    } catch (\Exception $e) {
                        $logger = $this->getLoggerFunction();
                        $logger->crit("blockVouchers :" . $e->getMessage());
                        throw new LocalizedException(
                            __('Unable to block voucher')
                        );
                    }
                }
            } else {
                $this->unBlockVouchers($orderId);
				throw new LocalizedException(
					__('Requested '.$skuData['count'].' voucher(s) for SKU '.$skuData['sku'].' are not available')
				);
			}
        }
    }

    /**
     * Assigns voucher to order
     *
     * @param  string $orderId
     * @return bool
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function assignVouchers($orderId)
    {
        $return = false;
        $vouchers = $this->voucherCollection->create();
        $vouchers->addFieldToSelect(['sku', 'order_id', 'is_used', 'voucher_code','serial_number']);
        $vouchers->addFieldToFilter('order_id', $orderId);
        $vouchers->addFieldToFilter('is_used', self::VOUCHER_BLOCK);
        if ($vouchers->getSize() > 0) {
            $skuVoucher = [];
            // Get voucher data against order increment id and set voucher used flag
            $logger = $this->getLoggerFunction();
            try {
                foreach ($vouchers as $voucher) {
                    $skuVoucher[$voucher->getSku()][$voucher->getSerialNumber()] = $voucher->getVoucherCode();                    
                    $voucher->setIsUsed(self::VOUCHER_USED);
                    $voucher->save();
                }
            } catch (\Exception $e) {
                $logger->crit("assignVouchers_1 :" . $e->getMessage());
                throw new LocalizedException(
                    __('Unable to assign vouchers to orders')
                );
            }

            try {
                // Assign voucher to order item
                $this->searchCriteriaBuilder->addFilter('increment_id', $orderId);
                $orders = $this->orderRepository->getList(
                    $this->searchCriteriaBuilder->create()
                );

                // Saving data to logs
                $logger->info("assignVouchers order id :" . $orderId);
                $logger->info("assignVouchers available vouchers :" . json_encode($skuVoucher));
                if (!empty($orders->getItems())) {
                    foreach ($orders as $orderData) {
                        foreach ($orderData->getAllItems() as $item) {
                            if ($item->getProductType() !== ProductType::TYPE_VIRTUAL) {
                                continue;
                            }
                            if (!isset($skuVoucher[$item->getSku()])) {
                                throw new LocalizedException(
                                    __('Unable to assign vouchers to orders. Order Id is not mapped with vouchers.')
                                );
                            }
                            $voucherData = $this->json->serialize($skuVoucher[$item->getSku()]);
                            $orderItem = $this->itemFactory->create()->load($item->getId());
                            $orderItem->setVouchers($voucherData);
                            $orderItem->save();
                            $logger->info("assignVouchers vouchers data :" . $item->getId() . ' --- ' . $voucherData);
                        }
                    }
                    $return = true;
                }
            } catch (\Exception $e) {
                $return = false;
                $logger->crit("assignVouchers :" . $e->getMessage());
                throw new LocalizedException(
                    __('Unable to assign vouchers to orders')
                );
            }
        }
        return $return;
    }

    /**
     * Unblock vouchers if payment is failed
     *
     * @param  int $orderId
     * @return void
     */
    public function unBlockVouchers($orderId)
    {
        $vouchers = $this->voucherCollection->create();
        $vouchers->addFieldToSelect(['sku', 'order_id', 'is_used', 'voucher_code']);
        $vouchers->addFieldToFilter('order_id', $orderId);
        $skuVoucher = [];
        // Get voucher data against order increment id and set voucher not used flag
        $logger = $this->getLoggerFunction();
        try {
            foreach ($vouchers as $voucher) {
                $skuVoucher[$voucher->getSku()][] = $voucher->getVoucherCode();
                $voucher->setIsUsed(self::VOUCHER_NOT_USED);
                $voucher->setOrderId(null);
                $voucher->save();
                $logger->info("Voucher Unblocked for OrderId: " . $orderId ." - For SKU:". $voucher->getSku());
            }
        } catch (\Exception $e) {
            $logger->info("Voucher Unblocked Error for OrderId: " . $orderId." , with error message ". $e->getMessage());            
            throw new LocalizedException(
                __('Unable to assign vouchers to orders')
            );
        }
    }
    
    /**
     * Returns log object
     *
     * @return \Zend\Log\Logger
     */
    protected function getLoggerFunction()
    {
        try {
            $this->_writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arbvouchers_Log-'.date("Ymd").'.log');
            $this->_logger = new \Zend\Log\Logger();
            return $this->_logger->addWriter($this->_writer);
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Error in log file creation')
            );
        }
    }
}
