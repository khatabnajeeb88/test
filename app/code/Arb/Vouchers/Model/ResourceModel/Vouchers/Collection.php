<?php
/**
 * Collection class for Voucher ressource model
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Model\ResourceModel\Vouchers;

/**
 * @codeCoverageIgnore
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'vouchers_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\Vouchers\Model\Vouchers::class, \Arb\Vouchers\Model\ResourceModel\Vouchers::class);
        $this->_map['fields']['vouchers_id'] = 'main_table.vouchers_id';
    }

    /**
     * Update Data for given condition for collection
     *
     * @param int|string $limit
     * @param int|string $offset
     * @return array
     */
    public function setTableRecords($condition, $columnData)
    {
        $this->getConnection()->update(
            $this->getTable('arb_vouchers'),
            ['status' => $columnData],
            ['vouchers_id IN (?)' => $condition]
        );
    }
    
    /**
     * _renderFiltersBefore
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        //@todo need to change
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $objectManager->get(\Magento\Framework\App\Request\Http::class);
        $fid = $request->getParam('file_id');
        if ($fid) {
            $this->addFieldToFilter('file_id', $fid);
        }
        parent::_renderFiltersBefore();
    }
}
