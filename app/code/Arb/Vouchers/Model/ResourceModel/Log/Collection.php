<?php
/**
 * Collection class for Vouchers log ressource model
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Model\ResourceModel\Log;

/**
 * @codeCoverageIgnore
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\Vouchers\Model\Log::class, \Arb\Vouchers\Model\ResourceModel\Log::class);
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
