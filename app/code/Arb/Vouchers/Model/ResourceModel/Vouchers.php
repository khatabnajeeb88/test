<?php
/**
 * Voucher DB adapter
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Model\ResourceModel;

class Vouchers extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     * @return $this
     */
    protected function _construct()
    {
        $this->_init('arb_vouchers', 'vouchers_id');
    }
}
