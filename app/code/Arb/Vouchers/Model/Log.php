<?php
/**
 * This file consist of class Vouchers which is used to define status of voucher.
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Model;

/**
 * class Vouchers returns status
 */
class Log extends \Magento\Framework\Model\AbstractModel
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\Vouchers\Model\ResourceModel\Log::class);
    }

    /**
     * Returns status
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        $availableOptions = ['1' => 'Approve',
                          '0' => 'Disapprove'];
        return $availableOptions;
    }
}
