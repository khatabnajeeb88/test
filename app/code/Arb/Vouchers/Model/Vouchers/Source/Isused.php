<?php
/**
 * Returns available Voucher status to GRID
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Model\Vouchers\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status used, unUsed and blocked
 */
class Isused implements OptionSourceInterface
{
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        
        $availableOptions = ['0' => 'Unused', '1' => 'Blocked', '2'=> 'Used'];
        
        $options = [];
        foreach ($availableOptions as $key => $label) {
            $options[] = [
                'label' => $label,
                'value' => $key,
            ];
        }
        return $options;
    }
}
