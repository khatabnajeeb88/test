<?php
/**
 * Returns available Voucher status to GRID
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Model\Vouchers\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Arb\Vouchers\Helper\Data;

/**
 * Class Status used yes and no
 */
class Vendorlist implements OptionSourceInterface
{

    /**
     * @var \Arb\Vouchers\Helper\Data
     */
    protected $helper;
    
    /**
     * Initialize resource model.
     *
     * @param \Arb\Vouchers\Helper\Data $helper
     */
    public function __construct(
        \Arb\Vouchers\Helper\Data $helper
    ) {
        $this->helper  = $helper;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $vendorList = $this->helper->getVendorList();

        $options[] = [
            'label' => __('Select Vendor'),
            'value' => ''
        ];
        
        foreach ($vendorList as $key => $seller) {
            $sellerLabel = $seller['name'];
            $sellerLabel .= isset($seller['shop_title'])?' ('.$seller['shop_title'].')':'';
            $options[] = [
                'label' => $sellerLabel,
                'value' => $seller['seller_id'],
            ];
        }
        
        return $options;
    }
}
