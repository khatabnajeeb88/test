<?php
/**
 * Returns available Voucher status to GRID
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Model\Vouchers\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status used to approve and disapprove voucher
 */
class Status implements OptionSourceInterface
{
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        
        $availableOptions = ['1' => 'Approved', '0' => 'Disapproved'];
        
        $options = [];
        foreach ($availableOptions as $key => $label) {
            $options[] = [
                'label' => $label,
                'value' => $key,
            ];
        }
        return $options;
    }
}
