## Synopsis
This is module for ARB Vouchers module. Vouchers are configured from Admin and merchant portal. Displayed on Email and SMS.

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).
4. Web email templates configuration details in [email_templates.xml](etc/webapi.xml).
5. Admin ACL configuration [acl.xml](etc/acl.xml).
6. Admin menu configuration [menu.xml](etc/adminhtml/menu.xml).
7. Admin menu routing [routes.xml](etc/adminhtml/routes.xml).
8. Module system configuration [system.xml](etc/adminhtml/routes.xml)
9. Frontend routing [routes.xml](etc/frontend/routes.xml)

Voucher module depends on module `ArbPayment`, `Marketplace`, `Checkout`

## Tests
Unit tests could be found in the [Test/Unit](Test/Unit) directory.


