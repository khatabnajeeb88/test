<?php
/**
 * DataTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\MailException;
use Arb\Vouchers\Helper\Email;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Arb\Vouchers\Mail\Template\TransportBuilder;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManager;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Class DataTest for testing  Vouchers class
 * @covers \Arb\Vouchers\Helper\Email
 */
class EmailTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $_scopeConfigMock;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(Context::class);
        $this->_mailException = $this->createMock(MailException::class);
        $this->_stateInterface = $this->createMock(StateInterface::class);
        $this->_driverInterface = $this->createMock(File::class);
        $this->_transportBuilder = $this->getMockBuilder(TransportBuilder::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "setTemplateIdentifier",
                                        "setTemplateOptions",
                                        "setTemplateVars",
                                        "setFrom",
                                        "addTo",
                                        "setReplyTo",
                                        "getStore",
                                        "getStoreId",
                                        "getTransport",
                                        "sendMessage",
                                        "addAttachment"
                                        ]
                                )
                                ->getMock();
        ;
        $this->_managerInterface = $this->createMock(ManagerInterface::class);
        $this->_storeManagerInterface = $this->getMockBuilder(StoreManager::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "getStore",
                                        "getStoreId",
                                        "getId"
                                        ]
                                )
                                ->getMock();
        $this->session = $this->objectManager->getObject(Session::class);
        $this->_scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);
        $this->receiverInfo= [
                                "email"=>"test@test.com",
                                "name"=>"test"
                                ];
        $this->senderInfo =   [
                                "email"=>"test@test.com",
                                "name"=>"test"
                                ];
        $this->emailTemplateVariables =   [
                                "email"=>"test@test.com",
                                "name"=>"test"
                                ];
        
        $this->_transportBuilder->expects($this->any())->method('setTemplateIdentifier')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('setTemplateOptions')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('setTemplateVars')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('addAttachment')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('setFrom')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('addTo')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('setReplyTo')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('getTransport')->willReturnSelf();
        $this->_transportBuilder->expects($this->any())->method('sendMessage')->willReturnSelf();
        $this->_storeManagerInterface->expects($this->any())->method('getStore')->willReturnSelf();
        $this->_storeManagerInterface->expects($this->any())->method('getStoreId')->willReturn(1);
        $this->_storeManagerInterface->expects($this->any())->method('getId')->willReturn(1);
        $this->_scopeConfigMock->expects($this->any())->method('getValue')->willReturn(1);
        $this->context->expects($this->any())->method('getScopeConfig')->willReturn($this->_scopeConfigMock);
        
        $this->_emailHelper = new Email(
            $this->context,
            $this->_stateInterface,
            $this->_transportBuilder,
            $this->_managerInterface,
            $this->_storeManagerInterface,
            $this->_driverInterface,
            $this->session
        );
    }

    /**
     * testGetStore method
     */
    public function testGetStore()
    {
        $this->_emailHelper->getStore();
    }

    /**
     * testGetTemplateId method
     */
    public function testGetTemplateId()
    {
        $this->_emailHelper->getTemplateId('Vouchers/email/voucher_upload_email_template');
    }

     /**
      * testGetSenderEmail method
      */
    public function testGetSenderEmail()
    {
        $this->_emailHelper->getSenderEmail();
    }

    /**
     * testGenerateTemplate method
     */
    public function testGenerateTemplate()
    {
        $this->_emailHelper->generateTemplate($this->emailTemplateVariables, $this->senderInfo, $this->receiverInfo);
        
        $this->_transportBuilder->method('sendMessage')->willThrowException(new \Exception('Error in saving logs.'));
        
        $this->_emailHelper->sendVoucherApproveMail(
            $this->emailTemplateVariables,
            $this->senderInfo,
            $this->receiverInfo
        );
        $this->_emailHelper->sendVoucherDisapproveMail(
            $this->emailTemplateVariables,
            $this->senderInfo,
            $this->receiverInfo,
            BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
            "test.csv"
        );
        $this->_emailHelper->sendVoucherMailToAdmin(
            $this->emailTemplateVariables,
            $this->senderInfo,
            $this->receiverInfo
        );
        $this->_emailHelper->sendFailedVoucherMailToMerchant(
            $this->emailTemplateVariables,
            $this->senderInfo,
            $this->receiverInfo,
            BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
            "test.csv"
        );
        $this->_emailHelper->sendFailedVoucherMailToAdmin(
            $this->emailTemplateVariables,
            $this->senderInfo,
            $this->receiverInfo,
            BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
            "test.csv"
        );
        $this->_emailHelper->getSalesTeamEmailId();
    }
}
