<?php
/**
 * DataTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Helper;

use Magento\Framework\App\Helper\Context;
use Arb\Vouchers\Helper\Data;
use Magento\Framework\File\Csv;
use Arb\Vouchers\Model\Vouchers;
use Arb\Vouchers\Helper\Email as HelperEmail;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File;
use Arb\Vouchers\Helper\CSVProcess;

/**
 * Class CSVProcessTest for testing  Vouchers class
 * @covers \Arb\Vouchers\Helper\CSVProcess
 */
class CSVProcessTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $_scopeConfigMock;

    /**
     * Main set up method
     */
    public function setUp()
    {
        
        $this->context = $this->createMock(Context::class);
       /*
        $this->_scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);
        $this->_scopeConfigMock->expects($this->any())->method('getValue')->willReturn(1);
        $this->context->expects($this->any())->method('getScopeConfig')->willReturn($this->_scopeConfigMock);      */

        $this->_csv = $this->getMockBuilder(Csv::class)
            ->setMethods(['setDelimiter' , 'setEnclosure' , 'saveData'])
            ->disableOriginalConstructor()
            ->getMock();
        ;

        $this->_csv->expects($this->any())->method('setDelimiter')->willReturnSelf();
        $this->_csv->expects($this->any())->method('setEnclosure')->willReturnSelf();
        $this->_vouchers = $this->createMock(Vouchers::class);
        $this->_data = $this->createMock(Data::class);
        $this->_helperEmail = $this->createMock(HelperEmail::class);
        $this->_directoryList = $this->createMock(DirectoryList::class);
        $this->_driverInterface = $this->createMock(File::class);

        $this->csvProcessMock = new CSVProcess(
            $this->context,
            $this->_data,
            $this->_csv,
            $this->_vouchers,
            $this->_helperEmail,
            $this->_directoryList,
            $this->_driverInterface
        );
    }

    public function testCSV()
    {
       
        $csvData[] = ["test","test2"];
        $this->csvProcessMock->getCsvData($csvData);
        $this->csvProcessMock->getVendorData("11");
        $vouchersData = [
                "Serial_Number"=>"123",
                "Voucher_Code"=>"teste",
                "Voucher_Expiry_Date"=>date("Y-m-d"),
                "Vendor"=>"test",
                "SKU_Code"=>"teste",
                "fileId"=>"123"
        ];
        $this->csvProcessMock->setVoucherData($vouchersData, 1);
        $vendorData = ["name"=>"teste","email"=>"test@tes.com"];
        $this->csvProcessMock
        ->sendFailedVoucherEmail(
            "testVoucher,
            testeVoucher1",
            $vendorData,
            BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
            true
        );
        $uploadStatus = [
            "totalRecords"=>1,
            "successRecords"=>1,
            "failedRecords"=>3
        ];

        $this->csvProcessMock->sendEmail($uploadStatus, $vendorData);
    }

    public function testFailedCase()
    {
        $csvData[0] = ["test","test2"];
        $csvData[1] = ["test","test2"];
        $this->csvProcessMock->getCsvData($csvData);
        $this->csvProcessMock->getVendorData("11");
        $vouchersData = [
                "Serial_Number"=>"123",
                "Voucher_Code"=>"teste",
                "Voucher_Expiry_Date"=>date("Y-m-d"),
                "Vendor"=>"test",
                "SKU_Code"=>"teste",
                "fileId"=>"123"
        ];
        $this->csvProcessMock->setVoucherData($vouchersData, 0);
        
        $vendorData = ["name"=>"teste","email"=>"test@tes.com"];
        $this->csvProcessMock
        ->sendFailedVoucherEmail(
            "testVoucher,
            testeVoucher1",
            $vendorData,
            BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
            false
        );
        $uploadStatus = [
            "totalRecords"=>1,
            "successRecords"=>1,
            "failedRecords"=>3
        ];

        $this->csvProcessMock->sendEmail($uploadStatus, $vendorData);
    }

    public function testProcessCSV()
    {
        $vendorId = 1;
        $fromAdmin = true;
        $defaultStatus = 1;
        $uploadedBy = "test@example.com";
        $fileData = [
                    "file"=>"test.csv",
                    "path"=>BP."/app/code/Arb/Vouchers/Test/Unit/"
                    ];
        $this->csvProcessMock->processCSV($fileData, $uploadedBy, $defaultStatus, $fromAdmin, $vendorId);
    }
}
