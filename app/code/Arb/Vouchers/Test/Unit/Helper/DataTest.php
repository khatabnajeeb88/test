<?php
/**
 * DataTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\MailException;
use Arb\Vouchers\Helper\Data;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManager;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollectionFactory;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory as VouchersCollectionFactory;
use Arb\Vouchers\Model\Log;
use Arb\Vouchers\Model\Vouchers;
use Arb\Vouchers\Model\LogFactory;
use Magento\Backend\Model\Auth\Session as AuthSession;
use Magento\Catalog\Model\Product;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem;
use Magento\CatalogInventory\Api\StockRegistryInterface;

/**
 * Class DataTest for testing  Vouchers class
 * @covers \Arb\Vouchers\Helper\Data
 */
class DataTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $_scopeConfigMock;

    /**
     * Main set up method
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(Context::class);
        $this->_mailException = $this->createMock(MailException::class);
        $this->_stateInterface = $this->createMock(StateInterface::class);
        $this->_managerInterface = $this->createMock(ManagerInterface::class);
        $this->_storeManagerInterface = $this->getMockBuilder(StoreManager::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "getStore",
                                        "getStoreId",
                                        "getId"
                                        ]
                                )
                                ->getMock();

        $this->session = $this->objectManager->getObject(CustomerSession::class);
        $this->_scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);
        $this->_scopeConfigMock->expects($this->any())->method('getValue')->willReturn(1);
        $this->context->expects($this->any())->method('getScopeConfig')->willReturn($this->_scopeConfigMock);

        $this->_sellerCollectionFactory = $this->getMockBuilder(SellerCollectionFactory::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "addFieldToFilter",
                                        "addFieldToSelect",
                                        "getTable",
                                        "getSelect",
                                        "join",
                                        "create",
                                        "getData"
                                        ]
                                )
                                ->getMock();

        $this->_sellerCollectionFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_sellerCollectionFactory->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $this->_sellerCollectionFactory->expects($this->any())->method('addFieldToSelect')->willReturnSelf();
        $this->_sellerCollectionFactory->expects($this->any())->method('getTable')->willReturn("customer_grid_flat");
        $this->_sellerCollectionFactory->expects($this->any())->method('getSelect')->willReturnSelf();
        $this->_sellerCollectionFactory->expects($this->any())->method('join')->willReturnSelf();
        $this->_sellerCollectionFactory->expects($this->any())->method('getData')->willReturnSelf();
                                
        $this->_encryptorInterface = $this->createMock(EncryptorInterface::class);
        $this->_encryptorInterface->expects($this->any())->method('encrypt')->willReturnSelf();

        $this->_dateTime = $this->createMock(DateTime::class);
        $this->_dateTime->expects($this->any())->method('date')->willReturn(date("Y-m-d"));
        $this->_vouchersCollectionFactory = $this->getMockBuilder(VouchersCollectionFactory::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "addFieldToFilter",
                                        "addFieldToSelect",
                                        "getTable",
                                        "getSelect",
                                        "join",
                                        "create",
                                        "getData",
                                        "getSize"
                                        ]
                                )
                                ->getMock();
        $this->_vouchersCollectionFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_vouchersCollectionFactory->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $this->_vouchersCollectionFactory->expects($this->any())->method('addFieldToSelect')->willReturnSelf();
        $this->_vouchersCollectionFactory->expects($this->any())->method('getSize')->willReturn(10);
        
        $this->_log = $this->createMock(Log::class);
        $this->_vouchers = $this->getMockBuilder(Vouchers::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "getCollection",
                                        "setTableRecords"
                                        ]
                                )
                                ->getMock();
        $this->_vouchers->expects($this->any())->method('getCollection')->willReturnSelf();
        $this->_vouchers->expects($this->any())->method('setTableRecords')->willReturnSelf();
        $this->_logFactory = $this->createMock(LogFactory::class);
        $this->_authSession = $this->getMockBuilder(AuthSession::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "getUser",
                                        "getUsername",
                                        "getEmail"
                                        ]
                                )
                                ->getMock();
        $this->_authSession->expects($this->any())->method('getUser')->willReturnSelf();
        $this->_authSession->expects($this->any())->method('getUsername')->willReturn("Test");
        $this->_authSession->expects($this->any())->method('getEmail')->willReturn("teste@test.com");

        $this->_product = $this->createMock(Product::class);
        $this->_productCollectionFactory = $this->getMockBuilder(ProductCollectionFactory::class)
                                ->disableOriginalConstructor()
                                ->setMethods(
                                    [
                                        "addFieldToFilter",
                                        "addFieldToSelect",
                                        "getTable",
                                        "getSelect",
                                        "join",
                                        "create",
                                        "getSize",
                                        "where"
                                        ]
                                )
                                ->getMock();

        $this->_productCollectionFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_productCollectionFactory->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $this->_productCollectionFactory->expects($this->any())->method('addFieldToSelect')->willReturnSelf();
        $this->_productCollectionFactory
                        ->expects($this->any())
                        ->method('getTable')
                        ->willReturn("catalog_product_entity");
        $this->_productCollectionFactory->expects($this->any())->method('getSelect')->willReturnSelf();
        $this->_productCollectionFactory->expects($this->any())->method('join')->willReturnSelf();
        $this->_productCollectionFactory->expects($this->any())->method('where')->willReturnSelf();
        $this->_productCollectionFactory->expects($this->any())->method('getSize')->willReturn(10);
        $this->_file = $this->createMock(File::class);
        $this->_filesystem = $this->createMock(Filesystem::class);
        $this->_stockItem = $this->getMockBuilder(\Magento\CatalogInventory\Api\Data\StockItemInterface::class)
            ->setMethods([
                'getStockItemBySku'
                ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->_stockRegistryInterface = $this->getMockBuilder(StockRegistryInterface::class)
                ->setMethods([
                'setQty',
                'setIsInStock'
                ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->_storeManagerInterface->expects($this->any())->method('getStore')->willReturnSelf();
        $this->_storeManagerInterface->expects($this->any())->method('getStoreId')->willReturn(1);
        $this->_storeManagerInterface->expects($this->any())->method('getId')->willReturn(1);
        $this->_scopeConfigMock->expects($this->any())->method('getValue')->willReturn(1);
        //To check exitisting seller
        $isSellerIdAndSkuExists = ["vm1"=>"1"];
        //To check non-existing seller
        $isSellerIdAndSkuNonExists = ["nonProduct"=>"2"];
        
        $this->_dataHelper = new Data(
            $this->context,
            $this->_stateInterface,
            $this->_managerInterface,
            $this->_storeManagerInterface,
            $this->session,
            $this->_sellerCollectionFactory,
            $this->_encryptorInterface,
            $this->_dateTime,
            $this->_vouchersCollectionFactory,
            $this->_log,
            $this->_vouchers,
            $this->_logFactory,
            $this->_authSession,
            $this->_product,
            $this->_productCollectionFactory,
            $this->_file,
            $this->_filesystem,
            $this->_stockRegistryInterface,
            $isSellerIdAndSkuExists,
            $isSellerIdAndSkuNonExists
        );
    }

    /**
     * testGetStore method
     */
    public function testGetStore()
    {
        $this->_dataHelper->getStore();
    }

    /**
     * testGetVendorList method
     */
    public function testGetVendorList()
    {
        $this->_dataHelper->getVendorList(1);
    }

     /**
      * testGetVouchersByFileId method
      */
    public function testGetVouchersByFileId()
    {
        $this->_dataHelper->getVouchersByFileId("123");
    }

    /**
     * testUpdateVouchersStatusByVoucherIds method
     */
    public function testUpdateVouchersStatusByVoucherIds()
    {
        $this->_dataHelper->updateVouchersStatusByVoucherIds(["1"], "1");
        $this->_dataHelper->getCurrentAdminUser();
        $data = ["virtual1","virtual2"];
    }

    /**
     * testAddLogs method
     */
    public function testAddLogs()
    {
        $this->_dataHelper->addLogs("123", "test.png", "10", "5", "5", "5", "test@test.com", "1", "test", "1");
           
        $this->_log->method('save')->willThrowException(new \Exception('Error in saving logs.'));
        $this->_dataHelper->addLogs("123", "test.png", "10", "5", "5", "5", "test@test.com", "1", "test", "1");
    }

     /**
      * testCheckSellerProductBySku method
      */
    public function testCheckSellerProductBySku()
    {
        $this->_dataHelper->checkSellerProductBySku("1", "1111");
        $voucher = [
                    "voucher_expiry" => "2020-05-01",
                    "voucher_code"=> "123",
                    "serial_number"=> "5555",
                    "sku"=> "vm1",
                    "vendor_name"=> "teste",
                    "shop_title"=>"tese",
                    "seller_id"=>"1"
                    ];
        $this->_dataHelper->validatVouchers($voucher, $voucher);

        $voucher = [
                    "voucher_expiry" => "2000-05-01",
                    "voucher_code"=> "",
                    "serial_number"=> "5555",
                    "sku"=> "vm1",
                    "vendor_name"=> "teste",
                    "shop_title"=>"test",
                    "seller_id"=>"1"
                    ];
        $this->_dataHelper->validatVouchers($voucher, $voucher);
        $this->_dataHelper->encrptVoucher("teste1234");
        $this->_dataHelper->convertDateFormat("Y-m-d", date("Y-m-d"));
        
        $this->_file->expects($this->any())->method('isExists')->willReturnSelf();
        $this->_file->expects($this->any())->method('deleteFile')->willReturnSelf();
        
        $this->_dataHelper->deleteUploadedFile("teste");
        $this->_dataHelper->getCurrentGmtDate();
    }

    public function testUpdateProductStock()
    {
        $data = ["test_sku"];
        $this->_stockRegistryInterface->method("getStockItemBySku")->willReturn($this->_stockItem);
        $this->_stockItem->method("setQty")->willReturn(10);
        $this->_dataHelper->updateProductStock($data);
        $this->_dataHelper->checkDateFormat("Y-m-d", date("Y-m-d"));
    }

    public function testValidaVouchers()
    {
        $this->_dataHelper->checkSellerProductBySku("1", "1111");
        $voucher = [
                    "voucher_expiry" => "2020-05-01",
                    "voucher_code"=> "123",
                    "serial_number"=> "5555",
                    "sku"=> "vm1",
                    "vendor_name"=> "teste",
                    "shop_title"=>"teste",
                    "seller_id"=>"1"
                    ];
        $this->_dataHelper->validatVouchers($voucher, $voucher);
    }
}
