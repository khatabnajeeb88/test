<?php
/**
 * FileUploaderTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Model;

use Arb\Vouchers\Model\FileUploader;

/**
 * Class FileUploaderTest for testing ImageUploader class
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class FileUploaderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Catalog\Model\ImageUploader
     */
    private $imageUploader;

    /**
     * Core file storage database
     *
     * @var \Magento\MediaStorage\Helper\File\Storage\Database|\PHPUnit_Framework_MockObject_MockObject
     */
    private $coreFileStorageDatabaseMock;

    /**
     * Media directory object (writable).
     *
     * @var \Magento\Framework\Filesystem|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mediaDirectoryMock;

    /**
     * Media directory object (writable).
     *
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mediaWriteDirectoryMock;

    /**
     * Uploader factory
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $uploaderFactoryMock;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManagerMock;

    /**
     * @var \Psr\Log\LoggerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * Base tmp path
     *
     * @var string
     */
    private $baseTmpPath;

    /**
     * Base path
     *
     * @var string
     */
    private $basePath;

    /**
     * Allowed extensions
     *
     * @var array
     */
    private $allowedExtensions;

    /**
     * Allowed mime types
     *
     * @var array
     */

    /**
     * Main setup method
     */
    protected function setUp()
    {
        $this->coreFileStorageDatabaseMock = $this->createMock(
            \Magento\MediaStorage\Helper\File\Storage\Database::class
        );
        $this->mediaDirectoryMock = $this->createMock(
            \Magento\Framework\Filesystem::class
        );
        $this->mediaWriteDirectoryMock = $this->createMock(
            \Magento\Framework\Filesystem\Directory\WriteInterface::class
        );
        $this->mediaDirectoryMock->expects($this->any())->method('getDirectoryWrite')->willReturn(
            $this->mediaWriteDirectoryMock
        );
        $this->uploaderFactoryMock = $this->createMock(
            \Magento\MediaStorage\Model\File\UploaderFactory::class
        );
        $this->storeManagerMock = $this->createMock(
            \Magento\Store\Model\StoreManagerInterface::class
        );
        $this->loggerMock = $this->createMock(\Psr\Log\LoggerInterface::class);
        $this->baseTmpPath = 'base/tmp/';
        $this->basePath =  'base/real/';
        $this->allowedExtensions = ['.jpg'];

        $this->imageUploader =
            new \Arb\Vouchers\Model\FileUploader(
                $this->coreFileStorageDatabaseMock,
                $this->mediaDirectoryMock,
                $this->uploaderFactoryMock,
                $this->storeManagerMock,
                $this->loggerMock,
                $this->baseTmpPath,
                $this->basePath,
                $this->allowedExtensions
            );
    }
    
    /**
     * testMoveFileFromTmp method
     */
    public function testMoveFileFromTmp()
    {
        $this->imageUploader->moveFileFromTmp('magento_small_image.jpg');
    }

    /**
     * testsetAllowedExtensions method
     */
    public function testsetAllowedExtensions()
    {
        $allowedExtensions = ['.jpg'];
        $this->imageUploader->setAllowedExtensions($allowedExtensions);
        $this->assertEquals($allowedExtensions, $this->allowedExtensions);
    }

    /**
     * testsetBaseTmpPath method
     */
    public function testsetBaseTmpPath()
    {
        $baseTmpPath = 'base/tmp/';
        $this->imageUploader->setBaseTmpPath($baseTmpPath);
        $this->assertEquals($baseTmpPath, $this->baseTmpPath);
    }

    /**
     * testsetBasePath method
     */
    public function testsetBasePath()
    {
        $basePath = 'base/real/';
        $this->imageUploader->setBasePath($basePath);
        $this->assertEquals($basePath, $this->basePath);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testMoveFileFromTmpWithException()
    {
        $fileId = 'file.jpg';
        $uploader = $this->createMock(\Magento\MediaStorage\Model\File\Uploader::class);

        $this->uploaderFactoryMock->expects($this->once())->method('create')->willReturn($uploader);

        $uploader->expects($this->once())->method('setAllowedExtensions')->with($this->allowedExtensions);
        $uploader->expects($this->once())->method('setAllowRenameFiles')->with(true);

        $this->mediaWriteDirectoryMock->expects($this->once())->method('getAbsolutePath')->with($this->baseTmpPath)
            ->willReturn($this->basePath);

        $uploader->expects($this->once())->method('save')->with($this->basePath)
            ->willReturn(null);

        $this->imageUploader->saveFileToTmpDir($fileId);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testMoveFileFromTmpErrorInCopy()
    {
        $fileId = 'file.jpg';
        
        $this->coreFileStorageDatabaseMock->expects($this->any())->method('copyFile')
            ->with($this->baseTmpPath.$fileId, $fileId)
            ->willThrowException(new \Exception());

        $this->imageUploader->moveFileFromTmp($fileId);
    }

    /**
     * testSaveFileToTmpDir method
     */
    public function testSaveFileToTmpDir()
    {
        $fileId = 'file.jpg';

        $uploader = $this->createMock(\Magento\MediaStorage\Model\File\Uploader::class);

        $this->uploaderFactoryMock->expects($this->once())->method('create')->willReturn($uploader);

        $uploader->expects($this->once())->method('setAllowedExtensions')->with($this->allowedExtensions);
        $uploader->expects($this->once())->method('setAllowRenameFiles')->with(true);

        $this->mediaWriteDirectoryMock->expects($this->once())->method('getAbsolutePath')->with($this->baseTmpPath)
            ->willReturn($this->basePath);

        $uploader->expects($this->once())->method('save')->with($this->basePath)
            ->willReturn(['tmp_name' => $this->baseTmpPath, 'file' => $fileId, 'path' => $this->basePath]);

        $storeMock = $this->createPartialMock(
            \Magento\Store\Model\Store::class,
            ['getBaseUrl']
        );

        $this->storeManagerMock->expects($this->once())->method('getStore')->willReturn($storeMock);
        $storeMock->expects($this->once())->method('getBaseUrl');

        $this->coreFileStorageDatabaseMock->expects($this->once())->method('saveFile');

        $result = $this->imageUploader->saveFileToTmpDir($fileId);

        $this->assertArrayNotHasKey('path', $result);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testSaveFileToTmpDirErrorInSave()
    {
            $fileId = 'file.jpg';

            $uploader = $this->createMock(\Magento\MediaStorage\Model\File\Uploader::class);

            $this->uploaderFactoryMock->expects($this->once())->method('create')->willReturn($uploader);
            
            $uploader->expects($this->once())->method('setAllowedExtensions')->with($this->allowedExtensions);
            $uploader->expects($this->once())->method('setAllowRenameFiles')->with(true);

            $this->mediaWriteDirectoryMock->expects($this->once())->method('getAbsolutePath')->with($this->baseTmpPath)
                ->willReturn($this->basePath);

            $uploader->expects($this->once())->method('save')->with($this->basePath)
                ->willReturn(['tmp_name' => $this->baseTmpPath, 'file' => $fileId, 'path' => $this->basePath]);

            $storeMock = $this->createPartialMock(
                \Magento\Store\Model\Store::class,
                ['getBaseUrl']
            );

            $this->storeManagerMock->expects($this->once())->method('getStore')->willReturn($storeMock);
            $storeMock->expects($this->once())->method('getBaseUrl');

            $this->coreFileStorageDatabaseMock->expects($this->once())->method('saveFile')
                ->with($this->baseTmpPath.$fileId)
                ->willThrowException(new \Exception());

            $this->imageUploader->saveFileToTmpDir($fileId);
    }
}
