<?php
/**
 * CollectionTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Model\ResourceModel\Vouchers;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class CollectionTest for testing Collection class
 *
 * @SuppressWarnings(PHPMD)
 * @covers \Arb\Vouchers\Model\ResourceModel\Vouchers\Collection
 */
class CollectionTest extends TestCase
{
    /**
     * Mock entityFactory
     *
     * @var \Magento\Framework\Data\Collection\EntityFactoryInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $entityFactory;

    /**
     * Mock logger
     *
     * @var \Psr\Log\LoggerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $logger;

    /**
     * Mock fetchStrategy
     *
     * @var \Magento\Framework\Data\Collection\Db\FetchStrategyInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $fetchStrategy;

    /**
     * Mock eventManager
     *
     * @var \Magento\Framework\Event\ManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManagerMock;

    /**
     * Mock connection
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $connection;

    /**
     * Mock resource
     *
     * @var \Magento\Framework\Model\ResourceModel\Db\AbstractDb|PHPUnit_Framework_MockObject_MockObject
     */
    private $resource;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $eventManager;

    /**
     * Object to test
     *
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\Collection
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {

        $this->objectManager = new ObjectManager($this);

        $this->entityFactory = $this->createMock(\Magento\Framework\Data\Collection\EntityFactoryInterface::class);
        $this->logger = $this->createMock(\Psr\Log\LoggerInterface::class);
        $this->fetchStrategy = $this->createMock(\Magento\Framework\Data\Collection\Db\FetchStrategyInterface::class);
        $this->eventManager = $this->createMock(\Magento\Framework\Event\ManagerInterface::class);
        //$this->connection = $this->createMock(\Magento\Framework\DB\Adapter\AdapterInterface::class);
        /*$this->connection = $this->getMockBuilder(
            \Magento\Framework\DB\Adapter\AdapterInterface::class
        )->disableOriginalConstructor()->getMock();*/

        $this->resource = $this->createMock(\Magento\Framework\Model\ResourceModel\Db\AbstractDb::class);
        /*$this->resource = $this->getMockBuilder(
            \Magento\Framework\Model\ResourceModel\Db\AbstractDb::class
        )->disableOriginalConstructor()->getMock();*/
    }

    /**
     * testBannerId method
     */
    public function testBannerId()
    {
        $bannerId = 1;
        $expected = 1;
        $this->assertEquals($expected, $bannerId);
    }
}
