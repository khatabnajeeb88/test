<?php
namespace Arb\Vouchers\Test\Unit\Model\ResourceModel;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Vouchers\Model\ResourceModel\Log
 */
class LogTest extends TestCase
{
    /**
     * Mock context
     *
     * @var \Magento\Framework\Model\ResourceModel\Db\Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Vouchers\Model\ResourceModel\Log
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\Model\ResourceModel\Db\Context::class);
        $this->testObject = $this->objectManager->getObject("Arb\Vouchers\Model\ResourceModel\Log");
    }

    public function testLoad()
    {
        $this->testObject->getConnection();
    }
}
