<?php
/**
 * StatusTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Model\Vouchers\Source;

use Arb\Vouchers\Model\Vouchers\Source\Status;

/**
 * Class StatusTest for testing Status class
 */
class StatusTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Summary
     */
    private $model;

    /**
     * main setup method
     */
    protected function setUp()
    {
        $this->model = new Status();
    }

    /**
     * testToOptionArray method
     */
    public function testToOptionArray()
    {
        
        $this->model->toOptionArray();
    }
}
