<?php
/**
 * VendorlistTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Model\Vouchers\Source;

use Arb\Vouchers\Model\Vouchers\Source\Vendorlist;
use Magento\Framework\Data\OptionSourceInterface;
use Arb\Vouchers\Helper\Data;

/**
 * Class VendorlistTest for testing Status class
 */
class VendorlistTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Summary
     */
    private $model;

    /**
     * main setup method
     */
    protected function setUp()
    {
        $this->helper = $this->createMock(Data::class);
        $options[] = ["shop_title"=>"test",
                        "seller_id"=>1,
                        "name"=>"teste"];
        $this->helper->expects($this->any())->method('getVendorList')->willReturn($options);
        $this->model = new Vendorlist($this->helper);
    }

    /**
     * testToOptionArray method
     */
    public function testToOptionArray()
    {
        
        $this->model->toOptionArray();
    }
}
