<?php
/**
 * IsusedTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Model\Vouchers\Source;

use Arb\Vouchers\Model\Vouchers\Source\Isused;

/**
 * Class IsusedTest for testing Status class
 */
class IsusedTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Summary
     */
    private $model;

    /**
     * main setup method
     */
    protected function setUp()
    {
        $this->model = new Isused();
    }

    /**
     * testToOptionArray method
     */
    public function testToOptionArray()
    {
        
        $this->model->toOptionArray();
    }
}
