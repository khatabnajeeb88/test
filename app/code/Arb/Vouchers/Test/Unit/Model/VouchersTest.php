<?php
/**
 * VouchersTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Model;

use Arb\Vouchers\Model\Vouchers;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * This class used for PHPUnit testing of \Arb\Vouchers\Model\Vouchers class
 * @covers \Arb\Vouchers\Model\Vouchers
 */
class VouchersTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject("Arb\Vouchers\Model\Vouchers");
    }

    /**
     * testGetAvailableStatuses method
     */
    public function testGetAvailableStatuses()
    {
        $status = $this->model->getAvailableStatuses();
        $this->assertEquals(['1' => 'Approve', '0' => 'Disapprove'], $status);
    }
}
