<?php
namespace Arb\Vouchers\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory as VouchersCollectionFactory;
use Arb\Vouchers\Model\ResourceModel\Vouchers\Collection as VouchersCollection;
use Magento\Framework\DB\Select;

/**
 * @covers \Arb\Vouchers\Model\VoucherOrders
 */
class VoucherOrdersTest extends TestCase
{
    /**
     * Mock context
     *
     * @var \Magento\Framework\Model\Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * Mock voucherCollectionInstance
     *
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\Collection|PHPUnit_Framework_MockObject_MockObject
     */
    private $voucherCollectionInstance;

    /**
     * Mock voucherCollection
     *
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $voucherCollection;

    /**
     * Mock resource
     *
     * @var \Magento\Framework\Model\ResourceModel\AbstractResource|PHPUnit_Framework_MockObject_MockObject
     */
    private $resource;

    /**
     * Mock resourceCollection
     *
     * @var \Magento\Framework\Data\Collection\AbstractDb|PHPUnit_Framework_MockObject_MockObject
     */
    private $resourceCollection;

    /**
     * Mock registry
     *
     * @var \Magento\Framework\Registry|PHPUnit_Framework_MockObject_MockObject
     */
    private $registry;

    /**
     * Mock json
     *
     * @var \Magento\Framework\Serialize\Serializer\Json|PHPUnit_Framework_MockObject_MockObject
     */
    private $json;

    /**
     * Mock itemFactoryInstance
     *
     * @var \Magento\Sales\Model\Order\Item|PHPUnit_Framework_MockObject_MockObject
     */
    private $itemFactoryInstance;

    /**
     * Mock itemFactory
     *
     * @var \Magento\Sales\Model\Order\ItemFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $itemFactory;

    /**
     * Mock orderRepository
     *
     * @var \Magento\Sales\Model\OrderRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderRepository;

    /**
     * Mock searchCriteriaBuilder
     *
     * @var \Magento\Framework\Api\SearchCriteriaBuilder|PHPUnit_Framework_MockObject_MockObject
     */
    private $searchCriteriaBuilder;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Vouchers\Model\VoucherOrders
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\Model\Context::class);
        $this->voucherCollection = $this->getMockBuilder(VouchersCollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create"])
            ->getMock();
        $this->selectMock = $this->createMock(Select::class);
        $this->vouchers = $this->createMock(\Arb\Vouchers\Model\Vouchers::class);
        $this->voucherCollectionMock = $this->objectManager->getCollectionMock(VouchersCollection::class, [$this->vouchers]);
        $this->voucherCollection->method('create')->willReturn($this->voucherCollectionMock);
        $sku = ["1001"];
        $this->voucherCollectionMock->method('getColumnValues')->willReturn($sku);
        $this->resource = $this->createMock(\Magento\Framework\Model\ResourceModel\AbstractResource::class);
        $this->resourceCollection = $this->createMock(\Magento\Framework\Data\Collection\AbstractDb::class);
        $this->registry = $this->createMock(\Magento\Framework\Registry::class);
        $this->json = $this->createMock(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->itemFactory = $this->createMock(\Magento\Sales\Model\Order\ItemFactory::class);
        
        $this->itemFactory->method('create')->willReturnSelf();
        $this->orderRepository = $this->getMockBuilder(\Magento\Sales\Model\OrderRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([
                            'getList',
                            "getItems",
                            "getAllItems"
                        ])
            ->getMock();
        $this->searchCriteriaInterface = $this->createMock(\Magento\Framework\Api\SearchCriteriaInterface::class);
        $this->searchCriteriaBuilder = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods([
                            'addFilter',
                            "create"
                        ])
            ->getMock();
        $this->searchCriteriaBuilder->method('create')->willReturn($this->searchCriteriaInterface);
        $this->testObject = $this->objectManager->getObject(
            \Arb\Vouchers\Model\VoucherOrders::class,
            [
                'context' => $this->context,
                'voucherCollection' => $this->voucherCollection,
                'resource' => $this->resource,
                'resourceCollection' => $this->resourceCollection,
                'registry' => $this->registry,
                'json' => $this->json,
                'itemFactory' => $this->itemFactory,
                'orderRepository' => $this->orderRepository,
                'searchCriteriaBuilder' => $this->searchCriteriaBuilder,
            ]
        );
    }

    public function testGetVoucherAvailability()
    {
        $sku = ["1001"];
        $this->searchCriteriaBuilder->method('addFilter')->willReturnSelf();
        $this->testObject->getVoucherAvailability($sku);
    }

    public function testUnBlockVouchers()
    {
        $this->searchCriteriaBuilder->method('addFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToSelect')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToFilter')->willReturnSelf();
        $this->testObject->unBlockVouchers("100");
    }

    /**
     * @expectedException Magento\Framework\Exception\LocalizedException
     */
    public function testUnBlockVouchersException()
    {
        $this->searchCriteriaBuilder->method('addFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToSelect')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToFilter')->willReturnSelf();
        $this->vouchers->method("save")->willThrowException(new \Exception("teste"));
        $this->testObject->unBlockVouchers("100");
    }

    public function testAssignVouchers()
    {
        $this->searchCriteriaBuilder->method('addFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToSelect')->willReturnSelf();
        $this->voucherCollectionMock->expects($this->at(1))->method('addFieldToFilter')->willReturnSelf();
        $orders = [$this->orderRepository];
        $items = [$this->itemFactory];
        $this->voucherCollectionMock->expects($this->at(2))->method('addFieldToFilter')->willReturn($orders);
        $this->orderRepository->method("getList")->willReturnSelf();
        $this->orderRepository->method("getItems")->willReturn($orders);
        $this->orderRepository->method("getAllItems")->willReturn($items);
        $this->voucherCollectionMock->method('getSize')->willReturn(10);
        $this->testObject->assignVouchers("100");
    }
    
    public function testBlockVouchers()
    {
        $this->searchCriteriaBuilder->method('addFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToSelect')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('getSelect')->willReturn($this->selectMock);
        $this->voucherCollectionMock->method('count')->willReturn("10");
        $sku = ["sku"=>"1001","count"=>"10"];
        $this->testObject->blockVouchers($sku, "100");
    }

    /**
     * @expectedException Magento\Framework\Exception\LocalizedException
     */
    public function testBlockVouchersError()
    {
        $this->searchCriteriaBuilder->method('addFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToSelect')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('getSelect')->willReturn($this->selectMock);
        $this->vouchers->method("save")->willThrowException(new \Exception("teste"));
        $sku = ["sku"=>"1001","count"=>"10"];
        $this->testObject->blockVouchers($sku, "100");
    }
    /**
     * @expectedException Magento\Framework\Exception\LocalizedException
     */
    public function testAssignVouchersExcpetion()
    {
        $this->searchCriteriaBuilder->method('addFilter')->willReturnSelf();
        $this->voucherCollectionMock->method('addFieldToSelect')->willReturnSelf();
        $this->voucherCollectionMock->expects($this->at(1))->method('addFieldToFilter')->willReturnSelf();
        $orders = [$this->orderRepository];
        $items = [$this->itemFactory];
        $this->voucherCollectionMock->expects($this->at(2))->method('addFieldToFilter')->willReturn($orders);
        $this->orderRepository->method("getList")->willReturnSelf();
        $this->orderRepository->method("getItems")->willReturn($orders);
        $this->orderRepository->method("getAllItems")->willReturn($items);
        $this->voucherCollectionMock->method('getSize')->willReturn(10);
        $this->vouchers->method("save")->willThrowException(new \Exception("teste"));
        $this->testObject->assignVouchers("100");
    }

    public function testAssignVouchersExcpetionList()
    {
        $this->voucherCollectionMock->method('addFieldToSelect')->willReturnSelf();
        $this->voucherCollectionMock->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $this->searchCriteriaBuilder->method('addFilter')->willThrowException(new \Exception("teste"));
        $this->testObject->assignVouchers("100");
    }
}
