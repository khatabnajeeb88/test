<?php
/**
 * VouchersTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Model;

use Arb\Vouchers\Model\Log;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class VouchersTest for testing  Vouchers class
 * @covers \Arb\Vouchers\Model\Log
 */
class LogTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject(Log::class);
    }

    /**
     * testGetAvailableStatuses method
     */
    public function testGetAvailableStatuses()
    {
        $status = $this->model->getAvailableStatuses();
         $availableOptions = ['1' => 'Approve',
                          '0' => 'Disapprove'];
         $this->assertEquals($availableOptions, $status);
    }
}
