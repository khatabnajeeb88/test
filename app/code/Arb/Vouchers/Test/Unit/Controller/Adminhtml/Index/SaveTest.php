<?php
/**
 * SaveTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Controller\Adminhtml\Index;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Vouchers\Helper\CSVProcess;

/**
 * Class SaveTest for testing Save class
 * @SuppressWarnings(PHPMD)
 * @covers \Arb\Vouchers\Controller\Adminhtml\Index\Save
 */
class SaveTest extends TestCase
{
    
    /**
     * @var \Magento\Framework\App\RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $requestMock;

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $dataPersistorMock;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultRedirect;

    /**
     * @var \Magento\Backend\App\Action\Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $contextMock;

    /**
     * @var \Magento\Framework\ObjectManager\ObjectManager|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $objectManagerMock;

    /**
     * @var \Magento\Framework\Message\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $messageManagerMock;

    /**
     * @var \Magento\Framework\Event\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $eventManagerMock;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \Magento\Cms\Controller\Adminhtml\Block\Save
     */
    protected $saveController;

    /**
     * @var \Arb\Vouchers\Model\Vouchers|\PHPUnit_Framework_MockObject_MockObject
     */
    private $VouchersFactory;

    /**
     * @var \Magento\Framework\Escaper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $escaper;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime|\PHPUnit_Framework_MockObject_MockObject
     */
    private $dateFactoryInstance;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTimeFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $dateFactory;

    /**
     * @var int
     */
    protected $blockId = 1;

    protected function setUp()
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->contextMock = $this->createMock(\Magento\Backend\App\Action\Context::class);
        $this->escaper = $this->createMock(\Magento\Framework\Escaper::class);
        $this->inlineTranslation = $this->createMock(\Magento\Framework\Translate\Inline\StateInterface::class);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->dateFactoryInstance = $this->createMock(\Magento\Framework\Stdlib\DateTime\DateTime::class);
        $this->dateFactory = $this->createMock(\Magento\Framework\Stdlib\DateTime\DateTimeFactory::class);
        $this->dateFactory->method('create')->willReturn($this->dateFactoryInstance);
        $this->csvparser = $this->createMock(\Magento\Framework\File\Csv::class);
        $this->directoryList = $this->createMock(\Magento\Framework\App\Filesystem\DirectoryList::class);
        $this->vouchers = $this->createMock(\Arb\Vouchers\Model\Vouchers::class);
        $this->vouchersEmail = $this->createMock(\Arb\Vouchers\Helper\Email::class);
        $this->vouchersData = $this->createMock(\Arb\Vouchers\Helper\Data::class);
        $this->csvProcessMock = $this->createMock(CSVProcess::class);
        $this->resultRedirectFactory = $this->getMockBuilder(\Magento\Backend\Model\View\Result\RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->resultRedirect = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Redirect::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultRedirectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultRedirect);

        $this->vouchersMock = $this->getMockBuilder(\Arb\Vouchers\Model\Vouchers::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->dataPersistorMock = $this->getMockBuilder(\Magento\Framework\App\Request\DataPersistorInterface::class)
            ->getMock();

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['getParam', 'getPostValue']
        );

        $this->messageManagerMock = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);

        $this->eventManagerMock = $this->getMockForAbstractClass(
            \Magento\Framework\Event\ManagerInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['dispatch']
        );

        $this->objectManagerMock = $this->getMockBuilder(\Magento\Framework\ObjectManager\ObjectManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['get', 'create'])
            ->getMock();

        $this->objectManagerMock->expects($this->any())
            ->method('create')
            ->with(\Arb\Vouchers\Model\Vouchers::class)
            ->willReturn($this->vouchersMock);

        $this->contextMock->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $this->contextMock->expects($this->any())->method('getObjectManager')->willReturn($this->objectManagerMock);
        $this->contextMock->expects($this->any())->method('getMessageManager')->willReturn($this->messageManagerMock);
        $this->contextMock->expects($this->any())->method('getEventManager')->willReturn($this->eventManagerMock);
        $this->contextMock->expects($this->any())
            ->method('getResultRedirectFactory')
            ->willReturn($this->resultRedirectFactory);

        $this->VouchersFactory = $this->getMockBuilder(\Arb\Vouchers\Model\VouchersFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        
        $this->saveController =  new \Arb\Vouchers\Controller\Adminhtml\Index\Save(
            $this->contextMock,
            $this->dataPersistorMock,
            $this->escaper,
            $this->inlineTranslation,
            $this->scopeConfig,
            $this->dateFactory,
            $this->directoryList,
            $this->csvProcessMock
        );
    }

    public function testExecute()
    {
        $postData["import"][0] = ["name"=>"teste","tmp_name"=>"temteste","size"=>"123"];
        $postData["vendor_id"] = 1;
        $this->requestMock->expects($this->any())->method('getPostValue')->willReturn($postData);
        $this->requestMock->expects($this->any())
            ->method('getParam')
            ->willReturnMap(
                [
                    ['Vouchers_id', null, 1],
                    ['back', null, 'continue'],
                ]
            );

            $vouchersId = 1;
        $this->vouchersMock->expects($this->any())
            ->method('load')
            ->with($vouchersId);

        $this->vouchersMock->expects($this->any())
            ->method('getId')
            ->willReturn($vouchersId);

        $this->dataPersistorMock->expects($this->any())
            ->method('clear')
            ->with('Arb_Vouchers');

        $this->messageManagerMock->expects($this->any())
            ->method('addSuccess')
            ->with(__('File upload failed. Please try again.'));

        $this->dataPersistorMock->expects($this->any())
            ->method('set')
            ->with('Arb_Vouchers', $postData);

        $this->resultRedirect->expects($this->any())->method('setPath')->with('*/*/') ->willReturnSelf();
        $rows[]["data"] = [
            "vendor_id"=>"teste",
            "vendor_id"=>"teste",
            "vendor_id"=>"teste",
            ];
        $this->csvparser->method('getData')->willReturn($rows);
        $this->vouchersData->expects($this->any())->method('getVendorList')->willReturn($rows);
        $this->saveController->execute();
    }

    public function testExecuteFailed()
    {
    }
}
