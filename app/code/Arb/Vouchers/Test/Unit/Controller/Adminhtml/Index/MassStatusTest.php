<?php
/**
 * EditTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Controller\Adminhtml\Index;

use Magento\Cms\Test\Unit\Controller\Adminhtml\AbstractMassActionTest;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory;
use Arb\Vouchers\Model\ResourceModel\Log\CollectionFactory as LogCollectionFactory;
use Arb\Vouchers\Helper\Email;
use Arb\Vouchers\Helper\Data;
use Arb\Vouchers\Helper\CSVProcess;
use Arb\Vouchers\Controller\Adminhtml\Index\MassStatus;

/**
 * Class MassStatusTest for testing Edit class
 * @covers \Arb\Vouchers\Controller\Adminhtml\Index\MassStatus
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class MassStatusTest extends AbstractMassActionTest
{
    /**
     * @var \Arb\Vouchers\Controller\Adminhtml\Index\MassStatus
     */
    protected $massStatusController;

    /**
     * @var \Magento\Framework\Message\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $messageManagerMock;

    /**
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $collectionFactoryMock;

    /**
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\Collection|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $voucherCollectionMock;

    /** @var \Magento\Framework\App\RequestInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $requestMock;

    /**
     * @var \Magento\Framework\Controller\ResultFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultFactoryMock;

    /**
     * main setup method
     */
    protected function setUp()
    {
        parent::setUp();
        $this->filterMock = $this->getMockBuilder(\Magento\Ui\Component\MassAction\Filter::class)
            ->setMethods(["getCollection"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->collectionFactoryMock = $this->createPartialMock(
            \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory::class,
            ['create']
        );

        $this->resultFactoryMock = $this->createPartialMock(
            \Magento\Framework\Controller\ResultFactory::class,
            ['create']
        );

        $this->itemOneMock = $this->createMock(\Arb\Vouchers\Model\ResourceModel\Log\Collection::class);

        $this->resultFactoryMock->expects($this->any())
            ->method('create')
            ->willReturnMap(
                [
                    [ResultFactory::TYPE_REDIRECT, [], $this->resultRedirectMock]
                ]
            );

        $this->voucherCollectionMock = $this->createMock(\Arb\Vouchers\Model\ResourceModel\Vouchers\Collection::class);
        $this->messageManagerMock = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            []
        );

        $this->contextMock = $this->createMock(Context::class);
        $this->emailHelperMock = $this->createMock(Email::class);
        $this->logCollectionFactoryMock = $this->getMockBuilder(LogCollectionFactory::class)
            ->setMethods(["create"])
            ->disableOriginalConstructor()
            ->getMock();

        $this->logCollectionFactoryMock->expects($this->any())->method('create')->willReturnSelf();
        $this->filterMock->expects($this->any())->method('getCollection')->willReturn([$this->itemOneMock]);
            
        $this->vouchersHelperMock = $this->createMock(Data::class);
        $this->csvProcessMock = $this->createMock(CSVProcess::class);
        $this->contextMock->expects($this->once())->method('getRequest')->willReturn($this->requestMock);
        $this->contextMock->expects($this->once())->method('getMessageManager')->willReturn($this->messageManagerMock);
        $this->contextMock->expects($this->any())->method('getResultFactory')->willReturn($this->resultFactoryMock);

        $this->massStatusController = new MassStatus(
            $this->contextMock,
            $this->filterMock,
            $this->collectionFactoryMock,
            $this->emailHelperMock,
            $this->logCollectionFactoryMock,
            $this->vouchersHelperMock,
            $this->csvProcessMock
        );
    }

    /**
     * testSendEmail method
     */
    public function testSendEmail()
    {
        $this->massStatusController->sendEmail(
            "teste@test.com",
            "123",
            1,
            BP."/app/code/Arb/Vouchers/Test/Unit/test.csv"
        );
        $this->massStatusController->sendEmail(
            "teste@test.com",
            "123",
            0,
            BP."/app/code/Arb/Vouchers/Test/Unit/test.csv"
        );
    }

    /**
     * testExecute method
     */
   /* public function testExecute()
    {
        $this->massStatusController->execute();
    }*/
}
