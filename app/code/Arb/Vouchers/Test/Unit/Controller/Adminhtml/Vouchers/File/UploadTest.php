<?php
/**
 * EditTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Controller\Adminhtml\Vouchers\File;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Backend\App\Action\Context;
use Arb\Vouchers\Model\FileUploader;
use Arb\Vouchers\Controller\Adminhtml\Vouchers\File\Upload;
use Magento\Cms\Test\Unit\Controller\Adminhtml\AbstractMassActionTest;
use Magento\Backend\Model\Auth\Session as AuthSession;

/**
 * Class DataTest for testing  Vouchers class
 * @covers \Arb\Vouchers\Controller\Adminhtml\Vouchers\File\Upload
 */
class UploadTest extends AbstractMassActionTest
{
     /**
      * Main set up method
      */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->imageUploader = $this->createMock(FileUploader::class);
        
        $this->_authSession = $this->getMockBuilder(\Magento\Backend\Model\Session::class)
            ->setMethods([
                                        "getName",
                                        "getSessionId",
                                        "getCookieLifetime",
                                        "getCookiePath",
                                        "getCookieDomain"
                                        ])
            ->setConstructorArgs($this->objectManager->getConstructArguments(\Magento\Backend\Model\Session::class))
            ->getMock();

        $args = $this->objectManager->getConstructArguments(
            \Magento\Backend\App\Action\Context::class,
            [
                'session' => $this->_authSession
            ]
        );
        $this->context = $this->getMockBuilder(Context::class)
            ->setMethods(['getRequest', 'getResponse', 'getMessageManager', 'getSession', 'getResultFactory'])
            ->setConstructorArgs($args)
            ->getMock();
        $this->resultFactory = $this->getMockBuilder(ResultFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create',"setData"])
            ->getMock();
        
        $this->imageUploader->expects($this->any())->method('saveFileToTmpDir')->willReturn(["import"=>"teste"]);
    
        $this->context->expects($this->once())->method('getSession')->willReturn($this->_authSession);
        
        $this->context->expects($this->once())->method('getResultFactory')->willReturn($this->resultFactory);

        $this->resultFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->resultFactory->expects($this->any())->method('setData')->willReturnSelf();
        $this->_authSession->expects($this->any())->method('getName')->willReturn("Test");
        $this->_authSession->expects($this->any())->method('getSessionId')->willReturn("Test");
        $this->_authSession->expects($this->any())->method('getCookieLifetime')->willReturn("100");
        $this->_authSession->expects($this->any())->method('getCookiePath')->willReturn("10");
        $this->_authSession->expects($this->any())->method('getCookieDomain')->willReturn("123");

        $this->upload = new Upload(
            $this->context,
            $this->imageUploader
        );
    }

    public function testExecute()
    {
        
        $this->upload->execute();
        $this->imageUploader->expects(
            $this->any()
        )
            ->method('saveFileToTmpDir')
            ->willThrowException(new \Exception('Error in saving logs.'));
        $this->upload->execute();
    }
}
