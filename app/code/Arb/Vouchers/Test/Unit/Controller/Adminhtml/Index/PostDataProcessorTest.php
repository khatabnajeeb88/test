<?php
/**
 * PostDataProcessorTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Controller\Adminhtml\Index;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class PostDataProcessorTest for testing PostDataProcessor class
 * @covers \Arb\Vouchers\Controller\Adminhtml\Index\PostDataProcessor
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class PostDataProcessorTest extends TestCase
{
    /**
     * Mock dateFilter
     *
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date|PHPUnit_Framework_MockObject_MockObject
     */
    private $dateFilter;

    /**
     * Mock messageManager
     *
     * @var \Magento\Framework\Message\ManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $messageManager;

    /**
     * Mock validatorFactoryInstance
     *
     * @var \Magento\Framework\View\Model\Layout\Update\Validator|PHPUnit_Framework_MockObject_MockObject
     */
    private $validatorFactoryInstance;

    /**
     * Mock validatorFactory
     *
     * @var \Magento\Framework\View\Model\Layout\Update\ValidatorFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $validatorFactory;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Vouchers\Controller\Adminhtml\Index\PostDataProcessor
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->dateFilter = $this->createMock(\Magento\Framework\Stdlib\DateTime\Filter\Date::class);
        $this->messageManager = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);
        $validatorMock = \Magento\Framework\View\Model\Layout\Update\Validator::class;
        $this->validatorFactoryInstance = $this->createMock($validatorMock);
        $validatorFactoryMock = \Magento\Framework\View\Model\Layout\Update\ValidatorFactory::class;
        $this->validatorFactory = $this->createMock($validatorFactoryMock);
        $this->validatorFactory->method('create')->willReturn($this->validatorFactoryInstance);
        $this->testObject = $this->objectManager->getObject(
            \Arb\Vouchers\Controller\Adminhtml\Index\PostDataProcessor::class,
            [
                'dateFilter' => $this->dateFilter,
                'messageManager' => $this->messageManager,
                'validatorFactory' => $this->validatorFactory,
            ]
        );
    }

    /**
     * testValidateRequireEntry method
     */
    public function testValidateRequireEntry()
    {
        $array = ['title' => '','status' => __('Status')];
        $bannerObject = $this->createMock(\Arb\Vouchers\Controller\Adminhtml\Index\PostDataProcessor::class);
        $map = false;
        $bannerObject->method('validateRequireEntry')->willReturn($map);
    
        $this->assertEquals(
            $bannerObject->validateRequireEntry($array),
            $this->testObject->validateRequireEntry($array)
        );
    }
}
