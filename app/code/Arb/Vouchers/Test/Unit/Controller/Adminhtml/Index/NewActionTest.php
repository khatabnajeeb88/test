<?php
/**
 * NewActionTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Controller\Adminhtml\Index;

use Arb\Vouchers\Controller\Adminhtml\Index\NewAction;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Backend\Block\Cache\Permissions;

/**
 * Class NewActionTest for testing NewAction class
 * @covers \Arb\Vouchers\Controller\Adminhtml\Index\NewAction
 */
class NewActionTest extends \PHPUnit\Framework\TestCase
{

     /**
      * @var \Magento\Framework\AuthorizationInterface
      */
    protected $authorizationMock;

    /**
     * @var Permissions
     */
    private $permissions;

      /**
       * @var \Magento\Framework\Translate\Inline
       */
    protected $_model;

    /**
     * Main set up method
     */
    public function setUp()
    {

        $this->_authorizationMock = $this->getMockBuilder(\Magento\Framework\AuthorizationInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['isAllowed'])
            ->getMock();

        $this->context = $this->createMock(\Magento\Backend\App\Action\Context::class);

        $this->resultForward = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Forward::class)
            ->disableOriginalConstructor()
            ->getMock();

        $resultForwardFactory = $this->getMockBuilder(\Magento\Backend\Model\View\Result\ForwardFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $resultForwardFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultForward);

        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->permissions = new Permissions($this->_authorizationMock);

        $this->action = (new ObjectManager($this))->getObject(
            \Arb\Vouchers\Controller\Adminhtml\Index\NewAction::class,
            [
                'context' => $this->context,
                'resultForwardFactory' => $resultForwardFactory,
                'authorization' => $this->_authorizationMock,
            ]
        );
    }

    /**
     * testExecute method
     */
    public function testExecute()
    {
        $this->action->execute();
    }

    /**
     * @return array
     */
    public function isAllowedDataProvider()
    {
        return [
            [true],
            [false],
        ];
    }
}
