<?php
/**
 * UploadTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SaleslistColl;
use Webkul\Marketplace\Model\ResourceModel\Saleperpartner\CollectionFactory;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Helper\Email as HelperEmail;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\RequestInterface;
use Arb\Vouchers\Controller\Index\Upload;

/**
 * Class UploadTest for testing View class
 * @covers \Arb\Vouchers\Controller\Index\Upload
 */
class UploadTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test setUp
     */
    protected function setUp()
    {
        $this->context = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->formKeyValidator = $this->createMock(FormKeyValidator::class);
        $this->_helperData = $this->getMockBuilder(HelperData::class)
            ->disableOriginalConstructor()
            ->setMethods(['getIsSeparatePanel',
                            "isSeller"])
            ->getMock();
        $this->_helperEmail = $this->createMock(HelperEmail::class);
        $this->_saleslistColl = $this->createMock(SaleslistColl::class);
        $this->_collectionFactory = $this->createMock(CollectionFactory::class);
        $this->_customerRepository = $this->createMock(CustomerRepositoryInterface::class);
        $this->resultPageFactory = $this->initResultPage();
        $this->_customerSession = $this->createMock(\Magento\Customer\Model\Session::class);
       
        $this->_customerUrl = $this->createMock(CustomerUrl::class);
        $this->_requestInterface = $this->createMock(RequestInterface::class);

        $this->resultRedirectFactory = $this->getMockBuilder(\Magento\Backend\Model\View\Result\RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->resultRedirect = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Redirect::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultRedirectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultRedirect);
        $this->context->expects($this->any())
            ->method('getResultRedirectFactory')
            ->willReturn($this->resultRedirectFactory);

        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
         $this->urlBuilderMock->expects($this->any())
            ->method('getUrl')
            ->with('marketplace/account/login/')
            ->willReturnArgument(1);
        $this->context->expects($this->any())
            ->method('getUrl')
            ->willReturn($this->urlBuilderMock);

        $this->_customerSession->expects($this->any())
            ->method('authenticate')
            ->willReturn(1);

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['isSecure',"getFullActionName"]
        );
        $this->requestMock->expects($this->any())->method('isSecure')->willReturn(1);
        $this->context->expects($this->any())->method('getRequest')->willReturn($this->requestMock);

        $this->upload = new Upload(
            $this->context,
            $this->formKeyValidator,
            $this->_helperData,
            $this->_helperEmail,
            $this->_saleslistColl,
            $this->_collectionFactory,
            $this->_customerRepository,
            $this->resultPageFactory,
            $this->_customerSession,
            $this->_customerUrl
        );
    }

     /**
      * @return \Magento\Framework\View\Result\PageFactory|\PHPUnit_Framework_MockObject_MockObject
      */
    protected function initResultPage()
    {
        $this->resultPage = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Page::class)
            ->disableOriginalConstructor()
            ->getMock();

        $resultPageFactory = $this->getMockBuilder(\Magento\Framework\View\Result\PageFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $resultPageFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultPage);
        return $resultPageFactory;
    }

    /**
     * testExecute method
     */
    public function testExecute()
    {
        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(false);
        $this->upload->execute();
    }

    /**
     * testExecuteWithSeller method
     */
    public function testExecuteWithSeller()
    {

        $this->_helperData->expects($this->any())
            ->method('getIsSeparatePanel')
            ->willReturn(1);

         $pageTitle = $this->getMockBuilder(\Magento\Framework\View\Page\Title::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pageTitle->expects($this->any())
            ->method('prepend')
            ->with(__('Marketplace Mass Upload Vouchers'))
            ->willReturnSelf();

        $pageConfig = $this->getMockBuilder(\Magento\Framework\View\Page\Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pageConfig->expects($this->any())
            ->method('getTitle')
            ->willReturn($pageTitle);
            
        $this->resultPage->expects($this->any())
            ->method('getConfig')
            ->willReturn($pageConfig);

        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(true);
        $this->upload->execute();
    }
}
