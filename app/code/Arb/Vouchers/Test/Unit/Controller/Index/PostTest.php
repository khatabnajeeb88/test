<?php
/**
 * PostTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Escaper;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\App\Filesystem\DirectoryList;
use Arb\Vouchers\Model\Vouchers;
use Arb\Vouchers\Helper\Email as HelperEmail;
use Arb\Vouchers\Helper\Data;
use Magento\Customer\Model\Session;
use Webkul\Marketplace\Helper\Data as HelperData;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Arb\Vouchers\Helper\CSVProcess;
use Arb\Vouchers\Controller\Index\Post;

/**
 * Class PostTest for testing View class
 * @covers \Arb\Vouchers\Controller\Index\Post
 */
class PostTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test setUp
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function setUp()
    {
        $this->context = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->_escaper = $this->createMock(Escaper::class);
        $this->_helperData = $this->getMockBuilder(HelperData::class)
            ->disableOriginalConstructor()
            ->setMethods([
                            'getIsSeparatePanel',
                            "isSeller",
                            "getCustomerId",
                            "updateProductStock",
                            "deleteUploadedFile",
                            "getVendorList"
                        ])
            ->getMock();
        $this->_helperEmail = $this->createMock(HelperEmail::class);
        $this->_directoryList = $this->createMock(DirectoryList::class);
        $this->_dateTimeFactory = $this->createMock(DateTimeFactory::class);
        $this->_stateInterface = $this->createMock(StateInterface::class);
        $this->_scopeConfigInterface = $this->createMock(ScopeConfigInterface::class);
        $this->_csv = $this->getMockBuilder(Csv::class)
            ->setMethods(['setDelimiter' , 'setEnclosure' , 'saveData'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_csv->expects($this->any())->method('setDelimiter')->willReturnSelf();
        $this->_csv->expects($this->any())->method('setEnclosure')->willReturnSelf();
        $this->_data = $this->createMock(Data::class);
        $this->_vouchers = $this->createMock(Vouchers::class);
        $this->_customerSession = $this->createMock(Session::class);
        $this->resultPageFactory = $this->initResultPage();
        $this->_uploaderFactory = $this->getMockBuilder(UploaderFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                    'create',
                    "setAllowedExtensions",
                    "setAllowRenameFiles",
                    "save"
                    ])
                    ->getMock();
        $this->csvProcessMock = $this->getMockBuilder(CSVProcess::class)
            ->disableOriginalConstructor()
            ->setMethods([
                    "processCSV"
                    ])
                    ->getMock();
        $this->resultRedirectFactory = $this->getMockBuilder(\Magento\Backend\Model\View\Result\RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(
                ['create',
                "setPath"]
            )
            ->getMock();
        $this->resultRedirect = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Redirect::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->resultRedirectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultRedirect);

        $this->resultRedirectFactory->expects($this->any())
            ->method('setPath')
            ->willReturn("voucher/index/upload");

        $this->context->expects($this->any())
            ->method('getResultRedirectFactory')
            ->willReturn($this->resultRedirectFactory);
        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
         $this->urlBuilderMock->expects($this->any())
            ->method('getUrl')
            ->with('marketplace/account/login/')
            ->willReturnArgument(1);
        $this->context->expects($this->any())
            ->method('getUrl')
            ->willReturn($this->urlBuilderMock);
        $this->_customerSession->expects($this->any())
            ->method('authenticate')
            ->willReturn(1);
        $this->messageManagerMock = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);
        $this->context->expects($this->any())->method('getMessageManager')->willReturn($this->messageManagerMock);

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['isSecure',"getFiles"]
        );
        $this->context->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $this->post = new Post(
            $this->context,
            $this->_directoryList,
            $this->_data,
            $this->_customerSession,
            $this->_helperData,
            $this->_uploaderFactory,
            $this->csvProcessMock
        );
    }
     /**
      * @return \Magento\Framework\View\Result\PageFactory|\PHPUnit_Framework_MockObject_MockObject
      */
    protected function initResultPage()
    {
        $this->resultPage = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Page::class)
            ->disableOriginalConstructor()
            ->getMock();
        $resultPageFactory = $this->getMockBuilder(\Magento\Framework\View\Result\PageFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $resultPageFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultPage);
        return $resultPageFactory;
    }

    /**
     * testExecute method
     */
    public function testExecute()
    {
        $this->_uploaderFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowedExtensions')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowRenameFiles')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('save')->willReturn(
            [
                "file"=>BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
                "path"=>""
            ]
        );
        $this->_helperData->expects($this->any())->method('getCustomerId')->willReturn(2);
        $this->_helperData->expects($this->any())->method('deleteUploadedFile')->willReturnSelf();
        $this->_helperData->expects($this->any())->method('updateProductStock')->willReturnSelf();
        $vendorData[0] = ["name"=>"teste","email"=>"test@tes.com"];
        $this->_helperData->expects($this->any())->method('getVendorList')->willReturn($vendorData);
        $this->messageManagerMock
            ->expects($this->any())
            ->method('addErrorMessage')
            ->willReturn(__('File upload failed. Please try again'));
        $vouchers["upload_vouchers"] =["size"=>10,"name"=>"teste","type"=>"application/csv"];
        $this->requestMock->expects($this->any())->method('isSecure')->willReturn(1);
        $this->requestMock->expects($this->any())->method('getFiles')->willReturn($vouchers);
        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(true);

        $this->post->execute();
    }

    /**
     * testExecuteWithFile method
     */
    public function testExecuteWithFile()
    {
        $this->_uploaderFactory->expects($this->any())->method('save')->willReturn(
            [
                "file"=>BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
                "path"=>""
            ]
        );
        $vouchers["upload_vouchers"] =["size"=>0,"name"=>"teste","type"=>"application/csv"];
        $this->requestMock->expects($this->any())->method('isSecure')->willReturn(1);
        $this->requestMock->expects($this->any())->method('getFiles')->willReturn($vouchers);
        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(false);
        $this->post->execute();
    }

    public function testFailedCase()
    {
        $this->_csv
        ->expects($this->any())
        ->method('saveData')
        ->willThrowException(new \Exception('Error in saving logs.'));
        $this->_uploaderFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowedExtensions')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowRenameFiles')->willReturnSelf();
        $this->_directoryList->expects($this->any())->method('getPath')->willReturn(null);
        $vendorData = ["name"=>"teste","email"=>"test@tes.com"];
        $vouchers["upload_vouchers"] =["size"=>0,"name"=>"teste","type"=>"application/csv"];
        $this->requestMock->expects($this->any())->method('isSecure')->willReturn(1);
        $this->requestMock->expects($this->any())->method('getFiles')->willReturn($vouchers);
        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(true);
        $this->post->execute();
    }

    public function testExceptionMessage()
    {
        $this->_uploaderFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowedExtensions')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowRenameFiles')->willReturnSelf();
        $this->_helperData->expects($this->any())->method('getCustomerId')->willReturn(2);
        $this->_helperData->expects($this->any())->method('deleteUploadedFile')->willReturnSelf();
        $this->_helperData->expects($this->any())->method('updateProductStock')->willReturnSelf();
        $vendorData[0] = ["name"=>"teste","email"=>"test@tes.com"];
        $this->_helperData->expects($this->any())->method('getVendorList')->willReturn($vendorData);
        $this->messageManagerMock
            ->expects($this->any())
            ->method('addSuccessMessage')
            ->willReturn(__('File upload successfully.'));
        $vouchers["upload_vouchers"] =["size"=>10,"name"=>"teste","type"=>"application/csv"];
        $this->requestMock->expects($this->any())->method('isSecure')->willReturn(1);
        $this->requestMock->expects($this->any())->method('getFiles')->willReturn($vouchers);
        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(true);
        $this->csvProcessMock->method("processCSV")->willReturn(["success"=>true,"message"=>"file processed"]);
        $this->_uploaderFactory->expects($this->any())->method('save')
            ->willThrowException(new \Exception('Error in saving logs.'));
        $this->post->execute();
    }
    public function testSuccess()
    {
        $this->_uploaderFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowedExtensions')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowRenameFiles')->willReturnSelf();
        $this->_helperData->expects($this->any())->method('getCustomerId')->willReturn(2);
        $this->_helperData->expects($this->any())->method('deleteUploadedFile')->willReturnSelf();
        $this->_helperData->expects($this->any())->method('updateProductStock')->willReturnSelf();
        $vendorData[0] = ["name"=>"teste","email"=>"test@tes.com"];
        $this->_helperData->expects($this->any())->method('getVendorList')->willReturn($vendorData);
        $this->messageManagerMock
            ->expects($this->any())
            ->method('addSuccessMessage')
            ->willReturn(__('File upload successfully.'));
        $vouchers["upload_vouchers"] =["size"=>10,"name"=>"teste","type"=>"application/csv"];
        $this->requestMock->expects($this->any())->method('isSecure')->willReturn(1);
        $this->requestMock->expects($this->any())->method('getFiles')->willReturn($vouchers);
        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(true);
        $this->csvProcessMock->method("processCSV")->willReturn(["success"=>true,"message"=>"file processed","log_message"=>"file processCSV"]);
        $this->_uploaderFactory->expects($this->any())->method('save')->willReturn(
            [
                "file"=>BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
                "path"=>""
            ]
        );
        $this->post->execute();
    }
    public function testNoVoucher()
    {
        $this->_uploaderFactory->expects($this->any())->method('create')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowedExtensions')->willReturnSelf();
        $this->_uploaderFactory->expects($this->any())->method('setAllowRenameFiles')->willReturnSelf();
        $this->_helperData->expects($this->any())->method('getCustomerId')->willReturn(2);
        $this->_helperData->expects($this->any())->method('deleteUploadedFile')->willReturnSelf();
        $this->_helperData->expects($this->any())->method('updateProductStock')->willReturnSelf();
        $vendorData[0] = ["name"=>"teste","email"=>"test@tes.com"];
        $this->_helperData->expects($this->any())->method('getVendorList')->willReturn($vendorData);
        $this->messageManagerMock
            ->expects($this->any())
            ->method('addErrorMessage')
            ->willReturn(__('File upload successfully.'));
        
        $this->requestMock->expects($this->any())->method('isSecure')->willReturn(1);
        $this->requestMock->expects($this->any())->method('getFiles')->willReturn("");
        $this->_helperData->expects($this->any())->method('isSeller')->willReturn(true);
        $this->csvProcessMock->method("processCSV")->willReturn(["success"=>true,"message"=>"file processed"]);
        $this->_uploaderFactory->expects($this->any())->method('save')->willReturn(
            [
                "file"=>BP."/app/code/Arb/Vouchers/Test/Unit/test.csv",
                "path"=>""
            ]
        );
        $this->post->execute();
    }
}
