<?php
/**
 * File for Vouchers Block/View
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Block;

use Arb\Vouchers\Block\Vouchers;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class VouchersTest for testing \Arb\Vouchers\Block\Vouchers
 * @covers \Arb\Vouchers\Block\Vouchers
 */

class VouchersTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Arb\Vouchers\Block\Vouchers
     */
    protected $block;

    protected function setUp()
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->block = $objectManager->getObject(Vouchers::class);
    }

     /**
      * testGetFormAction method
      */
    public function testGetFormAction()
    {
        $this->block->getFormAction();
    }
}
