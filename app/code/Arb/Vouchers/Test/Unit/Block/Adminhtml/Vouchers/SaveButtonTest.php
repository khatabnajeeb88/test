<?php
/**
 * SaveButtonTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Test\Unit\Block\Adminhtml\Vouchers;

/**
 * SaveButtonTest class for PHPUnit test of SaveButton class
 */
class SaveButtonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Arb\Vouchers\Block\Adminhtml\Vouchers\SaveButton
     */
    protected $block;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $urlBuilderMock;

    /**
     * Main setup method
     */
    protected function setUp()
    {
        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->getMock();
        $this->block = new \Arb\Vouchers\Block\Adminhtml\Vouchers\SaveButton(
            $this->urlBuilderMock
        );
    }

    /**
     * testGetButtonData method
     */
    public function testGetButtonData()
    {
        $expectedResult = [
            'label' => __('Upload'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];

        $this->assertEquals($expectedResult, $this->block->getButtonData());
    }
}
