<?php
namespace Arb\Vouchers\Test\Unit\Plugin;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * @covers \Arb\Vouchers\Plugin\OrderManagement
 */
class OrderManagementTest extends TestCase
{
    /**
     * Mock voucherCollection
     *
     * @var \Arb\Vouchers\Model\VoucherOrders|PHPUnit_Framework_MockObject_MockObject
     */
    private $voucherCollection;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Vouchers\Plugin\OrderManagement
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->voucherCollection = $this->getMockBuilder(\Arb\Vouchers\Model\VoucherOrders::class)
            ->disableOriginalConstructor()
            ->setMethods([
                     "blockVouchers",
                    "getVoucherAvailability"
                        ])
            ->getMock();
        $this->orderInterface = $this->getMockBuilder(OrderInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                            "getAllItems"
                        ])
            ->getMockForAbstractClass();
        $this->itemFactory = $this->getMockBuilder(\Magento\Sales\Model\Order\ItemFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                     "getSku",
                    "getQtyOrdered",
                    "getProductType"
                        ])
            ->getMock();
        $items = [$this->itemFactory];
        $this->orderInterface->method("getAllItems")->willReturn($items);
        $this->orderManagementInterface = $this->createMock(OrderManagementInterface::class);
        $this->testObject = $this->objectManager->getObject(
            \Arb\Vouchers\Plugin\OrderManagement::class,
            [
                'voucherCollection' => $this->voucherCollection,
            ]
        );
    }
   
    public function testBeforePlaceWithException()
    {
        $this->itemFactory->method("getSku")->willReturn("teste");
        $this->itemFactory->method("getQtyOrdered")->willReturn("5");
        $this->itemFactory->method("getProductType")->willReturn("virtual");
        $this->voucherCollection->method("getVoucherAvailability")->willReturn(["1001"]);
        $this->testObject->beforePlace($this->orderManagementInterface, $this->orderInterface);
    }
   
    public function testBeforePlaceLessVoucher()
    {
        $this->itemFactory->method("getSku")->willReturn("teste");
        $this->itemFactory->method("getQtyOrdered")->willReturn("5");
        $this->itemFactory->method("getProductType")->willReturn("virtual");
        $this->voucherCollection->method("getVoucherAvailability")->willReturn(["teste"=>"4"]);
        $this->testObject->beforePlace($this->orderManagementInterface, $this->orderInterface);
    }
    public function testBeforePlace()
    {
        $this->itemFactory->method("getSku")->willReturn("teste");
        $this->itemFactory->method("getQtyOrdered")->willReturn("5");
        $this->itemFactory->method("getProductType")->willReturn("virtual");
        $this->voucherCollection->method("getVoucherAvailability")->willReturn(["teste"=>"6"]);
        $this->testObject->beforePlace($this->orderManagementInterface, $this->orderInterface);
    }
}
