<?php
/**
 * Block file for voucher upload form
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Block;

/**
 * Marketplace Vouchers upload class
 *
 */
class Vouchers extends \Magento\Framework\View\Element\Template
{
    /**
     * Returns voucher upload URL to form section
     *
     * @param void
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('vouchers/index/post', ['_secure' => true]);
    }
}
