<?php

/*
 * This file consist of class BackButton which is used to reverse back in voucher listing.
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Block\Adminhtml\Vouchers;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Framework\UrlInterface;

/**
 * Class BackButton in voucher
 */
class BackButton implements ButtonProviderInterface
{
    private $urlBuilder;

    /**
     * Button function
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->urlBuilder->getUrl('*/')),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }
}
