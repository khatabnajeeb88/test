<?php
/**
 * This file consist of class SaveButton which is used to save Vouchers.
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Block\Adminhtml\Vouchers;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton used to save voucher
 */
class SaveButton implements ButtonProviderInterface
{
    /**
     * getButtonData
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Upload'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
