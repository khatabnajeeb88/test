<?php
/**
 * Controller file to process voucher csv
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */

namespace Arb\Vouchers\Controller\Index;

use Magento\Framework\App\Action\Context;
use Webkul\Marketplace\Helper\Data as HelperData;
use Magento\Framework\App\RequestInterface;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Arb\Vouchers\Helper\CSVProcess;
use Magento\Framework\App\ObjectManager;

/**
 * Controller class to process voucher csv
 */
class Post extends \Magento\Customer\Controller\AbstractAccount
{
    
    /**
     * Default voucher status for merchant upload
     */
    const DEFAULT_VOUCHER_STATUS = 0;

    /**
     * Voucher upload by merchant flag
     */
    const UPLOAD_BY_MERCHANT = 'Merchant';

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \Arb\Vouchers\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var HelperData
     */
    protected $mpHelper;

    /**
     * @var UploaderFactory
     */
    protected $fileUploaderFactory;

    /**
     * @var CSVProcess
     */
    protected $csvProcess;

    /**
     * MIME type to check file type
     *
     * @var array
     */
    protected $mimeType = [
        'text/csv',
        'text/plain',
        'application/csv',
        'text/comma-separated-values',
        'application/excel',
        'application/vnd.ms-excel',
        'application/vnd.msexcel',
        'text/anytext',
        'application/octet-stream',
        'application/txt',
    ];

    /**
     * Controller class Post constructor
     *
     * @param Context $context
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Arb\Vouchers\Model\Vouchers $evocherModel
     * @param \Arb\Vouchers\Helper\Email $emailHelper
     * @param \Arb\Vouchers\Helper\Data $helper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param HelperData $mpHelper
     * @param UploaderFactory $fileUploaderFactory
     * @param CSVProcess $csvProcess
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Arb\Vouchers\Helper\Data $helper,
        \Magento\Customer\Model\Session $customerSession,
        HelperData $mpHelper,
        UploaderFactory $fileUploaderFactory,
        CSVProcess $csvProcess
    ) {
        $this->directoryList = $directoryList;
        $this->helper  = $helper;
        $this->mpHelper = $mpHelper ?: ObjectManager::getInstance()->create(HelperData::class);
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->customerSession = $customerSession;
        $this->csvProcess = $csvProcess;
        parent::__construct(
            $context
        );
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/voucher_upload_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @codeCoverageIgnore
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl =  $this->_url->getUrl(
            'marketplace/account/login/',
            ['_secure' => $this->getRequest()->isSecure()]
        );

        if (!$this->customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }

    /**
     * Controller function to upload voucher CSV
     *
     * @param void
     * @return \Magento\Framework\View\Result\Page
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {   
        $mpHelper = $this->mpHelper;
        $isPartner = $mpHelper->isSeller();
        if ($isPartner) {
            $this->logger->info("Voucher file upload process start");
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $this->resultRedirectFactory->create();
            $file = $this->getRequest()->getFiles();
            $uploadVouchers = !empty($file['upload_vouchers']) ? $file['upload_vouchers'] : [];
            if (!empty($uploadVouchers['name']) && in_array($uploadVouchers['type'], $this->mimeType)) {
                try {
                    //Upload CSV file to media folder
                    $mediaPath =  $this->directoryList->getPath('media');
                    $uploader = $this->fileUploaderFactory->create(['fileId' => 'upload_vouchers']);
                    $uploader->setAllowedExtensions(['csv']);
                    $uploader->setAllowRenameFiles(true);
                    $path = $mediaPath.CSVProcess::VOUCHER_CSV_UPLOAD_DIRECTORY;
                    $result = $uploader->save($path);
                    $this->logger->info("Voucher file upload process end, filename - ". $uploadVouchers['name']);
                } catch (\Exception $e) {
                    $this->logger->info("An error occured while saving csv file, filename - ". $uploadVouchers['name'] . "Error : ".$e->getMessage());
                    $this->messageManager->addErrorMessage(__('An error occured while saving csv file'));
                    return $this->resultRedirectFactory->create()->setPath(
                        'vouchers/index/upload',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }

                //Upload response validation
                if (isset($result['file']) && isset($result['path'])) {
                   //Process CSV file function
                    $vendorId = $this->mpHelper->getCustomerId();
                    $this->logger->info("Voucher file process start, filename - ". $result['file']. ", Vendor ID : ". $vendorId);
                    $updateFile = $this->csvProcess->processCSV(
                        $result,
                        self::UPLOAD_BY_MERCHANT,
                        self::DEFAULT_VOUCHER_STATUS,
                        false,
                        $vendorId
                    );
                    $this->logger->info("Voucher file process end, filename - ". $result['file']. ", Vendor ID : ". $vendorId);
                    if ($updateFile['success']) {
                        $this->logger->info("Voucher file process success, filename - ". $result['file']. ", Vendor ID : ". $vendorId . " , Message: ".$updateFile['log_message']);
                        $this->messageManager->addSuccess($updateFile['message']);
                        return $this->resultRedirectFactory->create()->setPath(
                            'vouchers/index/upload',
                            ['_secure' => $this->getRequest()->isSecure()]
                        );
                    } else {
                        $this->logger->info("Voucher file process error, filename - ". $result['file']. ", Vendor ID : ". $vendorId. " , Message: ". $updateFile['log_message']);
                        $this->messageManager->addError($updateFile['message']);
                        return $this->resultRedirectFactory->create()->setPath(
                            'vouchers/index/upload',
                            ['_secure' => $this->getRequest()->isSecure()]
                        );
                    }
                }
            } else {
                $this->logger->info("Error in file upload. Please try again., No valid file available");
                $this->messageManager->addErrorMessage(__('Error in file upload. Please try again.'));
                return $this->resultRedirectFactory->create()->setPath(
                    'vouchers/index/upload',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            $this->logger->info("No valid seller available");
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}
