<?php
/**
 * View controller file for voucher upload form
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SaleslistColl;
use Webkul\Marketplace\Model\ResourceModel\Saleperpartner\CollectionFactory;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Helper\Email as HelperEmail;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\RequestInterface;

/**
 * Upload class for voucher upload file
 */
class Upload extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * @var HelperEmail
     */
    protected $helperEmail;

    /**
     * @var SaleslistColl
     */
    protected $saleslistColl;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * Upload controller constructor
     *
     * @param Context $context
     * @param FormKeyValidator $formKeyValidator
     * @param HelperData $helper
     * @param HelperEmail $helperEmail
     * @param SaleslistColl $saleslistColl
     * @param CollectionFactory $collectionFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        Context $context,
        FormKeyValidator $formKeyValidator,
        HelperData $helper,
        HelperEmail $helperEmail,
        SaleslistColl $saleslistColl,
        CollectionFactory $collectionFactory,
        CustomerRepositoryInterface $customerRepository,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_formKeyValidator = $formKeyValidator;
        $this->helper = $helper;
        $this->helperEmail = $helperEmail;
        $this->saleslistColl = $saleslistColl;
        $this->collectionFactory = $collectionFactory;
        $this->customerRepository = $customerRepository;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $customerSession;
        parent::__construct(
            $context
        );
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @codeCoverageIgnore
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl =  $this->_url->getUrl(
            'marketplace/account/login/',
            ['_secure' => $this->getRequest()->isSecure()]
        );
        
        if (!$this->customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }

    /**
     * Sets layout for upload form
     *
     * @param void
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $helper = $this->helper;
        $isPartner = $helper->isSeller();
        $resultPage = $this->resultPageFactory->create();
        if ($isPartner == 1) {
            if ($helper->getIsSeparatePanel()) {
                $resultPage->addHandle('vouchers_layout2_upload');
                $resultPage->getConfig()->getTitle()->set(__('Marketplace Mass Upload Vouchers'));
                return $resultPage;
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}
