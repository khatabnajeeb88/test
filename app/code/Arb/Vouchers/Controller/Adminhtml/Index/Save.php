<?php
/**
 * File to save voucher data form in Database
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;
use Arb\Vouchers\Helper\CSVProcess;

/**
 * Consist of Voucher save function
 */
class Save extends \Magento\Backend\App\Action
{

    /**
     * Default voucher status for admin upload
     */
    const DEFAULT_VOUCHER_STATUS = 1;

    /**
     * voucher upload directory
     */
    const VOUCHER_UPLOAD_DIR = 'vouchers';

    /**
     * Voucher upload by merchant flag
     */
    const UPLOAD_BY = 'Admin';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
   
    /**
     * @var escaper
     */
    protected $_escaper;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var DirectoryList
     */
    protected $directoryList;

    /**
     * @var CSVProcess
     */
    protected $csvProcess;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param Escaper $escaper
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
     * @param \Magento\Framework\App\Filesystem\DirectoryList $DirectoryList
     * @param CSVProcess $csvProcess
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $DirectoryList,
        CSVProcess $csvProcess
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->inlineTranslation = $inlineTranslation;
        $this->directoryList = $DirectoryList;
        $this->csvProcess = $csvProcess;
        parent::__construct($context);
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/voucher_upload_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $this->logger->info("Voucher file upload process start");
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            // check for CSV file data
            if (!isset($data['import'][0]['name'])
                || !isset($data['import'][0]['tmp_name'])
                || !isset($data['import'][0]['size'])
                || empty($data['vendor_id'])) {
                $this->messageManager->addSuccess(__('File upload failed. Please try again.'));
                return $resultRedirect->setPath('*/*/');
            }

            //get csv file details
            $filename = $data['import'][0]['name'];
            $filepath =  $this->directoryList->getPath('media') .'/'. self::VOUCHER_UPLOAD_DIR .'/';
            $fileData['file'] = $filename;
            $fileData['path'] = $filepath;
            $vendorId = $data['vendor_id'];

            $this->logger->info("Voucher file process start, filename - ". $filename. ", Vendor ID : ". $vendorId);

            // procees voucher file
            $updateFile = $this->csvProcess->processCSV(
                $fileData,
                self::UPLOAD_BY,
                self::DEFAULT_VOUCHER_STATUS,
                true,
                $vendorId
            );

            $this->logger->info("Voucher file process end, filename - ". $filename. ", Vendor ID : ". $vendorId);
            if ($updateFile['success']) {
                $this->logger->info("Voucher file process success, filename - ". $filename. ", Vendor ID : ". $vendorId. " , Message: ".$updateFile['log_message']);
                $this->messageManager->addSuccess($updateFile['message']);
                return $resultRedirect->setPath('*/*/');
            } else {
                $this->logger->info("Voucher file process error, filename - ". $filename. ", Vendor ID : ". $vendorId. " , Message: ". $updateFile['log_message']);
                $this->messageManager->addError($updateFile['message']);
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $this->logger->info("Error in file upload. Please try again., No valid file available");
            $this->messageManager->addError(__('File upload failed. Please try again.'));
            return $resultRedirect->setPath('*/*/');
        }
    }
}
