<?php
/**
 * Voucher GRID page to display UI GRID in Admin panel
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index to display GRID
 */
class View extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    /**
     * Check the permission to run it
     * Code coverage is ignored as function _iSAllowed() does not provide coverage in core.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
       // @codeCoverageIgnoreStart
        return $this->_authorization->isAllowed('Arb_Vouchers::vouchers');
        // @codeCoverageIgnoreEnd
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $resultPage->setActiveMenu('Arb_Vouchers::vouchers');
        $resultPage->addBreadcrumb(__('Vouchers'), __('Vouchers'));
        $resultPage->addBreadcrumb(__('Manage Vouchers'), __('Manage Vouchers'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Vouchers View'));

        return $resultPage;
    }
}
