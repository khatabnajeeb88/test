<?php
/**
 * File to update voucher status
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory;
use Arb\Vouchers\Model\ResourceModel\Log\CollectionFactory as LogCollectionFactory;
use Arb\Vouchers\Helper\Email;
use Arb\Vouchers\Helper\Data;
use Arb\Vouchers\Helper\CSVProcess;

/**
 * Class MassStatus to update Voucher status in bulk
 */
class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var Email
     */
    protected $emailHelper;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var LogCollectionFactory
     */
    protected $logCollectionFactory;

    /**
     * @var CSVProcess
     */
    protected $csvProcess;

    /**
     * __construct
     *
     * @param  Context $context
     * @param  Filter $filter
     * @param  CollectionFactory $collectionFactory
     * @param  Email $emailHelper
     * @param  LogCollectionFactory $logCollectionFactory
     * @param  Data $helper
     * @param  CSVProcess $csvProcess
     * @return void
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        Email $emailHelper,
        LogCollectionFactory $logCollectionFactory,
        Data $helper,
        CSVProcess $csvProcess
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->emailHelper = $emailHelper;
        $this->logCollectionFactory = $logCollectionFactory;
        $this->helper = $helper;
        $this->csvProcess = $csvProcess;
        parent::__construct($context);
    }

    /**
     * Check admin permissions for this controller
     * Code coverage is ignored as function _iSAllowed() does not provide coverage in core.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
         // @codeCoverageIgnoreStart
        return $this->_authorization->isAllowed('Arb_Vouchers::vouchers');
        // @codeCoverageIgnoreEnd
    }
    
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     * @codeCoverageIgnore
     */
    public function execute()
    {
        $status = $this->getRequest()->getParam('status');
        
        $collection = $this->filter->getCollection($this->logCollectionFactory->create());

        $data = [];
        foreach ($collection as $item) {
            $item->setStatus($status);
            $item->save();
            
            if ($item->getVendorEmail()) {
                $vouchersData = $this->helper->getVouchersByFileId($item->getId());
                $data[$item->getId()]['email'] = $item->getVendorEmail();
                $data[$item->getId()]['reference_number'] = $item->getReferenceNumber();
                $data[$item->getId()]['voucherData']  = $vouchersData;
            }
        }

        $sku = [];
        foreach ($data as $key => $row) {
            $merchantEmail = $row['email'];
            $referenceNumber = $row['reference_number'];
            $ids = '';
            $serial_numbers = '';
            foreach ($row['voucherData'] as $voucher) {
                $ids .=  $voucher['vouchers_id'] .',';
                $serial_numbers .= $voucher['serial_number'] .', ';
                $sku[] = $voucher['sku'];
            }

            $voucherIds = explode(',', rtrim($ids, ','));
            // Update vouchers status
            $this->helper->updateVouchersStatusByVoucherIds($voucherIds, $status);
            $this->sendEmail($merchantEmail, rtrim($serial_numbers, ','), $status, $referenceNumber);
        }

        // Update product stock as per the available approved vouchers
        $this->helper->updateProductStock(array_unique($sku));

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been changed.', $collection->getSize()));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
    
    /**
     * send Email for approve and disapprove the vouchers file
     *
     * @param  string $merchantEmail
     * @param  string $serial_numbers
     * @param  int $status
     * @return void
     */
    public function sendEmail($merchantEmail, $serial_numbers, $status, $fileName)
    {
        $emailTempVariables = [];

        $receiverInfo = [
            'name' => 'ARB partner',
            'email' => $merchantEmail,
        ];

        $senderAdminEmail = $this->emailHelper->getSenderEmail();
        $senderInfo = [
            'name' => 'Admin Support',
            'email' => $senderAdminEmail,
        ];

        if ($status == 0) {
            $filePath = $this->csvProcess->createFailedVouchersCSV($serial_numbers, $fileName);
            $this->emailHelper->sendVoucherDisapproveMail(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo,
                $filePath['filePath'],
                $fileName
            );
        }

        if ($status == 1) {
            $this->emailHelper->sendVoucherApproveMail(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo
            );
        }
    }
}
