<?php
/**
 * This file consist of class Edit which execute edit Voucher action.
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */
namespace Arb\Vouchers\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

/**
 * Consist of Voucher backend edit actions
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Check admin permissions for this controller
     * Code coverage is ignored as function _iSAllowed() does not provide coverage in core.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
         // @codeCoverageIgnoreStart
        return $this->_authorization->isAllowed('Arb_Vouchers::vouchers');
        // @codeCoverageIgnoreEnd
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Arb_Vouchers::vouchers')
            ->addBreadcrumb(__('Vouchers'), __('Vouchers'))
            ->addBreadcrumb(__('Manage Vouchers'), __('Manage Vouchers'));
        return $resultPage;
    }

    /**
     * Edit Voucher upload page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('Vouchers_id');
        $model = $this->_objectManager->create(\Arb\Vouchers\Model\Vouchers::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This record no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        // 4. Register model to use later in blocks
        $this->_coreRegistry->register('Vouchers', $model);
        
        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(__('Upload Vouchers'), __('Upload Vouchers'));
        $resultPage->getConfig()->getTitle()->prepend(__('Vouchers'));
       
        return $resultPage;
    }
}
