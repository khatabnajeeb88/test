<?php
/**
 * File to save Voucher data form in Database
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */

namespace Arb\Vouchers\Helper;

/**
 * Arb Vouchers Helper CSV process.
 */
class CSVProcess extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Maximun file upload size in bytes
     */
    const MAXSIZE = 1000000;
    
    /**
     * Default voucher flag used flag
     */
    const VOUCHER_USED_FLAG = 0;

    /**
     *  Voucher CSV upload directory path
     */
    const VOUCHER_CSV_UPLOAD_DIRECTORY = '/vouchers/';

    /**
     * Voucher error CSV upload directory
     */
    const VOUCHER_CSV_UPLOAD_ERROR_DRIRECTORY = '/voucherError/';

    /**
     * Admin email name
     */
    const ADMIN_SENDER_NAME = 'ARB Marketplace Support';

    /**
     * Sales email name for admin
     */
    const SALES_EMAIL_NAME = 'ARB Team';

    /**
     * CSV header of error file
     */
    const CSV_HEADER_FAILED_VOUCHER = 'Serial Number';

    /**
     * CSV valid header for validation
     */
    const CSV_VALID_HEADER =
    [
        'Serial_Number', 'Voucher_Code', 'Reference_Number', 'Voucher_Expiry_Date', 'Vendor', 'SKU_Code'
    ];

     /**
     * Default initialize value
     */
    const INITIALIZE_WITH_ZERO = 0;

    /**
     * @var \Arb\Vouchers\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvparser;

    /**
     * @var \Arb\Vouchers\Model\Vouchers
     */
    protected $evocherModel;

    /**
     * @var \Arb\Vouchers\Helper\Email
     */
    protected $emailHelper;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var Magento\Framework\Filesystem\Driver\File
     */
    protected $driverInterface;

    /**
     *  CSV process Helper class constructor
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Arb\Vouchers\Helper\Data $helper
     * @param \Magento\Framework\File\Csv $csvparser
     * @param \Arb\Vouchers\Model\Vouchers $evocherModel
     * @param \Arb\Vouchers\Helper\Email $emailHelper
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Filesystem\Driver\File $driverInterface
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Arb\Vouchers\Helper\Data $helper,
        \Magento\Framework\File\Csv $csvparser,
        \Arb\Vouchers\Model\Vouchers $evocherModel,
        \Arb\Vouchers\Helper\Email $emailHelper,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Driver\File $driverInterface
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->csvparser = $csvparser;
        $this->evocherModel  = $evocherModel;
        $this->emailHelper  = $emailHelper;
        $this->directoryList = $directoryList;
        $this->driverInterface = $driverInterface;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/voucher_upload_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Returns CSV process result
     *
     * @param array $fileData
     * @param string $uploadedBy
     * @param int $defaultStatus
     * @param int $fromAdmin
     * @param int $vendorId
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function processCSV($fileData, $uploadedBy, $defaultStatus, $fromAdmin, $vendorId)
    {
        $result = [
            'success' => false,
            'message' => __('Error in processing of CSV file')
        ];
        $fileName = isset($fileData['file']) ? $fileData['file'] : null;
        $filePath = isset($fileData['path']) ? $fileData['path'] : null;
        
        if (($fileName != null) && ($filePath != null)) {
            $csvData = $this->csvparser->getData($filePath.$fileName);
        }
        //Error when any error in vouchers upload.
        $standardError = "Thank you very much for trying to upload your Voucher file.<br>Unfortunately, The upload failed for one of the follow reasons:</p><ul><li>Wrong file format - if the uploaded file is in the wrong format, then the upload will fail.</li><li>Network issue - If there is an issue with the network when uploading, the upload will fail.</li><li>File is empty - If the file has no inputs, then the upload will fail.</li></ul>";

        if (is_array($csvData) && !empty($csvData)) {
            try {
                $allRows = $this->getCsvData($csvData);
                $totalRecords = count($allRows);
                if ($totalRecords > 0) {
                    $countFailRecords = self::INITIALIZE_WITH_ZERO;
                    $countSuccessRecords = self::INITIALIZE_WITH_ZERO;
                    $failedRecordData = '';
                    $status = $defaultStatus;
                    $approvedSku = [];

                    $model = $this->evocherModel;
                    $datetime   = $this->helper->getCurrentGmtDate();

                    //renaming file with time stamp for reference
                    $referenceNumber    = $datetime. '_' .$fileName;

                    $vendorData = $this->getVendorData($vendorId);

                    // add log of success, failed in database
                    $fileId = $this->helper->addLogs(
                        $referenceNumber,
                        $fileName,
                        $totalRecords,
                        $countFailRecords,
                        $countSuccessRecords,
                        $failedRecordData,
                        $vendorData['email'],
                        $status,
                        $uploadedBy
                    );

                    $fileData = [];
                    $fileData['filename'] = $fileName;
                    $fileData['vendorId'] = $vendorId;

                    foreach ($allRows as $rows) {
                        $rows['fileId'] = $fileId;
                        $rows['vendorName'] = $vendorData['shop_title'];
                        $voucherRow = $this->setVoucherData($rows, $defaultStatus);

                        //Validate voucher data
                        $isValidate = $this->helper->validatVouchers($voucherRow, $vendorData,  $fileData);
                        if ($isValidate) {
                            $model->setData($voucherRow);
                            $model->save();
                            $approvedSku[] = $voucherRow['sku'];
                            $countSuccessRecords++;
                        } else {
                            $countFailRecords++;
                            $failedRecordData .= $voucherRow['serial_number'] . ',';
                        }
                    }

                    // add log of success, failed in database
                    $this->helper->addLogs(
                        $referenceNumber,
                        $fileName,
                        $totalRecords,
                        $countFailRecords,
                        $countSuccessRecords,
                        $failedRecordData,
                        $vendorData['email'],
                        $status,
                        $uploadedBy,
                        $fileId
                    );
                    $uploadStatus = [
                        'totalRecords' => $totalRecords,
                        'failedRecords' => $countFailRecords,
                        'successRecords' => $countSuccessRecords
                    ];

                    // send email for success vouchers
                    if ($countSuccessRecords > 0 && $fromAdmin == false) {
                        $this->sendEmail($uploadStatus, $vendorData);
                    }

                    // send email for failed vouchers
                    if ($countFailRecords > 0) {
                        if ($fromAdmin) {
                            $receiverData = $this->helper->getCurrentAdminUser();
                            $vendorData['name'] =  $receiverData['name'];
                            $vendorData['email'] = $receiverData['email'];
                        }
                        
                        $this->sendFailedVoucherEmail($failedRecordData, $vendorData, $referenceNumber, $fromAdmin);
                    }

                    // Update product stock as per the available approved vouchers
                    // this is to check for update the stock after approve the voucher from admin or uploading vouchers from admin only
                    if($fromAdmin){                        
                        $this->helper->updateProductStock(array_unique($approvedSku));
                    }
                
                    //delete uploaded vocuher file
                    $this->helper->deleteUploadedFile($filePath . $fileName);

                    $result['success'] = true;
                    if($countFailRecords){
                        //Adding total failed counts with updated error.
                        $summary = "<p>File uploaded successfully, %1 Total, %2 Successful, %3 Failed<br>".$standardError;
                        $result['message'] = __($summary,
                        $totalRecords,
                        $countSuccessRecords,
                        $countFailRecords
                    );
                        $result['log_message'] = "File uploaded successfully, ".$totalRecords." Total, ".$countSuccessRecords." Successful, ".$countFailRecords." Failed";
                    }else{
                        $result['message'] = __(
                        'File uploaded successfully, %1 Total, %2 Successful, %3 Failed',
                        $totalRecords,
                        $countSuccessRecords,
                        $countFailRecords
                    );
                        $result['log_message'] = "File uploaded successfully, ".$totalRecords." Total, ".$countSuccessRecords." Successful, ".$countFailRecords." Failed";
                    }
                   
                } else {
                    $result['success'] = false;
                    $result['message'] = __($standardError);
                    $result['log_message'] = $standardError;
                    $this->logger->info("Empty/invalid Voucher file, filename - ". $fileName. ", Vendor ID : ". $vendorId);
                }
            } catch (\Exception $e) {
                $result['success'] = false;
                $result['message'] = __($e->getMessage());
                $result['log_message'] = $e->getMessage();

            }
        } else {
            $result['success'] = false;
            $result['message'] = __($standardError);
            $result['log_message'] = $standardError;
            $this->logger->info("Empty/invalid Voucher file, filename - ". $fileName. ", Vendor ID : ". $vendorId);
        }
        return $result;
    }

    /**
     * Process CSV file to array
     *
     * @param  array $csvData
     * @return array
     */
    public function getCsvData($csvData)
    {
        $allRows = [];
        $header = [];
        $validHeader = self::CSV_VALID_HEADER;
        foreach ($csvData as $row => $rowdata) {
            if ($row == 0) {
                foreach ($rowdata as $key => $value) {
                    $value = preg_replace('/[^\x0A\x20-\x7E]/', '', $value);
                    if (!in_array(trim($value), $validHeader)) {
                        return $allRows;
                    }
                    $header[] = $value;
                }
            } else {
                $allRows[] = array_combine($header, $rowdata);
            }
        }

        return $allRows;
    }

    /**
     * Returns vendor data by vendor id
     *
     * @param  int $vendorId
     * @return array
     */
    public function getVendorData($vendorId)
    {
        $vendorData = $this->helper->getVendorList($vendorId);
        if (is_array($vendorData) && !empty($vendorData)) {
            return $vendorData[0];
        } else {
            $vendorData = [
                'shop_title' => '',
                'email' => ''
            ];
            return $vendorData;
        }
    }

    /**
     * Converts voucher data into array
     *
     * @param  array $rows
     * @param  int $defaultStatus
     * @return array
     */
    public function setVoucherData($rows, $defaultStatus)
    {
        $voucherData                    = [];
        $date                           = $rows['Voucher_Expiry_Date'];
        $isValidDate                    = $this->helper->checkDateFormat('d/m/Y', $date);
        $voucherCode                    = $rows['Voucher_Code'];

        $voucherData['serial_number']   = $rows['Serial_Number'];
        $voucherData['voucher_code']    = ($voucherCode) ? $this->helper->encrptVoucher($rows['Voucher_Code']) : '';
        $voucherData['voucher_expiry']  = ($isValidDate) ? $this->helper->convertDateFormat('d/m/Y', $date) : '';
        $voucherData['vendor_name']     = $rows['Vendor'];
        $voucherData['sku']             = $rows['SKU_Code'];
        $voucherData['file_id']         = $rows['fileId'];
        $voucherData['is_used']         = self::VOUCHER_USED_FLAG;
        $voucherData['status']          = $defaultStatus;
        
        return $voucherData;
    }

    /**
     * Send email to admin about voucher status update
     *
     * @param array $uploadStatus
     * @param array $vendorData
     * @return void
     */
    public function sendEmail($uploadStatus, $vendorData)
    {
        $uploadResult = __(
            ' %1 Total, %2 Successful, %3 Failed',
            $uploadStatus['totalRecords'],
            $uploadStatus['successRecords'],
            $uploadStatus['failedRecords']
        );

        $emailTempVariables = [
            'upload_result' => $uploadResult,
            'vendor_name' => $vendorData['name'],
        ];

        $adminEmail = $this->emailHelper->getSenderEmail();

        $senderInfo = [
            'name' => self::ADMIN_SENDER_NAME,
            'email' => $adminEmail,
        ];

        $salesteamEmail = $this->emailHelper->getSalesTeamEmailId();

        $receiverInfo = [
            'name' => self::SALES_EMAIL_NAME,
            'email' => $salesteamEmail,
        ];

        $this->emailHelper->sendVoucherMailToAdmin(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo
        );
    }

    /**
     * Send failed voucher details to merchant
     *
     * @param array $failedVouchers
     * @param array $vendorData
     * @param string $fileName
     * @return void
     */
    public function sendFailedVoucherEmail($failedVouchers, $vendorData, $fileName, $isAdmin)
    {
        $adminEmail = $this->emailHelper->getSenderEmail();
        $filePath = $this->createFailedVouchersCSV($failedVouchers, $fileName);

        $emailTempVariables = [
            'reference_number' => $fileName
        ];

        $receiverInfo = [
            'name' => $vendorData['name'],
            'email' => $vendorData['email'],
        ];

        $senderInfo = [
            'name' => self::ADMIN_SENDER_NAME,
            'email' => $adminEmail,
        ];

        if ($isAdmin) {
            $this->emailHelper->sendFailedVoucherMailToAdmin(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo,
                $filePath['filePath'],
                $fileName
            );
        } else {
            $this->emailHelper->sendFailedVoucherMailToMerchant(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo,
                $filePath['filePath'],
                $fileName
            );
        }
    }
    
    /**
     * Generates CSV file with list of failed serial number
     *
     * @param array $failedVouchers
     * @param string $fileName
     * @return void
     */
    public function createFailedVouchersCSV($failedVouchers, $fileName)
    {
        $result = [
            'success' => false,
            'message' => __('Error in creation of Failed vouchers'),
            'filePath' => null
        ];
        $mediaPath =  $this->directoryList->getPath('media');
        $fileDirectoryPath = $mediaPath .self::VOUCHER_CSV_UPLOAD_ERROR_DRIRECTORY;
        if (!$this->driverInterface->isDirectory($fileDirectoryPath)) {
            $this->driverInterface->createDirectory($fileDirectoryPath, 0755);
        }

        $fileName = 'Error_' . $fileName;
        $filePath =  $fileDirectoryPath . '/' . $fileName;
        $data = explode(',', $failedVouchers);
        $csvData = [];
        $csvData[] = [self::CSV_HEADER_FAILED_VOUCHER];
        foreach ($data as $serialNumber) {
            $csvData[] = [$serialNumber];
        }
        try {
            $this->csvparser
                ->setDelimiter(',')
                ->setEnclosure('"')
                ->saveData(
                    $filePath,
                    $csvData
                );
            $result['success'] = true;
            $result['message'] = null;
            $result['filePath'] = $filePath;
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = __($e->getMessage());
            $result['filePath'] = null;
        }
        return $result;
    }
}
