<?php
/**
 * File to save Voucher data form in Database
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */

namespace Arb\Vouchers\Helper;

use Magento\Customer\Model\Session;
use Magento\Framework\Exception\MailException;

/**
 * Arb Vouchers Helper Email.
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Default expiry days validity
     */
    const XML_PATH_EXPIRY_DATE_VALIDITY = 'vouchers/validation/voucher_expiry_date_validation_limit';

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory
     */
    protected $sellerlistCollectionFactory;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Arb\Vouchers\Model\Vouchers
     */
    protected $evoucherModel;

    /**
     * @var \Arb\Vouchers\Model\Log
     */
    protected $logModel;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var \Arb\Vouchers\Model\LogFactory
     */
    protected $logModelFactory;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $productModel;

    /**
     * @var MpProductCollection
     */
    protected $mpProductCollection;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $file;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @param Magento\Framework\App\Helper\Context $context
     * @param Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param Magento\Framework\Message\ManagerInterface $messageManager
     * @param Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Session $customerSession
     * @param Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $sellerlistCollectionFactory
     * @param Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $collectionFactory
     * @param Arb\Vouchers\Model\Log $logModel
     * @param Arb\Vouchers\Model\Vouchers $evoucherModel
     * @param Arb\Vouchers\Model\LogFactory $logModelFactory
     * @param Magento\Backend\Model\Auth\Session $authSession
     * @param Magento\Catalog\Model\Product $productModel
     * @param Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollection
     * @param Magento\Framework\Filesystem\Driver\File $file
     * @param Magento\Framework\Filesystem $filesystem
     * @param Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     *
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Session $customerSession,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $sellerlistCollectionFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $collectionFactory,
        \Arb\Vouchers\Model\Log $logModel,
        \Arb\Vouchers\Model\Vouchers $evoucherModel,
        \Arb\Vouchers\Model\LogFactory $logModelFactory,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Catalog\Model\Product $productModel,
        \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollection,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        array $isSellerIdAndSkuExists = [],
        array $isSellerIdAndSkuNonExists = []
    ) {
        parent::__construct($context);
        $this->_inlineTranslation = $inlineTranslation;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_messageManager = $messageManager;
        $this->sellerlistCollectionFactory = $sellerlistCollectionFactory;
        $this->encryptor = $encryptor;
        $this->dateTime  = $dateTime;
        $this->collectionFactory    = $collectionFactory;
        $this->logModel    = $logModel;
        $this->evoucherModel    = $evoucherModel;
        $this->logModelFactory = $logModelFactory;
        $this->authSession = $authSession;
        $this->productModel = $productModel;
        $this->mpProductCollection = $mpProductCollection;
        $this->file = $file;
        $this->filesystem = $filesystem;
        $this->stockRegistry = $stockRegistry;
        $this->isSellerIdAndSkuExists = $isSellerIdAndSkuExists;
        $this->isSellerIdAndSkuNonExists = $isSellerIdAndSkuNonExists;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/voucher_upload_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Return store configuration value.
     *
     * @param string $path
     * @param int    $storeId
     *
     * @return mixed
     */
    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Return store.
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    /**
     * getVendorList
     *
     * @param  int $sellerId
     * @return array
     */
    public function getVendorList($sellerId = 0)
    {
        $sellerCollection = $this->sellerlistCollectionFactory->create();
        $sellerCollection->addFieldToFilter(
            'is_seller',
            ['eq' => 1]
        )->addFieldToFilter(
            'store_id',
            0
        );

        if ($sellerId) {
            $sellerCollection->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            );
        }

        $sellerCollection->addFieldToSelect(
            [
                'seller_id',
                'shop_title'
            ]
        );

        $customertable = $sellerCollection->getTable('customer_grid_flat');
        $sellerCollection->getSelect()->join(
            $customertable. ' as cgf',
            'main_table.seller_id = cgf.entity_id'
        );

        return $sellerCollection->getData();
    }

    /**
     * validatVouchers
     *
     * @param  string $voucher
     * @param  array $vendorData
     * @return boolean
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function validatVouchers($voucher, $vendorData, $fileData = null)
    {
        if (empty($voucher['voucher_expiry'])
            || empty($voucher['voucher_code'])
            || empty($voucher['serial_number'])
            || empty($voucher['sku'])
            || empty($voucher['vendor_name'])) {
            $this->logger->info("Empty voucher_expiry/voucher_code/serial_number/sku/vendor_name in Voucher file, filename - ". $fileData['filename']. ", Vendor ID : ". $fileData['vendorId'].", Serial No : ".$voucher['serial_number']);
            return false;
        }

        //voucher expiry date check
        $expiryDate = $voucher['voucher_expiry'];
        $expiryLimit = $this->getConfigValue(self::XML_PATH_EXPIRY_DATE_VALIDITY, $this->getStore()->getStoreId());
        $validity = '+'.$expiryLimit.' days';

        $timeStampOfExpiryDate = $this->dateTime->gmtTimestamp($expiryDate);
        $timeStampOfValidDate = $this->dateTime->gmtTimestamp($validity);
        $timeDifference = $timeStampOfExpiryDate - $timeStampOfValidDate;
        if ($timeDifference < 0) {
            $this->logger->info("Invalid date, filename - ". $fileData['filename']. ", Vendor ID : ". $fileData['vendorId'].", Serial No : ".$voucher['serial_number']);
            return false;
        }

        //vendor name check
        if ($vendorData['shop_title'] != trim($voucher['vendor_name'])) {
            $this->logger->info("Invalid Vendor Name, filename - ". $fileData['filename']. ", Vendor ID : ". $fileData['vendorId'].", Serial No : ".$voucher['serial_number']);
            return false;
        }

        //voucher code and serial number duplicate check
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter(
            ['serial_number', 'voucher_code'],
            [
                ['eq' => $voucher['serial_number']],
                ['eq' => $voucher['voucher_code']]
            ]
        );
        $isVoucherOrSerialNoExists = $collection->getSize();

        if ($isVoucherOrSerialNoExists) {
            $this->logger->info("Serial number or Voucher code already exists, filename - ". $fileData['filename']. ", Vendor ID : ". $fileData['vendorId'].", Serial No : ".$voucher['serial_number']);
            return false;
        }


        //SKU and vendor exists check
        if (array_key_exists($voucher['sku'], $this->isSellerIdAndSkuNonExists)) {
            $this->logger->info("Product not attached to Seller, filename - ". $fileData['filename']. ", Vendor ID : ". $fileData['vendorId'].", Serial No : ".$voucher['serial_number']);
            return false;
        }

        if (!array_key_exists($voucher['sku'], $this->isSellerIdAndSkuNonExists) || !array_key_exists($voucher['sku'], $this->isSellerIdAndSkuExists)) {
            //check for product is assigned to merchant
            $isSellerProductsExists = $this->checkSellerProductBySku($vendorData['seller_id'], $voucher['sku']);
            if (!$isSellerProductsExists) {
                $this->isSellerIdAndSkuNonExists[$voucher['sku']] = $vendorData['seller_id'];
                $this->logger->info("Product not attached to Seller, filename - ". $fileData['filename']. ", Vendor ID : ". $fileData['vendorId'].", Serial No : ".$voucher['serial_number']);
                return false;
            } else {
                $this->isSellerIdAndSkuExists[$voucher['sku']] = $vendorData['seller_id'];
            }

        }
        return true;
    }

    /**
     * checkSellerProductBySku
     *
     * @param  int $vendorId
     * @param  string $sku
     * @return int
     */
    public function checkSellerProductBySku($vendorId, $sku)
    {
        $sellerProductCollection = $this->mpProductCollection->create()
            ->addFieldToFilter(
                'seller_id',
                $vendorId
            )->addFieldToSelect(
                ['mageproduct_id']
            );

        $productTable = $sellerProductCollection->getTable('catalog_product_entity');
        $sellerProductCollection->getSelect()->join(
            $productTable.' as cpe',
            'main_table.mageproduct_id = cpe.entity_id'
        )->where(
            "cpe.sku = '".$sku."'"
        );

        return $sellerProductCollection->getSize();
    }

    /**
     * addLogs
     *
     * @param  string $referenceNumber
     * @param  string $filename
     * @param  int $totalRecords
     * @param  string $countFailRecords
     * @param  int $countSuccessRecords
     * @param  array $failedRecordData
     * @param  string $vendorEmail
     * @param  int $status
     * @param  string $addedBy
     * @param  int $id
     * @return int
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function addLogs(
        $referenceNumber,
        $filename,
        $totalRecords,
        $countFailRecords,
        $countSuccessRecords,
        $failedRecordData,
        $vendorEmail,
        $status,
        $addedBy,
        $id = 0
    ) {
        try {
            $logModel = $this->logModel;
            $logModel->load($id);
            $logModel->setReferenceNumber($referenceNumber);
            $logModel->setFilename($filename);
            $logModel->setTotalRecords($totalRecords);
            $logModel->setFailedRecords($countFailRecords);
            $logModel->setSuccessRecords($countSuccessRecords);
            $logModel->setFailedVoucherData(rtrim($failedRecordData, ','));
            $logModel->setVendorEmail($vendorEmail);
            $logModel->setAddedBy($addedBy);
            $logModel->setStatus($status);
            $logModel->save();
            return $logModel->getId();
        } catch (\Exception $e) {
            $this->logger->info("Error in saving logs. filename - ". $filename);
            $this->_messageManager->addException($e, __('Error in saving logs.'));
        }
    }

    /**
     * encrptVoucher
     *
     * @param  string $voucherCode
     * @return string
     */
    public function encrptVoucher($voucherCode)
    {
        return $this->encryptor->encrypt($voucherCode);
    }

    /**
     * getCurrentAdminUser
     *
     * @return array
     */
    public function getCurrentAdminUser()
    {
        $user = $this->authSession->getUser();
        $userDetails = [
            'name' => $user->getUsername(),
            'email' => $user->getEmail()
        ];

        return $userDetails;
    }

    /**
     * getCurrentGmtDate
     *
     * @return string
     */
    public function getCurrentGmtDate()
    {
        return $this->dateTime->gmtDate('dmyyHis');
    }

    /**
     * deleteUploadedFile
     *
     * @param  string $fileurl
     * @return void
     */
    public function deleteUploadedFile($fileurl)
    {
        if ($this->file->isExists($fileurl)) {
            $this->file->deleteFile($fileurl);
        }
    }

    /**
     * updateProductStock
     *
     * @param  array $data
     * @return void
     */
    public function updateProductStock($data)
    {
        if (!empty($data)) {
            foreach ($data as $sku) {
                $collection = $this->collectionFactory->create();
                $collection->addFieldToFilter('sku', $sku)
                ->addFieldToFilter('is_used', 0)
                ->addFieldToFilter('status', 1);

                $qty = $collection->getSize();
                $stockItem = $this->stockRegistry->getStockItemBySku($sku);
                $stockItem->setQty($qty);
                if ($qty > 0) {
                    $stockItem->setIsInStock((bool)$qty);
                }
                $this->stockRegistry->updateStockItemBySku($sku, $stockItem);
            }
        }
    }

    /**
     * convertDateFormat
     *
     * @param  string $format
     * @param  date $date
     * @return string
     */
    public function convertDateFormat($format, $date)
    {
        return \DateTime::createFromFormat($format, $date);
    }

    /**
     * getVouchersByFileId
     *
     * @param  int $fileId
     * @return array
     */
    public function getVouchersByFileId($fileId)
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('file_id', $fileId);
        $collection->addFieldToFilter('is_used', 0);
        $collection->addFieldToSelect(
            ['serial_number', 'vouchers_id', 'sku']
        );

        return $collection->getData();
    }

    /**
     * updateVouchersStatusByVoucherIds
     *
     * @param  array $voucherIds
     * @param  int $status
     * @return void
     */
    public function updateVouchersStatusByVoucherIds($voucherIds, $status)
    {
        $this->evoucherModel->getCollection()
            ->setTableRecords(
                $voucherIds,
                $status
            );
    }

    /**
     * check Date Format
     *
     * @param  string $format
     * @param  string $date
     * @return boolean
     */
    public function checkDateFormat($format, $date)
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
