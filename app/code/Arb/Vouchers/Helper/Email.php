<?php
/**
 * File to save Voucher data form in Database
 *
 * @category Arb
 * @package Arb_Vouchers
 * @author Arb Magento Team
 *
 */

namespace Arb\Vouchers\Helper;

use Magento\Customer\Model\Session;

/**
 * Arb Vouchers Helper Email.
 */
class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Email upload template
     */
    const XML_PATH_EMAIL_VOUCHER_UPLOAD = 'vouchers/email/voucher_upload_email_template';
    
    /**
     * Email template for approve
     */
    const XML_PATH_EMAIL_VOUCHER_APPROVAL = 'vouchers/email/voucher_approve_email_template';

    /**
     * Email template disapprove template
     */
    const XML_PATH_EMAIL_VOUCHER_DISAPPROVE = 'vouchers/email/voucher_disapprove_email_template';

    /**
     * Email template for vendor upload
     */
    const XML_PATH_EMAIL_VOUCHER_UPLOAD_MERCHANT = 'vouchers/email/voucher_vendor_upload';

    /**
     * Email template for vendor failed upload
     */
    const XML_PATH_EMAIL_VOUCHER_UPLOAD_FAILED = 'vouchers/email/voucher_vendor_upload_failed';

    /**
     * Email id for sales email
     */
    const XML_PATH_SALES_EMAIL = 'vouchers/email/sales_email';

    /**
     * extention for failed CSV
     */
    const ERROR_FILE_EXTENTION = 'application/csv';

    /**
     * Prefix for failed CSV file
     */
    const ERROR_FILE_PREFIX = 'error_voucher_upload_';

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var \Arb\Vouchers\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var $_template
     */
    protected $_template;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var $_messageManager
     */
    protected $_messageManager;

    /**
     * @var Magento\Framework\Filesystem\Driver\File
     */
    protected $driverInterface;

    /**
     * Email helper constructor
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Arb\Vouchers\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem\Driver\File $driverInterface
     * @param Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Arb\Vouchers\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\Driver\File $driverInterface,
        Session $customerSession
    ) {
        parent::__construct($context);
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_messageManager = $messageManager;
        $this->driverInterface = $driverInterface;
    }

    /**
     * Return store configuration value.
     *
     * @param string $path
     * @param int    $storeId
     *
     * @return mixed
     */
    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Return store.
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    /**
     * Return template id.
     *
     * @return int
     */
    public function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }

    /**
     * Return sales team email id.
     *
     * @return int
     */
    public function getSalesTeamEmailId()
    {
        $xmlPath = self::XML_PATH_SALES_EMAIL;
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }

    /**
     * Return admin email.
     *
     * @return string
     */
    public function getSenderEmail()
    {
        $adminEmail = $this->getConfigValue(
            'trans_email/ident_general/email',
            $this->getStore()->getStoreId()
        );

        return $adminEmail;
    }

     /**
      * generate email Template
      *
      * @param array $emailTemplateVariables
      * @param array $senderInfo
      * @param array $receiverInfo
      * @param array $attachment
      */
    public function generateTemplate(
        $emailTemplateVariables,
        $senderInfo,
        $receiverInfo,
        $attachment = null,
        $fileName = null
    ) {
        $template = $this->_transportBuilder->setTemplateIdentifier($this->_template)
            ->setTemplateOptions(
                [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->_storeManager->getStore()->getId(),
                    ]
            )
            ->setTemplateVars($emailTemplateVariables)
            ->setFrom($senderInfo)
            ->addTo($receiverInfo['email'], $receiverInfo['name'])
            ->setReplyTo($senderInfo['email'], $senderInfo['name']);
        if ($attachment != null) {
            $fileContent = $this->driverInterface->fileGetContents($attachment);
            $template->addAttachment(
                $fileContent,
                $fileName,
                self::ERROR_FILE_EXTENTION
            );
        }
        return $this;
    }

    /**
     * send Voucher Upload Mail to Admin
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendFailedVoucherMailToAdmin(
        $emailTemplateVariables,
        $senderInfo,
        $receiverInfo,
        $attachment,
        $fileName
    ) {
        $this->_template = $this->getTemplateId(self::XML_PATH_EMAIL_VOUCHER_UPLOAD);
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo, $attachment, $fileName);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * send Voucher Approve Mail to merchant
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendVoucherApproveMail($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->_template = $this->getTemplateId(self::XML_PATH_EMAIL_VOUCHER_APPROVAL);
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * send Voucher Disapprove Mail to merchant
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendVoucherDisapproveMail(
        $emailTemplateVariables,
        $senderInfo,
        $receiverInfo,
        $attachment,
        $fileName
    ) {
        $this->_template = $this->getTemplateId(self::XML_PATH_EMAIL_VOUCHER_DISAPPROVE);
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo, $attachment, $fileName);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * Sends failed vouchers mail to merchant
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     * @param string $attachment
     * @param string $fileName
     */
    public function sendFailedVoucherMailToMerchant(
        $emailTemplateVariables,
        $senderInfo,
        $receiverInfo,
        $attachment,
        $fileName
    ) {
        $this->_template = $this->getTemplateId(self::XML_PATH_EMAIL_VOUCHER_UPLOAD_FAILED);
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo, $attachment, $fileName);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }

    /**
     * Sends voucher upload email to admin
     *
     * @param array $emailTemplateVariables
     * @param array $senderInfo
     * @param array $receiverInfo
     */
    public function sendVoucherMailToAdmin($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->_template = $this->getTemplateId(self::XML_PATH_EMAIL_VOUCHER_UPLOAD_MERCHANT);
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->_inlineTranslation->resume();
    }
}
