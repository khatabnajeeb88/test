<?php
/**
 * Banner table installer
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * This class upgrade Banner database schema class
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrade table schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings("unused")
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        if (version_compare($context->getVersion(), "1.0.6", "<")) {
            $tableName = $installer->getTable('Arb_Banners');
            $fullTextIntex = ['title'];
            
            $setup->getConnection()->addIndex(
                $tableName,
                $installer->getIdxName(
                    $tableName,
                    $fullTextIntex,
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                $fullTextIntex,
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }
        
        /*
        * Adding a new table - Arb_Banners_store for store id in GRID/FORM
        */
        if (version_compare($context->getVersion(), "1.0.7", "<")) {
            $tableArbStore = $installer->getTable('Arb_Banners_store');

            $table = $installer->getConnection()
                    ->newTable($tableArbStore)
                    ->addColumn(
                        'store_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        null,
                        [
                               'unsigned'  => true,
                                'nullable'  => false,
                                'primary'   => true,
                            ]
                    )->addColumn(
                        'banners_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        null,
                        [
                                'nullable'  => false,
                                'primary'   => true,
                            ]
                    );

                $installer->getConnection()->createTable($table);
        }

        /*
        * Adding a new table - Arb_Banners_store for store id in GRID/FORM
        * With foregn key and cascading delete
        */
        if (version_compare($context->getVersion(), "1.0.8", "<")) {
            $tableArbStore = $installer->getTable('Arb_Banners_store');
            if ($installer->getConnection()->isTableExists($tableArbStore)) {
                $installer->getConnection()->dropTable($tableArbStore);
            }
            $table = $installer->getConnection()
                    ->newTable($tableArbStore)
                    ->addColumn(
                        'banners_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['unsigned' => true, 'nullable' => false, 'primary' => true],
                        'Banners ID'
                    )->addColumn(
                        'store_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        null,
                        ['unsigned' => true, 'nullable' => false, 'primary' => true],
                        'Store ID'
                    )
                    ->addIndex(
                        $installer->getIdxName('Arb_Banners_store', ['store_id']),
                        ['store_id']
                    )->addForeignKey(
                        $installer->getFkName('Arb_Banners_store', 'banners_id', 'Arb_Banners', 'banners_id'),
                        'banners_id',
                        $installer->getTable('Arb_Banners'),
                        'banners_id',
                        \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                    )->addForeignKey(
                        $installer->getFkName('Arb_Banners_store', 'store_id', 'store', 'store_id'),
                        'store_id',
                        $installer->getTable('store'),
                        'store_id',
                        \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                    )->setComment(
                        'Arb Banners To Store Linkage Table'
                    );

                $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
