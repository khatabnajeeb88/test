## Synopsis
This is module for ARB Banners module. Banners are configured from Admin and displayed via API.
Admin user can upload and sort banner images from admin panel.

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).
4. Web API configuration details in [webapi.xml](etc/webapi.xml).
5. Admin ACL configuration [acl.xml](etc/acl.xml).
6. Admin menu configuration [mexu.xml](etc/adminhtml/menu.xml).
7. Admin menu routing [routes.xml](etc/adminhtml/routes.xml).


