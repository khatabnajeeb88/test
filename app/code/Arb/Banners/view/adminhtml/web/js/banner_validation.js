/**
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 */

/**
 * @api
 */
require(
    [
            'Magento_Ui/js/lib/validation/validator',
            'jquery',
            'Magento_Ui/js/lib/validation/utils',
            'mage/translate'
    ],
    function (validator, $,utils) {

            validator.addRule(
                'validate-alphanum-with-custom-rule',
                function (value) {
                    return utils.isEmpty(value) || /^[a-zA-Z0-9\-:{}\s ]+$/.test(value);
                }
                ,
                $.mage.__('Letters, numbers, {} , : or - only please')
            );
    }
);