<?php
/**
 * This file consist of class Banners which is used to define status of banner.
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Model;

/**
 * class Bannerstore returns status
 */
class Bannerstore extends \Magento\Framework\Model\AbstractModel
{

     const CACHE_TAG = "Arb_Banners_store";
/**
 *
 * @return void
 */
    protected function _construct()
    {
        $this->_init(\Arb\Banners\Model\ResourceModel\Bannerstore::class);
    }

 /**
  * get Identities from Cache.
  */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
