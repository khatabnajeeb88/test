<?php
/**
 * File for calling Banner API
 *
 * @category Arb
 * @package  Arb_Banners
 * @author   Arb Magento Team
 */
namespace Arb\Banners\Model;

use Arb\Banners\Model\ResourceModel\Banners\CollectionFactory as BannersCollectionFactory;
use Magento\Store\Model\StoreManagerInterface as storeManagerInterface;

/**
 * Consist of Banner API implemetation
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class BannerManagement implements \Arb\Banners\Api\BannerManagementInterface
{
    /**
     * @SuppressWarnings(PHPMD.LongVariable)
     */
    protected $bannersCollectionFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    const DEFAULT_STORE_ID = 0;

    /**
     *
     * @param BannersCollectionFactory $bannersCollectionFactory
     * @param storeManagerInterface $storeManager
     */
    public function __construct(
        storeManagerInterface $storeManager,
        BannersCollectionFactory $bannersCollectionFactory
    ) {
        $this->bannersCollectionFactory = $bannersCollectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * getBanners according to store view(English or Arabic)
     * @return array
     */
    public function getBanners()
    {
        $storeId = $this->storeManager->getStore()->getId();
        $storeFilter = [self::DEFAULT_STORE_ID,$storeId];
        $collection = $this->bannersCollectionFactory->create();
        $collection->addFieldTofilter('status', 1);
        $collection->addFieldTofilter('Arb_Banners_store.store_id', ["in"=>$storeFilter]);
        $collection->setOrder('sort_order', 'ASC');
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getData();
        }
        return $items;
    }
}
