<?php
/**
 * This file consist of class Banners which is used to define status of banner.
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Model;

/**
 * class Banners returns status
 */
class Banners extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{

    /**
     * Banner cache tag
     */
    public const CACHE_TAG = 'arb_banners';

    /**
     * @var string
     */
    public $_eventPrefix = 'arb_banners';


    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\Banners\Model\ResourceModel\Banners::class);
    }

    /**
     * Returns status
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        $availableOptions = ['1' => 'Enable',
                          '0' => 'Disable'];
        return $availableOptions;
    }

    /**
     * Returns status
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
