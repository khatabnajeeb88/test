<?php
/**
 * Collection class for Banner ressource model
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Model\ResourceModel\Banners;

/**
 * @codeCoverageIgnore
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'banners_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\Banners\Model\Banners::class, \Arb\Banners\Model\ResourceModel\Banners::class);
        $this->_map['fields']['banners_id'] = 'main_table.banners_id';
        $this->_map['fields']['store_id'] = 'Arb_Banners_store.store_id';
    }

    /**
     * join store table in collection
     *
     * @return this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->join(
            ['Arb_Banners_store' => "Arb_Banners_store"],
            "main_table.banners_id = Arb_Banners_store.banners_id",
            ["*"]
        );
        $this->getSelect()->group("main_table.banners_id");
        return $this;
    }
}
