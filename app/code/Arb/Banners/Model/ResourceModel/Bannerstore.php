<?php
/**
 * Banner DB adapter
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Model\ResourceModel;

class Bannerstore extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     * @return $this
     */
    protected function _construct()
    {
        $this->_init('Arb_Banners_store', 'banners_id');
    }
}
