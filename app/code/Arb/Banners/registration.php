<?php
/**
 * Registration file for Banners module
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */

// @codeCoverageIgnoreStart
/** it is a default magento module registration code */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_Banners',
    __DIR__
);
// @codeCoverageIgnoreEnd
