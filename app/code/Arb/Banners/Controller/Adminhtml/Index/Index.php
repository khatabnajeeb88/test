<?php
/**
 * Banner GRID page to display UI GRID in Admin panel
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index to display GRID
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    /**
     * Check the permission to run it
     * Code coverage is ignored as function _iSAllowed() does not provide coverage in core.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
       // @codeCoverageIgnoreStart
        return $this->_authorization->isAllowed('Arb_Banners::banner');
        // @codeCoverageIgnoreEnd
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Arb_Banners::banner');
        $resultPage->addBreadcrumb(__('Banners'), __('Banners'));
        $resultPage->addBreadcrumb(__('Manage Banners'), __('Manage Banners'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Banners'));

        return $resultPage;
    }
}
