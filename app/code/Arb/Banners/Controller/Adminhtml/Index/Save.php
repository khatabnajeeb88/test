<?php
/**
 * File to save banner data form in Database
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Arb\Banners\Model\Banners;
use Arb\Banners\Model\Bannerstore;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;

/**
 * Consist of Banner save function
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
   
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    /**
     * @var ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory,
        ResourceConnection $_resourceConnection
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->_resourceConnection = $_resourceConnection;
        $this->connection = $_resourceConnection->getConnection();
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('banners_id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = 1;
            }
            if (empty($data['banners_id'])) {
                $data['banners_id'] = null;
            }
            if (isset($data['bannerimage'][0]['name']) && isset($data['bannerimage'][0]['tmp_name'])) {
                $data['bannerimage'] ='/banners/'.$data['bannerimage'][0]['name'];
            } elseif (isset($data['bannerimage'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['bannerimage'] =$data['bannerimage'][0]['name'];
            } else {
                $data['bannerimage'] = null;
            }

            try {
                $model = $this->_objectManager->create(\Arb\Banners\Model\Banners::class);
                if (!empty($id)) {
                    $model->load($id);
                }

                $model->setData($data);
                $this->inlineTranslation->suspend();
                $model->save();

                $bannersId = $model->getBannersId();

                /*save store_id*/
                if (isset($data["store_id"])) {
                    foreach ($data["store_id"] as $value) {
                        $arbStore[] =["store_id"=>$value,"banners_id"=>$bannersId];
                    }
                    /*
                    *  Assuming update in store view permition for banners so
                    * removing older store views and seeting up new one.
                    * deleteOldRecords
                    * insertMultiple
                    */
                    $this->deleteOldRecords(
                        "Arb_Banners_store",
                        ["banners_id = ?"=>$bannersId]
                    );
                    $this->insertMultiple("Arb_Banners_store", $arbStore);
                }

                $this->messageManager->addSuccess(__('Banner Saved successfully'));
                $this->dataPersistor->clear('Arb_Banners');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['banners_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the banner.'));
            }
        }
    }

    /*
    *  Assuming update in store view permition for banners so
    * setting up new one.
    * insertMultiple
    */

    public function insertMultiple($table, $data)
    {
        $tableName = $this->_resourceConnection->getTableName($table);
        return $this->connection->insertOnDuplicate($tableName, $data);
    }

    /*
    *  Assuming update in store view permition for banners so
    * removing older store views
    * deleteOldRecords
    */

    public function deleteOldRecords($table, $data)
    {
        $tableName = $this->_resourceConnection->getTableName($table);
        return $this->connection->delete($tableName, $data);
    }
}
