<?php
/**
 * This file consist of class Delete which execute delete banner action.
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Controller\Adminhtml\Index;

/**
 * Consist of Banner backend actions
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * Check admin permissions for this controller
     * Code coverage is ignored as function _iSAllowed() does not provide coverage in core.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
         // @codeCoverageIgnoreStart
        return $this->_authorization->isAllowed('Arb_Banners::banner');
        // @codeCoverageIgnoreEnd
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('banners_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Arb\Banners\Model\Banners::class);
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The banner has been deleted.'));
                // go to grid
                $this->_eventManager->dispatch(
                    'adminhtml_banners_on_delete',
                    ['title' => $title, 'status' => 'success']
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_banners_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['banners_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a banner to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
