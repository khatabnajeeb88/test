<?php
/**
 * SaveAndContinueButton PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Block\Adminhtml\Banners;

/**
 * SaveAndContinueButtonTest class to test SaveAndContinueButton class
 */
class SaveAndContinueButtonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Arb\Banners\Block\Adminhtml\Banners\SaveAndContinueButton
     */
    protected $block;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $urlBuilderMock;

    /**
     * Main setup method
     */
    protected function setUp()
    {
        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->getMock();
        $this->block = new \Arb\Banners\Block\Adminhtml\Banners\SaveAndContinueButton(
            $this->urlBuilderMock
        );
    }

    /**
     * testGetButtonData method
     */
    public function testGetButtonData()
    {
        $expectedResult = [
            'label' => __('Save and Continue Edit'),
            'class' => 'save',
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit'],
                ],
            ],
            'sort_order' => 80,
        ];

        $this->assertEquals($expectedResult, $this->block->getButtonData());
    }
}
