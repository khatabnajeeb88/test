<?php
/**
 * ResetButton PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Block\Adminhtml\Banners;

/**
 * ResetButtonTest class for testing BackButton block class
 */
class ResetButtonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\AsynchronousOperations\Block\Adminhtml\Bulk\Details\ResetButton
     */
    protected $block;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $urlBuilderMock;

    /**
     * Main setup method
     */
    protected function setUp()
    {
        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->getMock();
        $this->block = new \Arb\Banners\Block\Adminhtml\Banners\ResetButton(
            $this->urlBuilderMock
        );
    }

    /**
     * testGetButtonData method
     */
    public function testGetButtonData()
    {
        $expectedResult = [
            'label' => __('Reset'),
            'on_click' => 'location.reload();',
            'class' => 'reset',
            'sort_order' => 30
        ];

        $this->assertEquals($expectedResult, $this->block->getButtonData());
    }
}
