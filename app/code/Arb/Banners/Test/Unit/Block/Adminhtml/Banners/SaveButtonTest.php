<?php
/**
 * SaveButtonTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Block\Adminhtml\Banners;

/**
 * SaveButtonTest class for PHPUnit test of SaveButton class
 */
class SaveButtonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Arb\Banners\Block\Adminhtml\Banners\SaveButton
     */
    protected $block;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $urlBuilderMock;

    /**
     * Main setup method
     */
    protected function setUp()
    {
        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->getMock();
        $this->block = new \Arb\Banners\Block\Adminhtml\Banners\SaveButton(
            $this->urlBuilderMock
        );
    }

    /**
     * testGetButtonData method
     */
    public function testGetButtonData()
    {
        $expectedResult = [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];

        $this->assertEquals($expectedResult, $this->block->getButtonData());
    }
}
