<?php
/**
 * BackButton PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Block\Adminhtml\Banners;

/**
 * BackButtonTest class for testing BackButton block class
 */
class BackButtonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\AsynchronousOperations\Block\Adminhtml\Bulk\Details\BackButton
     */
    protected $block;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $urlBuilderMock;

    /**
     * Main setup method
     */
    protected function setUp()
    {
        $this->urlBuilderMock = $this->getMockBuilder(\Magento\Framework\UrlInterface::class)
            ->getMock();
        $this->block = new \Arb\Banners\Block\Adminhtml\Banners\BackButton(
            $this->urlBuilderMock
        );
    }

    /**
     * testGetButtonData method
     */
    public function testGetButtonData()
    {
        $backUrl = 'back url';
        $expectedResult = [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $backUrl),
            'class' => 'back',
            'sort_order' => 10
        ];

        $this->urlBuilderMock->expects($this->once())
            ->method('getUrl')
            ->with('*/')
            ->willReturn($backUrl);

        $this->assertEquals($expectedResult, $this->block->getButtonData());
    }
}
