<?php
/**
 * EditTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Controller\Adminhtml\Index;

use Magento\Cms\Test\Unit\Controller\Adminhtml\AbstractMassActionTest;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class MassStatusTest for testing Edit class
 * @covers \Arb\Banners\Controller\Adminhtml\Index\MassStatus
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class MassStatusTest extends AbstractMassActionTest
{
    /**
     * @var \Arb\Banners\Controller\Adminhtml\Index\MassStatus
     */
    protected $massStatusController;

    /**
     * @var \Magento\Framework\Message\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $messageManagerMock;

    /**
     * @var \Arb\Banners\Model\ResourceModel\Banners\CollectionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $collectionFactoryMock;

    /**
     * @var \Arb\Banners\Model\ResourceModel\Banners\Collection|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $bannerCollectionMock;

    /** @var \Magento\Framework\App\RequestInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $requestMock;

    /**
     * @var \Magento\Framework\Controller\ResultFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultFactoryMock;

    /**
     * main setup method
     */
    protected function setUp()
    {
        parent::setUp();

        $this->collectionFactoryMock = $this->createPartialMock(
            \Arb\Banners\Model\ResourceModel\Banners\CollectionFactory::class,
            ['create']
        );

        $this->resultFactoryMock = $this->createPartialMock(
            \Magento\Framework\Controller\ResultFactory::class,
            ['create']
        );

        $this->resultFactoryMock->expects($this->any())
            ->method('create')
            ->willReturnMap(
                [
                    [ResultFactory::TYPE_REDIRECT, [], $this->resultRedirectMock]
                ]
            );

        $this->bannerCollectionMock = $this->createMock(\Arb\Banners\Model\ResourceModel\Banners\Collection::class);
        $this->messageManagerMock = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            []
        );

        $this->contextMock = $this->createMock(\Magento\Backend\App\Action\Context::class);
        $this->contextMock->expects($this->once())->method('getRequest')->willReturn($this->requestMock);
        $this->contextMock->expects($this->once())->method('getMessageManager')->willReturn($this->messageManagerMock);
        $this->contextMock->expects($this->any())->method('getResultFactory')->willReturn($this->resultFactoryMock);

        $this->massStatusController = $this->objectManager->getObject(
            \Arb\Banners\Controller\Adminhtml\Index\MassStatus::class,
            [
                'context' => $this->contextMock,
                'filter' => $this->filterMock,
                'collectionFactory' => $this->collectionFactoryMock
            ]
        );
    }

    /**
     * testMassEnableAction method
     */
    public function testMassEnableAction()
    {
        $enabledbannersCount = 2;

        $status = 1;

        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->with('status')
            ->willReturn($status);
       
        $collection = [
            $this->getBannerMock(),
            $this->getBannerMock()
        ];

        $this->collectionFactoryMock->expects($this->once())->method('create')->willReturn($this->bannerCollectionMock);

        $this->filterMock->expects($this->once())
            ->method('getCollection')
            ->with($this->bannerCollectionMock)
            ->willReturn($this->bannerCollectionMock);

        $this->bannerCollectionMock->expects($this->once())->method('getSize')->willReturn($enabledbannersCount);
        $this->bannerCollectionMock->expects($this->once())
            ->method('getIterator')
            ->willReturn(new \ArrayIterator($collection));

        $this->messageManagerMock->expects($this->once())
            ->method('addSuccess')
            ->with(__('A total of %1 record(s) have been changed.', $enabledbannersCount));
        $this->messageManagerMock->expects($this->never())->method('addErrorMessage');

        $this->resultRedirectMock->expects($this->once())
            ->method('setPath')
            ->with('*/*/')
            ->willReturnSelf();

        $this->assertSame($this->resultRedirectMock, $this->massStatusController->execute());
    }

    /**
     * Create banner Collection Mock
     *
     * @return \Arb\Banners\Model\ResourceModel\Banners\Collection|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getBannerMock()
    {
        $pageMock = $this->createPartialMock(
            \Arb\Banners\Model\ResourceModel\Banners\Collection::class,
            ['setStatus', 'save']
        );
        $pageMock->expects($this->once())->method('setStatus')->with(true)->willReturn(true);
        $pageMock->expects($this->once())->method('save')->willReturn(true);

        return $pageMock;
    }
}
