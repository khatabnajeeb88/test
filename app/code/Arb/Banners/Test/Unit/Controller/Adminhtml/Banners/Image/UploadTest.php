<?php
/**
 * UploadTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Controller\Adminhtml\Banners\Image;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Banners\Controller\Adminhtml\Banners\Image\Upload as Model;
use Magento\Framework\App\Request\Http as Request;
use Arb\Banners\Model\ImageUploader;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\DataObject;

/**
 * UploadTest class for testing Upload class
 */
class UploadTest extends TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    private $objectManager;

    /**
     * main setup method
     */
    protected function setUp()
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
    }

    /**
     * @return array
     */
    public function executeDataProvider()
    {
        return [
            ['bannerimage']
        ];
    }

    /**
     * @param string $savedName
     *
     * @dataProvider executeDataProvider
     */
    public function testExecute($savedName)
    {

        $uploader = $this->createPartialMock(ImageUploader::class, ['saveFileToTmpDir']);

        $resultFactory = $this->createPartialMock(ResultFactory::class, ['create']);

        $resultFactory->expects($this->once())
            ->method('create')
            ->will($this->returnValue(new DataObject()));

        $model = $this->objectManager->getObject(Model::class, [
            'resultFactory' => $resultFactory,
            'imageUploader' => $uploader
        ]);

        $uploader->expects($this->once())
            ->method('saveFileToTmpDir')
            ->with($savedName)
            ->will($this->returnValue([]));

        $model->execute();
    }

    /**
     * @return array
     */
    public function executeDataProviderforError()
    {
        return [
            ['bannerimage']
        ];
    }

    /**
     * @param string $savedName
     *
     * @dataProvider executeDataProviderforError
     */
    public function testExecuteForError($savedName)
    {

        $uploader = $this->createPartialMock(ImageUploader::class, ['saveFileToTmpDir']);

        $resultFactory = $this->createPartialMock(ResultFactory::class, ['create']);

        $resultFactory->expects($this->once())
            ->method('create')
            ->will($this->returnValue(new DataObject()));

        $model = $this->objectManager->getObject(Model::class, [
            'resultFactory' => $resultFactory,
            'imageUploader' => $uploader
        ]);

        $mockError = new \Magento\Framework\Phrase('File can not be saved to the destination folder');
        $mockClass = new \Magento\Framework\Exception\LocalizedException($mockError);

        $uploader->expects($this->once())
            ->method('saveFileToTmpDir')
            ->with($savedName)
            ->willThrowException($mockClass);

        $model->execute();
    }
}
