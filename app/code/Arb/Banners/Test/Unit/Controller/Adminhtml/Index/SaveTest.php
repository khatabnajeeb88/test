<?php
/**
 * SaveTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Controller\Adminhtml\Index;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class SaveTest for testing Save class
 * @SuppressWarnings(PHPMD)
 * @covers \Arb\Banners\Controller\Adminhtml\Index\Save
 */
class SaveTest extends TestCase
{
    
    /**
     * @var \Magento\Framework\App\RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $requestMock;

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $dataPersistorMock;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultRedirect;

    /**
     * @var \Magento\Backend\App\Action\Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $contextMock;

    /**
     * @var \Magento\Framework\ObjectManager\ObjectManager|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $objectManagerMock;

    /**
     * @var \Magento\Framework\Message\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $messageManagerMock;

    /**
     * @var \Magento\Framework\Event\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $eventManagerMock;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \Magento\Cms\Controller\Adminhtml\Block\Save
     */
    protected $saveController;

    /**
     * @var \Arb\Banners\Model\Banners|\PHPUnit_Framework_MockObject_MockObject
     */
    private $bannersFactory;

    /**
     * @var \Magento\Framework\Escaper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $escaper;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime|\PHPUnit_Framework_MockObject_MockObject
     */
    private $dateFactoryInstance;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTimeFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $dateFactory;

    /**
     * @var int
     */
    protected $blockId = 1;

    protected function setUp()
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->contextMock = $this->createMock(\Magento\Backend\App\Action\Context::class);
        $this->escaper = $this->createMock(\Magento\Framework\Escaper::class);
        $this->inlineTranslation = $this->createMock(\Magento\Framework\Translate\Inline\StateInterface::class);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->dateFactoryInstance = $this->createMock(\Magento\Framework\Stdlib\DateTime\DateTime::class);
        $this->dateFactory = $this->createMock(\Magento\Framework\Stdlib\DateTime\DateTimeFactory::class);
        $this->dateFactory->expects($this->any())->method('create')->willReturn($this->dateFactoryInstance);

        $this->resultRedirectFactory = $this->getMockBuilder(\Magento\Backend\Model\View\Result\RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->resultRedirect = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Redirect::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultRedirectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultRedirect);

        $this->bannerMock = $this->getMockBuilder(\Arb\Banners\Model\Banners::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->dataPersistorMock = $this->getMockBuilder(\Magento\Framework\App\Request\DataPersistorInterface::class)
            ->getMock();

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['getParam', 'getPostValue']
        );

        $this->messageManagerMock = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);

        $this->eventManagerMock = $this->getMockForAbstractClass(
            \Magento\Framework\Event\ManagerInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['dispatch']
        );

        $this->objectManagerMock = $this->getMockBuilder(\Magento\Framework\ObjectManager\ObjectManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['get', 'create'])
            ->getMock();

        $this->objectManagerMock->expects($this->any())
            ->method('create')
            ->with(\Arb\Banners\Model\Banners::class)
            ->willReturn($this->bannerMock);

        $this->contextMock->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $this->contextMock->expects($this->any())->method('getObjectManager')->willReturn($this->objectManagerMock);
        $this->contextMock->expects($this->any())->method('getMessageManager')->willReturn($this->messageManagerMock);
        $this->contextMock->expects($this->any())->method('getEventManager')->willReturn($this->eventManagerMock);
        $this->contextMock->expects($this->any())
            ->method('getResultRedirectFactory')
            ->willReturn($this->resultRedirectFactory);

        $this->bannersFactory = $this->getMockBuilder(\Arb\Banners\Model\BannersFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->saveController = $this->objectManager->getObject(
            \Arb\Banners\Controller\Adminhtml\Index\Save::class,
            [
                'context' => $this->contextMock,
                'dataPersistor' => $this->dataPersistorMock,
                'escaper' => $this->escaper,
                'inlineTranslation' => $this->inlineTranslation,
                'scopeConfig' => $this->scopeConfig,
                'dateFactory' => $this->dateFactory,
            ]
        );
    }

    /**
     * @param int $bannerId
     * @dataProvider editActionData
     */
    public function testSaveAction($bannerId, $postData)
    {

        $this->requestMock->expects($this->any())->method('getPostValue')->willReturn($postData);
        $this->requestMock->expects($this->any())
            ->method('getParam')
            ->willReturnMap(
                [
                    ['banners_id', null, 1],
                    ['back', null, 'continue'],
                ]
            );

        $this->bannerMock->expects($this->any())
            ->method('load')
            ->with($bannerId);

        $this->bannerMock->expects($this->any())
            ->method('getId')
            ->willReturn($bannerId);

        $this->dataPersistorMock->expects($this->any())
            ->method('clear')
            ->with('Arb_Banners');

        $this->messageManagerMock->expects($this->any())
            ->method('addSuccess')
            ->with(__('Banner Saved successfully'));

        $this->dataPersistorMock->expects($this->any())
            ->method('set')
            ->with('Arb_Banners', $postData);

        $this->resultRedirect->expects($this->any())->method('setPath')->with('*/*/edit') ->willReturnSelf();

        $this->assertSame($this->resultRedirect, $this->saveController->execute());
    }

    /**
     * @param int $bannerId
     * @dataProvider saveActionData2
     */
    public function testSaveActionNoId($bannerId, $postData)
    {

        $this->requestMock->expects($this->any())->method('getPostValue')->willReturn($postData);
        $this->requestMock->expects($this->any())
            ->method('getParam')
            ->willReturnMap(
                [
                    ['banners_id', null, null],
                    ['back', null, false],
                ]
            );

        $this->bannerMock->expects($this->any())
            ->method('load')
            ->with($bannerId);

        $this->bannerMock->expects($this->any())
            ->method('getId')
            ->with(1);

        $this->messageManagerMock
            ->method('addError')
            ->with(__('This Banner no longer exists.'));

        $this->resultRedirect->expects($this->any())->method('setPath')->with('*/*/') ->willReturnSelf();

        $this->assertSame($this->resultRedirect, $this->saveController->execute());
    }
    
    /**
     * @param int $bannerId
     * @dataProvider saveActionData3
     */
    public function testSaveActionThrowsException($bannerId, $postData)
    {
        $this->requestMock->expects($this->any())->method('getPostValue')->willReturn($postData);
        $this->requestMock->expects($this->atLeastOnce())
            ->method('getParam')
            ->willReturnMap(
                [
                    ['banners_id', null, null],
                    ['back', null, true],
                ]
            );
        
        $this->bannerMock->expects($this->any())
            ->method('load')
            ->with($bannerId);

        $this->bannerMock->expects($this->any())
            ->method('getId')
            ->willReturn($bannerId);

        $this->messageManagerMock->expects($this->any())
            ->method('addException');

        $this->dataPersistorMock->expects($this->any())
            ->method('set')
            ->with('Arb_Banners', array_merge($postData, ['banners_id' => null]));

        $this->assertSame(null, $this->saveController->execute());
    }

    /**
     * @return array
     */
    public function editActionData()
    {
        $postData1 = [
            'title' => 'test',
            'link' => 'test',
            'target' => 'test',
            'sort_order' => 1,
            'status' => 'true',
            'bannerimage' => [0=>['name'=>'test.jpg', 'tmp_name'=>'test.jpg']],
            'back' => 'continue'
        ];

        $postData2 = [
            'title' => 'test',
            'link' => 'test',
            'target' => 'test',
            'sort_order' => 1,
            'status' => 'true',
            'bannerimage' => [0=>['name'=>'test.jpg']],
            'back' => 'continue'
        ];

        $postData3 = [
            'title' => 'test',
            'link' => 'test',
            'target' => 'test',
            'sort_order' => 1,
            'status' => 1,
            'bannerimage' => null,
            'back' => 'continue',
            'banners_id' => null
        ];

        $postData4 = [
            'title' => null
        ];

        return [
            [1,$postData1],
            [1, $postData2],
            [1, $postData3],
           
        ];
    }

    /**
     * @return array
     */
    public function saveActionData2()
    {
        
        $postData2 = [
            'title' => 'test',
            'link' => 'test',
            'target' => 'test',
            'sort_order' => 1,
            'status' => 1,
            'bannerimage' => null,
            'back' => 'continue'
        ];
        return [
            [null, $postData2]
        ];
    }

    /**
     * @return array
     */
    public function saveActionData3()
    {
        
        $postData2 = [
            'title' => 'test',
            'link' => 'test',
            'target' => 'test',
            'sort_order' => 1,
            'status' => 1,
            'bannerimage' => 'test'
        ];
        return [
            [2, $postData2]
        ];
    }
}
