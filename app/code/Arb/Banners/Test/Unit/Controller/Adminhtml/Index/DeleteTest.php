<?php
/**
 * DeleteTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Controller\Adminhtml\Index;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class DeleteTest for testing Delete class
 * @covers \Arb\Banners\Controller\Adminhtml\Index\Delete
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class DeleteTest extends TestCase
{
    /**
     * @var \Arb\Banners\Model\Banners\Delete
     */
    protected $deleteController;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \Magento\Backend\App\Action\Context|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $contextMock;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultRedirectFactoryMock;

    /**
     * @var \Magento\Backend\Model\View\Result\Redirect|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultRedirectMock;

    /**
     * @var \Magento\Framework\Message\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $messageManagerMock;

    /**
     * @var \Magento\Framework\App\RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $requestMock;

    /**
     * @var \Magento\Framework\ObjectManager\ObjectManager|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $objectManagerMock;

    /**
     * @var \Arb\Banners\Model\Banners|\PHPUnit_Framework_MockObject_MockObject $bannerMock
     */
    protected $bannerMock;

    /**
     * @var \Magento\Framework\Event\ManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $eventManagerMock;

    /** @var string */
    protected $title = '';

    /**
     * @var int
     */
    protected $bannerId = 1;

    /**
     * Main setup method
     */
    protected function setUp()
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->messageManagerMock = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['getParam']
        );

        $this->bannerMock = $this->getMockBuilder(\Arb\Banners\Model\Banners::class)
            ->disableOriginalConstructor()
            ->setMethods(['load', 'delete','getTitle'])
            ->getMock();

        $this->objectManagerMock = $this->getMockBuilder(\Magento\Framework\ObjectManager\ObjectManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->resultRedirectMock = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Redirect::class)
            ->setMethods(['setPath'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultRedirectFactoryMock = $this->getMockBuilder(
            \Magento\Backend\Model\View\Result\RedirectFactory::class
        )
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->resultRedirectFactoryMock->expects($this->atLeastOnce())
            ->method('create')
            ->willReturn($this->resultRedirectMock);

        $this->eventManagerMock = $this->createMock(\Magento\Framework\Event\ManagerInterface::class);

        $this->contextMock = $this->createMock(\Magento\Backend\App\Action\Context::class);

        $this->contextMock->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $this->contextMock->expects($this->any())->method('getMessageManager')->willReturn($this->messageManagerMock);
        $this->contextMock->expects($this->any())->method('getObjectManager')->willReturn($this->objectManagerMock);
        $this->contextMock->expects($this->any())->method('getEventManager')->willReturn($this->eventManagerMock);
        $this->contextMock->expects($this->any())
            ->method('getResultRedirectFactory')
            ->willReturn($this->resultRedirectFactoryMock);

        $this->deleteController = $this->objectManager->getObject(
            \Arb\Banners\Controller\Adminhtml\Index\Delete::class,
            [
                'context' => $this->contextMock,
            ]
        );
    }

    /**
     * testDeleteAction method
     */
    public function testDeleteAction()
    {
        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->willReturn($this->bannerId);

        $this->objectManagerMock->expects($this->once())
            ->method('create')
            ->with(\Arb\Banners\Model\Banners::class)
            ->willReturn($this->bannerMock);

        $this->bannerMock->expects($this->once())
            ->method('load')
            ->with($this->bannerId);

        $this->bannerMock->expects($this->once())
            ->method('getTitle')
            ->willReturn($this->title);
        $this->bannerMock->expects($this->once())
            ->method('delete');

        $this->messageManagerMock->expects($this->any())
            ->method('addSuccessMessage')
            ->with(__('The banner has been deleted.'));

        $this->eventManagerMock->expects($this->once())
            ->method('dispatch')
            ->with(
                'adminhtml_banners_on_delete',
                ['title' => $this->title, 'status' => 'success']
            );

        $this->resultRedirectMock->expects($this->once())
            ->method('setPath')
            ->with('*/*/')
            ->willReturnSelf();

        $this->assertSame($this->resultRedirectMock, $this->deleteController->execute());
    }

    /**
     * testDeleteActionNoId method
     */
    public function testDeleteActionNoId()
    {
        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->willReturn(null);

        $this->messageManagerMock->expects($this->once())
            ->method('addError')
            ->with(__('We can\'t find a banner to delete.'));

        $this->resultRedirectMock->expects($this->once())
            ->method('setPath')
            ->with('*/*/')
            ->willReturnSelf();

        $this->assertSame($this->resultRedirectMock, $this->deleteController->execute());
    }

    /**
     * testDeleteActionThrowsException method
     */
    public function testDeleteActionThrowsException()
    {
        $errorMsg = 'Can\'t delete the Banner';

        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->willReturn($this->bannerId);

        $this->objectManagerMock->expects($this->once())
            ->method('create')
            ->with(\Arb\Banners\Model\Banners::class)
            ->willReturn($this->bannerMock);

        $this->bannerMock->expects($this->any())
            ->method('load')
            ->with($this->bannerId);

        $this->bannerMock->expects($this->any())
            ->method('getTitle')
            ->willReturn($this->title);

        $this->bannerMock->expects($this->any())
            ->method('delete')
            ->willThrowException(new \Exception(__($errorMsg)));

        $this->eventManagerMock->expects($this->once())
            ->method('dispatch')
            ->with(
                'adminhtml_banners_on_delete',
                ['title' => $this->title, 'status' => 'fail']
            );

        $this->messageManagerMock->expects($this->any())
            ->method('addErrorMessage')
            ->with($errorMsg);

        $this->resultRedirectMock->expects($this->once())
            ->method('setPath')
            ->with('*/*/edit', ['banners_id' => $this->bannerId])
            ->willReturnSelf();

        $this->assertSame($this->resultRedirectMock, $this->deleteController->execute());
    }
}
