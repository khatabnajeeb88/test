<?php
/**
 * ActionsTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Ui\Component\Listing\Column;

use Arb\Banners\Ui\Component\Listing\Column\Actions;
use Magento\Framework\View\Element\UiComponent\Processor;
use Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder;

/**
 * Class ActionsTest for testing  Actions class
 * @covers \Arb\Banners\Ui\Component\Listing\Column\Actions
 */
class ActionsTest extends \PHPUnit\Framework\TestCase
{
    /** @var Actions */
    protected $component;

    /** @var \Magento\Framework\View\Element\UiComponent\ContextInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $context;

    /** @var \Magento\Framework\View\Element\UiComponentFactory|\PHPUnit_Framework_MockObject_MockObject */
    protected $uiComponentFactory;

    /** @var \Magento\Framework\UrlInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $urlBuilder;

    /**
     * @var \Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $actionUrlBuilder;

    /**
     * Main set up method
     */
    public function setup()
    {
        $this->context = $this->getMockBuilder(\Magento\Framework\View\Element\UiComponent\ContextInterface::class)
            ->getMockForAbstractClass();
        $processor = $this->getMockBuilder(Processor::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->context->expects($this->never())->method('getProcessor')->willReturn($processor);
        $this->actionUrlBuilder = $this->createMock(UrlBuilder::class);
        $this->uiComponentFactory = $this->createMock(\Magento\Framework\View\Element\UiComponentFactory::class);
        $this->urlBuilder = $this->getMockForAbstractClass(
            \Magento\Framework\UrlInterface::class,
            [],
            '',
            false
        );
        $this->component = new Actions(
            $this->context,
            $this->uiComponentFactory,
            $this->actionUrlBuilder,
            $this->urlBuilder
        );
        $this->component->setData('name', 'name');
    }

    /**
     * testPrepareDataSource method
     */
    public function testPrepareDataSource()
    {
        $dataSource = [
            'data' => [
                'items' => [
                    [
                        'banners_id' => 1
                    ],
                ]
            ]
        ];
        $expectedDataSource = [
            'data' => [
                'items' => [
                    [
                        'banners_id' => 1,
                        'name' => [
                            'edit' => [
                                'href' => 'banners/index/edit',
                                'label' => __('Edit')
                            ],
                            'delete' => [
                                'href' => 'banners/index/delete',
                                'label' => __('Delete'),
                                'confirm' => [
                                    'title' => __('Delete ${ $.$data.title }'),
                                    'message' => __('Are you sure you wan\'t to delete a ${ $.$data.title } record?')
                                ]
                            ]
                        ]
                    ],
                ]
            ]
        ];
        $this->urlBuilder->expects($this->at(0))
            ->method('getUrl')
            ->with(
                'banners/index/edit',
                ['banners_id' => 1]
            )
            ->willReturn('banners/index/edit');
        
        $this->urlBuilder->expects($this->at(1))
            ->method('getUrl')
            ->with(
                'banners/index/delete',
                ['banners_id' => 1]
            )
            ->willReturn('banners/index/delete');

        $dataSource = $this->component->prepareDataSource($dataSource);

        $this->assertEquals($expectedDataSource, $dataSource);
    }
}
