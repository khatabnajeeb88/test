<?php
/**
 * BannerImageTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Ui\Component\Listing\Column;

use Arb\Banners\Ui\Component\Listing\Column\BannerImage;
use Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder;

/**
 * Class BannerImageTest for testing  Actions class
 * @covers \Arb\Banners\Ui\Component\Listing\Column\BannerImage
 */
class BannerImageTest extends \PHPUnit\Framework\TestCase
{
    /** @var Actions */
    protected $component;

    /** @var \Magento\Framework\View\Element\UiComponent\ContextInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $context;

    /** @var \Magento\Framework\View\Element\UiComponentFactory|\PHPUnit_Framework_MockObject_MockObject */
    protected $uiComponentFactory;

    /** @var \Magento\Framework\UrlInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $urlBuilder;

    /** @var \Magento\Store\Model\StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $storeManager;

    /** @var \Magento\Catalog\Helper\Image|\PHPUnit_Framework_MockObject_MockObject */
    protected $imageHelper;

    /** @var \Magento\Store\Model\StoreManager|\PHPUnit_Framework_MockObject_MockObject */
    protected $storeManagerMock;

    /** @var \Magento\Store\Model\Store|\PHPUnit_Framework_MockObject_MockObject */
    protected $storeMock;

    /**
     * Main set up method
     */
    public function setup()
    {
        $this->context = $this->getMockBuilder(\Magento\Framework\View\Element\UiComponent\ContextInterface::class)
            ->getMockForAbstractClass();
        $processor = $this->getMockBuilder(\Magento\Framework\View\Element\UiComponent\Processor::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->context->expects($this->never())->method('getProcessor')->willReturn($processor);
        $this->actionUrlBuilder = $this->createMock(UrlBuilder::class);
        $this->uiComponentFactory = $this->createMock(\Magento\Framework\View\Element\UiComponentFactory::class);
        $this->storeManager = $this->createMock(\Magento\Store\Model\StoreManagerInterface::class);
        $this->storeManagerMock = $this->createPartialMock(\Magento\Store\Model\StoreManager::class, ['getStore']);
        $this->storeMock = $this->createPartialMock(\Magento\Store\Model\Store::class, ['getBaseUrl']);
        $this->imageHelper = $this->createMock(\Magento\Catalog\Helper\Image::class);
        $this->urlBuilder = $this->getMockForAbstractClass(
            \Magento\Framework\UrlInterface::class,
            [],
            '',
            false
        );
        $this->component = new BannerImage(
            $this->context,
            $this->uiComponentFactory,
            $this->imageHelper,
            $this->urlBuilder,
            $this->storeManagerMock
        );
        $this->component->setData('name', 'name');
    }

    /**
     * testPrepareDataSource method
     */
    public function testPrepareDataSource()
    {
        $dataSource = [
            'data' => [
                'items' => [
                    [
                        'banners_id' => 1,
                        'url' => 'base/image/',
                        'name' => 'name',
                        'title' => 'name',
                    ],
                ]
            ]
        ];
        $expectedDataSource = [
            'data' => [
                'items' => [
                    [
                        'name_src' => 'base/image/name',
                        'name_alt' => 'name',
                        'name_link' => 'banners/index/edit',
                        'name_orig_src' => 'base/image/name',
                        'url' => 'base/image/',
                        'name' => 'name',
                        'title' => 'name',
                        'banners_id' => 1
                    ]
                ]
            ]
        ];

        $this->storeManagerMock
            ->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);

        $this->storeMock
            ->expects($this->once())
            ->method('getBaseUrl')
            ->with('media')
            ->willReturn('base/image/');

        $this->urlBuilder->expects($this->at(0))
            ->method('getUrl')
            ->with(
                'banners/index/edit',
                ['banners_id' => 1]
            )
            ->willReturn('banners/index/edit');

        $dataSource = $this->component->prepareDataSource($dataSource);

        $this->assertEquals($expectedDataSource, $dataSource);
    }
}
