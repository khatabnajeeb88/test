<?php
/**
 * DataProviderTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Model\Banners;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class DataProviderTest for testing DataProvider class
 * @covers \Arb\Banners\Model\Banners\DataProvider
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class DataProviderTest extends TestCase
{
    /**
     * @var \Arb\Banners\Model\Banners\DataProvider
     */
    protected $model;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $collectionFactoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $collectionMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $dataPersistorMock;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    private $storeManagerMock;
    
    /**
     * @var \Magento\Store\Model\Store
     */
    private $storeMock;

    /**
     * Main Setup method
     */
    protected function setUp()
    {
        $this->collectionFactoryMock = $this->createPartialMock(
            \Arb\Banners\Model\ResourceModel\Banners\CollectionFactory::class,
            ['create']
        );
        $this->collectionMock = $this->createMock(\Arb\Banners\Model\ResourceModel\Banners\Collection::class);
        $this->collectionFactoryMock->expects($this->once())->method('create')->willReturn($this->collectionMock);
        $this->dataPersistorMock = $this->createMock(\Magento\Framework\App\Request\DataPersistorInterface::class);
        $this->storeManagerMock = $this->createPartialMock(\Magento\Store\Model\StoreManager::class, ['getStore']);
        $this->storeMock = $this->createPartialMock(\Magento\Store\Model\Store::class, ['getBaseUrl']);

        $this->model = new \Arb\Banners\Model\Banners\DataProvider(
            'Name',
            'Primary',
            'Request',
            $this->collectionFactoryMock,
            $this->dataPersistorMock,
            $this->storeManagerMock
        );
    }

    /**
     * testGetData method
     */
    public function testGetData()
    {
        $bannerId = 42;
        $bannerData = [
            'bannerimage' => 'test'
        ];

        $expected = [
            'bannerimage' =>  [
                0 =>  [
                    'name' => 'test',
                    'url' => 'http://url/pub/media/test'
                ]
            ]
        ];

        $this->storeManagerMock
            ->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);

        $this->storeMock
            ->expects($this->once())
            ->method('getBaseUrl')
            ->with('media')
            ->willReturn('http://url/pub/media/');

        $bannerMock = $this->createMock(\Arb\Banners\Model\Banners::class);
        $this->collectionMock->expects($this->once())->method('getItems')->willReturn([$bannerMock]);

        $bannerMock->expects($this->atLeastOnce())->method('getId')->willReturn($bannerId);
        $bannerMock->expects($this->any())->method('load')->willReturnSelf();
        $bannerMock->expects($this->any())->method('getData')->willReturn($bannerData);
        $this->dataPersistorMock->expects($this->once())->method('get')->with('Arb_Banners')->willReturn(null);

        $this->assertEquals([$bannerId => $expected], $this->model->getData());
    }

    /**
     * testGetDataIfRulePersisted method
     */
    public function testGetDataIfRulePersisted()
    {
        $bannerId = 42;
        $bannerData = [
            'bannerimage' =>  [
                0 =>  [
                    'name' => 'test',
                    'url' => 'http://url/pub/media/test'
                ]
            ]
        ];

        $this->storeManagerMock
            ->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);

        $this->storeMock
            ->expects($this->once())
            ->method('getBaseUrl')
            ->with('media')
            ->willReturn('http://url/pub/media/');

        $this->collectionMock->expects($this->once())->method('getItems')->willReturn([]);

        $persistedData = ['bannerimage' => 'test'];
        $this->dataPersistorMock->expects($this->once())
            ->method('get')
            ->with('Arb_Banners')
            ->willReturn($persistedData);
        $this->dataPersistorMock->expects($this->once())
            ->method('clear')
            ->with('Arb_Banners');

        $newbannerMock = $this->createMock(\Arb\Banners\Model\Banners::class);
        $newbannerMock->expects($this->atLeastOnce())->method('setData')->with($persistedData)->willReturnSelf();
        $newbannerMock->expects($this->atLeastOnce())->method('getId')->willReturn($bannerId);
        $newbannerMock->expects($this->any())->method('getData')->willReturn($bannerData);
        $this->collectionMock->expects($this->once())->method('getNewEmptyItem')->willReturn($newbannerMock);
  
        $this->assertEquals([$bannerId => []], $this->model->getData());
    }
}
