<?php
/**
 * StatusTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Model\Banners\Source;

use Arb\Banners\Model\Banners\Source\Status;

/**
 * Class StatusTest for testing Status class
 */
class StatusTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Summary
     */
    private $model;

    /**
     * main setup method
     */
    protected function setUp()
    {
        $this->model = new Status();
    }

    /**
     * testToOptionArray method
     */
    public function testToOptionArray()
    {
        $expectedResult = [
            ['value' => 1, 'label' => __('Enable')],
            ['value' => 0, 'label' => __('Disable')],
        ];
        $this->assertEquals($expectedResult, $this->model->toOptionArray());
    }
}
