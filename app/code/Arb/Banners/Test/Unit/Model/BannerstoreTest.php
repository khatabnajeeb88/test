<?php
/**
 * BannerstoreTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Model;

use Arb\Banners\Model\Bannerstore;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class BannerstoreTest for testing  Bannerstore class
 * @covers \Arb\Banners\Model\Bannerstore
 */
class BannerstoreTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject(Bannerstore::class);
    }

    /**
     * testGetAvailableStatuses method
     */

    public function testGetIdentities()
    {
        $status = $this->model->getIdentities();
        $this->assertEquals(["0" => "Arb_Banners_store_"], $status);
    }
}
