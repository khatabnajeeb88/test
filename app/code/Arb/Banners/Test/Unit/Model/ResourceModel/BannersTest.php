<?php
namespace Arb\Banners\Test\Unit\Model\ResourceModel;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Banners\Model\ResourceModel\Banners
 */
class BannersTest extends TestCase
{
    /**
     * Mock context
     *
     * @var \Magento\Framework\Model\ResourceModel\Db\Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Banners\Model\ResourceModel\Banners
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\Model\ResourceModel\Db\Context::class);
        $this->testObject = $this->objectManager->getObject(
            \Arb\Banners\Model\ResourceModel\Banners::class,
            [
                'context' => $this->context,
            ]
        );
    }

    /**
     * @return array
     */
    public function dataProviderForTestGetIdFieldName()
    {
        return [
            'Testcase 1' => [
                'prerequisites' => ['param' => 1],
                'expectedResult' => ['param' => 1]
            ]
        ];
    }

    /**
     * @dataProvider dataProviderForTestGetIdFieldName
     */
    public function testGetIdFieldName(array $prerequisites, array $expectedResult)
    {
        $this->assertEquals($expectedResult['param'], $prerequisites['param']);
    }
}
