<?php
/**
 * BannersTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Model;

use Arb\Banners\Model\Banners;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class BannersTest for testing  Banners class
 * @covers \Arb\Banners\Model\Banners
 */
class BannersTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject("Arb\Banners\Model\Banners");
    }

    /**
     * testGetAvailableStatuses method
     */
    public function testGetAvailableStatuses()
    {
        $status = $this->model->getAvailableStatuses();
        $this->assertEquals(['1' => 'Enable', '0' => 'Disable'], $status);
    }
}
