<?php
/**
 * BannerManagementTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Store\Model\StoreManagerInterface as storeManagerInterface;
use Arb\Banners\Model\BannerManagement;
use Arb\Banners\Model\ResourceModel\Banners\Collection;

/**
 * Class BannerManagementTest for testing BannerManagement class
 * @covers \Arb\Banners\Model\BannerManagement
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class BannerManagementTest extends TestCase
{
    /**
     * Mock bannersCollectionFactoryInstance
     *
     * @var \Arb\Banners\Model\ResourceModel\Banners\Collection|PHPUnit_Framework_MockObject_MockObject
     */
    private $bannersCollectionFactoryInstance;

    /**
     * Mock bannersCollectionFactory
     *
     * @var \Arb\Banners\Model\ResourceModel\Banners\CollectionFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $bannersCollectionFactory;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Banners\Model\BannerManagement
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->bannersCollectionFactory = $this->getMockBuilder(
            \Arb\Banners\Model\ResourceModel\Banners\CollectionFactory::class
        )
            ->disableOriginalConstructor()
            ->setMethods(
                ['create','getCollection','getData',"getSelect","join","group"]
            )
            ->getMock();

        $this->storeManagerFactory = $this->getMockBuilder(
            \Magento\Store\Model\StoreManagerFactory::class
        )
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $this->bannersCollectionFactoryInstance = $this->objectManager->getCollectionMock(
            \Arb\Banners\Model\ResourceModel\Banners\Collection::class,
            []
        );

        $this->bannersCollection = $this->getMockBuilder(
            \Arb\Banners\Model\ResourceModel\Banners\Collection::class
        )
            ->disableOriginalConstructor()
            ->setMethods(
                ['create','getCollection','getData',"getSelect","join","group"]
            )
            ->getMock();

        $this->storeManagerInterface = $this->createMock(storeManagerInterface::class);
        $this->storeManagerInterface->expects($this->any())->method('getStore')->willReturn($this->storeManagerFactory);
        $this->storeManagerFactory->expects($this->any())->method('getId')->willReturn(1);
        $this->bannersCollectionFactory
            ->expects($this->any())
            ->method('create')
            ->willReturn($this->bannersCollectionFactoryInstance);

        $this->bannersCollectionFactory
            ->expects($this->any())
            ->method('getCollection')
            ->willReturn($this->bannersCollection);
        $this->bannersCollectionFactoryInstance
            ->expects($this->any())
            ->method('getSelect')
            ->willReturn($this->bannersCollection);
        $this->bannersCollection->expects($this->any())->method('group')->willReturn($this->bannersCollection);
        
        $this->testObject = new BannerManagement(
            $this->storeManagerInterface,
            $this->bannersCollectionFactory
        );
    }
    
    public function testGetBanners()
    {
        $this->bannersCollectionFactoryInstance->method('getData')->willReturn([]);
        $this->assertEquals($this->bannersCollectionFactoryInstance->getData(), $this->testObject->getBanners());
    }

    /**
     * @return array
     */
    public function dataProviderForTestGetBanners()
    {
        return [
            'Testcase 1' => [
                'expectedResult' => ['banner_id' => 1]
            ]
        ];
    }
}
