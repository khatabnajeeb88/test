<?php
/**
 * This file consist of class BackButton which is used to reverse back in banner listing.
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\Banners\Block\Adminhtml\Banners;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Framework\UrlInterface;

/**
 * Class BackButton in banner
 */
class BackButton implements ButtonProviderInterface
{
    private $urlBuilder;

    /**
     * Button function
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->urlBuilder->getUrl('*/')),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
   //public function getBackUrl()
    //{
       // return $this->getUrl('*/*/');
    //}
}
