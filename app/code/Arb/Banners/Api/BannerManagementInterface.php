<?php
/**
 * This file consist of interface for banner details
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */

namespace Arb\Banners\Api;

/**
 * Inteface for Banner Management
 */
interface BannerManagementInterface
{

    /**
     * GET for banner api
     *
     * @param  string $param
     *
     * @return array
     * @return void
     */
    public function getBanners();
}
