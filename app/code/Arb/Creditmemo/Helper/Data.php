<?php
/**
 * Validate refund data before calling refund API
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */

namespace Arb\Creditmemo\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;

class Data extends AbstractHelper
{
    /**
     * void payment transaction action code
     */
    const VOID_PAYMENT_GATEWAY_ACTION = '3';

    /**
     * refund payment transaction action code
     */
    const REFUND_PAYMENT_GATEWAY_ACTION = '2';

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    protected $voidOrderStates = [
        Order::STATE_PENDING_PAYMENT,
        Order::STATE_PROCESSING
    ];

    /**
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * Helper class constructor
     *
     * @param Context $context
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Context $context,
        OrderRepositoryInterface $orderRepository
    ) {
        parent::__construct($context);
        $this->orderRepository = $orderRepository;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Refund_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Checks if order can be refunded or void.
     *
     * @param \Magento\Sales\Api\Data\OrderInterface|int $order
     * @return string|bool
     */
    public function getRefundTransactionType($order)
    {
        if ($order instanceof \Magento\Sales\Api\Data\OrderInterface) {
            $orderObject = $order;
        } elseif (is_numeric($order)) {
            try {
                $orderObject = $this->orderRepository->get($order);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        } else {
            return false;
        }

        $isCortexExported = (int)$orderObject->getData('is_cortex_exported');
        $orderState = $orderObject->getState();
        $this->logger->info('isCortexExported '.$isCortexExported);
        $this->logger->info('orderState '.$orderState);
        //checking if order amount can be reversed
        if (!$isCortexExported) {
            return self::VOID_PAYMENT_GATEWAY_ACTION;
        } elseif ($isCortexExported) {
            return self::REFUND_PAYMENT_GATEWAY_ACTION;
        }
    }

    /**
     * Returns view credit memo button can be displayed on Admin/Merchant portal
     *
     * @param  \Magento\Sales\Model\Order $order
     * @return boolean
     */
    public function isCreditMemoAllowed($order)
    {
        if ($order->getIsVirtual()) {
            return false;
        }
        
        $orderState = $order->getState();
        return (in_array($orderState, $this->voidOrderStates));
    }
}
