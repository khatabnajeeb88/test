<?php
namespace Arb\Creditmemo\Test\Unit\Model\Order\Creditmemo;

use Magento\Sales\Api\Data\CreditmemoItemInterface;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Exception\LocalizedException;

/**
 * Creditmemo item model.
 */
class ItemTest extends TestCase
{
    

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\App\Helper\Context::class);

        $this->orderItemFactoryMock = $this->getMockBuilder(\Magento\Sales\Model\Order\ItemFactory::class)
                            ->disableOriginalConstructor()
                              ->setMethods([
                                  'create',
                                  'load'                                  
                              ])->getMock();

        $this->testObject = $this->objectManager->getObject(
            \Arb\Creditmemo\Model\Order\Creditmemo\Item::class,
            [
                'orderItemFactory' => $this->orderItemFactoryMock
            ]
        );
    }


    public function testGetProductType()
    {
        $this->testObject->getProductType();
    }

    public function testSetProductType()
    {
        $this->testObject->setProductType('virtual');
    }

    public function testGetQty()
    {
        $orderItemId = 1;

        $orderItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderItemMock->expects($this->once())
            ->method('load')
            ->with($orderItemId)
            ->willReturnSelf();

        $this->orderItemFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($orderItemMock);
        
        $this->testObject->setData(CreditmemoItemInterface::ORDER_ITEM_ID, $orderItemId);

        $result = $this->testObject->getOrderItem();
        
        $this->assertInstanceOf(\Magento\Sales\Model\Order\Item::class, $result);
        
        $this->testObject->getQty();
    }

    public function testGetQtyVirtual()
    {
        $orderItemId = 1;

        $orderItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProductType','load'])
            ->getMock();

        $orderItemMock->expects($this->once())
            ->method('load')
            ->with($orderItemId)
            ->willReturnSelf();
        
        $orderItemMock->expects($this->any())
            ->method('getProductType')
            ->willReturn('virtual');

        $this->orderItemFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($orderItemMock);
        
        $this->testObject->setData(CreditmemoItemInterface::ORDER_ITEM_ID, $orderItemId);

        $result = $this->testObject->getOrderItem();
        
        $this->assertInstanceOf(\Magento\Sales\Model\Order\Item::class, $result);
        
        $this->testObject->getQty();

        $this->testObject->isLast();

    }


    public function testIsLastVirtual()
    {
        $orderItemId = 1;

        $orderItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProductType','load'])
            ->getMock();

        $orderItemMock->expects($this->once())
            ->method('load')
            ->with($orderItemId)
            ->willReturnSelf();
        
        $orderItemMock->expects($this->any())
            ->method('getProductType')
            ->willReturn('virtual');

        $this->orderItemFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($orderItemMock);
        
        $this->testObject->setData(CreditmemoItemInterface::ORDER_ITEM_ID, $orderItemId);
        
        $this->testObject->isLast();

    }

    public function testIsLast()
    {
        $orderItemId = 1;

        $orderItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProductType','load','isQtyAvailable','getQtyToRefund'])
            ->getMock();

        $orderItemMock->expects($this->once())
            ->method('load')
            ->with($orderItemId)
            ->willReturnSelf();
        
        $orderItemMock->expects($this->any())
            ->method('getQtyToRefund')
            ->willReturn(8);

        $orderItemMock->expects($this->any())
            ->method('isQtyAvailable')
            ->willReturnSelf();

        $this->orderItemFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($orderItemMock);
        
        $this->testObject->setData(CreditmemoItemInterface::ORDER_ITEM_ID, $orderItemId);
        
        $this->testObject->isLast();
    }


    public function testIsLastWithDecimalQty()
    {
        $orderItemId = 1;

        $orderItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProductType','load','isQtyAvailable','getQtyToRefund','getIsQtyDecimal'])
            ->getMock();

        $orderItemMock->expects($this->once())
            ->method('load')
            ->with($orderItemId)
            ->willReturnSelf();
        
        $orderItemMock->expects($this->any())
            ->method('getQtyToRefund')
            ->willReturn(8);

        $orderItemMock->expects($this->any())
            ->method('getIsQtyDecimal')
            ->willReturn(true);

        $orderItemMock->expects($this->any())
            ->method('isQtyAvailable')
            ->willReturnSelf();

        $this->orderItemFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($orderItemMock);
        
        $this->testObject->setData(CreditmemoItemInterface::ORDER_ITEM_ID, $orderItemId);
        
        $this->testObject->isLast();
    }


    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testIsLastWithDecimalQtyWithError()
    {
        $orderItemId = 1;

        $orderItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProductType','load','isQtyAvailable','getQtyToRefund','getIsQtyDecimal','getQty','isDummy'])
            ->getMock();

        $orderItemMock->expects($this->once())
            ->method('load')
            ->with($orderItemId)
            ->willReturnSelf();
        
        $orderItemMock->expects($this->any())
            ->method('getQtyToRefund')
            ->willReturn(-8);

        // $orderItemMock->expects($this->any())
        //     ->method('getQty')
        //     ->willReturn(-1);

        $orderItemMock->expects($this->any())
            ->method('getIsQtyDecimal')
            ->willReturn(true);

        $orderItemMock->expects($this->any())
            ->method('isDummy')
            ->willReturn(0);
        

        $this->orderItemFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($orderItemMock);
        
        // $creditMemoItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Creditmemo\Item::class)
        //     ->disableOriginalConstructor()
        //     ->setMethods(['isQtyAvailable'])
        //     ->getMock();
        
        // $creditMemoItemMock->expects($this->any())
        //     ->method('isQtyAvailable')
        //     ->willReturn(false);

        $this->testObject->setData(CreditmemoItemInterface::ORDER_ITEM_ID, $orderItemId);
        
        $this->testObject->isLast();
    }

}
