<?php
/**
 * Override admin order view button section
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
namespace Arb\Creditmemo\Test\Unit\Block\Adminhtml\Order;

use Magento\Framework\View\Element\Template;
use Magento\Sales\Block\Adminhtml\Order\View as ParentView;
use Magento\Sales\Model\ConfigInterface;
use Magento\Backend\Block\Widget\Button\Item;
use Magento\Backend\Block\Widget\Button\ItemFactory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;


//Parent core Magento class doen not have code coverage
/**
 * @codeCoverageIgnore
 */
class ViewTest extends TestCase
{

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Backend\Block\Widget\Context::class);
        $this->registry = $this->createMock(\Magento\Framework\Registry::class);
        $this->salesConfig = $this->createMock(ConfigInterface::class);

        $this->reorderHelper = $this->getMockBuilder(\Magento\Sales\Helper\Reorder::class)
                            ->disableOriginalConstructor()
                              ->setMethods([
                                  'getIsVirtual',
                                  'getData',
                                  'getState',
                                  'isAllowed'
                              ])->getMock();
        $this->arbCreditMemoHelper = $this->getMockBuilder(\Arb\Creditmemo\Helper\Data::class)
                              ->disableOriginalConstructor()
                                ->setMethods([
                                  'getIsVirtual',
                                  'getData',
                                  'getState',
                                  'getList',
                                    'get',
                                    'delete',
                                    'save'
                                ])->getMock();

        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['getParam']
        );
        $this->context->expects($this->any())->method('getRequest')->willReturn($this->requestMock);

        $this->urlBuilderMock = $this->getMockForAbstractClass(\Magento\Framework\UrlInterface::class);
        $this->context->expects($this->any())->method('getUrlBuilder')->willReturn($this->urlBuilderMock);

        // $this->buttonListMock = $this->getMockBuilder(\Magento\Backend\Block\Widget\Button\ButtonList::class)
        //     ->disableOriginalConstructor()
        //     ->setMethods([
        //         'add'
        //     ])->getMock();
        $this->buttonMock = $this->getMockBuilder(Item::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->itemFactoryMock = $this->getMockBuilder(ItemFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->addButtonMock = $this->getMockBuilder(\Magento\Sales\Block\Adminhtml\Order\Create\Items::class)
            ->disableOriginalConstructor()
            ->setMethods(['addButton'])
            ->getMock();

        $this->itemFactoryMock->expects($this->any())
            ->method('create')
            ->willReturn($this->buttonMock);

        $this->buttonListMock = $this->objectManager->getObject(
            \Magento\Backend\Block\Widget\Button\ButtonList::class,
            [ 'itemFactory' => $this->itemFactoryMock]
        );
        $this->authorizationMock = $this->createMock(\Magento\Framework\AuthorizationInterface::class);

        $this->context->expects($this->any())->method('getAuthorization')->willReturn($this->authorizationMock);
        $this->context->expects($this->any())->method('getButtonList')->willReturn($this->buttonListMock);
            
        $this->testObject = $this->objectManager->getObject(
            \Arb\Creditmemo\Block\Adminhtml\Order\View::class,
            [
                'context' => $this->context,
                'registry' => $this->registry,
                'salesConfig' => $this->salesConfig,
                'reorderHelper' => $this->reorderHelper
            ]
        );
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testSetterGetterWithError()
    {
        $this->requestMock->expects($this->any())
            ->method('getParam')
            ->willReturn(1);
        
        
        $this->urlBuilderMock->expects($this->any())
           ->method('getUrl')            
           ->willReturn('test');

        // $this->buttonListMock->add('test'. 'test');
        // $buttons = $this->buttonListMock->getItems()[0];
        // $this->assertArrayHasKey('myButton', $buttons);

        $this->testObject->_construct();
        $this->assertInstanceOf(\Arb\Creditmemo\Block\Adminhtml\Order\View::class, $this->testObject);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testSetterGetter()
    {
        $this->requestMock->expects($this->any())
            ->method('getParam')
            ->willReturn(1);
        
        
        $this->urlBuilderMock->expects($this->any())
           ->method('getUrl')            
           ->willReturn('test');

        
        $registry = $this->getMockBuilder(\Magento\Framework\Registry::class)
           ->disableOriginalConstructor()
             ->setMethods([
                 'registry'
             ])->getMock();

        $registry->expects($this->any())
            ->method('registry')
            ->with('sales_order')            
            ->willReturnSelf();
        
        
        $this->authorizationMock->expects($this->any())
            ->method('isAllowed')
            ->with('Magento_Sales::cancel')
            ->willReturn(true);

        $this->addButtonMock->expects($this->any())
            ->method('addButton')         
            ->willReturnSelf();

        $this->testObject->_construct();
        $this->assertInstanceOf(\Arb\Creditmemo\Block\Adminhtml\Order\View::class, $this->testObject);
    }
}
