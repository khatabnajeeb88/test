<?php
/**
 * Overriding webkul RMA process refund controller
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
namespace Arb\Creditmemo\Test\Unit\Controller\Rma;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

class RefundTest extends TestCase
{
    /**
     * Mock context
     *
     * @var \Magento\Framework\App\Helper\Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * Mock orderRepository
     *
     * @var \Magento\Sales\Api\OrderRepositoryInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderRepository;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Creditmemo\Helper\Data
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\App\Action\Context::class);
        $this->helperMock = $this->getMockBuilder(\Webkul\MpRmaSystem\Helper\Data::class)
            ->disableOriginalConstructor()
                ->setMethods([
                    'isSeller',
                    'getRmaProductDetails',
                    'getShippingAmountForOrder',
                    'updateMpOrder',
                    'sendUpdateRmaEmail',
                    'manageStock',
                    'updateRmaItemQtyStatus',
                    'createCreditMemo',
                    'getItemFinalPrice',
                    'getSize'

                ])->getMock();
        

        $this->detailResourceMock = $this->getMockBuilder(\Webkul\MpRmaSystem\Model\ResourceModel\Details::class)
            ->disableOriginalConstructor()
                ->setMethods([
                    'getRmaProductDetails',
                    'getSize'
                ])->getMock();
    
        $this->urlMock = $this->getMockBuilder(\Magento\Customer\Model\Url::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getLoginUrl'
            ])->getMock();

        $this->sessionMock = $this->getMockBuilder(\Magento\Customer\Model\Session::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'authenticate'
            ])->getMock();

        $this->detailsMock = $this->getMockBuilder(\Webkul\MpRmaSystem\Model\DetailsFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'isSeller',
                'logDataInLogger',
                'create',
                'load',
                'getOrderId',
                'addData',
                'setId',
                'save'
            ])->getMock();

        $this->orderCollectionMock = $this->getMockBuilder(\Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'create',
                'getSize',
                'getItems',
                'getData',
                'getIterator'
            ])->getMock();

        
        $this->orderColMock = $this->createMock(\Magento\Sales\Model\ResourceModel\Order\Item\Collection::class);

            
            
        $this->resourceConnectionMock = $this->getMockBuilder(\Magento\Framework\App\ResourceConnection::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getTableName'
            ])->getMock();
            
        $this->requestMock = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['getParam','getFullActionName','getRouteName','isDispatched']
        );

        $this->action = $this->getMockForAbstractClass(
            \Magento\Framework\App\Action\Action::class,
            [],
            '',
            false,
            true,
            true,
            ['dispatch']
        );

        $this->action->expects($this->any())->method('dispatch')->willReturnSelf();

        $this->context->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        
        $this->messageManagerMock = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);
        $this->context->expects($this->any())->method('getMessageManager')->willReturn($this->messageManagerMock);

        $this->eventManagerMock = $this->createMock(\Magento\Framework\Event\ManagerInterface::class);
        $this->context->expects($this->any())->method('getEventManager')->willReturn($this->eventManagerMock);

        $this->actionFlag = $this->createMock(\Magento\Framework\App\ActionFlag::class);

        $this->resultRedirectFactory = $this->getMockBuilder(\Magento\Backend\Model\View\Result\RedirectFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(
                ['create',
                "setPath"]
            )
            ->getMock();
        
        $this->resultRedirect = $this->getMockBuilder(\Magento\Backend\Model\View\Result\Redirect::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->context->expects($this->any())
            ->method('getResultRedirectFactory')
            ->willReturn($this->resultRedirectFactory);

        $this->helperMock
            ->method('getRmaProductDetails')
            ->with(2)
            ->willReturn($this->orderColMock);

        $this->orderColMock->method('getSize')
            ->willReturn(5);


        $this->orderItemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
            ->disableOriginalConstructor()
            ->setMethods(
                ['getQty',
                "getQtyOrdered",
                'getRowTotal',
                'getDiscountAmount',
                'getTaxAmount'
                ]
            )
            ->getMock();

        $this->orderColMock->expects($this->any())
            ->method('getIterator')
            ->willReturn(new \ArrayIterator([$this->orderItemMock]));

        $this->arbHelperMock = $this->getMockBuilder(\Arb\Creditmemo\Webkul\Helper::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                'getItemFinalPrice'
                ]
            )
            ->getMock();
            
        
        $this->arbHelperMock->method('getItemFinalPrice')
            ->with($this->orderItemMock)
            ->willReturn(5);

        $this->testObject = $this->objectManager->getObject(
            \Arb\Creditmemo\Controller\Rma\Refund::class,
            [
                'context' => $this->context,
                'url' => $this->urlMock,
                'session' => $this->sessionMock,
                'mpRmaHelper' => $this->helperMock,
                'details' => $this->detailsMock,
                '_actionFlag' => $this->actionFlag,
                'action' => $this->action
            ]
        );
    }


    public function testExecuteData()
    {
        $shippingAmount = [
            'shipping_amount' => 0,
            'payment_type' => 2,
            'rma_id' => 2,
            'partial_amount' => 2
        ];

        $this->requestMock->expects($this->any())
            ->method('getParams')
            ->willReturn($shippingAmount);
        
        $this->detailsMock
            ->method('create')
            ->willReturnSelf();

        $this->detailsMock
            ->method('load')
            ->willReturnSelf();

        $this->detailsMock
            ->method('addData')
            ->willReturnSelf();
        
        $this->detailsMock
            ->method('setId')
            ->willReturnSelf();
        
        $this->resultRedirectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultRedirect);

        $this->resultRedirectFactory->expects($this->any())
            ->method('setPath')
            ->willReturn("/seller/rma");

        $this->testObject->execute();

    }

    public function testExecuteDataWithError()
    {
        $shippingAmount = [
            'shipping_amount' => 0,
            'payment_type' => 2,
            'rma_id' => 2,
            'partial_amount' => 2
        ];

        $this->requestMock->expects($this->any())
            ->method('getParams')
            ->willReturn($shippingAmount);
        
        $this->detailsMock
            ->method('create')
            ->willReturnSelf();

        $this->detailsMock
            ->method('load')
            ->willReturnSelf();

        $error = ['error' => '1', 'msg' => 'test'];
        $this->helperMock
            ->method('createCreditMemo')
            ->willReturn($error);
        
        $this->resultRedirectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultRedirect);

        $this->resultRedirectFactory->expects($this->any())
            ->method('setPath')
            ->willReturn("/seller/rma");

        $this->testObject->execute();

    }

    public function testExecuteError()
    {
        $shippingAmount = [
            'shipping_amount' => 5,
            'payment_type' => 2,
            'rma_id' => 2
        ];

        $this->requestMock->expects($this->any())
            ->method('getParams')
            ->willReturn($shippingAmount);
        
        $this->detailsMock
            ->method('create')
            ->willReturnSelf();

        $this->detailsMock
            ->method('load')
            ->willReturnSelf();

        $this->helperMock
            ->method('getShippingAmountForOrder')
            ->willReturn(-1);

        $this->messageManagerMock
            ->expects($this->any())
            ->method('addErrorMessage')
            ->willReturn(__('Shipping amount can not be greater.'));

        $this->resultRedirectFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->resultRedirect);

        $this->resultRedirectFactory->expects($this->any())
            ->method('setPath')
            ->willReturn("/seller/rma");
        
        $this->testObject->execute();


    }


    public function testDispatch()
    {
        $this->urlMock->expects($this->any())
            ->method('getLoginUrl')
            ->willReturnSelf();

        $this->sessionMock->expects($this->any())
            ->method('authenticate')
            ->willReturn(false);

        $this->actionFlag = $this->createMock(\Magento\Framework\App\ActionFlag::class);
        
        $this->actionFlag->expects($this->any())
        ->method('set')
        ->with(
            '',
            \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH,
            true
        );

        $this->testObject->dispatch($this->requestMock);
    }


    public function testDispatchError()
    {
        $this->urlMock->expects($this->any())
            ->method('getLoginUrl')
            ->willReturnSelf();

        $this->sessionMock->expects($this->any())
            ->method('authenticate')
            ->willReturn(true);
        
        $this->actionFlag->expects($this->any())
        ->method('set')
        ->willReturnSelf();

        $this->testObject->dispatch($this->requestMock);
    }
}
