<?php
namespace Arb\Creditmemo\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Creditmemo\Helper\Data
 */
class DataTest extends TestCase
{
    /**
     * Mock context
     *
     * @var \Magento\Framework\App\Helper\Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * Mock orderRepository
     *
     * @var \Magento\Sales\Api\OrderRepositoryInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderRepository;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Creditmemo\Helper\Data
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(\Magento\Framework\App\Helper\Context::class);
        $this->orderRepository = $this->createMock(\Magento\Sales\Api\OrderRepositoryInterface::class);
        $this->orderModel = $this->getMockBuilder(\Magento\Sales\Model\Order::class)
                            ->disableOriginalConstructor()
                              ->setMethods([
                                  'getIsVirtual',
                                  'getData',
                                  'getState'
                              ])->getMock();
        $this->orderDataModel = $this->getMockBuilder(\Magento\Sales\Api\Data\OrderInterface::class)
                              ->disableOriginalConstructor()
                                ->setMethods([
                                  'getIsVirtual',
                                  'getData',
                                  'getState',
                                  'getList',
                                    'get',
                                    'delete',
                                    'save'
                                ])->getMockForAbstractClass();
        $this->testObject = $this->objectManager->getObject(
            \Arb\Creditmemo\Helper\Data::class,
            [
                'context' => $this->context,
                'orderRepository' => $this->orderRepository,
            ]
        );
    }

    public function testGetRefundTransactionTypeOrderObject()
    {
        $this->orderDataModel->method('getState')->willReturn('pending_payment');
        $this->orderDataModel->method('getData')->willReturn(false);
        $this->testObject->getRefundTransactionType($this->orderDataModel);
    }

    public function testGetRefundTransactionTypeOrderRefundObject()
    {
        $this->orderDataModel->method('getState')->willReturn('complete');
        $this->orderDataModel->method('getData')->willReturn(true);
        $this->testObject->getRefundTransactionType($this->orderDataModel);
    }

    public function testGetRefundTransactionTypeOrderId()
    {
        $this->orderRepository->method('get')->willReturn($this->orderDataModel);
        $this->orderDataModel->method('getState')->willReturn('complete');
        $this->orderDataModel->method('getData')->willReturn(true);
        $this->testObject->getRefundTransactionType(1);
    }

    public function testGetRefundTransactionTypeOrderNull()
    {
        $this->orderRepository->method('get')->willReturn($this->orderDataModel);
        $this->orderDataModel->method('getState')->willReturn('complete');
        $this->orderDataModel->method('getData')->willReturn(true);
        $this->testObject->getRefundTransactionType('');
    }

    public function testIsCreditMemoNotAllowed()
    {
        $this->orderModel->method('getIsVirtual')->willReturn(false);
        $this->testObject->isCreditMemoAllowed($this->orderModel);
    }

    public function testIsCreditMemoAllowedVirtualCheck()
    {
        $this->orderModel->method('getIsVirtual')->willReturn(true);
        $this->testObject->isCreditMemoAllowed($this->orderModel);
    }

    public function testIsCreditMemoAllowed()
    {
        $this->orderModel->method('getIsVirtual')->willReturn(false);
        $this->orderModel->method('getState')->willReturn('pending_payment');
        $this->orderModel->method('getData')->willReturn(false);
        $this->testObject->isCreditMemoAllowed($this->orderModel);
    }
}
