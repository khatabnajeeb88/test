<?php
namespace Arb\Creditmemo\Test\Unit\Plugin\Adminhtml\Order;

use Magento\Sales\Block\Adminhtml\Order\View as BaseView;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button\ButtonList;
use Magento\Backend\Block\Widget\Button\Item;
use Magento\Backend\Block\Widget\Button\ItemFactory;
use Magento\Sales\Model\Order;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\UrlInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Remove credit memo button.
 */
class ViewPluginTest extends TestCase
{	

    protected function setUp(): void
    {
        $this->objectManager = new ObjectManager($this);
        $this->itemFactoryMock = $this->getMockBuilder(ItemFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->buttonMock = $this->getMockBuilder(Item::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->itemFactoryMock->expects($this->any())
            ->method('create')
            ->willReturn($this->buttonMock);
        $this->buttonList = $this->objectManager->getObject(
            ButtonList::class,
            [ 'itemFactory' => $this->itemFactoryMock]
        );

        $this->order = $this->getMockBuilder(Order::class)
            ->disableOriginalConstructor()
            ->setMethods(['getIsVirtual'])
            ->getMock();

        $this->urlBuilderMock = $this->getMockForAbstractClass(
            UrlInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['getUrl']
        );
        $this->context = $this->objectManager->getObject(
            Context::class,
            [
                'urlBuilder' => $this->urlBuilderMock
            ]
        );
        $this->testObject = $this->objectManager->getObject(
            \Arb\Creditmemo\Plugin\Adminhtml\Order\ViewPlugin::class,
            []
        );

        $this->BaseviewMock = $this->getMockBuilder(Baseview::class)
            ->disableOriginalConstructor()
            ->setMethods(['getOrder','removeButton'])
            ->getMock();
    }

	public function testAfterAddButton() {

        $this->BaseviewMock->expects($this->once())->method('getOrder')->willReturn($this->order);
        $this->order->expects($this->once())->method('getIsVirtual')->willReturn(true);
        $this->BaseviewMock->expects($this->once())->method('removeButton')->willReturnSelf();
        $this->testObject->afterAddButton($this->BaseviewMock, [], 'order_creditmemo');
        
    }
}