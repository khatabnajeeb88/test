<?php

namespace Arb\Creditmemo\Test\Unit\Plugin\Api;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Store\Model\ScopeInterface;

/**
 * @covers \Arb\Creditmemo\Plugin\Api\CreditmemoRepositoryInterfacePlugin
 */
class CreditmemoRepositoryInterfacePluginTest extends TestCase
{
    /**
     * Mock creditMemoHelper
     *
     * @var \Arb\Creditmemo\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $creditMemoHelper;

    /**
     * Mock messageManager
     *
     * @var \Magento\Framework\Message\ManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $messageManager;

    /**
     * Mock productRepository
     *
     * @var \Magento\Catalog\Model\ProductRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepository;

    /**
     * Mock orderRepository
     *
     * @var \Magento\Sales\Api\OrderRepositoryInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderRepository;

    /**
     * Mock paymentRefund
     *
     * @var \Arb\ArbPayment\Model\PaymentRefund|PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentRefund;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Creditmemo\Plugin\Api\CreditmemoRepositoryInterfacePlugin
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->paymentLog = $this->createMock(\Arb\ArbPayment\Model\PaymentLog::class);
        $this->paymentRefund = $this->getMockBuilder(\Arb\ArbPayment\Model\PaymentRefund::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getRefundAPIData',
                'refundAPIRequest',
                'processRefundInquiry'
            ])
            ->getMock();
        $this->cortexRefund = $this->getMockBuilder(\Arb\Cortex\Model\CortexRefundFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'create',
                'setData',
                'save'
            ])
            ->getMock();
        $this->creditMemoHelper = $this->getMockBuilder(\Arb\Creditmemo\Helper\Data::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getRefundTransactionType',
            ])
            ->getMock();

        $this->messageManager = $this->createMock(\Magento\Framework\Message\ManagerInterface::class);
        $this->productRepository = $this->createMock(\Magento\Catalog\Model\ProductRepository::class);
        $this->productRepository = $this->getMockBuilder(\Magento\Catalog\Model\ProductRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'get',
                'getTypeId'
            ])
            ->getMock();
        $this->order = $this->getMockBuilder(\Magento\Sales\Api\OrderRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getIncrementId',
                'getList',
                'get',
                'delete',
                'save'
            ])
            ->getMock();
        $this->orderRepository = $this->createMock(\Magento\Sales\Api\OrderRepositoryInterface::class);

        $this->creditMemo = $this->getMockBuilder(\Magento\Sales\Api\Data\CreditmemoInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getItems',
                'getOrderId',
                'getGrandTotal'
            ])->getMockForAbstractClass();

        //$this->creditMemo = $this->createMock(\Magento\Sales\Api\Data\CreditmemoInterface::class);
        $this->creditMemoItem = $this->getMockBuilder(\Magento\Sales\Api\Data\CreditmemoItemInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getSku',
                'getQty',
                'getName'
            ])->getMockForAbstractClass();
        
            $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);

        $this->testObject = $this->objectManager->getObject(
            \Arb\Creditmemo\Plugin\Api\CreditmemoRepositoryInterfacePlugin::class,
            [
                'paymentLog' => $this->paymentLog,
                'scopeConfig' => $this->scopeConfig,
                'messageManager' => $this->messageManager,
                'productRepository' => $this->productRepository,
                'orderRepository' => $this->orderRepository,
                'creditMemoHelper' => $this->creditMemoHelper,
                'paymentRefund' => $this->paymentRefund,
                'cortexRefund' => $this->cortexRefund
            ]
        );
    }

    public function testBeforeSave()
    {
        $this->creditMemo->method('getItems')->willReturn([$this->creditMemoItem]);
        $this->creditMemo->method('getGrandTotal')->willReturn('42.00');
        $this->productRepository->method('get')->willReturnSelf();
        $this->productRepository->method('getTypeId')->willReturn('simple');
        $this->orderRepository->method('get')->willReturn($this->order);
        $this->order->method('getIncrementId')->willReturn('1000001');
        $this->creditMemoHelper->method('getRefundTransactionType')->willReturn('3');

        $paymentData[] =
            [
                'amt' => '42.00',
                'action' => '2',
                'password' => 'w3j$1$I3wM6WcM@',
                'id' => 'KUmW2iI41n3sY8q',
                'currencyCode' => '682',
                'trackId' => '855291596524096',
                'udf5' => 'TrackID',
                'transId' => '2000000323',
            ];

        $refundResponse = [
            'status' => true,
            'message' => new \Magento\Framework\Phrase('Refund Process Success'),
            'transData' => ['ref' => 'test','authCode'=>'00']
        ];

        $this->paymentRefund->method('getRefundAPIData')->with(
            ['amt' => '42.00', 'transId' => '1000001'],
            3
        )->willReturn($paymentData);
        
        $this->paymentRefund->method('refundAPIRequest')->with(
            $paymentData,
            \Arb\ArbPayment\Model\PaymentLog::VOID
        )->willReturn($refundResponse);
        
        $cortexMock = $this->createMock(\Arb\Cortex\Model\CortexRefund::class,['setData']);
        $this->cortexRefund->expects($this->once())->method('create')->willReturn($cortexMock);
        $cortexMock->expects($this->once())->method('setData')->willReturnSelf();
        
        $this->creditMemoInterface = $this->createMock(\Magento\Sales\Api\CreditmemoRepositoryInterface::class);
        $this->testObject->beforeSave($this->creditMemoInterface, $this->creditMemo);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testBeforeSaveError()
    {
        $this->creditMemo->method('getItems')->willReturn([$this->creditMemoItem]);
        $this->creditMemo->method('getGrandTotal')->willReturn('42.00');
        $this->productRepository->method('get')->willReturnSelf();
        $this->productRepository->method('getTypeId')->willReturn('simple');
        $this->orderRepository->method('get')->willReturn($this->order);
        $this->order->method('getIncrementId')->willReturn('1000001');
        $this->creditMemoHelper->method('getRefundTransactionType')->willReturn('3');

        $paymentData[] =
            [
                'amt' => '42.00',
                'action' => '2',
                'password' => 'w3j$1$I3wM6WcM@',
                'id' => 'KUmW2iI41n3sY8q',
                'currencyCode' => '682',
                'trackId' => '855291596524096',
                'udf5' => 'TrackID',
                'transId' => '2000000323',
            ];

        $refundResponse = [
            'status' => false,
            'message' => new \Magento\Framework\Phrase('Refund Process Failed')
        ];

        $this->paymentRefund->method('getRefundAPIData')->with(
            ['amt' => '42.00', 'transId' => '1000001'],
            3
        )->willReturn($paymentData);
        $this->paymentRefund->method('refundAPIRequest')->with(
            $paymentData,
            \Arb\ArbPayment\Model\PaymentLog::VOID
        )->willReturn($refundResponse);
        
        $this->creditMemoInterface = $this->createMock(\Magento\Sales\Api\CreditmemoRepositoryInterface::class);
        $this->testObject->beforeSave($this->creditMemoInterface, $this->creditMemo);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testNoActionCodeBeforeSave()
    {
        $this->creditMemo->method('getItems')->willReturn($this->creditMemoItem);
        $this->productRepository->method('get')->willReturnSelf();
        $this->productRepository->method('getTypeId')->willReturn('simple');
        $this->orderRepository->method('get')->willReturn($this->order);
        $this->order->method('getIncrementId')->willReturn('1000001');
        $this->creditMemoInterface = $this->createMock(\Magento\Sales\Api\CreditmemoRepositoryInterface::class);
        $this->testObject->beforeSave($this->creditMemoInterface, $this->creditMemo);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testBeforeSaveVirtualProduct()
    {
        $this->creditMemo->method('getItems')->willReturn([$this->creditMemoItem]);
        $this->creditMemoItem->method('getSku')->willReturn('test-sku');
        $this->creditMemoItem->method('getQty')->willReturn(2);
        $this->productRepository->method('get')->willReturnSelf();
        $this->productRepository->method('getTypeId')->willReturn('virtual');
        $this->orderRepository->method('get')->willReturn($this->order);
        $this->order->method('getIncrementId')->willReturn('1000001');
        $this->creditMemoInterface = $this->createMock(\Magento\Sales\Api\CreditmemoRepositoryInterface::class);
        $this->testObject->beforeSave($this->creditMemoInterface, $this->creditMemo);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testBeforeSaveProductNotExist()
    {
        $this->creditMemo->method('getItems')->willReturn([$this->creditMemoItem]);
        $this->creditMemoItem->method('getSku')->willReturn('test-sku');
        $mockError = new \Magento\Framework\Phrase('test-sku SKU Does Not Exist');
        $mockClass = new \Magento\Framework\Exception\NoSuchEntityException($mockError);
        $this->productRepository->method('get')->willThrowException($mockClass);
        $this->creditMemoInterface = $this->createMock(\Magento\Sales\Api\CreditmemoRepositoryInterface::class);
        $this->testObject->beforeSave($this->creditMemoInterface, $this->creditMemo);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testBeforeSaveOrderNotExist()
    {
        $this->creditMemo->method('getItems')->willReturn([$this->creditMemoItem]);
        $this->creditMemoItem->method('getSku')->willReturn('test-sku');
        $this->creditMemoItem->method('getQty')->willReturn(2);
        $this->productRepository->method('get')->willReturnSelf();
        $this->productRepository->method('getTypeId')->willReturn('simple');
        $mockError = new \Magento\Framework\Phrase('Invalid Order ID');
        $mockClass = new \Magento\Framework\Exception\NoSuchEntityException($mockError);
        $this->orderRepository->method('get')->willThrowException($mockClass);
        $this->creditMemoInterface = $this->createMock(\Magento\Sales\Api\CreditmemoRepositoryInterface::class);
        $this->testObject->beforeSave($this->creditMemoInterface, $this->creditMemo);
    }
}
