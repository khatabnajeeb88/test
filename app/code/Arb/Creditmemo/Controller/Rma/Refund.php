<?php
/**
 * Overriding webkul RMA process refund controller
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
namespace Arb\Creditmemo\Controller\Rma;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Webkul\MpRmaSystem\Helper\Data;
use Webkul\MpRmaSystem\Controller\Rma\Refund as ParentRefund;


class Refund extends ParentRefund
{
    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $url;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * @var \Webkul\MpRmaSystem\Helper\Data
     */
    protected $mpRmaHelper;

    /**
     * @var \Webkul\MpRmaSystem\Model\DetailsFactory
     */
    protected $details;

    /**
     * @param Context $context
     * @param \Magento\Customer\Model\Url $url
     * @param \Magento\Customer\Model\Session $session
     * @param \Webkul\MpRmaSystem\Helper\Data $mpRmaHelper
     * @param \Webkul\MpRmaSystem\Model\DetailsFactory $details
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\Session $session,
        \Webkul\MpRmaSystem\Helper\Data $mpRmaHelper,
        \Webkul\MpRmaSystem\Model\DetailsFactory $details
    ) {
        parent::__construct(
            $context,
            $url,
            $session,
            $mpRmaHelper,
            $details
        );
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     *
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->url->getLoginUrl();
        if (!$this->session->authenticate($loginUrl)) {
            $this->_actionFlag->set('', static::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $helper = $this->mpRmaHelper;
        $data = $this->getRequest()->getParams();
        $rmaId = $data['rma_id'];
        $shippingAmount = $data['shipping_amount'];
        $negative = 0;
        $totalPrice = 0;
        $partial_amount = 0;
        $productDetails = $helper->getRmaProductDetails($rmaId);
        $orderId = $this->details->create()->load($rmaId)->getOrderId();
        $shippingTotals = $helper->getShippingAmountForOrder($orderId);
        
        if ($shippingAmount > $shippingTotals) {
            $this->messageManager->addErrorMessage(__("Shipping amount can not be greater than %1", $shippingTotals));
            return $this->resultRedirectFactory
                    ->create()
                    ->setPath(
                        '*/seller/rma',
                        ['id' => $rmaId, 'back' => null, '_current' => true]
                    );
        }

        if ($productDetails->getSize()) {
            foreach ($productDetails as $item) {
                $totalPrice += $helper->getItemFinalPrice($item);
            }
        }

        if ($data['payment_type'] == 2) {
            $partial_amount = str_replace(',', '', $data['partial_amount']);
            $negative = $totalPrice - $partial_amount;
        }
        
        $data = ['rma_id' => $rmaId, 'negative' => $negative, 'shipping_amount' => $shippingAmount];
        $result = $helper->createCreditMemo($data);
        if ($result['error']) {
            $this->messageManager->addError($result['msg']);
        } else {
            $rmaData = [
                        'status' => Data::RMA_STATUS_SOLVED,
                        'seller_status' => Data::SELLER_STATUS_SOLVED,
                        'final_status' => Data::FINAL_STATUS_SOLVED,
                        'refunded_amount' => $totalPrice - $negative + $shippingAmount,
                        'memo_id' => $result['memo_id'],
                    ];
            $this->messageManager->addSuccess($result['msg']);
            $rma = $this->details->create()->load($rmaId);
            $orderId = $rma->getOrderId();
            $rma->addData($rmaData)->setId($rmaId)->save();
            $helper->updateMpOrder($orderId, $result['memo_id']);
            $helper->sendUpdateRmaEmail($data);
            $helper->manageStock($rmaId, $productDetails);
            $helper->updateRmaItemQtyStatus($rmaId);
        }

        return $this->resultRedirectFactory
                    ->create()
                    ->setPath(
                        '*/seller/rma',
                        ['id' => $rmaId, 'back' => null, '_current' => true]
                    );
    }
}
