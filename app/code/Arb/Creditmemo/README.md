## Synopsis
An extension to implement ARB refund and void logic before creating credit memo

## Technical feature

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Dependency injection configuration [di.xml](etc/di.xml)

Credit Memo module depends on `Catalog`,`Sales`, `Payment`, `Checkout` and `Config` Magento modules, `ArbPayment` ARB custom module. 

