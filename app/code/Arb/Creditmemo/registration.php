<?php
/**
 * Credit Memo module registration
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
// @codeCoverageIgnoreStart
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_Creditmemo',
    __DIR__
);
// @codeCoverageIgnoreEnd
