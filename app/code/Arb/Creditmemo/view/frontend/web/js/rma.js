/**
 * webkul RMA js file
 *  
 * @category    Arb
 * @package     Arb_Creditmemo
 * @author Arb Magento Team
 */
define([
    "jquery",
    "Magento_Ui/js/modal/alert",
    "jquery/ui",
], function ($, alert) {
    'use strict';
    $.widget('mprma.rma', {
        options: {},
        _create: function () {
            var self = this;
            var totalPrice = self.options.totalPrice;
            var shippingPrice = self.options.shippingPrice;
            var totalPriceWithCurrency = self.options.totalPriceWithCurrency;
            var totalShippingWithCurrency = self.options.totalShippingWithCurrency;
            var totalRefundableAmountWithCurrency = self.options.totalRefundableAmount;
            var errorMsg = self.options.errorMsg;
            var shippingErrorMsg = self.options.shippingErrorMsg;
            var warningLable = self.options.warningLable;
            $(document).ready(function () {
                $(".wk-refund-amount").html(totalRefundableAmountWithCurrency);
                $(".wk-refundable-amount").html(totalRefundableAmountWithCurrency);
                $(".wk-refund-block").removeClass("wk-display-none");
                $("#payment_type").change(function (e) {
                    var val = $(this).val();
                    if (val == 1) {
                        $(".wk-partial-amount").hide();
                        $("#partial_amount").removeClass("required-entry");
                    } else {
                        $(".wk-partial-amount").show();
                        $("#partial_amount").addClass("required-entry");
                    }
                });
                $(".wk-refund").click(function (e) {
                    if ($('#wk_rma_refund_form').valid()) {
                        var price = $("#partial_amount").val();
			            var actualPrice = parseFloat(totalPrice) - parseFloat(shippingPrice);
                        if (parseFloat(parseFloat(price).toFixed(2)) >parseFloat(actualPrice.toFixed(2))){
                            alert({
                                title: warningLable,
                                content: "<div class='wk-mprma-warning-content'>"+errorMsg+"</div>",
                                actions: {
                                    always: function (){}
                                }
                            });
                            return false;
                        }

                        var shippingAmount = $("#shipping_amount").val();
                        if(parseFloat(parseFloat(shippingAmount).toFixed(2)) > parseFloat(parseFloat(shippingPrice).toFixed(2))){
                            alert({
                                title: warningLable,

                                content: "<div class='wk-mprma-warning-content'>"+shippingErrorMsg+"</div>",
                                actions: {
                                    always: function (){}
                                }
                            });
                            return false;
                        }
                    }
                });
            });
        }
    });
    return $.mprma.rma;
});
