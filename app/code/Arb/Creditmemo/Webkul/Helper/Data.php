<?php
/**
 * Webkul RMA helper extended file
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
namespace Arb\Creditmemo\Webkul\Helper;

use Arb\Customer\Plugin\Magento\Customer\Model\ResourceModel\CustomerRepository;
use Arb\CustomWebkul\Helper\Email;
use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Customer\Model\ResourceModel\CustomerRepository as MagentoCustomerRepository;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\UrlInterface;
use Magento\Rma\Helper\Eav;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Webkul\MpRmaSystem\Helper\Data as ParentRmaHelper;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Item\CollectionFactory as InvoiceItemCollection;
use Magento\Sales\Model\ResourceModel\Order\Shipment\Item\CollectionFactory as ShipmentItemCollection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollection;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory as OrdersCollection;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SalesListCollection;
use Webkul\MpRmaSystem\Model\ResourceModel\Details\CollectionFactory as DetailsCollection;
use Webkul\MpRmaSystem\Model\ResourceModel\Reasons\CollectionFactory as ReasonsCollection;
use Webkul\MpRmaSystem\Model\ResourceModel\Conversation\CollectionFactory as ConversationCollection;
use Magento\Sales\Block\Order\Item\Renderer\DefaultRendererFactory;
use Magento\CatalogInventory\Api\StockManagementInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Webkul\Marketplace\Model\OrdersFactory;
use Arb\Sla\Helper\SlaEventManagement;

//Ignoring code coverage for webkul file
/**
 * @codeCoverageIgnore
 */
class Data extends ParentRmaHelper
{
    const CUSTOM_EMAIL_TEMPLATE_CUSTOMER_UPDATE_RMA = 'custom_update_rma_customer';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\Session\SessionManager
     */
    protected $session;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * @var SellerCollection
     */
    protected $sellerCollection;

    /**
     * @var ProductCollection
     */
    protected $productCollection;

    /**
     * @var OrdersCollection
     */
    protected $ordersCollection;

    /**
     * @var OrderCollection
     */
    protected $orderCollectionFactory;

    /**
     * @var DetailsCollection
     */
    protected $detailsCollection;

    /**
     * @var ReasonsCollection
     */
    protected $reasonsCollection;

    /**
     * @var ConversationCollection
     */
    protected $conversationCollection;

    /**
     * @var \Webkul\MpRmaSystem\Model\DetailsFactory
     */
    protected $rma;

    /**
     * @var \Webkul\MpRmaSystem\Model\ReasonsFactory
     */
    protected $reason;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader
     */
    protected $memoLoader;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\CreditmemoSender
     */
    protected $memoSender;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $currency;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Sales\Api\CreditmemoManagementInterface
     */
    protected $creditmemoManagement;

    /**
     * @var \Webkul\Marketplace\Helper\Orders
     */
    protected $mpOrdersHelper;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Webkul\Marketplace\Model\Orders
     */
    protected $mpOrder;

    /**
     * @var CreditmemoRepositoryInterface
     */
    protected $creditMemoRepository;

    /**
     * @var SearchCriteriaInterface
     */
    protected $searchCriteria;
    
    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var OrdersFactory
     */
    private $ordersFactory;

    /**
     * @var PriceHelper
     */
    private $priceHelper;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;
    /**
     * @var Eav
     */
    private $eav;

    /**
     * @var MagentoCustomerRepository
     */
    private $customerRepository;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Session\SessionManager $session
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param SellerCollection $sellerCollection
     * @param ProductCollection $productCollection
     * @param InvoiceItemCollection $invoiceItemCollection
     * @param ShipmentItemCollection $shipmentItemCollection
     * @param OrdersCollection $ordersCollection
     * @param SalesListCollection $salesListCollection
     * @param OrderCollection $orderCollectionFactory
     * @param DetailsCollection $detailsCollectionFactory
     * @param ReasonsCollection $reasonsCollectionFactory
     * @param ConversationCollection $conversationCollectionFactory
     * @param \Webkul\MpRmaSystem\Model\DetailsFactory $rma
     * @param \Webkul\MpRmaSystem\Model\ReasonsFactory $reason
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param OrderFactory $orderFactory
     * @param \Webkul\MpRmaSystem\Model\ItemsFactory $items
     * @param \Magento\Framework\Registry $registry
     * @param \Webkul\MpRmaSystem\Model\ResourceModel\Details $detailsResource
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $creditmemoLoader
     * @param \Magento\Sales\Model\Order\Email\Sender\CreditmemoSender $creditmemoSender
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Locale\CurrencyInterface $currency
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Sales\Api\CreditmemoManagementInterface $creditmemoManagement
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param DefaultRendererFactory $defaultRenderer
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param StockManagementInterface $stockManagement
     * @param \Magento\Catalog\Model\Indexer\Product\Price\Processor $priceIndexer
     * @param \Webkul\Marketplace\Helper\Data $mpHelper ,
     * @param \Webkul\Marketplace\Helper\Orders $mpOrdersHelper
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Framework\Module\ResourceInterface $moduleResource
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Webkul\Marketplace\Model\OrdersFactory $mpOrders
     * @param CreditmemoRepositoryInterface $creditMemoRepository
     * @param SearchCriteriaBuilder $searchCriteria
     * @param FilterBuilder $filterBuilder
     * @param FilterGroupBuilder $filterGroupBuilder
     * @param OrdersFactory $ordersFactory
     * @param ManagerInterface $messageManager
     * @param PriceHelper $priceHelper
     * @param Email $email
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Session\SessionManager $session,
        \Magento\Framework\Filesystem $fileSystem,
        SellerCollection $sellerCollection,
        ProductCollection $productCollection,
        InvoiceItemCollection $invoiceItemCollection,
        ShipmentItemCollection $shipmentItemCollection,
        OrdersCollection $ordersCollection,
        SalesListCollection $salesListCollection,
        OrderCollection $orderCollectionFactory,
        DetailsCollection $detailsCollectionFactory,
        ReasonsCollection $reasonsCollectionFactory,
        ConversationCollection $conversationCollectionFactory,
        \Webkul\MpRmaSystem\Model\DetailsFactory $rma,
        \Webkul\MpRmaSystem\Model\ReasonsFactory $reason,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        OrderFactory $orderFactory,
        \Webkul\MpRmaSystem\Model\ItemsFactory $items,
        \Magento\Framework\Registry $registry,
        \Webkul\MpRmaSystem\Model\ResourceModel\Details $detailsResource,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $creditmemoLoader,
        \Magento\Sales\Model\Order\Email\Sender\CreditmemoSender $creditmemoSender,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Locale\CurrencyInterface $currency,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Sales\Api\CreditmemoManagementInterface $creditmemoManagement,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        DefaultRendererFactory $defaultRenderer,
        \Magento\Framework\Escaper $escaper,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        StockManagementInterface $stockManagement,
        \Magento\Catalog\Model\Indexer\Product\Price\Processor $priceIndexer,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\Marketplace\Helper\Orders $mpOrdersHelper,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Framework\Module\ResourceInterface $moduleResource,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\Marketplace\Model\OrdersFactory $mpOrders,
        CreditmemoRepositoryInterface $creditMemoRepository,
        SearchCriteriaBuilder $searchCriteria,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        OrdersFactory $ordersFactory,
        PriceHelper $priceHelper,
        Email $email,
        SlaEventManagement $slaEventManagement,
        Eav $eav,
        MagentoCustomerRepository $customerRepository
    ) {
        parent::__construct(
            $context,
            $storeManager,
            $customerSession,
            $session,
            $fileSystem,
            $sellerCollection,
            $productCollection,
            $invoiceItemCollection,
            $shipmentItemCollection,
            $ordersCollection,
            $salesListCollection,
            $orderCollectionFactory,
            $detailsCollectionFactory,
            $reasonsCollectionFactory,
            $conversationCollectionFactory,
            $rma,
            $reason,
            $productFactory,
            $orderFactory,
            $items,
            $registry,
            $detailsResource,
            $transportBuilder,
            $inlineTranslation,
            $creditmemoLoader,
            $creditmemoSender,
            $resource,
            $currency,
            $customerFactory,
            $creditmemoManagement,
            $fileUploaderFactory,
            $defaultRenderer,
            $escaper,
            $stockRegistry,
            $stockManagement,
            $priceIndexer,
            $mpHelper,
            $mpOrdersHelper,
            $priceCurrency,
            $currencyFactory,
            $moduleResource,
            $jsonHelper,
            $mpOrders
        );
        $this->creditMemoRepository = $creditMemoRepository;
        $this->searchCriteria = $searchCriteria;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->ordersFactory = $ordersFactory;
        $this->priceHelper = $priceHelper;
        $this->email = $email;
        $this->slaEventManagement = $slaEventManagement;
        $this->eav = $eav;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Get Customer's Orders with specified statuses
     *
     * @param int|string $customerId
     *
     * @return Collection
     * @codeCoverageIgnore
     */
    public function getOrdersOfCustomer($customerId = '')
    {
        $days = $this->getDefaultDays();
        $from = date('Y-m-d', strtotime("-" . $days . " days"));
        $allowedStatus = ['pending', 'processing', 'complete', 'shipped'];
        if ($customerId == '') {
            $customerId = $this->getCustomerId();
        }

        $orders = $this->orderCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('status', ['in' => $allowedStatus])
            ->addFieldToFilter('created_at', ['from' => $from])
            ->setOrder('created_at', 'desc');

        return $orders;
    }

    /**
     * Get Guest's Orders
     *
     * @param int $customerId Optional
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function getOrdersOfGuest($email = '')
    {
        if (strlen($email) <= 0) {
            $email = $this->getGuestEmailId();
        }

        $days = $this->getDefaultDays();
        $from = date('Y-m-d', strtotime("-".$days." days"));
        $allowedStatus = ['pending', 'processing', 'complete','shipped'];
        $orders = $this->orderCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_email', $email)
            ->addFieldToFilter('status', ['in' => $allowedStatus])
            ->addFieldToFilter('created_at', ['from' => $from])
            ->setOrder('created_at', 'desc');

        return $orders;
    }

    /**
     * Create Creditmemo
     *
     * @param array $data
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function createCreditMemo($data)
    {
        $error = 0;
        $result = ['msg' => '', 'error' => ''];
        $rmaId = $data['rma_id'];
        $negative = $data['negative'];
        $shipping = $data['shipping_amount'];
        $rma = $this->getRmaDetails($rmaId);
        $orderId = $rma->getOrderId();
        $productDetails = $this->getRmaProductDetails($rmaId);
        $items = [];
        foreach ($productDetails as $product) {
            $items[$product->getItemId()] = ['qty' => $product->getQty()];
        }

        $memo = [
            'items' => $items,
            'do_offline' => 1,
            'comment_text' => "",
            'shipping_amount' => $shipping,
            'adjustment_positive' => 0,
            'adjustment_negative' => $negative
        ];

        try {
            $this->memoLoader->setOrderId($orderId);
            $this->memoLoader->setCreditmemoId("");
            $this->memoLoader->setCreditmemo($memo);
            $this->memoLoader->setInvoiceId("");
            $memo = $this->memoLoader->load();
            if ($memo) {
                if (!$memo->isValidGrandTotal()) {
                    $result['msg'] = __('Total must be positive.');
                    $result['error'] = 1;
                    return $result;
                }

                if (!empty($memo['comment_text'])) {
                    $memo->addComment(
                        $memo['comment_text'],
                        isset($memo['comment_customer_notify']),
                        isset($memo['is_visible_on_front'])
                    );

                    $memo->setCustomerNote($memo['comment_text']);
                    $memo->setCustomerNoteNotify(isset($memo['comment_customer_notify']));
                }

                if (isset($memo['do_offline'])) {
                    //do not allow online refund for Refund to Store Credit
                    if (!$memo['do_offline'] && !empty($memo['refund_customerbalance_return_enable'])) {
                        $result['msg'] = __('Cannot create online refund.');
                        $result['error'] = 1;
                        return $result;
                    }
                }

                $memoManagement = $this->creditmemoManagement;
                $memoManagement->refund($memo, (bool)$memo['do_offline'], !empty($memo['send_email']));

                if (!empty($memo['send_email'])) {
                    $this->memoSender->send($memo);
                }

                $result['msg'] = __('Credit memo generated successfully.');
                $result['error'] = 0;
                $result['memo_id'] = $memo->getId();
                return $result;
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $result['msg'] = $e->getMessage();
            $result['error'] = 1;
        } catch (\Exception $e) {
            $result['msg'] = __('Unable to save credit memo right now.');
            $result['error'] = 1;
        }

        return $result;
    }

    /**
     * Get Valid Price Of Item For RMA Refund
     *
     * @param Object $item
     * @return float
     * @codeCoverageIgnore
     */
    public function getItemFinalPrice($item)
    {
        $rmaQty = $item->getQty();
        $qty = $item->getQtyOrdered();
        $totalPrice = $item->getRowTotal();
        $discountAmount = $item->getDiscountAmount();
        $taxAmount = $item->getTaxAmount();
        $finalPrice = $totalPrice + $taxAmount - $discountAmount;
        $unitPrice = $finalPrice / $qty;
        $finalPrice = $unitPrice * $rmaQty;
        return $finalPrice;
    }

    /**
     * @param $orderId
     *
     * @return int
     */
    public function getShippingOrderTotals($orderId)
    {
        $sellerId = $this->getSellerId();
        $wkOrderCollection = $this->ordersCollection->create();
        $wkOrderCollection->addFieldToFilter('seller_id', $sellerId);
        $wkOrderCollection->addFieldToFilter('order_id', $orderId);
        $shippingTotal = 0;
        foreach ($wkOrderCollection as $wkOders) {
            $shippingTotal += $wkOders->getShippingCharges();
        }

        return $shippingTotal;
    }

    /**
     * Returns total shipping amount for RMA order
     *
     * @return float
     */
    public function getShippingAmountForOrder($orderId)
    {
        $sellerId = $this->getSellerId();
        $wkOrderCollection = $this->ordersCollection->create();
        $wkOrderCollection->addFieldToFilter('seller_id', $sellerId);
        $wkOrderCollection->addFieldToFilter('order_id', $orderId);
        $shippingTotal = 0;
        $creditMemoIds = 0;
        foreach ($wkOrderCollection as $wkOders) {
            $shippingTotal += $wkOders->getShippingCharges();
            $creditMemoIds = $wkOders->getCreditmemoId();
        }

        $shippingAmountCreditMemo = 0;
        if ($creditMemoIds) {
            $creditMemoData = explode(',', $creditMemoIds);
            $shippingAmountCreditMemo = $this->getShippingAmountFromCreditMemo($creditMemoData);
        }
       
        return number_format(($shippingTotal-$shippingAmountCreditMemo), 2, '.', '');
    }

    /**
     * Returns shipping amount from credit memo of same order
     * This is used for single order multiple merchants
     *
     * @param  array $creditMemoIds
     * @return float
     */
    public function getShippingAmountFromCreditMemo($creditMemoIds)
    {
        $filter = $this->filterBuilder
            ->setField('entity_id')
            ->setConditionType('in')
            ->setValue($creditMemoIds)
            ->create();

        $filterGroup = $this->filterGroupBuilder
            ->addFilter($filter)
            ->create();

        $searchCriteria = $this->searchCriteria
        ->setFilterGroups([$filterGroup])
        ->create();
        $creditMemoData = $this->creditMemoRepository->getList($searchCriteria);

        $shippingAmountCreditMemo = 0;
        foreach ($creditMemoData->getItems() as $creditMemo) {
            $shippingAmountCreditMemo += $creditMemo->getShippingAmount();
        }
        return (float)$shippingAmountCreditMemo;
    }

    /**
     * Get Is Separate Seller Allow
     *
     * @return boolean
     * @codeCoverageIgnore
     */
    public function getIsSeparatePanel()
    {
        return $this->mpHelper->getIsSeparatePanel();
    }

    /**
     * Send New RMA Email with custom template
     *
     * @param array $details
     *
     * @return void
     * @throws LocalizedException
     * @codeCoverageIgnore
     */
    public function sendNewRmaEmail($details = [])
    {
        $this->_eventManager->dispatch(
            'arb_new_rma_send_email',
            [
                'rma_id' => $details['rma']['rma_id'],
                'seller_id' => $details['rma']['seller_id']
            ]
        );

        $details['template'] = 'custom_new_rma';
        $orderStatus = $this->getOrderStatusTitle($details['rma']['order_status']);
        $resolutionType = $this->getResolutionTypeTitle($details['rma']['resolution_type']);
        $additionalInfo = $details['rma']['additional_info'];
        $customerName = $details['name'];

        // rma url and SLA time frame for RMA added to template vars
        $rmaUrl = $this->_urlBuilder->getBaseUrl() . 'mprmasystem/seller/rma/id/' . $details['rma']['rma_id'];
        $sla = $this->slaEventManagement->getSla(SlaDataInterface::SLA_TYPE_RETURN_REQUEST, $details['rma']['rma_id']);

        $order = $this->getOrder( $details['rma']['order_id']);

        $templateVars = [
            'name' => $customerName,
            'rma_id' => $details['rma']['rma_id'],
            'order_increment_id' => $order->getIncrementId(),
            'order_id' => $details['rma']['order_ref'],
            'order_status' => $orderStatus,
            'resolution_type' => $resolutionType,
            'additional_info' => $additionalInfo,
            'sla_date' => $sla ? $sla->getDeadlineTime() : '',
            'rma_url' => $rmaUrl
        ];

        $details['email'] = $details['rma']['customer_email'];
        if ($details['rma']['customer_id'] > 0) {
            $msg = __("New RMA is requested by customer.");
        } else {
            $msg = __("New RMA is requested by guest.");
        }

        //send to seller
        $sellerId = $details['rma']['seller_id'];
        if ($sellerId > 0) {
            $seller = $this->getCustomer($sellerId);
            $email = $seller->getEmail();
            $sellerName = $seller->getName();
            $templateVars['msg'] = $msg;
            $templateVars['name'] = $sellerName;
            $details['email'] = $email;
            $templateVars['email_content_alignment'] = 'left';
            $templateVars['email_content_direction'] = 'ltr';

            $store = $seller->getStore();
            if ($store->getCode() === 'ar_SA') {
                $templateVars['email_content_alignment'] = 'right';
                $templateVars['email_content_direction'] = 'rtl';
            }

            $templateVars['store_id'] = $store->getId();

            $details['template_vars'] = $templateVars;
            $this->sendRmaEmail($details);
        }

        //send to admin
        if ($this->isAllowedNotification()) {
            $adminEmail = $this->getAdminEmail();
            $templateVars['msg'] = $msg;
            $templateVars['name'] = static::ADMIN;
            $details['template_vars'] = $templateVars;
            $details['email'] = $adminEmail;
            $this->sendRmaEmail($details);
        }
    }

    /**
     * Send Update RMA Email with custom templates
     *
     * @param array $details
     *
     * @return void
     * @throws LocalizedException
     * @codeCoverageIgnore
     */
    public function sendUpdateRmaEmail($details = [])
    {
        $rma = $this->getRmaDetails($details['rma_id']);
        $rmaStatus = (int)$rma->getStatus();
        $rmaStatusTitle = $this->getRmaStatusTitle($rmaStatus, $rma->getFinalStatus());
        $sellerStatusTitle = $this->getSellerStatusTitle($rma->getSellerStatus());
        $seller = $this->getCustomer($rma->getSellerId());

        $webkulMerchant = $this->mpHelper->getSellerCollectionObj($rma->getSellerId());

        foreach ($webkulMerchant as $merchant) {
            $logopic = (string)$merchant->getLogoPic();
            $contact = (string)$merchant->getContactNumber();
            if (strlen($logopic) <= 0) {
                $logopic = 'noimage.png';
            }
            if (strlen($contact) <= 0) {
                $contact = '';
            }
        }
        $logo = $logopic;
        $contactNumber = $contact;

        $sellerName = $seller->getName();

        $order = $this->getOrder($rma->getOrderId());
        $mpOrders = $this->ordersCollection
            ->create()
            ->addFieldToFilter('seller_id', $rma->getSellerId())
            ->addFieldToFilter('order_id', $rma->getOrderId());
        
        $mpOrder = $mpOrders->getFirstItem();

        $sellerOrderTotals = $this->salesListCollection->create()
            ->addFieldToFilter('main_table.order_id', $rma->getOrderId())
            ->addFieldToFilter('main_table.seller_id', $rma->getSellerId())
            ->getSellerOrderTotals();

        if (isset($sellerOrderTotals[0])) {
            $shippingCost = $order->formatPrice($this->getMerchantShippingFee($sellerOrderTotals));
            $rmaGrandTotal = $order->formatPrice(
                $this->getRmaGrandTotals($details['rma_id']) + $this->getMerchantShippingFee($sellerOrderTotals)
            );
        }

        $RmaItemsDetailsHtml = $this->getRmaItemsDetails($details['rma_id'], $order);

        $addressData = $order->getShippingAddress()->getData();
        $addressHtml = $addressData['firstname'] . ' ' . $addressData['lastname'] . '<br />' .
            $addressData['street'] . '<br />' . $addressData['city'] . ', ' . $addressData['postcode'] . ', ' .
            $addressData['country_id'] . '<br />' . 'T: ' . $addressData['telephone'];

        $storeId = null;
        $customerId = $order->getCustomerId();
        if ($customerId) {
            $customer = $this->customerRepository->getById($customerId);
            $store = $this->storeManager->getStore($customer->getStoreId());
            $storeCode = $store->getCode();
            $storeId = $store->getId();
        } else {
            $storeCode = $order->getStore()->getCode();
            $storeId = $order->getStore()->getId();
        }

        $alignment = 'left';
        $direction = 'ltr';

        if ($storeCode === 'ar_SA') {
            $alignment = 'right';
            $direction = 'rtl';
        }

        // inserted custom vars into template
        $templateVars = [
            'name' => $addressData['firstname'],
            'rma_id' => $details['rma_id'],
            'order_increment_id' => $order->getIncrementId(),
            'merchant_name' => $sellerName,
            'merchant_logo' => $this->getBaseMediaUrl() . '/avatar/' . $logo,
            'merchant_phone' => $contactNumber,
            'rma_status' => $rmaStatusTitle,
            'seller_status' => $sellerStatusTitle,
            'order_number' => $order->getIncrementId(),
            'reject_reason' => '',
            'product_data' => $RmaItemsDetailsHtml,
            'shipping_fee' => $shippingCost ?? 0,
            'grand_total' => $rmaGrandTotal,
            'customer_address' => $addressHtml,
            'email_content_alignment' => $alignment,
            'email_content_direction' => $direction,
            'courier_name' => $mpOrder['carrier_name'],
            'courier_number' => $mpOrder['tracking_number'],
            'store_id' => $storeId
        ];

        if ($rmaStatus === static::RMA_STATUS_DECLINED) {
            $templateVars['reject_reason'] = __('The merchant rejection reason is: %1', $details['decline-rma-reason']);
            $msg = __(
                'We would like to inform you that merchant %1 has denied to return your order %2',
                $sellerName,
                $order->getIncrementId()
            );
        } else {
            $msg = __(
                'We would like to inform you that merchant %1 has agreed to return your order %2',
                $sellerName,
                $order->getIncrementId()
            );
        }

        //send to customer/guest - WILL SEND ON DECLINE/SOLVE
        $details['template'] = static::CUSTOM_EMAIL_TEMPLATE_CUSTOMER_UPDATE_RMA;

        $templateVars['msg'] = $msg;
        $details['template_vars'] = $templateVars;
        $details['email'] = $order->getCustomerEmail();
        if ($rmaStatus === static::RMA_STATUS_DECLINED || $rmaStatus === static::RMA_STATUS_SOLVED) {
            $this->_eventManager->dispatch(
                'arb_closed_rma_send_email',
                [
                    'rma' => $rma,
                    'seller_id' => $seller->getId()
                ]
            );
            $this->sendRmaEmail($details);
        } else {
            $this->_eventManager->dispatch(
                'arb_updated_rma_send_email',
                [
                    'rma' => $rma,
                    'seller_id' => $seller->getId()
                ]
            );
        }

        //send to seller - WILL NOT SEND
        $details['template'] = static::UPDATE_RMA;
        $msg = __("RMA status is updated.");
        $templateVars['msg'] = $msg;
        $email = $seller->getEmail();
        $templateVars['name'] = $sellerName;
        $details['email'] = $email;
        $details['template_vars'] = $templateVars;
        // removed sending of email for merchant as it was not part of requirement

        //send to admin - WILL SEND IF ENABLED IN CONFIG
        if ($this->isAllowedNotification()) {
            $details['email'] = $this->getAdminEmail();
            $details['template'] = static::UPDATE_RMA;
            $templateVars['name'] = static::ADMIN;
            $details['template_vars'] = $templateVars;
            $this->sendNotificationToAdmin($details);
        }
    }

    /**
     * @param $rmaId
     *
     * @return float|int
     */
    private function getRmaGrandTotals($rmaId)
    {
        $rmaProductDetails = $this->getRmaProductDetails($rmaId);
        $total = 0;
        foreach ($rmaProductDetails->getItems() as $rmaItem) {
            $discount = $rmaItem->getDiscountAmount() / $rmaItem->getQtyOrdered();
            $itemPrice = $rmaItem->getQty() * ($rmaItem->getPriceInclTax() - $discount);
            $total += $itemPrice;
        }

        return $total;
    }

    /**
     * Gather RMA Item Data and convert it to HTML to be inserted into Email Template
     *
     * @param string $rmaId
     * @param Order $order
     *
     * @return string
     * @throws NoSuchEntityException
     */
    private function getRmaItemsDetails(string $rmaId, Order $order)
    {
        $items = $this->getRmaProductDetails($rmaId);
        $itemsData = [];

        foreach ($items as $product) {
            $itemsData[] = [
                'name' => $product->getName(),
                'qty' => $product->getQty(),
                'price' => $order->formatPrice($product->getData("price")),
                'image' => $product->getProduct()->getImage()
            ];
        }

        $html = '';
        foreach ($itemsData as $item) {
            $html .=
                '<tr><td class="item">' . $item['qty'] . 'x ' . $item['name'] . '</td>' .
                '<td class="item">' . $item['price'] . '</td>' .
                '<td class="item"><img style="max-width:200px;" src="' .
                $this->getBaseMediaUrl() . '/catalog/product/' . $item['image'] . '"></td></tr>';
        }

        return $html;
    }

    /**
     * Get Shipping Cost for Merchant Consignment
     *
     * @param array $sellerOrderTotals
     *
     * @return float
     */
    private function getMerchantShippingFee(array $sellerOrderTotals)
    {
        $shippingAmount = $sellerOrderTotals[0]['shipping_charges'];
        $currencyRate = $sellerOrderTotals[0]['currency_rate'];

        return (float)$this->mpHelper->getCurrentCurrencyPrice($currencyRate, $shippingAmount);
    }

    /**
     * Get Grant Total Amount for Merchant Consignment
     *
     * @param array $sellerOrderTotals
     *
     * @return float
     */
    private function getMerchantConsignmentGrandTotal(array $sellerOrderTotals)
    {
        $subtotal = $sellerOrderTotals[0]['magepro_price'];
        $shippingAmount = $sellerOrderTotals[0]['shipping_charges'];
        $totalCouponAmount = $sellerOrderTotals[0]['coupon_amount'];
        $totalTax = $sellerOrderTotals[0]['total_tax'];
        $currencyRate = $sellerOrderTotals[0]['currency_rate'];
        $sum = $subtotal + $shippingAmount + $totalTax - $totalCouponAmount;

        return (float)$this->mpHelper->getCurrentCurrencyPrice($currencyRate, $sum);
    }

    /**
     * @return string
     *
     * @throws NoSuchEntityException
     */
    private function getBaseMediaUrl()
    {
        return rtrim($this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA), '/');
    }

    /**
     * Getting marketplace order status
     *
     * @param $orderId
     * @param $sellerId
     *
     * @return mixed|string
     */
    public function getMpOrderStatus($orderId, $sellerId)
    {
        /** @var \Webkul\Marketplace\Model\ResourceModel\Orders\Collection $sellerOrderCollection */
        $sellerOrderCollection = $this->ordersFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                $orderId
            )
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            );

        $mpOrderStatus = '';
        if (!empty($sellerOrderCollection->getItems())) {
            $mpOrder = $sellerOrderCollection->getFirstItem();
            $mpOrderStatus = $mpOrder->getData('order_status');
        }

        return $mpOrderStatus;
    }

    /**
     * @param string $rmaId
     *
     * @throws NoSuchEntityException
     */
    public function sendRmaConfirmationEmail(string $rmaId)
    {
        $rmaDetails = $this->getRmaDetails($rmaId);
        $sellerId = $rmaDetails['seller_id'];
        $seller = $this->getCustomer($sellerId);
        $sellerAddress = $seller->getDefaultShippingAddress();

        $address = 'Return address not provided';
        if ($sellerAddress) {
            $address = [
                implode(' ', $sellerAddress->getStreet()),
                $sellerAddress->getCity(),
                $sellerAddress->getPostCode(),
                $sellerAddress->getCountryId()
            ];

            $address = implode(', ', $address);
        }

        $orderId = $rmaDetails['order_id'];
        $orders = $this->orderCollectionFactory
            ->create()
            ->addFieldToFilter('entity_id', $orderId);

        /** @var Order $order */
        $order = $orders->getFirstItem();

        $rmaProductsDetails = $this->getRmaProductDetails($rmaId);
        $rmaItems = $rmaProductsDetails->getItems();

        $mpOrders = $this->ordersCollection
            ->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('order_id', $orderId);
        $sellers = $this->sellerCollection
            ->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            );

        $mpOrder = $mpOrders->getFirstItem();

        $shippingCharges = $mpOrder['shipping_charges'];
        $refundAmount = 0;
        foreach ($rmaItems as $rmaItem) {
            $productPrice = $rmaItem->getData('price');
            $qtyShipped = $rmaItem->getData('qty_shipped');
            $discount = $rmaItem->getData('discount_amount');

            $productDiscount = $discount / $qtyShipped;

            $refundAmount += $productPrice;
            $refundAmount -= $productDiscount;
        }

        $refundAmount += $shippingCharges;

        $mpSeller = $sellers->getFirstItem();
        $customerId = $rmaDetails['customer_id'];

        // for guest user store data is pulled from order
        $store = null;
        if ($customerId <= 0) {
            $store = $order->getStore();
        } else {
            $customer = $this->getCustomer($customerId);
            $store = $customer->getStore();
        }
        $storeCode = $store->getCode();
        $alignment = 'left';
        $direction = 'ltr';
        if ($storeCode === 'ar_SA') {
            $alignment = 'right';
            $direction = 'rtl';
        }

        $returnDetails = $this->getRmaItemsHtml($rmaId, $order, $alignment);

        $baseUrl = rtrim($this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA), '/');
        $merchantLogo = $baseUrl . '/avatar/' . $mpSeller['logo_pic'] ?? 'noimage.png';
        $delivery = $order->getShippingAddress();
        $deliveryAddress = [
            implode(' ', $delivery->getStreet()),
            $delivery->getCity(),
            $delivery->getPostCode(),
            $delivery->getCountryId()
        ];
        $deliveryAddress = implode(', ', $deliveryAddress);

        $templateVars = [
            'customer_name' => $rmaDetails['customer_name'],
            'merchant_name' => $mpSeller['shop_title'],
            'order_increment_id' => $order->getIncrementId(),
            'delivery_address' => $deliveryAddress,
            'return_number' => $rmaDetails['id'],
            'merchant_logo' => $merchantLogo,
            'merchant_service_number' => $mpSeller['contact_number'],
            'courier_name' => $mpOrder['carrier_name'],
            'courier_number' => $mpOrder['tracking_number'],
            'return_address' => $address,
            'return_details' => $returnDetails,
            'shipping_fees' => $this->priceHelper->currency($shippingCharges, true, false),
            'grand_total' => $this->priceHelper->currency($refundAmount, true, false),
            'email_content_alignment' => $alignment,
            'email_content_direction' => $direction,
            'store_id' => $store->getId(),
        ];

        $templateVars['confirmation_email'] = 1;
        $details['template_vars'] = $templateVars;
        $details['template'] = 'marketplace_email_order_returned_template';
        $details['email'] = $order->getCustomerEmail();

        $this->sendRmaEmail($details);
    }

    /**
     * Preparing table content for refund email
     *
     * @param string $rmaId
     * @param Order $order
     * @param string $alignment
     *
     * @return string
     * @throws NoSuchEntityException
     */
    private function getRmaItemsHtml(string $rmaId, Order $order, string $alignment)
    {
        $items = $this->getRmaProductDetails($rmaId);
        $itemsData = [];

        foreach ($items as $product) {
            $itemsData[] = [
                'name' => $product->getName(),
                'qty' => $product->getQty(),
                'price' => $order->formatPrice($product->getData("price")),
                'image' => $product->getProduct()->getImage()
            ];
        }
        $html = '';
        foreach ($itemsData as $item) {
            $html .=
                '<tr>' .
                '<td align=' . $alignment .'><img alt="product_image" style="max-width:200px;" src="' .
                $this->getBaseMediaUrl() . '/catalog/product/' . $item['image'] . '"></td>' .
            '<td align=' . $alignment .'>' . $item['name'] . '</td>' .
            '<td align=' . $alignment .' dir="ltr">' . $item['qty'] . ' x ' . $item['price'] . '</td>'.
                '</tr>';
        }

        return $html;
    }

    /**
     * Send Email
     *
     * @param array $details
     */
    public function sendRmaEmail(array $details)
    {
        if (empty($details['email'])) {
            $order = $this->orderFactory->create();
            $rmaOrder = $order->loadByIncrementId($details['template_vars']['order_increment_id']);
            $details['email'] = $rmaOrder->getCustomerEmail();
        }

        $name = $details['template_vars']['customer_name'] ?? $details['name'] ?? 'not provided';
        $emailTemplateVariables = $details['template_vars'];
        $emailTemplateVariables['template'] = $details['template'];
        $senderInfo = ['name' => static::ADMIN, 'email' => $this->getAdminEmail()];
        $receiverInfo = ['name' => $name, 'email' => $details['email']];

        $this->email->sendRmaNotificationMail($emailTemplateVariables, $senderInfo, $receiverInfo);
    }

    /**
     * @param int $conditionId
     *
     * @return mixed|string
     */
    public function getConditionById(int $conditionId)
    {
        $condition = '';
        $eavConditions = $this->eav->getAttributeOptionValues('condition');
        if (array_key_exists($conditionId, $eavConditions)) {
            $condition = $eavConditions[$conditionId];
        }

        return $condition;
    }

    /**
     * Get Current Seller Id
     *
     * @return int
     */
    public function getSellerId()
    {
        return $this->mpHelper->getCustomerId();
    }

    /*
     * final_status is a RMA state
     * Seller status is a Seller RMA status 
     * Pending  - (final_status = 0) and (seller_status = 0 or seller_status = 1)
     * Processing -(final_status = 1 or final_status = 0) and (seller_status = 2 or seller_status = 3)
     * Solved - (final_status = 3 or final_status = 4) and (seller_status = 4)
     * Declined - (final_status = 2) and (seller_status = 5)
     * Cancelled - (final_status = 4 and seller_status = 6)
    */
    public function applyStatusFilter($collection, $status)
    {
        if ($status != self::FILTER_STATUS_ALL) {
            $sql = "final_status > 0";
            if ($status == self::FILTER_STATUS_PENDING) {
                /*if RMA is in pending state*/
                $sql = "(final_status = 0) and (seller_status = 0 or seller_status = 1)";
            } elseif ($status == self::FILTER_STATUS_PROCESSING) {
                /*if RMA is in processing state*/
                $sql = "(final_status = 1 or final_status = 0) and (seller_status = 2 or seller_status = 3)";
            } elseif ($status == self::FILTER_STATUS_SOLVED) {
                /*if RMA is in solved state*/
                $sql = "(final_status = 3 or final_status = 4) and (seller_status = 4)";
            } elseif ($status == self::FILTER_STATUS_DECLINED) {
                /*if RMA is in declined state*/
                $sql = "(final_status = 2) and (seller_status = 5)";
            } elseif ($status == self::FILTER_STATUS_CANCELED) {
                /*if RMA is in canceled state*/
                $sql = "(final_status = 4 and seller_status = 6)";
            }
            $collection->getSelect()->where($sql);
        }
        return $collection;
    }
}
