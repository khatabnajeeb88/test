<?php
/**
 * set credit memo for non virtual products only
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
namespace Arb\Creditmemo\Model\Order\Creditmemo;

use Magento\Sales\Api\Data\CreditmemoItemInterface;

/**
 * Creditmemo item model.
 */
class Item extends \Magento\Sales\Model\Order\Creditmemo\Item implements CreditmemoItemInterface
{
    
    /**
     * Calculate qty for creditmemo item.
     *
     * @return int|float
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function processQty()
    {
        $orderItem = $this->getOrderItem();
        if ($orderItem->getProductType() == 'virtual') {
                return 0;
            }
        $qty = $this->getQty();
        if ($orderItem->getIsQtyDecimal()) {
            $qty = (double)$qty;
        } else {
            $qty = (int)$qty;
        }
        $qty = $qty > 0 ? $qty : 0;
        if ($this->isQtyAvailable($qty, $orderItem)) {
            return $qty;
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We found an invalid quantity to refund item "%1".', $this->getName())
            );
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getProductType()
    {
        return $this->getData('product_type');
    }
    
    /**
     * Undocumented function
     *
     * @param [type] $type
     * @return void
     */
    public function setProductType($type)
    {
        return $this->setData('product_type', $type);
    }

    /**
     * Returns qty
     *
     * @return float
     */
    public function getQty()
    {
        $orderItem = $this->getOrderItem();
        $this->setProductType($orderItem->getProductType());
        if ($orderItem->getProductType() == 'virtual') {
            return 0;
        }
        return $this->getData(CreditmemoItemInterface::QTY);
    }

    
    /**
     * Checking if the item is last
     *
     * @return bool
     */
    public function isLast()
    {
        $orderItem = $this->getOrderItem();
        $qty = $this->processQty();
        if ((string)(double)$qty == (string)(double)$orderItem->getQtyToRefund()) {
            return true;
        }
        return false;
    }

    /**
     * Checks if quantity available for refund
     *
     * @param int $qty
     * @param \Magento\Sales\Model\Order\Item $orderItem
     * @return bool
     */
    private function isQtyAvailable($qty, \Magento\Sales\Model\Order\Item $orderItem)
    {
        return $qty <= $orderItem->getQtyToRefund() || $orderItem->isDummy();
    }

}
