<?php
/**
 * Remove credit memo button.
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
namespace Arb\Creditmemo\Plugin\Adminhtml\Order;

use Magento\Sales\Block\Adminhtml\Order\View as BaseView;
/**
 * Remove credit memo button.
 */
class ViewPlugin
{	
	public function afterAddButton(
        BaseView $subject,
        $result,
        $buttonId
    ) {
        if ($buttonId === 'order_creditmemo') {
        	$isVirtual = $subject->getOrder()->getIsVirtual();
        	if($isVirtual){
        		$subject->removeButton('order_creditmemo');	
        	}            
        }
        return $result;
    }
}