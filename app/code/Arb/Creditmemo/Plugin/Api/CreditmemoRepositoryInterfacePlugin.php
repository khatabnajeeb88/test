<?php
/**
 * Validate refund data before calling refund API
 *
 * @category Arb
 * @package Arb_Creditmemo
 * @author Arb Magento Team
 *
 */
namespace Arb\Creditmemo\Plugin\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Framework\Exception\LocalizedException;
use Arb\ArbPayment\Model\PaymentLog;
use Arb\Creditmemo\Helper\Data as ArbCreditMemoHelper;
use Arb\Cortex\Model\CortexRefundFactory as CortexRefund;

/**
 * Before credit memo creation
 */
class CreditmemoRepositoryInterfacePlugin
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $redirectFactory;
    
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Arb\ArbPayment\Model\PaymentRefund
     */
    protected $paymentRefund;

    /**
     * @var ArbCreditMemoHelper
     */
    protected $creditMemoHelper;

    /**
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * @var \Webkul\MpAdvancedCommission\Helper\Data
     */
    protected $wkHelper;

    /**
     * @var \Arb\Cortex\Model\CortexRefund
     */
    protected $cortexRefund;

    /**
     * API type and action code mapping
     *
     * @var array
     */
    protected $apiTypeMapping = [
        ArbCreditMemoHelper::REFUND_PAYMENT_GATEWAY_ACTION => PaymentLog::REFUND,
        ArbCreditMemoHelper::VOID_PAYMENT_GATEWAY_ACTION => PaymentLog::VOID
    ];
   
   /**
    * Before Creditmemo create plugin constructor
    *
    * @param ArbCreditMemoHelper $creditMemoHelper
    * @param \Magento\Framework\Message\ManagerInterface $messageManager
    * @param \Magento\Catalog\Model\ProductRepository $productRepository
    * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    * @param \Arb\ArbPayment\Model\PaymentRefund $paymentRefund
    */
    public function __construct(
        ArbCreditMemoHelper $creditMemoHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Arb\ArbPayment\Model\PaymentRefund $paymentRefund,
        \Webkul\Marketplace\Helper\Data $wkHelper,
        CortexRefund $cortexRefund
    ) {
        $this->messageManager = $messageManager;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Refund_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->paymentRefund = $paymentRefund;
        $this->creditMemoHelper = $creditMemoHelper;
        $this->wkHelper = $wkHelper;
        $this->cortexRefund = $cortexRefund;
    }

    /**
     * Before credit memo create plugin
     *
     * @param \Magento\Sales\Api\CreditmemoRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $entity
     * @throws LocalizedException
     * @return void
     */
    public function beforeSave(
        \Magento\Sales\Api\CreditmemoRepositoryInterface $subject,
        \Magento\Sales\Api\Data\CreditmemoInterface $entity
    ) {
        $refundDetails = [];
        foreach ($entity->getItems() as $item) {
            $productType = $this->getProductType($item->getSku());
            
            if (!$productType) {
                throw new LocalizedException(__('%1 SKU Does Not Exist', $item->getSku()));
            }
            
            if (($productType === ProductType::TYPE_VIRTUAL) && ($item->getQty() > 0)) {
                throw new LocalizedException(__('Virtual product %1 can not be cancelled', $item->getName()));
            }

            if (($productType !== ProductType::TYPE_VIRTUAL)) {
                //This is to restrict parent entry of configurable product from return.
                if ($productType == 'configurable') {
                    continue;
                }
                $refundDetails[$item->getSku()] = $this->wkHelper->getSellerIdByProductId($item->getProductId());
            }
        }
        
        try {
             /** @var \Magento\Sales\Api\Data\OrderInterface $order */
             $order = $this->orderRepository->get($entity->getOrderId());
        } catch (NoSuchEntityException $e) {
            $this->logger->crit('Error for Primary Order Id '.$entity->getOrderId());
            throw new LocalizedException(__('Invalid Order ID'));
        }

        $orderId = $order->getIncrementId();
       
        $refundData = [];
        $refundAmount = number_format($entity->getGrandTotal(), 2, '.', '') ;
        $refundData['amt'] = $refundAmount;
        $refundData['transId'] = $orderId;
        $actionCode = $this->creditMemoHelper->getRefundTransactionType($order);
        
        $this->logger->info('Refund or action code');
        $this->logger->info($actionCode);

        if (!$actionCode) {
            throw new LocalizedException(__('Invalid Refund or Void action code'));
        }

        $refundAPIData = $this->paymentRefund->getRefundAPIData($refundData, $actionCode);
        
        $returnData = $this->paymentRefund->refundAPIRequest($refundAPIData, $this->apiTypeMapping[$actionCode]);
        $this->logger->info('Refund/void API response');
        $this->logger->info($returnData);

        //intiating inquiry for refund action not for void API call
        if ($actionCode === ArbCreditMemoHelper::REFUND_PAYMENT_GATEWAY_ACTION) {
            $refundInquiryData = $this->paymentRefund->processRefundInquiry($refundAPIData);
            if (!isset($refundInquiryData['message'])) {
                $returnData = $this->paymentRefund->refundAPIRequest($refundInquiryData, PaymentLog::REFUND_INQUIRY);
            } else {
                throw new LocalizedException($refundInquiryData['message']);
            }
        }
     
        if ($returnData['status']) {
            //set the cortex data
            $this->setCortexRefundData($returnData, $entity, 
            $refundDetails, $this->apiTypeMapping[$actionCode]);

            $this->messageManager->addSuccessMessage($returnData['message']);
            return;
        } else {
            throw new LocalizedException($returnData['message']);
        }
    }

    /**
     * save cortex refund data
     */
    protected function setCortexRefundData($refundData, $entity, $refundDetails, $returnType) {
        $refundDetails = array_unique($refundDetails);
        foreach ($refundDetails as $item) {
            $setRefundData = [
                'return_type' => $returnType,
                'return_ref_id' => $refundData['transData']['ref'],
                'return_auth_code' => $refundData['transData']['authCode'] ?? \null,
                'seller_id' => $item,
                'mage_order_id' => $entity->getOrderId()
            ];

            $this->cortexRefund->create()->setData($setRefundData)->save();
        }
    }

    /**
     * Get Product Type by SKU
     *
     * @param string $sku
     * @return string|bool
     */
    protected function getProductType($sku)
    {
        try {
            $product = $this->productRepository->get($sku);
            $productType = $product->getTypeId();
            return $productType;
        } catch (NoSuchEntityException $e) {
            $this->logger->crit('Product not available for cancel '.$sku);
            return false;
        }
    }
}
