<?php
/**
 * Handler to log Closed Order info in separate file to avoid clutter
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Logger;

use Monolog\Logger as MonologLogger;
use Magento\Framework\Logger\Handler\Base as HandlerBase;

class Handler extends HandlerBase
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = MonologLogger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/closed_orders.log';
}
