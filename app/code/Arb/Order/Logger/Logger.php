<?php
/**
 * Logger to track Closed Orders after Refund Window Expired
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Logger;

use Monolog\Logger as MonologLogger;

class Logger extends MonologLogger
{

}
