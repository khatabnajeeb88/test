<?php
/**
 * Repository for OrderHistory
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model;

use Arb\Order\Api\Data\OrderHistoryInterface;
use Arb\Order\Api\OrderHistoryRepositoryInterface;
use Arb\Order\Model\ResourceModel\OrderHistoryResource;
use Arb\Order\Model\ResourceModel\OrderHistory\CollectionFactory;
use Arb\Order\Model\ResourceModel\OrderHistory\Collection;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Exception;
use DateTime;

class OrderHistoryRepository implements OrderHistoryRepositoryInterface
{
    /**
     * @var OrderHistoryResource
     */
    private $orderHistoryResource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsInterfaceFactory;

    /**
     * @param OrderHistoryResource $orderHistoryResource
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchResultsInterfaceFactory $searchResultsInterfaceFactory
     */
    public function __construct(
        OrderHistoryResource $orderHistoryResource,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultsInterfaceFactory $searchResultsInterfaceFactory
    ) {
        $this->orderHistoryResource = $orderHistoryResource;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsInterfaceFactory = $searchResultsInterfaceFactory;
    }

    /**
     * @param OrderHistoryInterface $orderHistory
     *
     * @return OrderHistoryInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(OrderHistoryInterface $orderHistory)
    {
        $currentTime = new DateTime();
        $orderHistory->setCreatedAt($currentTime->format('Y-m-d H:i:s'));

        try {
            /** @noinspection PhpParamsInspection */
            $this->orderHistoryResource->save($orderHistory);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $orderHistory;
    }

    /**
     * @param OrderHistoryInterface $orderHistory
     *
     * @throws Exception
     */
    public function delete(OrderHistoryInterface $orderHistory)
    {
        /** @noinspection PhpParamsInspection */
        $this->orderHistoryResource->delete($orderHistory);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsInterfaceFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param int $orderId
     *
     * @return SearchResultsInterface
     */
    public function getByOrderId(int $orderId)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            OrderHistoryInterface::ORDER_HISTORY_ORDER_ID,
            $orderId,
            'eq'
        )->create();

        return $this->getList($searchCriteria);
    }
}
