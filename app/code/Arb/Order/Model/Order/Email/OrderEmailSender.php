<?php
/**
 * Model class file for sending order email for physical products
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model\Order\Email;

use Arb\CustomWebkul\Model\Webkul\ResourceModel\Saleslist\Collection;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\Marketplace\Model\Saleslist;
use Webkul\Marketplace\Model\SaleslistFactory;

/**
 * Order email transaction
 */
class OrderEmailSender
{
    /**
     * Here section and group refer to name of section and
     * group where you create this field in configuration
     */
    const XML_PATH_EMAIL_TEMPLATE_FIELD  = 'order/emails/order_confirmed_email_template';

    /**
     *  From sales name
     */
    const XML_PATH_TO_FROM_EMAIL_NAME = 'trans_email/ident_sales/name';

    /**
     *  From sales email
     */
    const XML_PATH_TO_FROM_EMAIL_EMAIL = 'trans_email/ident_sales/email';

    /**
     * Seller image store location
     */
    const SELLER_PROFILE_IMAGE_LOCATION = 'avatar/';

    /**
     * seller image placehoder
     */
    const SELLER_NO_IMAGE = 'noimage.png';

    /**
     * order confirmation email type
     */
    const ORDER_EMAIL_CONFIRMATION_TYPE = 'confirmation';

    /**
     * order complete email type
     */
    const ORDER_EMAIL_COMPLETE_TYPE = 'complete';
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     *
     * @var \Webkul\Marketplace\Model\ProductFactory
     */
    protected $wkProductFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $wkHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Helper\Image $imageHelper
     */
    protected $imageHelper;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $pricingHelper;
    
    /**
     * @var string
     */
    protected $templateId;

    /**
     * @var \Webkul\Marketplace\Model\OrdersFactory
     */
    protected $wkOrders;

    /**
     * @var SaleslistFactory
     */
    private $saleslistFactory;

    /**
     * Undocumented function
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Webkul\Marketplace\Helper\Data $wkHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Sales\Api\Data\OrderInterface $orderRepository
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Webkul\Marketplace\Helper\Data $wkHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Sales\Api\Data\OrderInterface $orderRepository,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Webkul\Marketplace\Model\OrdersFactory $wkOrders,
        SaleslistFactory $saleslistFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->wkHelper = $wkHelper;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->orderRepository = $orderRepository;
        $this->pricingHelper = $pricingHelper;
        $this->wkOrders = $wkOrders;
        $this->saleslistFactory = $saleslistFactory;
    }

    /**
     * Prepares order data for physical order email
     *
     * @param  int $orderId
     * @return void
     */
    public function preparePhysicalProductEmail($orderId)
    {
        $emailTemplateVariables = [];
        try {
            $order = $this->orderRepository->load($orderId);
        } catch (NoSuchEntityException $e) {
            return false;
        }

        $productInfo = $this->getShippmentPackage($order);
        $grandTotals = 0;

        foreach ($productInfo as $products) {
            reset($products);
            $firstProduct = key($products);
            $grandTotals += $products[$firstProduct]['bare_grand_total_amount'];
        }

        $emailTemplateVariables['increment_id'] = $order->getIncrementId();
        $emailTemplateVariables['shipping_fees'] =
        $this->pricingHelper->currency($order->getShippingAmount(), true, false);
        $emailTemplateVariables['total_amount'] = $this->pricingHelper->currency($grandTotals, true, false);
        $emailTemplateVariables['customer_name'] = $order->getCustomerFirstname().' '.$order->getCustomerLastname();

        $alignment = 'left';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $alignment = 'right';
        }

        $merchantData = $this->prepareMerchantForOrder($order);
        $uniqueMerchants = $this->getUniqueMerchants($merchantData);
        $emailTemplateVariables['merchant_info'] = $this->getMerchantInformationHTML($uniqueMerchants, $alignment);
        $emailTemplateVariables['shipments_count'] = count($uniqueMerchants);
        $emailTemplateVariables['product_info'] = $productInfo;
        $shippingAddress = $order->getShippingAddress();

        $address = [
            $shippingAddress->getFirstname().' '. $shippingAddress->getLastname(),
            implode(' ', $shippingAddress->getStreet()),
            $shippingAddress->getCity(),
            $shippingAddress->getPostCode(),
            $shippingAddress->getCountryId()
        ];

        $addressLine = implode(', ', $address);
        $emailTemplateVariables['formattedShippingAddress'] = $addressLine;
        $emailTemplateVariables['customer_mobile'] = $order->getShippingAddress()->getTelephone();
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = __($method->getTitle());
        $emailTemplateVariables['payment_method'] = $methodTitle;
        $emailTemplateVariables['customer_note'] = $order->getCustomerNote();
        $emailTemplateVariables['item_count'] = count($order->getAllVisibleItems());
        $emailTemplateVariables['store_id'] = $order->getStore()->getId();
        $emailTemplateVariables['email_content_alignment'] = 'left';
        $emailTemplateVariables['email_content_direction'] = 'ltr';

        if ($order->getStore()->getCode() === 'ar_SA') {
            $emailTemplateVariables['email_content_alignment'] = 'right';
            $emailTemplateVariables['email_content_direction'] = 'rtl';
        }

        $senderInfo = [
            'name' => $this->getConfig(self::XML_PATH_TO_FROM_EMAIL_NAME),
            'email' => $this->getConfig(self::XML_PATH_TO_FROM_EMAIL_EMAIL),
        ];

        $receiverInfo = [
            'name' => $order->getCustomerFirstname(),
            'email' => $order->getCustomerEmail(),
        ];

        $this->sendPhysicalOrderEmail($emailTemplateVariables, $senderInfo, $receiverInfo, self::XML_PATH_EMAIL_TEMPLATE_FIELD);
    }

    /**
     * @param $merchantData
     *
     * @return array
     */
    private function getUniqueMerchants($merchantData)
    {
        $uniqueMerchants = [];
        foreach ($merchantData as $merchant) {
            $id = $merchant['merchant_id'];
            if ($id !== null) {
                $uniqueMerchants[$id] = $merchant;
            }
        }

        return $uniqueMerchants;
    }

    /**
     * Returns order confirmed product data in array
     *
     * @param  \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function prepareMerchantForOrder($order)
    {
        $productData = [];
        foreach ($order->getItems() as $item) {
            if ($item->getProductType() === ProductType::TYPE_VIRTUAL) {
                continue;
            } else {
                $sellerId = $this->wkHelper->getSellerIdByProductId($item->getProductId());
               
                $sellerData = [
                    'merchant_name' => null,
                    'merchant_contact' => null,
                    'merchant_logo' => null
                ];
                $seller = $this->wkHelper->getSellerDataBySellerId($sellerId)->getData();
                if (!empty($seller[0])) {
                    $sellerInfo = $seller[0];
                    $sellerData['merchant_name'] = $sellerInfo['shop_title'];
                    $sellerData['merchant_contact'] = $sellerInfo['contact_number'];
                    $sellerLogo = !empty($sellerInfo['logo_pic'])?$sellerInfo['logo_pic']:self::SELLER_NO_IMAGE;
                    $sellerData['merchant_logo'] =
                    $this->wkHelper->getMediaUrl().'/'.self::SELLER_PROFILE_IMAGE_LOCATION.$sellerLogo;
                }

                $productData[$item->getItemId()]['merchant_id'] = $sellerId;
                $productData[$item->getItemId()]['merchant_name'] = $sellerData['merchant_name'];
                $productData[$item->getItemId()]['merchant_contact'] = $sellerData['merchant_contact'];
                $productData[$item->getItemId()]['merchant_logo'] = $sellerData['merchant_logo'];
            }
        }

        return $productData;
    }

    /**
     * Returns Merchant data in html format
     *
     * @param array $merchantData
     * @param $alignment
     *
     * @return string
     */
    private function getMerchantInformationHTML($merchantData, $alignment)
    {
        $html = '';
        foreach ($merchantData as $merchantData) {
            $html .=
                '<tr>' .
                '<td align="' . $alignment . '">' . $merchantData['merchant_name'] . '</td>' .
                '<td align="' . $alignment . '">
                    <img width="200" height="200" alt="product_image" src="' . $merchantData['merchant_logo'] . '"/>
                </td>' .
                '<td align="' . $alignment . '">' . $merchantData['merchant_contact'] . '</td>' .
                '</tr>';
        }
        return $html;
    }

    /**
     * Returns order data shipment wise
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    private function getShippmentPackage($order)
    {
        $sellerData = [];
        $orderId = $order->getId();

        $alignment = 'left';
        $direction = 'ltr';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $alignment = 'right';
            $direction = 'rtl';
        }

        foreach ($order->getAllVisibleItems() as $item) {
            if ($item->getProductType() === ProductType::TYPE_VIRTUAL) {
                continue;
            }
            $sellerId = $this->wkHelper->getSellerIdByProductId($item->getProductId());
            if (empty($sellerData[$sellerId])) {
                $sellerData[$sellerId] = [];
            }

            $product = $this->productRepository->get($item->getSku());
            $productImage = $this->imageHelper->init($product, 'product_thumbnail_image')->getUrl();

            $shippingFee = $this->getShippedOrderTotal($orderId, $sellerId);
            $grandTotalAmount = $this->getTotalOrderedAmount($sellerId, $orderId) + $shippingFee;

            $sellerData[$sellerId][] = [
                'product_id' => $item->getProductId(),
                'item_id' => $item->getId(),
                'price' => round($item->getPriceInclTax(), 2),
                'product_name' => $item->getName(),
                'qty' => (int)$item->getQtyOrdered(),
                'product_image' => $productImage,
                'shipping_fees' => $this->pricingHelper->currency($shippingFee, true, false),
                'bare_grand_total_amount' => $grandTotalAmount,
                'grand_total_amount' => $this->pricingHelper->currency($grandTotalAmount, true, false),
                'alignment' => $alignment,
                'direction' => $direction
            ];
        }

        return $sellerData;
    }

    /**
     * Return shipping
     *
     * @param int $orderId
     * @param int $sellerId
     */
    private function getShippedOrderTotal($orderId, $sellerId)
    {
        $shippingCharges = 0;
        $sellerOrder = $this->wkOrders->create()
            ->getCollection()
            ->addFieldToSelect('shipping_charges')
            ->addFieldToFilter('order_id', $orderId)
            ->addFieldToFilter('seller_id', $sellerId);
        foreach ($sellerOrder as $sellerData) {
            $shippingCharges += $sellerData->getShippingCharges();
        }

        return $shippingCharges;
    }

    /**
     * @param $sellerId
     * @param $orderId
     *
     * @return string
     */
    private function getTotalOrderedAmount($sellerId, $orderId)
    {
        $salesCollection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $orderId
            );

        $totalAmount = $this->getSaleCollectionData($salesCollection, 'total_amount');
        $totalTax = $this->getSaleCollectionData($salesCollection, 'total_tax');
        $appliedCouponAmount = $this->getSaleCollectionData($salesCollection, 'applied_coupon_amount');
        $shippingCharges = $this->getSaleCollectionData($salesCollection, 'shipping_charges');
        $totalAmount -= $appliedCouponAmount;
        $totalAmount += $totalTax;
        $totalAmount += $shippingCharges;

        $totalAmount = sprintf('%0.2f', $totalAmount);

        return $totalAmount;
    }

    /**
     * Getting specific sale data from collection
     *
     * @param Collection $collection
     * @param string $columnName
     *
     * @return float
     */
    private function getSaleCollectionData(Collection $collection, string $columnName)
    {
        $total = 0;

        $items = $collection->getItems();

        /** @var Saleslist $coll */
        foreach ($items as $coll) {
            $total += (float)$coll->getData($columnName);
        }

        return $total;
    }

    /**
     * Return store configuration value of your template field that which id you set for template
     *
     * @param string $path
     * @param int $storeId
     * @return mixed
     */
    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Return store
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * Return system configuration according to store
     *
     * @return mixed
     */
    public function getConfig($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }

    /**
     * [generateTemplate description]  with template file and tempaltes variables values
     * @param  Mixed $emailTemplateVariables
     * @param  Mixed $senderInfo
     * @param  Mixed $receiverInfo
     * @return void
     */
    private function generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $template =  $this->transportBuilder->setTemplateIdentifier($this->templateId)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $emailTemplateVariables['store_id'],
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom($senderInfo)
                ->addTo($receiverInfo['email'], $receiverInfo['name']);
        return $this;
    }

    /**
     * Sends only physical product in email
     *
     * @param Mixed $emailTemplateVariables
     * @param Mixed $senderInfo
     * @param Mixed $receiverInfo
     * @param $emailTemplate
     *
     * @return bool
     */
    private function sendPhysicalOrderEmail($emailTemplateVariables, $senderInfo, $receiverInfo, $emailTemplate)
    {
        $this->templateId = $this->getConfig($emailTemplate);
        $this->inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            return false;
        }
        
        $this->inlineTranslation->resume();
    }
}
