<?php
/**
 * Override shippment email class to add variables
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Model\Order\Email\Sender\Shipment;

use Arb\Order\Logger\Logger;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Container\ShipmentIdentity;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Email\Sender;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\ResourceModel\Order\Shipment as ShipmentResource;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\DataObject;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Marketplace\Model\Orders;

/**
 * Shippment email variables
 */
class ShipmentSender extends \Magento\Sales\Model\Order\Email\Sender\ShipmentSender
{
    /**
     * Seller image store location
     */
    const SELLER_PROFILE_IMAGE_LOCATION = 'avatar/';

    /**
     * seller image placehoder
     */
    const SELLER_NO_IMAGE = 'noimage.png';

    /**
     * Catalog image path separator
     */
    const CATALOG_PRODUCT_IMAGE = '/catalog/product/';

    /**
     * @var PaymentHelper
     */
    protected $paymentHelper;

    /**
     * @var ShipmentResource
     */
    protected $shipmentResource;

    /**
     * Global configuration storage.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $globalConfig;

    /**
     * @var Renderer
     */
    protected $addressRenderer;

    /**
     * Application Event Dispatcher
     *
     * @var ManagerInterface
     */
    protected $eventManager;
    
     /**
      * @var \Webkul\Marketplace\Helper\Data
      */
    protected $wkHelper;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $priceHelper;

    /**
     * @var \Webkul\Marketplace\Model\SaleslistFactory
     */
    protected $salesList;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Webkul\Marketplace\Model\OrdersFactory
     */
    protected $wkOrders;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Logger
     */
    private $orderLogger;

    /**
     * Undocumented function
     *
     * @param Template $templateContainer
     * @param ShipmentIdentity $identityContainer
     * @param \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param Renderer $addressRenderer
     * @param PaymentHelper $paymentHelper
     * @param ShipmentResource $shipmentResource
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig
     * @param ManagerInterface $eventManager
     * @param StoreManagerInterface $storeManager
     * @param \Webkul\Marketplace\Helper\Data $wkHelper
     * @param \Webkul\Marketplace\Model\SaleslistFactory $salesList
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Webkul\Marketplace\Model\OrdersFactory $wkOrders
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Template $templateContainer,
        ShipmentIdentity $identityContainer,
        \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory,
        \Psr\Log\LoggerInterface $logger,
        Renderer $addressRenderer,
        PaymentHelper $paymentHelper,
        ShipmentResource $shipmentResource,
        \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        \Webkul\Marketplace\Helper\Data $wkHelper,
        \Webkul\Marketplace\Model\SaleslistFactory $salesList,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Webkul\Marketplace\Model\OrdersFactory $wkOrders,
        \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory $collectionFactory,
        Logger $orderLogger
    ) {
        parent::__construct(
            $templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $shipmentResource,
            $globalConfig,
            $eventManager
        );
        $this->wkHelper = $wkHelper;
        $this->salesList = $salesList;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->wkOrders = $wkOrders;
        $this->collectionFactory = $collectionFactory;
        $this->orderLogger = $orderLogger;
    }

    /**
     * Sends order shipment email to the customer.
     *
     * Email will be sent immediately in two cases:
     *
     * - if asynchronous email sending is disabled in global settings
     * - if $forceSyncMode parameter is set to TRUE
     *
     * Otherwise, email will be sent later during running of
     * corresponding cron job.
     *
     * @param Shipment $shipment
     * @param bool $forceSyncMode
     * @return bool
     * @throws \Exception
     */
    public function send(Shipment $shipment, $forceSyncMode = false)
    {
        $shipment->setSendEmail($this->identityContainer->isEnabled());

        if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
            $order = $shipment->getOrder();
            $this->identityContainer->setStore($order->getStore());
            $productId = $this->getProductIdFromShipment($shipment);
            $sellerId = $this->getSellerId($productId);
            $sellerOrderData = $this->prepareOrderDetails($sellerId, $order);
            $orderTotals = $this->getShippedOrderTotal(
                $sellerOrderData['total_product_price'],
                $order->getId(),
                $sellerId
            );
            $merchant = $this->getSellerDataBySellerId($sellerId);
            $tracking = $this->getTrackingInformation($shipment, $sellerId);
            $transport = [
                'order' => $order,
                'increment_id' => $order->getIncrementId(),
                'shipment' => $shipment,
                'comment' => $shipment->getCustomerNoteNotify() ? $shipment->getCustomerNote() : '',
                'billing' => $order->getBillingAddress(),
                'payment_html' => $this->getPaymentHtml($order),
                'store' => $order->getStore(),
                'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
                'tracking_number' => $tracking['tracking_number'],
                'tracking_courier_name' => $tracking['tracking_courier_name'],
                'order_data' => [
                    'customer_name' => $order->getCustomerName(),
                    'is_not_virtual' => $order->getIsNotVirtual(),
                    'email_customer_note' => $order->getEmailCustomerNote(),
                    'frontend_status_label' => $order->getFrontendStatusLabel(),
                ],
                'merchant_name' => $merchant['merchant_name'],
                'merchant_contact' => $merchant['merchant_contact'],
                'merchant_logo' => $merchant['merchant_logo'],
                'product_info'=> $sellerOrderData['html'],
                'shipping_fees'=> $order->formatPrice($orderTotals['shipping_fees']),
                'grand_total' => $order->formatPrice($orderTotals['grand_total']),
            ];

            $transport['email_content_alignment'] = 'left';
            $transport['email_content_direction'] = 'ltr';
            if ($order->getStore()->getCode() === 'ar_SA') {
                $transport['email_content_alignment'] = 'right';
                $transport['email_content_direction'] = 'rtl';
            }

            $transportObject = new DataObject($transport);

            /**
             * Event argument `transport` is @deprecated. Use `transportObject` instead.
             */
            $this->eventManager->dispatch(
                'email_shipment_set_template_vars_before',
                ['sender' => $this, 'transport' => $transportObject->getData(), 'transportObject' => $transportObject]
            );

            $this->templateContainer->setTemplateVars($transportObject->getData());

            if ($this->checkAndSend($order)) {
                $shipment->setEmailSent(true);
                $this->shipmentResource->saveAttribute($shipment, ['send_email', 'email_sent']);
                return true;
            }
        } else {
            $shipment->setEmailSent(null);
            $this->shipmentResource->saveAttribute($shipment, 'email_sent');
        }

        $this->shipmentResource->saveAttribute($shipment, 'send_email');

        return false;
    }

    /**
     * Returns product id from shipment object
     *
     * @param Shipment $shipment
     * @return int
     */
    private function getProductIdFromShipment(Shipment $shipment)
    {
        $productId = 0;
        foreach ($shipment->getItemsCollection() as $item) {
            $productId = $item->getProductId();
            break;
        }
        return $productId;
    }

     /**
      * Get seller id by product id
      *
      * @param int $productId
      * @return int
      */
    private function getSellerId($productId)
    {
        return $this->wkHelper->getSellerIdByProductId($productId);
    }

    /**
     * Returns tracking number for specified consignment
     *
     * @param Shipment $shipment
     * @param int $sellerId
     *
     * @return array
     */
    private function getTrackingInformation(Shipment $shipment, int $sellerId)
    {
        $return = [
            'tracking_number' => null,
            'tracking_courier_name'=> null,
        ];
        $order = $shipment->getOrder();
        $tracksCollection = $order->getTracksCollection();
        $trackNumber = null;
        $courier = null;
        $trackId = null;

        foreach ($tracksCollection->getItems() as $track) {
            $mpOrderCollection = $this->collectionFactory->create()
                ->addFieldToFilter('order_id', $order->getId())
                ->addFieldToFilter('seller_id', $sellerId);

            if (!empty($mpOrderCollection)) {
                /** @var Orders $mpOrder */
                $mpOrder = $mpOrderCollection->getFirstItem();
                $trackNumber = $mpOrder->getData('tracking_number');
                $courier = $mpOrder->getData('carrier_name');
            }
        }

        $return['tracking_number'] =  $trackNumber;
        $return['tracking_courier_name'] =  $courier;

        return $return;
    }

    /**
     * Returns seller data by id
     *
     * @param int $sellerId
     * @return array
     */
    private function getSellerDataBySellerId($sellerId)
    {
        $sellerData = [
            'merchant_name' => null,
            'merchant_contact' => null,
            'merchant_logo' => null
        ];

        $collection = $this->wkHelper->getSellerCollection();
        $collection->addFieldToFilter('seller_id', $sellerId);
        $logopic = null;
        $contact = '';
        $shopTitle = '';
        foreach ($collection->getItems() as $merchant) {
            $logopic = (string)$merchant->getData('logo_pic');
            $contact = (string)$merchant->getData('contact_number');
            $shopTitle = (string)$merchant->getData('shop_title');
            if (strlen($logopic) <= 0) {
                $logopic = self::SELLER_NO_IMAGE;
            }
            if (strlen($contact) <= 0) {
                $contact = '';
            }
            if (strlen($shopTitle) <= 0) {
                $shopTitle = '';
            }
        }

        $sellerData['merchant_name'] = $shopTitle;
        $sellerData['merchant_contact'] = $contact ?: 'not provided';
        $sellerLogo = $logopic;
        $sellerData['merchant_logo'] =
            $this->wkHelper->getMediaUrl() . '/' . self::SELLER_PROFILE_IMAGE_LOCATION . $sellerLogo;

        return $sellerData;
    }

     /**
      * Preparing order details table for email template
      *
      * @param $sellerId
      * @param $order
      *
      * @return array
      * @throws NoSuchEntityException
      */
    private function prepareOrderDetails($sellerId, $order)
    {
        $html = '';
        $return = [];
        $salesCollection = $this->salesList->create()
            ->getCollection()
            ->addFieldToFilter(
                'main_table.seller_id',
                $sellerId
            )->addFieldToFilter(
                'main_table.order_id',
                $order->getId()
            );

        $alignment = 'left';
        if ($order->getStore()->getCode() === 'ar_SA') {
            $alignment = 'right';
        }

        $totalPrice = 0;
        $items = $salesCollection->getItems();
        foreach ($items as $item) {
            $parentItem = $item->getData('parent_item_id');
            if ($parentItem !== null) {
                continue;
            }

            $productName = $item->getData('magepro_name');
            $inclusiveTaxPrice = $item->getData('magepro_price') + $item->getData('total_tax');
            $productPrice = $item->getData('magequantity') . ' x ' . round($inclusiveTaxPrice, 2);

            $product = $this->productRepository->getById($item->getData('mageproduct_id'));
            $urlBaseMedia = rtrim($this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA), '/');
            $productImg = $urlBaseMedia . self::CATALOG_PRODUCT_IMAGE . $product->getSmallImage();
            $html .=
                '<tr>' .
                '<td align="' . $alignment . '"><img style="max-width:200px;" alt="product_image" src="' . $productImg . '"/></td>' .
                '<td align="' . $alignment . '">' . $productName . '</td>' .
                '<td align="' . $alignment . '" dir="ltr">' . $productPrice . '</td>' .
                '</tr>';

            $itemPrice = $item->getData('total_amount');
            $itemPrice += $item->getData('total_tax');
            $itemPrice -= $item->getData('applied_coupon_amount');

            $totalPrice += $itemPrice;
        }

        $return['total_product_price'] = $totalPrice;
        $return['html'] = $html;

        return $return;
    }

    /**
     * Return shipping and grand total in array
     *
     * @param float $orderTotal
     * @param int $orderId
     * @param int $sellerId
     * @return array
     */
    private function getShippedOrderTotal($orderTotal, $orderId, $sellerId)
    {
        $return = [];
        $shippingCharges = 0;
        $sellerOrder = $this->wkOrders->create()
            ->getCollection()
            ->addFieldToSelect('shipping_charges')
            ->addFieldToFilter('order_id', $orderId)
            ->addFieldToFilter('seller_id', $sellerId);
        foreach ($sellerOrder as $sellerData) {
            $shippingCharges += $sellerData->getShippingCharges();
        }
        $return['grand_total'] = $shippingCharges + $orderTotal;
        $return['shipping_fees'] = $shippingCharges;
        return $return;
    }
}
