<?php
/**
 * Repository for Extended Mage Order Details
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model;

use Arb\Order\Api\Data\MageOrderDetailsInterface;
use Arb\Order\Api\MageOrderDetailsRepositoryInterface;
use Arb\Order\Model\ResourceModel\MageOrderDetailsResource;
use Arb\Order\Model\ResourceModel\MageOrderDetails\Collection;
use Arb\Order\Model\ResourceModel\MageOrderDetails\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Arb\Order\Model\MageOrderDetailsFactory;

class MageOrderDetailsRepository implements MageOrderDetailsRepositoryInterface
{
    /**
     * @var MageOrderDetailsResource
     */
    private $resource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param MageOrderDetailsResource $resource
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        MageOrderDetailsResource $resource,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param MageOrderDetailsInterface $orderDetails
     *
     * @return MageOrderDetailsInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(MageOrderDetailsInterface $orderDetails)
    {
        try {
            $this->resource->save($orderDetails);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $orderDetails;
    }
    
    /**
     * @param int $id
     *
     * @return MageOrderDetailsInterface|null
     */
    public function getByOrderId(int $id)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            'order_id',
            $id
        )->create();

        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        return $collection->getSize() > 0 ? $collection->getFirstItem() : null;
    }
}
