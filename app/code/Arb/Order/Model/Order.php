<?php

/**
 * This file consist of class Order to get data with filters by
 * increment id and telephone no.
 *
 * @category Arb
 * @package Arb_Orders
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model;

use Exception;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderStatusHistoryInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Sales\Model\Order\ItemFactory;
use Arb\EsbNotifications\Helper\Data as EsbHelper;
use RuntimeException;
use Webkul\MpAdvancedCommission\Helper\Data as MpHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Webkul\Marketplace\Helper\Data as WebkulHelper;
use Arb\Order\Model\OrdertokenFactory;
use Arb\ArbPayment\Model\Encryption;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use Arb\OTPLogin\Model\OTP;
use Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Model\Oauth\TokenFactory;
use Webkul\Marketplace\Model\OrdersRepository;
use Magento\Customer\Model\CustomerFactory;
use Arb\Order\Api\OrderHistoryRepositoryInterface;
use Magento\Framework\Webapi\Request;
use Magento\Sales\Model\OrderRepositoryFactory;
use Zend\Uri\Uri;
use Magento\Framework\Webapi\ServiceInputProcessor;
use Arb\Order\Model\Order\Email\OrderEmailSender;
use Webkul\MpRmaSystem\Helper\Data;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Stdlib\DateTime as StdlibDateTime;

/**
 * class to get the order data with filters for increment id and telephone
 * @api
 *
 */
class Order implements \Arb\Order\Api\OrderInterface
{
    /**
     * @param \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @param \Magento\Sales\Model\Order\ItemFactory
     */
    protected $itemFactory;

    /**
     * @param \EsbHelper
     */
    protected $esbHelper;

    /**
     * @param \MpHelper
     */
    protected $mpHelper;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var PriceHelper
     */
    protected $priceHelper;

    /**
     * @var WebkulHelper
     */
    protected $wkHelper;

    /**
     * @var OrdertokenFactory
     */
    protected $ordertokenFactory;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var OTP
     */
    protected $otp;

    /*
    * Default Not available
    */
    const NOT_AVAILABLE = "NA";

    /*
    * Dummy LastName
    */
    const LASTNAME = "arb";

    /*
    * Default Quantity
    */
    const DEFAULT_QTY = "0";

    /*
    * reqType OTP
    */
    const REQTYPE_OTP = "otp";

    /*
    * reqType Confirmation
    */
    const REQTYPE_CONFIRMATION = "orderConfirmation";

    /*
    * reqType Order Login History
    */
    const REQTYPE_LOGIN_HISTORY = "loginOrderHistory";

    /**
     * 1 Hour TimeStamp
     */
    const HOUR = "3600";

    /**
     * Expiry time in sec for order confirmation
     */
    const EXIPRY_TIME = "600";

    /**
     * Order search criteria interface for order repository
     */
    const ORDER_SEARCH_CRITERIA_INTERFACE = 'Magento\Framework\Api\SearchCriteriaInterface';

    /*
    * Encryption
    */
    private $encryption;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var OrderHistoryRepositoryInterface
     */
    private $orderHistoryRepository;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var OrderRepositoryFactory
     */
    protected $orderRepositoryFactory;

    /**
     * @var Uri
     */
    protected $urlParse;

    /**
     * @var ServiceInputProcessor
     */
    protected $serviceInputProcessor;

    /**
     * @var OrderEmailSender
     */
    private $orderEmailSender;
    /**
     * @var Data
     */
    private $rmaHelper;
    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * Order constructor
     *
     * @param OrderFactory $orderFactory
     * @param ItemFactory $itemFactory
     * @param EsbHelper $esbHelper
     * @param MpHelper $mpHelper
     * @param CustomerRepositoryInterface $_customerRepository
     * @param Json $serializer
     * @param StoreManagerInterface $_storeManager
     * @param EncryptorInterface $encryptor
     * @param PriceHelper $priceHelper
     * @param WebkulHelper $wkHelper
     * @param Encryption $encryption
     * @param ScopeConfigInterface $scopeConfig
     * @param OrdertokenFactory $ordertokenFactory
     * @param DateTime $_dateTime
     * @param OTP $otp
     * @param TokenFactory $tokenModelFactory
     * @param Request $request
     * @param OrderRepositoryFactory $orderRepositoryFactory
     * @param Uri $urlParse
     * @param ServiceInputProcessor $serviceInputProcessor
     * @param OrdersRepository $ordersRepository
     * @param CustomerFactory $customerFactory
     * @param OrderHistoryRepositoryInterface $orderHistoryRepository
     * @param OrderEmailSender $orderEmailSender
     * @param Data $rmaHelper
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        OrderFactory $orderFactory,
        ItemFactory $itemFactory,
        EsbHelper $esbHelper,
        MpHelper $mpHelper,
        CustomerRepositoryInterface $_customerRepository,
        Json $serializer,
        StoreManagerInterface $_storeManager,
        EncryptorInterface $encryptor,
        PriceHelper $priceHelper,
        WebkulHelper $wkHelper,
        Encryption $encryption,
        ScopeConfigInterface $scopeConfig,
        OrdertokenFactory $ordertokenFactory,
        DateTime $_dateTime,
        OTP $otp,
        TokenFactory $tokenModelFactory,
        Request $request,
        OrderRepositoryFactory $orderRepositoryFactory,
        Uri $urlParse,
        ServiceInputProcessor $serviceInputProcessor,
        OrdersRepository $ordersRepository,
        CustomerFactory $customerFactory,
        OrderHistoryRepositoryInterface $orderHistoryRepository,
        OrderEmailSender $orderEmailSender,
        Data $rmaHelper,
        TimezoneInterface $timezone
    ) {
        $this->orderFactory = $orderFactory;
        $this->itemFactory = $itemFactory;
        $this->esbHelper = $esbHelper;
        $this->mpHelper = $mpHelper;
        $this->_customerRepository = $_customerRepository;
        $this->serializer = $serializer;
        $this->_storeManager = $_storeManager;
        $this->encryptor = $encryptor;
        $this->priceHelper = $priceHelper;
        $this->wkHelper = $wkHelper;
        $this->encryption = $encryption;
        $this->scopeConfig = $scopeConfig;
        $this->ordertokenFactory = $ordertokenFactory;
        $this->_dateTime = $_dateTime;
        $this->otp = $otp;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->ordersRepository = $ordersRepository;
        $this->customerFactory = $customerFactory;
        $this->orderHistoryRepository = $orderHistoryRepository;
        $this->request = $request;
        $this->orderRepositoryFactory = $orderRepositoryFactory;
        $this->urlParse = $urlParse;
        $this->serviceInputProcessor = $serviceInputProcessor;
        $this->orderEmailSender = $orderEmailSender;
        $this->rmaHelper = $rmaHelper;
        $this->timezone = $timezone;
    }

    /**
     * function return order details
     *
     * @param string $incrementId
     *
     * @return array Order.
     * @throws LocalizedException
     */
    public function getOrder($incrementId)
    {

        $resourceKey = $this->scopeConfig->getValue(
            'order/general/resourcekey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $vectorinit = $this->scopeConfig->getValue(
            'order/general/vector_init',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $decodedOrderData = urldecode(
            $this->encryption->decryptAES(
                $incrementId,
                $resourceKey,
                $vectorinit,
                'order'
            )
        );
        $orderParam = $this->serializer->unserialize($decodedOrderData);
        //check reqType
        if (empty($orderParam[0]['reqType'])) {
            throw new LocalizedException(__('Invalid Request'));
        }
        //Order History
        if ($orderParam[0]["reqType"] == self::REQTYPE_OTP) {
            //Get Order ID from Token
            if (!empty($orderParam[0]['token']) && !empty($orderParam[0]['otp'])) {
                $ordertokenFactoryModel = $this->ordertokenFactory->create();
                $ordertokenFactoryModel->load($orderParam[0]['token'], "token");
                if ($ordertokenFactoryModel->getOrderId()) {
                    $tokenExpireTime = $this->scopeConfig->getValue(
                        'order/general/token_expire_time',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    );
                    $currentDateTime = strtotime($this->_dateTime->date("Y-m-d H:i:s"));
                    $tokenExpireTime = $tokenExpireTime * (int) self::HOUR;
                    $createdAt = strtotime($ordertokenFactoryModel->getCreatedAt());
                    $timeDiff = (int) $currentDateTime - (int) $createdAt;
                    if ($timeDiff > $tokenExpireTime) {
                        throw new LocalizedException(__('Token has been expired.'));
                    }
                    $telephone = $ordertokenFactoryModel->getTelephone();
                    $response  = $this->otp->validateOTP($orderParam[0]['otp'], null, $telephone);
                    $orderId = $ordertokenFactoryModel->getOrderId();
                    if (empty($response["status"])) {
                        $orderData["order_id"] = 0;
                        $orderData["is_virtual"] = null;
                        $orderData["increment_id"] = $orderParam[0]['token'];
                        $orderData["created_at"] = "";
                        $orderData["customer_email"] = $response["message"];
                        $orderData["grand_total"] = 0;
                        $orderData["product_image"] = null;
                        $orderData["billing_address"] = null;
                        $orderData["coupon_code"] = null;
                        $orderData["consignments"] = [];
                        $orderData["base_subtotal"] = null;
                        $orderData["base_discount_amount"] = null;
                        $orderData["base_shipping_amount"] = null;
                        $orderData["total_items"] = null;
                        return ['results' =>  $orderData];
                    }
                } else {
                    throw new LocalizedException(__('Requested order doesn\'t exist'));
                }
            } else {
                throw new LocalizedException(__('Token or OTP should not be empty.'));
            }
        } elseif ($orderParam[0]["reqType"] == self::REQTYPE_CONFIRMATION) {
            //get order data from order increment Id
            if (!empty($orderParam[0]['orderId'])) {
                $incrementId = $orderParam[0]['orderId'];
                $order = $this->orderFactory->create()
                    ->getCollection()
                    ->addAddressFields()
                    ->addAttributeToFilter('increment_id', $incrementId)
                    ->getFirstItem();
                $orderId = $order->getIncrementId();
            }
        } elseif ($orderParam[0]["reqType"] == self::REQTYPE_LOGIN_HISTORY) {
            //Check if User is logged In
            $authorizationHeaderValue = $this->request->getHeader('Authorization');
            if (!$authorizationHeaderValue) {
                throw new LocalizedException(__('Token Invalid'));
            }
            $headerPieces = explode(" ", $authorizationHeaderValue);
            if (count($headerPieces) !== 2) {
                throw new LocalizedException(__('Token Invalid'));
            }
            $tokenType = strtolower($headerPieces[0]);
            if ($tokenType !== 'bearer') {
                throw new LocalizedException(__('Token Invalid'));
            }
            $customerToken = $headerPieces[1];
            //Validate Token
            $customerOuthModel = $this->tokenModelFactory->create();
            $customerOuthModel->load($customerToken, "token");
            $incrementId = $orderParam[0]['orderId'];
            //If customer exist
            if (!empty($customerOuthModel->getCustomerId()) && empty($customerOuthModel->getRevoked())) {
                $customerId = $customerOuthModel->getCustomerId();
                //filter by customer id for authorization token and order belongs to the same customer.
                $order = $this->orderFactory->create()
                    ->getCollection()
                    ->addAddressFields()
                    ->addFieldToFilter('increment_id', $incrementId)
                    ->addFieldToFilter('customer_id', $customerId)
                    ->getFirstItem();
                //Find out Order if exist
                if ($order->getIncrementId()) {
                    $orderId = $order->getIncrementId();
                } else {
                    throw new LocalizedException(__('Requested order doesn\'t exist'));
                }
            } else {
                throw new LocalizedException(__('Token Invalid'));
            }
        } else {
            //get order data from order increment Id or telephone no.
            if (!empty($orderParam[0]['orderId']) && !empty($orderParam[0]['telephone'])) {
                $telephone = $orderParam[0]['telephone'];
                $incrementId = $orderParam[0]['orderId'];
                $order = $this->orderFactory->create()
                    ->getCollection()
                    ->addAddressFields()
                    ->addAttributeToFilter('increment_id', $incrementId)
                    ->addAttributeToFilter('billing_telephone', $telephone)
                    ->getFirstItem();
                $orderId = $order->getIncrementId();
            } else {
                throw new LocalizedException(__('Telephone Number or Order ID should not be empty.'));
            }
        }
        // set order data
        if (!empty($orderId)) {
            switch ($orderParam[0]['reqType']) {
                case 'otp':
                    $order = $this->orderFactory->create()->load($orderId, "increment_id");
                    $orderData = $this->getOrderDetails($order, $resourceKey, $vectorinit);
                    break;
                case 'orderHistory':
                    //Generate Token and Save Token
                    try {
                        $ordertokenFactoryModel = $this->ordertokenFactory->create();
                        $telephone = $orderParam[0]["telephone"];
                        $orderId = $orderParam[0]["orderId"];
                        $createdAt = $this->_dateTime->date("Y-m-d H:i:s");
                        //16 charachrer token
                        $token = substr(uniqid(time() . rand()), 0, 16);
                        $ordertokenFactoryModel
                            ->setOrderId($orderId)
                            ->setTelephone($telephone)
                            ->setToken($token)
                            ->setCreatedAt($createdAt);
                        $ordertokenFactoryModel->save();
                        //Call generate OTP
                        $response = $this->otp->generateOTP($telephone);
                        $orderData["order_id"] = 0;
                        $orderData["is_virtual"] = null;
                        $orderData["increment_id"] = $token;
                        $orderData["created_at"] = "";
                        $orderData["customer_email"] = $response["message"];
                        $orderData["grand_total"] = 0;
                        $orderData["product_image"] = null;
                        $orderData["billing_address"] = null;
                        $orderData["coupon_code"] = null;
                        $orderData["consignments"] = [];
                        $orderData["base_subtotal"] = null;
                        $orderData["base_discount_amount"] = null;
                        $orderData["base_shipping_amount"] = null;
                        $orderData["total_items"] = null;
                    } catch (LocalizedException $e) {
                        throw new LocalizedException(__($e->getMessage()));
                    }
                    break;
                case 'orderConfirmation':
                    // check for first time hit
                    $orderUpdatedDate = strtotime($order->getUpdatedAt());
                    $currentTime = strtotime($this->_dateTime->date("Y-m-d H:i:s"));
                    $timeDiff = (int) $currentTime - (int) $orderUpdatedDate;
                    if ($timeDiff < self::EXIPRY_TIME) {
                        $orderData = $this->getOrderDetails($order, $resourceKey, $vectorinit);
                    } else {
                        throw new LocalizedException(__('Invalid request'));
                    }
                    break;
                case 'loginOrderHistory':
                    //For logged In customer
                    $orderData = $this->getOrderDetails($order, $resourceKey, $vectorinit);
                    break;
            }
        } else {
            throw new LocalizedException(__('Requested order doesn\'t exist'));
        }
        return ['results' =>  $orderData];
    }

    /**
     * @param int $orderId
     * @return boolean
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\MailException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function sendMail($orderId)
    {
        $order = $this->orderFactory->create()->load($orderId, "increment_id");

        if ($order->getId()) {
            if (!$order->getIsVirtual()) {
                if (false !== $this->orderEmailSender->preparePhysicalProductEmail($order->getId())) {
                    return true;
                }
            }
            $payment = $order->getPayment()->getData();
            $orderData["email"] = $order->getCustomerEmail();
            $customerFirstName = !empty($order->getCustomerFirstname()) ? $order->getCustomerFirstname() : "";
            $customerLastName = !empty($order->getCustomerLastname()) ? $order->getCustomerLastname() : "";
            //Removed ARB as last name when last name is empty
            if (strtolower($customerLastName) == self::LASTNAME) {
                $customerLastName = "";
            }
            $customerName = $customerFirstName . " " . $customerLastName;
            $orderData['customer_name'] = $customerName;
            $orderData["order_number"] = $order->getIncrementId();
            $storeCode = $order->getStore()->getCode();
            $orderData['store'] = $this->getStoreCode($storeCode);
            $orderData["total_amount"] = $this->priceHelper->currency($order->getGrandTotal(), true, false);
            $orderData["ref_no"] = !empty($payment["arb_ref_id"]) ? $payment["arb_ref_id"] : 0;
            $orderData["tran_id"] = !empty($payment["arb_payment_id"]) ? $payment["arb_payment_id"] : 0;
            $orderData["payment_id"] = !empty($payment["arb_ref_id"]) ? $payment["arb_ref_id"] : 0;
            $result = false;
            $error = 0;
            foreach ($order->getAllItems() as $item) {
                $itemData = $item->getData();
                //Voucher Code Decryption
                $vouchers = !empty($itemData["vouchers"]) ? $this->serializer->unserialize($itemData["vouchers"]) : [];
                $decryptedVouchers = [];
                foreach ($vouchers as $key => $voucher) {
                    $decryptedVouchers[$key] =  $this->encryptor->decrypt($voucher);
                }
                $voucherAmount = !empty($itemData["price_incl_tax"]) ? $itemData["price_incl_tax"] : 0;
                $orderData["voucher_amount"] = $this->priceHelper->currency($voucherAmount, true, false);
                $productName = !empty($itemData["name"]) ? $itemData["name"] : "";
                $orderData["product_name"] = $productName;
                $sellerId = $this->mpHelper->getSellerIdByItem($item);
                $orderData["seller_phone_no"] = self::NOT_AVAILABLE;
                $orderData["seller_name"] = self::NOT_AVAILABLE;
                if (!empty($sellerId)) {
                    //get Seller Phone Number
                    $seller = $this->_customerRepository->getById($sellerId);
                    $sellerPhone = $seller->getCustomAttribute("wkv_customer_care_number");
                    $orderData["seller_phone_no"] = $sellerPhone ? $sellerPhone->getValue() : '';
                    //Get Seller Shop Title
                    $sellerData = $this->wkHelper->getSellerDataBySellerId($sellerId)->getData();
                    $shopTitle = !empty($sellerData[0]["shop_title"]) ? $sellerData[0]["shop_title"] : "";
                    $orderData["seller_name"] = $shopTitle;
                }
                try {
                    //call send email ESB API
                    foreach ($decryptedVouchers as $key => $vocuher) {
                        $orderData["voucher_name"] =   $vocuher;
                        $orderData["ref_no"] =   $key;
                        $response = $this->esbHelper->sendEmailNotification($orderData);
                        $responseArray = json_decode($response, true);
                        if (empty($responseArray["success"])) {
                            $error++;
                        }
                    }
                    if (!$error) {
                        $result = true;
                    }
                } catch (Exception $e) {
                    $result = false;
                }
            }
            return $result;
        } else {
            throw new LocalizedException(__('Requested order doesn\'t exist'));
        }
    }
    /**
     * Get Store code
     *
     * @param string $code
     * @return string
     */
    private function getStoreCode($code)
    {
        switch ($code) {
            case 'ar_SA':
                $storeCode = "AR";
                break;
            default:
                $storeCode = "EN";
                break;
        }
        return $storeCode;
    }
    /**
     * Get Order Detail
     *
     * @param object $order
     * @return array
     */
    protected function getOrderDetails($order, $resourceKey, $vectorinit)
    {
        $orderData = [];
        $items = [];
        $itemImage = '';
        $orderIncrementId = ['incrementId' => $order->getIncrementId()];
        $jsonOrderIncrementId = $this->serializer->serialize($orderIncrementId);
        $incrementIdUrlEncodeData = urlencode($jsonOrderIncrementId);
        $encIncrementId = $this->encryption->encryptAES(
            $incrementIdUrlEncodeData,
            $resourceKey,
            $vectorinit,
            'order'
        );
        $orderData['order_id'] = $order->getId();
        $orderData['is_virtual'] = $order->getIsVirtual();
        $orderData['increment_id'] = $encIncrementId;
        $orderData['created_at'] = $order->getCreatedAt();
        $orderData['customer_email'] = $order->getCustomerEmail();
        $orderData['grand_total'] = (float)$order->getGrandTotal();
        $qty = self::DEFAULT_QTY;

        $orderData["base_subtotal"] = (float)$order->getSubtotalInclTax();
        $orderData["base_discount_amount"] = (float)$order->getDiscountAmount();
        $orderData["base_shipping_amount"] = (float)$order->getShippingAmount();

        $billingAddress = $order->getBillingAddress();
        $billingAddressData["address_type"] = $billingAddress->getAddressType();
        $billingAddressData["city"] = $billingAddress->getCity();
        $billingAddressData["company"] = $billingAddress->getCompany();
        $billingAddressData["country_id"] = $billingAddress->getCountryId();
        $billingAddressData["customer_address_id"] = $billingAddress->getCustomerAddressId();
        $billingAddressData["customer_id"] = $billingAddress->getCustomerId();
        $billingAddressData["email"] = $billingAddress->getEmail();
        $billingAddressData["entity_id"] = (int)$billingAddress->getEntityId();
        $billingAddressData["fax"] = $billingAddress->getFax();
        $billingAddressData["firstname"] = $billingAddress->getFirstName();
        $billingAddressData["lastname"] = $billingAddress->getLastName();
        $billingAddressData["middlename"] = $billingAddress->getMiddleName();
        $billingAddressData["parent_id"] = (int)$billingAddress->getParentId();
        $billingAddressData["postcode"] = $billingAddress->getPostCode();
        $billingAddressData["prefix"] = $billingAddress->getPrefix();
        $billingAddressData["region"] = $billingAddress->getRegion();
        $billingAddressData["region_id"] = (int)$billingAddress->getRegionId();
        $billingAddressData["street"] = $billingAddress->getStreet();
        $billingAddressData["suffix"] = $billingAddress->getSuffix();
        $billingAddressData["telephone"] = $billingAddress->getTelephone();
        $billingAddressData["vat_id"] = $billingAddress->getVatId();
        $billingAddressData["vat_is_valid"] = $billingAddress->getVatIsValid();
        $billingAddressData["vat_request_date"] = $billingAddress->getVatRequestDate();
        $billingAddressData["vat_request_id"] = $billingAddress->getVatRequestId();
        $billingAddressData["vat_request_success"] = $billingAddress->getVatRequestSuccess();
        $billingAddressData["extension_attributes"] = $billingAddress->getExtensionAttributes();
        $orderData["billing_address"] = $billingAddressData;

        //set item data
        $configurableParent = [];
        foreach ($order->getItems() as $item) {
            if ($item->getProduct()->getTypeId() === 'configurable') {
                $configurableParent[$item->getItemId()] = $item;
                continue;
            }
            $itemData = [];
            $encVouchers = "";
            $itemData['name'] = !empty($item->getProduct()) ? $item->getProduct()->getName() : $item->getName();
            $itemData['sku'] = $item->getSku();
            $qty += $item->getQtyOrdered();
            // voucher code updated to get voucher code for order
            $vouchers = !empty($item->getVouchers()) ? $this->serializer->unserialize($item->getVouchers()) : [];
            //Voucher Code Decryption
            $decryptedVouchers = [];
            foreach ($vouchers as $voucher) {
                $decryptedVouchers[] = $this->encryptor->decrypt($voucher);
            }
            if (!empty($decryptedVouchers)) {
                $orderItemVouchers = ['voucher' => implode(",", $decryptedVouchers)];
                $jsonOrderItemVouchers = $this->serializer->serialize($orderItemVouchers);
                $voucherUrlEncodeData = urlencode($jsonOrderItemVouchers);
                $encVouchers = $this->encryption->encryptAES($voucherUrlEncodeData, $resourceKey, $vectorinit, 'order');
            }
            $itemData['voucher_code'] = $encVouchers;
            $itemData['image_url'] = $item->getProduct()->getImage();
            $itemData['quantity'] = $item->getQtyOrdered();
            $itemData['type'] = $item->getProduct()->getTypeId();
            $itemData['discount_amount'] = $item->getDiscountAmount();
            if ($item->getParentItemId()) {
                $parent = $configurableParent[$item->getParentItemId()];
                $options = $parent->getProductOptions();
                foreach ($options['attributes_info'] as $option) {
                    $itemData['product_options'][] = [
                        'option_name' => __($option['label']),
                        'option_value' => __($option['value'])
                    ];
                }
                $itemData['original_price'] = $parent->getOriginalPrice();
                $itemData['item_id'] = $parent->getId();
                $itemData['product_id'] = $parent->getProduct()->getId();
                $itemData['row_total'] = $parent->getRowTotalInclTax();
                $itemData['discounted_price'] = $parent->getPriceInclTax();
                $itemData['item_status'] = $parent->getStatus();
            } else {
                $itemData['row_total'] = $item->getRowTotalInclTax();
                $itemData['item_id'] = $item->getId();
                $itemData['product_id'] = $item->getProduct()->getId();
                $itemData['original_price'] = $item->getOriginalPrice();
                $itemData['discounted_price'] = $item->getPriceInclTax();
                $itemData['item_status'] = $item->getStatus();
                $itemData['product_options'] = [];
            }

            $itemData['returnable_quantity'] = $this->rmaHelper->getRmaQty(
                $itemData['item_id'],
                $orderData['order_id'],
                $itemData['quantity'],
                0
            );

            $items[] = $itemData;
            if ($itemImage == '') {
                $itemImage = $item->getThumbnail();
            }
        }
        $orderData['product_image'] = $itemImage;

        $merchantConsignment = [];
        $merchantOrders = $this->ordersRepository->getByOrderId($order->getId());

        foreach ($merchantOrders as $merchantOrder) {
            $seller = $this->_customerRepository->getById($merchantOrder->getSellerId());
            $consignmentProductsIds = explode(',', $merchantOrder->getProductIds());

            $consignmentItems = [];
            foreach ($items as $orderItem) {
                if (in_array($orderItem['product_id'], $consignmentProductsIds)) {
                    unset($orderItem['product_id']);
                    $consignmentItems[] = $orderItem;
                }
            }

            $statusHistories = [];
            $statusHistory = $this->orderHistoryRepository->getByOrderId((int)$merchantOrder->getId());
            foreach ($statusHistory->getItems() as $status) {
                $formatTime = $this->timezone->date($status->getCreatedAt())
                    ->format(StdlibDateTime::DATETIME_PHP_FORMAT);

                $statusHistories[] = [
                    'created_at' => $formatTime,
                    'entity_id' => (int)$status->getId(),
                    'status' => __($status->getStatus())
                ];
            }

            $collection = $this->wkHelper->getSellerCollection();
            $collection->addFieldToFilter('seller_id', $merchantOrder->getSellerId());
            $shopTitle = '';
            foreach ($collection->getItems() as $merchant) {
                $shopTitle = (string)$merchant->getData('shop_title');
                if (strlen($shopTitle) <= 0) {
                    $shopTitle = '';
                }
            }

            $sellerPhone = $seller->getCustomAttribute("wkv_customer_care_number");
            $sellerPhoneNumber = $sellerPhone ? $sellerPhone->getValue() : '';

            $merchantConsignment[] = [
                'merchant_id' => $seller ? $seller->getId() : '',
                'is_returnable' => $this->isReturnable($statusHistory),
                'merchant_name' => $shopTitle,
                'merchant_phone' => $sellerPhoneNumber,
                'estimated_delivery_time' => $order->getEstimatedDeliveryTime(),     // logic to be added
                'tracking_no' => $merchantOrder->getTrackingNumber(),
                'status_histories' => $statusHistories,
                'items' => $consignmentItems
            ];
        }

        $orderData['coupon_code'] = $order->getCouponCode();
        $orderData['consignments'] = $merchantConsignment;
        $orderData['total_items'] = $qty;

        //Added 'items' key to maintain API compatibility in order APIs start
        $itemData = [];
        foreach ($merchantConsignment as $items) {
            foreach ($items['items'] as $item) {
                $itemData[] =  $item;
            }
        }
        $orderData['items'] = $itemData;
        //Added 'items' key to maintain API compatibility in order APIs end

        return $orderData;
    }

    /**
     * @param $statusHistories
     *
     * @return bool
     */
    private function isReturnable($statusHistories)
    {
        $returnDays = $this->scopeConfig->getValue('mprmasystem/settings/default_days');

        /** @var OrderStatusHistoryInterface $statusHistory */
        foreach ($statusHistories->getItems() as $statusHistory) {
            if ($statusHistory->getStatus() === 'delivered') {
                try {
                    $maxReturnDate = new \DateTime($statusHistory->getCreatedAt());
                    $maxReturnDate->modify('+ ' . $returnDays . ' day');
                    $todaysDate = new \DateTime();
                    if ($todaysDate <= $maxReturnDate) {
                        return true;
                    }
                } catch (Exception $e) {
                    throw new RuntimeException('There is a problem with date.');
                }
            }
        }

        return false;
    }

    /**
     * Lists orders that match specified search criteria.
     * Decrypts input data string and pass \Magento\Framework\Api\SearchCriteriaInterface
     *
     * @param  string $data encrypted search criteria.
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface Order search result interface.
     */
    public function getOrderList($data)
    {
        $resourceKey = $this->scopeConfig->getValue(
            'order/general/resourcekey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $vectorinit = $this->scopeConfig->getValue(
            'order/general/vector_init',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $decodedOrderData = urldecode(
            $this->encryption->decryptAES(
                $data,
                $resourceKey,
                $vectorinit,
                'order'
            )
        );

        $orderParam = $this->serializer->unserialize($decodedOrderData);

        $inputData = $this->urlParse->setQuery($orderParam)->getQueryAsArray();

        if (empty($inputData)) {
            throw new LocalizedException(__('Invalid request'));
        }

        if (!isset($inputData['searchCriteria'])) {
            throw new LocalizedException(__('Invalid request'));
        }

        //For API backward compatibility, this must be deleted after all customer upgraded to physical APP.
        foreach($inputData['searchCriteria']['filter_groups'] as $key => $search){
            if(isset($search['filters'])){
                if($search['filters'][0]['field'] == 'state' && $search['filters'][0]['value'] == 'complete') {
                    $inputData['searchCriteria']['filter_groups'][$key]['filters'][0]['value'] = 'complete,closed';
                    $inputData['searchCriteria']['filter_groups'][$key]['filters'][0]['condition_type'] = 'in';
                }
            }
        }
        
        $searchCriteria = $this->serviceInputProcessor->convertValue($inputData['searchCriteria'], self::ORDER_SEARCH_CRITERIA_INTERFACE);
        return $this->orderRepositoryFactory->create()->getList($searchCriteria);
    }
}
