<?php
/**
 * This file consist of class OTP Validation Model
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class Ordertoken extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = "order_detail_token";
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\Order\Model\ResourceModel\Ordertoken::class
        );
    }
    /**
     * get Identities from Cache.
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
