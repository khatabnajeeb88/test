<?php
/**
 * Mage Extended Order Data Class with getters/setters
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Model;

use Arb\Order\Api\Data\MageOrderDetailsInterface;
use Magento\Framework\Model\AbstractModel;
use Arb\Order\Model\ResourceModel\MageOrderDetailsResource;

class MageOrderDetails extends AbstractModel implements MageOrderDetailsInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init(MageOrderDetailsResource::class);
    }
}
