<?php
/**
 * OrderHistory Data Class with getters/setters
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Model;

use Magento\Framework\Model\AbstractModel;
use Arb\Order\Model\ResourceModel\OrderHistoryResource;
use Arb\Order\Api\Data\OrderHistoryInterface;

class OrderHistory extends AbstractModel implements OrderHistoryInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init(OrderHistoryResource::class);
    }

    /**
     * @return int|null
     */
    public function getOrderId()
    {
        $entityId = $this->getData(OrderHistoryInterface::ORDER_HISTORY_ORDER_ID);

        return $entityId ? (int)$entityId : null;
    }

    /**
     * @param int $entityId
     *
     * @return OrderHistoryInterface
     */
    public function setOrderId(int $entityId)
    {
        $this->setData(OrderHistoryInterface::ORDER_HISTORY_ORDER_ID, $entityId);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->getData(OrderHistoryInterface::ORDER_HISTORY_STATUS);
    }

    /**
     * @param string $status
     *
     * @return OrderHistoryInterface
     */
    public function setStatus(string $status)
    {
        $this->setData(OrderHistoryInterface::ORDER_HISTORY_STATUS, $status);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(OrderHistoryInterface::ORDER_HISTORY_CREATED_AT);
    }

    /**
     * @param string $createdAt
     *
     * @return OrderHistoryInterface
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->setData(OrderHistoryInterface::ORDER_HISTORY_CREATED_AT, $createdAt);

        return $this;
    }
}
