<?php
/**
 * Resource Model for Extended Mage Order Details
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Arb\Order\Api\Data\MageOrderDetailsInterface;

class MageOrderDetailsResource extends AbstractDb
{
    /**
     * Initialize Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(MageOrderDetailsInterface::ORDER_TABLE_NAME, MageOrderDetailsInterface::ORDER_ENTITY_ID);
    }
}
