<?php
/**
 * This file consist of class Ordertoken collection
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model\ResourceModel\Ordertoken;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Ordertoken collection
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\Order\Model\Ordertoken::class,
            \Arb\Order\Model\ResourceModel\Ordertoken::class
        );
    }
}
