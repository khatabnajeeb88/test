<?php
/**
 * This file consist of class order_detail_token Resource Model collection
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Model\ResourceModel;
 
class Ordertoken extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('order_detail_token', 'entity_id');
    }
}
