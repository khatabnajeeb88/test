<?php
/**
 * Resource Model for Order History
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Arb\Order\Api\Data\OrderHistoryInterface;

class OrderHistoryResource extends AbstractDb
{
    /**
     * Initialize Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(OrderHistoryInterface::ORDER_HISTORY_TABLE_NAME, OrderHistoryInterface::ORDER_HISTORY_ENTITY_ID);
    }
}
