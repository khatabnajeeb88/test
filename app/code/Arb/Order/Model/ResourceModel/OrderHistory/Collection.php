<?php
/**
 * Collection for OrderHistory entity
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model\ResourceModel\OrderHistory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Arb\Order\Model\ResourceModel\OrderHistoryResource;
use Arb\Order\Model\OrderHistory;

class Collection extends AbstractCollection
{
    /**
     * Define Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(OrderHistory::class, OrderHistoryResource::class);
    }
}
