<?php
/**
 * Collection for Mage Order Extended Details
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Model\ResourceModel\MageOrderDetails;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Arb\Order\Model\ResourceModel\MageOrderDetailsResource;
use Arb\Order\Model\MageOrderDetails;

class Collection extends AbstractCollection
{
    /**
     * Define Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(MageOrderDetails::class, MageOrderDetailsResource::class);
    }
}
