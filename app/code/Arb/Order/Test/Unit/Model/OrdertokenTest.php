<?php
/**
 * OrderTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Test\Unit\Model;

use Arb\Order\Model\Ordertoken;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class OrderTest for testing  Ordertoken class
 * @covers \Arb\Order\Model\Ordertoken
 */
class OrdertokenTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject(\Arb\Order\Model\Ordertoken::class);
    }

    /**
     * testGetAvailableStatuses method
     */
    public function testGetIdentities()
    {
        $status = $this->model->getIdentities();
        $this->assertEquals(["0" => "order_detail_token_"], $status);
    }
}
