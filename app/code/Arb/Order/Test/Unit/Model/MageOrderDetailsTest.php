<?php 
/**
 * This file consist of PHPUnit test case for Order History Repository
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Model;

use Arb\Order\Model\MageOrderDetails;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Exception;
use ReflectionException;
use Arb\Order\Api\Data\MageOrderDetailsInterface;
use Magento\Framework\Model\AbstractModel;
use Arb\Order\Model\ResourceModel\MageOrderDetailsResource;

/**
 * @covers \Arb\Order\Model\MageOrderDetails
 */
class MageOrderDetailsTest extends TestCase
{
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject("Arb\Order\Model\MageOrderDetails");
    }

    /**
     * testGetIdentities method
     */
    public function testGetIdentities()
    {
        $status = $this->model->getIdentities();
        $this->assertEquals(null, $status);    
    }
}
