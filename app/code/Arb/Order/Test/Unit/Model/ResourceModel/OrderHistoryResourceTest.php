<?php 
/**
 * This file consist of PHPUnit test case for OrderHistoryResource
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Model\ResourceModel;

use Arb\Order\Api\Data\OrderHistoryInterface;
use Arb\Order\Model\ResourceModel\OrderHistoryResource;
use Magento\Framework\Exception\LocalizedException;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Order\Model\ResourceModel\OrderHistoryResource
 */
class OrderHistoryResourceTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderHistoryResource
     */
    private $resourceModel;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->resourceModel = $objectManager->getObject(OrderHistoryResource::class);
    }

    /**
     * Test getting table
     *
     * @throws LocalizedException
     *
     * @return void
     */
    public function testGetMainTable()
    {
        $this->assertSame(
            $this->resourceModel->getMainTable(),
            $this->resourceModel->getTable(OrderHistoryInterface::ORDER_HISTORY_TABLE_NAME)
        );
    }
}
