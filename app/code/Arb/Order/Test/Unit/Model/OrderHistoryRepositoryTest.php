<?php 
/**
 * This file consist of PHPUnit test case for Order History Repository
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Model;

use Arb\Order\Model\OrderHistory;
use Arb\Order\Model\OrderHistoryFactory;
use Arb\Order\Model\ResourceModel\OrderHistoryResource;
use Arb\Order\Model\OrderHistoryRepository;
use Magento\CatalogRule\Model\ResourceModel\Product\CollectionProcessor;
use Magento\Framework\Api\Search\SearchCriteria;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Arb\Order\Model\ResourceModel\OrderHistory\CollectionFactory;
use Arb\Order\Model\ResourceModel\OrderHistory\Collection;
use Magento\Framework\Api\SearchResultsInterface;
use Exception;
use ReflectionException;

/**
 * @covers \Arb\Order\Model\OrderHistoryRepository
 */
class OrderHistoryRepositoryTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderHistoryResource
     */
    private $orderHistoryResourceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionProcessor
     */
    private $collectionProcessorMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterfaceFactory
     */
    private $searchResultsInterfaceFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderHistoryRepository
     */
    private $testedObject;

    /**
     * Setting up tested object
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->orderHistoryResourceMock = $this->createMock(OrderHistoryResource::class);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->collectionProcessorMock = $this->createMock(CollectionProcessorInterface::class);
        $this->searchResultsInterfaceFactoryMock = $this->createMock(SearchResultsInterfaceFactory::class);

        $this->testedObject = $objectManager->getObject(OrderHistoryRepository::class, [
            'orderHistoryResource' => $this->orderHistoryResourceMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'collectionProcessor' => $this->collectionProcessorMock,
            'searchResultsInterfaceFactory' => $this->searchResultsInterfaceFactoryMock
        ]);
    }

    /**
     * @return void
     *
     * @throws CouldNotSaveException
     */
    public function testSave()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|OrderHistory $orderHistoryMock */
        $orderHistoryMock = $this->createMock(OrderHistory::class);

        $this->orderHistoryResourceMock
            ->expects($this->once())
            ->method('save')
            ->willReturn($orderHistoryMock);

        $save = $this->testedObject->save($orderHistoryMock);
        $this->assertEquals($orderHistoryMock, $save);
    }

    /**
     * Test save() method for throwing an exception
     *
     * @return void
     *
     * @throws CouldNotSaveException
     */
    public function testSaveExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|OrderHistory $orderHistoryMock */
        $orderHistoryMock = $this->createMock(OrderHistory::class);

        $this->orderHistoryResourceMock
            ->expects($this->once())
            ->method('save')
            ->willThrowException(new Exception('Save error'));

        $this->expectException(CouldNotSaveException::class);

        $this->testedObject->save($orderHistoryMock);
    }

    /**
     * @return void
     *
     * @throws Exception
     */
    public function testDelete()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|OrderHistory $orderHistoryMock */
        $orderHistoryMock = $this->createMock(OrderHistory::class);

        $this->orderHistoryResourceMock
            ->expects($this->once())
            ->method('delete');

        $this->testedObject->delete($orderHistoryMock);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetList()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        $this->collectionProcessorMock
            ->expects($this->once())
            ->method('process');

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultMock */
        $searchResultMock = $this->createMock(SearchResultsInterface::class);

        $this->searchResultsInterfaceFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchResultMock);

        $collectionMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn([]);

        $collectionMock
            ->expects($this->once())
            ->method('getSize')
            ->willReturn(1);

        $searchResultMock
            ->expects($this->once())
            ->method('setSearchCriteria')
            ->willReturnSelf();

        $searchResultMock
            ->expects($this->once())
            ->method('setItems')
            ->willReturnSelf();

        $searchResultMock
            ->expects($this->once())
            ->method('setTotalCount')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $getList = $this->testedObject->getList($searchCriteriaMock);
        $this->assertInstanceOf(SearchResultsInterface::class, $getList);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetByOrderId()
    {
        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('addFilter')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultMock */
        $searchResultMock = $this->createMock(SearchResultsInterface::class);

        $this->searchResultsInterfaceFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchResultMock);

        $collectionMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn([]);

        $collectionMock
            ->expects($this->once())
            ->method('getSize')
            ->willReturn(1);

        $getByOrderId = $this->testedObject->getByOrderId(5);
        $this->assertInstanceOf(SearchResultsInterface::class, $getByOrderId);
    }
}
