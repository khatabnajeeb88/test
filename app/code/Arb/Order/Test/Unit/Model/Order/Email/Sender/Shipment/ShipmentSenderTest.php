<?php
namespace Arb\Order\Test\Unit\Model\Order\Email\Sender\Shipment;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Order\Model\Order\Email\Sender\Shipment\ShipmentSender
 */
class ShipmentSenderTest extends TestCase
{
    /**
     * Mock templateContainer
     *
     * @var \Magento\Sales\Model\Order\Email\Container\Template|PHPUnit_Framework_MockObject_MockObject
     */
    private $templateContainer;

    /**
     * Mock identityContainer
     *
     * @var \Magento\Sales\Model\Order\Email\Container\ShipmentIdentity|PHPUnit_Framework_MockObject_MockObject
     */
    private $identityContainer;

    /**
     * Mock senderBuilderFactoryInstance
     *
     * @var \Magento\Sales\Model\Order\Email\SenderBuilder|PHPUnit_Framework_MockObject_MockObject
     */
    private $senderBuilderFactoryInstance;

    /**
     * Mock senderBuilderFactory
     *
     * @var \Magento\Sales\Model\Order\Email\SenderBuilderFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $senderBuilderFactory;

    /**
     * Mock logger
     *
     * @var \Psr\Log\LoggerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $logger;

    /**
     * Mock addressRenderer
     *
     * @var \Magento\Sales\Model\Order\Address\Renderer|PHPUnit_Framework_MockObject_MockObject
     */
    private $addressRenderer;

    /**
     * Mock paymentHelper
     *
     * @var \Magento\Payment\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentHelper;

    /**
     * Mock shipmentResource
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Shipment|PHPUnit_Framework_MockObject_MockObject
     */
    private $shipmentResource;

    /**
     * Mock globalConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $globalConfig;

    /**
     * Mock eventManager
     *
     * @var \Magento\Framework\Event\ManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $eventManager;

    /**
     * Mock storeManager
     *
     * @var \Magento\Store\Model\StoreManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManager;

    /**
     * Mock wkHelper
     *
     * @var \Webkul\Marketplace\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $wkHelper;

    /**
     * Mock salesListInstance
     *
     * @var \Webkul\Marketplace\Model\Saleslist|PHPUnit_Framework_MockObject_MockObject
     */
    private $salesListInstance;

    /**
     * Mock salesList
     *
     * @var \Webkul\Marketplace\Model\SaleslistFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $salesList;

    /**
     * Mock productRepository
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepository;

    /**
     * Mock wkOrdersInstance
     *
     * @var \Webkul\Marketplace\Model\Orders|PHPUnit_Framework_MockObject_MockObject
     */
    private $wkOrdersInstance;

    /**
     * Mock wkOrders
     *
     * @var \Webkul\Marketplace\Model\OrdersFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $wkOrders;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Order\Model\Order\Email\Sender\Shipment\ShipmentSender
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->templateContainer = $this->createMock(\Magento\Sales\Model\Order\Email\Container\Template::class);
        $this->identityContainer = $this->createMock(\Magento\Sales\Model\Order\Email\Container\ShipmentIdentity::class);
        $this->senderBuilderFactoryInstance = $this->createMock(\Magento\Sales\Model\Order\Email\SenderBuilder::class);
        $this->senderBuilderFactory = $this->createMock(\Magento\Sales\Model\Order\Email\SenderBuilderFactory::class);
        $this->senderBuilderFactory->method('create')->willReturn($this->senderBuilderFactoryInstance);
        $this->logger = $this->createMock(\Psr\Log\LoggerInterface::class);
        $this->addressRenderer = $this->createMock(\Magento\Sales\Model\Order\Address\Renderer::class);
        $this->paymentHelper = $this->createMock(\Magento\Payment\Helper\Data::class);
        $this->shipmentResource = $this->createMock(\Magento\Sales\Model\ResourceModel\Order\Shipment::class);
        $this->globalConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->eventManager = $this->createMock(\Magento\Framework\Event\ManagerInterface::class);
        $this->storeManager = $this->createMock(\Magento\Store\Model\StoreManagerInterface::class);
        $this->wkHelper = $this->createMock(\Webkul\Marketplace\Helper\Data::class);
        $this->salesListInstance = $this->createMock(\Webkul\Marketplace\Model\Saleslist::class);
        $this->salesList = $this->createMock(\Webkul\Marketplace\Model\SaleslistFactory::class);
        $this->salesList->method('create')->willReturn($this->salesListInstance);
        $this->productRepository = $this->createMock(\Magento\Catalog\Api\ProductRepositoryInterface::class);
        $this->wkOrdersInstance = $this->createMock(\Webkul\Marketplace\Model\Orders::class);
        $this->wkOrders = $this->createMock(\Webkul\Marketplace\Model\OrdersFactory::class);
        $this->wkOrders->method('create')->willReturn($this->wkOrdersInstance);
        $this->testObject = $this->objectManager->getObject(
        \Arb\Order\Model\Order\Email\Sender\Shipment\ShipmentSender::class,
            [
                'templateContainer' => $this->templateContainer,
                'identityContainer' => $this->identityContainer,
                'senderBuilderFactory' => $this->senderBuilderFactory,
                'logger' => $this->logger,
                'addressRenderer' => $this->addressRenderer,
                'paymentHelper' => $this->paymentHelper,
                'shipmentResource' => $this->shipmentResource,
                'globalConfig' => $this->globalConfig,
                'eventManager' => $this->eventManager,
                'storeManager' => $this->storeManager,
                'wkHelper' => $this->wkHelper,
                'salesList' => $this->salesList,
                'productRepository' => $this->productRepository,
                'wkOrders' => $this->wkOrders,
            ]
        );
    }

    /**
     * @return array
     */
    public function dataProviderForTestSend()
    {
        return [
            'Testcase 1' => [
                'prerequisites' => ['param' => 1],
                'expectedResult' => ['param' => 1]
            ]
        ];
    }

    /**
     * @dataProvider dataProviderForTestSend
     */
    public function testSend(array $prerequisites, array $expectedResult)
    {
        $this->assertEquals($expectedResult['param'], $prerequisites['param']);
    }
}
