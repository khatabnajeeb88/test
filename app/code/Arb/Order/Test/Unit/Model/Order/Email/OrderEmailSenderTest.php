<?php
namespace Arb\Order\Test\Unit\Model\Order\Email;

use Arb\CustomWebkul\Model\Webkul\ResourceModel\Saleslist\Collection;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\Store;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection as WebkulOrderCollection;
use Magento\Framework\Exception\CouldNotSaveException;
use Exception;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @covers \Arb\Order\Model\Order\Email\OrderEmailSender
 */
class OrderEmailSenderTest extends TestCase
{
    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock storeManager
     *
     * @var \Magento\Store\Model\StoreManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManager;

    /**
     * Mock inlineTranslation
     *
     * @var \Magento\Framework\Translate\Inline\StateInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $inlineTranslation;

    /**
     * Mock transportBuilder
     *
     * @var \Magento\Framework\Mail\Template\TransportBuilder|PHPUnit_Framework_MockObject_MockObject
     */
    private $transportBuilder;

    /**
     * Mock wkHelper
     *
     * @var \Webkul\Marketplace\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $wkHelper;

    /**
     * Mock productRepository
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepository;

    /**
     * Mock imageHelper
     *
     * @var \Magento\Catalog\Helper\Image|PHPUnit_Framework_MockObject_MockObject
     */
    private $imageHelper;

    /**
     * Mock orderRepository
     *
     * @var \Magento\Sales\Api\Data\OrderInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderRepository;

    /**
     * Mock pricingHelper
     *
     * @var \Magento\Framework\Pricing\Helper\Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $pricingHelper;

    /**
     * Mock wkOrdersInstance
     *
     * @var \Webkul\Marketplace\Model\Orders|PHPUnit_Framework_MockObject_MockObject
     */
    private $wkOrdersInstance;

    /**
     * Mock wkOrders
     *
     * @var \Webkul\Marketplace\Model\OrdersFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $wkOrders;

    /**
     * Mock wkOrders
     *
     * @var \Webkul\Marketplace\Model\SaleslistFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $saleslistFactory;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\Order\Model\Order\Email\OrderEmailSender
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->storeManager = $this->createMock(\Magento\Store\Model\StoreManagerInterface::class);
        $this->inlineTranslation = $this->createMock(\Magento\Framework\Translate\Inline\StateInterface::class);
        $this->transportBuilder = $this->createMock(\Magento\Framework\Mail\Template\TransportBuilder::class);
        $this->wkHelper = $this->createMock(\Webkul\Marketplace\Helper\Data::class);
        $this->productRepository = $this->createMock(\Magento\Catalog\Api\ProductRepositoryInterface::class);
        $this->imageHelper = $this->createMock(\Magento\Catalog\Helper\Image::class);
        $this->orderRepository = $this->getMockBuilder(\Magento\Sales\Api\Data\OrderInterface::class)
            ->setMethods(['load'])
            ->getMockForAbstractClass();
        $this->pricingHelper = $this->createMock(\Magento\Framework\Pricing\Helper\Data::class);
        $this->wkOrdersInstance = $this->createMock(\Webkul\Marketplace\Model\Orders::class);

//        $this->wkOrders = $this->createMock(\Webkul\Marketplace\Model\OrdersFactory::class);
//        $this->wkOrders = $this->getMockBuilder(\Webkul\Marketplace\Model\OrdersFactory::class)
//            ->setMethods(['getCollection'])
//            ->getMockForAbstractClass();
        $this->wkOrders = $this->getMockBuilder(\Webkul\Marketplace\Model\OrdersFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create', 'getCollection'])
            ->getMock();


        $this->saleslistFactory = $this->getMockBuilder(\Webkul\Marketplace\Model\SaleslistFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create', 'getCollection'])
            ->getMock();

//        $this->wkOrders->method('create')->willReturn($this->wkOrdersInstance);
        $this->testObject = $this->objectManager->getObject(
            \Arb\Order\Model\Order\Email\OrderEmailSender::class,
            [
                'scopeConfig' => $this->scopeConfig,
                'storeManager' => $this->storeManager,
                'inlineTranslation' => $this->inlineTranslation,
                'transportBuilder' => $this->transportBuilder,
                'wkHelper' => $this->wkHelper,
                'productRepository' => $this->productRepository,
                'imageHelper' => $this->imageHelper,
                'orderRepository' => $this->orderRepository,
                'pricingHelper' => $this->pricingHelper,
                'wkOrders' => $this->wkOrders,
                'saleslistFactory' => $this->saleslistFactory
            ]
        );
    }

    /**
     * Test forceReload parameter
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testPreparePhysicalProductEmail()
    {
        $orderMock = $this->createMock(Order::class);
        $this->orderRepository->expects($this->once())->method('load')->willReturn($orderMock);
        $storeMock = $this->createMock(Store::class);
        $orderMock->expects($this->atLeastOnce())->method('getStore')->willReturn($storeMock);

        $itemMock = $this->getMockBuilder(OrderItemInterface::class)
            ->setMethods(['getId'])
            ->getMockForAbstractClass();
        $orderMock->expects($this->atLeastOnce())->method('getAllVisibleItems')->willReturn([$itemMock]);

        $productMock = $this->createMock(ProductInterface::class);
        $this->productRepository->expects($this->once())->method('get')->willReturn($productMock);

        $this->imageHelper->expects($this->once())->method('init')->willReturnSelf();

        $this->wkOrders->expects($this->once())->method('create')->willReturnSelf();

        $webkulOrderCollection = $this->createMock(WebkulOrderCollection::class);
        $this->wkOrders->expects($this->once())->method('getCollection')->willReturn($webkulOrderCollection);

        $wkOrdersMock= $this->createMock(\Webkul\Marketplace\Model\Orders::class);
        $webkulOrderCollection->expects($this->once())->method('addFieldToSelect')->willReturnSelf();
        $webkulOrderCollection
            ->method('addFieldToFilter')
            ->willReturnOnConsecutiveCalls($webkulOrderCollection, [$wkOrdersMock]);

        $this->saleslistFactory->expects($this->once())->method('create')->willReturnSelf();

        $saleslistCollection = $this->createMock(Collection::class);
        $this->saleslistFactory->expects($this->once())->method('getCollection')->willReturn($saleslistCollection);

        $saleslistCollection->method('addFieldToFilter')->willReturnSelf();

        $saleslistCollection->expects($this->exactly(4))->method('getItems')->willReturn([]);

        $itemMock->expects($this->once())->method('getId')->willReturn(1);

        $orderItemMock = $this->createMock(\Magento\Sales\Api\Data\OrderItemInterface::class);
        $orderMock->expects($this->once())->method('getItems')->willReturn([$orderItemMock]);

        $sellerCollectionMock = $this->createMock(\Webkul\Marketplace\Model\ResourceModel\Seller\Collection::class);
        $this->wkHelper->expects($this->once())->method('getSellerDataBySellerId')->willReturn($sellerCollectionMock);

        $addressMock = $this->createMock(Address::class);
        $orderMock->expects($this->atLeastOnce())->method('getShippingAddress')->willReturn($addressMock);
        $addressMock->expects($this->once())->method('getFirstname')->willReturn('firstname');
        $addressMock->expects($this->once())->method('getLastname')->willReturn('lastname');
        $addressMock->expects($this->once())->method('getStreet')->willReturn(['street', '65']);
        $addressMock->expects($this->once())->method('getPostCode')->willReturn('123');
        $addressMock->expects($this->once())->method('getCountryId')->willReturn('qwe');

        $paymentMock = $this->getMockBuilder(OrderPaymentInterface::class)
            ->setMethods(['getMethodInstance'])
            ->getMockForAbstractClass();

        $orderMock->expects($this->once())->method('getPayment')->willReturn($paymentMock);

        $methodMock = $this->createMock(\Magento\Payment\Model\MethodInterface::class);
        $paymentMock->expects($this->once())->method('getMethodInstance')->willReturn($methodMock);

        $storeMock = $this->createMock(Store::class);
        $this->storeManager->expects($this->atLeastOnce())->method('getStore')->willReturn($storeMock);

        $this->transportBuilder->expects($this->once())->method('setTemplateIdentifier')->willReturnSelf();
        $this->transportBuilder->expects($this->once())->method('setTemplateOptions')->willReturnSelf();
        $this->transportBuilder->expects($this->once())->method('setTemplateVars')->willReturnSelf();
        $this->transportBuilder->expects($this->once())->method('setFrom')->willReturnSelf();

        $transportMock = $this->createMock(TransportInterface::class);
        $this->transportBuilder->expects($this->once())->method('getTransport')->willReturn($transportMock);

        $this->testObject->preparePhysicalProductEmail(1);
    }

    
    /**
     * @throws ReflectionException
     */
    public function testGetStore()
    {
        $storeMock = $this->createMock(Store::class);
        $this->storeManager->expects($this->once())->method('getStore')->willReturn($storeMock);

        $this->assertInstanceOf(Store::class, $this->testObject->getStore());
    }

    /**
     * @throws ReflectionException
     */
    public function testGetConfig()
    {
        $storeMock = $this->createMock(Store::class);
        $this->storeManager->expects($this->once())->method('getStore')->willReturn($storeMock);
        $this->scopeConfig->expects($this->once())->method('getValue')->willReturn('value');

        $this->testObject->getConfig('path');
    }
}
