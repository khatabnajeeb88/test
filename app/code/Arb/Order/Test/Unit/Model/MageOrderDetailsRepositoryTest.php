<?php 
/**
 * This file consist of PHPUnit test case for Order History Repository
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Model;

use Arb\Order\Model\MageOrderDetails;
use Arb\Order\Model\MageOrderDetailsRepository;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Order\Api\Data\MageOrderDetailsInterface;
use Arb\Order\Api\MageOrderDetailsRepositoryInterface;
use Arb\Order\Model\ResourceModel\MageOrderDetailsResource;
use Arb\Order\Model\ResourceModel\MageOrderDetails\Collection;
use Arb\Order\Model\ResourceModel\MageOrderDetails\CollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\Search\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Arb\Order\Model\MageOrderDetailsFactory;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Exception;
use ReflectionException;

/**
 * @covers \Arb\Order\Model\MageOrderDetailsRepository
 */
class MageOrderDetailsRepositoryTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|MageOrderDetailsResource
     */
    private $orderDetailResourceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionProcessor
     */
    private $collectionProcessorMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|MageOrderDetailsRepository
     */
    private $testedObject;

    /**
     * Setting up tested object
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->orderDetailResourceMock = $this->createMock(MageOrderDetailsResource::class);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->collectionProcessorMock = $this->createMock(CollectionProcessorInterface::class);
        $this->searchResultsInterfaceFactoryMock = $this->createMock(SearchResultsInterfaceFactory::class);

        $this->testedObject = $objectManager->getObject(MageOrderDetailsRepository::class, [
            'resource' => $this->orderDetailResourceMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'collectionProcessor' => $this->collectionProcessorMock
        ]);
    }

    /**
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws ReflectionException
     */
    public function testSave()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|MageOrderDetails $orderDetailsMock */
        $orderDetailsMock = $this->createMock(MageOrderDetails::class);

        $this->orderDetailResourceMock
            ->expects($this->once())
            ->method('save')
            ->willReturn($orderDetailsMock);

        $save = $this->testedObject->save($orderDetailsMock);
        $this->assertEquals($orderDetailsMock, $save);
    }

    /**
     * Test save() method for throwing an exception
     *
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws ReflectionException
     */
    public function testSaveExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|MageOrderDetails $orderDetailsMock */
        $orderDetailsMock = $this->createMock(MageOrderDetails::class);

        $this->orderDetailResourceMock
            ->expects($this->once())
            ->method('save')
            ->willThrowException(new Exception('Save error'));

        $this->expectException(CouldNotSaveException::class);

        $this->testedObject->save($orderDetailsMock);
    }


    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetByOrderId()
    {
         /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('addFilter')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        $this->collectionProcessorMock
            ->expects($this->once())
            ->method('process');

        $collectionMock
            ->expects($this->once())
            ->method('getSize')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|MageOrderDetails $orderDetailsMock */
        $orderDetailsMock = $this->createMock(MageOrderDetails::class);

        $collectionMock
            ->expects($this->once())
            ->method('getFirstItem')
            ->willReturn($orderDetailsMock);

        $testGetByOrderId = $this->testedObject->getByOrderId(1);
        $this->assertInstanceOf(MageOrderDetails::class, $testGetByOrderId);
    }
}
