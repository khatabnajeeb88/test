<?php
/**
 * This file consist of PHPUnit test case for OrderHistory
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Model;

use Arb\Order\Model\OrderHistory;
use Arb\Order\Model\ResourceModel\OrderHistoryResource;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Order\Model\OrderHistory
 */
class OrderHistoryTest extends TestCase
{
    private const ORDER_HISTORY_ORDER_ID = 5;
    private const ORDER_HISTORY_STATUS = 'status';
    private const ORDER_HISTORY_CREATED_AT = '2020-07-06 11:14:31';

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderHistory
     */
    private $testedObject;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $resource = $this->createMock(OrderHistoryResource::class);
        $resource->method('save')->willReturnSelf();

        $this->testedObject = $objectManager->getObject(OrderHistory::class, [
            '_resource' => $resource
        ]);
    }

    /**
     * Test method getOrderId and setOrderId
     *
     * @return void
     */
    public function testGetSetOrderId()
    {
        $this->assertEquals(
            $this->testedObject->setOrderId(self::ORDER_HISTORY_ORDER_ID)->getOrderId(),
            self::ORDER_HISTORY_ORDER_ID
        );
    }

    /**
     * Test method getStatus and setStatus
     *
     * @return void
     */
    public function testGetSetStatus()
    {
        $this->assertEquals(
            $this->testedObject->setStatus(self::ORDER_HISTORY_STATUS)->getStatus(),
            self::ORDER_HISTORY_STATUS
        );
    }

    /**
     * Test method getCreatedAt and setCreatedAt
     *
     * @return void
     */
    public function testGetSetCreatedAt()
    {
        $this->assertEquals(
            $this->testedObject->setCreatedAt(self::ORDER_HISTORY_CREATED_AT)->getCreatedAt(),
            self::ORDER_HISTORY_CREATED_AT
        );
    }
}
