<?php
/**
 * This file consist of PHPUnit test case for class Order
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Test\Unit\Model;

use Arb\Order\Model\Order\Email\OrderEmailSender;
use Arb\Order\Model\OrderHistory;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Customer;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Sales\Model\Order\ItemFactory;
use Arb\EsbNotifications\Helper\Data as EsbHelper;
use Temando\Shipping\ViewModel\DataProvider\OrderAddress;
use Webkul\Marketplace\Model\Orders;
use Webkul\MpAdvancedCommission\Helper\Data as MpHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Webkul\Marketplace\Helper\Data as WebkulHelper;
use Arb\Order\Model\OrdertokenFactory;
use Arb\ArbPayment\Model\Encryption;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use Arb\OTPLogin\Model\OTP;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\Framework\Webapi\Request;
use Magento\Sales\Model\OrderRepositoryFactory;
use Webkul\MpRmaSystem\Helper\Data;
use Zend\Uri\Uri;
use Magento\Framework\Webapi\ServiceInputProcessor;
use \Arb\Order\Model\Order as ArbOrder;
use Webkul\Marketplace\Model\OrdersRepository;
use Magento\Customer\Model\CustomerFactory;
use Arb\Order\Api\OrderHistoryRepositoryInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;

/**
 * test order model
 */
class OrderTest extends TestCase
{
    /**
     * @param \Magento\Sales\Model\Order
     */
    protected $orderFactory;

    /**
     * @param \Magento\Sales\Model\Order\ItemFactory
     */
    protected $itemFactory;

    /**
     * @param \Webkul\Marketplace\Model\OrdersRepository
     */
    private $orderHistoryRepository;

    /**
     * @param \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;

    /**
     * @param \Arb\Order\Api\OrderHistoryRepositoryInterface
     */
    private $ordersRepository;

    /**
     * @param Request
     */
    private $requestMock;

    /**
     * @param OrderEmailSender
     */
    private $orderEmailSenderMock;

    /**
     * @param Data
     */
    private $rmaHelperMock;

    /**
     * @param TimezoneInterface
     */
    private $timezoneMock;

    /**
     * @param
     */
    private $_scopeConfigMock;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->order = $this->getMockBuilder(\Magento\Sales\Model\Order::class)
                        ->disableOriginalConstructor()
                        ->setMethods(['create','getCollection','addAddressFields','addAttributeToFilter',
                'getFirstItem','setBaseTotalDue','setCustomerEmail','setGrandTotal',
                'setCreatedAt','setIncrementId','setBaseGrandTotal','setItems',
                "setTotalItemCount","load","getId","getBaseGrandTotal","getPayment",
                "getBaseTotalDue","getCustomerEmail","getGrandTotal","getCreatedAt",
                "getIncrementId","getTotalItemCount","getItems","getData",
                "getCustomerFirstname","getCustomerLastname","getAllItems",
                "getStore","getCode", "getBillingAddress"
                        ])
                        ->getMock();
        $this->_storeManagerMock = $this->getMockBuilder(StoreManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->OTPMock = $this->getMockBuilder(OTP::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->sellerCollectionMock = $this->getMockBuilder(\Webkul\Marketplace\Model\ResourceModel\Seller\Collection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->ordertokenFactory = $this->getMockBuilder(OrdertokenFactory::class)
            ->setMethods([
                "setOrderId",
                "setTelephone",
                "setToken",
                "setCreatedAt",
                "save",
                "create",
                "load",
                "getOrderId",
                "getCreatedAt",
                "getTelephone"
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $this->tokenFactoryMock = $this->getMockBuilder(TokenFactory::class)
            ->setMethods([
                "create",
                "load",
                "getCustomerId",
                "getRevoked"
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $this->ordertokenFactory->method("create")->willReturnSelf();
        $this->dateTime = $this->getMockBuilder(DateTime::class)
            ->setMethods(["date"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->orderFactory = $this->getMockBuilder(\Magento\Sales\Model\OrderFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create','getCollection','addAddressFields','addAttributeToFilter',
                'getFirstItem','setBaseTotalDue','setCustomerEmail','setGrandTotal',
                'setCreatedAt','setIncrementId','setBaseGrandTotal','setItems',
                "setTotalItemCount","load","getId","getBaseGrandTotal","getPayment",
                "getBaseTotalDue","getCustomerEmail","getGrandTotal","getCreatedAt",
                "getIncrementId","getTotalItemCount","getItems","getData",
                "getCustomerFirstname","getCustomerLastname","getAllItems",
                "getStore","getCode","addFieldToFilter", 'getIsVirtual'
                ])
            ->getMock();
        $this->orderFactory->expects($this->any())->method('getStore')->willReturnSelf();
        $this->orderFactory->method('create')->willReturnSelf();
        $this->orderFactory->method('getCollection')->willReturnSelf();
        $this->orderFactory->method('addAddressFields')->willReturnSelf();
        $this->orderFactory->method('addAttributeToFilter')->willReturnSelf();
        $this->orderFactory->method('addFieldToFilter')->willReturnSelf();
        $this->orderFactory->method('getFirstItem')->willReturn($this->order);
        $this->itemMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Item::class)
                        ->disableOriginalConstructor()
                        ->setMethods(['getVouchers',"getData","getProduct","getName"])
                        ->getMock();
        $this->itemMock->method("getVouchers")->willReturn("teste");
        $this->itemMock->method("getProduct")->willReturnSelf();
        $this->itemMock->method("getName")->willReturn("testProduct");
        $this->itemFactory = $this->getMockBuilder(\Magento\Sales\Model\Order\ItemFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([ 'create'])
            ->getMock();
        $this->encryptionMock = $this->getMockBuilder(Encryption::class)
            ->disableOriginalConstructor()
            ->setMethods(['decryptAES',"encryptAES"])
            ->getMock();
        $this->itemFactory->method('create')->willReturn(
            $this->itemMock
        );

        $this->esbHelper = $this->getMockBuilder(EsbHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['sendEmailNotification'])
            ->getMock();
        $this->mpHelper = $this->getMockBuilder(MpHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSellerIdByItem'])
            ->getMock();
        $this->mpHelper->method("getSellerIdByItem")->willReturn("2");
        $this->_customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                "getById",
                                "getCustomAttribute",
                                "getValue",
                                "getEmail",
                                "getId"
                            ])
                            ->getMockForAbstractClass();
        $this->wpHelper = $this->getMockBuilder(WebkulHelper::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'getSellerDataBySellerId','getData','getSellerCollection'
                            ])
                            ->getMock();
        $this->requestMock = $this->getMockBuilder(Request::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'getHeader'
                            ])
                            ->getMock();
        $this->wpHelper->expects($this->any())->method("getSellerDataBySellerId")->willReturnSelf();
        $this->wpHelper->expects($this->any())->method("getSellerCollection")->willReturn($this->sellerCollectionMock);
        $this->sellerCollectionMock->expects($this->any())->method("addFieldToFilter")->willReturnSelf();
        $this->sellerCollectionMock->expects($this->any())->method("getItems")->willReturn([$this->sellerCollectionMock]);
        $this->wpHelper->expects($this->any())->method("getData")->willReturn([
            "0"=>[
                "shop_title"=>"test"
                ]
        ]);
        $this->ordersRepository = $this->createMOck(OrdersRepository::class);
        $this->customerFactory = $this->createMock(CustomerFactory::class);
        $this->orderHistoryRepository = $this->createMock(OrderHistoryRepositoryInterface::class);

        $this->orderFactory->method('create')->willReturnSelf();
        $this->orderFactory->method('load')->willReturnSelf();
        $this->orderFactory->method('getPayment')->willReturnSelf();
        $this->orderFactory->method('getData')->willReturn(["last_trans_id"=>"dslgase",
                                                        "arb_payment_id"=>"26986986",
                                                        "arb_ref_id"=>"sagdjgwe"
                                                        ]);
        $this->_serializationMock = $this->getMockBuilder(Json::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'unserialize'
                            ])
                            ->getMock();
        
        $this->_encryptorInterfaceMock = $this->createMock(EncryptorInterface::class);
        $this->_priceHelperMock = $this->createMock(PriceHelper::class);
        $this->_scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->encryptionMock->method("decryptAES")->willReturn("teste");
        $this->encryptionMock->method("encryptAES")->willReturn("encryptAEStestebstes");
        $this->orderRepositoryFactory = $this->getMockBuilder(OrderRepositoryFactory::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'create','getList'
                            ])
                            ->getMock();

        $this->requestMock = $this->createMock(Request::class);

        $this->urlParse = $this->getMockBuilder(Uri::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'setQuery','getQueryAsArray'
                            ])
                            ->getMock();
        $this->serviceInputProcessor = $this->getMockBuilder(ServiceInputProcessor::class)
                            ->disableOriginalConstructor()
                            ->setMethods([
                                'convertValue'
                            ])
                            ->getMock();

        $this->orderEmailSenderMock = $this->createMock(OrderEmailSender::class);
        $this->rmaHelperMock = $this->createMock(Data::class);
        $this->timezoneMock = $this->createMock(TimezoneInterface::class);

        $this->testObject = new ArbOrder(
            $this->orderFactory,
            $this->itemFactory,
            $this->esbHelper,
            $this->mpHelper,
            $this->_customerRepositoryMock,
            $this->_serializationMock,
            $this->_storeManagerMock,
            $this->_encryptorInterfaceMock,
            $this->_priceHelperMock,
            $this->wpHelper,
            $this->encryptionMock,
            $this->_scopeConfigMock,
            $this->ordertokenFactory,
            $this->dateTime,
            $this->OTPMock,
            $this->tokenFactoryMock,
            $this->requestMock,
            $this->orderRepositoryFactory,
            $this->urlParse,
            $this->serviceInputProcessor,
            $this->ordersRepository,
            $this->customerFactory,
            $this->orderHistoryRepository,
            $this->orderEmailSenderMock,
            $this->rmaHelperMock,
            $this->timezoneMock
        );
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testOrderNotFound()
    {
        $increment_id = "000000005";
        $telephone = "0123456789";
        $this->testObject->getOrder($increment_id, $telephone);
        $orderId = 3;
        $this->testObject->sendMail($orderId);
    }

    public function testGetOrder()
    {
        $this->orderFactory->method('setBaseTotalDue')->will(
            $this->returnSelf()
        );

        $this->orderFactory->method('setCustomerEmail')->will(
            $this->returnSelf()
        );

        $this->orderFactory->method('setGrandTotal')->will(
            $this->returnSelf()
        );

        $this->orderFactory->method('setCreatedAt')->will(
            $this->returnSelf()
        );

        $this->orderFactory->method('setIncrementId')->will(
            $this->returnSelf()
        );

        $this->orderFactory->method('setTotalItemCount')->will(
            $this->returnSelf()
        );
        
        $this->orderFactory->method('setBaseGrandTotal')->will(
            $this->returnSelf()
        );

        $this->orderFactory->method('setItems')->will(
            $this->returnSelf()
        );

        $this->order->method('getId')->will(
            $this->returnValue(3)
        );
        $this->order->method('getIncrementId')->will(
            $this->returnValue(3)
        );
        $this->ordertokenFactory->method("setOrderId")->willReturnSelf();
        $this->ordertokenFactory->method("setTelephone")->willReturnSelf();
        $this->ordertokenFactory->method("setToken")->willReturnSelf();
        $this->ordertokenFactory->method("setCreatedAt")->willReturnSelf();
        $this->ordertokenFactory->method("save")->willReturnSelf();
        $items = [$this->itemMock];

        $this->order->method('getItems')->willReturn(
            $items
        );
        $this->_serializationMock->method('unserialize')->willReturn(
            ["0"=>
                ["orderId"=>"000000005","telephone"=>"0123456789","reqType"=>"orderHistory"]
            ]
        );
        $increment_id = "000000005";
        $telephone = "0123456789";
        $this->assertInternalType('array', $this->testObject->getOrder($increment_id));
    }

    /**
     * test order data get it without telephone
     *
     * @return void
     */
    public function testGetOrderWithoutTelephone()
    {
        $this->orderFactory->method('getId')->will(
            $this->returnValue(30)
        );
        $items = [$this->itemMock];

        $this->orderFactory->method('getItems')->willReturn(
            $items
        );
        $increment_id = "000000030";
        $telephone = "";
        $this->expectException(LocalizedException::class);
        $this->assertInternalType('array', $this->testObject->getOrder($increment_id, $telephone));
    }

    /**
     * test order detail api with confimation
     *
     * @return void
     */
    public function testGetOrderConfimation()
    {
        $this->_scopeConfigMock->method('getValue')->willReturn(10);

        $this->order->method('getId')->will(
            $this->returnValue(3)
        );
        $this->order->method('getIncrementId')->will(
            $this->returnValue(3)
        );
        $items = [$this->itemMock];

        $this->order->method('getItems')->willReturn(
            $items
        );

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()
            ->setMethods(['getTypeId', 'getImage'])
            ->getMock();

        $this->itemMock->method("getProduct")->willReturn($productMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderAddress $orderAddressMock */
        $orderAddressMock = $this->getMockBuilder(OrderAddress::class)
            ->disableOriginalConstructor()
            ->setMethods(['getData', 'getExtensionAttributes', 'getVatRequestSuccess', 'getVatRequestId',
                'getVatRequestDate', 'getVatIsValid', 'getVatId', 'getTelephone', 'getSuffix', 'getStreet',
                'getRegionId', 'getRegion', 'getPrefix', 'getPostCode', 'getParentId', 'getMiddleName', 'getLastName',
                'getFirstName', 'getFax', 'getEntityId', 'getEmail', 'getCustomerId', 'getCustomerAddressId',
                'getCountryId', 'getCompany', 'getCity', 'getAddressType'])
            ->getMock();

        $this->order->method('getBillingAddress')->willReturn($orderAddressMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        $this->ordersRepository->method('getByOrderId')->willReturn([$merchantOrderMock]);

        /** @var PHPUnit_Framework_MockObject_MockObject|Customer $customerMock */
        $customerMock = $this->createMock(Customer::class);

        $this->customerFactory->method('create')->willReturn($customerMock);

        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getId")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("2168963286");

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $orderHistoryListMock */
        $orderHistoryListMock = $this->createMock(SearchResultsInterface::class);

        $this->orderHistoryRepository->method('getByOrderId')->willReturn($orderHistoryListMock);
        $orderHistoryListMock->method('getItems')->willReturn([]);

        $this->_serializationMock->method('unserialize')->willReturn(
            ["0"=>
                ["orderId"=>"000000005","reqType"=>"orderConfirmation"]
            ]
        );
        $increment_id = "000000005";
        $this->testObject->getOrder($increment_id);
    }

    /**
     * test order detail api with OTP
     *
     * @return void
     */
    public function testGetOrderOtp()
    {
        $this->order->method('getId')->will(
            $this->returnValue(3)
        );
        $this->order->method('getIncrementId')->will(
            $this->returnValue(3)
        );
        $items = [$this->itemMock];
        $this->ordertokenFactory->method("load")->willReturnSelf();
        $this->ordertokenFactory->method("getOrderId")->willReturn("000000005");
        $this->ordertokenFactory->method("getCreatedAt")->willReturn(date("Y-m-d H:i:s"));
        $this->dateTime->method("date")->willReturn(date("Y-m-d H:i:s"));
        $this->ordertokenFactory->method("getTelephone")->willReturn("298612896986");
        $this->order->method('getItems')->willReturn(
            $items
        );

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderAddress $orderAddressMock */
        $orderAddressMock = $this->getMockBuilder(OrderAddress::class)
            ->disableOriginalConstructor()
            ->setMethods(['getData', 'getExtensionAttributes', 'getVatRequestSuccess', 'getVatRequestId',
                'getVatRequestDate', 'getVatIsValid', 'getVatId', 'getTelephone', 'getSuffix', 'getStreet',
                'getRegionId', 'getRegion', 'getPrefix', 'getPostCode', 'getParentId', 'getMiddleName', 'getLastName',
                'getFirstName', 'getFax', 'getEntityId', 'getEmail', 'getCustomerId', 'getCustomerAddressId',
                'getCountryId', 'getCompany', 'getCity', 'getAddressType'])
            ->getMock();

        $this->order->method('getBillingAddress')->willReturn($orderAddressMock);

        $this->_serializationMock->method('unserialize')->willReturn(
            ["0"=>
                ["token"=>"000000005","otp"=>"1234","reqType"=>"otp"]
            ]
        );
        $increment_id = "000000005";
        $this->testObject->getOrder($increment_id);
    }

     /**
      * test order detail api for logged in user
      *
      * @return void
      */
    public function testGetOrderLoggedInHistory()
    {
        $this->_scopeConfigMock->method('getValue')->willReturn(10);
        $this->order->method('getId')->will(
            $this->returnValue(3)
        );
        $this->order->method('getIncrementId')->will(
            $this->returnValue(3)
        );
        $items = [$this->itemMock];
        $this->tokenFactoryMock->method("load")->willReturnSelf();
        $this->tokenFactoryMock->method("create")->willReturnSelf();
        $this->tokenFactoryMock->method("getCustomerId")->willReturn(12);
        $this->tokenFactoryMock->method("getRevoked")->willReturn("0");
        $this->order->method('getItems')->willReturn(
            $items
        );
        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getEmail")->willReturn("test@example.com");
        $this->_serializationMock->method('unserialize')->willReturn(
            ["0"=>
                ['orderId' => '3000000152',
                "reqType"=>"loginOrderHistory"
                ]
            ]
        );

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()
            ->setMethods(['getTypeId', 'getImage'])
            ->getMock();

        $this->itemMock->method("getProduct")->willReturn($productMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderAddress $orderAddressMock */
        $orderAddressMock = $this->getMockBuilder(OrderAddress::class)
            ->disableOriginalConstructor()
            ->setMethods(['getData', 'getExtensionAttributes', 'getVatRequestSuccess', 'getVatRequestId',
                'getVatRequestDate', 'getVatIsValid', 'getVatId', 'getTelephone', 'getSuffix', 'getStreet',
                'getRegionId', 'getRegion', 'getPrefix', 'getPostCode', 'getParentId', 'getMiddleName', 'getLastName',
                'getFirstName', 'getFax', 'getEntityId', 'getEmail', 'getCustomerId', 'getCustomerAddressId',
                'getCountryId', 'getCompany', 'getCity', 'getAddressType'])
            ->getMock();

        $this->order->method('getBillingAddress')->willReturn($orderAddressMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        $this->ordersRepository->method('getByOrderId')->willReturn([$merchantOrderMock]);

        /** @var PHPUnit_Framework_MockObject_MockObject|Customer $customerMock */
        $customerMock = $this->createMock(Customer::class);

        $this->customerFactory->method('create')->willReturn($customerMock);

        $this->_customerRepositoryMock->expects($this->any())->method("getId")->willReturnSelf();
        
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $orderHistoryListMock */
        $orderHistoryListMock = $this->createMock(SearchResultsInterface::class);

        $this->orderHistoryRepository->method('getByOrderId')->willReturn($orderHistoryListMock);
        $orderHistoryListMock->method('getItems')->willReturn([]);

        $this->requestMock
        ->method("getHeader")
        ->willReturn("bearer znf6lyqzm4056ov8mxewsmv5v2ny6itk");
        $increment_id = "3000000152";
        $this->testObject->getOrder($increment_id);
    }

    /**
     * test order detail api for logged in user
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function testGetOrderLoggedInHistoryErrorOne()
    {
        $this->order->method('getId')->will(
            $this->returnValue(3)
        );
        $this->order->method('getIncrementId')->will(
            $this->returnValue(3)
        );
        $items = [$this->itemMock];
        $this->tokenFactoryMock->method("load")->willReturnSelf();
        $this->tokenFactoryMock->method("create")->willReturnSelf();
        $this->tokenFactoryMock->method("getCustomerId")->willReturn(12);
        $this->tokenFactoryMock->method("getRevoked")->willReturn("0");
        $this->order->method('getItems')->willReturn(
            $items
        );

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()
            ->setMethods(['getTypeId', 'getImage'])
            ->getMock();

        $this->itemMock->method("getProduct")->willReturn($productMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderAddress $orderAddressMock */
        $orderAddressMock = $this->getMockBuilder(OrderAddress::class)
            ->disableOriginalConstructor()
            ->setMethods(['getData', 'getExtensionAttributes', 'getVatRequestSuccess', 'getVatRequestId',
                'getVatRequestDate', 'getVatIsValid', 'getVatId', 'getTelephone', 'getSuffix', 'getStreet',
                'getRegionId', 'getRegion', 'getPrefix', 'getPostCode', 'getParentId', 'getMiddleName', 'getLastName',
                'getFirstName', 'getFax', 'getEntityId', 'getEmail', 'getCustomerId', 'getCustomerAddressId',
                'getCountryId', 'getCompany', 'getCity', 'getAddressType'])
            ->getMock();

        $this->order->method('getBillingAddress')->willReturn($orderAddressMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        $this->ordersRepository->method('getByOrderId')->willReturn([$merchantOrderMock]);

        /** @var PHPUnit_Framework_MockObject_MockObject|Customer $customerMock */
        $customerMock = $this->createMock(Customer::class);

        $this->customerFactory->method('create')->willReturn($customerMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $orderHistoryListMock */
        $orderHistoryListMock = $this->createMock(SearchResultsInterface::class);

        $this->orderHistoryRepository->method('getByOrderId')->willReturn($orderHistoryListMock);
        $orderHistoryListMock->method('getItems')->willReturn([]);

        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getEmail")->willReturn("test@example.com");
        $this->_serializationMock->method('unserialize')->willReturn(
            ["0"=>
                ['orderId' => '3000000152',
                "reqType"=>"loginOrderHistory"
                ]
            ]
        );
        $this->requestMock
        ->method("getHeader")
        ->willReturn("znf6lyqzm4056ov8mxewsmv5v2ny6itk");
        $increment_id = "3000000152";
        $this->testObject->getOrder($increment_id);
    }

    /**
     * test order detail api for logged in user
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function testGetOrderLoggedInHistoryErrorTwo()
    {
        $this->order->method('getId')->will(
            $this->returnValue(3)
        );
        $this->order->method('getIncrementId')->will(
            $this->returnValue(3)
        );
        $items = [$this->itemMock];
        $this->tokenFactoryMock->method("load")->willReturnSelf();
        $this->tokenFactoryMock->method("create")->willReturnSelf();
        $this->tokenFactoryMock->method("getCustomerId")->willReturn(12);
        $this->tokenFactoryMock->method("getRevoked")->willReturn("0");
        $this->order->method('getItems')->willReturn(
            $items
        );
        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getEmail")->willReturn("test@example.com");
        $this->_serializationMock->method('unserialize')->willReturn(
            ["0"=>
                ['orderId' => '3000000152',
                "reqType"=>"loginOrderHistory"
                ]
            ]
        );
        $this->requestMock
        ->method("getHeader")
        ->willReturn("test znf6lyqzm4056ov8mxewsmv5v2ny6itk");
        $increment_id = "3000000152";
        $this->testObject->getOrder($increment_id);
    }

    /**
     * test order detail api for logged in user
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function testGetOrderLoggedInHistoryErrorThree()
    {
        $this->order->method('getId')->will(
            $this->returnValue(3)
        );
        $this->order->method('getIncrementId')->will(
            $this->returnValue(3)
        );
        $items = [$this->itemMock];
        $this->tokenFactoryMock->method("load")->willReturnSelf();
        $this->tokenFactoryMock->method("create")->willReturnSelf();
        $this->tokenFactoryMock->method("getCustomerId")->willReturn(12);
        $this->tokenFactoryMock->method("getRevoked")->willReturn("0");
        $this->order->method('getItems')->willReturn(
            $items
        );
        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getEmail")->willReturn("test@example.com");
        $this->_serializationMock->method('unserialize')->willReturn(
            ["0"=>
                ['orderId' => '3000000152',
                "reqType"=>"loginOrderHistory"
                ]
            ]
        );
        $increment_id = "3000000152";
        $this->testObject->getOrder($increment_id);
    }

    /**
     * test send mail
     */
    public function testSendMail()
    {
        $orderId = 3;

        $this->orderFactory->method('getId')->will(
            $this->returnValue($orderId)
        );
        $items = [$this->itemMock];
        $this->itemMock->method("getData")->willReturn([
            "row_total"=>"120",
            "name"=>"Teste",
            "vouchers"=>"teste_vouchers"
        ]);
        $this->orderFactory->method('getIsVirtual')->willReturn(
            false
        );
        $this->orderFactory->method('getAllItems')->willReturn(
            $items
        );
        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("2168963286");
        $this->_serializationMock->method('unserialize')->willReturn(["voucher1"]);
        $this->orderFactory->expects($this->any())->method('getCode')->willReturn("ar_SA");

        $this->assertTrue($this->testObject->sendMail($orderId));
    }

    /**
     * test send maix exception
     */
    public function testSendMailExecption()
    {
        $orderId = 3;
        $this->orderFactory->method('getId')->will(
            $this->returnValue($orderId)
        );
        $items = [$this->itemMock];
        $this->itemMock->method("getData")->willReturn([
            "row_total"=>"120",
            "name"=>"Teste",
            "vouchers"=>"teste_vouchers"
        ]);
        $this->orderFactory->method('getAllItems')->willReturn(
            $items
        );
        $this->esbHelper->expects($this->any())
        ->method('sendEmailNotification')
        ->willThrowException(new \Exception("invalid"));
        $this->orderFactory->expects($this->any())->method('getCode')->willReturn("EN");
        $this->_customerRepositoryMock->expects($this->any())->method("getById")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getCustomAttribute")->willReturnSelf();
        $this->_customerRepositoryMock->expects($this->any())->method("getValue")->willReturn("2168963286");
        $this->_serializationMock->method('unserialize')->willReturn(["voucher1"]);
        $this->testObject->sendMail($orderId);
    }
    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testSendMailExecptionNoOrder()
    {
        $orderId = 3;
        $mockError = new \Magento\Framework\Phrase('Requested order doesn\'t exist');
        $mockClass = new \Magento\Framework\Exception\LocalizedException($mockError);
        $this->orderFactory->method('getId')->willThrowException($mockClass);
        $this->testObject->sendMail($orderId);
    }

    public function testGetOrderList()
    {
        $this->urlParse->method("setQuery")->willReturnSelf();
        $this->urlParse->method("getQueryAsArray")->willReturn(["searchCriteria"=>"beste"]);
        $this->orderRepositoryFactory->method("create")->willReturnSelf();
        $this->orderRepositoryFactory->method("getList")->willReturn(["teste"]);
        $this->testObject->getOrderList("testestebstestesawsjjgdsjjk");
    }
    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testGetOrderList1()
    {
        $this->urlParse->method("setQuery")->willReturnSelf();
        $this->urlParse->method("getQueryAsArray")->willReturn(["test"=>"beste"]);
        $this->testObject->getOrderList("testestebstestesawsjjgdsjjk");
    }
    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testGetOrderList2()
    {
        $this->urlParse->method("setQuery")->willReturnSelf();
        $this->testObject->getOrderList("testestebstestesawsjjgdsjjk");
    }
}
