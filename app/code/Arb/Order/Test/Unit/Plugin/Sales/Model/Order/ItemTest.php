<?php

namespace Arb\Order\Test\Unit\Plugin\Sales\Model\Order;

use Arb\Order\Plugin\Sales\Model\Order\Item;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Sales\Model\Order\Item as ItemModel;
use ReflectionException;

class ItemTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Item
     */
    private $testObject;

    /**
     * Setting up necessary mocks
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->testObject = $objectManager->getObject(Item::class, []);
    }

    /**
     * @throws ReflectionException
     */
    public function testAfterGetQtyToCancel()
    {
        $itemModelMock = $this->createMock(ItemModel::class);
        $itemModelMock->expects($this->once())->method('getQtyToShip')->willReturn(5);

        $this->assertSame($this->testObject->afterGetQtyToCancel($itemModelMock), 5);
    }
}
