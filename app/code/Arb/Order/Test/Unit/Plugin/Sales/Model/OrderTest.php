<?php

namespace Arb\Order\Test\Unit\Plugin\Sales\Model;

use Arb\Order\Plugin\Sales\Model\Order;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;
use Webkul\Marketplace\Api\Data\OrdersInterface;
use Webkul\Marketplace\Helper\Orders;
use Magento\Sales\Model\Order as ModelOrder;

class OrderTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Order
     */
    private $testObject;

    /**
     * @var Orders|PHPUnit_Framework_MockObject_MockObject
     */
    private $webkulOrdersMock;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->webkulOrdersMock = $this->createMock(Orders::class);

        $this->testObject = $objectManager->getObject(Order::class, [
            'webkulOrders' => $this->webkulOrdersMock
        ]);
    }

    /**
     * @dataProvider afterCanCancelProvider
     *
     * @param $status
     * @param $expectedOrderInfo
     * @param $shipmentId
     * @param $invoiceId
     * @param $result
     * @param $expectedResult
     *
     * @throws ReflectionException
     */
    public function testAfterCanCancel($status, $expectedOrderInfo, $shipmentId, $invoiceId, $result, $expectedResult)
    {
        $modelOrderMock = $this->createMock(ModelOrder::class);
        $modelOrderMock->expects($this->once())->method('getStatus')->willReturn($status);

        $merchantOrderMock= $this->getMockBuilder(OrdersInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getId',
                'setId',
                'getCreatedAt',
                'setCreatedAt',
                'getUpdatedAt',
                'setUpdatedAt',
                'getShipmentId',
                'getInvoiceId'
            ])->getMock();

        $this->webkulOrdersMock
            ->expects($this->exactly($expectedOrderInfo))
            ->method('getOrderinfo')
            ->willReturn($merchantOrderMock);

        $merchantOrderMock->method('getShipmentId')->willReturn($shipmentId);
        $merchantOrderMock->method('getShipmentId')->willReturn($invoiceId);

        $this->assertSame($this->testObject->afterCanCancel($modelOrderMock, $result), $expectedResult);
    }

    /**
     * @dataProvider afterCanCancelProvider
     *
     * @param $status
     * @param $expectedOrderInfo
     * @param $shipmentId
     * @param $invoiceId
     * @param $result
     * @param $expectedResult
     *
     * @throws ReflectionException
     */
    public function testAfterCanCancelError($status, $expectedOrderInfo, $shipmentId, $invoiceId, $result, $expectedResult)
    {
        $modelOrderMock = $this->createMock(ModelOrder::class);
        $modelOrderMock->expects($this->once())->method('getStatus')->willReturn($status);

        $this->webkulOrdersMock
            ->expects($this->exactly($expectedOrderInfo))
            ->method('getOrderinfo')
            ->willReturn(false);

        $this->assertSame($this->testObject->afterCanCancel($modelOrderMock, $result), $result);
    }
    /**
     * @return array
     */
    public function afterCanCancelProvider()
    {
        return [
            [                   // Scenario 0: status is different than 'processing', result is true, expected true
                'something',    // status
                0,              // expected getOrderinfo method execution number
                null,           // shipmentId
                null,           // invoiceId
                true,           // result passed to tested method
                true            // expected method result
            ],
            [                   // Scenario 1: status is different than 'processing', result is false, expected false
                'something',
                0,
                null,
                null,
                false,
                false
            ],
            [                   // Scenario 2: status is 'processing', shipmentId is different than 0, expected false
                'processing',
                1,
                '1',
                null,
                false,
                false
            ],
            [                   // Scenario 3: status is 'processing', shipmentId equals 0, invoiceId is different than 0, expected true
                'processing',
                1,
                '0',
                '1',
                false,
                true
            ],
            [                   // Scenario 4: status is 'processing', shipmentId equals 0, invoiceId equals 0, expected true
                'processing',
                1,
                '0',
                '0',
                true,
                true
            ],
        ];
    }

     public function testAfterSave()
    {
        $modelOrderMock = $this->createMock(ModelOrder::class);
        $afterSave = $this->testObject->afterSave($modelOrderMock,$modelOrderMock);
        $this->assertNull($afterSave);
    }
    
    public function testAfterLoad()
    {
        $modelOrderMock = $this->createMock(ModelOrder::class);
        $afterSave = $this->testObject->afterLoad($modelOrderMock,$modelOrderMock);
        $this->assertNull($afterSave);
    }
}
