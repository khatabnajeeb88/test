<?php
/**
 * This file consist of PHPUnit test case for class OrderRepositoryPlugin to get API data of all order list
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Test\Unit\Plugin\Block\Widget\Button;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Exception;
use ReflectionException;
use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Backend\Block\Widget\Button\ButtonList;
use Arb\Order\Plugin\Block\Widget\Button\Toolbar;
use Magento\Sales\Block\Adminhtml\Order\View;

/**
 * @covers \Arb\Order\Plugin\Block\Widget\Button\Toolbar
 */
class ToolbarTest extends TestCase
{
    /**
     * test for product observer
     *
     * @return void
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->toolbarContext = $this->createMock(ToolbarContext::class);
        $this->buttonList = $this->createMock(ButtonList::class);
        $this->abstractBlock = $this->createMock(AbstractBlock::class);
        $this->view = $this->createMock(View::class);
        $this->testObject = $this->objectManager->getObject(
            Toolbar::class,[]
        );
    }

    public function testBeforePushButtons()
    {
        $this->testObject->beforePushButtons($this->toolbarContext, $this->view, $this->buttonList);
    }
    
    public function testBeforePushButtonsNotInstance()
    {
        $this->testObject->beforePushButtons($this->toolbarContext, $this->abstractBlock, $this->buttonList);
    }
}
