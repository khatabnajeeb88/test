<?php
/**
 * This file consist of PHPUnit test case for class OrderRepositoryPlugin to get API data of all order list
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Test\Unit\Plugin;

use Arb\Order\Helper\OrderExtData;
use PHPUnit\Framework\TestCase;
use Magento\Sales\Model\Order;
use Arb\Order\Plugin\OrderRepositoryPlugin;
use Magento\Sales\Api\Data\OrderExtension;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory as ProductRepository;
use Magento\Catalog\Model\ProductRepository as Product;
use Arb\Order\Api\Data\MageOrderDetailsInterface;
use Arb\Order\Api\MageOrderDetailsRepositoryInterface;
use Arb\Order\Model\MageOrderDetailsFactory;
use Exception;
use ReflectionException;
/**
 * @covers \Arb\Order\Plugin\OrderRepositoryPlugin
 */
class OrderRepositoryPluginTest extends TestCase
{
    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $productRepository;

    /**
     * @var OrderExtensionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $subjectMock;

    /**
     * @var OrderExtensionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $orderMock;

    /**
     * @var OrderExtensionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $orderExtensionInterface;

    /**
     * @var OrderExtensionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $extensionAttributeMock;

    /**
     * @var OrderExtensionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $itemOneMock;

    /**
     * @var OrderExtensionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $orderExtensionFactoryMock;

    /**
     * @var OrderExtensionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $extensionFactory;

    /**
     * Object to test
     *
     * @var OrderRepositoryPlugin
     */
    private $plugin;



    /**
     * test for product observer
     *
     * @return void
     */
    public function setUp()
    {
        $this->mageOrderDetailsRepositoryInterfaceMock = $this->createMock(MageOrderDetailsRepositoryInterface::class);
        $this->mageOrderDetailsFactoryMock = $this->createMock(MageOrderDetailsFactory::class);
       $this->resultMock = $this->getMockBuilder(Order::class)
            ->setMethods(['getId', 'getCompletedAt'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->productRepository = $this->getMockBuilder(ProductRepository::class)
            ->setMethods([
                'getExtensionAttributes',
                'setExtensionAttributes',
                'create',
                'getById',
                'getThumbnail'
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $this->subjectMock = $this->getMockForAbstractClass(OrderRepositoryInterface::class);
        $this->orderExtMock = $this->getMockBuilder(OrderExtData::class)
        ->setMethods(
            [
            'loadCompletedAt','updateCompletedAt'
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->orderMock = $this->getMockBuilder(OrderSearchResultInterface::class)
            ->setMethods([
                'getExtensionAttributes',
                'setExtensionAttributes',
                "getAllItems",
                "setProductImage",
                "getItems"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->orderExtensionInterface = $this->getMockBuilder(\Magento\Sales\Api\Data\OrderExtensionInterface::class)
            ->setMethods([
                'getExtensionAttributes',
                'setExtensionAttributes',
                "getAllItems",
                "setProductImage"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->extensionAttributeMock = $this->getMockBuilder(OrderExtension::class)
            ->setMethods([
                "setProductImage"
            ])
            ->disableOriginalConstructor()
            ->getMock();

        $this->itemOneMock = $this->getMockBuilder(\Magento\Sales\Model\ResourceModel\Order\Item::class)
            ->setMethods(
                [
                'getProductId',
                'getExtensionAttributes',
                'setExtensionAttributes'
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderExtensionFactoryMock = $this->getMockBuilder(OrderExtensionFactory::class)
            ->setMethods(
                [
                'create',
                "setProductImage",
                'getExtensionAttributes',
                'setExtensionAttributes'
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();
        $this->salesOrderMock = $this->getMockBuilder(Order::class)->disableOriginalConstructor()->getMock();
        $this->productRepository->expects($this->any())->method('create')->willReturnSelf();
        $this->productRepository->expects($this->any())->method('getById')->willReturnSelf();
        $this->orderMock->expects($this->any())->method("getItems")->willReturn([$this->salesOrderMock]);
        $this->salesOrderMock->expects($this->any())->method("getAllItems")->willReturn([$this->itemOneMock]);
        $this->salesOrderMock->expects($this->any())->method('getExtensionAttributes')->willReturn($this->orderExtensionInterface);
        $this->orderExtensionInterface->expects($this->any())->method('setProductImage')->willReturn($this->orderExtensionInterface);
        
        $this->orderExtMock->expects($this->any())->method('loadCompletedAt')->willReturn($this->salesOrderMock);
        $this->plugin = new OrderRepositoryPlugin(
            $this->productRepository,
            $this->orderExtensionFactoryMock,
            $this->orderExtMock
        );
    }

    /**
     * test execute function
     *
     * @return void
     */
    public function testAfterGet()
    {
        $this->productRepository->expects($this->any())->method('getThumbnail')->willReturn("test.jpg");
        $this->assertInstanceOf(OrderSearchResultInterface::class,$this->plugin->afterGetList($this->subjectMock, $this->orderMock));
    }

    public function testAfterGetException()
    {

        $this->productRepository->expects($this->any())->method('create')->willReturnSelf();
        $this->productRepository->expects($this->any())
        ->method('getById')
        ->willThrowException(new \Exception("invalid"));
        $this->assertInstanceOf(OrderSearchResultInterface::class,$this->plugin->afterGetList($this->subjectMock, $this->orderMock));
    }
     
    public function testAfterSave()
    {
        $this->orderExtMock->expects($this->any())->method('updateCompletedAt')->willReturn($this->salesOrderMock);
            
        $afterSave = $this->plugin->afterSave($this->subjectMock,$this->salesOrderMock);
        $this->assertInstanceOf(Order::class, $afterSave);
        //$this->assertNull($afterSave);
    }
}
