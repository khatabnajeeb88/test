<?php 
/**
 * This file consist of PHPUnit test case for OrderStatusPlacedObserver
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Observer;

use Arb\Order\Model\Order;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Arb\Order\Observer\OrderStatusPlacedObserver;
use Arb\Order\Helper\OrderStatusHistoryManagement;
use Webkul\Marketplace\Model\OrdersRepository;

/**
 * @covers \Arb\Order\Observer\OrderStatusPlacedObserver
 */
class OrderStatusPlacedObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagementMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersRepository
     */
    private $ordersRepositoryMock;

    /**
     * Object to test
     *
     * @var OrderStatusPlacedObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->orderStatusHistoryManagementMock = $this->createMock(OrderStatusHistoryManagement::class);
        $this->ordersRepositoryMock = $this->createMock(OrdersRepository::class);

        $this->testObject = $objectManager->getObject(OrderStatusPlacedObserver::class, [
            'orderStatusHistoryManagement' => $this->orderStatusHistoryManagementMock,
            'ordersRepository' => $this->ordersRepositoryMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->getMockBuilder(Order::class)
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $orderMock
            ->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        $merchantsOrdersArray = [$merchantOrderMock];

        $this->ordersRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn($merchantsOrdersArray);

        $this->orderStatusHistoryManagementMock
            ->expects($this->exactly(count($merchantsOrdersArray)))
            ->method('createOrderHistoryEntry');

        $this->testObject->execute($observerMock);
    }
}
