<?php
/**
 * This file consist of PHPUnit test case for OrderStatusCloseObserver
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Model\Order;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository;
use Arb\Order\Observer\OrderStatusCloseObserver;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection;
use Arb\Order\Model\OrderHistoryFactory;
use Arb\Order\Helper\OrderStatusHistoryManagement;

/**
 * @covers \Arb\Order\Observer\OrderStatusCloseObserver
 */
class OrderStatusCloseObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagementMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersRepository
     */
    private $ordersRepositoryMock;

    /**
     * Object to test
     *
     * @var OrderStatusCloseObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->orderStatusHistoryManagementMock = $this->createMock(OrderStatusHistoryManagement::class);
        $this->ordersRepositoryMock = $this->createMock(OrdersRepository::class);

        $this->testObject = $objectManager->getObject(OrderStatusCloseObserver::class, [
            'orderStatusHistoryManagement' => $this->orderStatusHistoryManagementMock,
            'ordersRepository' => $this->ordersRepositoryMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->createMock(Order::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        $merchantOrderArray = [$merchantOrderMock, $merchantOrderMock];

        $this->ordersRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn($merchantOrderArray);

        foreach ($merchantOrderArray as $merchantItemMock) {
            $merchantItemMock->method('getId')->willReturn('1');
        }

        $this->orderStatusHistoryManagementMock
            ->expects($this->exactly(count($merchantOrderArray)))
            ->method('createOrderHistoryEntry');

        $this->testObject->execute($observerMock);
    }
}
