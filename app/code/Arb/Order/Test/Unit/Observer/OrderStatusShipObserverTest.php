<?php
/**
 * This file consist of PHPUnit test case for OrderStatusShipObserver
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Arb\Order\Observer\OrderStatusShipObserver;
use Arb\Order\Helper\OrderStatusHistoryManagement;

/**
 * @covers \Arb\Order\Observer\OrderStatusShipObserver
 */
class OrderStatusShipObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagementMock;

    /**
     * Object to test
     *
     * @var OrderStatusShipObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->orderStatusHistoryManagementMock = $this->createMock(OrderStatusHistoryManagement::class);

        $this->testObject = $objectManager->getObject(OrderStatusShipObserver::class, [
            'orderStatusHistoryManagement' => $this->orderStatusHistoryManagementMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($merchantOrderMock);

        $this->orderStatusHistoryManagementMock
            ->expects($this->once())
            ->method('createOrderHistoryEntry');

        $this->testObject->execute($observerMock);
    }
}
