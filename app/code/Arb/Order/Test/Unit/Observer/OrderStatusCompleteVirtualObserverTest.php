<?php
/**
 * This file consist of PHPUnit test case for OrderStatusCompleteVirtualObserver
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Observer;

use Arb\Order\Model\Order;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Arb\Order\Observer\OrderStatusCompleteVirtualObserver;
use Arb\Order\Helper\OrderStatusHistoryManagement;
use Webkul\Marketplace\Model\OrdersRepository as WebkulOrderRepository;
use Zend\Soap\Client\Local;

/**
 * @covers \Arb\Order\Observer\OrderStatusCompleteVirtualObserver
 */
class OrderStatusCompleteVirtualObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagementMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulOrderRepository
     */
    private $webkulOrdersRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ProductRepository
     */
    private $productRepositoryMock;

    /**
     * Object to test
     *
     * @var OrderStatusCompleteVirtualObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->orderStatusHistoryManagementMock = $this->createMock(OrderStatusHistoryManagement::class);
        $this->webkulOrdersRepositoryMock = $this->createMock(WebkulOrderRepository::class);
        $this->productRepositoryMock = $this->createMock(ProductRepository::class);

        $this->testObject = $objectManager->getObject(OrderStatusCompleteVirtualObserver::class, [
            'orderStatusHistoryManagement' => $this->orderStatusHistoryManagementMock,
            'webkulOrdersRepository' => $this->webkulOrdersRepositoryMock,
            'productRepository' => $this->productRepositoryMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->getMockBuilder(Orders::class)
            ->setMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $orderMock
            ->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->getMockBuilder(Orders::class)
            ->setMethods(['getProductIds'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->webkulOrdersRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn([$merchantOrderMock]);

        $merchantOrderMock
            ->expects($this->once())
            ->method('getProductIds')
            ->willReturn('5,6');

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->getMockBuilder(Product::class)
            ->setMethods(['getTypeId'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->productRepositoryMock
            ->method('getById')
            ->willReturn($productMock);

        $productMock
            ->method('getTypeId')
            ->willReturn(Type::TYPE_SIMPLE);

        $this->testObject->execute($observerMock);
    }

    /**
     * @return void
     */
    public function testOrderRepositoryNoSuchEntityExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->getMockBuilder(Orders::class)
            ->setMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $orderMock
            ->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $this->webkulOrdersRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willThrowException(new NoSuchEntityException(__('Error')));

        $this->testObject->execute($observerMock);
    }

    /**
     * @return void
     */
    public function testOrderRepositoryLocalizedExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->getMockBuilder(Orders::class)
            ->setMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $orderMock
            ->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $this->webkulOrdersRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willThrowException(new LocalizedException(__('Error')));

        $this->testObject->execute($observerMock);
    }

    /**
     * @return void
     */
    public function testProductRepositoryExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->getMockBuilder(Orders::class)
            ->setMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $orderMock
            ->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->getMockBuilder(Orders::class)
            ->setMethods(['getProductIds'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->webkulOrdersRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn([$merchantOrderMock]);

        $merchantOrderMock
            ->expects($this->once())
            ->method('getProductIds')
            ->willReturn('5,6');

        $this->productRepositoryMock
            ->method('getById')
            ->willThrowException(new NoSuchEntityException(__('Error')));

        $this->testObject->execute($observerMock);
    }
}
