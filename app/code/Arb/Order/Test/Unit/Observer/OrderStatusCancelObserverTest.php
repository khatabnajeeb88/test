<?php 
/**
 * This file consist of PHPUnit test case for OrderStatusCancelObserver
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Model\Order;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory;
use Arb\Order\Observer\OrderStatusCancelObserver;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection;
use Arb\Order\Model\OrderHistoryFactory;
use Arb\Order\Helper\OrderStatusHistoryManagement;

/**
 * @covers \Arb\Order\Observer\OrderStatusCancelObserver
 */
class OrderStatusCancelObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagementMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersRepository
     */
    private $ordersRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $ordersCollectionMock;

    /**
     * Object to test
     *
     * @var OrderStatusCancelObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->orderStatusHistoryManagementMock = $this->createMock(OrderStatusHistoryManagement::class);
        $this->ordersRepositoryMock = $this->createMock(OrdersRepository::class);
        $this->ordersCollectionMock = $this->createMock(CollectionFactory::class);

        $this->testObject = $objectManager->getObject(OrderStatusCancelObserver::class, [
            'orderStatusHistoryManagement' => $this->orderStatusHistoryManagementMock,
            'ordersRepository' => $this->ordersRepositoryMock,
            'collectionFactory' => $this->ordersCollectionMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        return [
            [
                '1',                                        // merchant Id
                [$merchantOrderMock, $merchantOrderMock]    // merchant orders array
            ],
            [
                null,                                       // merchant Id
                [$merchantOrderMock]                        // merchant orders array
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param string|null $merchantId
     * @param array $merchantOrderArray
     *
     * @return void
     */
    public function testExecute(?string $merchantId, array $merchantOrderArray)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder', 'getSellerId'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->createMock(Order::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $observerMock
            ->expects($this->once())
            ->method('getSellerId')
            ->willReturn($merchantId);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $collectionCallCount = $merchantId ? 1 : 0;
        $orderRepoCallCount = $merchantId ? 0 : 1;

        $this->ordersCollectionMock
            ->expects($this->exactly($collectionCallCount))
            ->method('create')
            ->willReturn($collectionMock);

        $collectionMock
            ->expects($this->exactly(2*$collectionCallCount))
            ->method('addFieldToFilter')
            ->willReturnOnConsecutiveCalls($collectionMock, $merchantOrderArray);

        $this->ordersRepositoryMock
            ->expects($this->exactly($orderRepoCallCount))
            ->method('getByOrderId')
            ->willReturn($merchantOrderArray);

        foreach ($merchantOrderArray as $merchantItemMock) {
            $merchantItemMock->method('getId')->willReturn('1');
        }

        $this->orderStatusHistoryManagementMock
            ->expects($this->exactly(count($merchantOrderArray)))
            ->method('createOrderHistoryEntry');

        $this->testObject->execute($observerMock);
    }
}
