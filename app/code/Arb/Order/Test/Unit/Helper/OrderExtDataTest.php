<?php
/**
 * This file consist of PHPUnit test case for OrderExtData
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Helper;

use Magento\Sales\Model\Order;
use Arb\CustomWebkul\Model\OrderDetails;
use Arb\Order\Api\Data\MageOrderDetailsInterface;
use Arb\Order\Api\MageOrderDetailsRepositoryInterface;
use Arb\Order\Model\MageOrderDetailsFactory;
use Arb\Order\Model\OrderHistory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Order\Helper\OrderExtData;

/**
 * @covers \Arb\Order\Helper\OrderExtData
 */
class OrderExtDataTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|MageOrderDetailsRepositoryInterface
     */
    private $mageOrderDetailsRepositoryInterfaceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|MageOrderDetailsFactory
     */
    private $mageOrderDetailsFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Order
     */
    private $resultMock;

    /**
     * Object to test
     *
     * @var OrderExtData
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->mageOrderDetailsRepositoryInterfaceMock = $this->createMock(MageOrderDetailsRepositoryInterface::class);
        $this->mageOrderDetailsFactoryMock = $this->createMock(MageOrderDetailsFactory::class);
        $this->resultMock = $this->getMockBuilder(Order::class)
            ->setMethods(['getId', 'getCompletedAt'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->testObject = $objectManager->getObject(OrderExtData::class, [
            'orderDetailsRepository' => $this->mageOrderDetailsRepositoryInterfaceMock,
            'orderDetailsFactory' => $this->mageOrderDetailsFactoryMock
        ]);
    }

    /**
     * @return void
     */
    public function testUpdateCompletedAt()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderDetails $orderDetailsMock */
        $orderDetailsMock = $this->createMock(MageOrderDetailsInterface::class);
        $orderDetailsMock = $this->getMockBuilder(MageOrderDetailsInterface::class)
            ->setMethods(['getCompletedAt','setCompletedAt','setOrderId'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->mageOrderDetailsRepositoryInterfaceMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn(null);

        $this->mageOrderDetailsFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($orderDetailsMock);

        $this->resultMock
            ->method('getCompletedAt')
            ->willReturn(1);

        $this->mageOrderDetailsRepositoryInterfaceMock->save($orderDetailsMock);
        $afterSave = $this->testObject->updateCompletedAt($this->resultMock);
        $this->assertInstanceOf(Order::class, $afterSave);
    }
    
    /**
     * @return void
     */
    public function testLoadCompletedAt()
    {
        $this->resultMock
            ->method('getId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderDetails $orderDetailsMock */
        $orderDetailsMock = $this->getMockBuilder(MageOrderDetailsInterface::class)
            ->setMethods(['getCompletedAt'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->mageOrderDetailsRepositoryInterfaceMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn($orderDetailsMock);

        $orderDetailsMock
            ->method('getCompletedAt')
            ->willReturn('2020/06/01 00:00:00');

        $afterLoad = $this->testObject->loadCompletedAt($this->resultMock);
        $this->assertInstanceOf(Order::class, $afterLoad);
    }
}
