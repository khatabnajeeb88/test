<?php
/**
 * This file consist of PHPUnit test case for OrderStatusHistoryManagement
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Helper;

use Arb\Order\Model\OrderHistory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Order\Model\OrderHistoryFactory;
use Arb\Order\Api\OrderHistoryRepositoryInterface;
use Arb\Order\Helper\OrderStatusHistoryManagement;

/**
 * @covers \Arb\Order\Helper\OrderStatusHistoryManagement
 */
class OrderStatusHistoryManagementTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderHistoryRepositoryInterface
     */
    private $orderHistoryRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderHistoryFactory
     */
    private $orderHistoryFactoryMock;

    /**
     * Object to test
     *
     * @var OrderStatusHistoryManagement
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->orderHistoryRepositoryMock = $this->createMock(OrderHistoryRepositoryInterface::class);
        $this->orderHistoryFactoryMock = $this->createMock(OrderHistoryFactory::class);

        $this->testObject = $objectManager->getObject(OrderStatusHistoryManagement::class, [
            'orderHistoryRepository' => $this->orderHistoryRepositoryMock,
            'orderHistoryFactory' => $this->orderHistoryFactoryMock
        ]);
    }

    /**
     * @return void
     */
    public function testCreateOrderHistoryEntry()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|OrderHistory $orderHistoryMock */
        $orderHistoryMock = $this->createMock(OrderHistory::class);

        $this->orderHistoryFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($orderHistoryMock);

        $orderHistoryMock
            ->expects($this->once())
            ->method('setOrderId')
            ->willReturnSelf();

        $orderHistoryMock
            ->expects($this->once())
            ->method('setStatus')
            ->willReturnSelf();

        $this->orderHistoryRepositoryMock
            ->expects($this->once())
            ->method('save');

        $this->testObject->createOrderHistoryEntry('status', 5);
    }
}
