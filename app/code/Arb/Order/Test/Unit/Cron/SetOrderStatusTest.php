<?php
/**
 * This file consist of PHPUnit test case for Cron class SetOrderStatus
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Test\Unit\Cron;

use Arb\Order\Cron\SetOrderStatus;
use Magento\Sales\Model\Order;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Arb\Order\Logger\Logger;
use Magento\Framework\Api\SearchCriteria;

/**
 * @covers \Arb\Order\Cron\SetOrderStatus
 */
class SetOrderStatusTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderRepositoryInterface
     */
    private $orderRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ScopeConfigInterface
     */
    private $scopeConfigMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Logger
     */
    private $loggerMock;

    /**
     * Object to test
     *
     * @var SetOrderStatus
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->orderRepositoryMock = $this->createMock(OrderRepositoryInterface::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->loggerMock = $this->createMock(Logger::class);

        $this->testObject = $objectManager->getObject(SetOrderStatus::class, [
            'orderRepository' => $this->orderRepositoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'scopeConfig' => $this->scopeConfigMock,
            'logger' => $this->loggerMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        return [
            [   // Scenario 1: Return window exists
                5,          // return window in days
                1           // method call count
            ],
            [   // Scenario 2: Return window doesn't exist
                null,       // return window in days
                0           // method call count
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param int|null $returnWindow
     * @param int $callCount
     *
     * @return void
     */
    public function testExecute(?int $returnWindow, int $callCount)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($returnWindow);

        $this->searchCriteriaBuilderMock
            ->expects($this->exactly(3 * $callCount))
            ->method('addFilter')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->exactly($callCount))
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->getMockBuilder(Order::class)
            ->disableOriginalConstructor()
            ->setMethods(['setState', 'setStatus'])
            ->getMock();

        $orders = [$orderMock, $orderMock];

        $this->orderRepositoryMock
            ->expects($this->exactly($callCount))
            ->method('getList')
            ->willReturn($orders);

        $orderMock
            ->method('setState')
            ->willReturnSelf();

        $orderMock
            ->method('setStatus')
            ->willReturnSelf();

        $this->orderRepositoryMock
            ->method('save');

        $this->testObject->execute();
    }
    
    /**
     * @dataProvider executeProvider
     *
     * @param int|null $returnWindow
     * @param int $callCount
     *
     * @return void
     */
    public function testExecuteContine(?int $returnWindow, int $callCount)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($returnWindow);

        $this->searchCriteriaBuilderMock
            ->expects($this->exactly(3 * $callCount))
            ->method('addFilter')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->exactly($callCount))
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->getMockBuilder(Order::class)
            ->disableOriginalConstructor()
            ->setMethods(['setState', 'setStatus'])
            ->getMock();

        $orders = [$orderMock, $orderMock];

        $this->orderRepositoryMock
            ->expects($this->exactly($callCount))
            ->method('getList')
            ->willReturn($orders);
            
        $this->testObject->execute();
    }
}
