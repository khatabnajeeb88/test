<?php
/**
 * Change Order Status on set time
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 */

namespace Arb\Order\Cron;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Arb\Order\Logger\Logger;
use Magento\Framework\Event\ManagerInterface;
use DateTime;
use Exception;

/**
 * Adjust Order Status to be 'Closed' if Order was completed, once Return Window expired
 */
class SetOrderStatus
{
    private const ORDER_STATUS_CLOSED = 'closed';
    private const REFUND_WINDOW_CONFIG_PATH = 'mprmasystem/settings/default_days';

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var ManagerInterface
     */
    private $manager;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param ManagerInterface $manager
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        ManagerInterface $manager
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->manager = $manager;
    }

    /**
     * Get Completed Orders of x days and close them
     *
     * @return void
     * @throws Exception
     */
    public function execute()
    {
        $refundWindow = $this->getRefundWindow();

        if (!$refundWindow) {
            return;
        }

        $maxCreatedDate = new DateTime();
        $maxCreatedDate->modify('- ' . $refundWindow . ' day');

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status', 'complete')
            ->addFilter('is_virtual', 0)
            ->addFilter('created_at', $maxCreatedDate->format('Y-m-d h:i:s'), 'lteq')
            ->create();

        foreach ($this->orderRepository->getList($searchCriteria) as $order) {
            $maxReturnDate = new DateTime($order->getCompletedAt());
            $maxReturnDate->modify('+ ' . $refundWindow . ' day');
            $todaysDate = new DateTime();

            if ($todaysDate <= $maxReturnDate) {
                continue;
            }

            $order->setState(Order::STATE_CLOSED);
            $order->setStatus(self::ORDER_STATUS_CLOSED);
            $this->orderRepository->save($order);
            $this->manager->dispatch('arb_order_closed', ['order' => $order]);
            $this->logger->info('Order #' . $order->getIncrementId() . ' has been closed automatically.');
        }
    }

    /**
     * Get Admin Value for Return Window days
     *
     * @return string|null
     */
    private function getRefundWindow()
    {
        return $this->scopeConfig->getValue(
            self::REFUND_WINDOW_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }
}
