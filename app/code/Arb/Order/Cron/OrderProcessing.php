<?php

/**
 * Process order
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Cron;

use Arb\ArbPayment\Model\PaymentInquiry;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * OrderProcessing
 */
class OrderProcessing
{

    /**
     *
     * @var \Arb\ArbPayment\Model\PaymentLog
     */
    protected $paymentLog;

    /**
     * @var \Arb\Vouchers\Model\VoucherOrders
     */
    protected $vouchers;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $orderObject;

    /**
     * esbNotification
     *
     * @var \Arb\ArbPayment\Model\ESBNotifications
     */
    protected $esbNotification;

    /**
     * paymentInquiry
     *
     * @var Arb\ArbPayment\Model\PaymentInquiry
     */
    protected $paymentInquiry;
    const VOUCHER_USED = 2;

    /**
     * Payment Inquiry class constructor
     * @param \Arb\Vouchers\Model\VoucherOrders $vouchers
     * @param ESBNotifications $esbNotification
     * @return void
     *  @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Arb\Vouchers\Model\VoucherOrders $vouchers,
        \Arb\ArbPayment\Model\ESBNotifications $esbNotification,
        \Magento\Sales\Model\Order $orderObject,
        PaymentInquiry $paymentInquiry,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $voucherCollection,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->vouchers = $vouchers;
        $this->orderObject = $orderObject;
        $this->esbNotification = $esbNotification;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Payment_Log-' . date("Ymd") . '.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->paymentInquiry = $paymentInquiry;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderRepository = $orderRepository;
        $this->json = $json;
        $this->voucherCollection = $voucherCollection;
        $this->itemFactory = $itemFactory;
        $this->_productRepository = $productRepository;
    }

    /**
     * OrderExecute
     *
     * @param  mixed $orderIds
     * @return void
     */
    public function execute()
    {
        $orderIds = [];
        foreach ($orderIds as $orderId) {
            $order = $this->orderObject->loadByIncrementId($orderId);
            $this->logger->info($orderId . " - orderId with order status" . $order->getStatus());
            $assignVouchers = $this->assignVouchers($order->getIncrementId());
            if ($assignVouchers === true) {
                $this->logger->info($orderId . " - processInquiryResponse voucher assigned");
                $this->esbNotification->prepareOrderData($order->getIncrementId());
            } else {
                $this->logger->info($orderId . " - processInquiryResponse voucher not assigned");
            }

            if (($order->canInvoice()) && ($assignVouchers === true)) {
                $this->paymentInquiry->createInvoice($order->getId());
                $this->logger->info($orderId . " - processInquiryResponse invoice generated");
            } else {
                $this->logger->info($orderId . " - processInquiryResponse invoice generated failed");
            }
            $return['status'] = true;
            $return['orderStatus'] = $order->getStatus();
            $return['error'] = isset($responseData[0]['error']) ? $responseData[0]['error'] : null;
            $return['errorText'] = isset($responseData[0]['errorText']) ? $responseData[0]['errorText'] : null;
            $this->logger->info($return);
        }
    }

    /**
     * assignVouchers
     *
     * @param  mixed $orderId
     * @return void
     */
    public function assignVouchers($orderId)
    {

        try {
            $return = false;
            $vouchers = $this->voucherCollection->create();
            $vouchers->addFieldToSelect(['sku', 'order_id', 'is_used', 'voucher_code']);
            $vouchers->addFieldToFilter('order_id', $orderId);
            $vouchers->addFieldToFilter('is_used', self::VOUCHER_USED);
            $logger = $this->getLoggerFunction();
            $logger->info("assignVouchers:" . $vouchers->getSize());
            if ($vouchers->getSize() > 0) {
                $skuVoucher = [];
                // Get voucher data against order increment id and set voucher used flag
                try {
                    foreach ($vouchers as $voucher) {
                        $skuVoucher[$voucher->getSku()][] = $voucher->getVoucherCode();
                    }
                } catch (\Exception $e) {
                    $logger->crit("Error happened while assign the vouchers:" . $e->getMessage());
                    throw new LocalizedException(
                        __('Unable to get assigned vouchers to orders')
                    );
                }
                // Assign voucher to order item
                $this->searchCriteriaBuilder->addFilter('increment_id', $orderId);
                $orders = $this->orderRepository->getList(
                    $this->searchCriteriaBuilder->create()
                );
                $logger =  $this->getLoggerFunction();
                // Saving data to logs
                $logger->info("assignVouchers order id :" . $orderId);
                $logger->info("assignVouchers available vouchers :" . json_encode($skuVoucher));
                if (!empty($orders->getItems())) {
                    foreach ($orders as $orderData) {
                        foreach ($orderData->getAllItems() as $item) {
                            if ($item->getProductType() !== ProductType::TYPE_VIRTUAL) {
                                continue;
                            }
                            $logger->info("assignVouchers item id :" . json_encode($item->getId()));
                            $product=$this->_productRepository->getById($item->getId());
                            $logger->info("assignVouchers item sku :" . json_encode($product->getSku()));
                            if (!isset($skuVoucher[$product->getSku()])) {
                                throw new LocalizedException(
                                    __('Unable to assign vouchers to orders. Order Id is not mapped with vouchers.')
                                );
                            }
                            $voucherData = $this->json->serialize($skuVoucher[$product->getSku()]);
                            $orderItem = $this->itemFactory->create()->load($item->getId());
                            $orderItem->setVouchers($voucherData);
                            $orderItem->save();
                            $logger->info("assignVouchers vouchers data :" . $item->getId() . ' --- ' . $voucherData);
                        }
                    }
                    $return = true;
                }
            }
        } catch (\Exception $e) {
            $return = false;
            $logger->crit("assignVouchers :" . $e->getMessage());
            throw new LocalizedException(
                __('Unable to assign vouchers to orders')
            );
        }
        return $return;
    }
    /**
     * Returns log object
     *
     * @return \Zend\Log\Logger
     */
    protected function getLoggerFunction()
    {
        try {
            $this->_writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arbvouchers_Log-'.date("Ymd").'.log');
            $this->_logger = new \Zend\Log\Logger();
            return $this->_logger->addWriter($this->_writer);
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Error in log file creation')
            );
        }
    }
}
