<?php
/**
 * Helper for Extended Magento Order Data
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Arb\Order\Api\MageOrderDetailsRepositoryInterface;
use Arb\Order\Model\MageOrderDetailsFactory;

class OrderExtData extends AbstractHelper
{
    /**
     * @var MageOrderDetailsRepositoryInterface
     */
    private $orderDetailsRepository;

    /**
     * @var MageOrderDetailsFactory
     */
    private $orderDetailsFactory;

    /**
     * @param Context $context
     * @param MageOrderDetailsRepositoryInterface $orderDetailsRepository
     * @param MageOrderDetailsFactory $orderDetailsFactory
     */
    public function __construct(
        Context $context,
        MageOrderDetailsRepositoryInterface $orderDetailsRepository,
        MageOrderDetailsFactory $orderDetailsFactory
    ) {
        parent::__construct($context);

        $this->orderDetailsRepository = $orderDetailsRepository;
        $this->orderDetailsFactory = $orderDetailsFactory;
    }

    /**
     * Save the CompletedAt data into table
     *
     * @param Order $order
     *
     * @return Order
     */
    public function updateCompletedAt(Order $order)
    {
        $orderDetails = $this->orderDetailsRepository->getByOrderId($order->getId());

        if (!$orderDetails) {
            $orderDetails = $this->orderDetailsFactory->create();
            $orderDetails->setOrderId($order->getId());
        }

        if ($order->getCompletedAt()) {
            $orderDetails->setCompletedAt($order->getCompletedAt());
        }

        $this->orderDetailsRepository->save($orderDetails);

        return $order;
    }

    /**
     * Pass stored data on load
     *
     * @param Order $order
     *
     * @return Order
     */
    public function loadCompletedAt(Order $order)
    {
        $orderDetails = $this->orderDetailsRepository->getByOrderId($order->getId());

        if ($orderDetails) {
            $order->setCompletedAt($orderDetails->getCompletedAt());
        }

        return $order;
    }
}
