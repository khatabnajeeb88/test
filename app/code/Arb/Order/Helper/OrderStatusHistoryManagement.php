<?php
/**
 * Helper for Order Status History Management
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Helper;

use Arb\Order\Api\Data\OrderHistoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Arb\Order\Api\OrderHistoryRepositoryInterface;
use Arb\Order\Model\OrderHistoryFactory;

class OrderStatusHistoryManagement extends AbstractHelper
{
    /**
     * @var OrderHistoryRepositoryInterface
     */
    private $orderHistoryRepository;

    /**
     * @var OrderHistoryFactory
     */
    private $orderHistoryFactory;

    /**
     * @param Context $context
     * @param OrderHistoryRepositoryInterface $orderHistoryRepository
     * @param OrderHistoryFactory $orderHistoryFactory
     */
    public function __construct(
        Context $context,
        OrderHistoryRepositoryInterface $orderHistoryRepository,
        OrderHistoryFactory $orderHistoryFactory
    ) {
        parent::__construct($context);

        $this->orderHistoryRepository = $orderHistoryRepository;
        $this->orderHistoryFactory = $orderHistoryFactory;
    }

    /**
     * @param string $status
     * @param int $orderId
     *
     * @return void
     */
    public function createOrderHistoryEntry(string $status, int $orderId)
    {
        /** @var OrderHistoryInterface $orderHistory */
        $orderHistory = $this->orderHistoryFactory->create();

        $orderHistory->setOrderId($orderId);
        $orderHistory->setStatus($status);

        $this->orderHistoryRepository->save($orderHistory);
    }
}
