<?php

namespace Arb\Order\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('arb_order_status_history')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('arb_order_status_history')
			)
				->addColumn(
					'entity_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Entity Id'
				)
				->addColumn(
					'order_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					255,
					['identity' => false,'nullable => false','unsigned' => true],
					'Order ID'
				)
				->addColumn(
					'status',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					64,
					[],
					'Consignment Status'
				)
				->addColumn(
					'created_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
					'Creation Time'
				)
				->setComment('Merchant Order Status History');
			$installer->getConnection()->createTable($table);
		}
		if (!$installer->tableExists('arb_mage_order_details')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('arb_mage_order_details')
			)
				->addColumn(
					'entity_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Entity Id'
				)
				->addColumn(
					'order_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					255,
					['identity' => false,'nullable => false','unsigned' => true],
					'Order ID'
				)
				
				->addColumn(
					'created_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
					'Date the Order was completed'
				)
				->setComment('Magento Orders Extended Values');
			$installer->getConnection()->createTable($table);
		}
		$installer->endSetup();
	}
}