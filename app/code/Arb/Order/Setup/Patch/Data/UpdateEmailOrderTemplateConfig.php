<?php
/**
 * DataPatch to set shipment and order email
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UpdateEmailOrderTemplateConfig implements DataPatchInterface
{
    private const EMAIL_ORDER_CONFIRM= 'order/emails/order_confirmed_email_template';

    private const EMAIL_ORDER_COMPLETE = 'order/emails/order_complete_email_template';

    private const EMAIL_ORDER_ARABIC_CONFIRM = 'order/emails/order_confirmed_email_arabic_template';

    private const ARB_EMAIL_ORDER_CONFIRM = 'order_emails_order_confirmed_email_template';

    private const ARB_EMAIL_ORDER_ARBIC_CONFIRM = 'order_emails_order_confirmed_email_arabic_template';

    private const ARB_EMAIL_ORDER_COMPLETE = 'order_emails_order_complete_email_template';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::EMAIL_ORDER_CONFIRM, self::ARB_EMAIL_ORDER_CONFIRM);
        $this->updateConfiguration(self::EMAIL_ORDER_COMPLETE, self::ARB_EMAIL_ORDER_COMPLETE);
        $this->updateConfiguration(self::EMAIL_ORDER_ARABIC_CONFIRM, self::ARB_EMAIL_ORDER_ARBIC_CONFIRM);
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param mixed $value
     *
     * @return void
     */
    private function updateConfiguration(string $path, $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
