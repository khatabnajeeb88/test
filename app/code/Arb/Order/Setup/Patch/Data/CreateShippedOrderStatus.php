<?php

namespace Arb\Order\Setup\Patch\Data;

use Exception;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Model\Order\Status;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

class CreateShippedOrderStatus implements DataPatchInterface
{
    private const SHIPPED_STATUS = 'shipped';
    private const SHIPPED_STATUS_LABEL = 'Shipped';

    /**
     * @var StatusResourceFactory
     */
    private $statusResourceFactory;

    /**
     * @var StatusFactory
     */
    private $statusFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * CreateShippedOrderStatus constructor.
     *
     * @param StatusResourceFactory $statusResourceFactory
     * @param StatusFactory $statusFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        StatusResourceFactory $statusResourceFactory,
        StatusFactory $statusFactory,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->statusResourceFactory = $statusResourceFactory;
        $this->statusFactory = $statusFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @throws Exception
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $statusResource = $this->statusResourceFactory->create();
        /** @var Status $status */
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => self::SHIPPED_STATUS,
            'label' => self::SHIPPED_STATUS_LABEL,
        ]);

        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {
            return;
        }

        $status->assignState('complete', true, true);

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
