<?php
/**
 * DataPatch to set shipment and order email
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UpdateEmailShipmentTemplateConfig implements DataPatchInterface
{
    private const EMAIL_SHIPMENT_CREATE_FOR_GUEST= 'sales_email/shipment/guest_template';

    private const EMAIL_SHIPMENT_CREATE_FOR_CUSTOMER = 'sales_email/shipment/template';

    private const EMAIL_SHIPMENT_ENABLED = 'sales_email/shipment/enabled';

    private const ENABLE_FLAG = 1;

    private const ARB_EMAIL_SHIPMENT_CREATE_FOR_GUEST = 'arb_orders_guest_sales_email_shipment_guest_template';

    private const ARB_EMAIL_SHIPMENT_CREATE_FOR_CUSTOMER = 'arb_orders_sales_email_shipment_template';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->updateConfiguration(self::EMAIL_SHIPMENT_CREATE_FOR_GUEST, self::ARB_EMAIL_SHIPMENT_CREATE_FOR_GUEST);
        $this->updateConfiguration(self::EMAIL_SHIPMENT_CREATE_FOR_CUSTOMER, self::ARB_EMAIL_SHIPMENT_CREATE_FOR_CUSTOMER);
        $this->updateConfiguration(self::EMAIL_SHIPMENT_ENABLED, self::ENABLE_FLAG);
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param string $path
     * @param mixed $value
     *
     * @return void
     */
    private function updateConfiguration(string $path, $value)
    {
        $this->writer->save(
            $path,
            $value
        );
    }
}
