<?php

namespace Arb\Order\Plugin\Sales\Model\Order;

use Magento\Sales\Model\Order\Item as ItemModel;

class Item
{
    /**
     * @var ItemModel
     */
    private $orderItem;

    /**
     * @return mixed
     */
    public function afterGetQtyToCancel(ItemModel $subject)
    {
        $qtyToCancel = $subject->getQtyToShip();
        return max($qtyToCancel, 0);
    }
}
