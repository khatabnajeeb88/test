<?php

namespace Arb\Order\Plugin\Sales\Model;

use Magento\Sales\Model\Order as ModelOrder;
use Webkul\Marketplace\Helper\Orders;
use Arb\Order\Helper\OrderExtData;
use Arb\Order\Api\MageOrderDetailsRepositoryInterface;
use Arb\Order\Model\MageOrderDetailsFactory;

class Order
{
    /**
     * @var Orders
     */
    private $webkulOrders;

    /**
     * @var MageOrderDetailsRepositoryInterface
     */
    private $orderDetailsRepository;

    /**
     * @var MageOrderDetailsFactory
     */
    private $orderDetailsFactory;

    /**
     * @var OrderExtData
     */
    private $orderExtData;

    /**
     * @param Orders $webkulOrders
     * @param MageOrderDetailsRepositoryInterface $orderDetailsRepository
     * @param MageOrderDetailsFactory $orderDetailsFactory
     * @param OrderExtData $orderExtData
     */
    public function __construct(
        Orders $webkulOrders,
        MageOrderDetailsRepositoryInterface $orderDetailsRepository,
        MageOrderDetailsFactory $orderDetailsFactory,
        OrderExtData $orderExtData
    ) {
        $this->webkulOrders = $webkulOrders;
        $this->orderDetailsRepository = $orderDetailsRepository;
        $this->orderDetailsFactory = $orderDetailsFactory;
        $this->orderExtData = $orderExtData;
    }

    /**
     * @param ModelOrder $subject
     * @param bool $result
     *
     * @return bool
     */
    public function afterCanCancel(ModelOrder $subject, bool $result)
    {
        if ($subject->getStatus() === 'processing') {
            $merchantOrder = $this->webkulOrders->getOrderinfo($subject->getId());
            if (!$merchantOrder) {
                return $result;
            }

            // checking if merchant order is invoiced or shipped,
            // it should be possible to cancel order when it is invoiced but not shipped
            if ($merchantOrder->getShipmentId() !== '0') {
                return false;
            }

            if ($merchantOrder->getInvoiceId() !== '0') {
                return true;
            }
        }

        return $result;
    }

    /**
     * @param ModelOrder $subject
     * @param ModelOrder $result
     *
     * @return ModelOrder
     */
    public function afterSave(ModelOrder $subject, ModelOrder $result)
    {
        return $this->orderExtData->updateCompletedAt($result);
    }

    /**
     * @param ModelOrder $subject
     * @param ModelOrder $result
     *
     * @return ModelOrder
     */
    public function afterLoad(ModelOrder $subject, ModelOrder $result)
    {
        return $this->orderExtData->loadCompletedAt($result);
    }
}
