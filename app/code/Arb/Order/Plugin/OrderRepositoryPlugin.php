<?php
/**
 * This file consist of class which display product image in ordr list API
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Plugin;

use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderExtensionInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory as ProductRepository;
use Magento\Sales\Model\Order as ModelOrder;
use Arb\Order\Helper\OrderExtData;

/**
 * Class OrderRepositoryPlugin for adding Image in Order Listing API
 */
class OrderRepositoryPlugin
{
    /**
     * Order product_image field name
     */
    const FIELD_NAME = 'product_image';

     /**
      * @var ProductRepository
      */
    protected $productRepository;

    /**
     * Order Extension Attributes Factory
     *
     * @var OrderExtensionFactory
     */
    protected $extensionFactory;

    /**
     * @var OrderExtData
     */
    private $orderExtData;

    /**
     * @param ProductRepository $productRepository
     * @param OrderExtensionFactory $extensionFactory
     * @param OrderExtData $orderExtData
     */
    public function __construct(
        ProductRepository $productRepository,
        OrderExtensionFactory $extensionFactory,
        OrderExtData $orderExtData
    ) {
        $this->productRepository = $productRepository;
        $this->extensionFactory = $extensionFactory;
        $this->orderExtData = $orderExtData;
    }

    /**
     * Add "product_image" extension attribute to order data object to make it accessible in API data of all order list
     *
     * @return OrderSearchResultInterface
     */
    public function afterGetList(OrderRepositoryInterface $subject, OrderSearchResultInterface $searchResult)
    {
        $orders = $searchResult->getItems();

        foreach ($orders as $order) {
            $order = $this->orderExtData->loadCompletedAt($order);
            $orderItems = $order->getAllItems();
            $orderItem = current($orderItems);
            $_productId = $orderItem->getProductId();

            /** get product thumbnail data */
            try {
                $product = $this->productRepository->create()->getById($_productId);
                $imageURL = $product->getThumbnail();
            } catch (\Exception $e) {
                $product = false;
                $imageURL = "";
            }

            //set product image in attribute
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
            $extensionAttributes->setProductImage($imageURL);
            $order->setExtensionAttributes($extensionAttributes);
        }

        return $searchResult;
    }

    /**
     * @param OrderRepositoryInterface $subject
     * @param ModelOrder $result
     *
     * @return ModelOrder
     */
    public function afterSave(OrderRepositoryInterface $subject, ModelOrder $result)
    {
        return $this->orderExtData->updateCompletedAt($result);
    }
}
