<?php
/**
 * Observer to track if Order got completed and then track the status change
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Observer;

use Arb\Order\Helper\OrderStatusHistoryManagement;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Webkul\Marketplace\Model\Orders;

class OrderStatusCompleteObserver implements ObserverInterface
{
    private const ORDER_STATUS_COMPLETE = 'delivered';

    /**
     * @var OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagement;

    /**
     * @param OrderStatusHistoryManagement $orderStatusHistoryManagement
     */
    public function __construct(
        OrderStatusHistoryManagement $orderStatusHistoryManagement
    ) {
        $this->orderStatusHistoryManagement = $orderStatusHistoryManagement;
    }

    /**
     * Create Order Status Change entry in database for Order Completion
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Orders $merchantOrder */
        $merchantOrder = $observer->getOrder();

        $this->orderStatusHistoryManagement->createOrderHistoryEntry(
            self::ORDER_STATUS_COMPLETE,
            (int)$merchantOrder->getId()
        );
    }
}
