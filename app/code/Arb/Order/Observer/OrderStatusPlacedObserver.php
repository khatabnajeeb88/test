<?php
/**
 * Observer to track if Order got created and then track the status change
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Arb\Order\Helper\OrderStatusHistoryManagement;
use Webkul\Marketplace\Model\OrdersRepository;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class OrderStatusPlacedObserver implements ObserverInterface
{
    private const ORDER_STATUS_CREATED = 'created';

    /**
     * @var OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagement;

    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * @param OrderStatusHistoryManagement $orderStatusHistoryManagement
     * @param OrdersRepository $ordersRepository
     */
    public function __construct(
        OrderStatusHistoryManagement $orderStatusHistoryManagement,
        OrdersRepository $ordersRepository
    ) {
        $this->orderStatusHistoryManagement = $orderStatusHistoryManagement;
        $this->ordersRepository = $ordersRepository;
    }

    /**
     * Create Order Status Change entry in database for New Order
     *
     * @param Observer $observer
     *
     * @return void
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();

        $merchantOrders = $this->ordersRepository->getByOrderId($order->getId());

        foreach ($merchantOrders as $merchantOrder) {
            $this->orderStatusHistoryManagement->createOrderHistoryEntry(
                self::ORDER_STATUS_CREATED,
                (int)$merchantOrder->getId()
            );
        }
    }
}
