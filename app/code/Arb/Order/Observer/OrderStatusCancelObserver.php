<?php
/**
 * Observer to track if Order got canceled and then track the status change
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Observer;

use Arb\Order\Helper\OrderStatusHistoryManagement;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Arb\Order\Model\OrderHistoryFactory;
use Webkul\Marketplace\Model\OrdersRepository;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;

class OrderStatusCancelObserver implements ObserverInterface
{
    private const ORDER_STATUS_CANCEL = 'canceled';

    /**
     * @var OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagement;

    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param OrderStatusHistoryManagement $orderStatusHistoryManagement
     * @param OrdersRepository $ordersRepository
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        OrderStatusHistoryManagement $orderStatusHistoryManagement,
        OrdersRepository $ordersRepository,
        CollectionFactory $collectionFactory
    ) {
        $this->orderStatusHistoryManagement = $orderStatusHistoryManagement;
        $this->ordersRepository = $ordersRepository;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Create Order Status Change entry in database for Canceling of Order
     *
     * @param Observer $observer
     *
     * @return void
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();
        $merchantId = $observer->getSellerId();

        if ($merchantId) {
            $merchantOrders = $this->collectionFactory->create()
                ->addFieldToFilter('order_id', $order->getEntityId())
                ->addFieldToFilter('seller_id', $merchantId);
        } else {
            $merchantOrders = $this->ordersRepository->getByOrderId($order->getEntityId());
        }

        foreach ($merchantOrders as $merchantOrder) {
            $this->orderStatusHistoryManagement->createOrderHistoryEntry(
                self::ORDER_STATUS_CANCEL,
                (int)$merchantOrder->getId()
            );
        }
    }
}
