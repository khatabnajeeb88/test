<?php
/**
 * Observer to check if Virtual Order got completed and then track the status change
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Observer;

use Arb\Order\Helper\OrderStatusHistoryManagement;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;
use Webkul\Marketplace\Model\OrdersRepository as WebkulOrderRepository;
use Magento\Catalog\Api\ProductRepositoryInterface;

class OrderStatusCompleteVirtualObserver implements ObserverInterface
{
    private const ORDER_STATUS_COMPLETE = 'complete';

    /**
     * @var OrderStatusHistoryManagement
     */
    private $orderStatusHistoryManagement;

    /**
     * @var WebkulOrderRepository
     */
    private $webkulOrdersRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param OrderStatusHistoryManagement $orderStatusHistoryManagement
     * @param WebkulOrderRepository $webkulOrdersRepository
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        OrderStatusHistoryManagement $orderStatusHistoryManagement,
        WebkulOrderRepository $webkulOrdersRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->orderStatusHistoryManagement = $orderStatusHistoryManagement;
        $this->webkulOrdersRepository = $webkulOrdersRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Create Order Status Change entry in database for Order Completion
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();

        try {
            $merchantOrders = $this->webkulOrdersRepository->getByOrderId($order->getId());
        } catch (NoSuchEntityException $e) {
            return;
        } catch (LocalizedException $e) {
            return;
        }

        foreach ($merchantOrders as $merchantOrder) {
            $productIds = explode(',', $merchantOrder->getProductIds());
            $isPhysical = false;
            foreach ($productIds as $productId) {
                try {
                    $product = $this->productRepository->getById($productId);
                } catch (NoSuchEntityException $e) {
                    continue;
                }
                if ($product->getTypeId() === Type::TYPE_SIMPLE) {
                    $isPhysical = true;
                    break;
                }
            }

            if (!$isPhysical) {
                $this->orderStatusHistoryManagement->createOrderHistoryEntry(
                    self::ORDER_STATUS_COMPLETE,
                    (int)$merchantOrder->getId()
                );
            }
        }
    }
}
