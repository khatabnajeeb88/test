## Synopsis
This is module used for Order API customization and depends on module Magento_Sales, Magento_Eav, Magento_Catalog, Magento_Quote

### API List
1. Get order details using order increment id API with telephone number as optional field
2. Order List API add image of first product in extention attribute
3. Send order email API with Voucher details when order id is provided


### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).
4. Web API configuration details in [webapi.xml](etc/webapi.xml).