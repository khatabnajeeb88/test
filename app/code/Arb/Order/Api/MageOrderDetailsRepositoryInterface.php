<?php
/**
 * This file consist of interface for Extended Magento Order Details Repository
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Api;

interface MageOrderDetailsRepositoryInterface
{
    /**
     * @param \Arb\Order\Api\Data\MageOrderDetailsInterface $orderDetails
     *
     * @return \Arb\Order\Api\Data\MageOrderDetailsInterface
     */
    public function save(\Arb\Order\Api\Data\MageOrderDetailsInterface $orderDetails);

    /**
     * @param int $orderId
     *
     * @return \Arb\Order\Api\Data\MageOrderDetailsInterface|null
     */
    public function getByOrderId(int $orderId);
}
