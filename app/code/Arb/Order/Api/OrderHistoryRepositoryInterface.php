<?php
/**
 * Interface for Order History Repository Class
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Api;

use Arb\Order\Api\Data\OrderHistoryInterface;
use Magento\Framework\Api\SearchResultsInterface;

interface OrderHistoryRepositoryInterface
{
    /**
     * @param \Arb\Order\Api\Data\OrderHistoryInterface $orderHistory
     *
     * @return \Arb\Order\Api\Data\OrderHistoryInterface
     */
    public function save(\Arb\Order\Api\Data\OrderHistoryInterface $orderHistory);

    /**
     * @param \Arb\Order\Api\Data\OrderHistoryInterface $orderHistory
     *
     * @return void
     */
    public function delete(\Arb\Order\Api\Data\OrderHistoryInterface $orderHistory);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $id
     *
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getByOrderId(int $id);
}
