<?php
/**
 * This file consist of interface for Extended Mage Order Details
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Api\Data;

interface MageOrderDetailsInterface
{
    const ORDER_TABLE_NAME = 'arb_mage_order_details';
    const ORDER_ENTITY_ID = 'entity_id';
}
