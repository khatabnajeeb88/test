<?php
/**
 * Interface for OrderHistory Class with getters/setters
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

namespace Arb\Order\Api\Data;

interface OrderHistoryInterface
{
    public const ORDER_HISTORY_TABLE_NAME = 'arb_order_status_history';

    public const ORDER_HISTORY_ENTITY_ID = 'entity_id';
    public const ORDER_HISTORY_ORDER_ID = 'order_id';
    public const ORDER_HISTORY_STATUS = 'status';
    public const ORDER_HISTORY_CREATED_AT = 'created_at';

    /**
     * @return int|null
     */
    public function getOrderId();

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setOrderId(int $id);

    /**
     * @return string|null
     */
    public function getStatus();

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status);

    /**
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * @param string $time
     *
     * @return $this
     */
    public function setCreatedAt(string $time);
}
