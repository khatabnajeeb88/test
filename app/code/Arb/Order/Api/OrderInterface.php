<?php
/**
 * This file file consist of interface for Order related API
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */
namespace Arb\Order\Api;

/**
 * Interface to get order details api
 */
interface OrderInterface
{
   /**
    * function return order details
    *
    * @param string $incrementId
    * @return \Magento\Sales\Api\Data\OrderInterface Array of items.
    * @throws \Magento\Framework\Exception\NoSuchEntityException
    */
    public function getOrder($incrementId);

    /**
     * function send customer order email
     *
     * @param integer $orderId
     * @return boolean
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendMail($orderId);

    /**
     * Lists orders that match specified search criteria.
     * Decrypts input data string and pass \Magento\Framework\Api\SearchCriteriaInterface
     *
     * @param  string $data encrypted search criteria.
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface Order search result interface.
     */
    public function getOrderList($data);
}
