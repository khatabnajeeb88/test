<?php
/**
 * This file consist of class Cron Setup
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Model\Config;

class CronConfig extends \Magento\Framework\App\Config\Value
{
    /**
     * Short description... Cron string path and Cron model path constant decalration
     * Cron string path
     */
    const CRON_STRING_PATH = "crontab/default/jobs/arb_cortex_salesexport/schedule/cron_expr";

    /**
     * @var \Magento\Framework\App\Config\ValueFactory
     */
    protected $_configValueFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Config\ValueFactory $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * 
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\ValueFactory $configValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_configValueFactory = $configValueFactory;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     * @throws \Exception
     * 
     */
    public function afterSave()
    {
        $time = $this->getData('groups/general/fields/time/value');
        $frequency = $this->getData('groups/general/fields/frequency/value');

        $cronExprArray = [
            (int) $time[1],
            (int) $time[0],
            $frequency == \Magento\Cron\Model\Config\Source\Frequency::CRON_MONTHLY ? '1' : '*',
            '*',
            $frequency == \Magento\Cron\Model\Config\Source\Frequency::CRON_WEEKLY ? '1' : '*',
        ];

        $cronExprString = join(' ', $cronExprArray);
        
        /** @var $configValue \Magento\Framework\App\Config\ValueInterface */
        $configValue = $this->_configValueFactory->create();
        $configValue->load(self::CRON_STRING_PATH, 'path');
        $configValue->setValue($cronExprString)->setPath(self::CRON_STRING_PATH);
        $configValue->save();

        return parent::afterSave();
    }
}
