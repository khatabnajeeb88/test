<?php
/**
 * This file consist of class Cortex Model
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class Cortex extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = "cortex_log";
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\Cortex\Model\ResourceModel\Cortex::class
        );
    }
    /**
     * get Identities from Cache.
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
