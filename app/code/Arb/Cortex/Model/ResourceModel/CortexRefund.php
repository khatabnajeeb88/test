<?php
/**
 * This file consist of class `arb_cortex_refunds_mapping` Resource Model collection
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Model\ResourceModel;
 
class CortexRefund extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('arb_cortex_refunds_mapping', 'entity_id');
    }
}
