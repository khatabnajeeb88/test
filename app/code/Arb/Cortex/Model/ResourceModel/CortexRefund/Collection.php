<?php
/**
 * This file consist of class arb_cortex_refunds_mapping collection
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */

namespace Arb\Cortex\Model\ResourceModel\CortexRefund;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Cortex collection
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\Cortex\Model\CortexRefund::class,
            \Arb\Cortex\Model\ResourceModel\CortexRefund::class
        );
    }
}
