<?php
/**
 * This file consist of class Cortex_log Resource Model collection
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Model\ResourceModel;
 
class Cortex extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('cortex_log', 'entity_id');
    }
}
