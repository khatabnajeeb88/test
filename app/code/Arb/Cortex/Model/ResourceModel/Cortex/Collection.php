<?php
/**
 * This file consist of class Cortex_log collection
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */

namespace Arb\Cortex\Model\ResourceModel\Cortex;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Cortex collection
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\Cortex\Model\Cortex::class,
            \Arb\Cortex\Model\ResourceModel\Cortex::class
        );
    }
}
