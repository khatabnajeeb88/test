<?php
/**
 * This file consist of class Cortex refund Model
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class CortexRefund extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = "arb_cortex_refunds_mapping";
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\Cortex\Model\ResourceModel\CortexRefund::class
        );
    }
    /**
     * get Identities from Cache.
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
