<?php
/**
 * SalesCommandTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Test\Unit\Console;

use Arb\Cortex\Console\SalesCommand;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class SalesCommandTest for testing Salesexport class
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class SalesCommandTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var SalesCommand
     */
    private $command;
    
    /**
     * @var ObjectManager
     */
    private $objectManager;
    
    /**
     * @var Salesexport|\PHPUnit_Framework_MockObject_MockObject
     */
    private $salesexport;
    
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->salesexport = $this->getMockBuilder(
            \Arb\Cortex\Cron\Salesexport::class
        )->disableOriginalConstructor()->getMock();
        
        $this->command = new SalesCommand(
            $this->salesexport
        );
    }

    public function testExecute()
    {
        $input =  $this->createMock(InputInterface::class);
        $output =  $this->createMock(OutputInterface::class);
        $output->expects($this->once())->method('writeln');
        $this->salesexport->expects($this->once())->method('execute');

        $this->command->execute($input, $output);
    }
}
