<?php
/**
 * SalesexportTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Test\Unit\Cron;

/**
 * Consist of order data funtions
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SalesListCollection;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\Collection as SalesListCollectionMock;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory as OrdersCollection;
use Magento\Framework\File\Csv;
use Magento\Framework\App\Filesystem\DirectoryList;
use Arb\Cortex\Model\CortexFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Filesystem\Driver\File as DirectoryListFile;
use Arb\Cortex\Cron\Salesexport;
use Magento\Framework\DB\Select;
use Arb\Cortex\Model\CortexRefundFactory as CortexRefund;
use Arb\Cortex\Model\ResourceModel\CortexRefund\Collection as CortexRefundCollection;
use Magento\Sales\Api\CreditmemoRepositoryInterface;

/**
 * Class SalesexportTest for testing Salesexport class
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class SalesexportTest extends TestCase
{
    
    /**
     * @var Csv|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_csvProcessorMock;
    /**
     * @var DirectoryList|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_directoryListMock;

    /**
     * @var SaleslistFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_salesListCollectionMock;

    /**
     * @var CortexFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_cortexFactoryMock;

    /**
     * @var DateTime|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_dateTimeMock;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_scopeConfigMock;
    
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactoryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Framework\App\ResourceConnection
     */
    protected $_resourceMock;
    
    /**
     * @var Salesexport
     */
    protected $_salesExport;

    /**
     * @var File
     */
    protected $_fileIoMock;

    /**
     * @var DirectoryListFile
     */
    protected $_directoryListFileMock;

    /**
     * @var \Arb\Cortex\Model\CortexRefund
     */
    protected $_cortexRefund;
    
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->_csvProcessorMock = $this->getMockBuilder(Csv::class)
            ->setMethods([
                'setDelimiter' ,
                'setEnclosure' ,
                'saveData'
                ])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_csvProcessorMock->expects($this->any())->method('setDelimiter')->willReturnSelf();
        $this->_csvProcessorMock->expects($this->any())->method('setEnclosure')->willReturnSelf();
        $this->_directoryListMock = $this->createMock(DirectoryList::class);
        $this->_salesListCollectionMock = $this->getMockBuilder(SalesListCollection::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->searchCriteriaBuilder = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(["addFilter","create"])
            ->getMockForAbstractClass();
        $this->searchCriteriaInterface = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["addFilter","create"])
            ->getMockForAbstractClass();
        $this->filterBuilder = $this->getMockBuilder(\Magento\Framework\Api\FilterBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(["setField","setValue","setConditionType","create"])
            ->getMock();
        $this->filterGroupBuilder = $this->getMockBuilder(\Magento\Framework\Api\Search\FilterGroupBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(["addFilter","create"])
            ->getMock();
        $this->filter = $this->createMock(\Magento\Framework\Api\Filter::class);

        $this->_salesListMock = $this->getMockBuilder(\Webkul\Marketplace\Model\Saleslist::class)
            ->setMethods(["getSelect","columns",'group',"addFieldToFilter",])
            ->disableOriginalConstructor()
            ->getMock();
        $this->salesListCollectionMock = $this->objectManager->getCollectionMock(
            SalesListCollectionMock::class,
            [$this->_salesListMock]
        );
        $this->_cortexFactoryMock = $this->createPartialMock(CortexFactory::class, ['create',"addData","save"]);

        $this->_dateTimeMock = $this->createMock(DateTime::class);
        $this->_scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->_resourceMock = $this->getMockBuilder(ResourceConnection::class)
            ->setMethods(['insertOnDuplicate',"getConnection","getTableName"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->selectMock = $this->createMock(Select::class);
        $this->_customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
            ->setMethods(["getById","__toArray"])
            ->disableOriginalConstructor()->getMockForAbstractClass();
        $this->_fileIoMock = $this->createMock(File::class);
        $this->_directoryListFileMock = $this->createMock(DirectoryListFile::class);
        $this->_orderFactoryMock = $this->getMockBuilder(OrderFactory::class)
            ->setMethods(["create","getCollection","addFieldToFilter","setOrder"])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_resourceMock->expects($this->any())->method('getConnection')->willReturnSelf();
        $this->_resourceMock->expects($this->any())->method('getTableName')->willReturn("sales_order");
        $this->_resourceMock->expects($this->any())->method('insertOnDuplicate')->willReturn(1);
        $this->orderMock = $this->getMockBuilder(\Magento\Sales\Model\Order::class)
                        ->disableOriginalConstructor()
                        ->getMock();
        $this->_cortexRefundFatory = $this->getMockBuilder(CortexRefund::class) 
                    ->disableOriginalConstructor()
                        ->setMethods([
                            "addFieldToFilter","create","load","getCollection","addFieldToSelect","getMageOrderId","getId","getReturnRefId","getReturnAuthCode"
                        ])
                        ->getMock();
        $this->_cortexRefund = $this->objectManager->getCollectionMock(
            CortexRefundCollection::class,
            [$this->_cortexRefundFatory]
            );       
        $this->orderCollectionMock = $this->getMockBuilder(OrdersCollection::class)
                        ->disableOriginalConstructor()
                        ->setMethods([
                            "addFieldToFilter","getFirstItem","create","getCreditmemoId","getShippingCharges"
                        ])
                        ->getMock();        
        $this->creditmemoRepositoryMock = $this->getMockBuilder(CreditmemoRepositoryInterface::class)
                        ->disableOriginalConstructor()
                        ->setMethods([
                            "get","getBaseGrandTotal", "getList","getItems","getIterator","getIsCortexExported"
                        ])
                        ->getMockForAbstractClass();

        $this->_salesExport = new Salesexport(
            $this->_csvProcessorMock,
            $this->_directoryListMock,
            $this->_salesListCollectionMock,
            $this->orderCollectionMock,
            $this->_cortexFactoryMock,
            $this->_dateTimeMock,
            $this->_scopeConfigMock,
            $this->_customerRepositoryMock,
            $this->_orderFactoryMock,
            $this->_resourceMock,
            $this->_fileIoMock,
            $this->_directoryListFileMock,
            $this->_cortexRefundFatory,
            $this->creditmemoRepositoryMock,
            $this->searchCriteriaBuilder,
            $this->filterBuilder,
            $this->filterGroupBuilder
        );
    }

    public function testExecute()
    {
        $this->_salesListCollectionMock->expects($this->any())
        ->method('create')->willReturn($this->salesListCollectionMock);
        $this->salesListCollectionMock->expects($this->any())->method('addFieldToFilter')
        ->willReturnSelf();
        $this->_cortexRefundFatory->method("create")->willReturnSelf();
        $this->_cortexRefundFatory->method("getCollection")->willReturnSelf();
        $this->_cortexRefundFatory->method("addFieldToFilter")->willReturnSelf();
        $this->_cortexRefundFatory->method("addFieldToSelect")->willReturnSelf();
        $this->_cortexRefundFatory->method("load")->willReturn($this->_cortexRefund);
        $this->orderCollectionMock->method("create")->willReturnSelf();
        $this->orderCollectionMock->expects($this->any())
        ->method('addFieldToFilter')
        ->willReturnSelf();
        $this->orderCollectionMock->expects($this->any())
        ->method('getFirstItem')
        ->willReturnSelf();
        $this->salesListCollectionMock->expects($this->any())->method('getSelect')->willReturn($this->selectMock);
        $this->orderCollectionMock->method("getCreditmemoId")->willReturn(4);
        $this->selectMock->expects($this->any())->method('columns')->willReturnSelf();
        $this->selectMock->expects($this->any())->method('group')->willReturn([
                            "seller_id"=>"2",
                            "total_actual_seller_amount"=>"30",
                            "created_at"=>date("Y-m-d"),
                            "seller_total_commission"=>"50"
                            ]);
        $this->_cortexFactoryMock->expects($this->any())->method('create')->willReturnSelf();
        $this->_cortexFactoryMock->expects($this->any())->method('addData')->willReturnSelf();
        $this->_orderFactoryMock->expects($this->any())->method('create')->willReturnSelf();
        $this->_orderFactoryMock->expects($this->any())->method('getCollection')->willReturnSelf();
        $this->_orderFactoryMock->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $orders = [$this->orderMock];
        $this->_orderFactoryMock->expects($this->any())->method('setOrder')->willReturn($orders);
        $this->_customerRepositoryMock->method("getById")->willReturnSelf();
        $customerData["custom_attributes"] = [
            "wkv_cortex_merchants_id"=>["value"=>10],
            "wkv_cortex_terminal_id"=>["value"=>10]
                ];
        $this->_customerRepositoryMock->method("__toArray")->willReturn($customerData);
        $this->creditmemoRepositoryMock->method("get")->willReturnSelf();        
        $this->creditmemoRepositoryMock->method("getBaseGrandTotal")->willReturn(100);
        
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->creditmemoRepositoryMock->method("getList")->willReturnSelf();
        

        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);

        $this->creditmemoRepositoryMock->method("getItems")->willReturn($this->creditmemoRepositoryMock);
        $this->creditmemoRepositoryMock->expects($this->any())
            ->method('getIterator')
            ->willReturn(new \ArrayObject([]));
        $this->creditmemoRepositoryMock->method("getIsCortexExported")->willReturnSelf();
        
        $this->_salesExport->execute();
    }
}
