<?php
/**
 * CronConfigTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Test\Unit\Model\Config;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Arb\Cortex\Model\Cortex;
use Arb\Cortex\Model\Config\CronConfig;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class CronConfigTest for testing  Cortex class
 * @covers \Arb\Cortex\Model\Config\CronConfig
 */
class CronConfigTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
     
        $this->contextMock = $this->createMock(Context::class);
     
        $this->registryMock = $this->createMock(Registry::class);
     
        $this->scopeConfigInterfaceMock = $this->createMock(ScopeConfigInterface::class);
     
        $this->typeListInterfaceMock = $this->createMock(TypeListInterface::class);
     
        $this->valueFactoryMock = $this->getMockBuilder(\Magento\Framework\App\Config\ValueFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create',"load","save","setValue","setPath"])
            ->getMock();
        
        $this->abstractResourceMock = $this->createMock(AbstractResource::class);
        $this->abstractDbMock = $this->createMock(AbstractDb::class);
        $this->model = $this->objectManager->getObject(
            \Arb\Cortex\Model\Config\CronConfig::class,
            [
                'contextMock' => $this->contextMock,
                'registryMock' => $this->registryMock,
                'scopeConfigInterfaceMock' => $this->scopeConfigInterfaceMock,
                'typeListInterfaceMock' => $this->typeListInterfaceMock,
                'configValueFactory' => $this->valueFactoryMock,
                'abstractResourceMock' => $this->abstractResourceMock,
                'abstractDbMock' => $this->abstractDbMock,
                []
            ]
        );
        
        $this->model->setDataChanges(true);
    }

    /**
     * testAfterSave method
     */
    public function testAfterSave()
    {
        $path = "crontab/default/jobs/arb_cortex_salesexport/schedule/cron_expr";
        $this->valueFactoryMock->expects($this->once())->method("create")->willReturnSelf();
        $this->valueFactoryMock->expects($this->once())->method("load")->with($path, "path");
        $this->valueFactoryMock->expects($this->once())->method("setValue")->with("0 0 * * *")->willReturnSelf();
        $this->valueFactoryMock->expects($this->once())->method("setPath")->with($path);
        $this->valueFactoryMock->expects($this->once())->method("save")->willReturnSelf();

        $this->model->afterSave();
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(
            \Arb\Cortex\Model\Config\CronConfig::class,
            $this->model
        );
    }
}
