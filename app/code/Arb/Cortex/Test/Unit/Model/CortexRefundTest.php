<?php
/**
 * CortexRefundTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Test\Unit\Model;

use Arb\Cortex\Model\CortexRefund;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class CortexRefundTest for testing  Cortex class
 * @covers \Arb\Cortex\Model\CortexRefund
 */
class CortexRefundTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject("Arb\Cortex\Model\CortexRefund");
    }

    /**
     * testGetAvailableStatuses method
     */
    public function testGetIdentities()
    {
        $status = $this->model->getIdentities();
        $this->assertEquals(["0" => "arb_cortex_refunds_mapping_"], $status);
    }
}
