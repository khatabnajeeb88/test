<?php
/**
 * CortexTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Test\Unit\Model;

use Arb\Cortex\Model\Cortex;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class CortexTest for testing  Cortex class
 * @covers \Arb\Cortex\Model\Cortex
 */
class CortexTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject("Arb\Cortex\Model\Cortex");
    }

    /**
     * testGetAvailableStatuses method
     */
    public function testGetIdentities()
    {
        $status = $this->model->getIdentities();
        $this->assertEquals(["0" => "cortex_log_"], $status);
    }
}
