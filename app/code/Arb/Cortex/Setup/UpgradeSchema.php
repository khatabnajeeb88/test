<?php
/**
 * @category    Arb
 * @package     Arb_Cortex
 * @author Arb Magento Team
 */
namespace Arb\Cortex\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Arb_Cortex module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addCortexTable($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->addRefundCortexMappingTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $this->addCortexFieldInCreditMemoTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->updateFieldInRefundCortexMappingTable($setup);
        }
    }

    /**
     * Add cortex_log table
     * Add a new column in sales_order table
     * @codeCoverageIgnore
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addCortexTable(SchemaSetupInterface $setup)
    {
            //Add column in sales_order table
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'is_cortex_exported',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'nullable' => false,
                        'default' => 0,
                        'comment' => 'Sales Flat Order'
                    ]
            );
            //Create table 'cortex_log'
            $table = $setup->getConnection()->newTable(
                $setup->getTable('cortex_log')
            )->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Entity Id'
            )->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )->addColumn(
                'file_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Cortex File Name'
            )->addColumn(
                'error_message',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Cortex File Error Message'
            )->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => '0'],
                'Status'
            )->setComment(
                'Cortex File Generation Log'
            );
        $setup->getConnection()->createTable($table);
        return $this;
    }

    /**
     * Add arb_cortex_refunds_mapping table
     *
     * @codeCoverageIgnore
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addRefundCortexMappingTable(SchemaSetupInterface $setup){

        $table = $setup->getConnection()->newTable(
            $setup->getTable('arb_cortex_refunds_mapping')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Entity Id Primary Key'
        )->addColumn(
            'return_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            64,
            ['nullable' => true],
            'Return Type Refund or Void'
        )->addColumn(
            'return_ref_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            64,
            ['nullable' => false],
            'Reference Id from PG'
        )->addColumn(
            'return_auth_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            64,
            ['nullable' => false],
            'Refund Auth code'
        )->addColumn(
            'seller_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            12,
            ['nullable' => false, 'default' => '0'],
            'Seller ID'
        )->addColumn(
            'mage_order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            12,
            ['nullable' => false, 'default' => '0'],
            'Magento order id'
        )->addColumn(
            'is_cortex_exported_refund',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '0'],
            'Flag for cortex import'
        )->setComment(
            'Cortex File Generation Log for Refund and Void'
        );
        $setup->getConnection()->createTable($table);
        return $this;
    }

    /**
     * Add a new column in sales_creditmemo table
     * @codeCoverageIgnore
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addCortexFieldInCreditMemoTable(SchemaSetupInterface $setup)
    {
        //Add column in sales_creditmemo table
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_creditmemo'),
            'is_cortex_exported',
            [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Sales creditmemo'
                ]
        );
            
        return $this;
    }

    /**
     * update data type of the column in arb_cortex_refunds_mapping table
     * @codeCoverageIgnore
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function updateFieldInRefundCortexMappingTable(SchemaSetupInterface $setup)
    {
        //update column type in arb_cortex_refunds_mapping table
        $setup->getConnection()->changeColumn(
            $setup->getTable('arb_cortex_refunds_mapping'),
            'entity_id',
            'entity_id',
            [
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length'   => 11,
                'nullable' => false,
                'primary' => true,
                'identity' => true,
                'comment'  => 'Entity Id Primary Key'
            ]
        );
            
        return $this;
    }
}
