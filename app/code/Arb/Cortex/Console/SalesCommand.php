<?php
/**
 * This file consist of class Salesexport which is used to export sales data
 *
 * @category Arb
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Arb\Cortex\Cron\Salesexport;

class SalesCommand extends Command
{
    /*
    * Date parameter key for export
    */
    const DATE = "date";

    /**
     * @var Salesexport
     */
    protected $_salesexport;

    /**
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     *
     * @throws LogicException When the command name is empty
     */
    public function __construct(
        Salesexport $_salesexport,
        $name = null
    ) {
        $this->_salesexport = $_salesexport;
        parent::__construct($name);
    }

   /**
    * Define command attributes such as name, description, arguments
    */
    protected function configure()
    {
        $options = [
            new InputOption(
                self::DATE, // the option name
                '-d', // the shortcut
                InputOption::VALUE_REQUIRED, // the option mode
                'Pass date in date=YYYY-MM-DD format' // the description
            ),
        ];

        $this->setName('cortex:salesexport');
        $this->setDescription(__('Export sales data passing date'));
        $this->setDefinition($options);
        parent::configure();
    }

   /**
    * method will run when the command is called via console
    * @param InputInterface $input
    * @param OutputInterface $output
    * @return int|void|null
    */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(__("Welcome to Cortex Sales Report!"));
        //Exporting data for given date
        $this->_salesexport->execute();
    }
}
