<?php

/**
 * This file consist of class Salesexport which is used to export sales data
 *
 * @category Arb 
 * @package Arb_Cortex
 * @author Arb Magento Team
 *
 */
namespace Arb\Cortex\Cron;

/**
 * Consist of order data funtions
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */

use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory as SalesListCollection;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory as OrdersCollection;
use Magento\Framework\File\Csv;
use Magento\Framework\App\Filesystem\DirectoryList;
use Arb\Cortex\Model\CortexFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Filesystem\Driver\File as DirectoryListFile;
use Arb\Cortex\Model\CortexRefundFactory as CortexRefund;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class Salesexport
{
    /**
     * @var Csv
     */
    protected $_csvProcessor;

    /**
     * @var DirectoryList
     */
    protected $_directoryList;

     /**
      * @var SalesListCollection
      */
    private $_salesListCollection; 

    /**
      * @var OrdersCollection
      */
    private $_ordersCollection;

    /**
     * @var CortexFactory
     */
    protected $_cortexFactory;

    /**
     * @var File
     */
    protected $_fileIo;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * @var DirectoryListFile
     */
    protected $_directoryListFile;

    /**
     * @var \Arb\Cortex\Model\CortexRefund
     */
    protected $cortexRefund;

    /**
     * @var \Magento\Sales\Api\CreditmemoRepositoryInterface
     */
    protected $creditmemoRepository;

    /**
    * Order Currency SAR
    */
    const ORDER_CURRENCY = "SAR";

    /**
    * Set Loyalty as Zero
    */
    const LOYALTY = 0;

    /**
    * Order Status as complete
    */
    const ORDER_STATUS_COMPLETE = "complete";

    /**
    * Order Status as processing
    */
    const ORDER_STATUS_PROCESSING = "processing";

    /**
    * Order Status as Shipped
    */
    const ORDER_STATUS_SHIPPED = "shipped";

    /**
    * Order Status as Invoiced
    */
    const ORDER_STATUS_INVOICED = "invoiced";

    /**
    * Order Status as closed
    */
    const ORDER_STATUS_CLOSED = "closed";

    /**
    * Order Status as canceled
    */
    const ORDER_STATUS_CANCELED = "canceled";

    /**
    * Default Status for cortex log
    */
    const DEFAULT_STATUS = "0";

    /**
    * Successful file transfer Status for cortex log
    */
    const SUCCESS_STATUS = "1";

    /**
    * Falied file transfer Status for cortex log
    */
    const FAILED_STATUS = "2";

    /**
    * Not Available Option
    */
    const NOT_AVAILABLE = "NA";

    /**
     * @param Csv $_csvProcessor
     * @param DirectoryList $_directoryList
     * @param SalesListCollection $_salesListCollection
     * @param OrdersCollection $_ordersCollection
     * @param CortexFactory $_cortexFactory
     * @param DateTime $_dateTime
     * @param ScopeConfigInterface $_scopeConfig
     * @param CustomerRepositoryInterface $_customerRepository
     * @param OrderFactory $_orderFactory
     * @param File $_fileIo
     * @param DirectoryListFile $_directoryListFile
     * @param ResourceConnection $_resourceConnection
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Csv $_csvProcessor,
        DirectoryList $_directoryList,
        SalesListCollection $_salesListCollection,
        OrdersCollection $_ordersCollection,
        CortexFactory $_cortexFactory,
        DateTime $_dateTime,
        ScopeConfigInterface $_scopeConfig,
        CustomerRepositoryInterface $_customerRepository,
        OrderFactory $_orderFactory,
        ResourceConnection $_resourceConnection,
        File $_fileIo,
        DirectoryListFile $_directoryListFile,
        CortexRefund $cortexRefund,
        CreditmemoRepositoryInterface $creditmemoRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->_csvProcessor = $_csvProcessor;
        $this->_directoryList = $_directoryList;
        $this->_salesListCollection = $_salesListCollection;
        $this->_ordersCollection = $_ordersCollection;
        $this->_cortexFactory = $_cortexFactory;
        $this->_dateTime = $_dateTime;
        $this->_scopeConfig = $_scopeConfig;
        $this->_customerRepository = $_customerRepository;
        $this->_orderFactory = $_orderFactory;
        $this->_resourceConnection = $_resourceConnection;
        $this->connection = $_resourceConnection->getConnection();
        $this->_fileIo = $_fileIo;
        $this->_directoryListFile = $_directoryListFile;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->_data = [];
        $this->_storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Cortex_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->cortexRefund = $cortexRefund;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_refundReturn = [];
    }

    /**
     * @return void
     * generating CSV file, logging in cortex_log
     */
    public function execute()
    {
        $this->logger->info("Cortex CRON has been started. - ".$this->_dateTime->date("Y-m-d H:i:s"));

        $fileName = "MP_".date("Ymd").".csv";
        $magentoDir = $this->_scopeConfig->getValue("cortex/general/magentodirpath", $this->_storeScope);
        $varDir = $this->_directoryList->getPath("var");
        $this->_fileIo->mkdir($varDir."/".$magentoDir, 0777);
        $filePath = $varDir."/".$magentoDir."/".$fileName;

        //get data to write in CSV
        $salesData = $this->getSalesData();

        $this->_csvProcessor
            ->setDelimiter(',')
            ->setEnclosure('"')
            ->saveData(
                $filePath,
                $salesData
            );
        $statusCode = self::SUCCESS_STATUS;
        $success =__("File has been generated successfully");

        if (count($this->_data)) {
            $this->insertMultiple("sales_order", $this->_data);
            $this->logger->info("Purchase Orders Data to export. - ".$this->_dateTime->date("Y-m-d H:i:s"));
            $this->logger->info("Total number of purchase record ".count($this->_data)." export");
        } else {
            $this->logger->info("No purchase Orders to export. - ".$this->_dateTime->date("Y-m-d H:i:s"));
        }

        if (count($this->_refundReturn)) {
            $this->insertMultiple("arb_cortex_refunds_mapping", $this->_refundReturn);
            $this->logger->info("Return Refund to export. - ".$this->_dateTime->date("Y-m-d H:i:s"));
            $this->logger->info("Total number of refund record ".count($this->_refundReturn)." export");
        } else {
            $this->logger->info("No Return Refund to export. - ".$this->_dateTime->date("Y-m-d H:i:s"));
        }

        //Adding data in cortex_log
        $cortexModel = $this->_cortexFactory->create();
        $cortexData =   [
                        "status"=>$statusCode,
                        "file_name"=>$fileName,
                        "created_at"=>$this->_dateTime->date("Y-m-d"),
                        "error_message"=>$success
                        ];
        $cortexModel->addData($cortexData);
        $cortexModel->save();

        //Adding data in cortex_log
        $this->logger->info("Cortex CRON has been executed. - ".$this->_dateTime->date("Y-m-d H:i:s"));
        return $this;
    }

    /**
     * Get sales data from Marketplace
     * @return array $salesData
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function getSalesData()
    {
        $salesData[] =  [
                        "PG Transaction Reference Number",
                        "Transaction Date",
                        "Amount",
                        "Currency",
                        "Authorization Number",
                        "Merchant ID",
                        "Cortex Terminal ID",
                        "Commission",
                        "Loyalty Usage"
                        ];

        //gather purchases data for Virtual and physical products
        $orderCollection = $this->_orderFactory
            ->create()
            ->getCollection()
            ->addFieldToFilter("is_cortex_exported", self::DEFAULT_STATUS)
            ->addFieldToFilter('status', ["in"=>
                [
                self::ORDER_STATUS_PROCESSING,
                self::ORDER_STATUS_COMPLETE,
                self::ORDER_STATUS_SHIPPED,
                self::ORDER_STATUS_INVOICED,
                self::ORDER_STATUS_CLOSED,
                self::ORDER_STATUS_CANCELED
                ]
            ])
            ->setOrder('entity_id', 'ASC');

        $cdata = [];
        foreach ($orderCollection as $order) {
            $this->logger->info("order - ".$order->getId()." with status - ".$order->getStatus());
            if ($order->getStatus() == 'canceled') {
                $payment = $order->getPayment();
                if ($payment) {
                    $paymentData = $payment->getAdditionalInformation();
                    if (is_array($paymentData) && !empty($paymentData)) {
                        if (isset($paymentData['raw_details_info']['result']) && $paymentData['raw_details_info']['result'] == 'CAPTURED') {
                            foreach ($this->getOrderSalesData($order,"purchase") as $data) {
                                $this->logger->info("purchase Order Id (status canceled due to refund) - ".$order->getId());
                                $salesData[] = $data; 
                            }  
                        } else {
                            $cdata[] =[
                                "entity_id"=>$order->getId(),
                                "is_cortex_exported"=>self::SUCCESS_STATUS
                            ];
                        }
                    } else {
                        $cdata[] =[
                            "entity_id"=>$order->getId(),
                            "is_cortex_exported"=>self::SUCCESS_STATUS
                        ];
                    }
                } else {
                    $cdata[] =[
                        "entity_id"=>$order->getId(),
                        "is_cortex_exported"=>self::SUCCESS_STATUS
                    ];
                }
            } else {                
                foreach ($this->getOrderSalesData($order,"purchase") as $data) {
                    $this->logger->info("purchase Order Id - ".$order->getId());
                    $salesData[] = $data; 
                }
            }
        }

        if (count($cdata)) {
            $this->logger->info("canceled Orders without payment");
            $this->logger->info($cdata);
            $this->insertMultiple("sales_order", $cdata);
        }

        //refunds and reversal orders for Physical products
        $reverseRefunds = $this->cortexRefund->create()
            ->getCollection()
            ->addFieldToFilter("is_cortex_exported_refund", self::DEFAULT_STATUS)
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('mage_order_id')
            ->addFieldToSelect('return_ref_id')
            ->addFieldToSelect('return_auth_code')
            ->load();
        
        foreach ($reverseRefunds as $data) {            
           

            $details = [
                'return_ref_id' => $data->getReturnRefId(),
                'return_auth_code' => $data->getReturnAuthCode()
            ];

            $orderCollection = $this->_orderFactory
                ->create()
                ->getCollection()
                ->addFieldToFilter('entity_id', $data->getMageOrderId())
                ->setOrder('entity_id', 'ASC');
                
            foreach ($orderCollection as $order) {
                foreach ($this->getOrderSalesData($order,"refund", $details) as $data) {
                    $this->logger->info("Refund Order Id - ".$order->getId());
                    $salesData[] = $data; 
                }  
            }
        }
        
        $salesData = array_filter($salesData);
        $this->logger->info($salesData);
        return $salesData;
    }

    /**
     * Insert Multiple record to improve performance
     * @param string $table
     * @param array $data
     * @return void
     */
    private function insertMultiple($table, $data)
    {
        $tableName = $this->_resourceConnection->getTableName($table);
        return $this->connection->insertOnDuplicate($tableName, $data);
    }

    private function getOrderSalesData($order, $type, $details=[]) {
        $salesData = [];
        $orderId = $order->getId();
        
        //get Payment data from Order
        $payment = $order->getPayment();
        $paymentData = !empty($payment) ? $payment->getData() : [];
        $referenceNo = !empty($paymentData["arb_ref_id"])?$paymentData["arb_ref_id"]:self::NOT_AVAILABLE;
        $authCode = !empty($paymentData["arb_auth_code"])?$paymentData["arb_auth_code"]:self::NOT_AVAILABLE; 

        $_webkulSalelists = $this->_salesListCollection->create()
            ->addFieldToFilter("seller_id", ["neq"=>self::DEFAULT_STATUS])
            ->addFieldToFilter("order_id", $orderId)
            ->addFieldToFilter("parent_item_id", ['null' => true]);   

        $_webkulSalelists->getSelect()
            ->columns([
                'total_actual_seller_amount' => new \Zend_Db_Expr('SUM(total_amount+total_tax-applied_coupon_amount)'),
                'seller_total_commission' => new \Zend_Db_Expr('SUM(total_commission)')
            ])
            ->group('seller_id');

        $_webkulSalelists->setOrder("order_id", "ASC");

        //Prepare Seller wise payment Data
        foreach ($_webkulSalelists as $_webkulSalelist) {
            $customerId = $_webkulSalelist["seller_id"];

            //Adding Delivery charges for Physical.
            $_ordersCollection = $this->_ordersCollection->create()
                ->addFieldToFilter("seller_id", $customerId)
                ->addFieldToFilter("order_id", $orderId); 

            $markteplaceOrder = $_ordersCollection->getFirstItem();
            //Get delivery charges from Order.
            $shippingCharges = $markteplaceOrder->getShippingCharges();
            //Get Data from marketplace_saleslist table
            //Total Amount
            $totalAmount = $shippingCharges + $_webkulSalelist["total_actual_seller_amount"];
            $actualSellerAmount = number_format($totalAmount, 2, ".", "");
            $transactionDate = $this->_dateTime->date("d-M-Y", strtotime($_webkulSalelist["created_at"]));
            $commission = number_format($_webkulSalelist["seller_total_commission"], 2, ".", "");
            //Get Seller Attributes to set Data for Cortex Merchant ID/Cortex Terminal ID
            $customer = $this->_customerRepository->getById($customerId);
            $cortexMerchantId = self::NOT_AVAILABLE;
            $cortexTerminalId = self::NOT_AVAILABLE;
            $customerAttrData = $customer->__toArray();
            if (isset($customerAttrData["custom_attributes"])) {
                $customAttributes = $customerAttrData["custom_attributes"];
                if (isset($customAttributes["wkv_cortex_merchants_id"])) {
                    $cortexMerchantId = $customAttributes["wkv_cortex_merchants_id"]["value"];
                }
                if (isset($customAttributes["wkv_cortex_terminal_id"])) {
                    $cortexTerminalId = $customAttributes["wkv_cortex_terminal_id"]["value"];
                }
            }

            //check for refund
            if($type=="refund" && $markteplaceOrder->getCreditmemoId() == 0) {
                continue;
            }

            //check if already exported order but refund is in progress.
            //if Refund is initiated.
            if ($markteplaceOrder->getCreditmemoId() != 0 && $type=="refund") {
                $creditMemoIds = $markteplaceOrder->getCreditmemoId();

                //calculate refund amount in case of multiple creditmemo
                $creditMemoIdArray = explode(',', $creditMemoIds);
                $actualSellerAmount = 0;
                $creditData = [];
                $isCortexExported = 0;
                foreach ($creditMemoIdArray as $creditMemoId) {                    
                    $searchCriteria = $this->searchCriteriaBuilder->addFilter('entity_id', $creditMemoId)->create();
                    $creditmemos = $this->creditmemoRepository->getList($searchCriteria);
                    $creditmemoRecords = $creditmemos->getItems();

                    foreach ($creditmemoRecords as $creditMemoData) {
                        $isCortexExported = $creditMemoData->getIsCortexExported();
                        //Check if creditmemo already exported in cortex
                        if (!$isCortexExported) {
                            //Actual Amount refunded by the seller with shipping charges.
                            $refundAmount = $creditMemoData->getBaseGrandTotal();
                            $actualSellerAmount += number_format($refundAmount, 2, ".", "");

                            $creditData[] =[
                                "entity_id"=>$creditMemoId,
                                "is_cortex_exported"=>self::SUCCESS_STATUS
                            ];   
                        } 
                    }                
                }
                
                //Set Commision is zero on refund.
                $commission=0;

                $referenceNo = $details['return_ref_id'];
                $authCode = $details['return_auth_code'];  
                
                if (count($creditData)) {
                    $this->logger->info("Refunded Orders credit memo Ids");
                    $this->logger->info($creditData);
                    $this->insertMultiple("sales_creditmemo", $creditData);

                    $this->_refundReturn[] = [
                        "entity_id"=> $order->getId(),
                        "mage_order_id" => $order->getIncrementId(),
                        "is_cortex_exported_refund"=>self::SUCCESS_STATUS
                    ];
                } else{
                    continue;
                } 
            }
            
            $salesData[] =  [
                $referenceNo,
                $transactionDate,
                $actualSellerAmount,
                self::ORDER_CURRENCY,
                $authCode,
                $cortexMerchantId,
                $cortexTerminalId,
                $commission,
                self::LOYALTY
                ];    
        }

        if ($type != 'refund') {
            $this->_data[] =[
                "entity_id"=>$orderId,
                "is_cortex_exported"=>self::SUCCESS_STATUS
            ];
        }       
        return $salesData;
    }
}