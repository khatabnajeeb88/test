<?php
/**
 * Esb Send notification and Email Notification
 *
 * @category Arb
 * @package Arb_EsbNotifications
 * @author Arb Magento Team
 *
 */
namespace Arb\EsbNotifications\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Math\Random;

/**
 * EsbNotifications data helper
 *
 * @api
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var Curl
     */
    protected $_curl;

    /**
     * @var Random
     */
    protected $_random;

    /**
    * Default code for sucessful email sent
    */
    const DEFAULT_SUCCESS_CODE = "I000000";

    /**
    * MAXIMUM ATTEMPTS
    */
    const MAX_ATTEMPTS = "3";

    /**
    * Input string for generating random string
    */
    const RANDOM_STRING = "abcdef0123456789";

    /**
    * Template Event Code for SMS
    */
    const SMS_EVENT_CODE = "VIS01";

    /**
    * Template Event Code for Email
    */
    const EMAIL_EVENT_CODE = "MKT01";

    /**
    * Svc id for ESB templete
    */
    const SVC_ID = "0080";

    /**
    *  Sub Svc id for ESB templete
    */
    const SUBSVC_ID = "0500";

    /**
    * Funcation id for ESB templete
    */
    const FUN_ID = "0002";

    /**
    * Version for ESB templete
    */
    const MSG_VER = "1.0";

    /**
    * OSID ESB templete
    */
    const OSID = "00";

    /**
     * @param Context $context
     * @param DateTime $_dateTime
     * @param Curl $_curl
     * @param Random $_random
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        DateTime $_dateTime,
        Curl $_curl,
        Random $_random
    ) {
        $this->_dateTime = $_dateTime;
        $this->_curl = $_curl;
        $this->_random = $_random;
        $this->_storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/ESB_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        parent::__construct($context);
    }

    /**
     * Sending OTP via SMS to Phone Number
     *
     * @param array $smsData
     *
     * @return json array
     */
    public function sendSmsNotification($smsData = [])
    {
            $data = $this->getDataArray($smsData);
            $this->logger->info("Order Id is  - ".$data["orderNumber"] . " - to send SMS");
            $this->logger->info("Request Id is  - ".$data["requestId"] . " - to send SMS");
            $endPoint = $this->scopeConfig->getValue("esbnotifications/sms/sms_end_point", $this->_storeScope);
            $xmlRequest = '<?xml version="1.0" encoding="UTF-8"?>
              <soapenv:Envelope
                    xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope"
                    xmlns:alr="http://www.alrajhiwebservices.com/"
                    xmlns:ejad="http://www.ejada.com/">
                 <soapenv:Body>
                    <ejad:NotificationSendRq>
                       <ejad:Hdr>
                          <alr:Msg>
                             <alr:RqID>'.$data["requestId"].'</alr:RqID>
                             <alr:SvcID>'.self::SVC_ID.'</alr:SvcID>
                             <alr:SubSvcID>'.self::SUBSVC_ID.'</alr:SubSvcID>
                             <alr:FuncID>'.self::FUN_ID.'</alr:FuncID>
                             <alr:MsgTimestamp>'.$data["dateStamp"].'</alr:MsgTimestamp>
                             <alr:MsgVer>'.self::MSG_VER.'</alr:MsgVer>
                          </alr:Msg>
                          <alr:Agt>
                             <alr:UserLang>'.$data["storeCode"].'</alr:UserLang>
                          </alr:Agt>
                          <alr:Sys>
                             <alr:ChID>OTHER</alr:ChID>
                             <alr:SessionID>'.$data["requestId"].'</alr:SessionID>
                             <alr:OSID>'.self::OSID.'</alr:OSID>
                             <alr:SessionLang>'.$data["storeCode"].'</alr:SessionLang>
                          </alr:Sys>
                       </ejad:Hdr>
                       <ejad:Body>
                          <ejad:EventCode>'.self::SMS_EVENT_CODE.'</ejad:EventCode>
                          <ejad:RecipsList>
                             <ejad:RecipInfo>
                                <ejad:LangPref>'.$data["storeCode"].'</ejad:LangPref>
                                <ejad:NotificationMethodInfo>
                                   <ejad:NotificationMethod>SMS</ejad:NotificationMethod>
                                   <ejad:Contact>'.$data["phonenumber"].'</ejad:Contact>
                                </ejad:NotificationMethodInfo>
                                    <ejad:ParamList>
                                     <ejad:ParamItem>
                                        <ejad:ParamCd>MERCHANT_NAME</ejad:ParamCd>
                                        <ejad:ParamVal>'.$data["sellerName"].'</ejad:ParamVal>
                                     </ejad:ParamItem>
                                     <ejad:ParamItem>
                                        <ejad:ParamCd>VOUCHER_NUMBER</ejad:ParamCd>
                                        <ejad:ParamVal>'.$data["voucherName"].'</ejad:ParamVal>
                                     </ejad:ParamItem>
                                     <ejad:ParamItem>
                                        <ejad:ParamCd>REF_NUMBER</ejad:ParamCd>
                                        <ejad:ParamVal>'.$data["refNo"].'</ejad:ParamVal>
                                     </ejad:ParamItem>
                                     <ejad:ParamItem>
                                        <ejad:ParamCd>MERCHANT_PHONE_NUMBER</ejad:ParamCd>
                                        <ejad:ParamVal>'.$data["sellerPhoneNo"].'</ejad:ParamVal>
                                     </ejad:ParamItem>
                                </ejad:ParamList>
                             </ejad:RecipInfo>
                          </ejad:RecipsList>
                       </ejad:Body>
                    </ejad:NotificationSendRq>
                 </soapenv:Body>
              </soapenv:Envelope>
              ';
        for ($i=0; $i < self::MAX_ATTEMPTS; $i++) {
            $xmlResponse = $this->_callSoapClient($endPoint, $xmlRequest);
            if ($xmlResponse == self::DEFAULT_SUCCESS_CODE) {
                $this->logger->info("Success code received from ESB is ". $xmlResponse. " - to send SMS");
                return json_encode(["success"=>true]);
            }
        }
          $this->logger->info("Error code received from ESB is ". $xmlResponse. " - to send SMS");
          return json_encode(["success"=>false]);
    }

    /**
     * Sending order detail email
     *
     * @param array $data
     *
     * @return json array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    public function sendEmailNotification($data = [])
    {
            $data = $this->getDataArray($data);
            $this->logger->info("Order Id is  - ".$data["orderNumber"] . " - to send Email");
            $this->logger->info("Request Id is  - ".$data["requestId"] . " - to send Email");
            $endPoint = $this->scopeConfig->getValue("esbnotifications/email/email_end_point", $this->_storeScope);
            $xmlRequest = '<soapenv:Envelope
            xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope"
            xmlns:alr="http://www.alrajhiwebservices.com/"
            xmlns:ejad="http://www.ejada.com/">
                               <soapenv:Body>
                                  <ejad:NotificationSendRq>
                                     <ejad:Hdr>
                                        <alr:Msg>
                                           <alr:RqID>'.$data["requestId"].'</alr:RqID>
                                           <alr:SvcID>'.self::SVC_ID.'</alr:SvcID>
                                           <alr:SubSvcID>'.self::SUBSVC_ID.'</alr:SubSvcID>
                                           <alr:FuncID>'.self::FUN_ID.'</alr:FuncID>
                                           <alr:MsgTimestamp>'.$data["dateStamp"].'</alr:MsgTimestamp>
                                           <alr:MsgVer>'.self::MSG_VER.'</alr:MsgVer>
                                        </alr:Msg>
                                        <alr:Agt>
                                           <alr:UserLang>'.$data["storeCode"].'</alr:UserLang>
                                        </alr:Agt>
                                        <alr:Sys>
                                           <alr:ChID>OTHER</alr:ChID>
                                           <alr:SessionID>'.$data["requestId"].'</alr:SessionID>
                                           <alr:OSID>'.self::OSID.'</alr:OSID>
                                           <alr:SessionLang>'.$data["storeCode"].'</alr:SessionLang>
                                        </alr:Sys>
                                     </ejad:Hdr>
                                     <ejad:Body>
                                        <ejad:EventCode>'.self::EMAIL_EVENT_CODE.'</ejad:EventCode>
                                        <ejad:RecipsList>
                                           <ejad:RecipInfo>
                                              <ejad:LangPref>'.$data["storeCode"].'</ejad:LangPref>
                                              <ejad:NotificationMethodInfo>
                                                 <ejad:NotificationMethod>EMAIL</ejad:NotificationMethod>
                                                 <ejad:Contact>'.$data["email"].'</ejad:Contact>
                                                 <ejad:EmailFrom/>
                                                 <ejad:EmailCC/>
                                                 <ejad:EmailBCC/>
                                                 <ejad:EmailReplyTo/>
                                                 <ejad:EmailSubject/>
                                              </ejad:NotificationMethodInfo>
                                              <ejad:EventChan/>
                                             <ejad:ParamList>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>CUST_FULL_NAME</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["customerName"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>ORDER_NUMBER</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["orderNumber"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>MERCHANT_NAME</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["sellerName"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>PRODUCT_NAME</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["productName"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>VOUCHER_NUMBER</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["voucherName"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>REF_NUMBER</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["refNo"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>VOUCHER_AMOUNT</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["voucherAmount"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>MERCHANT_PHONE_NUMBER</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["sellerPhoneNo"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>TOTAL_AMOUNT</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["totalAmount"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>TRANSACTION_ID</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["tranId"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                                 <ejad:ParamItem>
                                                    <ejad:ParamCd>PAYMENT_ID</ejad:ParamCd>
                                                    <ejad:ParamVal>'.$data["paymentId"].'</ejad:ParamVal>
                                                 </ejad:ParamItem>
                                              </ejad:ParamList>
                                           </ejad:RecipInfo>
                                        </ejad:RecipsList>
                                     </ejad:Body>
                                  </ejad:NotificationSendRq>
                               </soapenv:Body>
                            </soapenv:Envelope>';
        for ($i=0; $i < self::MAX_ATTEMPTS; $i++) {
            $xmlResponse = $this->_callSoapClient($endPoint, $xmlRequest);
            if ($xmlResponse == self::DEFAULT_SUCCESS_CODE) {
                $this->logger->info("Success code received from ESB is ". $xmlResponse. " - to send Email");
                return json_encode(["success"=>true]);
            }
        }
           $this->logger->info("Error code received from ESB is ". $xmlResponse. " - to send Email");
           return json_encode(["success"=>false]);
    }

    /**
     * call SOAP Client for EBS API
     *
     * @param string $endPoint
     * @param string $xmlRequest
     *
     * @return string $xmlResponseStatus
     */
    private function _callSoapClient($endPoint, $xmlRequest)
    {
        try {
            //Curl request header set for SOAP API
            $this->_curl->setHeaders(['Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($xmlRequest)]);
            $this->_curl->setOption(CURLOPT_RETURNTRANSFER, 1);
            $this->_curl->post($endPoint, $xmlRequest);
            $xmlResponse = $this->_curl->getBody();
            // Convert XML response to Array
            $xmlResponse = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xmlResponse);
            $xml = simplexml_load_string($xmlResponse);
            $xmlResponse = json_decode(json_encode((array)$xml), true);
            $ejadNotificationSendRs = $xmlResponse["soapenvBody"]["ejadNotificationSendRs"];
            $xmlResponseStatus = $ejadNotificationSendRs["ejadHdr"]["arbStatus"]["arbStatusCd"];            
        } catch (\Exception $e) {
            //return SOAP Client Error
            $xmlResponseStatus=$e->getMessage();
        }

        return $xmlResponseStatus;
    }

    /**
     * Get Random String
     *
     * @param null
     *
     * @return string
     */
    private function getRandomString()
    {
        $id = $this->_random->getRandomString(8, self::RANDOM_STRING)."-";
        $id .= $this->_random->getRandomString(4, self::RANDOM_STRING)."-";
        $id.= $this->_random->getRandomString(4, self::RANDOM_STRING)."-";
        $id.= $this->_random->getRandomString(4, self::RANDOM_STRING)."-";
        $id.= $this->_random->getRandomString(12, self::RANDOM_STRING);
        return $id;
    }

     /**
      * Get Data Array
      *
      * @param Array $data
      *
      * @return Array
      * @SuppressWarnings(PHPMD.CyclomaticComplexity)
      * @SuppressWarnings(PHPMD.NPathComplexity)
      * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
      */
    private function getDataArray($data = [])
    {
        $result = [];
        $result["email"] = !empty($data["email"]) ?  $data["email"] : "";
        $result["phonenumber"] = !empty($data["phonenumber"]) ?  $data["phonenumber"] : "";
        $result["customerName"] = !empty($data["customer_name"]) ?  $data["customer_name"] : "";
        $result["orderNumber"] = !empty($data["order_number"]) ?  $data["order_number"] : "";
        $result["voucherName"] = !empty($data["voucher_name"]) ?  $data["voucher_name"] : "";
        $result["sellerName"] = !empty($data["seller_name"]) ?  $data["seller_name"] : "";
        $result["sellerPhoneNo"] = !empty($data["seller_phone_no"]) ?  $data["seller_phone_no"] : "";
        $result["voucherAmount"] = !empty($data["voucher_amount"]) ?  $data["voucher_amount"] : "";
        $result["totalAmount"] = !empty($data["total_amount"]) ?  $data["total_amount"] : "";
        $result["productName"] = !empty($data["product_name"]) ?  $data["product_name"] : "";
        $result["refNo"] = !empty($data["ref_no"]) ?  $data["ref_no"] : "";
        $result["tranId"] = !empty($data["tran_id"]) ?  $data["tran_id"] : "";
        $result["paymentId"] = !empty($data["payment_id"]) ?  $data["payment_id"] : "";
        $result["requestId"] = $this->getRandomString();
        $result["dateStamp"] = str_replace(" ", "T", $this->_dateTime->date());
        $result["storeCode"] = !empty($data["store"]) ?  $data["store"] : "EN";
        return $result;
    }
}
