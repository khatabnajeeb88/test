<?php
/**
 * DataTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_EsbNotifications
 * @author Arb Magento Team
 *
 */
namespace Arb\EsbNotifications\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\EsbNotifications\Helper\Data as ArbHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Math\Random;

/**
 * Class DataTest for testing  EsbNotifications class
 * @covers \Arb\EsbNotifications\Helper\Data
 */
class DataTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(Context::class);
        $this->_dateTimeMock = $this->createMock(DateTime::class);
        $this->_scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);
        $this->context->expects($this->any())->method('getScopeConfig')->willReturn($this->_scopeConfigMock);
        $this->_curlMock = $this->getMockBuilder(Curl::class)->getMock();
        $this->_randomMock = $this->createMock(Random::class);
        $this->_arbHelper = new ArbHelper(
            $this->context,
            $this->_dateTimeMock,
            $this->_curlMock,
            $this->_randomMock
        );
    }

    /**
     * testSendSmsNotification method
     */
    public function testSendSmsNotification()
    {
         $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body>
      <ejad:NotificationSendRs xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">
         <ejad:Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </ejad:Hdr>
      </ejad:NotificationSendRs>
   </soapenv:Body>
</soapenv:Envelope>';
        $this->_curlMock->method("getBody")->willReturn($xml);
        $this->_scopeConfigMock->expects($this->any())
            ->method('getValue')
            ->with("esbnotifications/sms/sms_end_point")
            ->willReturn("http://172.21.161.79:7886/NotificationSend");
        $data = [
                    "phonenumber" => "919624345498",
                    "otp"=>"1253"
                ];
        $this->_arbHelper->sendSmsNotification($data);
        $data = [
                    "phonenumber" => "919624345498"
                ];
        $this->_arbHelper->sendSmsNotification($data);
    }

    /**
     * testSendSmsNotificationFailed method
     */
    public function testSendSmsNotificationFailed()
    {
        $this->_scopeConfigMock->expects($this->any())
            ->method('getValue')
            ->with("esbnotifications/sms/sms_end_point")
            ->willReturn("http://172.21.161.79:7886/NotificationSend");
        $data = [
                    "phonenumber" => "919624345498",
                    "otp"=>"1253"
                ];
        $this->_arbHelper->sendSmsNotification($data);
        $data = [
                    "phonenumber" => "919624345498"
                ];
        $this->_arbHelper->sendSmsNotification($data);
    }
     /**
      * testSendEmailNotification method
      */
    public function testSendEmailNotification()
    {
         $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body>
      <ejad:NotificationSendRs xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">
         <ejad:Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </ejad:Hdr>
      </ejad:NotificationSendRs>
   </soapenv:Body>
</soapenv:Envelope>';
        $this->_curlMock->method("getBody")->willReturn($xml);
        $this->_scopeConfigMock
            ->expects($this->any())
            ->method('getValue')
            ->with("esbnotifications/email/email_end_point")
            ->willReturn("http://172.21.161.79:7886/NotificationSend");
       
        $data = [
                "email"=>"parth.p.pandya@accenture.com",
                "ccEmail"=>"neeraj.b.garg@accenture.com",
                "customer_name"=>"Parth Pandya",
                "order_number"=>"#00012010102",
                "voucher_name"=>"APRIL_OFFER",
                "seller_name"=>"Niraj G",
                "seller_phone_no"=>"123421916",
                "voucher_amount"=>"120",
                "total_amount"=>"1531",
                "product_name"=>"April-0102",
                "ref_no"=>"ref123-762",
                "tran_id"=>"fdwfshoifweaoihsd723868231",
                "payment_id"=>"dsahkla21698216892316"
                ];
        $this->_arbHelper->sendEmailNotification($data);
    }

     /**
      * testSendEmailNotificationFailed method
      */
    public function testSendEmailNotificationFailed()
    {
        $this->_scopeConfigMock
            ->expects($this->any())
            ->method('getValue')
            ->with("esbnotifications/email/email_end_point")
            ->willReturn("http://172.21.161.79:7886/NotificationSend");
       
        $data = [
                "email"=>"parth.p.pandya@accenture.com",
                "ccEmail"=>"neeraj.b.garg@accenture.com",
                "customer_name"=>"Parth Pandya",
                "order_number"=>"#00012010102",
                "voucher_name"=>"APRIL_OFFER",
                "seller_name"=>"Niraj G",
                "seller_phone_no"=>"123421916",
                "voucher_amount"=>"120",
                "total_amount"=>"1531",
                "product_name"=>"April-0102",
                "ref_no"=>"ref123-762",
                "tran_id"=>"fdwfshoifweaoihsd723868231",
                "payment_id"=>"dsahkla21698216892316"
                ];
        $this->_arbHelper->sendEmailNotification($data);
    }
}
