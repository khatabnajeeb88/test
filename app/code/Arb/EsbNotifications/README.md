## Synopsis
This module used for sending SMS and email through ESB api.

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details [module.xml](etc/module.xml).
3. Module system configuration details in [config.xml](etc/config.xml).
4. Admin ACL configuration [acl.xml](etc/acl.xml).
5. Admin menu routing [routes.xml](etc/adminhtml/routes.xml).
6. Admin system configuration [system.xml](etc/adminhtml/routes.xml)

## Tests
Unit tests could be found in the [Test/Unit](Test/Unit) directory.
