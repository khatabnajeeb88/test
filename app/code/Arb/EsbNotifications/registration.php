<?php
/**
 * Module registration file
 *
 * @category Arb
 * @package Arb_EsbNotifications
 * @author Arb Magento Team
 */

 // @codeCoverageIgnoreStart
/** it is a default magento module registration code */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_EsbNotifications',
    __DIR__
);
// @codeCoverageIgnoreEnd
