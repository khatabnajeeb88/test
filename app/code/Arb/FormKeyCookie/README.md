## Synopsis
Magento core does not create form key cookie if domain or ip is not used. In DEV and UAT we are not using domain, to fix issue of Invalid form key enable configuration

## Technical feature

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module configuration available through Stores->Configuration [system.xml](etc/adminhtml/system.xml)
4. ACL configuration [acl.xml](etc/acl.xml)
5. Dependency injection configuration [di.xml](etc/di.xml)
6. Module configuration [module.xml](etc/module.xml)
7. Disables plugin `page_cache_from_key_from_cookie` of file `vendor/magento/module-page-cache/Plugin/RegisterFormKeyFromCookie.php `
8. New plugin created for `Magento\Framework\App\FrontControllerInterface`

This module depends on `PageCache`


