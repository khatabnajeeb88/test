<?php
/**
 * sets form key cookie if not available
 *
 * @category    Arb
 * @package     Arb_FormKeyCookie
 * @author      Arb Magento Team
 */
 namespace Arb\FormKeyCookie\PageCache\Plugin;

use Magento\Framework\App\PageCache\FormKey as CacheFormKey;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Escaper;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Allow for registration of a form key through cookies.
 */
class RegisterFormKeyFromCookie
{
   /**
   * Form key configuration settings
   */
    const XML_PATH_FORMKEY_SETTINGS = 'formkeycookie/general/enable';

    /**
     * @var CacheFormKey
     */
    private $cookieFormKey;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var FormKey
     */
    private $formKey;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var ConfigInterface
     */
    private $sessionConfig;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param CacheFormKey $cacheFormKey
     * @param Escaper $escaper
     * @param FormKey $formKey
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param ConfigInterface $sessionConfig
     */
    public function __construct(
        CacheFormKey $cacheFormKey,
        Escaper $escaper,
        FormKey $formKey,
        CookieMetadataFactory $cookieMetadataFactory,
        ConfigInterface $sessionConfig,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->cookieFormKey = $cacheFormKey;
        $this->escaper = $escaper;
        $this->formKey = $formKey;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionConfig = $sessionConfig;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Set form key from the cookie.
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeDispatch()
    {
        if ($this->cookieFormKey->get()) {
            $this->updateCookieFormKey($this->cookieFormKey->get());

            $this->formKey->set(
                $this->escaper->escapeHtml($this->cookieFormKey->get())
            );
        }

        if ((!$this->cookieFormKey->get()) &&
            ($this->scopeConfig->getValue(
                self::XML_PATH_FORMKEY_SETTINGS,
                ScopeInterface::SCOPE_STORE
            )
        )) {
            $formKey = $this->formKey->getFormKey();
            $this->updateCookieFormKey($formKey);
        }
    }

    /**
     * Set form key cookie
     *
     * @param string $formKey
     * @return void
     */
    private function updateCookieFormKey(string $formKey)
    {
        $cookieMetadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata();
        $cookieMetadata->setDomain($this->sessionConfig->getCookieDomain());
        $cookieMetadata->setPath($this->sessionConfig->getCookiePath());
        $cookieMetadata->setSecure($this->sessionConfig->getCookieSecure());
        $lifetime = $this->sessionConfig->getCookieLifetime();
        if ($lifetime !== 0) {
            $cookieMetadata->setDuration($lifetime);
        }

        $this->cookieFormKey->set(
            $formKey,
            $cookieMetadata
        );
    }
}
