<?php
/**
 * sets form key cookie if not available
 *
 * @category    Arb
 * @package     Arb_FormKeyCookie
 * @author      Arb Magento Team
 */
namespace Arb\FormKeyCookie\Test\Unit\Plugin;

use PHPUnit\Framework\TestCase;
use Magento\Framework\App\PageCache\FormKey as CacheFormKey;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Escaper;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Arb\FormKeyCookie\PageCache\Plugin\RegisterFormKeyFromCookie;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;

/**
 * @covers \Arb\FormKeyCookie\PageCache\Plugin\RegisterFormKeyFromCookie
 */
class RegisterFormKeyFromCookieTest extends TestCase
{
    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_cacheFormKey;

    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_escaper;

    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_formKey;

    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_cookieMetadataFactory;

    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_configInterface;

    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_scopeConfigInterface;

    /**
     * @var ProductRepositoryInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $_registerFormKeyFromCookie;

    /**
     * test for RegisterFormKeyFromCookie
     *
     * @return void
     */
    public function setUp()
    {
        $this->_cacheFormKey = $this->createMock(CacheFormKey::class);
        $this->_escaper = $this->createMock(Escaper::class);
        $this->_formKey = $this->createMock(FormKey::class);
        $this->_cookieMetadataFactory = $this->getMockBuilder(CookieMetadataFactory::class)
            ->setMethods([
                'createPublicCookieMetadata'
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $this->_publicCookieMetadata = $this->getMockBuilder(PublicCookieMetadata::class) ->setMethods([
                'setDomain',
                'setPath',
                'setSecure',
                "setDuration"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->_configInterface = $this->createMock(ConfigInterface::class);
        $this->_scopeConfigInterface = $this->createMock(ScopeConfigInterface::class);

        $this->_registerFormKeyFromCookie = new RegisterFormKeyFromCookie(
            $this->_cacheFormKey,
            $this->_escaper,
            $this->_formKey,
            $this->_cookieMetadataFactory,
            $this->_configInterface,
            $this->_scopeConfigInterface
        );
    }

    public function testBeforeDispatch()
    {
        $this->_registerFormKeyFromCookie->beforeDispatch();
    }

    public function testBeforeDispatchWithData()
    {
        $this->_cacheFormKey->method("get")->willReturn(true);
        $this->_cookieMetadataFactory->method("createPublicCookieMetadata")->willReturn($this->_publicCookieMetadata);
        $this->_publicCookieMetadata->method("setDomain")->willReturnSelf();
        $this->_publicCookieMetadata->method("setPath")->willReturnSelf();
        $this->_publicCookieMetadata->method("setSecure")->willReturnSelf();
        $this->_publicCookieMetadata->method("setDuration")->willReturnSelf();
        //$this->_cacheFormKey->method("set")->willReturnSelf();
        $this->_registerFormKeyFromCookie->beforeDispatch();
    }

    public function testBeforeDispatchFailed()
    {
        $this->_cacheFormKey->method("get")->willReturn(false);
        $this->_formKey->method("getFormKey")->willReturn("qskjagskjgkjsagufuwe");
        $this->_scopeConfigInterface->method("getValue")->willReturn(true);
        $this->_cookieMetadataFactory->method("createPublicCookieMetadata")
        ->willReturn($this->_publicCookieMetadata);
        $this->_publicCookieMetadata->method("setDomain")->willReturnSelf();
        $this->_publicCookieMetadata->method("setPath")->willReturnSelf();
        $this->_publicCookieMetadata->method("setSecure")->willReturnSelf();
        $this->_publicCookieMetadata->method("setDuration")->willReturnSelf();
        $this->_registerFormKeyFromCookie->beforeDispatch();
    }
}
