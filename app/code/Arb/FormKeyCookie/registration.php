<?php
/**
 * Form key override registration file
 *
 * @category Arb
 * @package Arb_FormKeyCookie
 * @author Arb Magento Team
 *
 */
// @codeCoverageIgnoreStart
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_FormKeyCookie',
    __DIR__
);
// @codeCoverageIgnoreEnd
