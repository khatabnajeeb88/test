<?php
/**
 * This file used for installation schema
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @covers Arb\LdapLogin\Setup
 *
 * @codeCoverageIgnore
 * Ignoring code coverage as setup file cannot be tested as per the magento standards
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws StateException
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (!extension_loaded('ldap')) {
            throw new StateException(__('PHP ldap extension is not present!'));
        }

        $adminUserTableName = $setup->getTable('admin_user');

        $connection = $setup->getConnection();

        $connection->addColumn($adminUserTableName, 'ldap_dn', [
            'type' => Table::TYPE_TEXT,
            'nullable' => true,
            'comment' => 'LDAP user dn',
        ]);

        $setup->endSetup();
    }
}
