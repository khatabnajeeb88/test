<?php
/**
 * This file used for setup uninstall
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\Ldap\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

/**
 * Class Uninstall
 *
 * @covers Arb\LdapLogin\Setup
 *
 * @codeCoverageIgnore
 * Ignoring code coverage as setup file cannot be tested as per the magento standards
 */
class Uninstall implements UninstallInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context) //NOSONAR
    {
        $setup->startSetup();

        $adminUserTableName = $setup->getTable('admin_user');

        $connection = $setup->getConnection();

        $connection->dropColumn($adminUserTableName, 'ldap_dn');

        $setup->endSetup();
    }
}
