<?php
/**
 * This file consist block view file to disable input fields for ldap users
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */

namespace Arb\LdapLogin\Block\System\Account\Edit;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * admin view function
 */
class Main extends \Magento\User\Block\User\Edit\Tab\Main
{

    /**
     * Prepare form fields
     *
     * @return Form
     */
    protected function _prepareForm()
    {
        $result = parent::_prepareForm();

        /** @var $model \Magento\User\Model\User */
        $model = $this->_coreRegistry->registry('permissions_user');

        $isLdapUser = $model->getLdapDn();

        if (strlen(trim($isLdapUser)) === 0) {
            return $result;
        }

        $fieldsToDisable = ['username', 'firstname', 'lastname', 'email', 'password', 'password_confirmation'];

        /** @var AbstractElement[] $field */
        $fields = $result->getForm()->getElements();

        foreach ($fields as $field) {
            /** @var AbstractElement $element */
            foreach ($field->getElements() as $element) {
                if (in_array($element->getName(), $fieldsToDisable)) {
                    $element->setReadonly(true);
                }
            }
        }

        return $result;
    }
}
