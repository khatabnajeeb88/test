<?php
/**
 * This file consist of interface for lofin bind methodes
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
 
namespace Arb\LdapLogin\Api;

/**
 * Interface LdapClientInterface for ldap interface
 *
 * @covers Arb\LdapLogin\Api
 */
interface LdapClientInterface
{
    /**
     * @return ldap_connection
     */
    public function bind();

    /**
     * Try to bind with the ldap server
     *
     * @return boolean true if ldap is connected otherwise false
     */
    public function canBind();

    /**
     * A global LDAP search routine for finding information of a user.
     *
     * @param [type] $username
     * @param [type] $password
     * @return void
     */
    public function getUserByUsername($username, $password);
}
