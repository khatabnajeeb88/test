<?php
/**
 * This file consist all the configration details used in this module
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */

namespace Arb\LdapLogin\Api;

/**
 * calss for config interface
 */
interface ConfigInterface
{
    /**
     * deployment configuration path
     */
    const CONFIG_KEY_ACTIVE = 'ldap/general/active';
    const CONFIG_KEY_HOST = 'ldap/ldap_connection/ldap_servers';
    const CONFIG_KEY_PORT = 'ldap/ldap_connection/ldap_port';
    const CONFIG_KEY_BASE_DN = 'ldap/ldap_connection/ldap_base_dn';
    const CONFIG_KEY_BIND_DN = 'ldap/ldap_connection/ldap_bind_dn';
    const CONFIG_KEY_ROLE = 'ldap/ldap_connection/ldap_user_role';
    const CONFIG_KEY_STATUS = 'ldap/ldap_connection/user_status';
    const CONFIG_KEY_USER_FILTER = 'ldap/ldap_connection/ldap_user_filter';
    const CONFIG_TLS_ENABLED = 'ldap/ldap_connection/ldap_tsl_certi';

    const CONFIG_KEY_ATTRIBUTE_USERNAME = 'ldap/ldap_attributes/ldap_username';
    const CONFIG_KEY_ATTRIBUTE_FIRST_NAME = 'ldap/ldap_attributes/ldap_first_name';
    const CONFIG_KEY_ATTRIBUTE_LAST_NAME = 'ldap/ldap_attributes/ldap_last_name';
    const CONFIG_KEY_ATTRIBUTE_EMAIL = 'ldap/ldap_attributes/ldap_email';
    const CONFIG_KEY_ATTRIBUTEPWD = 'ldap/ldap_attributes/ldap_password';

    /**
     * default values
     */
    const DEFAULT_LDAP_OPT_PROTOCOL_VERSION = 3;
    const DEFAULT_LDAP_OPT_REFERRALS = 0;
    const DEFAULT_CACHE_PASSWORD = false;
    const DEFAULT_ALLOW_EMPTY_PASSWORD = false;

    /**
     * return if the module is active
     *
     * @return boolean
     */
    public function isLdapActive();

    /**
     * Returns the LDAP user filter
     *
     * @return string
     */
    public function getUserFilter();

    /**
     * Returns true if it's allowed to cache the users password otherwise false
     *
     * @return boolean
     */
    public function getCachePassword();

    /**
     * Returns the attribute key in LDAP defining the user’s username.
     *
     * @return string
     */
    public function getAttributeNameUsername();

    /**
     * Returns the attribute key in LDAP defining the user’s first name.
     *
     * @return string
     */
    public function getAttributeNameFirstName();

    /**
     * Returns the attribute key in LDAP defining the user’s last name.
     *
     * @return string
     */
    public function getAttributeNameLastName();

    /**
     * Returns the technical role id
     *
     * @return integer
     */
    public function getDefaultRoleId();

    /**
     * Returns the default user role id to set
     *
     * @return integer
     */
    public function getDefaultUserStatus();

    /**
     * Returns the attribute key in LDAP defining the user’s email.
     *
     * @return string
     */
    public function getAttributeNameEmail();

    /**
     * Returns the attribute key in LDAP defining the user’s encryped password.
     *
     * @return string
     */
    public function getAttributeNamePassword();

    /**
     * Returns an prepared array for the ldap connector
     *
     * @return array
     */
    public function getLdapConnectionOptions($username);

    /**
     * Returns the ldap host
     *
     * @return string
     */
    public function getHost();

    /**
     * Returns the ldap port
     *
     * @return string
     */
    public function getPort();

    /**
     * Returns the bind dn
     *
     * @return string
     */
    public function getBindDn();

    /**
     * Returns the base dn where
     *
     * @return string
     */
    public function getBaseDn();

    /**
     * Returns  ldap opt protocal
     *
     * @return boolean
     */
    public function getLdapOptProtocal();

    /**
     * returns ldap opt referals
     *
     * @return void
     */
    public function getLdapOptReferals();

    /**
     * Returns true if empty passwords are allowed
     *
     * @return boolean
     */
    public function getAllowEmptyPassword();

     /**
      * Returns true need the TLS certificate enabled
      *
      * @return boolean
      */
    public function getTLSCerti();
}
