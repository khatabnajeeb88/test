<?php
/**
 * This file used for creating user object for existing users
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Plugin\User\Model;

use Closure;
use Magento\User\Model\User;

/**
 * Class UserPlugin
 *
 * @package Arb\LdapLogin\Plugin\User\Model
 */
 // @codingStandardsIgnoreStart
final class UserPlugin
{
     // @codingStandardsIgnoreEnd
    /**
     * Skip before save methods if user comes from ldap
     *
     * @param User $subject
     * @param Closure $proceed
     * @return User
     */
    public function aroundValidateBeforeSave(User $subject, Closure $proceed)
    {
        // only validate non ldap users
        /** @noinspection PhpUndefinedMethodInspection */
        if (strlen(trim($subject->getLdapDn())) === 0) {
            return $proceed();
        }

        return $subject;
    }
}
