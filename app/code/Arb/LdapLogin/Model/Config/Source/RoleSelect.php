<?php
/**
 * This file consist role list for role select in admin ldap settings
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
 
namespace Arb\LdapLogin\Model\Config\Source;

use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;

/**
 * class to get admin roles in dropdown
 */
class RoleSelect implements \Magento\Framework\Data\OptionSourceInterface
{

    protected $roleCollectionFactory;

    public function __construct(
        \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $roleCollectionFactory
    ) {
        $this->roleCollectionFactory = $roleCollectionFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        
        $roles = $this->roleCollectionFactory->create();
        $roles->addFieldToFilter('role_type', RoleGroup::ROLE_TYPE);
        $rolesData = [];

        foreach ($roles as $getRole) {
            $rolesData[] = ['value' => $getRole->getRoleId(), 'label' => $getRole->getRoleName()];
        }
        
        return $rolesData;
    }
}
