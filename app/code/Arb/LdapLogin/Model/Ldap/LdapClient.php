<?php
/**
 * This file consist of Ldap client connection
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Model\Ldap;

use Exception;
use Arb\LdapLogin\Api\LdapClientInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\RequestInterface;

/**
 * Class LdapClient for Ldap connection
 *
 * @covers Arb\LdapLogin\Model\Ldap
 */
class LdapClient implements LdapClientInterface
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var Ldap_connect
     */
    private $ldap;

     /**
      * @var RequestInterface
      */
    protected $request;

    /**
     * @var \Zend\Log\Logger
     */
    private $logger;

    /**
     * first item indexer
     *
     * @var integer
     */
    private $getIndexer = 0;

    /**
     * LdapClient constructor.
     *
     * @param Configuration $configuration
     * @param LoggerInterface $logger
     */
    public function __construct(
        Configuration $configuration,
        RequestInterface $request
    ) {
        $this->configuration = $configuration;
        $this->request = $request;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/ldap_login-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * checks the ldap connection to provided username & password
     *
     * @param string $username
     * @param string $password
     * @return array
     *
     * @SuppressWarnings('all')
     */
    public function getUserByUsername($username, $password)
    {
        $this->bind($username, $password);

		$query = str_replace('%s', $username, $this->configuration->getUserFilter());

        try {
            $ldapDcn = $this->configuration->getBaseDn();
            // @codingStandardsIgnoreStart
            $ldapSearch = ldap_search($this->ldap, $ldapDcn, "($query)");

            $ldapData = ldap_get_entries($this->ldap, $ldapSearch);

            $passwordIndex = $this->configuration->getAttributeNamePassword();
            if ($passwordIndex === '' || empty($passwordIndex)) {
                $ldapData[$this->getIndexer]['userpassword'][$this->getIndexer]
                    = $password;
            } else {
                $ldapData[$this->getIndexer]['userpassword'][$this->getIndexer]
                    = $ldapData[$this->getIndexer][$passwordIndex][$this->getIndexer];
            }

            // @codingStandardsIgnoreEnd
            if ($ldapData['count'] > 0) {
                $ldapData = $ldapData[$this->getIndexer];
                $this->logger->info(__('Ldap Success: Login with login username '.$username));
                $ldapSucessResult = [
                    $this->configuration->getAttributeNameUsername() => $ldapData
                        [$this->configuration->getAttributeNameUsername()][$this->getIndexer],
                    $this->configuration->getAttributeNameFirstName() => $ldapData
                        [$this->configuration->getAttributeNameFirstName()][$this->getIndexer],
                    $this->configuration->getAttributeNameLastName() => $ldapData
                        [$this->configuration->getAttributeNameLastName()][$this->getIndexer],
                    $this->configuration->getAttributeNameEmail() => $ldapData
                        [$this->configuration->getAttributeNameEmail()][$this->getIndexer],
                ];
                $this->logger->info($ldapSucessResult);
                return $ldapData;
            } else {
                $this->logger->crit('User not active for LDAP login.');
                throw new LocalizedException(__('User not active for LDAP login.'));
            }
        } catch (Exception $e) {
            $this->logger->crit('Ldap Error getUser: '.$e->getMessage());
            throw new LocalizedException(__('Login temporarily deactivated.'.
            ' Check with administrator for more information.'));
        }
    }

    /**
     * bind with the ldap server
     *
     * @param string $username
     * @param string $password
     * @return boolean true if ldap is connected otherwise false
     */
    public function bind($username = '', $password = '')
    {
        if ($username == '' || $password == '') {
            throw new LocalizedException(__('Please provide username & password'));
        }

        if ($this->ldap === null) {
            $ldapDcn = $this->configuration->getLdapConnectionOptions($username);

             // @codingStandardsIgnoreStart
            $this->ldap = ldap_connect(
                $this->configuration->getHost(),
                $this->configuration->getPort()
            );

            //set ldap options
            ldap_set_option($this->ldap, LDAP_OPT_PROTOCOL_VERSION, $this->configuration->getLdapOptProtocal());
            ldap_set_option($this->ldap, LDAP_OPT_REFERRALS, $this->configuration->getLdapOptReferals());

            // binding to ldap server
            if ($this->configuration->getTLSCerti()) {
                if (ldap_start_tls($this->ldap)) {
                    ldap_bind($this->ldap, $ldapDcn, $password);
                }
            } else {
                ldap_bind($this->ldap, $ldapDcn, $password);
            }
            // @codingStandardsIgnoreEnd
        }
    }

     /**
     * Try to bind with the ldap server
     *
     * @param string $username
     * @param string $password
     * @return boolean true if ldap is connected otherwise false
     */
    public function canBind($username = '', $password = '')
    {
        try {
            if ($this->configuration->isLdapActive()) {
                try {
                    $this->bind($username, $password);
                } catch (Exception $e) {
                    $this->logger->crit('Ldap Error canBind : '.$e->getMessage());
                    throw new LocalizedException(__('Login temporarily deactivated.'.
                    ' Check with administrator for more information.'));
                }
            } else {
                throw new LocalizedException(__('The account sign-in was incorrect or your'.
                'account is disabled temporarily. Please wait and try again later.'));
            }
        } catch (Exception $e) {
            $this->logger->crit('Ldap Error canBind : '.$e->getMessage());
            throw new LocalizedException(__($e->getMessage()));
        }

        return true;
    }
}
