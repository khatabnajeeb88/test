<?php
/**
 * This file consist of password check for admin login
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */

namespace Arb\LdapLogin\Model\Ldap;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class PasswordValidator for chekcing password encreption
 *
 * @covers Arb\LdapLogin\Model\Ldap
 */
class PasswordValidator
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * LdapClient constructor.
     *
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param string $password
     * @param string $hash
     * @return bool
     * @throws LocalizedException
     * @SuppressWarnings('all')
     */
    public function validatePassword($password, $hash)
    {
        // empty password
        if (empty($hash)) {
            return $this->configuration->getAllowEmptyPassword() === '1' && $password === $hash;
        }

        // plaintext password
        if ($hash{0} !== '{') {
            return $password === $hash;
        }

        if (strpos($hash, '{crypt}') === 0) {
            $encryptedPassword = '{crypt}' . crypt($password, substr($hash, 7));
        } elseif (strpos($hash, '{CRYPT}') === 0) {
            $encryptedPassword = '{CRYPT}' . crypt($password, substr($hash, 7));
        } elseif (strpos($hash, '{SSHA}') === 0) {
            $salt = substr(base64_decode(substr($hash, 6)), 20);
            $encryptedPassword = '{SSHA}' . base64_encode(sha1($password . $salt, true) . $salt);
        } else {
            throw new LocalizedException(__('Unsupported password hash format'));
        }

        return $hash === $encryptedPassword;
    }

    /**
     * @return bool
     */
    public function isPasswordCachedAllowed()
    {
        return $this->configuration->getCachePassword();
    }
}
