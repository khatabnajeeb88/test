<?php
/**
 * This file used for loin user mapping
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Model\Ldap;

use Magento\User\Model\User;

/**
 * Class UserMapper used for loin user mapping
 *
 * @covers Arb\LdapLogin\Model\Ldap
 */
class UserMapper
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * UserMapper constructor.
     *
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param array $ldapAttributes
     * @param string $password
     * @param User $user
     * @return void
     */
    public function mapUser($ldapAttributes, $password, User $user)
    {
        $user->setFirstName($this->getFirstName($ldapAttributes));
        $user->setLastName($this->getLastName($ldapAttributes));
        $user->setUserName($this->getUsername($ldapAttributes));
        $user->setEmail($this->getEmail($ldapAttributes));

        /** @noinspection PhpUndefinedMethodInspection */
        $user->setLdapDn($ldapAttributes['dn']);

        if ($user->isObjectNew()) {
            $user->setIsActive($this->configuration->getDefaultUserStatus());
            /** @noinspection PhpUndefinedMethodInspection */
            $user->setRoleId($this->configuration->getDefaultRoleId());
        }

        if ($this->configuration->getCachePassword()) {
            $user->setPassword($password);
        } else {
            $user->setPassword(uniqid());
        }
    }

    /**
     * @param array $ldapAttributes
     * @return string
     */
    private function getFirstName($ldapAttributes)
    {
        return $this->getFirstAttribute($ldapAttributes, $this->configuration->getAttributeNameFirstName());
    }

    /**
     * @param array $ldapAttributes
     * @param string $name
     * @return string
     */
    private function getFirstAttribute($ldapAttributes, $name)
    {
        return isset($ldapAttributes[$name]) ? $ldapAttributes[$name][0] : '';
    }

    /**
     * @param array $ldapAttributes
     * @return string
     */
    private function getLastName($ldapAttributes)
    {
        return $this->getFirstAttribute($ldapAttributes, $this->configuration->getAttributeNameLastName());
    }

    /**
     * @param array $ldapAttributes
     * @return string
     */
    private function getUsername($ldapAttributes)
    {
        return $this->getFirstAttribute($ldapAttributes, $this->configuration->getAttributeNameUsername());
    }

    /**
     * @param array $ldapAttributes
     * @return string
     */
    private function getEmail($ldapAttributes)
    {
        return $this->getFirstAttribute($ldapAttributes, $this->configuration->getAttributeNameEmail());
    }
}
