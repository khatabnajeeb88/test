<?php
/**
 * This file consist role list for role select in admin ldap settings
 *
 * @category Arb
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */

namespace Arb\LdapLogin\Model\Ldap;

use Arb\LdapLogin\Api\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Configuration for getting config from admin
 *
 * @covers Arb\LdapLogin\Model\Ldap
 */
class Configuration implements ConfigInterface
{
    /**
     * scope config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Configuration constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * return if the module is active
     *
     * @return boolean
     */
    public function isLdapActive()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_ACTIVE);
    }

    /**
     * Returns the LDAP user filter
     *
     * @return string
     */
    public function getUserFilter()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_USER_FILTER);
    }

    /**
     * Returns the attribute key in LDAP defining the user’s username.
     *
     * @return string
     */
    public function getAttributeNameUsername()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_ATTRIBUTE_USERNAME);
    }

    /**
     * Returns the attribute key in LDAP defining the user’s first name.
     *
     * @return string
     */
    public function getAttributeNameFirstName()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_ATTRIBUTE_FIRST_NAME);
    }

    /**
     * Returns the attribute key in LDAP defining the user’s last name.
     *
     * @return string
     */
    public function getAttributeNameLastName()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_ATTRIBUTE_LAST_NAME);
    }

    /**
     * Returns the technical role id
     *
     * @return integer
     */
    public function getDefaultRoleId()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_ROLE);
    }

    /**
     * Returns the default user role id to set
     *
     * @return integer
     */
    public function getDefaultUserStatus()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_STATUS);
    }

    /**
     * Returns true if it's allowed to cache the users password otherwise false
     *
     * @return boolean
     */
    public function getCachePassword()
    {
        return ConfigInterface::DEFAULT_CACHE_PASSWORD;
    }

    /**
     * Returns the attribute key in LDAP defining the user’s email.
     *
     * @return string
     */
    public function getAttributeNameEmail()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_ATTRIBUTE_EMAIL);
    }

    /**
     * Returns the attribute key in LDAP defining the user’s encryped password.
     *
     * @return string
     */
    public function getAttributeNamePassword()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_ATTRIBUTEPWD);
    }

    /**
     * Returns an prepared array for the ldap connector
     *
     * @return array
     */
    public function getLdapConnectionOptions($username)
    {
        $bindDn = $this->getBindDn();
        return str_replace('%s', $username, $bindDn);
    }

     /**
     * Returns the ldap host
     *
     * @return string
     */
    public function getHost()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_HOST);
    }

     /**
     * Returns the ldap port
     *
     * @return string
     */
    public function getPort()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_PORT);
    }

    /**
     * Returns the bind dn
     *
     * @return string
     */
    public function getBindDn()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_BIND_DN);
    }

    /**
     * Returns the base dn where
     *
     * @return string
     */
    public function getBaseDn()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_KEY_BASE_DN);
    }

    /**
     * Returns  ldap opt protocal
     *
     * @return boolean
     */
    public function getLdapOptProtocal()
    {
        return ConfigInterface::DEFAULT_LDAP_OPT_PROTOCOL_VERSION;
    }

    /**
     * get Ldap Opt Referals
     *
     * @return int
     */
    public function getLdapOptReferals()
    {
        return ConfigInterface::DEFAULT_LDAP_OPT_REFERRALS;
    }

    /**
     * Returns true if empty passwords are allowed
     *
     * @return boolean
     */
    public function getAllowEmptyPassword()
    {
        return ConfigInterface::DEFAULT_ALLOW_EMPTY_PASSWORD;
    }

    /**
     * Returns true need the TLS certificate enabled
     *
     * @return boolean
     */
    public function getTLSCerti()
    {
        return $this->scopeConfig->getValue(ConfigInterface::CONFIG_TLS_ENABLED);
    }
}
