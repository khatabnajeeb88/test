<?php
/**
 * This file consist of PHPUnit test case for class PasswordChecker
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Model\Ldap;

use Arb\LdapLogin\Model\Ldap\Configuration;
use Arb\LdapLogin\Model\Ldap\PasswordValidator;
use Magento\Framework\Exception\LocalizedException;
use PHPUnit\Framework\TestCase;

/**
 * Class PasswordValidatorTest PHPUnit test case for class PasswordValidator
 *
 * @covers Arb\LdapLogin\Model\Ldap\PasswordValidator
 */
class PasswordValidatorTest extends TestCase
{
    /**
     * Holds a mock of Configuration
     *
     * @var Configuration|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $configuration;
    /**
     * @var PasswordValidator
     */
    private $passwordValidator;

    protected function setUp()
    {
        $this->configuration = $this->getMockBuilder(Configuration::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->passwordValidator = new PasswordValidator($this->configuration);
    }

    public function testSimple()
    {
        static::assertTrue($this->passwordValidator->validatePassword('123', '123'));
    }

    public function testEmptyAllowed()
    {
        // setup mock functions
        $this->configuration->expects(static::once())
            ->method('getAllowEmptyPassword')
            ->willReturn('1');

        static::assertTrue($this->passwordValidator->validatePassword('', ''));
    }

    public function testEmptyPermit()
    {
        // setup mock functions
        $this->configuration->expects(static::once())
            ->method('getAllowEmptyPassword')
            ->willReturn('0');

        static::assertFalse($this->passwordValidator->validatePassword('', ''));
    }

    public function testCrypt()
    {
        static::assertTrue($this->passwordValidator->validatePassword('helloworld', '{crypt}0pnSC65.QhkYc'));
    }

    public function testCryptCaps()
    {
        static::assertTrue($this->passwordValidator->validatePassword('helloworld', '{CRYPT}0pnSC65.QhkYc'));
    }

    public function testSSHA()
    {
        $hash = '{SSHA}UqeHYFMhsBo/mwjFP1rNxcmKP9//OZmK';

        static::assertTrue($this->passwordValidator->validatePassword('abcd123', $hash));
    }

    /**
     * @expectedException LocalizedException
     */
    public function testUnsupportedHashFormat()
    {

        static::expectException(LocalizedException::class);
        $this->passwordValidator->validatePassword('12345', '{NOT}12345');
        //static::expectException($this->passwordValidator->validatePassword('12345', '{NOT}12345'));
    }

    public function testIsPasswordCachedAllowed()
    {
        static::assertNull($this->passwordValidator->isPasswordCachedAllowed());
    }
}
