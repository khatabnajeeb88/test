<?php
/**
 * This file consist of PHPUnit test case for class Configuration
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Model\Ldap;

use Arb\LdapLogin\Api\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Arb\LdapLogin\Model\Ldap\Configuration;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigurationTest phpUnit for Configuration
 *
 * @covers Arb\LdapLogin\Model\Ldap\Configuration
 */
class ConfigurationTest extends TestCase
{
    /**
     * scope config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $configSet;

    protected function setUp()
    {
        $this->scopeConfig = $this->getMockBuilder(ScopeConfigInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->configSet = new Configuration($this->scopeConfig);
    }

    public function testIsLdapActive()
    {
        $this->assertNull($this->configSet->isLdapActive());
    }

    public function testGetUserFilter()
    {
        $this->assertNull($this->configSet->getUserFilter());
    }
    public function testGetAttributeNameUsername()
    {
        $this->assertNull($this->configSet->getAttributeNameUsername());
    }
    public function testTetAttributeNameFirstName()
    {
        $this->assertNull($this->configSet->getAttributeNameFirstName());
    }
    public function testGetAttributeNameLastName()
    {
        $this->assertNull($this->configSet->getAttributeNameLastName());
    }
    public function testGetDefaultRoleId()
    {
        $this->assertNull($this->configSet->getDefaultRoleId());
    }
    public function testTetDefaultUserStatus()
    {
        $this->assertNull($this->configSet->getDefaultUserStatus());
    }
    public function testGetCachePassword()
    {
        $this->assertFalse($this->configSet->getCachePassword());
    }
    public function testGetAttributeNameEmail()
    {
        $this->assertNull($this->configSet->getAttributeNameEmail());
    }
    public function testGetAttributeNamePassword()
    {
        $this->assertNull($this->configSet->getAttributeNamePassword());
    }
    public function testGetLdapConnectionOptions()
    {
        $this->assertEquals('', $this->configSet->getLdapConnectionOptions('adminuser'));
    }
    public function testGetHost()
    {
        $this->assertNull($this->configSet->getHost());
    }
    public function testGetPort()
    {
        $this->assertNull($this->configSet->getPort());
    }
    public function testGetBindDn()
    {
        $this->assertNull($this->configSet->getBindDn());
    }
    public function testGetBaseDn()
    {
        $this->assertNull($this->configSet->getBaseDn());
    }
    public function testGetLdapOptProtocal()
    {
        $this->assertEquals(3, $this->configSet->getLdapOptProtocal());
    }
    public function testGetLdapOptReferals()
    {
        $this->assertEquals(0, $this->configSet->getLdapOptReferals());
    }
    public function testGetAllowEmptyPassword()
    {
        $this->assertFalse($this->configSet->getAllowEmptyPassword());
    }
    public function testGetTLSCerti()
    {
        $this->assertNull($this->configSet->getTLSCerti());
    }
}
