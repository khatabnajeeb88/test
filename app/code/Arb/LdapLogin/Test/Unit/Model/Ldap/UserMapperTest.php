<?php
/**
 * This file consist of PHPUnit test case for class UserMapper
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Model\Ldap;

use Magento\User\Model\User;
use Arb\LdapLogin\Model\Ldap\Configuration;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use \Arb\LdapLogin\Model\Ldap\UserMapper;

/**
 * Class UserMapper UserMapperTest for  UserMapper
 *
 * @covers Arb\LdapLogin\Model\Ldap\UserMapper
 */
class UserMapperTest extends TestCase
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ldapAttributes
     */
    private $ldapAttributes;

    /**
     * @var userMapper
     */
    private $userMapper;

    /**
     * @var user
     */
    private $user;

    /**
     * Main set up method
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->configuration = $this->getMockBuilder(Configuration::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->user = $this->createPartialMock(
            User::class,
            [
                'loadByUsername',
                'setFirstName',
                'setLastName',
                'setUserName',
                'setEmail',
                'setIsActive',
                'load',
                'setData'
            ]
        );

        $this->testObject = $this->objectManager->getObject(
            UserMapper::class,
            [
                'configuration' => $this->configuration
            ]
        );

        $this->ldapAttributes =  [
            'count' => '1',
            0 =>
             [
              'objectclass' =>
               [
                'count' => '7',
                0 => 'top',
                1 => 'person',
                2 => 'organizationalPerson',
                3 => 'inetOrgPerson',
                4 => 'shadowAccount',
                5 => 'posixAccount',
                6 => 'jumpcloudUser',
              ],
              0 => 'objectclass',
              'uidnumber' =>
               [
                'count' => '1',
                0 => '5002',
              ],
              1 => 'uidnumber',
              'description' =>
               [
                'count' => '1',
                0 => 'Magento 2 admin 2',
              ],
              2 => 'description',
              'uid' =>
               [
                'count' => '1',
                0 => 'jadmin',
              ],
              3 => 'uid',
              'userpassword' =>
               [
                'count' => '1',
                0 => '{CRYPT}$6$WFDdmuqb25tTlNUG$mHkH0ypmfo1cbXrksM5JMBH2..
                  SNoGin28jvzl9ye7qxBvXDsmxwLGRhc3d76hFtgurm9pDUVJKmVO/xvB2Uw.',
              ],
              4 => 'userpassword',
              'gidnumber' =>
               [
                'count' => '1',
                0 => '5002',
              ],
              5 => 'gidnumber',
              'jcldapadmin' =>
               [
                'count' => '1',
                0 => 'TRUE',
              ],
              6 => 'jcldapadmin',
              'loginshell' =>
               [
                'count' => '1',
                0 => '/bin/bash',
              ],
              7 => 'loginshell',
              'givenname' =>
               [
                'count' => '1',
                0 => 'madmin',
              ],
              8 => 'givenname',
              'sn' =>
               [
                'count' => '1',
                0 => 'admin',
              ],
              9 => 'sn',
              'cn' =>
               [
                'count' => '1',
                0 => 'madmin admin',
              ],
              10 => 'cn',
              'homedirectory' =>
               [
                'count' => '1',
                0 => '/home/jadmin',
              ],
              11 => 'homedirectory',
              'mail' =>
               [
                'count' => '1',
                0 => 'jadmin@alcenture.com',
              ],
              12 => 'mail',
              'displayname' =>
               [
                'count' => '1',
                0 => 'jadmin',
              ],
              13 => 'displayname',
              'memberof' =>
               [
                'count' => '1',
                0 => 'cn=Magneto Admin,ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com',
              ],
              14 => 'memberof',
              'count' => '15',
              'dn' => 'uid=jadmin,ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com',
            ],
        ];
    }

    /**
     * Test Map user
     *
     * @return void
     */
    public function testMapUser()
    {
        $password = 'admin@123';

        $this->assertNull(null, $this->testObject
          ->mapUser($this->ldapAttributes[0], $password, $this->user));
    }

    /**
     * Test Map user
     *
     * @return void
     */
    public function testMapUserSetActiveUser()
    {
        $password = 'admin@123';

        $this->user->setIsObjectNew(true);
        $this->user->setIsActive(1);
        $this->configuration->method('getCachePassword')->willReturn(true);
        $this->assertNull(null, $this->testObject
          ->mapUser($this->ldapAttributes[0], $password, $this->user));
    }

    /**
     * Test Map user
     *
     * @return void
     */
    public function testMapUserSetInActiveUser()
    {
        $password = 'admin@123';

        $this->user->setIsObjectNew(true);
        $this->user->setIsActive(0);

        $this->assertNull($this->testObject
          ->mapUser($this->ldapAttributes[0], $password, $this->user));
    }
}
