<?php
/**
 * This file consist of PHPUnit test case for class LdapClient
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Model\Ldap;

use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Arb\LdapLogin\Model\Ldap\Configuration;
use Magento\Framework\App\RequestInterface;

/**
 * Class LdapClientTest for testing LdapClient
 *
 * @covers Arb\LdapLogin\Model\Ldap\LdapClient
 */
class LdapClientTest extends TestCase
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var Ldap_connect
     */
    private $ldap;

     /**
      * @var RequestInterface
      */
    protected $request;

    /**
     * first item indexer
     *
     * @var integer
     */
    private $getIndexer = 0;

    /**
     * @var testObject
     */
    private $testObject;

    /**
     * @var ldapAttributes
     */
    private $ldapAttributes;

    /**
     * Main set up method
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->configuration = $this->getMockBuilder(Configuration::class)
        ->disableOriginalConstructor()
        ->getMock();
        $this->configuration->method('getHost')->willReturn('ldap.jumpcloud.com');
        $this->configuration->method('getPort')->willReturn(389);
        $this->configuration->method('getLdapOptProtocal')->willReturn(3);
        $this->configuration->method('getLdapOptReferals')->willReturn(0);

        $this->request = $this->getMockBuilder(RequestInterface::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->logger = $this->getMockBuilder(LoggerInterface::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->testObject = $this->objectManager->getObject(
            \Arb\LdapLogin\Model\Ldap\LdapClient::class,
            [
                'configuration' => $this->configuration,
                'request' => $this->request
            ]
        );

        $cryptPw = '{CRYPT}$6$WFDdmuqb25tTlNUG$mHkH0ypmfo1cbXrksM5JMBH2..'.
          'SNoGin28jvzl9ye7qxBvXDsmxwLGRhc3d76hFtgurm9pDUVJKmVO/xvB2Uw.';

        $this->ldapAttributes =  [
            'count' => '1',
            0 =>
             [
              'objectclass' =>
               [
                'count' => '7',
                0 => 'top',
                1 => 'person',
                2 => 'organizationalPerson',
                3 => 'inetOrgPerson',
                4 => 'shadowAccount',
                5 => 'posixAccount',
                6 => 'jumpcloudUser',
              ],
              0 => 'objectclass',
              'uidnumber' =>
               [
                'count' => '1',
                0 => '5002',
              ],
              1 => 'uidnumber',
              'description' =>
               [
                'count' => '1',
                0 => 'Magento 2 admin 2',
              ],
              2 => 'description',
              'uid' =>
               [
                'count' => '1',
                0 => 'jadmin',
              ],
              3 => 'uid',
              'userpassword' =>
               [
                'count' => '1',
                0 => $cryptPw,
              ],
              4 => 'userpassword',
              'gidnumber' =>
               [
                'count' => '1',
                0 => '5002',
              ],
              5 => 'gidnumber',
              'jcldapadmin' =>
               [
                'count' => '1',
                0 => 'TRUE',
              ],
              6 => 'jcldapadmin',
              'loginshell' =>
               [
                'count' => '1',
                0 => '/bin/bash',
              ],
              7 => 'loginshell',
              'givenname' =>
               [
                'count' => '1',
                0 => 'madmin',
              ],
              8 => 'givenname',
              'sn' =>
               [
                'count' => '1',
                0 => 'admin',
              ],
              9 => 'sn',
              'cn' =>
               [
                'count' => '1',
                0 => 'madmin admin',
              ],
              10 => 'cn',
              'homedirectory' =>
               [
                'count' => '1',
                0 => '/home/jadmin',
              ],
              11 => 'homedirectory',
              'mail' =>
               [
                'count' => '1',
                0 => 'jadmin@alcenture.com',
              ],
              12 => 'mail',
              'displayname' =>
               [
                'count' => '1',
                0 => 'jadmin',
              ],
              13 => 'displayname',
              'memberof' =>
               [
                'count' => '1',
                0 => 'cn=Magneto Admin,ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com',
              ],
              14 => 'memberof',
              'count' => '15',
              'dn' => 'uid=jadmin,ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com',
            ],
        ];
    }

    /**
     * test get user by username function
     * @return void
     */
    public function testGetUserByUsername()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $ldapDcn = 'uid='.$username.',ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $filter = 'objectClass=*';

        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('getUserFilter')->willReturn($filter);

        $expectedData = $this->ldapAttributes[$this->getIndexer];

        $this->assertEquals(
            $this->ldapAttributes[$this->getIndexer]['uidnumber'],
            $expectedData['uidnumber']
        );
    }
    /**
     * @return void
     */
    public function testGetUserByUsernameValidData()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $ldapDcn = 'uid='.$username.',ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $filter = 'objectClass=*';

        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('getUserFilter')->willReturn($filter);
        $this->configuration->method('getAttributeNamePassword')->willReturn("testpassword");

        $expectedData = $this->ldapAttributes[$this->getIndexer];

        $this->assertEquals(
            $this->ldapAttributes[$this->getIndexer]['userpassword'],
            $expectedData['userpassword']
        );
    }

    /**
     * check for blank user
     *
     * @return void
     */
    public function testGetUserByBlankUsername()
    {
        $username = '';
        $password = '';
        $ldapDcn = 'uid='.$username.',ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $filter = 'objectClass=*';

        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('getUserFilter')->willReturn($filter);
        $this->configuration->method('getTLSCerti')->willReturn(1);
        $this->expectException(LocalizedException::class);
        $this->testObject->getUserByUsername($username, $password);
    }

    /**
     * check exception for inactive user
     *
     * @return void
     */
    public function testGetUserByInactiveUsername()
    {
        $username = 'inactiveuser';
        $password = 'adminJTest@123';
        $ldapDcn = 'uid='.$username.',ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $filter = 'objectClass=*';

        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('getUserFilter')->willReturn($filter);
    }

    /**
     * check exception for inactive user
     *
     * @return void
     */
    public function testGetUserByNoSearchUsername()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $ldapDcn = 'uid='.$username.',ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $filter = 'objectClass=Uid';

        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('getUserFilter')->willReturn($filter);
    }

     /**
      * check if Ladap workingwith active ldap setting
      *
      * @return void
      */
    public function testCanBind()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $ldapDcn = 'uid='.$username.',ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('isLdapActive')->willReturn(true);

        $this->assertTrue(true);
    }

    /**
     * check if Ladap bind exception
     *
     * @return void
     */
    public function testCanBindLdapFalse()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $ldapDcn = 'uid='.$username.',ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('isLdapActive')->willReturn(false);

        $this->expectException(LocalizedException::class);
        $this->testObject->canBind($username, $password);
    }

    /**
     * check if Ladap bind exception
     *
     * @return void
     */
    public function testCanBindLdapException()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $ldapDcn = 'uid='.$username.',ou=Admin,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com';
        $this->configuration->method('getLdapConnectionOptions')->with($username)->willReturn($ldapDcn);
        $this->configuration->method('isLdapActive')->willReturn(true);
    }
}
