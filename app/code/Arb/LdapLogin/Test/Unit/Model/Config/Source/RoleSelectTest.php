<?php
/**
 * This file consist of PHPUnit test case for class RoleSelect
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Unit\Model\Config\Source;

use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\ResourceModel\Role\CollectionFactory as RoleCollectionFactory;
use Magento\Authorization\Model\ResourceModel\Role\Collection as RoleCollectionMock;

/**
 * Class RoleSelectTest used for PhpUnit test RoleSelect
 *
 * @covers Arb\LdapLogin\Model\Config\Source\RoleSelect
 */
class RoleSelectTest extends TestCase
{
    /**
     * @var roleCollectionFactory
     */
    protected $roleCollectionFactory;

    /**
     * @var testObject
     */
    private $testObject;

    /**
     * @var roles
     */
    private $roles;

    /**
     * Main set up method
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->roleCollectionFactory = $this->createPartialMock(
            RoleCollectionFactory::class,
            ['create', 'addFieldToFilter']
        );

        $this->itemOneMock = $this->getMockBuilder(\Magento\Authorization\Model\Role::class)
            ->setMethods(['getRoleId', 'getRoleName'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->roleCollectionFactoryMock = $this->objectManager->getCollectionMock(
            RoleCollectionMock::class,
            [$this->itemOneMock]
        );
        $this->itemOneMock->method("getRoleId")->willReturn("1");
        $this->itemOneMock->method("getRoleName")->willReturn("teste");
        $this->roleCollectionFactory->expects($this->once())
            ->method('create')->willReturn($this->roleCollectionFactoryMock);
        $this->roleCollectionFactoryMock->expects($this->once())
            ->method('addFieldToFilter')->willReturn([[$this->itemOneMock]]);

        $this->testObject = $this->objectManager->getObject(
            \Arb\LdapLogin\Model\Config\Source\RoleSelect::class,
            [
                'roleCollectionFactory' => $this->roleCollectionFactory
            ]
        );
    }

    public function testToOptionArray()
    {


        $this->assertEquals([
            ["value"=>"1","label"=>"teste"
        ]],
         $this->testObject->toOptionArray());
    }
}
