<?php
/**
 * This file consist of PHPUnit test case for class UserPlugin
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Plugin\User\Model;

use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

use Closure;
use Magento\User\Model\User;

/**
 * @covers Arb\LdapLogin\Plugin\User\Model\UserPlugin
 */
class UserPluginTest extends TestCase
{
    /**
     *
     * @var Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    /**
     * @var testObject
     */
    private $testObject;

    /**
     * @var closureMock
     */
    protected $closureMock;

    /**
     * @var subject
     */
    protected $subject;

    /**
     * unit test setup
     *
     * @return void
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->testObject = $this->objectManager->getObject(
            \Arb\LdapLogin\Plugin\User\Model\UserPlugin::class,
            []
        );

        $this->closureMock = function () {
            return 'Expected';
        };

        $this->subject = $this->createPartialMock(
            User::class,
            [
                'getLdapDn'
            ]
        );
    }

    /**
     * Skip before save methods if user comes from ldap
     *
     * @return void
     */
    public function testAroundValidateBeforeSave()
    {
        $this->assertEquals('Expected', $this->testObject
            ->aroundValidateBeforeSave($this->subject, $this->closureMock));
    }

    /**
     * validation test
     *
     * @return void
     */
    public function testAroundValidateForLdap()
    {
        $this->subject->method('getLdapDn')->willReturn(0);
        $this->assertEquals($this->subject, $this->testObject
            ->aroundValidateBeforeSave($this->subject, $this->closureMock));
    }
}
