<?php
/**
 * This file consist of PHPUnit test case for class UserMapper
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Plugin\Backend\Model\Auth\Credential;

use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Closure;
use Arb\LdapLogin\Api\LdapClientInterface;
use Arb\LdapLogin\Model\Ldap\PasswordValidator;
use Arb\LdapLogin\Model\Ldap\UserMapper;
use Magento\Backend\Model\Auth\Credential\StorageInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\LocalizedException;
use Magento\User\Model\User;
use Psr\Log\LoggerInterface;

/**
 * Class StoragePluginTest for testing StoragePlugin
 *
 * @covers Arb\LdapLogin\Plugin\Backend\Model\Auth\Credential\StoragePlugin
 */
class StoragePluginTest extends TestCase
{
    /**
     * @var LdapClientInterface
     */
    private $ldapClient;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var PasswordValidator
     */
    private $passwordValidator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var UserMapper
     */
    private $userMapper;

     /**
      * @var \PHPUnit_Framework_MockObject_MockObject
      */
    protected $closureMock;

    /**
     * @var \Magento\User\Model\ResourceModel\User
     */
    private $userResource;

    /**
     * ldap outout array
     *
     * @var array
     */
    private $ldapAttributes;

    /**
     * user object
     *
     * @var \Magento\User\Model\User
     */
    private $subject;

    /**
     * unit test setup
     *
     * @return void
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->logger = $this->getMockBuilder(LoggerInterface::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->ldapClient = $this->getMockBuilder(LdapClientInterface::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->ldapAttributes =  [
            'count' => '1',
            0 => ['objectclass' =>
               ['count' => '7',
                0 => 'top',
                1 => 'person',
                2 => 'organizationalPerson',
                3 => 'inetOrgPerson',
                4 => 'shadowAccount',
                5 => 'posixAccount',
                6 => 'jumpcloudUser',
              ],
              0 => 'objectclass',
              'uidnumber' =>  [ 'count' => '1', 0 => '5002'],
              1 => 'uidnumber',
              'description' =>  [
                'count' => '1', 0 => 'Magento 2 admin 2'],
              2 => 'description',
              'uid' => [ 'count' => '1', 0 => 'jadmin'],
              3 => 'uid',
              'userpassword' => [ 'count' => '1',
                0 => '{CRYPT}$6$WFDdmuqb25tTlNUG$mHkH0ypmfo1cbXrksM5JMBH2..
                SNoGin28jvzl9ye7qxBvXDsmxwLGRhc3d76hFtgurm9pDUVJKmVO/xvB2Uw.',
              ],
              4 => 'userpassword',
              'gidnumber' =>
               [
                'count' => '1',
                0 => '5002',
              ],
              5 => 'gidnumber',
              'jcldapadmin' =>
               [
                'count' => '1',
                0 => 'TRUE',
              ],
              6 => 'jcldapadmin',
              'loginshell' =>
               [
                'count' => '1',
                0 => '/bin/bash',
              ],
              7 => 'loginshell',
              'givenname' =>
               [
                'count' => '1',
                0 => 'madmin',
              ],
              8 => 'givenname',
              'sn' =>
               [
                'count' => '1',
                0 => 'admin',
              ],
              9 => 'sn',
              'cn' =>
               [
                'count' => '1',
                0 => 'madmin admin',
              ],
              10 => 'cn',
              'homedirectory' =>
               [
                'count' => '1',
                0 => '/home/jadmin',
              ],
              11 => 'homedirectory',
              'mail' =>
               [
                'count' => '1',
                0 => 'jadmin@alcenture.com',
              ],
              12 => 'mail',
              'displayname' =>
               [
                'count' => '1',
                0 => 'jadmin',
              ],
              13 => 'displayname',
              'memberof' =>
               [
                'count' => '1',
                0 => 'cn=Magneto Admin,ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com',
              ],
              14 => 'memberof',
              'count' => '15',
              'dn' => 'uid=jadmin,ou=Users,o=5e78ab0bc57e633083a888a6,dc=jumpcloud,dc=com',
            ],
        ];

        $this->userMapper = $this->getMockBuilder(UserMapper::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->eventManager = $this->getMockBuilder(ManagerInterface::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->passwordValidator = $this->getMockBuilder(PasswordValidator::class)
        ->disableOriginalConstructor()
        ->setMethods(['isPasswordCachedAllowed',"validatePassword"])
        ->getMock();

        $this->passwordValidator->method('validatePassword')->willReturn(true);

        $this->userResource = $this->getMockBuilder(\Magento\User\Model\ResourceModel\User::class)
        ->disableOriginalConstructor()
        ->getMock();

        $this->closureMock = function () {
            return 'Expected';
        };

        $this->subject = $this->createPartialMock(
            User::class,
            [
                'loadByUsername',
                'setFirstName',
                'setLastName',
                'setUserName',
                'setEmail',
                'setIsActive',
                'load',
                'setData',
                'isEmpty',
                'getLdapDn',
                'getIsActive',
                'hasAssigned2Role'
            ]
        );


        $this->testObject = $this->objectManager->getObject(
            \Arb\LdapLogin\Plugin\Backend\Model\Auth\Credential\StoragePlugin::class,
            [
                'ldapClient' => $this->ldapClient,
                'eventManager' => $this->eventManager,
                'passwordValidator' => $this->passwordValidator,
                'logger' => $this->logger,
                'userMapper' => $this->userMapper,
                'userResource' => $this->userResource
            ]
        );
    }

    /**
     * test the around auth method
     *
     * @return void
     */
    public function testAroundAuthenticate()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $this->ldapClient->method('canBind')->with($username, $password)->willReturn(false);

        $this->assertEquals('Expected', $this->testObject
          ->aroundAuthenticate($this->subject, $this->closureMock, $username, $password));
    }
     /**
     * test the around auth method
     * @expectedException Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function testAroundAuthenticateWithData()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $this->ldapClient->method('canBind')->willReturn(false);
        $this->ldapClient->method('getUserByUsername')->willReturn($this->ldapAttributes[0]);
        $this->subject->method('hasAssigned2Role')->willReturn(false);
        $this->subject->method('getLdapDn')->willReturn(0);
        $this->passwordValidator->method('isPasswordCachedAllowed')->willReturn(false);
        $this->subject->method('isEmpty')->willReturn(false);
         $this->subject->method("getIsActive")->willReturn(true);
        $this->testObject->aroundAuthenticate($this->subject, $this->closureMock, $username, $password);
    }

    

    /**
     * test the around auth method
     * @expectedException Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function testAroundAuthenticateWithDataAccess()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $this->ldapClient->method('canBind')->willReturn(true);
        $this->ldapClient->method('getUserByUsername')->willReturn($this->ldapAttributes[0]);
        $this->subject->method('isEmpty')->willReturn(true);
        $this->subject->method('hasAssigned2Role')->willReturn(false);
        $this->testObject->aroundAuthenticate($this->subject, $this->closureMock, $username, $password);
    }

    /**
     * test the around auth method not empty
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function testAroundAuthenticateWithDataAccessNotEmpty()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $this->ldapClient->method('canBind')->willReturn(true);
        $this->subject->method("getIsActive")->willReturn(true);
        $this->ldapClient->method('getUserByUsername')->willReturn($this->ldapAttributes[0]);
        $this->subject->method('isEmpty')->willReturn(true);
        $this->subject->method('hasAssigned2Role')->willReturn(false);
        $this->testObject->aroundAuthenticate($this->subject, $this->closureMock, $username, $password);
    }

     /**
     * test the around auth method not empty with role
     *
     * @return void
     */
    public function testAroundAuthenticateWithDataAccessNotEmptyWithRole()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $this->ldapClient->method('canBind')->willReturn(true);
        $this->subject->method("getIsActive")->willReturn(true);
        $this->ldapClient->method('getUserByUsername')->willReturn($this->ldapAttributes[0]);
        $this->subject->method('isEmpty')->willReturn(true);
        $this->subject->method('hasAssigned2Role')->willReturn(true);
        $this->testObject->aroundAuthenticate($this->subject, $this->closureMock, $username, $password);
    }

      /**
     * test the around auth method
     * @return void
     */
    public function testAroundAuthenticateCachePass()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $this->ldapClient->method('canBind')->willReturn(true);
        $this->ldapClient->method('getUserByUsername')->willReturn("");
        $this->subject->method('isEmpty')->willReturn(true);

        $this->testObject->aroundAuthenticate($this->subject, $this->closureMock, $username, $password);
    }

     /**
      * test the around auth method user instance
      *
      * @return void
      */
    public function testAroundAuthUser()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';

        $this->subject = $this->createPartialMock(
            StorageInterface::class,
            [
                'authenticate',
                'login',
                'reload',
                'hasAvailableResources',
                'setHasAvailableResources'
            ]
        );

        $this->assertEquals(
            'Expected',
            $this->testObject
              ->aroundAuthenticate($this->subject, $this->closureMock, $username, $password)
        );
    }

    /**
     * test the around auth method for empty user & Ldap Dn
     *
     * @return void
     */
    public function testAroundAuthLdapLn()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $this->subject->method('isEmpty')->willReturn(false);
        $this->subject->method('getLdapDn')->willReturn(0);
        $this->passwordValidator->method('isPasswordCachedAllowed')->willReturn(true);
        $this->assertEquals(
            'Expected',
            $this->testObject
              ->aroundAuthenticate($this->subject, $this->closureMock, $username, $password)
        );
    }
    

    /**
     * test the around auth method Exception
     *
     * @return void
     */
    public function testAroundAuthException()
    {
        $username = 'jadmin';
        $password = 'adminJTest@123';
        $result = true;

        $params = ['username' => $username, 'password' => $password, 'user' => $this->subject, 'result' => $result];
        $this->eventManager->method('dispatch')->with('admin_user_authenticate_after', $params)->willReturn([]);

        $this->assertEquals(
            'Expected',
            $this->testObject
              ->aroundAuthenticate($this->subject, $this->closureMock, $username, $password)
        );
    }
}
