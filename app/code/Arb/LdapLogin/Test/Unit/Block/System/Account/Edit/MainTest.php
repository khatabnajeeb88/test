<?php
/**
 * This file consist of PHPUnit test case for class Main
 *
 * @category Arb LDAP
 * @package Arb_LdapLogin
 * @author Arb Magento Team
 *
 */
namespace Arb\LdapLogin\Test\Block\System\Account\Edit;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class MainTest to cover Arb\LdapLogin\Block\System\Account\Edit\Main
 *
 */
class MainTest extends \PHPUnit\Framework\TestCase
{

    /** @var \Magento\User\Block\User\Edit\Tab\Roles */
    protected $model;

    /** @var \Magento\Framework\Json\EncoderInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $jsonEncoderMock;

    /** @var \Magento\Backend\Model\Auth\Session|\PHPUnit_Framework_MockObject_MockObject */
    protected $authSessionsMock;

    /** @var \Magento\Framework\Registry|\PHPUnit_Framework_MockObject_MockObject */
    protected $registryMock;

    /** @var \Magento\Framework\View\LayoutInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $layoutInterfaceMock;

    /**
     * Set required values
     * @return void
     */
    protected function setUp()
    {
        $this->registryMock = $this->getMockBuilder(Magento\User\Block\User\Edit\Tab::class)
        ->disableOriginalConstructor()
        ->setMethods(['_prepareForm'])
        ->getMock();

        $this->registryMock = $this->getMockBuilder(\Magento\Framework\Registry::class)
        ->disableOriginalConstructor()
        ->setMethods([])
        ->getMock();

        $this->layoutInterfaceMock = $this->getMockBuilder(\Magento\Framework\View\LayoutInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['setRole', 'setActive', 'getId'])
            ->getMockForAbstractClass();

            $objectManagerHelper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->model = $objectManagerHelper->getObject(
            \Arb\LdapLogin\Block\System\Account\Edit\Main::class,
            [
                'registry' => $this->registryMock,
                'layout' => $this->layoutInterfaceMock
            ]
        );
    }

    public function testPrepareLayout()
    {
        $this->assertInstanceOf(
            \Magento\Backend\Block\Widget\Form::class,
            $this->invokeMethod($this->model, '_prepareLayout')
        );
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object
     * @param string $methodName
     * @param array  $parameters
     *
     * @return mixed Method return.
     */
    private function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
