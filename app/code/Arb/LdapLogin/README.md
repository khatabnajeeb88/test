## Synopsis
This is module used for arb magento admin login using LDAP connectivity using LDAP username and password details

### Prerequisites 
1.	Install LDAP using apt-get install php7.2-ldap for Linux server (change version as per the php version install in your system)
2.	Restart server (nginx / apache server)
3.	At Magento admin System->Configraiton->Services->LDAP update the following settings as per the LDAP server
  a.	LDAP connection host name
  b.	LDAP connection port number
  c.	LDAP connection base dn pattern
  d.	LDAP connection bind dn pattern
  e.	User role / status etc.

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).
4. Configuration default value settings [config.xml](etc/config.xml).
5. Ldap admin system configuration settings [system.xml](etc/adminhtml/system.xml)

