<?php
/**
 * Helper for Sla Event Handling
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Helper;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Arb\Sla\Model\SlaDataFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Arb\Sla\Api\SlaRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use DateTime;
use DateInterval;

class SlaEventManagement extends AbstractHelper
{
    /**
     * @var SlaRepositoryInterface
     */
    private $slaRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var SlaDataFactory
     */
    private $slaDataFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param Context $context
     * @param SlaRepositoryInterface $slaRepository
     * @param StoreManagerInterface $storeManager
     * @param SlaDataFactory $slaDataFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        Context $context,
        SlaRepositoryInterface $slaRepository,
        StoreManagerInterface $storeManager,
        SlaDataFactory $slaDataFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        parent::__construct($context);

        $this->slaRepository = $slaRepository;
        $this->storeManager = $storeManager;
        $this->slaDataFactory = $slaDataFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param SlaDataInterface $sla
     *
     * @return void
     */
    public function saveSla(SlaDataInterface $sla)
    {
        $this->slaRepository->save($sla);
    }

    /**
     * Get existing SLA
     *
     * @param string $type
     * @param int $entityId
     *
     * @return SlaDataInterface|null
     */
    public function getSla(string $type, int $entityId)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(
                SlaDataInterface::SLA_TYPE,
                $type
            )->addFilter(
                SlaDataInterface::SLA_RELATED_ENTITY_ID,
                $entityId
            )->create();

        $slaItems = $this->slaRepository->getList($searchCriteria);

        // there can be max 1 item with given search criteria
        foreach ($slaItems->getItems() as $slaItem) {
            return $slaItem;
        }

        return null;
    }

    /**
     * Load SLA if exists or create new one if doesn't
     *
     * @param string $type
     * @param int $entityId
     *
     * @return SlaDataInterface
     */
    public function initSla(string $type, int $entityId)
    {
        $sla = $this->getSla($type, $entityId);

        return $sla ? $sla : $this->slaDataFactory->create();
    }

    /**
     * @param SlaDataInterface $sla
     * @param int $reminderValue
     * @param int $deadlineValue
     * @param int $relatedEntityId
     * @param string $type
     * @param int $merchantId
     *
     * @return SlaDataInterface
     *
     * @throws NoSuchEntityException
     */
    public function setSlaData(
        SlaDataInterface $sla,
        int $relatedEntityId,
        int $reminderValue,
        int $deadlineValue,
        string $type,
        int $merchantId
    ) {
        if ($reminderValue) {
            if ($reminderValue >= 100) {
                $reminderValue = 99;
            }
            $calculatedValue = $reminderValue / 100 * $deadlineValue;
            $hours = (int)$calculatedValue;
            $minutes = $hours ? fmod($calculatedValue, $hours) * 60 : $calculatedValue * 60;
            $minutesFormat = (int)$minutes;
            $remindTime = new DateTime();
            $remindTime->add(new DateInterval("PT{$hours}H{$minutesFormat}M"));
            $sla->setReminderTime($remindTime->format('Y-m-d H:i:s'));
        } else {
            $sla->setMerchantNotification(SlaDataInterface::SLA_NOTIFICATION_REMINDER_SENT);
            $sla->setAdminNotification(SlaDataInterface::SLA_NOTIFICATION_REMINDER_SENT);
        }

        $currentTime = new DateTime();
        $sla->setTriggerTime($currentTime->format('Y-m-d H:i:s'));
        $currentTime->add(new DateInterval("PT{$deadlineValue}H"));
        $sla->setDeadlineTime($currentTime->format('Y-m-d H:i:s'));

        $sla->getRelatedEntityId() ?: $sla->setRelatedEntityId($relatedEntityId);
        $sla->getType() ?: $sla->setType($type);
        $sla->getMerchantId()?: $sla->setMerchantId($merchantId);

        if (!$this->isAdminSla($sla->getType()) && !$sla->getStoreId()) {
            $sla->setStoreId((int)$this->storeManager->getStore()->getId());
        }

        return $sla;
    }

    /**
     * @param string $type
     * @param int $entityId
     *
     * @return void
     */
    public function deleteSla(string $type, int $entityId)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(
                SlaDataInterface::SLA_TYPE,
                $type
            )->addFilter(
                SlaDataInterface::SLA_RELATED_ENTITY_ID,
                $entityId
            )->create();

        $slaItems = $this->slaRepository->getList($searchCriteria);

        foreach ($slaItems->getItems() as $slaItem) {
            $this->slaRepository->delete($slaItem);
        }
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    private function isAdminSla(string $type)
    {
        if (SlaDataInterface::RESPONSIBLE_PERSON_ADMIN === SlaDataInterface::SLA_RESPONSIBLE_FOR_TYPE[$type]) {
            return true;
        }

        return false;
    }
}
