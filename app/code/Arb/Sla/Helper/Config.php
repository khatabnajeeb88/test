<?php
/**
 * Helper for Admin Config values for SLA
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{
    private const CONFIG_XML_PATH_ADMIN_EMAIL_SENDER = 'trans_email/ident_general/email';
    private const CONFIG_XML_PATH_ADMIN_EMAIL_RECEIVER = 'marketplace/general_settings/adminemail';

    private const CONFIG_XML_PATH_PRODUCT_APPROVAL_CRON_ENABLED = 'sla/sla_admin_product_approval/cron_enabled';
    private const CONFIG_XML_PATH_LOW_STOCK_CRON_ENABLED = 'sla/sla_low_stock/cron_enabled';
    private const CONFIG_XML_PATH_RETURN_REQUEST_CRON_ENABLED = 'sla/sla_return_request/cron_enabled';
    private const CONFIG_XML_PATH_NEW_ORDER_CRON_ENABLED = 'sla/sla_new_order/cron_enabled';
    private const CONFIG_XML_PATH_FINALIZE_ORDER_CRON_ENABLED = 'sla/sla_order_finalize/cron_enabled';

    private const CONFIG_XML_PATH_PRODUCT_APPROVAL_REMINDER = 'sla/sla_admin_product_approval/sla_reminder';
    private const CONFIG_XML_PATH_PRODUCT_APPROVAL_DEADLINE = 'sla/sla_admin_product_approval/sla_deadline';

    private const CONFIG_XML_PATH_LOW_STOCK_REMINDER = 'sla/sla_low_stock/sla_reminder';
    private const CONFIG_XML_PATH_LOW_STOCK_DEADLINE = 'sla/sla_low_stock/sla_deadline';

    private const CONFIG_XML_PATH_RETURN_REQUEST_REMINDER = 'sla/sla_return_request/sla_reminder';
    private const CONFIG_XML_PATH_RETURN_REQUEST_DEADLINE = 'sla/sla_return_request/sla_deadline';

    private const CONFIG_XML_PATH_NEW_ORDER_REMINDER = 'sla/sla_new_order/sla_reminder';
    private const CONFIG_XML_PATH_NEW_ORDER_DEADLINE = 'sla/sla_new_order/sla_deadline';

    private const CONFIG_XML_PATH_FINALIZE_ORDER_REMINDER = 'sla/sla_order_finalize/sla_reminder';
    private const CONFIG_XML_PATH_FINALIZE_ORDER_DEADLINE = 'sla/sla_order_finalize/sla_deadline';

    /**
     * @return int
     */
    public function getProductApprovalCronEnabled()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_PRODUCT_APPROVAL_CRON_ENABLED);
    }

    /**
     * @return int
     */
    public function getProductLowStockCronEnabled()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_LOW_STOCK_CRON_ENABLED);
    }

    /**
     * @return int
     */
    public function getReturnRequestCronEnabled()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_RETURN_REQUEST_CRON_ENABLED);
    }

    /**
     * @return int
     */
    public function getNewOrderRequiresActionCronEnabled()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_NEW_ORDER_CRON_ENABLED);
    }

    /**
     * @return int
     */
    public function getFinalizeOrderCronEnabled()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_FINALIZE_ORDER_CRON_ENABLED);
    }

    /**
     * @return int
     */
    public function getProductApprovalReminderTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_PRODUCT_APPROVAL_REMINDER, 'store');
    }

    /**
     * @return int
     */
    public function getProductApprovalDeadlineTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_PRODUCT_APPROVAL_DEADLINE, 'store');
    }

    /**
     * @return int
     */
    public function getProductLowStockReminderTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_LOW_STOCK_REMINDER, 'store');
    }

    /**
     * @return int
     */
    public function getProductLowStockDeadlineTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_LOW_STOCK_DEADLINE, 'store');
    }

    /**
     * @return int
     */
    public function getOrderReturnRequestReminderTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_RETURN_REQUEST_REMINDER, 'store');
    }

    /**
     * @return int
     */
    public function getOrderReturnRequestDeadlineTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_RETURN_REQUEST_DEADLINE, 'store');
    }

    /**
     * @return int
     */
    public function getNewOrderRequiresActionReminderTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_NEW_ORDER_REMINDER, 'store');
    }

    /**
     * @return int
     */
    public function getNewOrderRequiresActionDeadlineTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_NEW_ORDER_DEADLINE, 'store');
    }

    /**
     * @return int
     */
    public function getFinalizeOrderReminderTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_FINALIZE_ORDER_REMINDER, 'store');
    }

    /**
     * @return int
     */
    public function getFinalizeOrderDeadlineTime()
    {
        return (int)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_FINALIZE_ORDER_DEADLINE, 'store');
    }

    /**
     * @return string
     */
    public function getSenderEmail()
    {
        return (string)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_ADMIN_EMAIL_SENDER, 'store');
    }

    /**
     * @return string
     */
    public function getAdminReceiverEmail()
    {
        return (string)$this->scopeConfig->getValue(self::CONFIG_XML_PATH_ADMIN_EMAIL_RECEIVER, 'store');
    }
}
