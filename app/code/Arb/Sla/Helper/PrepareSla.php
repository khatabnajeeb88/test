<?php
/**
 * Helper for filtering and preparing SLA entities
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Helper;

use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\SlaRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use DateTime;

class PrepareSla extends AbstractHelper
{
    private const ADMIN_SENDER_NAME = 'Admin';

    /**
     * @var SlaRepositoryInterface
     */
    private $slaRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * @var SlaEmail
     */
    private $email;

    /**
     * @var SlaUrlBuilder
     */
    private $slaUrlBuilder;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @param Context $context
     * @param SlaRepositoryInterface $slaRepository
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     * @param Emulation $emulation
     * @param SlaEmail $email
     * @param SlaUrlBuilder $slaUrlBuilder
     */
    public function __construct(
        Context $context,
        SlaRepositoryInterface $slaRepository,
        Config $config,
        StoreManagerInterface $storeManager,
        Emulation $emulation,
        SlaEmail $email,
        SlaUrlBuilder $slaUrlBuilder
    ) {
        parent::__construct($context);

        $this->slaRepository = $slaRepository;
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->emulation = $emulation;
        $this->email = $email;
        $this->slaUrlBuilder = $slaUrlBuilder;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Prepare_sla_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Check for SLA that breached their time threshold
     *
     * @param string $type
     *
     * @return void
     *
     * @throws LocalizedException
     */
    public function runSlaCheck(string $type,$string='')
    {
        $this->date = new DateTime();
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_'.$string.'_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $reasonLabel = SlaDataInterface::SLA_TYPE_LABEL[$type];
        $details = [
            'sender' => [
                'name' => self::ADMIN_SENDER_NAME
            ],
            'template_vars' => [
                'sla_breach_date' => $this->date->format('Y-m-d H:i:s'),
                'sla_cause' => __($reasonLabel),
                'responsible_person' => SlaDataInterface::SLA_RESPONSIBLE_FOR_TYPE[$type]
            ]
        ];

        foreach ($this->storeManager->getStores(true) as $store) {
            $this->emulation->startEnvironmentEmulation($store->getId());
            $this->runExpiredSlaCheck($type, $details, (int)$store->getId());
            $this->logger->info("Value for SLA that breached their time threshold checked for store ID". (int)$store->getId());
            $this->emulation->stopEnvironmentEmulation();
        }
    }

    /**
     * @param string $type
     * @param array $details
     * @param int $storeId
     *
     * @return void
     *
     * @throws LocalizedException
     */
    private function runExpiredSlaCheck(string $type, array $details, int $storeId)
    {
        $expiredDeadlineSla = $this->slaRepository->getAllExpiredSla($type, $storeId);

        /** @var SlaDataInterface $sla */
        foreach ($expiredDeadlineSla->getItems() as $sla) {
            $details['template_vars']['sla_date'] = $sla->getDeadlineTime();
            $details['template_vars']['url'] =
                '<a href="' . $this->slaUrlBuilder->getUrl($sla) . '">' . __('click here') . '</a>';
            $details['sender']['email'] = $this->config->getSenderEmail();
            $details['sender']['store_id'] = $storeId;

            // Check for Breached Deadlines first so we dont send Reminder as well
            if ($sla->getDeadlineTime() < $this->date->format('Y-m-d H:i:s')) {
                $details['breach_type'] = SlaDataInterface::BREACH_TYPE_DEADLINE;
                $this->logger->info("Breached Deadlines has passed so sending breach deadline email from store ".$details['sender']['store_id']);
                $this->email->sendEmail($sla, $details);
            }

            // Send Reminder email if not sent yet and if no deadline breach happened yet
            if ($sla->getReminderTime() < $this->date->format('Y-m-d H:i:s')
                && ($this->canSendMerchantReminder($sla) || $this->canSendAdminReminder($sla, $type))) {
                $details['breach_type'] = SlaDataInterface::BREACH_TYPE_REMINDER;
                $this->logger->info("Breached Deadlines has not passed so sending Reminder from store ".$details['sender']['store_id']);
                $this->email->sendEmail($sla, $details);
            }
        }
    }

    /**
     * Verify no Breach/Reminder email sent yet to Merchant
     *
     * @param SlaDataInterface $sla
     *
     * @return bool
     */
    private function canSendMerchantReminder(SlaDataInterface $sla)
    {
        // Need to verify date since getList stores the Sla which might have had deadline breach
        if ($sla->getDeadlineTime() > $this->date->format('Y-m-d H:i:s')
            && $sla->getMerchantNotification() === SlaDataInterface::SLA_NOTIFICATION_NOTHING_SENT) {
                $this->logger->info("Breach/Reminder email has been sent to Merchant");
                return true;
        }
        $this->logger->info("Breach/Reminder email not yet sent to Merchant");
        return false;
    }

    /**
     * Verify no Breach/Reminder email sent yet to Admin
     *
     * @param SlaDataInterface $sla
     * @param string $type
     *
     * @return bool
     */
    private function canSendAdminReminder(SlaDataInterface $sla, string $type)
    {
        // Need to verify date since getList stores the Sla which might have had deadline breach
        if ($sla->getDeadlineTime() > $this->date->format('Y-m-d H:i:s')
            && $sla->getAdminNotification() === SlaDataInterface::SLA_NOTIFICATION_NOTHING_SENT
            && SlaDataInterface::SLA_RESPONSIBLE_FOR_TYPE[$type] === SlaDataInterface::RESPONSIBLE_PERSON_ADMIN) {
                $this->logger->info("Breach/Reminder email has been sent to Admin");
            return true;
        }
        $this->logger->info("Breach/Reminder email not yet sent to Admin");
        return false;
    }
}
