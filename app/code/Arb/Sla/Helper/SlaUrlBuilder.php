<?php
/**
 * Helper for building URLs for SLA emails
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Helper;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class SlaUrlBuilder extends AbstractHelper
{
    private const URL_PATH_ORDER = 'marketplace/order/view/id/';
    private const URL_PATH_RMA = 'mprmasystem/seller/rma/id/';
    private const URL_PATH_PRODUCT = 'marketplace/product/edit/id/';

    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * @var ProductCollection
     */
    private $productCollection;

    /**
     * @param Context $context
     * @param OrdersRepository $ordersRepository
     * @param ProductCollection $productCollection
     */
    public function __construct(
        Context $context,
        OrdersRepository $ordersRepository,
        ProductCollection $productCollection
    ) {
        parent::__construct($context);

        $this->ordersRepository = $ordersRepository;
        $this->productCollection = $productCollection;
    }

    /**
     * Build URL for SLA to take action
     *
     * @param SlaDataInterface $slaData
     *
     * @return string
     */
    public function getUrl(SlaDataInterface $slaData)
    {
        $baseUrl = $this->_urlBuilder->getBaseUrl();

        switch ($slaData->getType()) {
            case SlaDataInterface::SLA_TYPE_RETURN_REQUEST:
                return $baseUrl . $this->buildRmaPageUrl($slaData);
            case SlaDataInterface::SLA_TYPE_ORDER_SHIPPING:
            case SlaDataInterface::SLA_TYPE_NEW_ORDER:
                return $baseUrl . $this->buildOrderPageUrl($slaData);
            case SlaDataInterface::SLA_TYPE_LOW_STOCK:
                return $baseUrl . $this->buildMerchantProductPageUrl($slaData);
            default:
                return $baseUrl;
        }
    }

    /**
     * @param SlaDataInterface $slaData
     *
     * @return string
     */
    private function buildOrderPageUrl(SlaDataInterface $slaData)
    {
        $merchantOrder = null;
        try {
            /** @var Orders $merchantOrder */
            $merchantOrder = $this->ordersRepository->getById($slaData->getRelatedEntityId());
        } catch (NoSuchEntityException $e) {
            $this->_logger->error($e->getMessage());
        } catch (LocalizedException $e) {
            $this->_logger->error($e->getMessage());
        }
        $id = $merchantOrder ? $merchantOrder->getOrderId() : '';

        return self::URL_PATH_ORDER . $id;
    }

    /**
     * @param SlaDataInterface $slaData
     *
     * @return string
     */
    private function buildRmaPageUrl(SlaDataInterface $slaData)
    {
        return self::URL_PATH_RMA . $slaData->getRelatedEntityId();
    }

    /**
     * @param SlaDataInterface $slaData
     *
     * @return string
     */
    private function buildMerchantProductPageUrl(SlaDataInterface $slaData)
    {
        $merchantItem = $this->productCollection
            ->create()
            ->addFieldToFilter('entity_id', $slaData->getRelatedEntityId())
            ->getFirstItem();

        return self::URL_PATH_PRODUCT . $merchantItem->getMageproductId();
    }
}
