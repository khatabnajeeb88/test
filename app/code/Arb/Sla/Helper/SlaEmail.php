<?php
/**
 * Helper for preparing Email with SLA notification
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Helper;

use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\SlaReportRepositoryInterface;
use Arb\Sla\Model\SlaReportFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Arb\Sla\Api\SlaRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Exception\MailException;
use Magento\Framework\App\Area;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;
use Exception;
use Arb\Sla\Logger\Logger as SlaLogger;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;

class SlaEmail extends AbstractHelper
{
    private const SLA_EMAIL_REMINDER_TEMPLATE_ID = 'sla_email_notification_reminder';
    private const SLA_EMAIL_DEADLINE_BREACH_TEMPLATE_ID = 'sla_email_notification_deadline';

    /**
     * @var SlaRepositoryInterface
     */
    private $slaRepository;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var StateInterface
     */
    private $inlineTranslation;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var SlaLogger
     */
    private $logger;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var SlaReportFactory
     */
    private $slaReportFactory;

    /**
     * @var SlaReportRepositoryInterface
     */
    private $slaReportRepository;

    /**
     * @var Config
     */
    private $config;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Context $context
     * @param SlaRepositoryInterface $slaRepository
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerFactory $customerFactory
     * @param SlaLogger $logger
     * @param TimezoneInterface $timezone
     * @param SlaReportRepositoryInterface $slaReportRepository
     * @param SlaReportFactory $slaReportFactory
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        SlaRepositoryInterface $slaRepository,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        CustomerRepositoryInterface $customerRepository,
        CustomerFactory $customerFactory,
        SlaLogger $logger,
        TimezoneInterface $timezone,
        SlaReportRepositoryInterface $slaReportRepository,
        SlaReportFactory $slaReportFactory,
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);

        $this->slaRepository = $slaRepository;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->logger = $logger;
        $this->timezone = $timezone;
        $this->slaReportRepository = $slaReportRepository;
        $this->slaReportFactory = $slaReportFactory;
        $this->config = $config;
        $this->storeManager = $storeManager;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Arb_Sla_Email_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * Send Emails with SLA breach notification
     *
     * @param SlaDataInterface $sla
     * @param array $details
     *
     * @return void
     *
     * @throws LocalizedException
     */
    public function sendEmail(SlaDataInterface $sla, array $details)
    {
        /** @var Customer $merchant */
        $merchant = $this->customerFactory->create()->load($sla->getMerchantId());

        $templateVars = $details['template_vars'];

        // Assign Merchant Name if Merchant SLA
        $templateVars['responsible_person'] =
            $templateVars['responsible_person'] === SlaDataInterface::RESPONSIBLE_PERSON_MERCHANT
                ? $merchant->getName() : $templateVars['responsible_person'];

        // Check for Admin-only SLA so Merchant doesn't receive them
        // Check if Breach Email has already been sent to Merchant or if it's flagged to not be sent
        if (!$this->checkIsAdminSla($templateVars['responsible_person'])
            && $sla->getMerchantNotification() !== SlaDataInterface::SLA_NOTIFICATION_DEADLINE_SENT
            && $sla->getMerchantNotification() !== SlaDataInterface::SLA_NOTIFICATION_DO_NOT_SEND) {
            $receiverInfo = [
                'name' => $templateVars['responsible_person'],
                'email' => $merchant->getEmail()
            ];

            $this->sendNotificationToMerchant(
                $sla,
                $templateVars,
                $details['sender'],
                $receiverInfo,
                $details['breach_type']
            );
        }

        // Verify if no Breach Email been sent yet and if this is reminder/breach for Admin
        if ($sla->getAdminNotification() !== SlaDataInterface::SLA_NOTIFICATION_DEADLINE_SENT
            && $this->checkIfAdminCanGetReminder($details['breach_type'], $templateVars['responsible_person'])) {
            $this->sendNotificationToAdmin($sla, $templateVars, $details['sender'], $details['breach_type']);
        }
    }

    /**
     * @param string $responsiblePerson
     *
     * @return bool
     */
    private function checkIsAdminSla(string $responsiblePerson)
    {
        if ($responsiblePerson === SlaDataInterface::RESPONSIBLE_PERSON_ADMIN) {
            return true;
        }

        return false;
    }

    /**
     * Send Deadline breach to Admin if Merchant SLA and/or Reminder if Admin SLA
     *
     * @param string $breachType
     * @param string $responsiblePerson
     *
     * @return bool
     */
    private function checkIfAdminCanGetReminder(string $breachType, string $responsiblePerson)
    {
        if ($breachType === SlaDataInterface::BREACH_TYPE_DEADLINE || $this->checkIsAdminSla($responsiblePerson)) {
            return true;
        }

        return false;
    }

    /**
     * Send Deadline Breach Email to Admin
     *
     * @param SlaDataInterface $sla
     * @param array $templateVars
     * @param array $adminInfo
     * @param string $breachType
     *
     * @return void
     *
     * @throws MailException
     */
    private function sendNotificationToAdmin(
        SlaDataInterface $sla,
        array $templateVars,
        array $adminInfo,
        string $breachType
    ) {
        $this->inlineTranslation->suspend();
        $templateVars['user_name'] = 'Admin';

        $store = $this->storeManager->getStore($adminInfo['store_id']);
        $templateVars['email_content_alignment'] = 'left';
        $templateVars['email_content_direction'] = 'ltr';

        if ($store->getCode() === 'ar_SA') {
            $templateVars['email_content_alignment'] = 'right';
            $templateVars['email_content_direction'] = 'rtl';
        }

        $this->generateTemplate($templateVars, $adminInfo);
        $this->transportBuilder->addTo($this->config->getAdminReceiverEmail(), $adminInfo['name']);
        $this->setTemplateByBreachType($breachType);
        try {
            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
            $this->logData('SUCCESS', $adminInfo['name'], $sla->getType(), $breachType);

            //Update SLA sent status and/or Delete it if sent to all
            if ($breachType === SlaDataInterface::BREACH_TYPE_DEADLINE) {
                if ($this->checkIfSlaSentToAll($sla->getMerchantNotification())) {
                    $this->preserveSlaDataForReport($sla);
                    $this->slaRepository->delete($sla);
                } else {
                    $sla->setAdminNotification(SlaDataInterface::SLA_NOTIFICATION_DEADLINE_SENT);
                    $this->slaRepository->save($sla);
                }
            } elseif ($breachType === SlaDataInterface::BREACH_TYPE_REMINDER) {
                $sla->setAdminNotification(SlaDataInterface::SLA_NOTIFICATION_REMINDER_SENT);
                $this->slaRepository->save($sla);
            }
        } catch (Exception $e) {
            $this->logData('FAILURE', $adminInfo['name'], $sla->getType(), $breachType);
            $this->_logger->error("SLA ADMIN EMAIL ERROR : " . $e->getMessage());
        }

        $this->inlineTranslation->resume();
    }

    /**
     * Send Reminder or Deadline Breach Email to Merchant
     *
     * @param SlaDataInterface $sla
     * @param array $templateVars
     * @param array $senderInfo
     * @param array $receiverInfo
     * @param string $breachType
     *
     * @return void
     *
     * @throws MailException
     */
    private function sendNotificationToMerchant(
        SlaDataInterface $sla,
        array $templateVars,
        array $senderInfo,
        array $receiverInfo,
        string $breachType
    ) {
        $this->inlineTranslation->suspend();
        $templateVars['user_name'] = $receiverInfo['name'];
        $templateVars['sla_breach_date'] = $this->timezone->formatDateTime($templateVars['sla_breach_date'], 2, 2);
        $templateVars['sla_date'] = $this->timezone->formatDateTime($templateVars['sla_date'], 2, 2);

        $store = $this->storeManager->getStore($senderInfo['store_id']);
        $templateVars['email_content_alignment'] = 'left';
        $templateVars['email_content_direction'] = 'ltr';

        if ($store->getCode() === 'ar_SA') {
            $templateVars['email_content_alignment'] = 'right';
            $templateVars['email_content_direction'] = 'rtl';
        }

        $this->generateTemplate($templateVars, $senderInfo);
        $this->transportBuilder->addTo($receiverInfo['email'], $receiverInfo['name']);
        $this->setTemplateByBreachType($breachType);
        try {
            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
            $this->logData('SUCCESS', $receiverInfo['name'], $sla->getType(), $breachType);

            //Update SLA sent status and/or Delete it if sent to all
            if ($breachType === SlaDataInterface::BREACH_TYPE_DEADLINE) {
                if ($this->checkIfSlaSentToAll($sla->getAdminNotification())) {
                    $this->preserveSlaDataForReport($sla);
                    $this->slaRepository->delete($sla);
                } else {
                    $sla->setMerchantNotification(SlaDataInterface::SLA_NOTIFICATION_DEADLINE_SENT);
                    $this->slaRepository->save($sla);
                }
            } elseif ($breachType === SlaDataInterface::BREACH_TYPE_REMINDER) {
                $sla->setMerchantNotification(SlaDataInterface::SLA_NOTIFICATION_REMINDER_SENT);
                $this->slaRepository->save($sla);
            }
        } catch (Exception $e) {
            $this->logData('FAILURE', $receiverInfo['name'], $sla->getType(), $breachType);
            $this->_logger->error("SLA MERCHANT EMAIL ERROR : " . $e->getMessage());
        }

        $this->inlineTranslation->resume();
    }

    /**
     * Check if SLA was sent to all and should be deleted
     *
     * @param int $notificationFlag
     *
     * @return bool
     */
    private function checkIfSlaSentToAll(int $notificationFlag)
    {
        if ($notificationFlag === SlaDataInterface::SLA_NOTIFICATION_DO_NOT_SEND ||
            $notificationFlag === SlaDataInterface::SLA_NOTIFICATION_DEADLINE_SENT) {
            return true;
        }

        return false;
    }

    /**
     * Generate email Template
     *
     * @param array $templateVariables
     * @param array $senderInfo
     *
     * @return void
     *
     * @throws MailException
     */
    private function generateTemplate(array $templateVariables, array $senderInfo)
    {
        $this->transportBuilder
            ->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $senderInfo['store_id'],
                ]
            )
            ->setTemplateVars($templateVariables)
            ->setFromByScope($senderInfo)
            ->setReplyTo($senderInfo['email'], $senderInfo['name']);
    }

    /**
     * @param string $breachType
     *
     * @return void
     */
    private function setTemplateByBreachType(string $breachType)
    {
        if ($breachType === SlaDataInterface::BREACH_TYPE_DEADLINE) {
            $this->transportBuilder->setTemplateIdentifier(self::SLA_EMAIL_DEADLINE_BREACH_TEMPLATE_ID);
        } else {
            $this->transportBuilder->setTemplateIdentifier(self::SLA_EMAIL_REMINDER_TEMPLATE_ID);
        }
    }

    /**
     * Log Email status and data into separate file
     *
     * @param string $status
     * @param string $receiver
     * @param string $slaType
     * @param string $emailType
     *
     * @return void
     */
    private function logData(string $status, string $receiver, string $slaType, string $emailType)
    {
        $message = '%s - EMAIL TYPE: %s >> RECEIVER: %s >> SLA TYPE: %s';
        $this->logger->info(sprintf($message, $status, $emailType, $receiver, $slaType));
    }

    /**
     * @param SlaDataInterface $slaData
     *
     * @return void
     */
    private function preserveSlaDataForReport(SlaDataInterface $slaData)
    {
        $slaReportData = $this->slaReportFactory->create();

        $slaReportData->setType($slaData->getType());
        $slaReportData->setMerchantId($slaData->getMerchantId());
        $slaReportData->setBreachTime($slaData->getDeadlineTime());
        $slaReportData->setStoreId((int)$slaData->getStoreId());
        $slaReportData->setRelatedEntityId($slaData->getRelatedEntityId());

        $this->slaReportRepository->save($slaReportData);
    }
}
