<?php
/**
 * Report Grid Container
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Block\Adminhtml\Sales;

use Magento\Backend\Block\Widget\Grid\Container;

class SlaReport extends Container
{
    protected $_template = 'Magento_Reports::report/grid/container.phtml';

    protected function _construct()
    {
        $this->_blockGroup = 'Arb_Sla';
        $this->_controller = 'adminhtml_sales_slareport';
        $this->_headerText = __('SLA Report');
        parent::_construct();

        $this->buttonList->remove('add');
        $this->addButton(
            'filter_form_submit',
            ['label' => __('Show Report'), 'onclick' => 'filterFormSubmit()', 'class' => 'primary']
        );
    }

    /**
     * Get filter URL
     *
     * @return string
     */
    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/slareport', ['_current' => true]);
    }
}
