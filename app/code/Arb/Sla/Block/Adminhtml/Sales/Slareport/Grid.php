<?php
/**
 * Custom Grid for SLA Report
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Block\Adminhtml\Sales\Slareport;

use Magento\Reports\Block\Adminhtml\Grid\AbstractGrid;
use Arb\Sla\Model\ResourceModel\Report\SlaReport\Collection;
use Magento\Reports\Model\ResourceModel\Report\Collection\AbstractCollection;
use Magento\Framework\DataObject;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\Datetime;

class Grid extends AbstractGrid
{
    /**
     * GROUP BY criteria
     *
     * @var string
     */
    protected $_columnGroupBy = 'id';

    protected function _construct()
    {
        parent::_construct();

        $this->setCountTotals(false);
    }

    /**
     * @return string
     */
    public function getResourceCollectionName()
    {
        return Collection::class;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return AbstractGrid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'period',
            [
                'header' => __('Breach Time'),
                'index' => 'period',
                'sortable' => true,
                'period_type' => $this->getPeriodType(),
                'renderer' => Datetime::class,
                'totals_label' => __('Total Count'),
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'sla_type',
            [
                'header' => __('SLA Type'),
                'index' => 'sla_type',
                'type' => 'string',
                'sortable' => false,
                'translate' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'merchant',
            [
                'header' => __('Merchant Name'),
                'index' => 'merchant',
                'type' => 'string',
                'sortable' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'entity_type',
            [
                'header' => __('Entity Type'),
                'index' => 'entity_type',
                'type' => 'string',
                'sortable' => false,
                'translate' => true,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        $this->addColumn(
            'related_entity_id',
            [
                'header' => __('Related Entity ID'),
                'index' => 'related_entity_id',
                'type' => 'string',
                'sortable' => false,
                'header_css_class' => 'col-product',
                'column_css_class' => 'col-product'
            ]
        );

        if ($this->getFilterData()->getStoreIds()) {
            $this->setStoreIds(explode(',', $this->getFilterData()->getStoreIds()));
        }

        $this->addExportType('*/*/exportSlaReportCsv', __('CSV'));
        $this->addExportType('*/*/exportSlaReportExcel', __('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * @codeCoverageIgnore
     *
     * @param AbstractCollection $collection
     * @param DataObject $filterData
     *
     * @return AbstractGrid
     */
    protected function _addCustomFilter($collection, $filterData)
    {
        if ($filterData->getSlaBreachType()) {
            $typesList = $filterData->getData('types_list');
            if (isset($typesList[0])) {
                $typesIds = explode(',', $typesList[0]);
                $collection->addSlaFilter($typesIds);
            }
        }

        if ($filterData->getSlaResponsiblePerson() === '1') {
            $collection->addMerchantIdFilter([0]);
        }

        if ($filterData->getMerchantIdField()) {
            $merchantIds = $filterData->getData('merchant_id_field');
            if ($merchantIds) {
                $merchantIdArray = explode(',', $merchantIds);
                $collection->addMerchantIdFilter($merchantIdArray);
            }
        }

        return parent::_addCustomFilter($filterData, $collection);
    }
}
