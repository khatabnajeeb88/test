<?php
/**
 * Custom Filters for Report
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Block\Adminhtml\Filter;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Block\Adminhtml\Report\Filter\Form as ParentForm;
use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Backend\Block\Widget\Form\Element\Dependence;
use Magento\Framework\Data\Form\Element\Fieldset;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Sales\Model\Order\ConfigFactory;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Backend\Block\Template\Context;

class Form extends ParentForm
{
    /**
     * @var SellerCollection
     */
    private $sellerCollectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param ConfigFactory $orderConfig
     * @param SellerCollection $sellerCollectionFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        ConfigFactory $orderConfig,
        SellerCollection $sellerCollectionFactory,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $orderConfig,
            $data
        );

        $this->sellerCollectionFactory = $sellerCollectionFactory;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Flag that keep info should we render specific dependent element or not
     *
     * @var bool
     */
    protected $_renderDependentElement = false;

    /**
     * @codeCoverageIgnore
     *
     * Add fields to base fieldset which are general to sales reports
     *
     * @return self
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        /** @var Fieldset $fieldset */
        $fieldset = $this->getForm()->getElement('base_fieldset');
        if (is_object($fieldset) && $fieldset instanceof Fieldset) {
            $fieldset->addField(
                'sla_responsible_person',
                'select',
                [
                    'name' => 'sla_responsible_person',
                    'options' => [__('All'), __('Admin'), __('Merchant')],
                    'label' => __('SLA Responsible Person')
                ]
            );

            $merchantCollection = $this->sellerCollectionFactory->create();
            $merchantCollection->addFieldToFilter(
                'is_seller',
                ['eq' => 1]
            );

            $merchants = [
                [
                    'label' => 'Any',
                    'value' => ''
                ]
            ];

            $customers = [];
            foreach ($merchantCollection->getItems() as $merchant) {
                $id = $merchant->getData('seller_id');
                if (!array_key_exists($id, $customers)) {
                    try {
                        $customers[$id] = $this->customerRepository->getById($id);
                        $customerName = $customers[$id]->getFirstname() . ' ' . $customers[$id]->getLastname();
                        $merchants[] = ['label' => __($customerName), 'value' => $id];
                    } catch (NoSuchEntityException $e) {
                        $customers[$id] = '';
                    } catch (LocalizedException $e) {
                        $customers[$id] = '';
                    }
                }
            }

            $fieldset->addField(
                'merchant_id_field',
                'select',
                [
                    'name' => 'merchant_id_field',
                    'label' => '',
                    'values' => $merchants,
                    'display' => 'none'
                ],
                'sla_responsible_person'
            );

            $fieldset->addField(
                'sla_breach_type',
                'select',
                [
                    'name' => 'sla_breach_type',
                    'options' => [__('Any'), __('Specified')],
                    'label' => __('SLA Type')
                ]
            );

            $slaTypeList = [];
            foreach (SlaDataInterface::SLA_TYPE_LABEL as $label => $typeCode) {
                $slaTypeList[] = ['label' => __($typeCode), 'value' => __($label), 'title' => $typeCode];
            }

            $fieldset->addField(
                'types_list',
                'multiselect',
                [
                    'name' => 'types_list',
                    'label' => '',
                    'values' => $slaTypeList,
                    'display' => 'none'
                ],
                'sla_breach_type'
            );

            $this->_renderDependentElement = true;
        }

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * Processing block html after rendering
     *
     * @param string $html
     * @return string
     *
     * @throws LocalizedException
     */
    protected function _afterToHtml($html)
    {
        if ($this->_renderDependentElement) {
            $form = $this->getForm();
            $htmlIdPrefix = $form->getHtmlIdPrefix();

            /** @var Dependence $formAfterBlock */
            $formAfterBlock = $this->getLayout()->createBlock(
                Dependence::class,
                'adminhtml.block.widget.form.element.dependence'
            );
            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'sla_breach_type',
                'sla_breach_type'
            )->addFieldMap(
                $htmlIdPrefix . 'types_list',
                'types_list'
            )->addFieldDependence(
                'types_list',
                'sla_breach_type',
                '1'
            );

            $formAfterBlock->addFieldMap(
                $htmlIdPrefix . 'sla_responsible_person',
                'sla_responsible_person'
            )->addFieldMap(
                $htmlIdPrefix . 'merchant_id_field',
                'merchant_id_field'
            )->addFieldDependence(
                'merchant_id_field',
                'sla_responsible_person',
                '2'
            );

            $html = $html . $formAfterBlock->toHtml();
        }

        return $html;
    }
}
