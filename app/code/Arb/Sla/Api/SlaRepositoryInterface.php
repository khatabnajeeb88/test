<?php
/**
 * Interface for SLA Repository Class
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Api;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\Api\SearchResultsInterface;

interface SlaRepositoryInterface
{
    /**
     * @param \Arb\Sla\Api\Data\SlaDataInterface $slaData
     *
     * @return \Arb\Sla\Api\Data\SlaDataInterface
     */
    public function save(\Arb\Sla\Api\Data\SlaDataInterface $slaData);

    /**
     * @param \Arb\Sla\Api\Data\SlaDataInterface $slaData
     *
     * @return void
     */
    public function delete(\Arb\Sla\Api\Data\SlaDataInterface $slaData);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param string $type
     * @param int|null $storeId
     *
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getAllExpiredSla(string $type, ?int $storeId);
}
