<?php
/**
 * Interface for SLA Report Repository Class
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Api;

use Arb\Sla\Api\Data\SlaReportInterface;
use Magento\Framework\Api\SearchResultsInterface;

interface SlaReportRepositoryInterface
{
    /**
     * @param \Arb\Sla\Api\Data\SlaReportInterface $slaReport
     *
     * @return \Arb\Sla\Api\Data\SlaReportInterface
     */
    public function save(\Arb\Sla\Api\Data\SlaReportInterface $slaReport);

    /**
     * @param \Arb\Sla\Api\Data\SlaDataInterface $slaReport
     *
     * @return void
     */
    public function delete(\Arb\Sla\Api\Data\SlaReportInterface $slaReport);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
