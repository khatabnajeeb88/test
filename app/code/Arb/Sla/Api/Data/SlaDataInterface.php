<?php
/**
 * Interface for SLA Data Class with getters/setters
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Api\Data;

interface SlaDataInterface
{
    public const SLA_TABLE_NAME = 'arb_sla_timers';

    public const SLA_ENTITY_ID = 'entity_id';
    public const SLA_TYPE = 'type';
    public const SLA_MERCHANT_ID = 'merchant_id';
    public const SLA_RELATED_ENTITY_ID = 'related_entity_id';
    public const SLA_STORE_ID = 'store_id';
    public const SLA_TRIGGER_TIME = 'triggered_time';
    public const SLA_DEADLINE_TIME = 'deadline_time';
    public const SLA_REMINDER_TIME = 'reminder_time';
    public const SLA_MERCHANT_NOTIFICATION = 'merchant_notification';
    public const SLA_ADMIN_NOTIFICATION = 'admin_notification';

    public const RESPONSIBLE_PERSON_MERCHANT = 'Merchant';
    public const RESPONSIBLE_PERSON_ADMIN = 'Admin';

    /**
     * Entries should be deleted once required emails are sent, flag '2' is in case an email fails to be sent
     */
    public const SLA_NOTIFICATION_NOTHING_SENT = 0;
    public const SLA_NOTIFICATION_REMINDER_SENT = 1;
    public const SLA_NOTIFICATION_DEADLINE_SENT = 2;
    public const SLA_NOTIFICATION_DO_NOT_SEND = 3; // e.g. in case when only Admin should receive notification

    public const SLA_TYPE_LOW_STOCK = 'product_low_stock';
    public const SLA_TYPE_NEW_ORDER = 'confirm_new_order';
    public const SLA_TYPE_ORDER_SHIPPING = 'required_order_shipping';
    public const SLA_TYPE_RETURN_REQUEST = 'return_request';
    public const SLA_TYPE_PRODUCT_APPROVAL = 'product_approval_required';

    public const SLA_TYPE_LABEL = [
        self::SLA_TYPE_LOW_STOCK => 'Product reached low stock status',
        self::SLA_TYPE_NEW_ORDER => 'New order requires merchant action',
        self::SLA_TYPE_ORDER_SHIPPING => 'Order requires shipping by merchant',
        self::SLA_TYPE_RETURN_REQUEST => 'Return Request requires merchant action',
        self::SLA_TYPE_PRODUCT_APPROVAL => 'Merchant Product requires Admin review'
    ];

    public const SLA_RESPONSIBLE_FOR_TYPE = [
        self::SLA_TYPE_LOW_STOCK => 'Merchant',
        self::SLA_TYPE_NEW_ORDER => 'Merchant',
        self::SLA_TYPE_ORDER_SHIPPING => 'Merchant',
        self::SLA_TYPE_RETURN_REQUEST => 'Merchant',
        self::SLA_TYPE_PRODUCT_APPROVAL => 'Admin'
    ];

    public const BREACH_TYPE_REMINDER = 'reminder';
    public const BREACH_TYPE_DEADLINE = 'deadline_breach';

    /**
     * @return string|null
     */
    public function getType();

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type);

    /**
     * @return int|null
     */
    public function getRelatedEntityId();

    /**
     * @param int $entityId
     *
     * @return $this
     */
    public function setRelatedEntityId(int $entityId);

    /**
     * @return int|null
     */
    public function getMerchantId();

    /**
     * @param int $merchantId
     *
     * @return $this
     */
    public function setMerchantId(int $merchantId);

    /**
     * @return int|null
     */
    public function getStoreId();

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function setStoreId(int $storeId);

    /**
     * @return string
     */
    public function getTriggerTime();

    /**
     * @param string $time
     *
     * @return $this
     */
    public function setTriggerTime(string $time);

    /**
     * @return string
     */
    public function getReminderTime();

    /**
     * @param string $time
     *
     * @return $this
     */
    public function setReminderTime(string $time);

    /**
     * @return string
     */
    public function getDeadlineTime();

    /**
     * @param string $time
     *
     * @return $this
     */
    public function setDeadlineTime(string $time);

    /**
     * @return int
     */
    public function getMerchantNotification();

    /**
     * @param int $notificationFlag
     *
     * @return $this
     */
    public function setMerchantNotification(int $notificationFlag);

    /**
     * @return int
     */
    public function getAdminNotification();

    /**
     * @param int $notificationFlag
     *
     * @return $this
     */
    public function setAdminNotification(int $notificationFlag);
}
