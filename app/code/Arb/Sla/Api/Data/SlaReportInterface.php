<?php
/**
 * Interface for SLA Report Class with getters/setters
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Api\Data;

interface SlaReportInterface
{
    public const SLA_TABLE_NAME = 'arb_sla_breached';

    public const SLA_ENTITY_ID = 'entity_id';
    public const SLA_TYPE = 'type';
    public const SLA_MERCHANT_ID = 'merchant_id';
    public const SLA_MERCHANT = 'merchant';
    public const SLA_RELATED_ENTITY_ID = 'related_entity_id';
    public const SLA_STORE_ID = 'store_id';
    public const SLA_BREACH_TIME = 'breach_time';

    /**
     * @return string|null
     */
    public function getType();

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type);

    /**
     * @return int|null
     */
    public function getRelatedEntityId();

    /**
     * @param int $entityId
     *
     * @return $this
     */
    public function setRelatedEntityId(int $entityId);

    /**
     * @return int|null
     */
    public function getMerchantId();

    /**
     * @param int $merchantId
     *
     * @return $this
     */
    public function setMerchantId(int $merchantId);

    /**
     * @return string|null
     */
    public function getMerchant();

    /**
     * @param string $merchant
     *
     * @return $this
     */
    public function setMerchant(string $merchant);

    /**
     * @return int|null
     */
    public function getStoreId();

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function setStoreId(int $storeId);

    /**
     * @return string
     */
    public function getBreachTime();

    /**
     * @param string $time
     *
     * @return $this
     */
    public function setBreachTime(string $time);
}
