<?php
/**
 * Plugin to enable SLA Report generation in admin
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Plugin\Magento\Reports\Model\ResourceModel\Refresh;

use Magento\Framework\Data\Collection as ParentCollection;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Reports\Model\FlagFactory;
use Magento\Framework\Data\Collection\EntityFactory;
use Magento\Framework\Exception\LocalizedException;
use Arb\Sla\Model\Flag;
use Magento\Framework\DataObject;

class Collection extends ParentCollection
{
    /**
     * @var TimezoneInterface
     */
    protected $_localeDate;

    /**
     * @var FlagFactory
     */
    protected $_reportsFlagFactory;

    /**
     * @param EntityFactory $entityFactory
     * @param TimezoneInterface $localeDate
     * @param FlagFactory $reportsFlagFactory
     */
    public function __construct(
        EntityFactory $entityFactory,
        TimezoneInterface $localeDate,
        FlagFactory $reportsFlagFactory
    ) {
        parent::__construct($entityFactory);

        $this->_localeDate = $localeDate;
        $this->_reportsFlagFactory = $reportsFlagFactory;
    }

    /**
     * Get if updated
     *
     * @param string $reportCode
     *
     * @return string
     *
     * @throws LocalizedException
     */
    protected function _getUpdatedAt($reportCode)
    {
        $flag = $this->_reportsFlagFactory->create()->setReportFlagCode($reportCode)->loadSelf();

        return $flag->hasData() ? $flag->getLastUpdate() : '';
    }

    /**
     * Load data
     *
     * @param $subject
     * @param $result
     * @param bool $printQuery
     * @param bool $logQuery
     *
     * @return $this
     *
     * @throws LocalizedException
     */
    public function afterLoadData($subject, $result, $printQuery = false, $logQuery = false)
    {
        if (!count($this->_items)) {
            $data = [
                [
                    'id' => 'slareport',
                    'report' => __('SLA Report'),
                    'comment' => __('SLA Report'),
                    'updated_at' => $this->_getUpdatedAt(Flag::REPORT_SLAREPORT_FLAG_CODE)
                ],
            ];

            foreach ($data as $value) {
                $item = new DataObject();
                $item->setData($value);
                $this->addItem($item);
                $subject->addItem($item);
            }
        }

        return $subject;
    }
}
