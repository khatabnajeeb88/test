<?php
/**
 * Handler to log SLA info in separate file to avoid clutter
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Logger;

use Magento\Framework\Filesystem\DriverInterface;
use Monolog\Logger as MonologLogger;
use Magento\Framework\Logger\Handler\Base as HandlerBase;
use Exception;

class Handler extends HandlerBase
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = MonologLogger::INFO;

    /**
     * @param DriverInterface $filesystem
     * @param null $filePath
     * @param null $fileName
     *
     * @throws Exception
     */
    public function __construct(DriverInterface $filesystem, $filePath = null, $fileName = null)
    {
        $this->fileName = '/var/log/sla-' . date("Ymd") . '.log';

        parent::__construct($filesystem, $filePath, $fileName);
    }
}
