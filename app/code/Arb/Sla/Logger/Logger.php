<?php
/**
 * Logger to track SLA flow
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Logger;

use Monolog\Logger as MonologLogger;

class Logger extends MonologLogger
{

}
