<?php
/**
 * Repository for SLA Report
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model;

use Arb\Sla\Api\Data\SlaReportInterface;
use Arb\Sla\Api\SlaReportRepositoryInterface;
use Arb\Sla\Model\ResourceModel\BreachedSla\CollectionFactory;
use Arb\Sla\Model\ResourceModel\BreachedSla\Collection;
use Arb\Sla\Model\ResourceModel\SlaReportResource;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Exception;

class SlaReportRepository implements SlaReportRepositoryInterface
{
    /**
     * @var SlaReportResource
     */
    private $slaResource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsInterfaceFactory;

    /**
     * @param SlaReportResource $slaResource
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchResultsInterfaceFactory $searchResultsInterfaceFactory
     */
    public function __construct(
        SlaReportResource $slaResource,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultsInterfaceFactory $searchResultsInterfaceFactory
    ) {
        $this->slaResource = $slaResource;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsInterfaceFactory = $searchResultsInterfaceFactory;
    }

    /**
     * @param SlaReportInterface $slaData
     *
     * @return SlaReportInterface
     * @throws CouldNotSaveException
     */
    public function save(SlaReportInterface $slaData)
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->slaResource->save($slaData);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $slaData;
    }

    /**
     * @param SlaReportInterface $slaData
     *
     * @throws Exception
     */
    public function delete(SlaReportInterface $slaData)
    {
        /** @noinspection PhpParamsInspection */
        $this->slaResource->delete($slaData);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsInterfaceFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
