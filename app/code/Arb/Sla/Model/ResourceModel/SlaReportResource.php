<?php
/**
 * Resource Model for SLA Report
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model\ResourceModel;

use Arb\Sla\Api\Data\SlaReportInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SlaReportResource extends AbstractDb
{
    /**
     * Initialize Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SlaReportInterface::SLA_TABLE_NAME, SlaReportInterface::SLA_ENTITY_ID);
    }
}
