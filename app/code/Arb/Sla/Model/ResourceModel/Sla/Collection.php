<?php
/**
 * Collection for SLA entity
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model\ResourceModel\Sla;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Arb\Sla\Model\ResourceModel\SlaResource as SlaResource;
use Arb\Sla\Model\SlaData;

class Collection extends AbstractCollection
{
    /**
     * @codeCoverageIgnore
     *
     * Define Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SlaData::class, SlaResource::class);
    }
}
