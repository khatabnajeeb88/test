<?php
/**
 * Collection for breached SLA entity for Reports
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model\ResourceModel\BreachedSla;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Arb\Sla\Model\ResourceModel\SlaReportResource;
use Arb\Sla\Model\SlaReport;

class Collection extends AbstractCollection
{
    /**
     * @codeCoverageIgnore
     *
     * Define Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SlaReport::class, SlaReportResource::class);
    }
}
