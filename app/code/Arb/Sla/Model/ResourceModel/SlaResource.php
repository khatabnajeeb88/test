<?php
/**
 * Resource Model for SLA
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Arb\Sla\Api\Data\SlaDataInterface;

class SlaResource extends AbstractDb
{
    /**
     * Initialize Resource Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SlaDataInterface::SLA_TABLE_NAME, SlaDataInterface::SLA_ENTITY_ID);
    }
}
