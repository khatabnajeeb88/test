<?php
/**
 * Report class for generating entries
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model\ResourceModel\Report;

use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\Data\SlaReportInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\ResourceModel\Report\AbstractReport;
use Arb\Sla\Model\Flag;
use Exception;
use Arb\Sla\Api\SlaReportRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\Timezone\Validator;
use Magento\Reports\Model\FlagFactory;
use Psr\Log\LoggerInterface;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository as WebkulOrderRepository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class SlaReport extends AbstractReport
{
    private const SLA_REPORT_TABLE = 'arb_sla_report';
    private const SLA_REPORT_ENTITY_ID = 'id';

    private const SLA_ENTITY_FOR_TYPE = [
        SlaDataInterface::SLA_TYPE_LOW_STOCK => 'Merchant Product',
        SlaDataInterface::SLA_TYPE_NEW_ORDER => 'Order',
        SlaDataInterface::SLA_TYPE_ORDER_SHIPPING => 'Order',
        SlaDataInterface::SLA_TYPE_RETURN_REQUEST => 'RMA',
        SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL => 'Merchant Product'
    ];

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SlaReportRepositoryInterface
     */
    private $slaReportRepository;

    /**
     * @var WebkulOrderRepository
     */
    private $webkulOrderRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @param Context $context
     * @param LoggerInterface $logger
     * @param TimezoneInterface $localeDate
     * @param FlagFactory $reportsFlagFactory
     * @param Validator $timezoneValidator
     * @param DateTime $dateTime
     * @param ResourceConnection $resource
     * @param TimezoneInterface $timezone
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SlaReportRepositoryInterface $slaReportRepository
     * @param WebkulOrderRepository $webkulOrderRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        TimezoneInterface $localeDate,
        FlagFactory $reportsFlagFactory,
        Validator $timezoneValidator,
        DateTime $dateTime,
        ResourceConnection $resource,
        TimezoneInterface $timezone,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SlaReportRepositoryInterface $slaReportRepository,
        WebkulOrderRepository $webkulOrderRepository,
        OrderRepositoryInterface $orderRepository,
        CustomerRepositoryInterface $customerRepository,
        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $logger,
            $localeDate,
            $reportsFlagFactory,
            $timezoneValidator,
            $dateTime,
            $connectionName
        );

        $this->resource = $resource;
        $this->timezone = $timezone;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->slaReportRepository = $slaReportRepository;
        $this->webkulOrderRepository = $webkulOrderRepository;
        $this->orderRepository = $orderRepository;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::SLA_REPORT_TABLE, self::SLA_REPORT_ENTITY_ID);
    }

    /**
     * @param string|int|DateTime|array|null $from
     * @param string|int|DateTime|array|null $to
     *
     * @return self
     */
    public function aggregate($from = null, $to = null)
    {
        $connection = $this->getConnection();

        if ($from) {
            $this->searchCriteriaBuilder->addFilter(
                'breach_time',
                $from,
                'gt'
            );
        }

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $slaList = $this->slaReportRepository->getList($searchCriteria);
        //$this->truncateTable();
        $insertBatches = [];

        if ($slaList) {
            $customerList = [];

            /** @var SlaReportInterface $slaData */
            foreach ($slaList->getItems() as $slaData) {
                $incrementTypes = [
                    SlaDataInterface::SLA_TYPE_NEW_ORDER,
                    SlaDataInterface::SLA_TYPE_ORDER_SHIPPING
                ];
                $relatedEntityId = $slaData->getRelatedEntityId();

                if (in_array($slaData->getType(), $incrementTypes)) {
                    try {
                        /** @var Orders $merchantOrder */
                        $merchantOrder = $this->webkulOrderRepository->getById($relatedEntityId);
                        $order = $this->orderRepository->get($merchantOrder->getOrderId());
                        $relatedEntityId = $order->getIncrementId();
                    } catch (NoSuchEntityException $e) {
                        $relatedEntityId = 0;
                    } catch (LocalizedException $e) {
                        $relatedEntityId = 0;
                    }
                }
                $merchantId = $slaData->getMerchantId();

                if (!array_key_exists($merchantId, $customerList)) {
                    try {
                        $customer = $this->customerRepository->getById($merchantId);
                        $customerList[$merchantId] = $customer->getFirstname() . ' ' . $customer->getLastname();
                    } catch (NoSuchEntityException $e) {
                        $customerList[$merchantId] = '';
                    } catch (LocalizedException $e) {
                        $customerList[$merchantId] = '';
                    }
                }

                $insertBatches[] = [
                    'period' => $slaData->getBreachTime(),
                    'store_id' => $slaData->getStoreId() ?? 1,
                    'sla_type' => __(SlaDataInterface::SLA_TYPE_LABEL[$slaData->getType()]),
                    'merchant_id' => $merchantId,
                    'merchant' =>  $customerList[$merchantId],
                    'entity_type' => self::SLA_ENTITY_FOR_TYPE[$slaData->getType()],
                    'related_entity_id' => $relatedEntityId
                ];
            }
        }

        $tableName = $this->resource->getTableName(self::SLA_REPORT_TABLE);
        foreach (array_chunk($insertBatches, 100) as $batch) {
            $connection->insertMultiple($tableName, $batch);
        }

        $this->_setFlagData(Flag::REPORT_SLAREPORT_FLAG_CODE);

        return $this;
    }

    /**
     * @return void
     */
    public function truncateTable()
    {
        $table = $this->resource->getTableName(self::SLA_REPORT_TABLE);
        $connection = $this->resource->getConnection();
        $connection->truncateTable($table);
    }
}
