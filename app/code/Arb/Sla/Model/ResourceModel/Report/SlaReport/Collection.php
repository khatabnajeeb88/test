<?php
/**
 * SLA Report Collection
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model\ResourceModel\Report\SlaReport;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Sales\Model\ResourceModel\Report\Collection\AbstractCollection;
use Zend_Db_Expr;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\ResourceModel\Report;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\EntityFactory;

class Collection extends AbstractCollection
{
    /**
     * @var array
     */
    protected $_selectedColumns = [];

    /**
     * @var Zend_Db_Expr
     */
    private $_periodFormat;

    /**
     * @var array
     */
    private $slaType;

    /**
     * @var array
     */
    private $merchantId;

    /**
     * @param EntityFactory $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param Report $resource
     * @param AdapterInterface $connection
     */
    public function __construct(
        EntityFactory $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        Report $resource,
        AdapterInterface $connection = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $resource, $connection);

        $resource->init('arb_sla_report');
    }

    /**
     * @codeCoverageIgnore
     *
     * Retrieve selected columns
     *
     * @return array
     */
    protected function _getSelectedColumns()
    {
        $connection = $this->getConnection();
        $this->_periodFormat = $connection->getDateFormatSql('period', '%Y-%m-%d %H:%i:%s');

        if (!$this->_selectedColumns) {
            if ($this->isTotals()) {
                $this->_selectedColumns = ['id'];
            } else {
                $this->_selectedColumns = [
                    'id' => 'id',
                    'period' => sprintf('MAX(%s)', $connection->getDateFormatSql('period', '%Y-%m-%d %H:%i:%s')),
                    'sla_type' => 'sla_type',
                    'merchant' => 'merchant',
                    'entity_type' => 'entity_type',
                    'related_entity_id' => 'related_entity_id'
                ];
            }
        }

        return $this->_selectedColumns;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return AbstractCollection
     *
     * @throws LocalizedException
     */
    protected function _applyAggregatedTable()
    {
        $this->getSelect()->from($this->getResource()->getMainTable(), $this->_getSelectedColumns());
        if ($this->isSubTotals()) {
            $this->getSelect()->group('id');
        } elseif (!$this->isTotals()) {
            $this->getSelect()->group(
                [
                    'id'
                ]
            );
        }

        return parent::_applyAggregatedTable();
    }

    /**
     * Add filtering by sla types
     *
     * @param array $slaTypes
     *
     * @return self
     */
    public function addSlaFilter(array $slaTypes)
    {
        $this->slaType = $slaTypes;

        return $this;
    }

    /**
     * Add filtering by merchant ids
     *
     * @param array $merchantId
     *
     * @return self
     */
    public function addMerchantIdFilter(array $merchantId)
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * Apply filtering by sla types
     *
     * @return self
     */
    protected function applySlaTypeFilter()
    {
        if (empty($this->slaType) || !is_array($this->slaType)) {
            return $this;
        }

        $slaTypeFilterSqlParts = [];
        foreach ($this->slaType as $slaType) {
            $slaLabel = SlaDataInterface::SLA_TYPE_LABEL[$slaType];
            $slaTypeFilterSqlParts[] = $this->getConnection()->quoteInto('sla_type = ?', $slaLabel);
        }

        if (!empty($slaTypeFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $slaTypeFilterSqlParts));
        }

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * Apply filtering by merchant ids
     *
     * @return self
     */
    protected function applyMerchantIdFilter()
    {
        if (empty($this->merchantId) || !is_array($this->merchantId)) {
            return $this;
        }

        if ($this->merchantId[0] === 0) {
            $this->slaType = [SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL];
            return $this->applySlaTypeFilter();
        }

        $merchantIdsFilterSqlParts = [];
        foreach ($this->merchantId as $merchantId) {
            $merchantIdsFilterSqlParts[] = $this->getConnection()->quoteInto('merchant_id = ?', $merchantId);
        }

        if (!empty($merchantIdsFilterSqlParts)) {
            $this->getSelect()->where(implode(' OR ', $merchantIdsFilterSqlParts));
        }

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * Apply collection custom filter
     *
     * @return AbstractCollection
     */
    protected function _applyCustomFilter()
    {
        $this->applySlaTypeFilter();
        $this->applyMerchantIdFilter();

        return parent::_applyCustomFilter();
    }
}
