<?php
/**
 * SLA Report Flag
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model;

use Magento\Reports\Model\Flag as ParentFlag;

class Flag extends ParentFlag
{
    const REPORT_SLAREPORT_FLAG_CODE = 'report_slareport_aggregated';
}
