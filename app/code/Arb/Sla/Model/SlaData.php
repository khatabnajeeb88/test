<?php
/**
 * SLA Data Class with getters/setters
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */
namespace Arb\Sla\Model;

use Magento\Framework\Model\AbstractModel;
use Arb\Sla\Model\ResourceModel\SlaResource;
use Arb\Sla\Api\Data\SlaDataInterface;

class SlaData extends AbstractModel implements SlaDataInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init(SlaResource::class);
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->getData(SlaDataInterface::SLA_TYPE);
    }

    /**
     * @param string $type
     *
     * @return SlaDataInterface
     */
    public function setType(string $type)
    {
        $this->setData(SlaDataInterface::SLA_TYPE, $type);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRelatedEntityId()
    {
        $entityId = $this->getData(SlaDataInterface::SLA_RELATED_ENTITY_ID);

        return $entityId ? (int)$entityId : null;
    }

    /**
     * @param int $entityId
     *
     * @return SlaDataInterface
     */
    public function setRelatedEntityId(int $entityId)
    {
        $this->setData(SlaDataInterface::SLA_RELATED_ENTITY_ID, $entityId);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMerchantId()
    {
        $merchantId = $this->getData(SlaDataInterface::SLA_MERCHANT_ID);

        return $merchantId ? (int)$merchantId : null;
    }

    /**
     * @param int $merchantId
     *
     * @return SlaDataInterface
     */
    public function setMerchantId(int $merchantId)
    {
        $this->setData(SlaDataInterface::SLA_MERCHANT_ID, $merchantId);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStoreId()
    {
        $storeId = $this->getData(SlaDataInterface::SLA_STORE_ID);

        return $storeId ? (int)$storeId : null;
    }

    /**
     * @param int $storeId
     *
     * @return SlaDataInterface
     */
    public function setStoreId(int $storeId)
    {
        $this->setData(SlaDataInterface::SLA_STORE_ID, $storeId);

        return $this;
    }

    /**
     * @return string
     */
    public function getTriggerTime()
    {
        return $this->getData(SlaDataInterface::SLA_TRIGGER_TIME);
    }

    /**
     * @param string $time
     *
     * @return SlaDataInterface
     */
    public function setTriggerTime(string $time)
    {
        $this->setData(SlaDataInterface::SLA_TRIGGER_TIME, $time);

        return $this;
    }

    /**
     * @return string
     */
    public function getReminderTime()
    {
        return $this->getData(SlaDataInterface::SLA_REMINDER_TIME);
    }

    /**
     * @param string $time
     *
     * @return SlaDataInterface
     */
    public function setReminderTime(string $time)
    {
        $this->setData(SlaDataInterface::SLA_REMINDER_TIME, $time);

        return $this;
    }

    /**
     * @return string
     */
    public function getDeadlineTime()
    {
        return $this->getData(SlaDataInterface::SLA_DEADLINE_TIME);
    }

    /**
     * @param string $time
     *
     * @return SlaDataInterface
     */
    public function setDeadlineTime(string $time)
    {
        $this->setData(SlaDataInterface::SLA_DEADLINE_TIME, $time);

        return $this;
    }

    /**
     * @return int
     */
    public function getMerchantNotification()
    {
        return (int)$this->getData(SlaDataInterface::SLA_MERCHANT_NOTIFICATION);
    }

    /**
     * @param int $flag
     *
     * @return SlaDataInterface
     */
    public function setMerchantNotification(int $flag)
    {
        $this->setData(SlaDataInterface::SLA_MERCHANT_NOTIFICATION, $flag);

        return $this;
    }

    /**
     * @return int
     */
    public function getAdminNotification()
    {
        return (int)$this->getData(SlaDataInterface::SLA_ADMIN_NOTIFICATION);
    }

    /**
     * @param int $flag
     *
     * @return SlaDataInterface
     */
    public function setAdminNotification(int $flag)
    {
        $this->setData(SlaDataInterface::SLA_ADMIN_NOTIFICATION, $flag);

        return $this;
    }
}
