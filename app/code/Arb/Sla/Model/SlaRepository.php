<?php
/**
 * Repository for SLA
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Model;

use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\SlaRepositoryInterface;
use Arb\Sla\Model\ResourceModel\SlaResource;
use Arb\Sla\Model\ResourceModel\Sla\CollectionFactory;
use Arb\Sla\Model\ResourceModel\Sla\Collection;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\FilterBuilder;
use Exception;

class SlaRepository implements SlaRepositoryInterface
{
    /**
     * @var SlaResource
     */
    private $slaResource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsInterfaceFactory;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    private $filterGroupBuilder;

    /**
     * @param SlaResource $slaResource
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchResultsInterfaceFactory $searchResultsInterfaceFactory
     * @param FilterBuilder $filterBuilder
     * @param FilterGroupBuilder $filterGroupBuilder
     */
    public function __construct(
        SlaResource $slaResource,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultsInterfaceFactory $searchResultsInterfaceFactory,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder
    ) {
        $this->slaResource = $slaResource;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsInterfaceFactory = $searchResultsInterfaceFactory;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
    }

    /**
     * @param SlaDataInterface $slaData
     *
     * @return SlaDataInterface
     * @throws CouldNotSaveException
     */
    public function save(SlaDataInterface $slaData)
    {
        // Set SLA to not send notifications to Merchant if Admin is responsible
        if (SlaDataInterface::RESPONSIBLE_PERSON_ADMIN ===
            SlaDataInterface::SLA_RESPONSIBLE_FOR_TYPE[$slaData->getType()]) {
            $slaData->setMerchantNotification(SlaDataInterface::SLA_NOTIFICATION_DO_NOT_SEND);
        }

        try {
            /** @noinspection PhpParamsInspection */
            $this->slaResource->save($slaData);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $slaData;
    }

    /**
     * @param SlaDataInterface $slaData
     *
     * @throws Exception
     */
    public function delete(SlaDataInterface $slaData)
    {
        /** @noinspection PhpParamsInspection */
        $this->slaResource->delete($slaData);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsInterfaceFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param string $type
     * @param int|null $storeId
     *
     * @return SearchResultsInterface
     */
    public function getAllExpiredSla(string $type, ?int $storeId)
    {
        $reminderFilter = $this->filterBuilder->setField(SlaDataInterface::SLA_REMINDER_TIME)
            ->setValue(date('Y-m-d H:i:s'))
            ->setConditionType('lt')
            ->create();

        $deadlineFilter = $this->filterBuilder->setField(SlaDataInterface::SLA_DEADLINE_TIME)
            ->setValue(date('Y-m-d H:i:s'))
            ->setConditionType('lt')
            ->create();

        $expiredFilter = $this->filterGroupBuilder
            ->addFilter($reminderFilter)
            ->addFilter($deadlineFilter)
            ->create();

        $this->searchCriteriaBuilder->setFilterGroups([$expiredFilter]);

        if ($storeId !== null) {
            $this->searchCriteriaBuilder->addFilter(
                SlaDataInterface::SLA_STORE_ID,
                $storeId
            );
        }

        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            SlaDataInterface::SLA_TYPE,
            $type
        )->create();

        return $this->getList($searchCriteria);
    }
}
