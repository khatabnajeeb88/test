<?php
/**
 * SLA Report Data Class
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */
namespace Arb\Sla\Model;

use Arb\Sla\Api\Data\SlaReportInterface;
use Magento\Framework\Model\AbstractModel;
use Arb\Sla\Model\ResourceModel\SlaReportResource;

class SlaReport extends AbstractModel implements SlaReportInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init(SlaReportResource::class);
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->getData(SlaReportInterface::SLA_TYPE);
    }

    /**
     * @param string $type
     *
     * @return SlaReportInterface
     */
    public function setType(string $type)
    {
        $this->setData(SlaReportInterface::SLA_TYPE, $type);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRelatedEntityId()
    {
        $entityId = $this->getData(SlaReportInterface::SLA_RELATED_ENTITY_ID);

        return $entityId ? (int)$entityId : null;
    }

    /**
     * @param int $entityId
     *
     * @return SlaReportInterface
     */
    public function setRelatedEntityId(int $entityId)
    {
        $this->setData(SlaReportInterface::SLA_RELATED_ENTITY_ID, $entityId);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMerchantId()
    {
        $merchantId = $this->getData(SlaReportInterface::SLA_MERCHANT_ID);

        return $merchantId ? (int)$merchantId : null;
    }

    /**
     * @param int $merchantId
     *
     * @return SlaReportInterface
     */
    public function setMerchantId(int $merchantId)
    {
        $this->setData(SlaReportInterface::SLA_MERCHANT_ID, $merchantId);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMerchant()
    {
        return $this->getData(SlaReportInterface::SLA_MERCHANT);
    }

    /**
     * @param string $merchant
     *
     * @return SlaReportInterface
     */
    public function setMerchant(string $merchant)
    {
        $this->setData(SlaReportInterface::SLA_MERCHANT, $merchant);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStoreId()
    {
        $storeId = $this->getData(SlaReportInterface::SLA_STORE_ID);

        return $storeId ? (int)$storeId : null;
    }

    /**
     * @param int $storeId
     *
     * @return SlaReportInterface
     */
    public function setStoreId(int $storeId)
    {
        $this->setData(SlaReportInterface::SLA_STORE_ID, $storeId);

        return $this;
    }

    /**
     * @return string
     */
    public function getBreachTime()
    {
        return $this->getData(SlaReportInterface::SLA_BREACH_TIME);
    }

    /**
     * @param string $time
     *
     * @return SlaReportInterface
     */
    public function setBreachTime(string $time)
    {
        $this->setData(SlaReportInterface::SLA_BREACH_TIME, $time);

        return $this;
    }
}
