<?php
/**
 * Observer to track if Order got Approved and requires Shipping
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Helper\Config;
use Exception;
use Webkul\Marketplace\Model\Orders;
use Arb\Sla\Helper\SlaEventManagement;

class CreateShippingRequiredSlaObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @param Config $config
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(
        Config $config,
        SlaEventManagement $slaEventManagement
    ) {
        $this->config = $config;
        $this->slaEventManagement = $slaEventManagement;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     *
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        /** @var Orders $merchantOrder */
        $merchantOrder = $observer->getOrder();
        $this->createSla($merchantOrder);
    }

    /**
     * Create New SLA and link it with Webkul Order to track further changes
     *
     * @param Orders $merchantOrder
     *
     * @return void
     *
     * @throws Exception
     */
    private function createSla(Orders $merchantOrder)
    {
        $sla = $this->slaEventManagement->initSla(
            SlaDataInterface::SLA_TYPE_ORDER_SHIPPING,
            (int)$merchantOrder->getId()
        );

        $sla = $this->slaEventManagement->setSlaData(
            $sla,
            (int)$merchantOrder->getId(),
            $this->config->getFinalizeOrderReminderTime(),
            $this->config->getFinalizeOrderDeadlineTime(),
            SlaDataInterface::SLA_TYPE_ORDER_SHIPPING,
            (int)$merchantOrder->getSellerId()
        );

        $this->slaEventManagement->saveSla($sla);
    }
}
