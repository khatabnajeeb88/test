<?php
/**
 * Observer to track if Product entered Low Stock state
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Helper\Config;
use Magento\Sales\Model\Order;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;

class CreateLowStockSlaObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var StockItemRepository
     */
    private $stockItemRepository;

    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @var ProductCollection
     */
    private $productCollection;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var StockItemCriteriaInterfaceFactory
     */
    private $stockItemCriteriaInterfaceFactory;

    /**
     * @param Config $config
     * @param StockItemRepository $stockItemRepository
     * @param SlaEventManagement $slaEventManagement
     * @param ProductCollection $productCollection
     * @param ProductRepositoryInterface $productRepository
     * @param StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory
     */
    public function __construct(
        Config $config,
        StockItemRepository $stockItemRepository,
        SlaEventManagement $slaEventManagement,
        ProductCollection $productCollection,
        ProductRepositoryInterface $productRepository,
        StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory
    ) {
        $this->config = $config;
        $this->stockItemRepository = $stockItemRepository;
        $this->slaEventManagement = $slaEventManagement;
        $this->productCollection = $productCollection;
        $this->productRepository = $productRepository;
        $this->stockItemCriteriaInterfaceFactory = $stockItemCriteriaInterfaceFactory;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();

        $orderItems = $order->getItems();

        foreach ($orderItems as $item) {
            $searchCriteria = $this->stockItemCriteriaInterfaceFactory->create();
            $searchCriteria->setProductsFilter($item->getProductId());
            $stockItemsList = $this->stockItemRepository->getList($searchCriteria);
            $stockItems = $stockItemsList->getItems();

            if (count($stockItems) === 0) {
                return;
            }

            foreach ($stockItems as $stockItem) {
                $lowStockDate = $stockItem->getLowStockDate();
                if ($lowStockDate) {
                    $product = $item->getProduct();
                    $merchantItems = $this->productCollection
                        ->create()
                        ->addFieldToFilter('mageproduct_id', $product->getId());

                    foreach ($merchantItems as $merchantItem) {
                        $merchantId = $merchantItem->getSellerId();
                        $this->createUpdateSla((int)$merchantItem->getId(), (int)$merchantId);
                    }
                }
            }
        }
    }

    /**
     * Create/Update SLA and link it with Webkul Product to track further changes
     *
     * @param int $productId
     * @param int $merchantId
     *
     * @return void
     *
     * @throws NoSuchEntityException
     */
    private function createUpdateSla(int $productId, int $merchantId)
    {
        $sla = $this->slaEventManagement->initSla(
            SlaDataInterface::SLA_TYPE_LOW_STOCK,
            $productId
        );

        $sla = $this->slaEventManagement->setSlaData(
            $sla,
            $productId,
            $this->config->getProductLowStockReminderTime(),
            $this->config->getProductLowStockDeadlineTime(),
            SlaDataInterface::SLA_TYPE_LOW_STOCK,
            $merchantId
        );

        $sla->setMerchantNotification(SlaDataInterface::SLA_NOTIFICATION_NOTHING_SENT);
        $this->slaEventManagement->saveSla($sla);
    }
}
