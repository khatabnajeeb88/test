<?php
/**
 * Observer to track if Order got Approved
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Helper\SlaEventManagement;
use Webkul\Marketplace\Model\Orders;

class RemoveNewOrderSlaObserver implements ObserverInterface
{
    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(SlaEventManagement $slaEventManagement)
    {
        $this->slaEventManagement = $slaEventManagement;
    }

    /**
     * Delete SLA associated with the Webkul Order
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Orders $merchantOrder */
        $merchantOrder = $observer->getOrder();
        $this->slaEventManagement->deleteSla(SlaDataInterface::SLA_TYPE_NEW_ORDER, (int)$merchantOrder->getId());
    }
}
