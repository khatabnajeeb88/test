<?php
/**
 * Observer to track if New Order got created and create SLA if so
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Model\SlaDataFactory;
use Arb\Sla\Helper\Config;
use Exception;
use Magento\Sales\Model\Order;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use Arb\Sla\Helper\SlaEventManagement;

class CreateNewOrderSlaObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @param Config $config
     * @param OrdersRepository $ordersRepository
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(
        Config $config,
        OrdersRepository $ordersRepository,
        SlaEventManagement $slaEventManagement
    ) {
        $this->config = $config;
        $this->ordersRepository = $ordersRepository;
        $this->slaEventManagement = $slaEventManagement;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();
        $merchantOrders = $this->ordersRepository->getByOrderId($order->getId());

        foreach ($merchantOrders as $merchantOrder) {
            $this->createSla($merchantOrder);
        }
    }

    /**
     * Create New SLA and link it with Webkul Order to track further changes
     *
     * @param Orders $merchantOrder
     *
     * @return void
     *
     * @throws Exception
     */
    private function createSla(Orders $merchantOrder)
    {
        $sla = $this->slaEventManagement->initSla(
            SlaDataInterface::SLA_TYPE_NEW_ORDER,
            (int)$merchantOrder->getId()
        );

        $sla = $this->slaEventManagement->setSlaData(
            $sla,
            (int)$merchantOrder->getId(),
            $this->config->getNewOrderRequiresActionReminderTime(),
            $this->config->getNewOrderRequiresActionDeadlineTime(),
            SlaDataInterface::SLA_TYPE_NEW_ORDER,
            (int)$merchantOrder->getSellerId()
        );

        $this->slaEventManagement->saveSla($sla);
    }
}
