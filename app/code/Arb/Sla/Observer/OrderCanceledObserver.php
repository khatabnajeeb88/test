<?php
/**
 * Observer to track if Order got canceled and if yes - remove any associated SLA
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Arb\Sla\Helper\SlaEventManagement;
use Webkul\Marketplace\Model\OrdersRepository;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;

class OrderCanceledObserver implements ObserverInterface
{
    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param SlaEventManagement $slaEventManagement
     * @param OrdersRepository $ordersRepository
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        SlaEventManagement $slaEventManagement,
        OrdersRepository $ordersRepository,
        CollectionFactory $collectionFactory
    ) {
        $this->slaEventManagement = $slaEventManagement;
        $this->ordersRepository = $ordersRepository;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();
        $merchantId = $observer->getSellerId();

        if ($merchantId) {
            $merchantOrders = $this->collectionFactory->create()
                ->addFieldToFilter('order_id', $order->getEntityId())
                ->addFieldToFilter('seller_id', $merchantId);
        } else {
            $merchantOrders = $this->ordersRepository->getByOrderId($order->getEntityId());
        }

        foreach ($merchantOrders as $merchantOrder) {
            $this->checkForSla(SlaDataInterface::SLA_TYPE_NEW_ORDER, (int)$merchantOrder->getId());
            $this->checkForSla(SlaDataInterface::SLA_TYPE_ORDER_SHIPPING, (int)$merchantOrder->getId());
        }
    }

    /**
     * Check for SLA attached to this Order and delete them
     *
     * @param string $type
     * @param int $entityId
     *
     * @return void
     */
    private function checkForSla(string $type, int $entityId)
    {
        $sla = $this->slaEventManagement->getSla($type, $entityId);
        !$sla ?: $this->slaEventManagement->deleteSla($type, $entityId);
    }
}
