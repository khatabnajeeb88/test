<?php
/**
 * Observer to track if Product got reviewed by Admin
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Helper\SlaEventManagement;
use Webkul\Marketplace\Model\ResourceModel\Product;

class RemoveProductApprovalSlaObserver implements ObserverInterface
{
    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(SlaEventManagement $slaEventManagement)
    {
        $this->slaEventManagement = $slaEventManagement;
    }

    /**
     * Delete SLA associated with the Webkul Product
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getProduct();
        $this->slaEventManagement->deleteSla(SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL, (int)$product->getId());
    }
}
