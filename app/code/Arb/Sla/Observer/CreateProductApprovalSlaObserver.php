<?php
/**
 * Observer to track Product Approval status
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Webkul\Marketplace\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Helper\Config;
use Exception;
use Webkul\Marketplace\Model\ProductFactory;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Arb\Sla\Helper\SlaEventManagement;

class CreateProductApprovalSlaObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @param Config $config
     * @param ProductFactory $productFactory
     * @param CollectionFactory $collectionFactory
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(
        Config $config,
        ProductFactory $productFactory,
        CollectionFactory $collectionFactory,
        SlaEventManagement $slaEventManagement
    ) {
        $this->config = $config;
        $this->productFactory = $productFactory;
        $this->collectionFactory = $collectionFactory;
        $this->slaEventManagement = $slaEventManagement;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     *
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $mageProductId = $observer->getProductId();
        $merchantId = $observer->getMerchantId();
        $sellerProductId = null;

        $sellerProducts = $this->collectionFactory->create()
            ->addFieldToFilter(
                'mageproduct_id',
                $mageProductId
            )->addFieldToFilter(
                'seller_id',
                $merchantId
            );

        foreach ($sellerProducts as $sellerProduct) {
            $sellerProductId = $sellerProduct->getId();
        }

        if (!$sellerProductId) {
            return;
        }

        $sellerProduct = $this->productFactory->create()->load($sellerProductId);
        $this->createUpdateSla($sellerProduct, (int)$merchantId);
    }

    /**
     * Create New SLA if doesn't exist or Update existing one and link it with Webkul Product to track further changes
     *
     * @param Product $product
     * @param int $merchantId
     *
     * @return void
     *
     * @throws Exception
     */
    private function createUpdateSla(Product $product, int $merchantId)
    {
        $sla = $this->slaEventManagement->initSla(
            SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,
            (int)$product->getId()
        );

        $sla = $this->slaEventManagement->setSlaData(
            $sla,
            (int)$product->getId(),
            $this->config->getProductApprovalReminderTime(),
            $this->config->getProductApprovalDeadlineTime(),
            SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,
            $merchantId
        );

        $sla->setAdminNotification(SlaDataInterface::SLA_NOTIFICATION_NOTHING_SENT);
        $this->slaEventManagement->saveSla($sla);
    }
}
