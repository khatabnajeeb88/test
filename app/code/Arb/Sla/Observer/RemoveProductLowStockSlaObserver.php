<?php
/**
 * Observer to track if Product is above Low Stock threshold
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;

class RemoveProductLowStockSlaObserver implements ObserverInterface
{
    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @var StockItemRepository
     */
    private $stockItemRepository;

    /**
     * @var ProductCollection
     */
    private $productCollection;

    /**
     * @var StockItemCriteriaInterfaceFactory
     */
    private $stockItemCriteriaInterfaceFactory;

    /**
     * @param SlaEventManagement $slaEventManagement
     * @param StockItemRepository $stockItemRepository
     * @param ProductCollection $productCollection
     * @param StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory
     */
    public function __construct(
        SlaEventManagement $slaEventManagement,
        StockItemRepository $stockItemRepository,
        ProductCollection $productCollection,
        StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory
    ) {
        $this->slaEventManagement = $slaEventManagement;
        $this->stockItemRepository = $stockItemRepository;
        $this->productCollection = $productCollection;
        $this->stockItemCriteriaInterfaceFactory = $stockItemCriteriaInterfaceFactory;
    }

    /**
     * Delete SLA associated with the Magento Product
     *
     * @param Observer $observer
     *
     * @return void
     *
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getProduct();
        $productId = (int)$product->getId();

        $merchantItems = $this->productCollection
            ->create()
            ->addFieldToFilter('mageproduct_id', $productId);

        foreach ($merchantItems as $merchantItem) {
            $sla = $this->slaEventManagement
                ->getSla(SlaDataInterface::SLA_TYPE_LOW_STOCK, (int)$merchantItem->getId());

            if ($sla) {
                $this->verifySla($productId, (int)$merchantItem->getId());
            }
        }
    }

    /**
     * Check if Product is still classified as Low Stock, delete SLA if not
     *
     * @param int $productId
     * @param int $relatedEntityId
     *
     * @return void
     */
    private function verifySla(int $productId, int $relatedEntityId)
    {
        $searchCriteria = $this->stockItemCriteriaInterfaceFactory->create();
        $searchCriteria->setProductsFilter($productId);
        $stockItemsList = $this->stockItemRepository->getList($searchCriteria);

        $stockItems = $stockItemsList->getItems();
        if (count($stockItems) === 0) {
            return;
        }

        foreach ($stockItems as $stockItem) {
            $lowStockDate = $stockItem->getLowStockDate();
            if (!$lowStockDate) {
                $this->slaEventManagement->deleteSla(SlaDataInterface::SLA_TYPE_LOW_STOCK, $relatedEntityId);
            }
        }
    }
}
