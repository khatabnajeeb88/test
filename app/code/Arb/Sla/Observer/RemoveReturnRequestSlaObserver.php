<?php
/**
 * Observer to track if RMA got Finalized
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Helper\SlaEventManagement;
use Webkul\MpRmaSystem\Model\Details;

class RemoveReturnRequestSlaObserver implements ObserverInterface
{
    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(SlaEventManagement $slaEventManagement)
    {
        $this->slaEventManagement = $slaEventManagement;
    }

    /**
     * Delete SLA associated with the Webkul RMA
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Details $rma */
        $rma = $observer->getRma();
        $this->slaEventManagement->deleteSla(SlaDataInterface::SLA_TYPE_RETURN_REQUEST, (int)$rma->getId());
    }
}
