<?php
/**
 * Observer to track if Return Request got created/updated
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Observer;

use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Model\SlaDataFactory;
use Arb\Sla\Helper\Config;
use Exception;
use Webkul\MpRmaSystem\Model\Details;
use Webkul\MpRmaSystem\Model\ResourceModel\Details\CollectionFactory;

class CreateReturnRequestSlaObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SlaEventManagement
     */
    private $slaEventManagement;

    /**
     * @param Config $config
     * @param CollectionFactory $collectionFactory
     * @param SlaEventManagement $slaEventManagement
     */
    public function __construct(
        Config $config,
        CollectionFactory $collectionFactory,
        SlaEventManagement $slaEventManagement
    ) {
        $this->config = $config;
        $this->collectionFactory = $collectionFactory;
        $this->slaEventManagement = $slaEventManagement;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     *
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $rma = $observer->getRma();
        $rmaId = $observer->getRmaId();
        $seller = $observer->getSellerId();

        if (!$rma && $rmaId) {
            $rma = $this->collectionFactory->create()->addFieldToFilter('id', $rmaId)->getFirstItem();
        }

        $this->createUpdateSla($rma, (int)$seller);
    }

    /**
     * Create/Update SLA and link it with Webkul Rma to track further changes
     *
     * @param Details $rma
     * @param int $merchantId
     *
     * @return void
     *
     * @throws Exception
     */
    private function createUpdateSla(Details $rma, int $merchantId)
    {
        $sla = $this->slaEventManagement->initSla(
            SlaDataInterface::SLA_TYPE_RETURN_REQUEST,
            (int)$rma->getId()
        );

        $sla = $this->slaEventManagement->setSlaData(
            $sla,
            (int)$rma->getId(),
            $this->config->getOrderReturnRequestReminderTime(),
            $this->config->getOrderReturnRequestDeadlineTime(),
            SlaDataInterface::SLA_TYPE_RETURN_REQUEST,
            $merchantId
        );

        $sla->setMerchantNotification(SlaDataInterface::SLA_NOTIFICATION_NOTHING_SENT);
        $this->slaEventManagement->saveSla($sla);
    }
}
