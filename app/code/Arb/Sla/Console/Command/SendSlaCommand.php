<?php
/**
 * Command used for testing of SLA emails
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Helper\PrepareSla;

class SendSlaCommand extends Command
{
    private const COMMAND = 'arb:sla';
    private const COMMAND_DESCRIPTION = 'Send SLA notifications of specified type';
    private const ARGUMENT_DESCRIPTION = 'Type of SLA to send. Example: product_low_stock';

    /**
     * @var State
     */
    private $state;

    /**
     * @var PrepareSla
     */
    private $prepareSla;

    /**
     * @codeCoverageIgnore
     *
     * @param State $state
     * @param PrepareSla $prepareSla
     */
    public function __construct(
        State $state,
        PrepareSla $prepareSla
    ) {
        parent::__construct();

        $this->state = $state;
        $this->prepareSla = $prepareSla;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Sla_Command_Log-'.date("Ymd").'.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * @codeCoverageIgnore
     *
     * Define command attributes such as description and arguments
     *
     * @return void
     */
    protected function configure()
    {
        $inputArguments = [
            new InputArgument(
                SlaDataInterface::SLA_TYPE,
                InputArgument::REQUIRED,
                self::ARGUMENT_DESCRIPTION
            )
        ];

        $this->setName(self::COMMAND)
            ->setDefinition($inputArguments)
            ->setDescription(self::COMMAND_DESCRIPTION);

        parent::configure();
    }

    /**
     * @codeCoverageIgnore
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     *
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // phpcs:disable Generic.CodeAnalysis.EmptyStatement
        try {
            $this->state->setAreaCode(Area::AREA_FRONTEND);
        } catch (LocalizedException $e) {
            // Intentionally nothing happens here.
            $this->logger->info("Localized exception has occured");
        }
        // phpcs:enable Generic.CodeAnalysis.EmptyStatement

        $type = $input->getArgument(SlaDataInterface::SLA_TYPE);
        $output->writeln('Processing SLA of type: ' . $type);

        $isProperType = $this->checkIfAvailableType($output, $type);

        if ($isProperType) {
            $this->prepareSla->runSlaCheck($type);
            $output->writeln('Operation finished.');
        }
    }

    /**
     * @codeCoverageIgnore
     *
     * Check if provided type exists or provide hint if not
     *
     * @param OutputInterface $output
     * @param string $type
     *
     * @return bool
     */
    private function checkIfAvailableType(OutputInterface $output, string $type)
    {
        $availableTypes = [
            SlaDataInterface::SLA_TYPE_LOW_STOCK,
            SlaDataInterface::SLA_TYPE_RETURN_REQUEST,
            SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,
            SlaDataInterface::SLA_TYPE_NEW_ORDER,
            SlaDataInterface::SLA_TYPE_ORDER_SHIPPING
        ];

        if (!in_array($type, $availableTypes)) {
            $output->writeln('Wrong type provided. Chose one of the following: ');
            foreach ($availableTypes as $type) {
                $output->writeln($type);
            }

            return false;
        }

        return true;
    }
}
