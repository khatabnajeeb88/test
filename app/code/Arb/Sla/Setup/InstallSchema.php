<?php

/**
 * SLA setup files
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;

/**
 * SLA installation class
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        //Create table 'arb_sla_timers'
        if (!$installer->tableExists('arb_sla_timers')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('arb_sla_timers')
            )
                ->addColumn(
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'Entity Id'
                )
                ->addColumn(
                    'type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Type of SLA'
                )
                ->addColumn(
                    'related_entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => true,
                        'identity' => false,
                        'unsigned' => true
                    ],
                    'Related Entity ID - Order/Product/Rma'
                )
                ->addColumn(
                    'merchant_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => true,
                        'identity' => false,
                        'unsigned' => true
                    ],
                    'Customer ID'
                )
                ->addColumn(
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,

                    [
                        'nullable' => true,
                        'identity' => false,
                        'unsigned' => true,
                        'default' => "0"
                    ],
                    'Store ID'
                )
                ->addColumn(
                    'triggered_time',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Creation Time'
                )
                ->addColumn(
                    'reminder_time',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Reminder Time'
                )
                ->addColumn(
                    'deadline_time',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Expiration Time'
                )
                ->addColumn(
                    'merchant_notification',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    [
                        'nullable' => false,
                        'default' => "0"
                    ],
                    'Has SLA been sent to Merchant'
                )
                ->addColumn(
                    'admin_notification',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    [
                        'nullable' => false,
                        'default' => "0"
                    ],
                    'Has SLA been sent to Admin'
                )
                ->addForeignKey(
                    $installer->getFkName('arb_sla_timers', 'store_id', 'store', 'store_id'),
                    'store_id',
                    $installer->getTable('store'),
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_SET_NULL
                )
                ->addForeignKey(
                    $installer->getFkName('arb_sla_timers', 'merchant_id', 'customer_entity', 'entity_id'),
                    'merchant_id',
                    $installer->getTable('customer_entity'),
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->setComment('ARB SLA Entries');
            $installer->getConnection()->createTable($table);
        }

        //Create table 'arb_sla_breached'
        if (!$installer->tableExists('arb_sla_breached')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('arb_sla_breached')
            )
                ->addColumn(
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'Entity Id'
                )
                ->addColumn(
                    'type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Type of SLA'
                )
                ->addColumn(
                    'related_entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => true,
                        'identity' => false,
                        'unsigned' => true
                    ],
                    'Related Entity ID - Order/Product/Rma'
                )
                ->addColumn(
                    'merchant_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => true,
                        'identity' => false,
                        'unsigned' => true
                    ],
                    'Customer ID'
                )
                ->addColumn(
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    [
                        'nullable' => true,
                        'identity' => false,
                        'unsigned' => true,
                        'default' => "0"
                    ],
                    'Store ID'
                )
                ->addColumn(
                    'breach_time',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Breach Time'
                )
                ->addForeignKey(
                    $installer->getFkName('arb_sla_breached', 'store_id', 'store', 'store_id'),
                    'store_id',
                    $installer->getTable('store'),
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_SET_NULL
                )
                ->setComment('ARB SLA Breached');
            $installer->getConnection()->createTable($table);
        }
        //Create table 'arb_sla_report'
        if (!$installer->tableExists('arb_sla_report')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('arb_sla_report')
            )
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'ID'
                )
                ->addColumn(
                    'period',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    [],
                    'Period'
                )
                ->addColumn(
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    [
                        'nullable' => true,
                        'identity' => false,
                        'unsigned' => true
                    ],
                    'Store ID'
                )
                ->addColumn(
                    'sla_type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    40,
                    [
                        'nullable' => false,
                        'identity' => false,
                        'unsigned' => true
                    ],
                    'SLA Type'
                )
                ->addColumn(
                    'merchant_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => false,
                        'identity' => false,
                        'unsigned' => true,
                        'default' => "0"
                    ],
                    'Merchant ID'
                )
                ->addColumn(
                    'merchant',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    40,
                    ['nullable' => false],
                    'Merchant Name'
                )
                ->addColumn(
                    'entity_type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    40,
                    ['nullable' => true],
                    'Related Entity Type'
                )
                ->addColumn(
                    'related_entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    40,
                    ['nullable' => true],
                    'Related Entity ID'
                )
                ->addForeignKey(
                    $installer->getFkName('arb_sla_report', 'store_id', 'store', 'store_id'),
                    'store_id',
                    $installer->getTable('store'),
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->setComment('ARB SLA Report');
            $installer->getConnection()->createTable($table);
           /* $installer->getConnection()->addIndex(
                $installer->getTable('arb_sla_report'),
                $setup->getIdxName(
                    $installer->getTable('arb_sla_report'),
                    ['id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ); */
        }
        $installer->endSetup();
    }
}
