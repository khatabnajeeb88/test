<?php
/**
 * This file consist of PHPUnit test case for SlaReportRepository
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Model;

use Arb\Sla\Model\SlaReport;
use Arb\Sla\Model\SlaReportFactory;
use Arb\Sla\Model\ResourceModel\SlaReportResource;
use Arb\Sla\Model\SlaReportRepository;
use Magento\CatalogRule\Model\ResourceModel\Product\CollectionProcessor;
use Magento\Framework\Api\Search\SearchCriteria;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Arb\Sla\Model\ResourceModel\BreachedSla\CollectionFactory;
use Arb\Sla\Model\ResourceModel\BreachedSla\Collection;
use Magento\Framework\Api\SearchResultsInterface;
use Exception;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\FilterBuilder;

/**
 * @covers \Arb\Sla\Model\SlaReportRepository
 */
class SlaReportRepositoryTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaReportResource
     */
    private $slaReportResourceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionProcessor
     */
    private $collectionProcessorMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterfaceFactory
     */
    private $searchResultsInterfaceFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaReportRepository
     */
    private $testedObject;

    /**
     * Setting up tested object
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaReportResourceMock = $this->createMock(SlaReportResource::class);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->collectionProcessorMock = $this->createMock(CollectionProcessorInterface::class);
        $this->searchResultsInterfaceFactoryMock = $this->createMock(SearchResultsInterfaceFactory::class);

        $this->testedObject = $objectManager->getObject(SlaReportRepository::class, [
            'slaResource' => $this->slaReportResourceMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'collectionProcessor' => $this->collectionProcessorMock,
            'searchResultsInterfaceFactory' => $this->searchResultsInterfaceFactoryMock
        ]);
    }

    /**
     * @return void
     *
     * @throws CouldNotSaveException
     */
    public function testSave()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReport $slaReportMock */
        $slaReportMock = $this->createMock(SlaReport::class);

        $this->slaReportResourceMock
            ->expects($this->once())
            ->method('save')
            ->willReturn($slaReportMock);

        $save = $this->testedObject->save($slaReportMock);
        $this->assertEquals($slaReportMock, $save);
    }

    /**
     * Test save() method for throwing an exception
     *
     * @return void
     *
     * @throws CouldNotSaveException
     */
    public function testSaveExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReport $slaReportMock */
        $slaReportMock = $this->createMock(SlaReport::class);

        $this->slaReportResourceMock
            ->expects($this->once())
            ->method('save')
            ->willThrowException(new Exception('Save error'));

        $this->expectException(CouldNotSaveException::class);

        $this->testedObject->save($slaReportMock);
    }

    /**
     * @return void
     *
     * @throws Exception
     */
    public function testDelete()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReport $slaReportMock */
        $slaReportMock = $this->createMock(SlaReport::class);

        $this->slaReportResourceMock
            ->expects($this->once())
            ->method('delete');

        $this->testedObject->delete($slaReportMock);
    }

    /**
     * @return void
     */
    public function testGetList()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        $this->collectionProcessorMock
            ->expects($this->once())
            ->method('process');

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultMock */
        $searchResultMock = $this->createMock(SearchResultsInterface::class);

        $this->searchResultsInterfaceFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchResultMock);

        $collectionMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn([]);

        $collectionMock
            ->expects($this->once())
            ->method('getSize')
            ->willReturn(1);

        $searchResultMock
            ->expects($this->once())
            ->method('setSearchCriteria')
            ->willReturnSelf();

        $searchResultMock
            ->expects($this->once())
            ->method('setItems')
            ->willReturnSelf();

        $searchResultMock
            ->expects($this->once())
            ->method('setTotalCount')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $getList = $this->testedObject->getList($searchCriteriaMock);
        $this->assertInstanceOf(SearchResultsInterface::class, $getList);
    }
}
