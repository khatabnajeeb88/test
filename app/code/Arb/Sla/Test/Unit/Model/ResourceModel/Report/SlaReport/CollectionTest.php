<?php
/**
 * This file consist of PHPUnit test case for Collection
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Model\ResourceModel\Report\SlaReport;

use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Model\ResourceModel\Report\SlaReport\Collection;
use Magento\Sales\Model\ResourceModel\Report;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\EntityFactory;
use Magento\Framework\DB\Adapter\Pdo\Mysql;

/**
 * @covers \Arb\Sla\Model\ResourceModel\Report\SlaReport\Collection
 */
class SlaReportTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|EntityFactory
     */
    private $entityFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|LoggerInterface
     */
    private $loggerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|FetchStrategyInterface
     */
    private $fetchStrategyMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ManagerInterface
     */
    private $eventManagerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Report
     */
    private $reportMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Collection
     */
    private $testedObject;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $this->entityFactoryMock = $this->createMock(EntityFactory::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);
        $this->fetchStrategyMock = $this->createMock(FetchStrategyInterface::class);
        $this->eventManagerMock = $this->createMock(ManagerInterface::class);

        $this->reportMock = $this->createPartialMock(
            Report::class,
            ['getConnection', 'getMainTable']
        );

        $connection = $this->createPartialMock(
            Mysql::class,
            ['select', 'getDateFormatSql', 'quoteInto']
        );

        $this->reportMock->expects($this->any())
            ->method('getConnection')
            ->will($this->returnValue($connection));

        $this->reportMock->expects($this->any())
            ->method('getMainTable')
            ->will($this->returnValue('test_main_table'));

        $this->testedObject = new Collection(
            $this->entityFactoryMock,
            $this->loggerMock,
            $this->fetchStrategyMock,
            $this->eventManagerMock,
            $this->reportMock
        );
    }

    /**
     * @return void
     */
    public function testAddSlaFilter()
    {
        $filter = $this->testedObject->addSlaFilter([]);
        $this->assertInstanceOf(Collection::class, $filter);
    }

    /**
     * @return void
     */
    public function testAddMerchantIdFilter()
    {
        $filter = $this->testedObject->addMerchantIdFilter([]);
        $this->assertInstanceOf(Collection::class, $filter);
    }
}
