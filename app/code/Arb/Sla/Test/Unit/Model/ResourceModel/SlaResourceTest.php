<?php
/**
 * This file consist of PHPUnit test case for SlaResource
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Model\ResourceModel;

use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Model\ResourceModel\SlaResource;
use Magento\Framework\Exception\LocalizedException;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Sla\Model\ResourceModel\SlaResource
 */
class SlaResourceTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaResource
     */
    private $resourceModel;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->resourceModel = $objectManager->getObject(SlaResource::class);
    }

    /**
     * Test getting table
     *
     * @throws LocalizedException
     *
     * @return void
     */
    public function testGetMainTable()
    {
        $this->assertSame(
            $this->resourceModel->getMainTable(),
            $this->resourceModel->getTable(SlaDataInterface::SLA_TABLE_NAME)
        );
    }
}
