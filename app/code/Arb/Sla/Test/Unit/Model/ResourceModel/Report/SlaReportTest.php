<?php
/**
 * This file consist of PHPUnit test case for SlaReport
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Model\ResourceModel\Report;

use Arb\Sla\Model\SlaData;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Sales\Api\Data\OrderInterface;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Model\ResourceModel\Report\SlaReport;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\Data\SlaReportInterface;
use Arb\Sla\Model\Flag;
use Arb\Sla\Api\SlaReportRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Reports\Model\FlagFactory;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository as WebkulOrderRepository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Api\SearchResultsInterface;

/**
 * @covers \Arb\Sla\Model\ResourceModel\Report\SlaReport
 */
class SlaReportTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ResourceConnection
     */
    private $resourceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|TimezoneInterface
     */
    private $timezoneMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaReportRepositoryInterface
     */
    private $slaReportRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|WebkulOrderRepository
     */
    private $webkulOrderRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderRepositoryInterface
     */
    private $orderRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CustomerRepositoryInterface
     */
    private $customerRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaData
     */
    private $testedObject;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $connectionMock = $this->getMockBuilder(AdapterInterface::class)->getMock();
        $this->resourceMock = $this->getMockBuilder(ResourceConnection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resourceMock->expects($this->any())->method('getConnection')->willReturn($connectionMock);
        $this->resourceMock->expects($this->any())->method('getTableName')->will(
            $this->returnCallback(
                function ($arg) {
                    return $arg;
                }
            )
        );

        $contextMock = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();

        $contextMock
            ->expects($this->once())
            ->method('getResources')
            ->willReturn($this->resourceMock);

        $this->timezoneMock = $this->createMock(TimezoneInterface::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->slaReportRepositoryMock = $this->createMock(SlaReportRepositoryInterface::class);
        $this->webkulOrderRepositoryMock = $this->createMock(WebkulOrderRepository::class);
        $this->orderRepositoryMock = $this->createMock(OrderRepositoryInterface::class);
        $this->customerRepositoryMock = $this->createMock(CustomerRepositoryInterface::class);

        $flagMock = $this->getMockBuilder(Flag::class)
            ->disableOriginalConstructor()
            ->setMethods(['setReportFlagCode', 'unsetData', 'loadSelf', 'setFlagData', 'setLastUpdate', 'save'])
            ->getMock();

        $flagFactoryMock = $this->getMockBuilder(FlagFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $flagFactoryMock->expects($this->any())->method('create')->willReturn($flagMock);

        $flagMock->method('setReportFlagCode')->willReturnSelf();
        $flagMock->method('unsetData')->willReturnSelf();

        $this->testedObject = $objectManager->getObject(SlaReport::class, [
            'context' => $contextMock,
            'reportsFlagFactory' => $flagFactoryMock,
            'resource' => $this->resourceMock,
            'timezone' => $this->timezoneMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'slaReportRepository' => $this->slaReportRepositoryMock,
            'webkulOrderRepository' => $this->webkulOrderRepositoryMock,
            'orderRepository' => $this->orderRepositoryMock,
            'customerRepository' => $this->customerRepositoryMock
        ]);
    }

    /**
     * @return array
     */
    public function aggregateProvider()
    {
        return [
            [
                date('Y-m-d H:i:s')     // 'from' limiter
            ],
            [
                null                          // 'from' limiter
            ]
        ];
    }

    /**
     * @dataProvider aggregateProvider
     *
     * @param string|null $from
     *
     * @return void
     */
    public function testAggregate(?string $from)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaInterface $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        $this->resourceMock
            ->method('getTableName')
            ->willReturn('Table name');

        /** @var PHPUnit_Framework_MockObject_MockObject|AdapterInterface $connectionMock */
        $connectionMock = $this->createMock(AdapterInterface::class);

        $this->resourceMock
            ->method('getConnection')
            ->willReturn($connectionMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultsMock */
        $searchResultsMock = $this->createMock(SearchResultsInterface::class);

        $this->slaReportRepositoryMock
            ->expects($this->once())
            ->method('getList')
            ->willReturn($searchResultsMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReportInterface $slaReportInterface1Mock */
        $slaReportInterface1Mock = $this->createMock(SlaReportInterface::class);

        $slaReportInterface1Mock
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_NEW_ORDER);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReportInterface $slaReportInterfaceException1Mock */
        $slaReportInterfaceException1Mock = $this->createMock(SlaReportInterface::class);

        $slaReportInterfaceException1Mock
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_NEW_ORDER);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReportInterface $slaReportInterfaceException2Mock */
        $slaReportInterfaceException2Mock = $this->createMock(SlaReportInterface::class);

        $slaReportInterfaceException2Mock
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_NEW_ORDER);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReportInterface $slaReportInterface2Mock */
        $slaReportInterface2Mock = $this->createMock(SlaReportInterface::class);

        $slaReportInterface2Mock
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_RETURN_REQUEST);

        $slaReportList = [
            $slaReportInterface1Mock,
            $slaReportInterface2Mock
        ];

        $searchResultsMock
            ->method('getItems')
            ->willReturn($slaReportList);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $webkulOrderMock */
        $webkulOrderMock = $this->createMock(Orders::class);

        $this->webkulOrderRepositoryMock
            ->method('getById')
            ->willReturn($webkulOrderMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderInterface $orderMock */
        $orderMock = $this->createMock(OrderInterface::class);

        $this->orderRepositoryMock
            ->method('get')
            ->willReturn($orderMock);

        $orderMock
            ->method('getIncrementId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|CustomerInterface $customerMock */
        $customerMock = $this->createMock(CustomerInterface::class);

        $this->customerRepositoryMock
            ->method('getById')
            ->willReturn($customerMock);

        $this->testedObject->aggregate($from);
    }
}
