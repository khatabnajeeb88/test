<?php
/**
 * This file consist of PHPUnit test case for SlaRepository
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Model;

use Arb\Sla\Model\SlaData;
use Arb\Sla\Model\SlaDataFactory;
use Arb\Sla\Model\ResourceModel\SlaResource;
use Arb\Sla\Model\SlaRepository;
use Magento\CatalogRule\Model\ResourceModel\Product\CollectionProcessor;
use Magento\Framework\Api\Search\SearchCriteria;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Arb\Sla\Model\ResourceModel\Sla\CollectionFactory;
use Arb\Sla\Model\ResourceModel\Sla\Collection;
use Magento\Framework\Api\SearchResultsInterface;
use Exception;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\FilterBuilder;
use ReflectionException;
use Magento\Framework\Api\Filter;

/**
 * @covers \Arb\Sla\Model\SlaRepository
 */
class SlaRepositoryTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaResource
     */
    private $slaResourceMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionProcessor
     */
    private $collectionProcessorMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterfaceFactory
     */
    private $searchResultsInterfaceFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|FilterBuilder
     */
    private $filterBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|FilterGroupBuilder
     */
    private $filterGroupBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaRepository
     */
    private $testedObject;

    /**
     * Setting up tested object
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaResourceMock = $this->createMock(SlaResource::class);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->collectionProcessorMock = $this->createMock(CollectionProcessorInterface::class);
        $this->searchResultsInterfaceFactoryMock = $this->createMock(SearchResultsInterfaceFactory::class);
        $this->filterBuilderMock = $this->createMock(FilterBuilder::class);
        $this->filterGroupBuilderMock = $this->createMock(FilterGroupBuilder::class);

        $this->testedObject = $objectManager->getObject(SlaRepository::class, [
            'slaResource' => $this->slaResourceMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'collectionProcessor' => $this->collectionProcessorMock,
            'searchResultsInterfaceFactory' => $this->searchResultsInterfaceFactoryMock,
            'filterBuilder' => $this->filterBuilderMock,
            'filterGroupBuilder' => $this->filterGroupBuilderMock
        ]);
    }

    /**
     * @return array
     */
    public function saveProvider()
    {
        return [
            [
                SlaDataInterface::SLA_TYPE_NEW_ORDER,           // sla type
                0                                               // merchant notification call count
            ],
            [
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                1                                               // merchant notification call count
            ]
        ];
    }

    /**
     * @dataProvider saveProvider
     *
     * @param string $type
     * @param int $callCount
     *
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws ReflectionException
     */
    public function testSave(string $type, int $callCount)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $slaMock
            ->expects($this->once())
            ->method('getType')
            ->willReturn($type);

        $slaMock
            ->expects($this->exactly($callCount))
            ->method('setMerchantNotification')
            ->willReturnSelf();

        $this->slaResourceMock
            ->expects($this->once())
            ->method('save')
            ->willReturn($slaMock);

        $save = $this->testedObject->save($slaMock);
        $this->assertEquals($slaMock, $save);
    }

    /**
     * Test save() method for throwing an exception
     *
     * @return void
     *
     * @throws CouldNotSaveException
     * @throws ReflectionException
     */
    public function testSaveExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $slaMock
            ->expects($this->once())
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_NEW_ORDER);

        $this->slaResourceMock
            ->expects($this->once())
            ->method('save')
            ->willThrowException(new Exception('Save error'));

        $this->expectException(CouldNotSaveException::class);

        $this->testedObject->save($slaMock);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testDelete()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $this->slaResourceMock
            ->expects($this->once())
            ->method('delete');

        $this->testedObject->delete($slaMock);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetList()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        $this->collectionProcessorMock
            ->expects($this->once())
            ->method('process');

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultMock */
        $searchResultMock = $this->createMock(SearchResultsInterface::class);

        $this->searchResultsInterfaceFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchResultMock);

        $collectionMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn([]);

        $collectionMock
            ->expects($this->once())
            ->method('getSize')
            ->willReturn(1);

        $searchResultMock
            ->expects($this->once())
            ->method('setSearchCriteria')
            ->willReturnSelf();

        $searchResultMock
            ->expects($this->once())
            ->method('setItems')
            ->willReturnSelf();

        $searchResultMock
            ->expects($this->once())
            ->method('setTotalCount')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $getList = $this->testedObject->getList($searchCriteriaMock);
        $this->assertInstanceOf(SearchResultsInterface::class, $getList);
    }

    public function getAllExpiredSlaProvider()
    {
        return [
            [
                1,      // store id
                2       // call count for store related filter
            ],
            [
                0,      // store id
                2       // call count for store related filter
            ],
            [
                null,   // store id
                1       // call count for store related filter
            ]
        ];
    }

    /**
     * @dataProvider getAllExpiredSlaProvider
     *
     * @param int|null $storeId
     * @param int $callCount
     *
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetAllExpiredSla(?int $storeId, int $callCount)
    {
        $this->filterBuilderMock
            ->expects($this->exactly(2))
            ->method('setField')
            ->willReturnSelf();

        $this->filterBuilderMock
            ->expects($this->exactly(2))
            ->method('setValue')
            ->willReturnSelf();

        $this->filterBuilderMock
            ->expects($this->exactly(2))
            ->method('setConditionType')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|Filter $filterMock */
        $filterMock = $this->createMock(Filter::class);

        $this->filterBuilderMock
            ->expects($this->exactly(2))
            ->method('create')
            ->willReturn($filterMock);

        $this->filterGroupBuilderMock
            ->expects($this->exactly(2))
            ->method('addFilter')
            ->willReturnSelf();

        $this->filterGroupBuilderMock
            ->expects($this->once())
            ->method('create');

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('setFilterGroups')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->exactly($callCount))
            ->method('addFilter')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteria $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultMock */
        $searchResultMock = $this->createMock(SearchResultsInterface::class);

        $this->searchResultsInterfaceFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchResultMock);

        $collectionMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn([]);

        $collectionMock
            ->expects($this->once())
            ->method('getSize')
            ->willReturn(1);

        $getAllExpiredSla = $this->testedObject->getAllExpiredSla(SlaDataInterface::SLA_TYPE_NEW_ORDER, $storeId);
        $this->assertInstanceOf(SearchResultsInterface::class, $getAllExpiredSla);
    }
}
