<?php
/**
 * This file consist of PHPUnit test case for SlaData
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Model;

use Arb\Sla\Model\SlaData;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Arb\Sla\Model\ResourceModel\SlaResource;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Sla\Model\SlaData
 */
class SlaDataTest extends TestCase
{
    private const TYPE = 'some type';
    private const MERCHANT_ID = 5;
    private const RELATED_ENTITY_ID = 10;
    private const STORE_ID = 3;
    private const TRIGGER_TIME = '2020-07-04 14:34:35';
    private const DEADLINE_TIME = '2020-07-08 17:23:15';
    private const REMINDER_TIME = '2020-07-06 11:14:31';
    private const MERCHANT_NOTIFICATION = 1;
    private const ADMIN_NOTIFICATION = 0;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaData
     */
    private $testedObject;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $resource = $this->createMock(SlaResource::class);
        $resource->method('save')->willReturnSelf();

        $this->testedObject = $objectManager->getObject(SlaData::class, [
            '_resource' => $resource
        ]);
    }

    /**
     * Test method getType and setType
     *
     * @return void
     */
    public function testGetSetType()
    {
        $this->assertEquals(
            $this->testedObject->setType(self::TYPE)->getType(),
            self::TYPE
        );
    }

    /**
     * Test method getRelatedEntityId and setRelatedEntityId
     *
     * @return void
     */
    public function testGetSetRelatedEntityId()
    {
        $this->assertEquals(
            $this->testedObject->setRelatedEntityId(self::RELATED_ENTITY_ID)->getRelatedEntityId(),
            self::RELATED_ENTITY_ID
        );
    }

    /**
     * Test method getMerchantId and setMerchantId
     *
     * @return void
     */
    public function testGetSetMerchantId()
    {
        $this->assertEquals(
            $this->testedObject->setMerchantId(self::MERCHANT_ID)->getMerchantId(),
            self::MERCHANT_ID
        );
    }

    /**
     * Test method getStoreId and setStoreId
     *
     * @return void
     */
    public function testGetSetStoreId()
    {
        $this->assertEquals(
            $this->testedObject->setStoreId(self::STORE_ID)->getStoreId(),
            self::STORE_ID
        );
    }

    /**
     * Test method getTriggerTime and setTriggerTime
     *
     * @return void
     */
    public function testGetSetTriggerTime()
    {
        $this->assertEquals(
            $this->testedObject->setTriggerTime(self::TRIGGER_TIME)->getTriggerTime(),
            self::TRIGGER_TIME
        );
    }

    /**
     * Test method getReminderTime and setReminderTime
     *
     * @return void
     */
    public function testGetSetReminderTime()
    {
        $this->assertEquals(
            $this->testedObject->setReminderTime(self::REMINDER_TIME)->getReminderTime(),
            self::REMINDER_TIME
        );
    }

    /**
     * Test method getDeadlineTime and setDeadlineTime
     *
     * @return void
     */
    public function testGetSetDeadlineTime()
    {
        $this->assertEquals(
            $this->testedObject->setDeadlineTime(self::DEADLINE_TIME)->getDeadlineTime(),
            self::DEADLINE_TIME
        );
    }

    /**
     * Test method getMerchantNotification and setMerchantNotification
     *
     * @return void
     */
    public function testGetSetMerchantNotification()
    {
        $this->assertEquals(
            $this->testedObject->setMerchantNotification(self::MERCHANT_NOTIFICATION)->getMerchantNotification(),
            self::MERCHANT_NOTIFICATION
        );
    }

    /**
     * Test method getAdminNotification and setAdminNotification
     *
     * @return void
     */
    public function testGetSetAdminNotification()
    {
        $this->assertEquals(
            $this->testedObject->setAdminNotification(self::ADMIN_NOTIFICATION)->getAdminNotification(),
            self::ADMIN_NOTIFICATION
        );
    }
}
