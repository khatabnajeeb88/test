<?php
/**
 * This file consist of PHPUnit test case for SlaReport
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Model;

use Arb\Sla\Model\SlaReport;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Arb\Sla\Model\ResourceModel\SlaReportResource;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\Sla\Model\SlaReport
 */
class SlaReportTest extends TestCase
{
    const TYPE = 'some type';
    const MERCHANT_ID = 5;
    const MERCHANT = 'merchant';
    const RELATED_ENTITY_ID = 10;
    const STORE_ID = 3;
    const BREACH_TIME = '2020-07-08 17:23:15';

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaReport
     */
    private $testedObject;

    /**
     * Setting up tested object and creating needed mocks
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $resource = $this->createMock(SlaReportResource::class);
        $resource->method('save')->willReturnSelf();

        $this->testedObject = $objectManager->getObject(SlaReport::class, [
            '_resource' => $resource
        ]);
    }

    /**
     * Test method getType and setType
     *
     * @return void
     */
    public function testGetSetType()
    {
        $this->assertEquals(
            $this->testedObject->setType(self::TYPE)->getType(),
            self::TYPE
        );
    }

    /**
     * Test method getRelatedEntityId and setRelatedEntityId
     *
     * @return void
     */
    public function testGetSetRelatedEntityId()
    {
        $this->assertEquals(
            $this->testedObject->setRelatedEntityId(self::RELATED_ENTITY_ID)->getRelatedEntityId(),
            self::RELATED_ENTITY_ID
        );
    }

    /**
     * Test method getMerchantId and setMerchantId
     *
     * @return void
     */
    public function testGetSetMerchantId()
    {
        $this->assertEquals(
            $this->testedObject->setMerchantId(self::MERCHANT_ID)->getMerchantId(),
            self::MERCHANT_ID
        );
    }

    /**
     * Test method getMerchant and setMerchant
     *
     * @return void
     */
    public function testGetSetMerchant()
    {
        $this->assertEquals(
            $this->testedObject->setMerchant(self::MERCHANT)->getMerchant(),
            self::MERCHANT
        );
    }

    /**
     * Test method getStoreId and setStoreId
     *
     * @return void
     */
    public function testGetSetStoreId()
    {
        $this->assertEquals(
            $this->testedObject->setStoreId(self::STORE_ID)->getStoreId(),
            self::STORE_ID
        );
    }

    /**
     * Test method getBreachTime and setBreachTime
     *
     * @return void
     */
    public function testGetSetBreachTime()
    {
        $this->assertEquals(
            $this->testedObject->setBreachTime(self::BREACH_TIME)->getBreachTime(),
            self::BREACH_TIME
        );
    }
}
