<?php
/**
 * This file consist of PHPUnit test case for Helper class Email
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Helper;

use Arb\Sla\Helper\SlaEmail;
use Arb\Sla\Model\SlaReport;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\SlaRepositoryInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;
use Arb\Sla\Logger\Logger as SlaLogger;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use ReflectionException;
use Arb\Sla\Api\SlaReportRepositoryInterface;
use Arb\Sla\Model\SlaReportFactory;
use Arb\Sla\Helper\Config;
use Exception;

/**
 * @covers \Arb\Sla\Helper\SlaEmail
 */
class SlaEmailTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaRepositoryInterface
     */
    private $slaRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|TransportBuilder
     */
    private $transportBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StateInterface
     */
    private $inlineTranslationMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CustomerRepositoryInterface
     */
    private $customerRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CustomerFactory
     */
    private $customerFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaLogger
     */
    private $loggerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|TimezoneInterface
     */
    private $timezoneMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaReportFactory
     */
    private $slaReportFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaReportRepositoryInterface
     */
    private $slaReportRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StoreManagerInterface
     */
    private $storeManagerMock;

    /**
     * Object to test
     *
     * @var SlaEmail
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     *
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaRepositoryMock = $this->createMock(SlaRepositoryInterface::class);
        $this->transportBuilderMock = $this->createMock(TransportBuilder::class);
        $this->inlineTranslationMock = $this->createMock(StateInterface::class);
        $this->customerRepositoryMock = $this->createMock(CustomerRepositoryInterface::class);
        $this->customerFactoryMock = $this->createMock(CustomerFactory::class);
        $this->loggerMock = $this->createMock(SlaLogger::class);
        $this->timezoneMock = $this->createMock(TimezoneInterface::class);
        $this->slaReportFactoryMock = $this->createMock(SlaReportFactory::class);
        $this->slaReportRepositoryMock = $this->createMock(SlaReportRepositoryInterface::class);
        $this->configMock = $this->createMock(Config::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->storeMock= $this->createMock(\Magento\Store\Api\Data\StoreInterface::class);
        $this->storeManagerMock->expects($this->any())->method("getStore")->willReturn( $this->storeMock);
        $this->testObject = $objectManager->getObject(SlaEmail::class, [
            'slaRepository' => $this->slaRepositoryMock,
            'transportBuilder' => $this->transportBuilderMock,
            'inlineTranslation' => $this->inlineTranslationMock,
            'customerRepository' => $this->customerRepositoryMock,
            'customerFactory' => $this->customerFactoryMock,
            'logger' => $this->loggerMock,
            'timezone' => $this->timezoneMock,
            'slaReportFactory' => $this->slaReportFactoryMock,
            'slaReportRepository' => $this->slaReportRepositoryMock,
            'configMock' => $this->configMock,
            'storeManager' => $this->storeManagerMock
        ]);
    }

    /**
     * @return array
     */
    public function sendEmailProvider()
    {
        return [
            [   // Scenario 1: SLA Breach for Admin only that was not sent yet
                $details = [
                    'sender' => [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'store_id' => 0
                    ],
                    'template_vars' => [
                        'sla_date' => date('Y-m-d H:i:s'),
                        'sla_breach_date' => date('Y-m-d H:i:s'),
                        'sla_cause' => 'some_cause',
                        'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_ADMIN
                    ],
                    'breach_type' => SlaDataInterface::BREACH_TYPE_DEADLINE
                ],
                0,                                              // will send to Merchant
                1,                                              // will send to Admin
                3,                                              // merchant notification flag
                0,                                              // admin notification flag
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                0                                               // will format displayed time
            ],
            [   // Scenario 2: SLA Breach for Admin only that was sent already
                $details = [
                    'sender' => [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'store_id' => 0
                    ],
                    'template_vars' => [
                        'sla_date' => date('Y-m-d H:i:s'),
                        'sla_breach_date' => date('Y-m-d H:i:s'),
                        'sla_cause' => 'some_cause',
                        'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_ADMIN
                    ],
                    'breach_type' => SlaDataInterface::BREACH_TYPE_DEADLINE
                ],
                0,                                              // will send to Merchant
                0,                                              // will send to Admin
                3,                                              // merchant notification flag
                2,                                              // admin notification flag
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                0                                               // will format displayed time
            ],
            [   // Scenario 3: SLA Reminder for Admin only that was not sent yet
                $details = [
                    'sender' => [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'store_id' => 0
                    ],
                    'template_vars' => [
                        'sla_date' => date('Y-m-d H:i:s'),
                        'sla_breach_date' => date('Y-m-d H:i:s'),
                        'sla_cause' => 'some_cause',
                        'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_ADMIN
                    ],
                    'breach_type' => SlaDataInterface::BREACH_TYPE_REMINDER
                ],
                0,                                              // will send to Merchant
                1,                                              // will send to Admin
                3,                                              // merchant notification flag
                1,                                              // admin notification flag
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                0                                               // will format displayed time
            ],
            [   // Scenario 3: SLA Reminder for Merchant that was not sent yet
                $details = [
                    'sender' => [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'store_id' => 1
                    ],
                    'template_vars' => [
                        'sla_date' => date('Y-m-d H:i:s'),
                        'sla_breach_date' => date('Y-m-d H:i:s'),
                        'sla_cause' => 'some_cause',
                        'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_MERCHANT
                    ],
                    'breach_type' => SlaDataInterface::BREACH_TYPE_REMINDER
                ],
                1,                                              // will send to Merchant
                0,                                              // will send to Admin
                0,                                              // merchant notification flag
                0,                                              // admin notification flag
                SlaDataInterface::SLA_TYPE_RETURN_REQUEST,      // sla type
                1                                               // will format displayed time
            ],
            [   // Scenario 4: SLA Breach for Merchant that was not sent yet
                $details = [
                    'sender' => [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'store_id' => 1
                    ],
                    'template_vars' => [
                        'sla_date' => date('Y-m-d H:i:s'),
                        'sla_breach_date' => date('Y-m-d H:i:s'),
                        'sla_cause' => 'some_cause',
                        'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_MERCHANT
                    ],
                    'breach_type' => SlaDataInterface::BREACH_TYPE_DEADLINE
                ],
                1,                                              // will send to Merchant
                1,                                              // will send to Admin
                1,                                              // merchant notification flag
                0,                                              // admin notification flag
                SlaDataInterface::SLA_TYPE_RETURN_REQUEST,      // sla type
                1                                               // will format displayed time
            ],
            [   // Scenario 5: SLA Breach for Merchant that was sent to Merchant but not Admin
                $details = [
                    'sender' => [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'store_id' => 1
                    ],
                    'template_vars' => [
                        'sla_date' => date('Y-m-d H:i:s'),
                        'sla_breach_date' => date('Y-m-d H:i:s'),
                        'sla_cause' => 'some_cause',
                        'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_MERCHANT
                    ],
                    'breach_type' => SlaDataInterface::BREACH_TYPE_DEADLINE
                ],
                0,                                              // will send to Merchant
                1,                                              // will send to Admin
                2,                                              // merchant notification flag
                0,                                              // admin notification flag
                SlaDataInterface::SLA_TYPE_RETURN_REQUEST,      // sla type
                0                                               // will format displayed time
            ],
            [   // Scenario 6: SLA Breach for Merchant that was already sent to Admin
                $details = [
                    'sender' => [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'store_id' => 1
                    ],
                    'template_vars' => [
                        'sla_date' => date('Y-m-d H:i:s'),
                        'sla_breach_date' => date('Y-m-d H:i:s'),
                        'sla_cause' => 'some_cause',
                        'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_MERCHANT
                    ],
                    'breach_type' => SlaDataInterface::BREACH_TYPE_DEADLINE
                ],
                1,                                              // will send to Merchant
                0,                                              // will send to Admin
                1,                                              // merchant notification flag
                2,                                              // admin notification flag
                SlaDataInterface::SLA_TYPE_RETURN_REQUEST,      // sla type
                1                                               // will format displayed time
            ]
        ];
    }

    /**
     * @dataProvider sendEmailProvider
     *
     * @param array $details
     * @param $merchantEmail
     * @param $adminEmail
     * @param int $merchantNotificationFlag
     * @param int $adminNotificationFlag
     * @param string $slaType
     * @param int $formatTime
     *
     * @return void
     *
     * @throws LocalizedException
     * @throws ReflectionException
     */
    public function testSendEmail(
        array $details,
        $merchantEmail,
        $adminEmail,
        int $merchantNotificationFlag,
        int $adminNotificationFlag,
        string $slaType,
        int $formatTime
    ) {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        /** @var PHPUnit_Framework_MockObject_MockObject|Customer $customerMock */
        $customerMock = $this->createMock(Customer::class);

        $this->customerFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($customerMock);

        $customerMock
            ->expects($this->once())
            ->method('load')
            ->willReturnSelf();

        $customerMock
            ->expects($this->any())
            ->method('getName')
            ->willReturn('John Doe');

        $slaMock
            ->expects($this->any())
            ->method('getMerchantId')
            ->willReturn(1);

        $slaMock
            ->expects($this->any())
            ->method('getDeadlineTime')
            ->willReturn('some time');

        $slaMock
            ->expects($this->any())
            ->method('getStoreId')
            ->willReturn(1);

        $slaMock
            ->expects($this->any())
            ->method('getRelatedEntityId')
            ->willReturn(1);

        $slaMock
            ->expects($this->any())
            ->method('getMerchantNotification')
            ->willReturn($merchantNotificationFlag);

        $slaMock
            ->expects($this->any())
            ->method('getAdminNotification')
            ->willReturn($adminNotificationFlag);

        $this->inlineTranslationMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('suspend')
            ->willReturnSelf();

        $storeMock = $this->createMock(StoreInterface::class);
        $this->storeManagerMock->method('getStore')->willReturn($storeMock);

        $this->transportBuilderMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('setTemplateOptions')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('setTemplateVars')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('setFromByScope')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('setReplyTo')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('addTo')
            ->willReturnSelf();

        $this->configMock
            ->method('getAdminReceiverEmail')
            ->willReturn('some@email.com');

        $this->transportBuilderMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('setTemplateIdentifier')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|TransportInterface $transportMock */
        $transportMock = $this->createMock(TransportInterface::class);

        $this->transportBuilderMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('getTransport')
            ->willReturn($transportMock);

        $slaMock
            ->expects($this->any())
            ->method('getType')
            ->willReturn($slaType);

        $this->loggerMock
            ->expects($this->any())
            ->method('info');

        $this->slaRepositoryMock
            ->expects($this->any())
            ->method('delete');

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaReport $slaReportMock */
        $slaReportMock = $this->createMock(SlaReport::class);

        $this->slaReportFactoryMock
            ->expects($this->any())
            ->method('create')
            ->willReturn($slaReportMock);

        $this->slaRepositoryMock
            ->expects($this->any())
            ->method('save');

        $this->timezoneMock
            ->expects($this->exactly($formatTime * 2))
            ->method('formatDateTime')
            ->willReturn('some date');

        $this->inlineTranslationMock
            ->expects($this->exactly($merchantEmail + $adminEmail))
            ->method('resume')
            ->willReturnSelf();

        $this->testObject->sendEmail($slaMock, $details);
    }

    /**
     * @throws LocalizedException
     */
    public function testSendEmailToAdminExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        /** @var PHPUnit_Framework_MockObject_MockObject|Customer $customerMock */
        $customerMock = $this->createMock(Customer::class);

        $this->customerFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($customerMock);

        $this->transportBuilderMock
            ->method('setTemplateOptions')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('setTemplateVars')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('setFromByScope')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('setReplyTo')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('addTo')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|TransportInterface $transportMock */
        $transportMock = $this->createMock(TransportInterface::class);

        $this->transportBuilderMock
            ->method('getTransport')
            ->willReturn($transportMock);

        $slaMock
            ->expects($this->any())
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_RETURN_REQUEST);

        $transportMock
            ->method('sendMessage')
            ->willThrowException(new Exception(__('Error')));

        $details = [
            'sender' => [
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'store_id' => 1
            ],
            'template_vars' => [
                'sla_date' => date('Y-m-d H:i:s'),
                'sla_breach_date' => date('Y-m-d H:i:s'),
                'sla_cause' => 'some_cause',
                'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_ADMIN
            ],
            'breach_type' => SlaDataInterface::BREACH_TYPE_DEADLINE
        ];

        $this->testObject->sendEmail($slaMock, $details);
    }

    /**
     * @throws LocalizedException
     */
    public function testSendEmailToMerchantExceptionHandling()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        /** @var PHPUnit_Framework_MockObject_MockObject|Customer $customerMock */
        $customerMock = $this->createMock(Customer::class);

        $this->customerFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($customerMock);

        $customerMock
            ->expects($this->once())
            ->method('load')
            ->willReturnSelf();

        $customerMock
            ->expects($this->any())
            ->method('getName')
            ->willReturn('John Doe');

        $this->transportBuilderMock
            ->method('setTemplateOptions')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('setTemplateVars')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('setFromByScope')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('setReplyTo')
            ->willReturnSelf();

        $this->transportBuilderMock
            ->method('addTo')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|TransportInterface $transportMock */
        $transportMock = $this->createMock(TransportInterface::class);

        $this->transportBuilderMock
            ->method('getTransport')
            ->willReturn($transportMock);

        $slaMock
            ->expects($this->any())
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_RETURN_REQUEST);

        $transportMock
            ->method('sendMessage')
            ->willThrowException(new Exception(__('Error')));

        $details = [
            'sender' => [
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'store_id' => 1
            ],
            'template_vars' => [
                'sla_date' => date('Y-m-d H:i:s'),
                'sla_breach_date' => date('Y-m-d H:i:s'),
                'sla_cause' => 'some_cause',
                'responsible_person' => SlaDataInterface::RESPONSIBLE_PERSON_MERCHANT
            ],
            'breach_type' => SlaDataInterface::BREACH_TYPE_REMINDER
        ];

        $this->testObject->sendEmail($slaMock, $details);
    }
}
