<?php
/**
 * This file consist of PHPUnit test case for Helper class SlaUrlBuilder
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Helper;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaUrlBuilder;
use Magento\Framework\UrlInterface;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository;
use Webkul\Marketplace\Model\ResourceModel\Product as MerchantProduct;
use Webkul\Marketplace\Model\ResourceModel\Product\Collection;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Exception;

/**
 * @covers \Arb\Sla\Helper\SlaUrlBuilder
 */
class SlaUrlBuilderTest extends TestCase
{
    private const URL_PATH_ORDER = 'marketplace/order/view/id/';
    private const URL_PATH_RMA = 'mprmasystem/seller/rma/id/';
    private const URL_PATH_PRODUCT = 'marketplace/product/edit/id/';

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|UrlInterface
     */
    private $urlBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersRepository
     */
    private $ordersRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ProductCollection
     */
    private $productCollectionMock;

    /**
     * Object to test
     *
     * @var SlaUrlBuilder
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->urlBuilderMock = $this->createMock(UrlInterface::class);
        $this->ordersRepositoryMock = $this->createMock(OrdersRepository::class);
        $this->productCollectionMock = $this->createMock(ProductCollection::class);

        $this->testObject = $objectManager->getObject(SlaUrlBuilder::class, [
            '_urlBuilder' => $this->urlBuilderMock,
            'ordersRepository' => $this->ordersRepositoryMock,
            'productCollection' => $this->productCollectionMock
        ]);
    }

    /**
     * @return array
     */
    public function getUrlProvider()
    {
        return [
            [   // Scenario 1: New Order SLA
                'www.base-url.com',                            // base url
                SlaDataInterface::SLA_TYPE_NEW_ORDER,          // type of SLA
                self::URL_PATH_ORDER,                          // url path to take action for sla
                1                                              // related entity id
            ],
            [   // Scenario 2: Shipping Required SLA
                'www.base-url.com',                            // base url
                SlaDataInterface::SLA_TYPE_ORDER_SHIPPING,     // type of SLA
                self::URL_PATH_ORDER,                          // url path to take action for sla
                1                                              // related entity id
            ],
            [   // Scenario 3: RMA Request SLA
                'www.base-url.com',                            // base url
                SlaDataInterface::SLA_TYPE_RETURN_REQUEST,     // type of SLA
                self::URL_PATH_RMA,                            // url path to take action for sla
                1                                              // related entity id
            ],
            [   // Scenario 4: Low Product Stock SLA
                'www.base-url.com',                            // base url
                SlaDataInterface::SLA_TYPE_LOW_STOCK,          // type of SLA
                self::URL_PATH_PRODUCT,                        // url path to take action for sla
                1                                              // related entity id
            ],
            [   // Scenario 5: Product Approval SLA
                'www.base-url.com',                           // base url
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,  // type of SLA
                '',                                           // url path to take action for sla
                null                                          // related entity id
            ],
            [   // Scenario 6: Any other type
                'www.base-url.com',                           // base url
                '',                                           // type of SLA
                '',                                           // url path to take action for sla
                null                                          // related entity id
            ]
        ];
    }

    /**
     * @dataProvider getUrlProvider
     *
     * @param string $baseUrl
     * @param string $type
     * @param string $path
     * @param int|null $entity
     *
     * @return void
     */
    public function testGetUrl(string $baseUrl, string $type, string $path, ?int $entity)
    {
        $this->urlBuilderMock
            ->expects($this->once())
            ->method('getBaseUrl')
            ->willReturn($baseUrl);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaDataMock */
        $slaDataMock = $this->createMock(SlaDataInterface::class);

        $slaDataMock
            ->expects($this->once())
            ->method('getType')
            ->willReturn($type);

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->getMockBuilder(Orders::class)
            ->disableOriginalConstructor()
            ->setMethods(['getOrderId'])
            ->getMock();

        $this->ordersRepositoryMock
            ->method('getById')
            ->willReturn($merchantOrderMock);

        $merchantOrderMock
            ->method('getOrderId')
            ->willReturn($entity);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->productCollectionMock
            ->method('create')
            ->willReturn($collectionMock);

        $collectionMock
            ->method('addFieldToFilter')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|MerchantProduct $productMock */
        $productMock = $this->getMockBuilder(MerchantProduct::class)
            ->disableOriginalConstructor()
            ->setMethods(['getMageproductId'])
            ->getMock();

        $collectionMock
            ->method('getFirstItem')
            ->willReturn($productMock);

        $productMock
            ->method('getMageproductId')
            ->willReturn($entity);

        $slaDataMock->method('getRelatedEntityId')->willReturn($entity);

        $getUrl = $this->testObject->getUrl($slaDataMock);
        $this->assertInternalType('string', $getUrl);
        $this->assertEquals($baseUrl . $path . $entity, $getUrl);
    }

    /**
     * @return void
     */
    public function testGetUrlNoSuchEntityExceptionHandling()
    {
        $this->urlBuilderMock
            ->expects($this->once())
            ->method('getBaseUrl')
            ->willReturn('www.base-url.com');

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaDataMock */
        $slaDataMock = $this->createMock(SlaDataInterface::class);

        $slaDataMock
            ->expects($this->once())
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_NEW_ORDER);

        $this->ordersRepositoryMock
            ->method('getById')
            ->willThrowException(new NoSuchEntityException(__('Error')));

        $this->testObject->getUrl($slaDataMock);
    }

    /**
     * @return void
     */
    public function testGetUrlLocalizedExceptionExceptionHandling()
    {
        $this->urlBuilderMock
            ->expects($this->once())
            ->method('getBaseUrl')
            ->willReturn('www.base-url.com');

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaDataMock */
        $slaDataMock = $this->createMock(SlaDataInterface::class);

        $slaDataMock
            ->expects($this->once())
            ->method('getType')
            ->willReturn(SlaDataInterface::SLA_TYPE_NEW_ORDER);

        $this->ordersRepositoryMock
            ->method('getById')
            ->willThrowException(new LocalizedException(__('Error')));

        $this->testObject->getUrl($slaDataMock);
    }
}
