<?php
/**
 * This file consist of PHPUnit test case for Helper class PrepareSla
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Helper;

use Arb\Sla\Helper\SlaEmail;
use Arb\Sla\Helper\SlaUrlBuilder;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\Config;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\SlaRepositoryInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Arb\Sla\Helper\PrepareSla;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\Api\SearchResultsInterface;

/**
 * @covers \Arb\Sla\Helper\PrepareSla
 */
class PrepareSlaTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaRepositoryInterface
     */
    private $slaRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StoreManagerInterface
     */
    private $storeManagerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Emulation
     */
    private $emulationMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEmail
     */
    private $emailMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaUrlBuilder
     */
    private $slaUrlBuilderMock;

    /**
     * Object to test
     *
     * @var PrepareSla
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaRepositoryMock = $this->createMock(SlaRepositoryInterface::class);
        $this->configMock = $this->createMock(Config::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->emulationMock = $this->createMock(Emulation::class);
        $this->emailMock = $this->createMock(SlaEmail::class);
        $this->slaUrlBuilderMock = $this->createMock(SlaUrlBuilder::class);

        $this->testObject = $objectManager->getObject(PrepareSla::class, [
            'slaRepository' => $this->slaRepositoryMock,
            'config' => $this->configMock,
            'storeManager' => $this->storeManagerMock,
            'emulation' => $this->emulationMock,
            'email' => $this->emailMock,
            'slaUrlBuilder' => $this->slaUrlBuilderMock
        ]);
    }

    /**
     * @return array
     */
    public function runSlaCheckProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        return [
            [
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                '0',                                            // store id
                [$slaMock]                                      // sla array
            ],
            [
                SlaDataInterface::SLA_TYPE_LOW_STOCK,           // sla type
                '1',                                            // store id
                [$slaMock,$slaMock]                             // sla array
            ],
            [
                SlaDataInterface::SLA_TYPE_ORDER_SHIPPING,      // sla type
                '1',                                            // store id
                [$slaMock,$slaMock]                             // sla array
            ],
            [
                SlaDataInterface::SLA_TYPE_RETURN_REQUEST,      // sla type
                '2',                                            // store id
                [$slaMock]                                      // sla array
            ]
        ];
    }

    /**
     * @dataProvider runSlaCheckProvider
     *
     * @param string $type
     * @param string $storeId
     * @param array $slaArray
     *
     * @return void
     */
    public function testRunSlaCheck(string $type, string $storeId, array $slaArray)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|StoreInterface $storeMock */
        $storeMock = $this->createMock(StoreInterface::class);

        $storesArray = [$storeMock];

        $this->emulationMock
            ->expects($this->exactly(count($storesArray)))
            ->method('startEnvironmentEmulation');

        $this->storeManagerMock
            ->expects($this->exactly(count($storesArray)))
            ->method('getStores')
            ->willReturn($storesArray);

        $storeMock
            ->method('getId')
            ->willReturn($storeId);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultMock */
        $searchResultMock = $this->createMock(SearchResultsInterface::class);

        $this->slaRepositoryMock
            ->expects($this->exactly(count($storesArray)))
            ->method('getAllExpiredSla')
            ->willReturn($searchResultMock);

        $searchResultMock
            ->expects($this->exactly(count($storesArray)))
            ->method('getItems')
            ->willReturn($slaArray);

        $this->slaUrlBuilderMock
            ->expects($this->exactly(count($slaArray)))
            ->method('getUrl')
            ->willReturn('www.someurl.com');

        $this->configMock
            ->expects($this->exactly(count($slaArray)))
            ->method('getSenderEmail')
            ->willReturn('some@email.com');

        $slaArray[0]
            ->method('getDeadlineTime')
            ->willReturn(date("Y-m-d H:i:s", strtotime('-5 hours')));

        $this->emailMock
            ->expects($this->exactly(count($slaArray)))
            ->method('sendEmail');

        $this->testObject->runSlaCheck($type);
    }
}
