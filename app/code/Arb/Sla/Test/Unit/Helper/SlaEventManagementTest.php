<?php
/**
 * This file consist of PHPUnit test case for Helper class SlaEventManagement
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Helper;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Api\SlaRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Arb\Sla\Model\SlaDataFactory;
use Arb\Sla\Helper\SlaEventManagement;
use DateTime;
use DateInterval;

/**
 * @covers \Arb\Sla\Helper\SlaEventManagement
 */
class SlaEventManagementTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaRepositoryInterface
     */
    private $slaRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StoreManagerInterface
     */
    private $storeManagerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaDataFactory
     */
    private $slaDataFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * Object to test
     *
     * @var SlaEventManagement
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaRepositoryMock = $this->createMock(SlaRepositoryInterface::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->slaDataFactoryMock = $this->createMock(SlaDataFactory::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);

        $this->testObject = $objectManager->getObject(SlaEventManagement::class, [
            'slaRepository' => $this->slaRepositoryMock,
            'storeManager' => $this->storeManagerMock,
            'slaDataFactory' => $this->slaDataFactoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock
        ]);
    }

    /**
     * @return void
     */
    public function testSaveSla()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        $this->slaRepositoryMock
            ->expects($this->once())
            ->method('save');

        $this->testObject->saveSla($slaMock);
    }

    /**
     * @return array
     */
    public function initGetSlaProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        return [
            [
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                [$slaMock],                                     // sla array
                $slaMock                                        // return value
            ],
            [
                SlaDataInterface::SLA_TYPE_LOW_STOCK,           // sla type
                [],                                             // sla array
                null                                            // return value
            ]
        ];
    }

    /**
     * @dataProvider initGetSlaProvider
     *
     * @param string $type
     * @param array $slaArray
     * @param SlaDataInterface|null $returnValue
     *
     * @return void
     */
    public function testInitGetSla(string $type, array $slaArray, ?SlaDataInterface $returnValue)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaInterface $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->exactly(2))
            ->method('addFilter')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultsMock */
        $searchResultsMock = $this->createMock(SearchResultsInterface::class);

        $this->slaRepositoryMock
            ->expects($this->once())
            ->method('getList')
            ->willReturn($searchResultsMock);

        $searchResultsMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn($slaArray);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        if (!$returnValue) {
            $this->slaDataFactoryMock
                ->expects($this->once())
                ->method('create')
                ->willReturn($slaMock);
        }

        $initSla = $this->testObject->initSla($type, 1);
        $this->assertInstanceOf(SlaDataInterface::class, $initSla);
    }

    /**
     * @return array
     */
    public function setSlaDataProvider()
    {
        return [
            [
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                50,                                             // reminder % value
                10                                              // deadline value (hours)
            ],
            [
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                150,                                            // reminder % value
                10                                              // deadline value (hours)
            ],
            [
                SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,    // sla type
                0,                                              // reminder % value
                10                                              // deadline value (hours)
            ],
            [
                SlaDataInterface::SLA_TYPE_LOW_STOCK,           // sla type
                50,                                             // reminder % value
                10                                              // deadline value (hours)
            ],
            [
            SlaDataInterface::SLA_TYPE_LOW_STOCK,               // sla type
                50,                                             // reminder % value
                1                                               // deadline value (hours)
            ],
            [
                SlaDataInterface::SLA_TYPE_LOW_STOCK,           // sla type
                44,                                             // reminder % value
                23                                              // deadline value (hours)
            ],
            [
                SlaDataInterface::SLA_TYPE_LOW_STOCK,           // sla type
                23,                                             // reminder % value
                100                                             // deadline value (hours)
            ],
            [
                SlaDataInterface::SLA_TYPE_LOW_STOCK,           // sla type
                99,                                             // reminder % value
                8                                               // deadline value (hours)
            ],
            [
                SlaDataInterface::SLA_TYPE_LOW_STOCK,           // sla type
                99,                                             // reminder % value
                0                                               // deadline value (hours)
            ]
        ];
    }

    /**
     * @dataProvider setSlaDataProvider
     *
     * @param string $type
     * @param int $reminderValue
     * @param int $deadlineValue
     *
     * @return void
     */
    public function testSetSlaData(string $type, int $reminderValue, int $deadlineValue)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        $relatedEntityId = 1;
        $merchantId = 1;
        $setNotificationCallCount = $reminderValue ? 0 : 1;
        $setReminderCallCount = $reminderValue ? 1 : 0;

        $slaMock->expects($this->exactly($setNotificationCallCount))
            ->method('setMerchantNotification')
            ->willReturnSelf();

        $slaMock->expects($this->exactly($setNotificationCallCount))
            ->method('setAdminNotification')
            ->willReturnSelf();

        $slaMock->expects($this->once())
            ->method('setTriggerTime')
            ->willReturnSelf();

        $slaMock->expects($this->exactly($setReminderCallCount))
            ->method('setReminderTime')
            ->willReturnSelf();

        $slaMock->expects($this->once())
            ->method('setDeadlineTime')
            ->willReturnSelf();

        $slaMock->expects($this->once())
            ->method('getRelatedEntityId')
            ->willReturn(null);

        $slaMock->expects($this->once())
            ->method('setRelatedEntityId')
            ->willReturnSelf();

        $slaMock->expects($this->exactly(2))
            ->method('getType')
            ->willReturnOnConsecutiveCalls(null, $type);

        $slaMock->expects($this->once())
            ->method('setType')
            ->willReturnSelf();

        $slaMock->expects($this->once())
            ->method('getMerchantId')
            ->willReturn(null);

        $slaMock->expects($this->once())
            ->method('setMerchantId')
            ->willReturnSelf();

        $slaMock->expects($this->any())
            ->method('getStoreId')
            ->willReturn(null);

        /** @var PHPUnit_Framework_MockObject_MockObject|StoreInterface $storeMock */
        $storeMock = $this->createMock(StoreInterface::class);

        $this->storeManagerMock
            ->expects($this->any())
            ->method('getStore')
            ->willReturn($storeMock);

        $storeMock
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);

        $slaMock->expects($this->any())
            ->method('setStoreId')
            ->willReturnSelf();

        $this->testObject->setSlaData(
            $slaMock,
            $relatedEntityId,
            $reminderValue,
            $deadlineValue,
            $type,
            $merchantId
        );
    }

    /**
     * @return void
     */
    public function testDeleteSla()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaInterface $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->exactly(2))
            ->method('addFilter')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchResultsInterface $searchResultsMock */
        $searchResultsMock = $this->createMock(SearchResultsInterface::class);

        $this->slaRepositoryMock
            ->expects($this->once())
            ->method('getList')
            ->willReturn($searchResultsMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        $slaArray = [$slaMock];

        $searchResultsMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn($slaArray);

        $this->slaRepositoryMock
            ->expects($this->exactly(count($slaArray)))
            ->method('delete');

        $this->testObject->deleteSla('any type', 1);
    }
}
