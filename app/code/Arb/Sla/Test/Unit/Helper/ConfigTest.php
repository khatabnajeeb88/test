<?php
/**
 * This file consist of PHPUnit test case for Helper class Config
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\CustomWebkul\Test\Unit\Controller\Order;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Arb\Sla\Helper\Config;

/**
 * @covers \Arb\Sla\Helper\Config
 */
class ConfigTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ScopeConfigInterface
     */
    private $scopeConfigMock;

    /**
     * Object to test
     *
     * @var Config
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->scopeConfigMock = $this->createMock(ScopeConfigInterface::class);

        $this->testObject = $objectManager->getObject(Config::class, [
            'scopeConfig' => $this->scopeConfigMock
        ]);
    }

    /**
     * @return array
     */
    public function configValuesProvider()
    {
        return [
            [
                'some config value'     // config value
            ],
            [
                null                    // config value
            ]
        ];
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetProductApprovalCronEnabled(?string $configValue)
    {
        $this->scopeConfigMock->expects($this->once())->method('getValue')->willReturn($configValue);

        $getProductApprovalCronEnabled = $this->testObject->getProductApprovalCronEnabled();
        $this->assertInternalType('int', $getProductApprovalCronEnabled);
        $this->assertEquals((int)$configValue, $getProductApprovalCronEnabled);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetProductLowStockCronEnabled(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getProductLowStockCronEnabled = $this->testObject->getProductLowStockCronEnabled();
        $this->assertInternalType('int', $getProductLowStockCronEnabled);
        $this->assertEquals((int)$configValue, $getProductLowStockCronEnabled);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetReturnRequestCronEnabled(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getReturnRequestCronEnabled = $this->testObject->getReturnRequestCronEnabled();
        $this->assertInternalType('int', $getReturnRequestCronEnabled);
        $this->assertEquals((int)$configValue, $getReturnRequestCronEnabled);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetNewOrderRequiresActionCronEnabled(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getNewOrderRequiresActionCronEnabled = $this->testObject->getNewOrderRequiresActionCronEnabled();
        $this->assertInternalType('int', $getNewOrderRequiresActionCronEnabled);
        $this->assertEquals((int)$configValue, $getNewOrderRequiresActionCronEnabled);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetFinalizeOrderCronEnabled(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getFinalizeOrderCronEnabled = $this->testObject->getFinalizeOrderCronEnabled();
        $this->assertInternalType('int', $getFinalizeOrderCronEnabled);
        $this->assertEquals((int)$configValue, $getFinalizeOrderCronEnabled);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetProductApprovalReminderTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getProductApprovalReminderTime = $this->testObject->getProductApprovalReminderTime();
        $this->assertInternalType('int', $getProductApprovalReminderTime);
        $this->assertEquals((int)$configValue, $getProductApprovalReminderTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetProductApprovalDeadlineTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getProductApprovalDeadlineTime = $this->testObject->getProductApprovalDeadlineTime();
        $this->assertInternalType('int', $getProductApprovalDeadlineTime);
        $this->assertEquals((int)$configValue, $getProductApprovalDeadlineTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetProductLowStockReminderTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getProductLowStockReminderTime = $this->testObject->getProductLowStockReminderTime();
        $this->assertInternalType('int', $getProductLowStockReminderTime);
        $this->assertEquals((int)$configValue, $getProductLowStockReminderTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetProductLowStockDeadlineTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getProductLowStockDeadlineTime = $this->testObject->getProductLowStockDeadlineTime();
        $this->assertInternalType('int', $getProductLowStockDeadlineTime);
        $this->assertEquals((int)$configValue, $getProductLowStockDeadlineTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetOrderReturnRequestReminderTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getOrderReturnRequestReminderTime = $this->testObject->getOrderReturnRequestReminderTime();
        $this->assertInternalType('int', $getOrderReturnRequestReminderTime);
        $this->assertEquals((int)$configValue, $getOrderReturnRequestReminderTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetOrderReturnRequestDeadlineTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getOrderReturnRequestDeadlineTime = $this->testObject->getOrderReturnRequestDeadlineTime();
        $this->assertInternalType('int', $getOrderReturnRequestDeadlineTime);
        $this->assertEquals((int)$configValue, $getOrderReturnRequestDeadlineTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetNewOrderRequiresActionReminderTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getNewOrderRequiresActionReminderTime = $this->testObject->getNewOrderRequiresActionReminderTime();
        $this->assertInternalType('int', $getNewOrderRequiresActionReminderTime);
        $this->assertEquals((int)$configValue, $getNewOrderRequiresActionReminderTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetNewOrderRequiresActionDeadlineTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getNewOrderRequiresActionDeadlineTime = $this->testObject->getNewOrderRequiresActionDeadlineTime();
        $this->assertInternalType('int', $getNewOrderRequiresActionDeadlineTime);
        $this->assertEquals((int)$configValue, $getNewOrderRequiresActionDeadlineTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetFinalizeOrderReminderTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getFinalizeOrderReminderTime = $this->testObject->getFinalizeOrderReminderTime();
        $this->assertInternalType('int', $getFinalizeOrderReminderTime);
        $this->assertEquals((int)$configValue, $getFinalizeOrderReminderTime);
    }

    /**
     * @dataProvider configValuesProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetFinalizeOrderDeadlineTime(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getFinalizeOrderDeadlineTime = $this->testObject->getFinalizeOrderDeadlineTime();
        $this->assertInternalType('int', $getFinalizeOrderDeadlineTime);
        $this->assertEquals((int)$configValue, $getFinalizeOrderDeadlineTime);
    }

    /**
     * @return array
     */
    public function getSenderEmailProvider()
    {
        return [
            [
                'admin@admin.com'     // config value
            ],
            [
                null                  // config value
            ]
        ];
    }

    /**
     * @dataProvider getSenderEmailProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetSenderEmail(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getSenderEmail = $this->testObject->getSenderEmail();
        $this->assertInternalType('string', $getSenderEmail);
        $this->assertEquals((string)$configValue, $getSenderEmail);
    }

    /**
     * @dataProvider getSenderEmailProvider
     *
     * @param string|null $configValue
     *
     * @return void
     */
    public function testGetAdminReceiverEmail(?string $configValue)
    {
        $this->scopeConfigMock->method('getValue')->willReturn($configValue);

        $getAdminReceiverEmail = $this->testObject->getAdminReceiverEmail();
        $this->assertInternalType('string', $getAdminReceiverEmail);
        $this->assertEquals((string)$configValue, $getAdminReceiverEmail);
    }
}
