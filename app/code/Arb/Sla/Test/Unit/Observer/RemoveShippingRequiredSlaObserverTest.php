<?php
/**
 * This file consist of PHPUnit test case for RemoveShippingRequiredSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Arb\Sla\Observer\RemoveShippingRequiredSlaObserver;

/**
 * @covers \Arb\Sla\Observer\RemoveShippingRequiredSlaObserver
 */
class RemoveShippingRequiredSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * Object to test
     *
     * @var RemoveShippingRequiredSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);

        $this->testObject = $objectManager->getObject(RemoveShippingRequiredSlaObserver::class, [
            'slaEventManagement' => $this->slaEventManagementMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($merchantOrderMock);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('deleteSla');

        $merchantOrderMock
            ->expects($this->once())
            ->method('getId');

        $this->testObject->execute($observerMock);
    }
}
