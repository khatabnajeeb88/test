<?php
/**
 * This file consist of PHPUnit test case for OrderCanceledObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Arb\Sla\Model\SlaData;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Model\Order;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory;
use Arb\Sla\Observer\OrderCanceledObserver;
use Webkul\Marketplace\Model\ResourceModel\Orders\Collection;

/**
 * @covers \Arb\Sla\Observer\OrderCanceledObserver
 */
class OrderCanceledObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersRepository
     */
    private $ordersRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $ordersCollectionMock;

    /**
     * Object to test
     *
     * @var RemoveNewOrderSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);
        $this->ordersRepositoryMock = $this->createMock(OrdersRepository::class);
        $this->ordersCollectionMock = $this->createMock(CollectionFactory::class);

        $this->testObject = $objectManager->getObject(OrderCanceledObserver::class, [
            'slaEventManagement' => $this->slaEventManagementMock,
            'ordersRepository' => $this->ordersRepositoryMock,
            'collectionFactory' => $this->ordersCollectionMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        return [
            [
                '1',                                        // merchant Id
                [$merchantOrderMock, $merchantOrderMock]    // merchant orders array
            ],
            [
                null,                                       // merchant Id
                [$merchantOrderMock]                        // merchant orders array
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param string|null $merchantId
     * @param array $merchantOrderArray
     *
     * @return void
     */
    public function testExecute(?string $merchantId, array $merchantOrderArray)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder', 'getSellerId'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->createMock(Order::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $observerMock
            ->expects($this->once())
            ->method('getSellerId')
            ->willReturn($merchantId);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $collectionCallCount = $merchantId ? 1 : 0;
        $orderRepoCallCount = $merchantId ? 0 : 1;

        $this->ordersCollectionMock
            ->expects($this->exactly($collectionCallCount))
            ->method('create')
            ->willReturn($collectionMock);

        $collectionMock
            ->expects($this->exactly(2*$collectionCallCount))
            ->method('addFieldToFilter')
            ->willReturnOnConsecutiveCalls($collectionMock, $merchantOrderArray);

        $this->ordersRepositoryMock
            ->expects($this->exactly($orderRepoCallCount))
            ->method('getByOrderId')
            ->willReturn($merchantOrderArray);

        foreach ($merchantOrderArray as $merchantItemMock) {
            $merchantItemMock->method('getId')->willReturn('1');
        }

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $this->slaEventManagementMock
            ->expects($this->exactly(count($merchantOrderArray) * 2))
            ->method('getSla')
            ->willReturnOnConsecutiveCalls(null, $slaMock);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('deleteSla');

        $this->testObject->execute($observerMock);
    }
}
