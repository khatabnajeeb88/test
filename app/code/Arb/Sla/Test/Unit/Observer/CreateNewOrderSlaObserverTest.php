<?php
/**
 * This file consist of PHPUnit test case for CreateNewOrderSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Arb\Sla\Model\SlaData;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Model\Order;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Arb\Sla\Helper\Config;
use Webkul\Marketplace\Model\Orders;
use Webkul\Marketplace\Model\OrdersRepository;
use Arb\Sla\Observer\CreateNewOrderSlaObserver;

/**
 * @covers \Arb\Sla\Observer\CreateNewOrderSlaObserver
 */
class CreateNewOrderSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrdersRepository
     */
    private $ordersRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * Object to test
     *
     * @var CreateNewOrderSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->configMock = $this->createMock(Config::class);
        $this->ordersRepositoryMock = $this->createMock(OrdersRepository::class);
        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);

        $this->testObject = $objectManager->getObject(CreateNewOrderSlaObserver::class, [
            'config' => $this->configMock,
            'ordersRepository' => $this->ordersRepositoryMock,
            'slaEventManagement' => $this->slaEventManagementMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $merchantOrderMock */
        $merchantOrderMock = $this->createMock(Orders::class);

        return [
            [
                [$merchantOrderMock]
            ],
            [
                [$merchantOrderMock, $merchantOrderMock]
            ],
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param array $merchantOrdersArray
     *
     * @return void
     */
    public function testExecute(array $merchantOrdersArray)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->createMock(Order::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        $this->ordersRepositoryMock
            ->expects($this->once())
            ->method('getByOrderId')
            ->willReturn($merchantOrdersArray);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $this->slaEventManagementMock
            ->expects($this->exactly(count($merchantOrdersArray)))
            ->method('initSla')
            ->willReturn($slaMock);

        $this->slaEventManagementMock
            ->expects($this->exactly(count($merchantOrdersArray)))
            ->method('setSlaData')
            ->willReturn($slaMock);

        $this->configMock
            ->expects($this->exactly(count($merchantOrdersArray)))
            ->method('getNewOrderRequiresActionReminderTime')
            ->willReturn(50);

        $this->configMock
            ->expects($this->exactly(count($merchantOrdersArray)))
            ->method('getNewOrderRequiresActionDeadlineTime')
            ->willReturn(1);

        $this->slaEventManagementMock
            ->expects($this->exactly(count($merchantOrdersArray)))
            ->method('saveSla');

        $this->testObject->execute($observerMock);
    }
}
