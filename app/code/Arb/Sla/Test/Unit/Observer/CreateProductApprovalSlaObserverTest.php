<?php
/**
 * This file consist of PHPUnit test case for CreateProductApprovalSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Arb\Sla\Model\SlaData;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Arb\Sla\Helper\Config;
use Webkul\Marketplace\Model\ProductFactory;
use Webkul\Marketplace\Model\Product;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Webkul\Marketplace\Model\ResourceModel\Product\Collection;
use Arb\Sla\Observer\CreateProductApprovalSlaObserver;

/**
 * @covers \Arb\Sla\Observer\CreateProductApprovalSlaObserver
 */
class CreateProductApprovalSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ProductFactory
     */
    private $productFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * Object to test
     *
     * @var CreateProductApprovalSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->configMock = $this->createMock(Config::class);
        $this->productFactoryMock = $this->createMock(ProductFactory::class);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);
        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);

        $this->testObject = $objectManager->getObject(CreateProductApprovalSlaObserver::class, [
            'config' => $this->configMock,
            'productFactory' => $this->productFactoryMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'slaEventManagement' => $this->slaEventManagementMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        return [
            [
                1,      // merchant product id
                1       // sla methods call amount
            ],
            [
                null,   // merchant product id
                0       // sla methods call amount
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param int|null $merchantProductId
     * @param int $slaDataCallCount
     *
     * @return void
     */
    public function testExecute(?int $merchantProductId, int $slaDataCallCount)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getProductId', 'getMerchantId'])
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getProductId')
            ->willReturn(1);

        $observerMock
            ->expects($this->once())
            ->method('getMerchantId')
            ->willReturn(1);

          /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->collectionFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $merchantProductMock */
        $merchantProductMock = $this->createMock(Product::class);

        $collectionMock
            ->expects($this->exactly(2))
            ->method('addFieldToFilter')
            ->willReturnOnConsecutiveCalls($collectionMock, [$merchantProductMock]);

        $merchantProductMock
            ->method('getId')
            ->willReturn($merchantProductId);

        $this->productFactoryMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('create')
            ->willReturn($merchantProductMock);

        $merchantProductMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('load')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $this->slaEventManagementMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('initSla')
            ->willReturn($slaMock);

        $this->slaEventManagementMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('setSlaData')
            ->willReturn($slaMock);

        $this->configMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('getProductApprovalReminderTime')
            ->willReturn(50);

        $this->configMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('getProductApprovalDeadlineTime')
            ->willReturn(1);

        $slaMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('setAdminNotification');

        $this->slaEventManagementMock
            ->expects($this->exactly($slaDataCallCount))
            ->method('saveSla');

        $this->testObject->execute($observerMock);
    }
}
