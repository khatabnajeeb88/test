<?php
/**
 * This file consist of PHPUnit test case for CreateShippingRequiredSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Arb\Sla\Model\SlaData;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\Orders;
use Arb\Sla\Observer\CreateShippingRequiredSlaObserver;
use Arb\Sla\Helper\Config;

/**
 * @covers \Arb\Sla\Observer\CreateShippingRequiredSlaObserver
 */
class CreateShippingRequiredSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * Object to test
     *
     * @var CreateShippingRequiredSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->configMock = $this->createMock(Config::class);
        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);

        $this->testObject = $objectManager->getObject(CreateShippingRequiredSlaObserver::class, [
            'config' => $this->configMock,
            'slaEventManagement' => $this->slaEventManagementMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Orders $orderMock */
        $orderMock = $this->createMock(Orders::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('initSla')
            ->willReturn($slaMock);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('setSlaData')
            ->willReturn($slaMock);

        $this->configMock
            ->expects($this->once())
            ->method('getFinalizeOrderReminderTime')
            ->willReturn(50);

        $this->configMock
            ->expects($this->once())
            ->method('getFinalizeOrderDeadlineTime')
            ->willReturn(1);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('saveSla');

        $this->testObject->execute($observerMock);
    }
}
