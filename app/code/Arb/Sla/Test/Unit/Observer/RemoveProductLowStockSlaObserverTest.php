<?php
/**
 * This file consist of PHPUnit test case for RemoveProductLowStockSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Arb\Sla\Api\Data\SlaDataInterface;
use Magento\CatalogInventory\Api\Data\StockItemCollectionInterface;
use Magento\CatalogInventory\Model\ResourceModel\Stock\Item\StockItemCriteria;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Arb\Sla\Observer\RemoveProductLowStockSlaObserver;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Catalog\Model\Product;
use Webkul\Marketplace\Model\ResourceModel\Product\Collection;
use Webkul\Marketplace\Model\ResourceModel\Product as MerchantProduct;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;

/**
 * @covers \Arb\Sla\Observer\RemoveProductLowStockSlaObserver
 */
class RemoveProductLowStockSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StockItemRepository
     */
    private $stockItemRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ProductCollection
     */
    private $productCollectionMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StockItemCriteriaInterfaceFactory
     */
    private $stockItemCriteriaInterfaceFactoryMock;

    /**
     * Object to test
     *
     * @var RemoveProductLowStockSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);
        $this->stockItemRepositoryMock = $this->createMock(StockItemRepository::class);
        $this->productCollectionMock = $this->createMock(ProductCollection::class);
        $this->stockItemCriteriaInterfaceFactoryMock = $this->createMock(StockItemCriteriaInterfaceFactory::class);

        $this->testObject = $objectManager->getObject(RemoveProductLowStockSlaObserver::class, [
            'slaEventManagement' => $this->slaEventManagementMock,
            'stockItemRepository' => $this->stockItemRepositoryMock,
            'productCollection' => $this->productCollectionMock,
            'stockItemCriteriaInterfaceFactory' => $this->stockItemCriteriaInterfaceFactoryMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|MerchantProduct $merchantProductMock */
        $merchantProductMock = $this->getMockBuilder(MerchantProduct::class)
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        return [
            [
                [$merchantProductMock],                                                 // merchant items array
                0,                                                                      // existing sla count
                0                                                                       // delete sla count
            ],
            [
                [$merchantProductMock, $merchantProductMock],                           // merchant items array
                1,                                                                      // existing sla count
                1                                                                       // delete sla count
            ],
            [
                [$merchantProductMock, $merchantProductMock, $merchantProductMock],     // merchant items array
                2,                                                                      // existing sla count
                1                                                                       // delete sla count
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param array $merchantArray
     * @param int $existingSlaCount
     * @param int $deleteSlaCount
     *
     * @return void
     */
    public function testExecute(array $merchantArray, int $existingSlaCount, int $deleteSlaCount)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getProduct'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->createMock(Product::class);

        $observerMock->expects($this->once())->method('getProduct')->willReturn($productMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->productCollectionMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($collectionMock);

        $collectionMock
            ->expects($this->once())
            ->method('addFieldToFilter')
            ->willReturn($merchantArray);

        foreach ($merchantArray as $merchantItemMock) {
            $merchantItemMock->method('getId')->willReturn('1');
        }

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaDataInterface $slaMock */
        $slaMock = $this->createMock(SlaDataInterface::class);

        $this->slaEventManagementMock
            ->expects($this->exactly(count($merchantArray)))
            ->method('getSla')
            ->willReturnOnConsecutiveCalls(null, $slaMock, $slaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemCriteria $stockItemCriteriaMock */
        $stockItemCriteriaMock = $this->createMock(StockItemCriteria::class);

        $this->stockItemCriteriaInterfaceFactoryMock
            ->expects($this->exactly($existingSlaCount))
            ->method('create')
            ->willReturn($stockItemCriteriaMock);

        $stockItemCriteriaMock
            ->expects($this->exactly($existingSlaCount))
            ->method('setProductsFilter')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemCollectionInterface $stockItemListMock */
        $stockItemListMock = $this->createMock(StockItemCollectionInterface::class);

        $this->stockItemRepositoryMock
            ->expects($this->exactly($existingSlaCount))
            ->method('getList')
            ->willReturn($stockItemListMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemInterface $stockItemMock */
        $stockItemMock = $this->createMock(StockItemInterface::class);

        $stockItemListMock
            ->expects($this->exactly($existingSlaCount))
            ->method('getItems')
            ->willReturn([$stockItemMock]);

        $stockItemMock
            ->expects($this->exactly($existingSlaCount))
            ->method('getLowStockDate')
            ->willReturnOnConsecutiveCalls(null, 'some date');

        $this->slaEventManagementMock
            ->expects($this->exactly($deleteSlaCount))
            ->method('deleteSla');

        $this->testObject->execute($observerMock);
    }
}
