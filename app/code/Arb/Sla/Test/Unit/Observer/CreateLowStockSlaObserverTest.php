<?php
/**
 * This file consist of PHPUnit test case for CreateLowStockSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Arb\Sla\Model\SlaData;
use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\Data\StockItemCollectionInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Model\ResourceModel\Stock\Item\StockItemCriteria;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Model\Order;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Arb\Sla\Helper\Config;
use Webkul\Marketplace\Model\Product as MerchantProduct;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Webkul\Marketplace\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Model\Order\Item;
use Arb\Sla\Observer\CreateLowStockSlaObserver;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;

/**
 * @covers \Arb\Sla\Observer\CreateLowStockSlaObserver
 */
class CreateLowStockSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StockItemRepository
     */
    private $stockItemRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ProductCollection
     */
    private $productCollectionMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StockItemCriteriaInterfaceFactory
     */
    private $stockItemCriteriaInterfaceFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ProductRepositoryInterface
     */
    private $productRepositoryMock;

    /**
     * Object to test
     *
     * @var CreateLowStockSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->configMock = $this->createMock(Config::class);
        $this->stockItemRepositoryMock = $this->createMock(StockItemRepository::class);
        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);
        $this->productCollectionMock = $this->createMock(ProductCollection::class);
        $this->productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);
        $this->stockItemCriteriaInterfaceFactoryMock = $this->createMock(StockItemCriteriaInterfaceFactory::class);

        $this->testObject = $objectManager->getObject(CreateLowStockSlaObserver::class, [
            'config' => $this->configMock,
            'stockItemRepository' => $this->stockItemRepositoryMock,
            'slaEventManagement' => $this->slaEventManagementMock,
            'productCollection' => $this->productCollectionMock,
            'stockItemCriteriaInterfaceFactory' => $this->stockItemCriteriaInterfaceFactoryMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        return [
            [
                null,           // low stock date for product
                0,              // product method call amount
                0               // sla method call amount
            ],
            [
                'Some date',    // low stock date for product
                1,              // product method call amount
                1               // sla method call amount
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param string|null $lowStockDate
     * @param int $productMethodCalls
     * @param int $slaMethodCalls
     *
     * @return void
     */
    public function testExecute(?string $lowStockDate, int $productMethodCalls, int $slaMethodCalls)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getOrder'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Order $orderMock */
        $orderMock = $this->createMock(Order::class);

        $observerMock
            ->expects($this->once())
            ->method('getOrder')
            ->willReturn($orderMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Item $orderItemMock */
        $orderItemMock = $this->createMock(Item::class);

        $orderItemsArray = [$orderItemMock];

        $orderMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn($orderItemsArray);

        $orderItemMock
            ->expects($this->exactly(count($orderItemsArray)))
            ->method('getProductId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemCriteria $stockItemCriteriaMock */
        $stockItemCriteriaMock = $this->createMock(StockItemCriteria::class);

        $this->stockItemCriteriaInterfaceFactoryMock
            ->expects($this->exactly(count($orderItemsArray)))
            ->method('create')
            ->willReturn($stockItemCriteriaMock);

        $stockItemCriteriaMock
            ->expects($this->exactly(count($orderItemsArray)))
            ->method('setProductsFilter')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemCollectionInterface $stockItemListMock */
        $stockItemListMock = $this->createMock(StockItemCollectionInterface::class);

        $this->stockItemRepositoryMock
            ->expects($this->exactly(count($orderItemsArray)))
            ->method('getList')
            ->willReturn($stockItemListMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemInterface $stockItemMock */
        $stockItemMock = $this->createMock(StockItemInterface::class);

        $stockItemListMock
            ->expects($this->exactly(count($orderItemsArray)))
            ->method('getItems')
            ->willReturn([$stockItemMock]);

        $stockItemMock
            ->expects($this->exactly(count($orderItemsArray)))
            ->method('getLowStockDate')
            ->willReturn($lowStockDate);

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->createMock(Product::class);

        $orderItemMock
            ->expects($this->exactly($productMethodCalls))
            ->method('getProduct')
            ->willReturn($productMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $this->productCollectionMock
            ->expects($this->exactly($productMethodCalls))
            ->method('create')
            ->willReturn($collectionMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|MerchantProduct $merchantProductMock */
        $merchantProductMock = $this->createMock(MerchantProduct::class);

        $merchantProductArray = [$merchantProductMock];

        $collectionMock
            ->expects($this->exactly($productMethodCalls))
            ->method('addFieldToFilter')
            ->willReturn($merchantProductArray);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $this->slaEventManagementMock
            ->expects($this->exactly($slaMethodCalls))
            ->method('initSla')
            ->willReturn($slaMock);

        $this->slaEventManagementMock
            ->expects($this->exactly($slaMethodCalls))
            ->method('setSlaData')
            ->willReturn($slaMock);

        $this->configMock
            ->expects($this->exactly($slaMethodCalls))
            ->method('getProductLowStockReminderTime')
            ->willReturn(50);

        $this->configMock
            ->expects($this->exactly($slaMethodCalls))
            ->method('getProductLowStockDeadlineTime')
            ->willReturn(5);

        $slaMock
            ->expects($this->exactly($slaMethodCalls))
            ->method('setMerchantNotification');

        $this->slaEventManagementMock
            ->expects($this->exactly($slaMethodCalls))
            ->method('saveSla');

        $this->testObject->execute($observerMock);
    }
}
