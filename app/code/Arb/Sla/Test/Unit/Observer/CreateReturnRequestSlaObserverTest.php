<?php
/**
 * This file consist of PHPUnit test case for CreateReturnRequestSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Arb\Sla\Model\SlaData;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Arb\Sla\Observer\CreateReturnRequestSlaObserver;
use Arb\Sla\Helper\Config;
use Webkul\MpRmaSystem\Model\ResourceModel\Details\CollectionFactory;
use Webkul\MpRmaSystem\Model\ResourceModel\Details\Collection;
use Webkul\MpRmaSystem\Model\Details;

/**
 * @covers \Arb\Sla\Observer\CreateReturnRequestSlaObserver
 */
class CreateReturnRequestSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * Object to test
     *
     * @var CreateReturnRequestSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->configMock = $this->createMock(Config::class);
        $this->collectionFactoryMock = $this->createMock(CollectionFactory::class);
        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);

        $this->testObject = $objectManager->getObject(CreateReturnRequestSlaObserver::class, [
            'config' => $this->configMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'slaEventManagement' => $this->slaEventManagementMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Details $rmaMock */
        $rmaMock = $this->createMock(Details::class);

        return [
            [
                $rmaMock,   // rma entity passed via observer
                null        // rma id passed via observer
            ],
            [
                $rmaMock,   // rma entity passed via observer
                1           // rma id passed via observer
            ],
            [
                null,       // rma entity passed via observer
                1           // rma id passed via observer
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param Details|null $rma
     * @param int|null $rmaId
     *
     * @return void
     */
    public function testExecute(?Details $rma, ?int $rmaId)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getRma', 'getRmaId', 'getSellerId'])
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getRma')
            ->willReturn($rma);

        $observerMock
            ->expects($this->once())
            ->method('getRmaId')
            ->willReturn($rmaId);

        $observerMock
            ->expects($this->once())
            ->method('getSellerId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $collectionMock */
        $collectionMock = $this->createMock(Collection::class);

        $collectionCallCount = (!$rma && $rmaId) ? 1 : 0;

        $this->collectionFactoryMock
            ->expects($this->exactly($collectionCallCount))
            ->method('create')
            ->willReturn($collectionMock);

        $collectionMock
            ->expects($this->exactly($collectionCallCount))
            ->method('addFieldToFilter')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|Details $rmaMock */
        $rmaMock = $this->createMock(Details::class);

        $collectionMock
            ->expects($this->exactly($collectionCallCount))
            ->method('getFirstItem')
            ->willReturn($rmaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|SlaData $slaMock */
        $slaMock = $this->createMock(SlaData::class);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('initSla')
            ->willReturn($slaMock);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('setSlaData')
            ->willReturn($slaMock);

        $this->configMock
            ->expects($this->once())
            ->method('getOrderReturnRequestReminderTime')
            ->willReturn(50);

        $this->configMock
            ->expects($this->once())
            ->method('getOrderReturnRequestDeadlineTime')
            ->willReturn(1);

        $slaMock
            ->expects($this->once())
            ->method('setMerchantNotification');

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('saveSla');

        $this->testObject->execute($observerMock);
    }
}
