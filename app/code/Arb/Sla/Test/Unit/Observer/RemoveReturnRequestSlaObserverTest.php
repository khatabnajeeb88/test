<?php
/**
 * This file consist of PHPUnit test case for RemoveReturnRequestSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Webkul\MpRmaSystem\Model\Details;
use Arb\Sla\Observer\RemoveReturnRequestSlaObserver;

/**
 * @covers \Arb\Sla\Observer\RemoveReturnRequestSlaObserver
 */
class RemoveReturnRequestSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * Object to test
     *
     * @var RemoveReturnRequestSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);

        $this->testObject = $objectManager->getObject(RemoveReturnRequestSlaObserver::class, [
            'slaEventManagement' => $this->slaEventManagementMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getRma'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|Details $rmaMock */
        $rmaMock = $this->createMock(Details::class);

        $observerMock
            ->expects($this->once())
            ->method('getRma')
            ->willReturn($rmaMock);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('deleteSla');

        $rmaMock
            ->expects($this->once())
            ->method('getId');

        $this->testObject->execute($observerMock);
    }
}
