<?php
/**
 * This file consist of PHPUnit test case for RemoveProductApprovalSlaObserver
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\SlaEventManagement;
use Magento\Framework\Event\Observer;
use Webkul\Marketplace\Model\ResourceModel\Product as MerchantProduct;
use Arb\Sla\Observer\RemoveProductApprovalSlaObserver;

/**
 * @covers \Arb\Sla\Observer\RemoveProductApprovalSlaObserver
 */
class RemoveProductApprovalSlaObserverTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SlaEventManagement
     */
    private $slaEventManagementMock;

    /**
     * Object to test
     *
     * @var RemoveProductApprovalSlaObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->slaEventManagementMock = $this->createMock(SlaEventManagement::class);

        $this->testObject = $objectManager->getObject(RemoveProductApprovalSlaObserver::class, [
            'slaEventManagement' => $this->slaEventManagementMock
        ]);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getProduct'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|MerchantProduct $productMock */
        $productMock = $this->getMockBuilder(MerchantProduct::class)
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getProduct')
            ->willReturn($productMock);

        $this->slaEventManagementMock
            ->expects($this->once())
            ->method('deleteSla');

        $this->testObject->execute($observerMock);
    }
}
