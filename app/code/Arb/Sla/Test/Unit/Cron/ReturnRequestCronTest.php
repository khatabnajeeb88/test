<?php
/**
 * This file consist of PHPUnit test case for Cron class ReturnRequestCron
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Cron;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\Sla\Helper\Config;
use Arb\Sla\Helper\PrepareSla;
use Arb\Sla\Cron\ReturnRequestCron;

/**
 * @covers \Arb\Sla\Cron\ReturnRequestCron
 */
class ReturnRequestCronTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|PrepareSla
     */
    private $prepareSlaMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Config
     */
    private $configMock;

    /**
     * Object to test
     *
     * @var ReturnRequestCron
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->prepareSlaMock = $this->createMock(PrepareSla::class);
        $this->configMock = $this->createMock(Config::class);

        $this->testObject = $objectManager->getObject(ReturnRequestCron::class, [
            'prepareSla' => $this->prepareSlaMock,
            'config' => $this->configMock
        ]);
    }

    /**
     * @return array
     */
    public function executeProvider()
    {
        return [
            [
                1   // config value for cron enabled/disabled state
            ],
            [
                0   // config value for cron enabled/disabled state
            ]
        ];
    }

    /**
     * @dataProvider executeProvider
     *
     * @param int $configValue
     *
     * @return void
     */
    public function testExecute(int $configValue)
    {
        $this->configMock
            ->expects($this->once())
            ->method('getReturnRequestCronEnabled')
            ->willReturn($configValue);

        $prepareSlaCallCount = $configValue ? 1 : 0;

        $this->prepareSlaMock
            ->expects($this->exactly($prepareSlaCallCount))
            ->method('runSlaCheck');

        $this->testObject->execute();
    }
}
