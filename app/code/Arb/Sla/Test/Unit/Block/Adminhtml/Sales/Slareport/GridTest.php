<?php
/**
 * This file consist of PHPUnit test case for class Grid
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Test\Unit\Block\Adminhtml\Sales\Slareport;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\Sla\Block\Adminhtml\Sales\Slareport\Grid;
use Arb\Sla\Model\ResourceModel\Report\SlaReport\Collection;

/**
 * @covers \Arb\Sla\Block\Adminhtml\Sales\Slareport\Grid
 */
class GridTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Grid
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->testObject = $objectManager->getObject(Grid::class, []);
    }

    /**
     * @return void
     */
    public function testGetResourceCollectionName()
    {
        $getResourceCollectionName = $this->testObject->getResourceCollectionName();
        $this->assertEquals(Collection::class, $getResourceCollectionName);
        $this->assertInternalType('string', $getResourceCollectionName);
    }
}
