<?php
/**
 * Cron for New Order SLA
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Cron;

use Arb\Sla\Helper\PrepareSla;
use Magento\Framework\Exception\LocalizedException;
use Arb\Sla\Api\Data\SlaDataInterface;
use Arb\Sla\Helper\Config;

class NewOrderCron
{
    /**
     * @var PrepareSla
     */
    private $prepareSla;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param PrepareSla $prepareSla
     * @param Config $config
     */
    public function __construct(
        PrepareSla $prepareSla,
        Config $config
    ) {
        $this->prepareSla = $prepareSla;
        $this->config = $config;
    }

    /**
     * @return void
     *
     * @throws LocalizedException
     */
    public function execute()
    {
        if ($this->config->getNewOrderRequiresActionCronEnabled()) {
            $this->prepareSla->runSlaCheck(SlaDataInterface::SLA_TYPE_NEW_ORDER,'new_order');
        }
    }
}
