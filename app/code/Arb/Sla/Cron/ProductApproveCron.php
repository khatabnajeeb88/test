<?php
/**
 * Cron for Product Approval SLA
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Cron;

use Arb\Sla\Helper\Config;
use Arb\Sla\Helper\PrepareSla;
use Magento\Framework\Exception\LocalizedException;
use Arb\Sla\Api\Data\SlaDataInterface;

class ProductApproveCron
{
    /**
     * @var PrepareSla
     */
    private $prepareSla;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param PrepareSla $prepareSla
     * @param Config $config
     */
    public function __construct(
        PrepareSla $prepareSla,
        Config $config
    ) {
        $this->prepareSla = $prepareSla;
        $this->config = $config;
    }

    /**
     * @return void
     *
     * @throws LocalizedException
     */
    public function execute()
    {
        if ($this->config->getProductApprovalCronEnabled()) {
            $this->prepareSla->runSlaCheck(SlaDataInterface::SLA_TYPE_PRODUCT_APPROVAL,'product_approval');
        }
    }
}
