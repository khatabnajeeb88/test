<?php
/**
 * Cron for finalizing/shipping Order SLA
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Cron;

use Arb\Sla\Helper\Config;
use Magento\Framework\Exception\LocalizedException;
use Arb\Sla\Helper\PrepareSla;
use Arb\Sla\Api\Data\SlaDataInterface;

class FinalizeOrderCron
{
    /**
     * @var PrepareSla
     */
    private $prepareSla;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param PrepareSla $prepareSla
     * @param Config $config
     */
    public function __construct(
        PrepareSla $prepareSla,
        Config $config
    ) {
        $this->prepareSla = $prepareSla;
        $this->config = $config;
    }

    /**
     * @return void
     *
     * @throws LocalizedException
     */
    public function execute()
    {
        if ($this->config->getFinalizeOrderCronEnabled()) {
            $this->prepareSla->runSlaCheck(SlaDataInterface::SLA_TYPE_ORDER_SHIPPING,'finalize_order');
        }
    }
}
