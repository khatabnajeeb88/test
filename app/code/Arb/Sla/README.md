## Synopsis
An extention which creates customer token by validating session id in ESB. Invalidates customer token after logout.

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module configuration available through Stores->Configuration [system.xml](etc/adminhtml/system.xml)
4. Dependency injection configuration [di.xml](etc/di.xml)
5. Webapi configuration [webapi.xml](etc/webapi.xml)

## Tests
Unit tests could be found in the [Test/Unit](Test/Unit) directory.


