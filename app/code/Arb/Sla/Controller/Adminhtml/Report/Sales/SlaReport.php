<?php
/**
 * Sla Report
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Controller\Adminhtml\Report\Sales;

use Arb\Sla\Model\Flag;
use Magento\Reports\Controller\Adminhtml\Report\Sales;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Backend\Helper\Data as BackendHelper;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class SlaReport extends Sales
{
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Stdlib\DateTime\Filter\DateTime $dateFilter,
        TimezoneInterface $timezone,
        BackendHelper $backendHelperData
    ) {

        $this->_fileFactory = $fileFactory;
        $this->_dateFilter = $dateFilter;
        $this->timezone = $timezone;
        $this->backendHelper = $backendHelperData ?: $this->_objectManager->get(\Magento\Backend\Helper\Data::class);
        parent::__construct($context, $fileFactory, $dateFilter, $timezone);
    }
    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $this->_showLastExecutionTime(Flag::REPORT_SLAREPORT_FLAG_CODE, 'slareport');

        $this->_initAction()->_setActiveMenu(
            'Arb_Sla::report_sla'
        )->_addBreadcrumb(
            __('SLA Report'),
            __('SLA Report')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('SLA Report'));

        $gridBlock = $this->_view->getLayout()->getBlock('adminhtml_sales_slareport.grid');
        $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form');

        $this->_initReportAction([$gridBlock, $filterFormBlock]);

        $this->_view->renderLayout();
    }
     /**
     * _initReportAction
     *
     * @param  mixed $blocks
     * @return void
     */
    public function _initReportAction($blocks)
    {
        if (!is_array($blocks)) {
            $blocks = [$blocks];
        }

        $params = $this->initFilterData();

        foreach ($blocks as $block) {
            if ($block) {
                $block->setPeriodType($params->getData('period_type'));
                $block->setFilterData($params);
            }
        }

        return $this;
    }
        
    /**
     * initFilterData
     *
     * @return Magento\Framework\DataObject
     */
    private function initFilterData(): \Magento\Framework\DataObject
    {
        $requestData = $this->backendHelper
            ->prepareFilterString(
                $this->getRequest()->getParam('filter')
            );

        $filterRules = ['from' => $this->_dateFilter, 'to' => $this->_dateFilter];

        $inputFilter = new \Zend_Filter_Input($filterRules, [], $requestData);

        $requestData = $inputFilter->getUnescaped();
        $requestData['store_ids'] = $this->getRequest()->getParam('store_ids');
        $requestData['group'] = $this->getRequest()->getParam('group');
        $requestData['website'] = $this->getRequest()->getParam('website');

        $params = new \Magento\Framework\DataObject();

        foreach ($requestData as $key => $value) {
            if (!empty($value)) {
                $params->setData($key, $value);
            }
        }
        return $params;
    }
}
