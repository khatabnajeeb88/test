<?php
/**
 * Export Sla CSV Controller
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Controller\Adminhtml\Report\Sales;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Reports\Controller\Adminhtml\Report\Sales;
use Arb\Sla\Block\Adminhtml\Sales\Slareport\Grid;
use Exception;
use Magento\Framework\Controller\ResultInterface;

class ExportSlaReportCsv extends Sales
{
    /**
     * @return ResponseInterface|ResultInterface
     *
     * @throws Exception
     */
    public function execute()
    {
        $fileName = 'slareport.csv';
        $grid = $this->_view->getLayout()->createBlock(Grid::class);
        $this->_initReportAction($grid);

        return $this->_fileFactory->create($fileName, $grid->getCsvFile(), DirectoryList::VAR_DIR);
    }
}
