<?php
/**
 * Export Sla Excel Controller
 *
 * @category Arb
 * @package Arb_Sla
 * @author Arb Magento Team
 *
 */

namespace Arb\Sla\Controller\Adminhtml\Report\Sales;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultInterface;
use Magento\Reports\Controller\Adminhtml\Report\Sales;
use Arb\Sla\Block\Adminhtml\Sales\Slareport\Grid;
use Exception;

class ExportSlaReportExcel extends Sales
{
    /**
     * @return ResponseInterface|ResultInterface
     *
     * @throws Exception
     */
    public function execute()
    {
        $fileName = 'slareport.xml';
        $grid = $this->_view->getLayout()->createBlock(Grid::class);
        $this->_initReportAction($grid);

        return $this->_fileFactory->create($fileName, $grid->getExcelFile($fileName), DirectoryList::VAR_DIR);
    }
}
