<?php

namespace Arb\Customer\Test\Unit\Plugin\Magento\Customer\Model\ResourceModel;

use Arb\Customer\Plugin\Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Model\Data\Customer;
use Magento\Customer\Model\ResourceModel\CustomerRepository as MagentoCustomerRepository;
use Magento\Framework\Api\AttributeInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;

class CustomerRepositoryTest extends TestCase
{
    /**
     * Object to test
     *
     * @var CustomerRepository
     */
    private $testObject;

    /**
     * @var MagentoCustomerRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerRepositoryMock;

    /**
     * @var Customer|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerMock;

    /**
     * @throws ReflectionException
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->customerRepositoryMock = $this->createMock(MagentoCustomerRepository::class);
        $this->customerMock = $this->createMock(Customer::class);

        $this->testObject = $objectManager->getObject(CustomerRepository::class, []);
    }

    /**
     * @dataProvider testBeforeSaveProvider
     *
     * @param AttributeInterface|null $attribute
     * @param int $expectedTitleValue
     *
     * @throws ReflectionException
     */
    public function testBeforeSave(?AttributeInterface $attribute, int $expectedTitleValue)
    {
        $addressMock = $this->createMock(AddressInterface::class);
        $this->customerMock->expects($this->once())->method('getAddresses')->willReturn([$addressMock]);

        $addressMock->expects($this->once())->method('getCustomAttribute')->willReturn($attribute);

        $addressMock->expects($this->exactly($expectedTitleValue))->method('getStreet')->willReturn('street name');
        $addressMock->expects($this->exactly($expectedTitleValue))->method('setCustomAttribute')->willReturnSelf();

        $this->testObject->beforeSave($this->customerRepositoryMock, $this->customerMock);
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function testBeforeSaveProvider()
    {
        $attributeMock = $this->createMock(AttributeInterface::class);
        return [
            [$attributeMock, 0],
            [null, 1]
        ];
    }
}
