<?php
/**
 * Data patch responsible for creating title attribute for address
 *
 * @category Arb
 * @package Arb_Customer
 * @author Arb Magento Team
 *
 */

namespace Arb\Customer\Setup\Patch\Data;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Data patch responsible for creating title attribute for address
 */
class CreateAddressTitleAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * CreateAddressTitleAttribute constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param Config $eavConfig
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        Config $eavConfig,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return DataPatchInterface|void
     * @throws LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create();

        $eavSetup->addAttribute('customer_address', 'title', [
            'type' => 'varchar',
            'input' => 'text',
            'label' => 'Title',
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'system'=> false,
            'group'=> 'General',
            'global' => true,
            'visible_on_front' => true
        ]);

        $customAttribute = $this->eavConfig->getAttribute('customer_address', 'title');

        $customAttribute->setData(
            'used_in_forms',
            ['adminhtml_customer_address','customer_address_edit','customer_register_address']
        );

        $customAttribute->save();

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
