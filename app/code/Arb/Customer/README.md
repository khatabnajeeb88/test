## Synopsis
This is module used for Order API customization and depends on module Magento_Sales, Magento_Eav, Magento_Catalog, Magento_Quote

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).