<?php
/**
 * Plugin class responsible for assigning title attribute if it is not provided
 *
 * @category Arb
 * @package Arb_Customer
 * @author Arb Magento Team
 *
 */

namespace Arb\Customer\Plugin\Magento\Customer\Model\ResourceModel;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Model\Data\Customer;
use Magento\Customer\Model\ResourceModel\CustomerRepository as MagentoCustomerRepository;

/**
 * Plugin for CustomerRepository class
 */
class CustomerRepository
{
    /**
     * @param MagentoCustomerRepository $customerRepository
     * @param Customer $customerModel
     *
     * @return void
     */
    public function beforeSave(MagentoCustomerRepository $customerRepository, Customer $customerModel)
    {
        $addresses = $customerModel->getAddresses();

        if (!empty($addresses)) {
            /** @var AddressInterface $address */
            foreach ($addresses as $address) {
                $title = $address->getCustomAttribute('title');
                if ($title === null) {
                    $titleValue = $address->getStreet();
                    $address->setCustomAttribute('title', $titleValue);
                }
            }
        }
    }
}
