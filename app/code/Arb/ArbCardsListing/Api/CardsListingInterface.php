<?php
/**
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 */
namespace Arb\ArbCardsListing\Api;

interface CardsListingInterface
{

    /**
     * GET for GET api
     * @param string    $request
     * @param  string   $cardtype
     * @param  string   $searchCriteria
     * @return string
     */

    public function getCards($request='creditcard', $cardtype='credit', $searchCriteria="0,1");
}
