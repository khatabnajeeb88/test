<?php
/**
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 */
namespace Arb\ArbCardsListing\Model\ResourceModel;


class Cards extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Cards constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     *
     * @return $this
     */
    protected function _construct()
    {
        $this->_init('arb_cards', 'id');
    }

}
