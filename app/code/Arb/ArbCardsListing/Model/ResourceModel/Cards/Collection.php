<?php
/**
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 */


namespace Arb\ArbCardsListing\Model\ResourceModel\Cards;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'arb_cards_collection';
    protected $_eventObject = 'cards_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Arb\ArbCardslisting\Model\Cards::class,
            \Arb\ArbCardslisting\Model\ResourceModel\Cards::class);

    }

}
