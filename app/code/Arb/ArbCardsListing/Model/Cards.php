<?php
/**
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 */

namespace Arb\ArbCardsListing\Model;

class Cards extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'arb_cards';

    protected $_cacheTag = 'arb_cards';

    protected $_eventPrefix = 'arb_cards';

    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Arb\ArbCardsListing\Model\ResourceModel\Cards::class
        );
    }


    /**
     * get Identities from Cache.
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


}
