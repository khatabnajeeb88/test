<?php
/**
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 */

namespace Arb\ArbCardsListing\Model;

use Arb\ArbCardsListing\Api\CardsListingInterface;
use Arb\ArbCardsListing\Model\ResourceModel\Cards\CollectionFactory as CardsCollectionFactory;
use Arb\ArbPayment\Model\Encryption;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Webapi\Request;
use Magento\Integration\Model\Oauth\TokenFactory;

/**
 *
 * @codeCoverageIgnore
 */

class CardsListing implements CardsListingInterface
{
    /**
     * @var TokenFactory
     */
    private $tokenModelFactory;
    /**
     * @var CardsFactory
     */
    private $cardsFactory;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var CardsCollectionFactory
     */
    private $cardsCollectionFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;
    /**
     * @var Encryption
     */
    private $encryption;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var Json
     */
    private $serializer;
    /**
     * @var \Arb\ArbCardsListing\Helper\Data
     */
    private $helper;

    const CREDIT_CARD = "creditcard";

    const DEBIT_CARD = "debitcard";

    /**
     * CardsListing constructor.
     * @param TokenFactory $tokenModelFactory
     * @param CardsCollectionFactory $cardsCollectionFactory
     * @param Request $request
     * @param CustomerRepositoryInterface $customerRepository
     * @param CardsFactory $cardsFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param Encryption $encryption
     * @param Json $serializer
     * @param \Arb\ArbCardsListing\Helper\Data $helper
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        TokenFactory $tokenModelFactory,
        CardsCollectionFactory  $cardsCollectionFactory,
        Request $request,
        CustomerRepositoryInterface $customerRepository,
        CardsFactory $cardsFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        Encryption $encryption,
        Json $serializer,
        \Arb\ArbCardsListing\Helper\Data $helper,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->tokenModelFactory = $tokenModelFactory;
        $this->request = $request;
        $this->customerRepository = $customerRepository;
        $this->cardsCollectionFactory = $cardsCollectionFactory;
        $this->dateTime = $dateTime;
        $this->cardsFactory = $cardsFactory;
        $this->encryption = $encryption;
        $this->scopeConfig = $scopeConfig;
        $this->serializer = $serializer;
        $this->helper = $helper;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/CardsListing_Log-' . date("Ymd") . '.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * @param string $request
     * @param string $cardtype
     * @param int $searchCriteria
     * @return array|string
     * @throws LocalizedException
     */
    public function getCards($request= 'creditcard', $cardtype ='credit', $searchCriteria="0,1")
    {
        $this->logger->info("Start GetCardList request:'" . $request. "' - CardType:' " . $cardtype . "' -  SarchCriteria:' " . $searchCriteria . "' ");
        //Authorize User Token.
        $authorizationHeaderValue = $this->request->getHeader('Authorization');

        if (!$authorizationHeaderValue) {
            $this->logger->info("Can't Authorize the user");
            throw new LocalizedException(__('Token Invalid.'));
        }

        $cic_number = $this->isValidUser($authorizationHeaderValue);
        if (empty($cic_number)) {
            $this->logger->info("The user is Authorized and CIC is empty");
            throw new LocalizedException(__("Token Invalid/CIC Not Found."));
        }

        $this->logger->info("The user is Authorized and CIC : '" . $cic_number . "' ");
        //End Authorization.

        $resourceKey = $this->scopeConfig->getValue(
            'order/general/resourcekey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $vectorinit = $this->scopeConfig->getValue(
            'order/general/vector_init',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $this->logger->info("Get Config values ResourceKey '" . $resourceKey . "' and Vectorinit: '" . $vectorinit . "' for CIC : '" . $cic_number . "' and Request:' " . $request . "' ");

        // get Cards from arb_cards table
        $isCacheExitsForCic = $this->getCardsfromCache($cic_number, $request);
        if (!empty($isCacheExitsForCic)) {
            $this->logger->info("Found: Get Cards: '" . $request . "' from Caching table for CIC : '" . $cic_number . "' ");

            return [$isCacheExitsForCic[0]["cards_data"]];
        } else {
            $this->logger->info(" No cached data available for this Cards: '" . $request . "' and CIC : '" . $cic_number . "' ");
            /** @var $cic_number $request for deleteCardsFromCache */
            $this->deleteCardsFromCache($cic_number, $request);

            /** @var TYPE_NAME $expiry_time_from_config */
            $expiry_time_from_config = $this->scopeConfig->getValue(
                "cardslisting/cache_expiry_time/hours",
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            /** @var TYPE_NAME $expiry_time_from_config */
            $expiry_time_from_config =   ($expiry_time_from_config>0) ? $expiry_time_from_config : 24;

            $now = new \DateTime();
            $now->add(new \DateInterval("PT" . $expiry_time_from_config . "M"));
            /** @var TYPE_NAME $cache_expiry_date */
            $cache_expiry_date =   $now->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT);

            $data_from_esb =[];

            /** @var TYPE_NAME $request */
            if ($request == self::CREDIT_CARD) {
                $this->logger->info("try to get cards from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ");
                $cardsFromESB= $this->helper->getCreditCardsListingFromESB($cic_number);

                if (isset($cardsFromESB['CreditCardLstItem']['0'])) {
                    $cardsFromESB =  $cardsFromESB['CreditCardLstItem'];
                }
                // TODO delete
                $this->logger->info("Cards list raw from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ::-> ");
                $this->logger->info($cardsFromESB);
                //Filter card array based on Configuration
                $credit_card_filter = $this->scopeConfig->getValue(
                    "cardslisting/credit_cards/credit_card_status",
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );

                // Processing the card data based on filters
                if (!empty($credit_card_filter)) {
                    $card_filter_ar = strpos($credit_card_filter, ',') !== false ? explode(',', $credit_card_filter) : [$credit_card_filter];

                    $cards_data=[];
                    if (!empty($cardsFromESB)) {
                        foreach ($card_filter_ar as $category) {
                            $cards_data[] =  array_filter($cardsFromESB, function ($item) use ($category) {
                                return trim($item['CardFullStatus']) == $category;
                            });
                        }
                        $cards_data = array_filter(array_map('array_filter', $cards_data));
                        // maping the required cards data
                        foreach ($cards_data as $cardData) {
                            foreach ($cardData as $data) {
                                $cards_before_encrption =$this->setCardsData($data, $request);
                                $data_from_esb[]   = $cards_before_encrption;
                            }
                        }
                    }
                } else {
                    $cards_data =$cardsFromESB;
                    if (!empty($cards_data)) {
                        foreach ($cards_data as $cardData) {
                            $cards_before_encrption =$this->setCardsData($cardData, $request);
                            $data_from_esb[]   = $cards_before_encrption;
                        }
                    }
                }
                // TODO delete
                $this->logger->info("Cards list from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ::-> ");
                $this->logger->info($data_from_esb);
            } elseif ($request == self::DEBIT_CARD) {
                $this->logger->info("Try to get cards from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ");
                $cardsFromESB= $this->helper->getDebitCardsListingFromESB($cic_number);
                $cardsFromESB = isset($cardsFromESB["ATMCardsLst"]["CardDtls"][0]) ? $cardsFromESB["ATMCardsLst"]["CardDtls"] : $cardsFromESB["ATMCardsLst"];

                // TODO delete
                $this->logger->info("Cards list raw from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ::-> ");
                $this->logger->info($cardsFromESB);

                //Filter card array based on Configuration
                $debit_card_filter = $this->scopeConfig->getValue(
                    "cardslisting/debit_cards/debit_card_status",
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                // Processing the card data based on filters
                if (!empty($debit_card_filter)) {
                    $debit_card_filter = strpos($debit_card_filter, ',') !== false ? explode(',', $debit_card_filter) : [$debit_card_filter];
                    $cards_data=[];
                    if (!empty($cardsFromESB)) {
                        foreach ($debit_card_filter as $category) {
                            $cards_data[]=  array_filter($cardsFromESB, function ($item) use ($category) {
                                return trim($item['alrCardStatus']) == $category;
                            });
                        }
                        $cards_data = array_filter(array_map('array_filter', $cards_data));
                        // maping the required cards data
                        foreach ($cards_data as $cardData) {
                            foreach ($cardData as $data) {
                                $cards_before_encrption =$this->setCardsData($data, $request);
                                $data_from_esb[]   = $cards_before_encrption;
                            }
                        }
                    }
                } else {
                    $cards_data =$cardsFromESB;
                    if (!empty($cards_data)) {
                        foreach ($cards_data as $cardData) {
                            $cards_before_encrption =$this->setCardsData($cardData, $request);
                            $data_from_esb[]   = $cards_before_encrption;
                        }
                    }
                }
                //TODO delete
                $this->logger->info("Cards list from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ::-> ");
                $this->logger->info($data_from_esb);
            } else {
                // TODO delete
                $this->logger->info("Cards list from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ::-> Exception-Invalid Request");
                throw new LocalizedException(__('Invalid Request.'));
            }

            if (!empty($data_from_esb)) {
                $jsonEncryptedData = $this->serializer->serialize($data_from_esb);
                $EncodeData = urlencode($jsonEncryptedData);
                $encdata = $this->encryption->encryptAES(
                    $EncodeData,
                    $resourceKey,
                    $vectorinit,
                    'cardListing'
                );

                $final_array =  [
                    "card_type"         =>  $request,
                    "cic_number"        =>  $cic_number,
                    "cards_data"        =>  $encdata,
                    "cache_expires_at"  =>  $cache_expiry_date
                ];
                $this->logger->info("Found: cards returned from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ");
                $this->logger->info("Found: cards returned from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "' - encrypted data ::->  ");// TODO: delete
                $this->logger->info(" . $encdata . "); // TODO: delete
                $this->saveCardsinCache($final_array);
                $response = $final_array["cards_data"];
                return [$response];
            } else {
                $this->logger->info("No cards returned from ESB for CIC: '" . $cic_number . "' and card : '" . $request . "'  ");
                return [];
            }
        }

    }

    /**
     * @param $cardData
     * @param $type
     * @return array
     */
    public function setCardsData($cardData, $type)
    {
        $encryptedData = [];
        if ($type==self::CREDIT_CARD) {
            $encryptedData['CardSeqNumber']         =   $cardData['CardSeqNumber'];
            $encryptedData['CardNumber']            =   $cardData['CardNumber'];
            $encryptedData['CreditCardType']        =   $cardData['CreditCardType'];
            $encryptedData['EmbossingName']         =   $cardData['EmbossingName'];
            $encryptedData['CardStatus']            =   $cardData['CardStatus'];
            $encryptedData['CardExpDate']           =   $cardData['CardExpDate'];
            $encryptedData['AvailableCredit']       =   $cardData['AvailableCredit'];
        } elseif ($type == self::DEBIT_CARD) {
            $encryptedData['CardSeqNumber']         =   $cardData['alrCardSeqNum'];
            $encryptedData['CardNumber']            =   $cardData['alrCardNum'];
            $encryptedData['EmbossingName']         =   $cardData['alrEmbossingName'];
            $encryptedData['CardType']              =   $cardData['alrCardType'];
            $encryptedData['CreditCardType']        =   $cardData['alrCardStatus'];
            $encryptedData['CardExpDate']           =   $cardData['alrExpiryDate'];
            $encryptedData['AvailableCredit']       =   "";
        }

        return $encryptedData;
    }

    /**
     * @param $authorizationHeaderValue
     * @return mixed
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function isValidUser($authorizationHeaderValue)
    {
        $headerPieces = explode(" ", $authorizationHeaderValue);
        if (count($headerPieces) !== 2) {
            $this->logger->info("Try to Get customer token - Exception 'Token Invalid' ");
            throw new LocalizedException(__('Token Invalid'));
        }
        $tokenType = strtolower($headerPieces[0]);
        if ($tokenType !== 'bearer') {
            $this->logger->info("Try to Get customer token - bearer - Exception-Token Invalid ");
            throw new LocalizedException(__('Token Invalid'));
        }
        $customerToken = $headerPieces[1];
        //Validate Token
        $customerOuthModel = $this->tokenModelFactory->create();
        $customerOuthModel->load($customerToken, "token");
        $outhCustomerId = $customerOuthModel->getCustomerId();

        // Getting customer ID && CIC number
        if (!empty($outhCustomerId)) {
            $customer = $this->customerRepository->getById($outhCustomerId);

            if (!empty($customer->getCustomAttribute('cic'))) {
                return  $customer->getCustomAttribute('cic')->getValue();
            } else {
                $this->logger->info("Try to Get customer: '" . $outhCustomerId . "' - Exception-CIC  Invalid/Missing not available. ");
                throw new LocalizedException(__('CIC  Invalid/Missing not available.'));
            }
        }

        if (empty($outhCustomerId)) {
            $this->logger->info("Try to Get customer: '" . $outhCustomerId . "' - Exception-Token Invalid ");
            throw new LocalizedException(__('Token Invalid'));
        }
        if (!empty($customerOuthModel->getRevoked())) {
            $this->logger->info("Try to Get customer CIC number: '" . $outhCustomerId . "' - Exception-Token Invalid ");
            throw new LocalizedException(__('Token Invalid'));
        }
    }
    /**
     * @return json array
     */

    public function getCardsfromCache($cicnumber, $request)
    {
        $this->logger->info("Try to Get Cards: '" . $request . "' from Caching table for CIC : '" . $cicnumber . "' ");
        $now = new \DateTime();
        try {
            $cards = $this->cardsFactory->create();
            $collection = $cards->getCollection()->addFieldToSelect('cards_data')
                ->addFieldTofilter('cic_number', ['eq' => $cicnumber])
                ->addFieldTofilter('card_type', ['eq' => $request])
                ->addFieldToFilter('cache_expires_at', ['gteq' => $now->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT)]);
            return $collection->getData();
        } catch (\Exception $e) {
            $this->logger->info("Try to Get Cards: '" . $request . "' from Caching table for CIC : '" . $cicnumber . "' - Exception '" . $e . "' ");
        }
    }

    /**
     * @param $data
     * @return Cards
     * @throws LocalizedException
     */
    public function saveCardsinCache($data)
    {
        try {
            $this->logger->info("try to Save cards : '" . $data['card_type'] . "' to cache table for CIC : '" . $data['cic_number'] . "' - start");
            $model = $this->cardsFactory->create();
            $model->addData([
                "card_type"         =>  $data['card_type'],
                "cic_number"        =>  $data['cic_number'],
                "cards_data"        =>  $data['cards_data'],
                "cache_expires_at"  =>  $data['cache_expires_at']
            ]);
            $saveToCache = $model->save();
            if($saveToCache) {
                $this->logger->info("try to Save cards : '" . $data['card_type'] . "' to cache table for CIC : '" . $data['cic_number'] . "' - data saved ");
            }else{
                $this->logger->info("try to Save cards : '" . $data['card_type'] . "' to cache table for CIC : '" . $data['cic_number'] . "' - data not saved ");
            }
            return $saveToCache;
        } catch (\Exception $e) {
            $this->logger->info(" try to Save cards : '" . $data['card_type'] . "' to cache table for CIC : '" . $data['cic_number'] . "' - Exception-Something went wrong, Please try again later: '$e' ");
            throw new LocalizedException(__('Something went wrong, Please try again later.'));
        }
    }

    /**
     * @param $cicnumber
     * @param $card_type
     * @throws LocalizedException
     */
    public function deleteCardsFromCache($cicnumber, $card_type)
    {
        try {
            $this->logger->info(" try to Delete cache for : '$card_type' and CIC : '$cicnumber'");
            $collection = $this->cardsCollectionFactory->create();

            $collection->addFieldToSelect('id');
            $collection->addFieldTofilter('cic_number', ['eq' => $cicnumber]);
            $collection->addFieldTofilter('card_type', ['eq' => $card_type]);
            $collection->walk('delete');
        } catch (\Exception $e) {
            $this->logger->info("Delete cache for : '" . $card_type . "' and CIC : '" . $cicnumber . "' - Exception-Something went wrong in delete from cards cache: '" . $e . "' ");
            throw new LocalizedException(__('Something went wrong in delete from cards cache.'));
        }
    }
}
