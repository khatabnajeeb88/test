<?php
/**
 * CardsTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_Banners
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbCardsListing\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class CardsTest for testing  Cards class
 * @covers Arb\ArbCardsListing\Model\Cards
 */
class CardsTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject("Arb\ArbCardsListing\Model\Cards");
    }

    /**
     * testGetIdentities method
     */
    public function testGetIdentities()
    {
        $status = $this->model->getIdentities();
        $this->assertEquals(["0" => "arb_cards_"], $status);    }
}
