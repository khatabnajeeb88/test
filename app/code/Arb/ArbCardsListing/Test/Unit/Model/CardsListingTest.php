<?php
/**
 * CardsListingTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 *
 */

namespace Arb\ArbCardsListing\Test\Unit\Model;

use Arb\ArbPayment\Model\Encryption;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\Webapi\Request;
use Magento\Integration\Model\Oauth\TokenFactory;
use PHPUnit\Framework\TestCase;
use Arb\ArbCardsListing\Model\CardsListing as CardsListing;


/**
 * @covers \Arb\ArbCardsListing\Model\CardsListing
 */
class CardsListingTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject |\PHPUnit_Framework_MockObject_MockObject
     */
    protected $collectionFactoryMock;

    /**
     * @var \Arb\ArbCardsListing\Model\CardsFactory |\PHPUnit_Framework_MockObject_MockObject
     */
    private $_cardsFactoryMock;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $_scopeConfigMock;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    private $testObject;

    /**
     *
     */
    public function setUp()
    {
        $this->_dateTimeMock = $this->createMock(DateTime::class);
        $this->objectManager = new ObjectManager($this);

        $this->_cardsFactoryMock =  $this->getMockBuilder(\Arb\ArbCardsListing\Model\CardsFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'create',
                "getCollection",
                "addFieldToFilter",
                "addFieldToSelect",
                "getData",
                "addData",
                "save"

            ])
            ->getMock();

        $this->collectionFactoryMock =  $this->getMockBuilder(\Arb\ArbCardsListing\Model\ResourceModel\Cards\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'create',
                "addFieldToFilter",
                "addFieldToSelect",
                "walk"

            ])
            ->getMock();
        $this->_cardsFactoryMock->expects($this->any())->method('create')->willReturnSelf();
        $this->_cardsFactoryMock->expects($this->any())->method('getCollection')->willReturnSelf();
        $this->_cardsFactoryMock->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $this->_cardsFactoryMock->expects($this->any())->method('addFieldToSelect')->willReturnSelf();
        $this->_cardsFactoryMock->expects($this->any())->method('getData')->willReturnSelf();
        $this->_cardsFactoryMock->expects($this->any())->method('addData')->willReturnSelf();
        $this->_cardsFactoryMock->expects($this->any())->method('save')->willReturnSelf();

        $this->tokenModelFactoryMock = $this->getMockBuilder(TokenFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([
                "create",
                "load",
                "getCustomerId",
                "getRevoked"
            ])
            ->getMock();
        $this->tokenModelFactoryMock->method("create")->willReturnSelf();
        $this->tokenModelFactoryMock->method("load")->willReturnSelf();
        $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
        $this->encryptionMock = $this->getMockBuilder(Encryption::class)
            ->disableOriginalConstructor()
            ->setMethods(['decryptAES',"encryptAES"])
            ->getMock();
        $this->requestMock =$this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods([

            ])
            ->getMock();
        $this->_scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->helperMock = $this->createMock(\Arb\ArbCardsListing\Helper\Data::class);
        $this->_serializationMock = $this->getMockBuilder(Json::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'unserialize',
                'serialize'
            ])
            ->getMock();
        $this->_customerRepositoryMock = $this->getMockBuilder(\Magento\Customer\Api\CustomerRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["get","getId","getCustomAttribute","getValue"])
            ->getMockForAbstractClass();

        $this->loggerMock = $this->createMock(\Psr\Log\LoggerInterface::class);

        $this->testObject = $this->objectManager->getObject(
            CardsListing::class,
            [
                "tokenModelFactory"         =>  $this->tokenModelFactoryMock,
                "cardsCollectionFactory"    =>  $this->collectionFactoryMock,
                "request"                   =>  $this->requestMock,
                "customerRepository"        =>  $this->_customerRepositoryMock,
                "cardsFactory"              =>  $this->_cardsFactoryMock,
                "dateTime"                  =>  $this->_dateTimeMock,
                "encryption"                =>  $this->encryptionMock,
                "serializer"                =>  $this->_serializationMock,
                "helper"                    =>  $this->helperMock,
                "scopeConfig"               =>  $this->_scopeConfigMock

            ]
        );
    }


    public function testgetCardsfromCache()
    {
        $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
        $result = $this->testObject->getCardsfromCache('0000000003044750', 'creditcard');
        $this->assertNotEmpty($result);
    }


    public function testSaveCardsinCache()
    {
        $result = $this->testObject->saveCardsinCache(
            [ 'card_type'=>'credit',
            'cic_number'=>'0000000003044750',
            'cards_data'=> '["c90a7a7970474f8c9dfdf1303d75331731a5cf6617ded9faa01248c914a8129f411487"]',
            'cache_expires_at'=>'2020-06-30 23:32:27']
        );
        $this->assertNotEmpty($result);
    }


    public function testDeleteCardsFromCache()
    {
        $cic = "0000000003044750";
        $card_type = "creditcard";

        $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
        $this->collectionFactoryMock->expects($this->any())->method('create')->willReturnSelf();
        $this->collectionFactoryMock->expects($this->any())->method('addFieldToSelect')->willReturnSelf();
        $this->collectionFactoryMock->expects($this->any())->method('addFieldToFilter')->willReturnSelf();
        $this->collectionFactoryMock->expects($this->any())->method('walk')->willReturnSelf();
        $result = $this->testObject->deleteCardsFromCache($cic, $card_type);
        $this->assertNull($result);
    }
}
