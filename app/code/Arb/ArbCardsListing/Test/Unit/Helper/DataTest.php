<?php
/**
 * DataTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 *
 */
namespace Arb\ArbCardsListing\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\ArbCardsListing\Helper\Data as ArbHelper;
 use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Math\Random;
use Arb\ArbPayment\Model\Encryption;
/**
 * Class DataTest for testing  ArbCardsListing class
 * @covers \Arb\ArbCardsListing\Helper\Data
 */
class DataTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(Context::class);
        $this->_dateTimeMock = $this->createMock(DateTime::class);
        $this->_scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);
        $this->context->expects($this->any())->method('getScopeConfig')->willReturn($this->_scopeConfigMock);
        $this->_curlMock = $this->getMockBuilder(Curl::class)->getMock();
        $this->_randomMock = $this->createMock(Random::class);
        $this->encryptionMock = $this->getMockBuilder(Encryption::class)
            ->disableOriginalConstructor()
            ->setMethods(['decryptAES',"encryptAES"])
            ->getMock();


        $this->_arbHelper = new ArbHelper(
            $this->context,
            $this->_dateTimeMock,
            $this->_curlMock,
            $this->_randomMock,
            $this->encryptionMock
        );
    }


    /**
     * testGetCreditCardsListingFromESB method
     */
    public function testGetCreditCardsListingFromESB()
    {
        $xml ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                        <soapenv:Body>
                        <acc:CreditCardsLstInqRs xmlns:acc="http://www.alrajhiwebservices.com/AlRajhiCreditCards" xmlns:alr="http://www.alrajhiwebservices.com/">
                         <Hdr>
                            <alr:Status>
                               <alr:StatusCd>I000000</alr:StatusCd>
                               <alr:StatusDesc>Successful Operation</alr:StatusDesc>
                            </alr:Status>
                            <alr:RqID>a6e03ba1-38b4-4159-b89b-588b71c5c93a</alr:RqID>
                         </Hdr>
                         <Body>
                            <RecPgCtrlOut>
                               <alr:SentRecs>40</alr:SentRecs>
                               <alr:MatchedRecs>40</alr:MatchedRecs>
                               <alr:ComplFlg>Y</alr:ComplFlg>
                            </RecPgCtrlOut>
                            <CreditCardLst>
                              <CreditCardLstItem>
                                  <CardSeqNumber>10547652</CardSeqNumber>
                                  <CardNumber>445827******6241</CardNumber>
                                  <CardNickName/>
                                  <EmbossingName>Branch Code Change</EmbossingName>
                                  <FirstName>ش​</FirstName>
                                  <LastName>ﻗﻬﻓﺛﺳﺎ</LastName>
                                  <CardExpDate>2022-01-31</CardExpDate>
                                  <SibAccountNumber>204000010006083044759</SibAccountNumber>
                                  <CardAccount>197647******4380</CardAccount>
                                  <AddressSeqNumber>9320012</AddressSeqNumber>
                                  <CreditCardType>2</CreditCardType>
                                  <ShowStatusFlg>S</ShowStatusFlg>
                                  <PayPalFlg>N</PayPalFlg>
                                  <RewardPoints>0</RewardPoints>
                                  <CardIndicator>P</CardIndicator>
                                  <CardFullStatus>NORI</CardFullStatus>
                                  <eStatementFlg>N</eStatementFlg>
                                  <FavouriteFlg>N</FavouriteFlg>
                                  <AvailableCash>0.00</AvailableCash>
                                  <AvailableCredit>0.00</AvailableCredit>
                                  <ConsumedLimit>0.00</ConsumedLimit>
                                  <DueAmount>0.00</DueAmount>
                                  <DueDate>2020-09-26</DueDate>
                                  <CardStatus>3</CardStatus>
                                  <CRLimit>0.00</CRLimit>
                                  <StmtAmt>0.00</StmtAmt>
                                  <UnbilledAmt>0.00</UnbilledAmt>
                                  <TotalAmt>0.00</TotalAmt>
                                  <ProdCode>00088</ProdCode>
                                  <ProdDesc>VISA VIRTUAL</ProdDesc>
                                  <ApplePayStatus>ELIGIBLE</ApplePayStatus>
                               </CreditCardLstItem>
                          </CreditCardLst>
                         </Body>
                        </acc:CreditCardsLstInqRs>
                        </soapenv:Body>
                        </soapenv:Envelope>';


        $this->_curlMock->method("getBody")->willReturn($xml);
        $this->_scopeConfigMock->expects($this->any())
            ->method('getValue')
            ->with("cardslisting/credit_cards_url/credit_end_point")
            ->willReturn("http://172.21.160.248:7905/AlRajhiCreditCards");
        $cic_number =  '0000000003044750';
        $this->_arbHelper->getCreditCardsListingFromESB($cic_number);

    }

    public function testGetDebitCardsListingFromESB(){
        $xml ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                <soapenv:Body>
                  <alr:getCardListRs xmlns:alr="http://www.alrajhiwebservices.com/AlRajhiDebitCards" xmlns:alr1="http://www.alrajhiwebservices.com/">
                     <Hdr>
                        <alr1:Status>
                           <alr1:StatusCd>I000000</alr1:StatusCd>
                           <alr1:StatusDesc>عملية ناجحة</alr1:StatusDesc>
                        </alr1:Status>
                        <alr1:RqID>800f48ec-a0fc-dea9-bf0d-e8fa33aeb338</alr1:RqID>
                     </Hdr>
                     <Body>
                        <alr:RecPgCtrlOut>
                           <alr1:SentRecs>5</alr1:SentRecs>
                           <alr1:MatchedRecs>9</alr1:MatchedRecs>
                           <alr1:ComplFlg>N</alr1:ComplFlg>
                        </alr:RecPgCtrlOut>
                        <ATMCardsLst>
                            <CardDtls>
                              <alr:CardNum>484783******4172</alr:CardNum>
                              <alr:CardSeqNum>0000002040001796</alr:CardSeqNum>
                              <alr:CardStatus>1</alr:CardStatus>
                              <alr:SecureFlg>Y</alr:SecureFlg>
                              <alr:AcctNum>204000010006085454873</alr:AcctNum>
                              <alr:ProdType>02</alr:ProdType>
                              <alr:ExpiryDate>201907</alr:ExpiryDate>
                              <alr:CardType>33</alr:CardType>
                              <alr:CardClass>2205</alr:CardClass>
                              <alr:EmbossingName>3044744DC6085454873</alr:EmbossingName>
                           </CardDtls>
                        </ATMCardsLst>
                     </Body>
                  </alr:getCardListRs>
                </soapenv:Body>
                </soapenv:Envelope>';
        $this->_curlMock->method("getBody")->willReturn($xml);
        $this->_scopeConfigMock->expects($this->any())
            ->method('getValue')
            ->with("cardslisting/debit_cards_url/debit_end_point")
            ->willReturn("http://172.21.160.248:7909/AlRajhiDebitCards");
        $cic_number =  '0000000003044744';
        $this->_arbHelper->getDebitCardsListingFromESB($cic_number);


    }



}
