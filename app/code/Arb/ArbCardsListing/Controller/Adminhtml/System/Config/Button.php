<?php
/**
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 */

namespace Arb\ArbCardsListing\Controller\Adminhtml\System\Config;

use Arb\ArbCardsListing\Model\CardsFactory;
use Arb\ArbCardsListing\Model\ResourceModel\Cards\CollectionFactory as CardsCollectionFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

/**
 *
 * @codeCoverageIgnore
 */
class Button extends Action
{

    /**
     * @var CardsCollectionFactory
     */
    private $cardsCollectionFactory;

    /**
     * @var CardsFactory
     */
    private $cardsFactory;

    /**
     * Button constructor.
     * @param Context $context
     * @param CardsCollectionFactory $cardsCollectionFactory
     */
    public function __construct(
        Context $context,
        CardsCollectionFactory  $cardsCollectionFactory,
        CardsFactory $cardsFactory
    ) {
        parent::__construct($context);
        $this->cardsCollectionFactory = $cardsCollectionFactory;
        $this->cardsFactory = $cardsFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        try {
            $cards = $this->cardsFactory->create();
            $collection = $cards->getCollection();
            $collection->walk('delete');

        } catch (\Exception $e) {
            throw new LocalizedException(__('Something went wrong.'));
        }
    }
}
