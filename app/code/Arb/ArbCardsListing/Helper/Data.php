<?php
/**
 * @category Arb
 * @package Arb_ArbCardsListing
 * @author Arb Magento Team
 */
namespace Arb\ArbCardsListing\Helper;

use Arb\ArbPayment\Model\Encryption;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Math\Random;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * ArbCardListing data helper
 *
 * @api
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MSG_VER = "004";

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @var Curl
     */
    protected $_curl;

    /**
     * @var Random
     */
    protected $_random;

    /**
     * Default code for sucessful email sent
     */
    const DEFAULT_SUCCESS_CODE = "I000000";

    /**
     * Input string for generating random string
     */
    const RANDOM_STRING = "abcdef0123456789";

    /**
     * Svc id for ESB templete
     */
    const CREDIT_SVC_ID = "0007";

    /**
     *  Sub Svc id for ESB templete
     */
    const CREDIT_SUBSVC_ID = "0100";

    /**
     * Funcation id for ESB templete
     */
    const CREDIT_FUN_ID = "0002";

    /**
     * Svc id for ESB templete
     */
    const DEBIT_SVC_ID = "0006";

    /**
     *  Sub Svc id for ESB templete
     */
    const DEBIT_SUBSVC_ID = "0100";

    /**
     * Funcation id for ESB templete
     */
    const DEBIT_FUN_ID = "0006";

    /**
     * OSID ESB templete
     */
    const OSID = "01";

    /**
     * @var Encryption
     */
    private $encryption;

    /**
     * @param Context $context
     * @param DateTime $_dateTime
     * @param Curl $_curl
     * @param Random $_random
     * @param Encryption $encryption
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        DateTime $_dateTime,
        Curl $_curl,
        Random $_random,
        Encryption $encryption
    ) {
        $this->_dateTime = $_dateTime;
        $this->_curl = $_curl;
        $this->_random = $_random;
        $this->_storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/CardsListing_Log-' . date("Ymd") . '.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        parent::__construct($context);
        $this->encryption = $encryption;
    }

    /**
     * @param array $data
     * @return false|string
     */
    public function getCreditCardsListingFromESB($cicnumber)
    {
        $endPoint = $this->scopeConfig->getValue("cardslisting/credit_cards_url/credit_end_point", $this->_storeScope);
        /** @var TYPE_NAME $xmlRequest */
        $xmlRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                        <soapenv:Header/>
                        <soapenv:Body>
                          <ns3:CreditCardsLstInqRq xmlns:ns3="http://www.alrajhiwebservices.com/AlRajhiCreditCards" xmlns:ns2="http://www.alrajhiwebservices.com/">
                             <Hdr>
                                <ns2:Msg>
                                   <ns2:RqID>' . $this->getRandomString() . '</ns2:RqID>
                                   <ns2:SvcID>' . self::CREDIT_SVC_ID . '</ns2:SvcID>
                                   <ns2:SubSvcID>' . self::CREDIT_SUBSVC_ID . '</ns2:SubSvcID>
                                   <ns2:FuncID>' . self::CREDIT_FUN_ID . '</ns2:FuncID>
                                   <ns2:MsgTimestamp>' . str_replace(" ", "T", $this->_dateTime->date()) . '</ns2:MsgTimestamp>
                                   <ns2:MsgVer>' . self::MSG_VER . '</ns2:MsgVer>
                                </ns2:Msg>
                                <ns2:Agt>
                                   <ns2:CICNum>' . $cicnumber . '</ns2:CICNum>
                               </ns2:Agt>
                                <ns2:Sys>
                                   <ns2:ChID>MOBILEWEB</ns2:ChID>
                                <ns2:OSID>' . self::OSID . '</ns2:OSID>
                               </ns2:Sys>
                             </Hdr>
                             <Body>
                                <InquiryOption>SHOW</InquiryOption>
                                <RecPgCtrlIn>
                                   <ns2:MaxRecs>5</ns2:MaxRecs>
                                   <ns2:Offset>1</ns2:Offset>
                                </RecPgCtrlIn>
                             </Body>
                          </ns3:CreditCardsLstInqRq>
                        </soapenv:Body>
                        </soapenv:Envelope>';

        $xmlResponse = $this->_callSoapClient($endPoint, $xmlRequest);

        if (!empty($xmlResponse)) {
             if ($xmlResponse["soapenvBody"]["accCreditCardsLstInqRs"]["Hdr"]["alrStatus"]["alrStatusCd"] == self::DEFAULT_SUCCESS_CODE) {
                return (isset($xmlResponse["soapenvBody"]["accCreditCardsLstInqRs"]["Body"]["CreditCardLst"])) ? $xmlResponse["soapenvBody"]["accCreditCardsLstInqRs"]["Body"]["CreditCardLst"] : [];
            } else {
                 $this->logger->info($xmlResponse["soapenvBody"]["accCreditCardsLstInqRs"]["Hdr"]["alrStatus"]["alrStatusCd"]);
                return false;
            }
        }else {
           $this->logger->info("No Response from ESB CreditCards Service");
            return false;
        }
    }

    /**
     * @param $cicnumber
     * @return false|string
     */
    public function getDebitCardsListingFromESB($cicnumber)
    {
        $endPoint = $this->scopeConfig->getValue("cardslisting/debit_cards_url/debit_end_point", $this->_storeScope);
        /** @var TYPE_NAME $xmlRequest */
        $xmlRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:alr="http://www.alrajhiwebservices.com/AlRajhiDebitCards" xmlns:alr1="http://www.alrajhiwebservices.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                          <getCardListRq xmlns="http://www.alrajhiwebservices.com/AlRajhiDebitCards">
                             <Hdr xmlns="">
                                <Msg xmlns="http://www.alrajhiwebservices.com/">
                                   <RqID>' . $this->getRandomString() . '</RqID>
                                   <SvcID>' . self::DEBIT_SVC_ID . '</SvcID>
                                   <SubSvcID>' . self::DEBIT_SUBSVC_ID . '</SubSvcID>
                                   <FuncID>' . self::DEBIT_FUN_ID . '</FuncID>
                                   <MsgTimestamp>' . str_replace(" ", "T", $this->_dateTime->date()) . '</MsgTimestamp>
                                </Msg>
                                <Agt xmlns="http://www.alrajhiwebservices.com/">
                                   <CICNum>' . $cicnumber . '</CICNum>
                                   <UserLang>AR</UserLang>
                                </Agt>
                                <Sys xmlns="http://www.alrajhiwebservices.com/">
                                   <ChID>MOBILEWEB</ChID>
                                   <ExternalSysID>MAGENTO</ExternalSysID>
                                   <OSID>00</OSID>
                                 </Sys>
                             </Hdr>
                             <Body xmlns="">
                                <TotalRecs>0</TotalRecs>
                                <RecPgCtrlIn xmlns="http://www.alrajhiwebservices.com/AlRajhiDebitCards">
                                   <MaxRecs xmlns="http://www.alrajhiwebservices.com/">5</MaxRecs>
                                   <Offset xmlns="http://www.alrajhiwebservices.com/">1</Offset>
                                </RecPgCtrlIn>
                             </Body>
                          </getCardListRq>
                        </soapenv:Body>
                        </soapenv:Envelope>';

        $xmlResponse = $this->_callSoapClient($endPoint, $xmlRequest);
        if (!empty($xmlResponse)) {
             if ($xmlResponse["soapenvBody"]["alrgetCardListRs"]["Hdr"]["alr1Status"]["alr1StatusCd"] == self::DEFAULT_SUCCESS_CODE) {
                return  (isset($xmlResponse["soapenvBody"]["alrgetCardListRs"]["Body"]["ATMCardsLst"])) ? $xmlResponse["soapenvBody"]["alrgetCardListRs"]["Body"] : [];
            } else {
                $this->logger->info($xmlResponse["soapenvBody"]["alrgetCardListRs"]["Hdr"]["alr1Status"]["alr1StatusCd"]);
                return false;
            }
        } else {
             $this->logger->info("No Response from ESB DebitCards Service");
            return false;
        }
    }

    /**
     * call SOAP Client for ESB API
     *
     * @param string $endPoint
     * @param string $xmlRequest
     *
     * @return string $xmlResponseStatus
     */
    private function _callSoapClient($endPoint, $xmlRequest)
    {
        try {
            //Curl request header set for SOAP API
            $this->_curl->setHeaders(['Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($xmlRequest)]);
            $this->_curl->setOption(CURLOPT_RETURNTRANSFER, 1);
            $this->_curl->post($endPoint, $xmlRequest);
            $xmlResponse = $this->_curl->getBody();
            // Convert XML response to Array
            $xmlResponse = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xmlResponse);
            $xml = simplexml_load_string($xmlResponse);
            $xmlResponse = json_decode(json_encode((array)$xml), true);
            return $xmlResponse;
            $this->logger->info($xmlResponse);
        } catch (\Exception $e) {
            //return SOAP Client Error
            $this->logger->info("Error -" . $e);
        }
    }

    /**
     * Get Random String
     *
     * @param null
     *
     * @return string
     */
    private function getRandomString()
    {
        $id = $this->_random->getRandomString(8, self::RANDOM_STRING) . "-";
        $id .= $this->_random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id.= $this->_random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id.= $this->_random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id.= $this->_random->getRandomString(12, self::RANDOM_STRING);
        return $id;
    }
}
