<?php
/**
 * File responsible for custom First Order Promotion condition
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */

namespace Arb\ShoppingCart\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Rule\Model\Condition\AbstractCondition;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Rule\Model\Condition\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class FirstOrderPromotion extends AbstractCondition
{
    /**
     * @var Yesno
     */
    protected $sourceYesNo;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param Context $context
     * @param Yesno $sourceYesNo
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        Yesno $sourceYesNo,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->sourceYesNo = $sourceYesNo;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setAttributeOption(['customer_first_order' => __('Customer first order')]);

        return $this;
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'select';
    }

    /**
     * @return string
     */
    public function getValueElementType()
    {
        return 'select';
    }

    /**
     * @return array|mixed
     */
    public function getValueSelectOptions()
    {
        if (!$this->hasData('value_select_options')) {
            $this->setData(
                'value_select_options',
                $this->sourceYesNo->toOptionArray()
            );
        }

        return $this->getData('value_select_options');
    }

    /**
     * @param AbstractModel $model
     *
     * @return bool
     */
    public function validate(AbstractModel $model)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('customer_id', $model->getCustomerId())
            ->create();

        $orders = $this->orderRepository->getList($searchCriteria);
        $isFirstOrder = $orders->getTotalCount() === 0 ? 1 : 0;
        $model->setData('customer_first_order', $isFirstOrder);

        return parent::validate($model);
    }
}
