<?php
/**
 * This file consist of class which display merchant data in cart
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */
namespace Arb\ShoppingCart\Model;

use Arb\ShoppingCart\Api\Data\ShoppingCartAttributeInterface;
        
class ShoppingCartAttribute implements ShoppingCartAttributeInterface
{

    /**
     * get value data
     *
     * @return void
     */
    public function getValue()
    {
        return self::VALUE;
    }
        
    /**
     * set Value data
     *
     * @param Array $value
     * @return void
     */
    public function setValue($value)
    {
        return true;
    }
}
