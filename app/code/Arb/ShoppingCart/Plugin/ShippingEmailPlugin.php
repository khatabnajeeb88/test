<?php
/**
 * class for updating email address in ShoppingCart module
 *
 * @category    Arb
 * @package     Arb_ShoppingCart
 * @author Arb Magento Team
 */
 namespace Arb\ShoppingCart\Plugin;

use Magento\Framework\Exception\LocalizedException;

 /**
  * class for updating email address after
  * shipping address and billing address are saved.
  */
class ShippingEmailPlugin
{
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/exception.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function afterSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $paymentDetails,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        //get current store id for quote
        $storeId = $quote->getStoreId();
        //check if the customer is not guest user.
        if (!empty($quote->getCustomerId())) {
            $requestedBillingAddress = $addressInformation->getBillingAddress();
            //get requested billing email address.
            if ($requestedBillingAddress !== null) {
                $requestEmailId = $requestedBillingAddress->getEmail();
                //set customer updated email address
                $quote->setStoreId($storeId)->setCustomerEmail($requestEmailId);

                //get billing address from quote
                $billingAddress = $quote->getBillingAddress();
                //get billing address from quote
                $shippingAddress = $quote->getShippingAddress();

                //set updated email in billing address and shipping address.
                if ($billingAddress !== null) {
                    $billingAddress->setEmail($requestEmailId);
                    $quote->setBillingAddress($billingAddress);
                }

                if ($shippingAddress !== null) {
                    $shippingAddress->setEmail($requestEmailId);
                    $quote->setShippingAddress($shippingAddress);
                }

                //save quote
                $this->quoteRepository->save($quote);
            }
        } else {
            $this->logger->info(__(date("Y-m-d h:i:s")." Not email set for the order id
             (".$quote->getId()."), type is guest customer."));
        }
        return $paymentDetails;
    }
}
