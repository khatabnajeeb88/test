<?php
/**
 * class for checking registered and billing phone number in ShoppingCart module
 *
 * @category    Arb
 * @package     Arb_ShoppingCart
 * @author Arb Magento Team
 */
 namespace Arb\ShoppingCart\Plugin;

use Magento\Framework\Exception\LocalizedException;

 /**
  * class for checking registered and billing phone number before
  * shipping address and billing address are saved.
  */
class ShippingInformationPlugin
{
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/exception.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        //check if the customer is not guest user.
        //If customer is exist then return the customer Id else it returns NULL.
        if (!empty($quote->getCustomerId())) {
            //get customer from the cart.
            /** @var \Magento\Customer\Model\Data\Customer $customer */
            $customer = $quote->getCustomer();
            $customerAttributes = $customer->getCustomAttributes();
            //If registered phone number is available.
            if (!empty($customerAttributes["phone_number"])) {
              //get customer registered phone number.
                $customerPhoneNo = $customerAttributes["phone_number"]->getValue();
              //get Billing and Shipping address
                $billingAddress = $addressInformation->getBillingAddress();
                $shippingAddress = $addressInformation->getShippingAddress();
              //set Customer Telephone in Billing and Shipping Information.
                if ($billingAddress !== null) {
                    $billingAddress->setTelephone($customerPhoneNo);
                    $quote->setBillingAddress($billingAddress);
                }

                if ($shippingAddress !== null) {
                    $shippingAddress->setTelephone($customerPhoneNo);
                    $quote->setShippingAddress($shippingAddress);
                }
              //get Store ID to set quote in the current store.
                $storeId = $quote->getStoreId();
                $quote->setStoreId($storeId);
            } else {
                $this->logger->info(__("the customer id".$quote->getCustomerId()." does not have phone number."));
            }
        }
    }
}
