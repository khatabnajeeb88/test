<?php
/**
 * This file consist of class which display merchant data in cart
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */
namespace Arb\ShoppingCart\Api\Data;
        
interface ShoppingCartAttributeInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const VALUE = 'value';
        
    /**
     * Return value.
     *
     * @return $this
     */
    public function getValue();
        
    /**
     * Set value.
     *
     * @param string|null $value
     * @return $this
     */
    public function setValue($value);
}
