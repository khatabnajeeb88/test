<?php
/**
 * Observer file responsible for adding custom promotion rules
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */

namespace Arb\ShoppingCart\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Arb\ShoppingCart\Model\FirstOrderPromotion;

class CustomerConditionObserver implements ObserverInterface
{
    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        $additional = $observer->getAdditional();
        $conditions = (array)$additional->getConditions();
        $conditions = array_merge_recursive($conditions, [
            $this->getCustomerFirstOrderCondition()
        ]);
        $additional->setConditions($conditions);
    }

    /**
     * Get condition for customer first order
     *
     * @return array
     */
    private function getCustomerFirstOrderCondition()
    {
        return [
            'label' => __('Customer First Order'),
            'value' => FirstOrderPromotion::class
        ];
    }
}
