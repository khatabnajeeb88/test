<?php
/**
 * This file consist of class which display merchant and additional price data in cart
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */
namespace Arb\ShoppingCart\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Catalog\Model\ProductFactory as ProductRepository;
use Magento\Quote\Api\Data\CartItemExtensionFactory;
use Magento\Quote\Api\Data\CartExtensionFactory;
use Webkul\Marketplace\Helper\Data as SellerHelper;
use Magento\Catalog\Helper\Data as TaxHelper;

/**
 * cart quote data class
 *
 */

class ProductObserver implements ObserverInterface
{
    /**
     * Inventory stock ID is used in following tables
     * inventory_stock
     * inventory_source
     * inventory_source_item
     * inventory_source_stock_link
     * This inventory stock ID stored in inventory_stock table where its default value is 1 and
     * This is linked with product SKUs in different inventory tables.
     */
    const DEFAULT_STOCK_ID = 1;
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var CartItemExtensionFactory
     */
    protected $extensionFactory;

    /**
     * @var CartExtensionFactory
     */
    protected $cartExtensionFactory;

    /**
     * getSeller data helper
     *
     * @var sellerDataHelper
     */
    protected $sellerDataHelper;

    /**
     * get tax data helper
     *
     * @var taxHelper
     */
    protected $taxHelper;

    /**
     * @var \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface
     */
    protected $stockItem;

    /**
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Product Observer cunstruct
     *
     * @param ProductRepository $productRepository
     * @param CartItemExtensionFactory $extensionFactory
     * @param \Webkul\Marketplace\Helper\Data $sellerDataHelper,
     * @param \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $collectionFactory
     */
    public function __construct(
        ProductRepository $productRepository,
        CartItemExtensionFactory $extensionFactory,
        CartExtensionFactory $cartExtensionFactory,
        SellerHelper $sellerDataHelper,
        TaxHelper $taxHelper,
        \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface $stockItem,
        \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $collectionFactory
    ) {
        $this->productRepository = $productRepository;
        $this->extensionFactory = $extensionFactory;
        $this->cartExtensionFactory = $cartExtensionFactory;
        $this->sellerDataHelper = $sellerDataHelper;
        $this->taxHelper = $taxHelper;
        $this->stockItem = $stockItem;
        $this->collectionFactory  = $collectionFactory;
    }

    /**
     * add product extention attributes for data
     *
     * @param Observer $observer
     * @param string $imageType
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var  Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        $configurableParent = [];
        /**
         * Code to add the items attribute to extension_attributes
         */
        foreach ($quote->getAllItems() as $quoteItem) {
            ///code coverage is skipped as defult magnto dont provide mock call overrride for it
            $product = $this->productRepository->create()->load($quoteItem->getProductId());
            //get product stock data
            $productStockItem = $product->getExtensionAttributes()->getStockItem();
            
            /** @var Item $item  */
            $item = $quoteItem->getExtensionAttributes();
            if ($item === null) {
                $item = $this->extensionFactory->create();
            }

            //@codeCoverageIgnoreStart
            //code coverage is skipped as webkul dont provide the test cases
            //get seller data
            $sellerId =  $shopTitle = '';
            $marketplaceProduct = $this->sellerDataHelper->getSellerProductDataByProductId($quoteItem->getProductId());
            
            foreach ($marketplaceProduct as $value) {
                $sellerId = $value['seller_id'];
            }

            if ($sellerId != "") {
                $sellerInfo = $this->sellerDataHelper->getSellerInfo($sellerId);
                 $shopTitle = $sellerInfo['shop_title'];
            }
            //@codeCoverageIgnoreEnd

            /** get product thumbnail data */
            $imageurl = $product->getThumbnail();

            /** get Product max qty sale*/
            $maxQty = $productStockItem->getMaxSaleQty();

            if ($quoteItem->getProduct()->getTypeId() === 'configurable') {
                $quoteItem->setData('sku', $product->getSku());
                $quoteItem->setData('image_url', $imageurl);
                $quoteItem->setData('max_qty_sale', $maxQty);
                $configurableParent[$quoteItem->getItemId()] = $quoteItem;
            }

            if ($quoteItem->getParentItemId()) {
                $parent = $configurableParent[$quoteItem->getParentItemId()];
                $quoteItem = $parent;

                $optionsData = [];
                $options = $parent->getProduct()->getTypeInstance(true)->getOrderOptions($parent->getProduct());
                foreach ($options['attributes_info'] as $option) {
                    $optionsData[] = [
                        'label' => __($option['label']),
                        'value' => __($option['value'])
                    ];
                }

                $imageurl = $imageurl ? $imageurl : $parent->getData('image_url');
                $maxQty = $maxQty ? $maxQty : $parent->getData('max_qty_sale');
                $item->setParentSku($quoteItem->getData('sku'));
                $item->setConfigurableProductOptions(json_encode($optionsData));
            }

            /** get Product stock qty*/
            $stockQty = $productStockItem->getQty();

            /** get Product Final price including tax */
            $finalPrice = $this->taxHelper->getTaxPrice($product, $product->getFinalPrice(), true);

            /** get Product original price */
            $originalPrice = (float)$product->getPrice();

            /** get Product original discount amount */
            $originalDiscountAmount = $originalPrice - $finalPrice;

            //Add saleble QTY in product detail API
            if ($product->getTypeId() !== 'configurable') {
                $stockQty = $this->stockItem->execute($product->getSku(), self::DEFAULT_STOCK_ID);
            } else {
                $stockQty = 1;
            }
            $item->setSalableQty($stockQty);

            $collection = $this->collectionFactory->create();
            $collection->addFieldToFilter('sku', $product->getSku())
                ->addFieldToFilter('is_used', 0)
                ->addFieldToFilter('status', 1);

            $voucherQty = $collection->getSize();

            // add merchant and other data
            $item->setProductName($product->getName());
            $item->setImageUrl($imageurl);
            $item->setMerchantId($sellerId);
            $item->setMerchantStoreName($shopTitle);
            $item->setStockQty($stockQty);
            $item->setMaxQtySale($maxQty);
            $item->setPriceInclTax($finalPrice);
            $item->setOriginalPrice($originalPrice);
            $item->setOriginalDiscountAmount($originalDiscountAmount);
            $item->setVoucherQty($voucherQty);
            $quoteItem->setExtensionAttributes($item);
        }

        /*adding total data in guest cart*/
        $cartExtension = $quote->getExtensionAttributes();
        if ($cartExtension === null) {
            $cartExtension = $this->cartExtensionFactory->create();
        }

        $allTotals = $quote->getTotals();

        $totals = [];
        
        foreach ($allTotals as $data) {
            if (isset($data['code'])) {
                $totals[$data['code']] = [
                    'code' => $data['code'],
                    'title' => $data['title'],
                    'value' => $data['value']
                ];
            }
        }

        if (array_key_exists('shipping', $totals)) {
            $totals['shipping']['value'] = $quote->getShippingAddress()->getShippingAmount();
        }

        $cartExtension->setTotals($totals);
        $quote->setExtensionAttributes($cartExtension);
    }
}
