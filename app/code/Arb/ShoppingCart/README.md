## Synopsis
This is module used for Shopping Cart API customization and depends on module Magento_Eav, Magento_Catalog, Magento_quote and Webkul_Marketplace.

### API List
1. Add merchant data and product image data in get cart API

### Plugin List
1. Update Customer Telephone number in Shopping Cart irrespective to telephone in request.
2. Update Customer Email ID in Shopping Cart irrespective to Email ID in request. 

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. API extention attributes injection details in [extension_attributes.xml](etc/extention_attribute.xml).
4. Event observer details configuration details in [events.xml](etc/events.xml).
5. Plugin details in [di.xml](etc/di.xml).



