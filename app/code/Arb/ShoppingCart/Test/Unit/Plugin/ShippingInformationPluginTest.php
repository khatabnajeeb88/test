<?php
/**
 * This file consist of PHPUnit test case for class ShippingInformationPlugin
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */
namespace Arb\ShoppingCart\Test\Unit\Plugin;

use \Magento\Quote\Api\CartRepositoryInterface;
use Arb\ShoppingCart\Plugin\ShippingInformationPlugin;
use \Magento\Checkout\Model\ShippingInformationManagement;
use \Magento\Checkout\Api\Data\ShippingInformationInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Arb\ShoppingCart\Plugin\ShippingInformationPlugin
 */
class ShippingInformationPluginTest extends TestCase
{
    public function setUp()
    {
         $this->quoteRepositoryMock = $this->getMockBuilder(CartRepositoryInterface::class)
            ->setMethods([
                'getActive',
                'getCustomerId',
                "getCustomer",
                "getCustomAttributes",
                "getItems",
                "getValue",
                "getStoreId",
                "setStoreId",
                "setBillingAddress",
                "setShippingAddress",
                "save"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->subject = $this->getMockBuilder(ShippingInformationManagement::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->addressInformation = $this->getMockBuilder(ShippingInformationInterface::class)
            ->setMethods([
                'getBillingAddress',
                'getShippingAddress',
                "setTelephone"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->shippingInformationPlugin = new ShippingInformationPlugin($this->quoteRepositoryMock);
    }

    public function testBeforeSaveAddressInformation()
    {
        $this->quoteRepositoryMock->expects($this->any())->method('getActive')
            ->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('getCustomerId')
            ->willReturn(2);
        $this->quoteRepositoryMock->expects($this->any())->method('getCustomer')
            ->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('getCustomAttributes')
            ->willReturn(["phone_number"=>$this->quoteRepositoryMock]);
        $this->quoteRepositoryMock->method("getValue")->willReturn("286982169869862");
        $this->addressInformation->expects($this->once())->method("getBillingAddress")->willReturnSelf();
        $this->addressInformation->expects($this->once())->method("getShippingAddress")->willReturnSelf();
        $this->addressInformation->expects($this->any())->method("setTelephone")->willReturn(true);
        $this->quoteRepositoryMock->expects($this->any())->method('getStoreId')
            ->willReturn(10);
        $this->quoteRepositoryMock->expects($this->any())->method('setStoreId')->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('setBillingAddress')->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('setShippingAddress')->willReturnSelf();
        $this->shippingInformationPlugin->beforeSaveAddressInformation(
            $this->subject,
            1,
            $this->addressInformation
        );
    }

    public function testBeforeSaveAddressInformationElse()
    {
        $this->quoteRepositoryMock->expects($this->any())->method('getActive')
            ->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('getCustomerId')
            ->willReturn(2);
        $this->quoteRepositoryMock->expects($this->any())->method('getCustomer')
            ->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('getCustomAttributes')
            ->willReturn(false);
        $this->quoteRepositoryMock->method("getValue")->willReturn("286982169869862");
        $this->addressInformation->expects($this->any())->method("getBillingAddress")->willReturnSelf();
        $this->addressInformation->expects($this->any())->method("getShippingAddress")->willReturnSelf();
        $this->addressInformation->expects($this->any())->method("setTelephone")->willReturn(true);
        $this->quoteRepositoryMock->expects($this->any())->method('getStoreId')
            ->willReturn(10);
        $this->quoteRepositoryMock->expects($this->any())->method('setStoreId')->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('setBillingAddress')->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('setShippingAddress')->willReturnSelf();
        $this->shippingInformationPlugin->beforeSaveAddressInformation(
            $this->subject,
            1,
            $this->addressInformation
        );
    }
}
