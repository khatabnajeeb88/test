<?php
/**
 * This file consist of PHPUnit test case for class ShippingEmailPlugin
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */
namespace Arb\ShoppingCart\Test\Unit\Plugin;

use \Magento\Quote\Api\CartRepositoryInterface;
use Arb\ShoppingCart\Plugin\ShippingEmailPlugin;
use \Magento\Checkout\Model\ShippingInformationManagement;
use \Magento\Checkout\Api\Data\ShippingInformationInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Arb\ShoppingCart\Plugin\ShippingEmailPlugin
 */
class ShippingEmailPluginTest extends TestCase
{
    public function setUp()
    {
        $this->quoteRepositoryMock = $this->createMock(\Magento\Quote\Api\CartRepositoryInterface::class);
        $this->subject = $this->getMockBuilder(ShippingInformationManagement::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->addressInformation = $this->getMockBuilder(ShippingInformationInterface::class)
            ->setMethods([
                'getBillingAddress',
                'getShippingAddress',
                "getEmail"
            ])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->shippingAddressMock = $this->createPartialMock(
            \Magento\Quote\Model\Quote\Address::class,
            [
                'getSaveInAddressBook',
                'getSameAsBilling',
                'getCustomerAddressId',
                'setShippingAddress',
                'getShippingAddress',
                'setSaveInAddressBook',
                'setSameAsBilling',
                'setCollectShippingRates',
                'getCountryId',
                'importCustomerAddressData',
                'save',
                'getShippingRateByCode',
                'getShippingMethod',
                'setLimitCarrier'
            ]
        );
        $this->billingAddress = $this->createMock(\Magento\Quote\Api\Data\AddressInterface::class);
        $this->quoteMock = $this->createPartialMock(
            \Magento\Quote\Model\Quote::class,
            [
                'isVirtual',
                'getItemsCount',
                'getIsMultiShipping',
                'setIsMultiShipping',
                'validateMinimumAmount',
                'getStoreId',
                'setShippingAddress',
                'getShippingAddress',
                'collectTotals',
                'getExtensionAttributes',
                'setExtensionAttributes',
                'setBillingAddress',
                "getCustomerId",
                "setStoreId",
                "getBillingAddress",
                "setEmail",
                "setCustomerEmail"
            ],
            [],
            '',
            false
        );
        $this->ShippingEmailPlugin = new ShippingEmailPlugin($this->quoteRepositoryMock);
    }

    public function testBeforeSaveAddressInformation()
    {
        $cartId = 100;
        $this->quoteRepositoryMock->expects($this->any())
            ->method('getActive')
            ->willReturn($this->quoteMock);
        $this->quoteMock->expects($this->any())->method('getCustomerId')->willReturn(2);
        $this->addressInformation->expects($this->once())->method("getBillingAddress")->willReturnSelf();
        $this->addressInformation->expects($this->any())->method("getEmail")->willReturn("test@example.com");
        $this->quoteMock->expects($this->any())->method('getStoreId')
            ->willReturn(10);
        $this->quoteMock->expects($this->any())->method('setStoreId')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('getBillingAddress')->willReturn($this->billingAddress);
        $this->quoteMock->expects($this->any())->method('setShippingAddress')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('setEmail')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('getShippingAddress')->willReturn($this->shippingAddressMock);
        $this->quoteMock->expects($this->any())->method('setCustomerEmail')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('setBillingAddress')->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('save')->with($this->quoteMock);
        $this->ShippingEmailPlugin->afterSaveAddressInformation(
            $this->subject,
            [],
            1,
            $this->addressInformation
        );
    }

    public function testBeforeSaveAddressInformationElse()
    {
        $cartId = 100;
        $this->quoteRepositoryMock->expects($this->any())
            ->method('getActive')
            ->willReturn($this->quoteMock);
        $this->quoteMock->expects($this->any())->method('getCustomerId')->willReturn(false);
        $this->addressInformation->expects($this->any())->method("getBillingAddress")->willReturnSelf();
        $this->addressInformation->expects($this->any())->method("getEmail")->willReturn("test@example.com");
        $this->quoteMock->expects($this->any())->method('getStoreId')
            ->willReturn(10);
        $this->quoteMock->expects($this->any())->method('setStoreId')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('getBillingAddress')->willReturn($this->billingAddress);
        $this->quoteMock->expects($this->any())->method('setShippingAddress')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('setEmail')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('getShippingAddress')->willReturn($this->shippingAddressMock);
        $this->quoteMock->expects($this->any())->method('setCustomerEmail')->willReturnSelf();
        $this->quoteMock->expects($this->any())->method('setBillingAddress')->willReturnSelf();
        $this->quoteRepositoryMock->expects($this->any())->method('save')->with($this->quoteMock);
        $this->ShippingEmailPlugin->afterSaveAddressInformation(
            $this->subject,
            [],
            1,
            $this->addressInformation
        );
    }
}
