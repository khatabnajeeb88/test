<?php
/**
 * This file consist of PHPUnit test case for class ProductObserver to update get car API
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */
namespace Arb\ShoppingCart\Test\Unit\Observer;

use Magento\Quote\Model\Quote\Item;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Magento\Quote\Model\Quote;
use Magento\Catalog\Model\ProductFactory as ProductRepository;
use Magento\Quote\Api\Data\CartItemExtensionFactory;
use Magento\Quote\Api\Data\CartExtensionFactory;
use \Arb\ShoppingCart\Observer\ProductObserver;
use Magento\Framework\Event;
use Magento\Catalog\Model\ProductRepository as Product;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Catalog\Helper\Data as TaxHelper;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\Framework\Api\ExtensionAttributesInterface;
use \Magento\CatalogInventory\Api\Data\StockItemInterface;
use \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory as VouchersCollectionFactory;
use PHPUnit_Framework_MockObject_MockObject;
use ReflectionException;

/**
 * @covers \Arb\ShoppingCart\Observer\ProductObserver
 */
class ProductObserverTest extends TestCase
{
    /**
     * @var ProductObserver
     */
    private $model;

    /**
     * @var ProductRepositoryInterfaceFactory|PHPUnit_Framework_MockObject_MockObject
     */
    protected $productRepository;

    /**
     * @var CartItemExtensionFactory|PHPUnit_Framework_MockObject_MockObject
     */
    protected $extensionFactory;

    /**
     * @var Data|PHPUnit_Framework_MockObject_MockObject
     */
    protected $sellerDataHelper;

    /**
     * @var Observer|PHPUnit_Framework_MockObject_MockObject
     */
    private $observerMock;

     /**
      * @var Product|PHPUnit_Framework_MockObject_MockObject
      */
    private $product;

    /**
     * @var CartExtensionFactory|PHPUnit_Framework_MockObject_MockObject
     */

    private $cartExtensionFactory;

     /**
      * @var taxHelper|PHPUnit_Framework_MockObject_MockObject
      */

    private $taxHelper;

     /**
      * @var GetProductSalableQtyInterface|PHPUnit_Framework_MockObject_MockObject
      */
    private $getProductSalableQtyMock;

    /**
     * @var Quote|PHPUnit_Framework_MockObject_MockObject
     */
    private $quoteMock;

    /**
     * @var Item|PHPUnit_Framework_MockObject_MockObject
     */
    private $itemOneMock;

    /**
     * test for product observer
     *
     * @return void
     */
    public function setUp()
    {
        $this->vouchersCollectionFactory = $this->createPartialMock(
            VouchersCollectionFactory::class,
            ['create', 'addFieldToFilter','getSize']
        );
        $this->productRepository = $this->createPartialMock(
            ProductRepository::class,
            ['create', 'getById', 'getExtensionAttributes',"getSku","load"]
        );
        $this->productRepository->expects($this->any())->method('create')
            ->willReturnSelf();
        $this->itemOneMock = $this->getMockBuilder(\Magento\Quote\Model\ResourceModel\Quote\Item::class)
            ->setMethods([
                'getProductId',
                'getExtensionAttributes',
                'setExtensionAttributes',
                'getProduct',
                'setData',
                'getItemId',
                'getParentItemId',
                'getData',
                'setConfigurableProductOptions'
            ])
            ->disableOriginalConstructor()
            ->getMock();
        $this->extensionFactory = $this->createPartialMock(
            CartItemExtensionFactory::class,
            [
                'create',
                'getExtensionAttributes',
                'setImageUrl',
                'setMerchantId',
                'setMerchantStoreName',
                'setStockQty',
                'setMaxQtySale',
                'setPriceInclTax',
                'setSalableQty',
                'setProductName',
                'setOriginalDiscountAmount',
                'setOriginalPrice',
                'setVoucherQty',
                'setParentSku',
                'setConfigurableProductOptions'
            ]
        );
        $this->extensionFactory->expects($this->any())->method('create')
            ->will($this->returnValue($this->extensionFactory));

        $this->extensionAttributeMock = $this->createPartialMock(
            \Magento\Quote\Api\Data\CartExtension::class,
            ['setTotals']
        );

        $this->cartExtensionFactory = $this->createPartialMock(
            CartExtensionFactory::class,
            ['create']
        );
        $this->cartExtensionFactory
                ->expects($this->any())
                ->method('create')
                ->willReturn($this->extensionAttributeMock);
        $this->sellerDataHelper =  $this->createMock(\Webkul\Marketplace\Helper\Data::class);
       
        $this->getTaxPrice =  $this->createPartialMock(
            taxHelper::class,
            [
                "getTaxPrice"
            ]
        );
       
        $this->quoteMock = $this->createMock(Quote::class);
        $eventMock = $this->getMockBuilder(Event::class)
            ->disableOriginalConstructor()
            ->setMethods(['getQuote'])
            ->getMock();

        $totalsArray = [
            'grand_total' => [
                'code' => 'grand_total',
                'title' => 'Grand Total',
                'value' => 20.00
            ],
            'shipping' => [
                'code' => 'shipping',
                'title' => 'Shipping',
                'value' => 20.00
            ]
        ];
        $this->observerMock = $this->createPartialMock(Observer::class, ['getEvent']);
        $this->observerMock->expects($this->any())->method('getEvent')->willReturn($eventMock);
        $eventMock->expects($this->once())->method('getQuote')->willReturn($this->quoteMock);
        $this->quoteMock->expects($this->once())->method('getAllItems')->willReturn([$this->itemOneMock]);

        $this->quoteMock->expects($this->once())->method('getExtensionAttributes')->willReturn(null);
        $this->quoteMock->expects($this->once())->method('getTotals')->willReturn($totalsArray);
        $this->quoteMock->expects($this->once())->method('getShippingAddress')->willReturnSelf();
        $this->quoteMock
        ->expects($this->once())
        ->method('setExtensionAttributes')
        ->willReturn($this->cartExtensionFactory);
        $this->getTaxPrice->expects($this->any())->method('getTaxPrice')->willReturn('20');
        $this->getProductSalableQtyMock = $this->createMock(GetProductSalableQtyInterface::class);
        $this->getProductSalableQtyMock->method("execute")->willReturn("245");
        $this->product =  $this->createPartialMock(
            Product::class,
            [
                'getId',
                'getSku',
                'setWebsiteIds',
                'getWebsiteIds',
                'load',
                'setData',
                'getThumbnail',
                'getStoreId',
                'getMediaGalleryEntries',
                'getExtensionAttributes',
                'getCategoryIds',
                'getAttributes',
                'getResource',
                'getAttributeRawValue',
                'setCustomAttribute',
                'getCustomAttribute',
                'getPrice',
                'getFinalPrice',
                'getName',
                'getTypeId',
                'getTypeInstance',
                'getOrderOptions',
                'setParentSku'
            ]
        );
        $this->vouchersCollectionFactory->method("create")->willReturnSelf();
        $this->vouchersCollectionFactory->method("addFieldToFilter")->willReturnSelf();
        $this->vouchersCollectionFactory->method("getSize")->willReturn("1");
        $this->extensionAttributesMock = $this->getMockBuilder(ExtensionAttributesInterface::class)
            ->disableOriginalConstructor()->setMethods(['getStockItem'])->getMock();
        $this->model = new ProductObserver(
            $this->productRepository,
            $this->extensionFactory,
            $this->cartExtensionFactory,
            $this->sellerDataHelper,
            $this->getTaxPrice,
            $this->getProductSalableQtyMock,
            $this->vouchersCollectionFactory
        );
    }

    /**
     * test execute function
     *
     * @return void
     * @throws ReflectionException
     */
    public function testExecute()
    {
        $this->productRepository->expects($this->any())->method('load')
            ->willReturn($this->product);

        $this->itemOneMock->method('getProduct')->willReturn($this->product);
        $this->product->method('getTypeId')->willReturn('configurable');        

        $this->product->method("getSku")->willReturn("teste");
        $this->product->method("getName")->willReturn("test_product");
        $this->product->expects($this->any())->method('getExtensionAttributes')->willReturn($this->extensionAttributesMock);
        $stockItemMock = $this->getMockBuilder(StockItemInterface::class)
            ->disableOriginalConstructor()->getMock();
        $this->extensionAttributesMock->expects($this->any())->method('getStockItem')->willReturn($stockItemMock);
        $stockItemMock->expects($this->any())->method('getMaxSaleQty')->willReturn('100');
        $stockItemMock->expects($this->any())->method('getQty')->willReturn('100');
        $cartItem = $this->createPartialMock(
            Item::class,
            ['create', 'getSellerProductDataByProductId']
        );
        $cartItem->expects($this->any())->method('create')->will($this->returnValue($cartItem));
        $this->sellerDataHelper->method('getSellerProductDataByProductId')->willReturn([]);
        $this->extensionFactory->expects($this->once())->method('setImageUrl')->willReturn('imgurl.jpg');
        $this->extensionFactory->expects($this->once())->method('setMerchantId')->willReturn('3');
        $this->extensionFactory->expects($this->once())->method('setMerchantStoreName')->willReturn('Noon Store');
        $this->extensionFactory->expects($this->once())->method('setStockQty')->willReturn("100");
        $this->extensionFactory->expects($this->once())->method('setMaxQtySale')->willReturn("10");
        $this->extensionFactory->expects($this->once())->method('setPriceInclTax')->willReturn("20");
        $this->extensionFactory->expects($this->once())->method('setSalableQty')->willReturn("20");
        $this->extensionFactory->expects($this->once())->method('setProductName')->willReturn("test_product");
        $this->extensionFactory->expects($this->once())->method('setVoucherQty')->willReturn("1");

        $this->quoteMock->method('setExtensionAttributes')->willReturn($this->extensionFactory);
        
        $this->model->execute($this->observerMock);
    }

    /**
     * test execute function
     *
     * @return void
     * @throws ReflectionException
     */
    public function testExecuteElse()
    {

        $productTypeInstanceMock = $this->getMockBuilder(\Magento\Catalog\Model\Product\Type\AbstractType::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->productRepository->expects($this->any())->method('load')
            ->willReturn($this->product);

        $this->itemOneMock->method('getProduct')->willReturn($this->product);
        $this->product->method('getTypeId')->willReturn('configurable');
        $this->product->method("getThumbnail")->willReturn("image_url");
        $this->itemOneMock->method('getItemId')->willReturn(1);
        $this->product->method('getTypeInstance')->willReturn($productTypeInstanceMock);
        $options = [
            'attributes_info' => [
                0 => [
                    'label' => 'test', 
                    'value'=> 'test'
                ]
            ]
        ];
        $productTypeInstanceMock->method('getOrderOptions')->willReturn($options);
        $this->product->method("getSku")->willReturn("teste");
        $this->product->method("getName")->willReturn("test_product");
        $this->product->expects($this->any())->method('getExtensionAttributes')->willReturn($this->extensionAttributesMock);
        $stockItemMock = $this->getMockBuilder(StockItemInterface::class)
            ->disableOriginalConstructor()->getMock();
        $this->extensionAttributesMock->expects($this->any())->method('getStockItem')->willReturn($stockItemMock);
        $stockItemMock->expects($this->any())->method('getMaxSaleQty')->willReturn('100');
        $stockItemMock->expects($this->any())->method('getQty')->willReturn('100');
        $cartItem = $this->createPartialMock(
            Item::class,
            ['create', 'getSellerProductDataByProductId']
        );
        
        $cartItem->expects($this->any())->method('create')->will($this->returnValue($cartItem));
        $this->sellerDataHelper->method('getSellerProductDataByProductId')->willReturn([]);
        $this->extensionFactory->expects($this->once())->method('setImageUrl')->willReturn('imgurl.jpg');
        $this->extensionFactory->expects($this->once())->method('setMerchantId')->willReturn('3');
        $this->extensionFactory->expects($this->once())->method('setMerchantStoreName')->willReturn('Noon Store');
        $this->extensionFactory->expects($this->once())->method('setStockQty')->willReturn("100");
        $this->extensionFactory->expects($this->once())->method('setMaxQtySale')->willReturn("10");
        $this->extensionFactory->expects($this->once())->method('setPriceInclTax')->willReturn("20");
        $this->extensionFactory->expects($this->once())->method('setSalableQty')->willReturn("20");
        $this->extensionFactory->expects($this->once())->method('setProductName')->willReturn("test_product");
        $this->extensionFactory->expects($this->once())->method('setVoucherQty')->willReturn("1");

        $this->quoteMock->method('setExtensionAttributes')->willReturn($this->extensionFactory);
        $this->itemOneMock->method('getParentItemId')->willReturn(1);
        $this->itemOneMock->expects($this->any())->method('getData')->with('sku')->willReturn('abc');
        $this->product->expects($this->any())->method('getTypeId')->willReturn('simple');
        $this->model->execute($this->observerMock);
    }

    /**
     * test execute function
     *
     * @return void
     * @throws ReflectionException
     */
    public function testExecuteElseSimple()
    {

        $productTypeInstanceMock = $this->getMockBuilder(\Magento\Catalog\Model\Product\Type\AbstractType::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->productRepository->expects($this->any())->method('load')
            ->willReturn($this->product);

        $this->itemOneMock->method('getProduct')->willReturn($this->product);
        $this->product->method('getTypeId')->willReturn('simple');
        $this->product->method("getThumbnail")->willReturn("image_url");
        $this->product->method('getTypeInstance')->willReturn($productTypeInstanceMock);
        $options = [
            'attributes_info' => [
                0 => [
                    'label' => 'test', 
                    'value'=> 'test'
                ]
            ]
        ];
        $productTypeInstanceMock->method('getOrderOptions')->willReturn($options);
        $this->product->method("getSku")->willReturn("teste");
        $this->product->method("getName")->willReturn("test_product");
        $this->product->expects($this->any())->method('getExtensionAttributes')->willReturn($this->extensionAttributesMock);
        $stockItemMock = $this->getMockBuilder(StockItemInterface::class)
            ->disableOriginalConstructor()->getMock();
        $this->extensionAttributesMock->expects($this->any())->method('getStockItem')->willReturn($stockItemMock);
        $stockItemMock->expects($this->any())->method('getMaxSaleQty')->willReturn('100');
        $stockItemMock->expects($this->any())->method('getQty')->willReturn('100');
        $cartItem = $this->createPartialMock(
            Item::class,
            ['create', 'getSellerProductDataByProductId']
        );
        
        $cartItem->expects($this->any())->method('create')->will($this->returnValue($cartItem));
        $this->sellerDataHelper->method('getSellerProductDataByProductId')->willReturn([]);
        $this->extensionFactory->expects($this->once())->method('setImageUrl')->willReturn('imgurl.jpg');
        $this->extensionFactory->expects($this->once())->method('setMerchantId')->willReturn('3');
        $this->extensionFactory->expects($this->once())->method('setMerchantStoreName')->willReturn('Noon Store');
        $this->extensionFactory->expects($this->once())->method('setStockQty')->willReturn("100");
        $this->extensionFactory->expects($this->once())->method('setMaxQtySale')->willReturn("10");
        $this->extensionFactory->expects($this->once())->method('setPriceInclTax')->willReturn("20");
        $this->extensionFactory->expects($this->once())->method('setSalableQty')->willReturn("20");
        $this->extensionFactory->expects($this->once())->method('setProductName')->willReturn("test_product");
        $this->extensionFactory->expects($this->once())->method('setVoucherQty')->willReturn("1");

        $this->quoteMock->method('setExtensionAttributes')->willReturn($this->extensionFactory);
        $this->itemOneMock->method('getParentItemId')->willReturn(0);
        $this->itemOneMock->expects($this->any())->method('getData')->with('sku')->willReturn('abc');
        $this->model->execute($this->observerMock);
    }
}
