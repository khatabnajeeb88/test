<?php
/**
 * This file consist of PHPUnit test case for CustomerConditionObserver
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */

namespace Arb\ShoppingCart\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Event\Observer;
use Arb\ShoppingCart\Observer\CustomerConditionObserver;
use Magento\Framework\DataObject;

/**
 * @covers \Arb\ShoppingCart\Observer\CustomerConditionObserver
 */
class CustomerConditionObserverTest extends TestCase
{
    /**
     * Object to test
     *
     * @var CustomerConditionObserver
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->testObject = $objectManager->getObject(CustomerConditionObserver::class, []);
    }

    /**
     * @return void
     */
    public function testExecute()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Observer $observerMock */
        $observerMock = $this->getMockBuilder(Observer::class)
            ->setMethods(['getAdditional'])
            ->getMock();

        /** @var PHPUnit_Framework_MockObject_MockObject|DataObject $additionalDataMock */
        $additionalDataMock = $this->getMockBuilder(DataObject::class)
            ->setMethods(['getConditions', 'setConditions'])
            ->getMock();

        $observerMock
            ->expects($this->once())
            ->method('getAdditional')
            ->willReturn($additionalDataMock);

        $additionalDataMock
            ->expects($this->once())
            ->method('getConditions')
            ->willReturn([]);

        $additionalDataMock
            ->expects($this->once())
            ->method('setConditions')
            ->willReturnSelf();

        $this->testObject->execute($observerMock);
    }
}
