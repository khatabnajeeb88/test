<?php
/**
 * ShoppingCartTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */
namespace Arb\ShoppingCart\Test\Unit\Model;

use Arb\ShoppingCart\Model\ShoppingCartAttribute;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class ShoppingCartTest for testing  ShoppingCart class
 * @covers \Arb\ShoppingCart\Model\ShoppingCartAttribute
 */
class ShoppingCartTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->model = $this->objectManager->getObject(ShoppingCartAttribute::class);
    }

    /**
     * testGetValue method
     */
    public function testGetValue()
    {
        $this->model->setValue("test");
        $this->assertSame($this->model->getValue(), ShoppingCartAttribute::VALUE);
    }
}
