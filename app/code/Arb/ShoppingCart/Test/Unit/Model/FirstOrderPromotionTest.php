<?php
/**
 * This file consist of PHPUnit test case for FirstOrderPromotion
 *
 * @category Arb
 * @package Arb_ShoppingCart
 * @author Arb Magento Team
 *
 */

namespace Arb\ShoppingCart\Test\Unit\Observer;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\ShoppingCart\Model\FirstOrderPromotion;
use Magento\Framework\Model\AbstractModel;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * @covers \Arb\ShoppingCart\Model\FirstOrderPromotion
 */
class FirstOrderPromotionTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Yesno
     */
    private $yesNoMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderRepositoryInterface
     */
    private $orderRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * Object to test
     *
     * @var FirstOrderPromotion
     */
    private $testObject;

    /**
     * Main Set Up method
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->yesNoMock = $this->createMock(Yesno::class);
        $this->orderRepositoryMock = $this->createMock(OrderRepositoryInterface::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);

        $this->testObject = $objectManager->getObject(FirstOrderPromotion::class, [
            'sourceYesNo' => $this->yesNoMock,
            'orderRepository' => $this->orderRepositoryMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock
        ]);
    }

    /**
     * @return void
     */
    public function testLoadAttributeOptions()
    {
        $loadAttributeOptions = $this->testObject->loadAttributeOptions();
        $this->assertInstanceOf(FirstOrderPromotion::class, $loadAttributeOptions);
    }

    /**
     * @return void
     */
    public function testGetInputType()
    {
        $getInputType = $this->testObject->getInputType();
        $this->assertInternalType('string', $getInputType);
    }

    /**
     * @return void
     */
    public function testGetValueElementType()
    {
        $getValueElementType = $this->testObject->getValueElementType();
        $this->assertInternalType('string', $getValueElementType);
    }

    /**
     * @return void
     */
    public function testGetValueSelectOptions()
    {
        $getValueElementType = $this->testObject->getValueSelectOptions();
    }

    /**
     * @return void
     */
    public function testValidate()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|AbstractModel $modelMock */
        $modelMock = $this->getMockBuilder(AbstractModel::class)
            ->disableOriginalConstructor()
            ->setMethods(['getCustomerId'])
            ->getMock();

        $modelMock
            ->expects($this->once())
            ->method('getCustomerId')
            ->willReturn(1);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaInterface $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('addFilter')
            ->willReturnSelf();

        $this->searchCriteriaBuilderMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|OrderSearchResultInterface $orderListMock */
        $orderListMock = $this->createMock(OrderSearchResultInterface::class);

        $this->orderRepositoryMock
            ->expects($this->once())
            ->method('getList')
            ->willReturn($orderListMock);

        $orderListMock
            ->expects($this->once())
            ->method('getTotalCount')
            ->willReturn(0);

        $validate = $this->testObject->validate($modelMock);
        $this->assertInternalType('boolean', $validate);
    }
}
