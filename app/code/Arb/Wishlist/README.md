## Synopsis
This is module used for Wishlist API which contains the functionality for add, delete and list customer wish list using customer Id

### API List
1. Add product to customer wishlist with customer Id
2. List all products in customer wishlist
3. Delete product form customer wishlist

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).
4. Web API configuration details in [webapi.xml](etc/webapi.xml).