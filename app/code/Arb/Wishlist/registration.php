<?php
/**
 * Module registration file
 *
 * @category Arb
 * @package Arb_Order
 * @author Arb Magento Team
 *
 */

// @codeCoverageIgnoreStart
// it is a default magento module registration code so no code coverage is required
// as default magneto also not providing it
Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_Wishlist',
    __DIR__
);
// @codeCoverageIgnoreEnd
