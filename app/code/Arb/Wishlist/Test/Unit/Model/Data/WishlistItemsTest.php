<?php
namespace Arb\Wishlist\Test\Unit\Model\Data;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

class WishlistItemsTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->testObject = $this->objectManager->getObject(
            \Arb\Wishlist\Model\Data\WishlistItems::class,
            [
            ]
        );
    }

    public function testItemId()
    {
        $itemId = 1;
        $this->testObject->setItemId($itemId);
        $this->assertEquals($itemId, $this->testObject->getItemId());
    }

    public function testName()
    {
        $name = "laptop";
        $this->testObject->setName($name);
        $this->assertEquals($name, $this->testObject->getName());
    }

    public function testPrice()
    {
        $price = 100;
        $this->testObject->setPrice($price);
        $this->assertEquals($price, $this->testObject->getPrice());
    }

    public function testMerchantName()
    {
        $StoreName = "croma";
        $this->testObject->setMerchantName($StoreName);
        $this->assertEquals($StoreName, $this->testObject->getMerchantName());
    }

    public function testGetSpecialPrice()
    {
        $this->testObject->getSpecialPrice();
        $this->testObject->getMerchantId();
        $this->testObject->getSku();
        $this->testObject->getImage();
        $this->testObject->getOptionData();
        $this->testObject->setOptionData(['test']);
        $this->testObject->setSpecialPrice(100);
        $this->testObject->setMerchantId(1);
        $this->testObject->setSku("test");
        $this->testObject->setImage("teste.jpg");
    }
}
