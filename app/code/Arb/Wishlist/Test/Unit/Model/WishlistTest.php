<?php
    namespace Arb\Wishlist\Test\Unit\Model;
    
    use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
    use PHPUnit\Framework\TestCase;
    use PHPUnit_Framework_MockObject_MockObject;
    use Webkul\Marketplace\Model\ResourceModel\Product\Collection as WebkulCollection;
    use \Magento\Framework\Exception\LocalizedException;
    use Magento\Integration\Model\Oauth\TokenFactory;
    use Magento\Framework\Webapi\Request;
    
    class WishlistTest extends TestCase
    {
        /**
         * @param \Magento\Wishlist\Model\WishlistFactory
         */
        private $wishlistFactoryMock;
    
        /**
         * @param \Magento\Catalog\Api\ProductRepositoryInterface
         */
        private $prodRepMock;
    
        /**
         * @param \Magento\Wishlist\Model\ItemFactory
         */
        private $itemFactoryMock;
    
        /**
         * @param \Arb\Wishlist\Model\Data\WishlistItemsFactory
         */
        private $wishItemFactoryMock;
    
        /**
         * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
         */
        private $productFactoryMock;
    
        /**
         * @param \Magento\Store\Model\StoreManagerInterface
         */
        private $storeManagerMock;
    
        /**
         * Main set up method
         */
        public function setUp()
        {
            $this->objectManager = new ObjectManager($this);
            $this->wishlistMock = $this->getMockBuilder(\Magento\Wishlist\Model\Wishlist::class)
                                       ->disableOriginalConstructor()
                                       ->setMethods([
                                           'getId',
                                           'getItemCollection',
                                           'getWishlistId',
                                           'addNewItem',
                                           'save',
                                           'getCollection',
                                            'addFieldToFilter',
                                            'getFirstItem',
                                            'setCustomerId',
                                            'setSharingCode',
                                            'load',
                                            'loadByCustomerId',
                                            'setPageSize',
                                            'setCurPage',
                                            'setOrder',
                                            'getCustomerId'
                                       ])
                                       ->getMock();
            $this->wishlistFactoryMock = $this->getMockBuilder(\Magento\Wishlist\Model\WishlistFactory::class)
                                              ->disableOriginalConstructor()
                                              ->setMethods([
                                                  'create',
                                                  'getItems'
                                              ])
                                              ->getMock();
            $this->productMock = $this->getMockBuilder(\Magento\Catalog\Model\Product::class)
                                      ->disableOriginalConstructor()
                                      ->setMethods([
                                          'getName',
                                          'getSku',
                                          'getPrice',
                                          'getShopTitle',
                                          'addAttributeToSelect',
                                          'addFieldToFilter',
                                          'getSelect',
                                          'join',
                                          'setPageSize',
                                          'setCurPage',
                                          'getFirstItem',
                                          'getTypeId',
                                          'where'
                                      ])
                                      ->getMock();
            $this->tokenModelFactoryMock = $this->getMockBuilder(TokenFactory::class)
                                      ->disableOriginalConstructor()
                                      ->setMethods([
                                            "create",
                                            "load",
                                            "getCustomerId",
                                            "getRevoked"
                                      ])
                                      ->getMock();
            $this->tokenModelFactoryMock->method("create")->willReturnSelf();
            $this->tokenModelFactoryMock->method("load")->willReturnSelf();
            //$this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $this->requestMock =$this->getMockBuilder(Request::class)
                                      ->disableOriginalConstructor()
                                      ->setMethods([
                                          
                                      ])
                                      ->getMock();
            $this->setWishlistMocks();
            $this->setProductWishlistMocks();
            $this->setItemMocks();
        }
        /**
         * set wishlist mock variables
         *
         * @return void
         */
        public function setWishlistMocks()
        {
            $this->prodRepMock = $this->getMockBuilder(\Magento\Catalog\Api\ProductRepositoryInterface::class)
                                                ->disableOriginalConstructor()
                                                ->setMethods([
                                                    'get',
                                                    'getById',
                                                    'delete',
                                                    'save',
                                                    'deleteById',
                                                    'getList'
                                                ])
                                                ->getMock();
            
            $this->itemMock = $this->getMockBuilder(\Magento\Wishlist\Model\Item::class)
                                   ->disableOriginalConstructor()
                                   ->setMethods([
                                       'getId',
                                       'getStoreId',
                                       'getAddedAt',
                                       'getDescription',
                                       'getQty',
                                       'getProductId',
                                       'getWishlistId',
                                       'delete',
                                       'load',
                                       'getItems',
                                       'getSku'
                                   ])
                                   ->getMock();
            $this->itemFactoryMock = $this->getMockBuilder(\Magento\Wishlist\Model\ItemFactory::class)
                                          ->disableOriginalConstructor()
                                          ->setMethods([
                                              'create',
                                              'load',
                                              'getItems',
                                              'setPageSize',
                                            'setCurPage',
                                            'setOrder',
                                            'getIterator'
                                          ])
                                          ->getMock();
            
            $this->wishlistResponseItemsMock = $this->getMockBuilder(\Arb\Wishlist\Model\Data\WishlistItems::class)
                                                    ->disableOriginalConstructor()
                                                    ->setMethods([
                                                        'setItemId',
                                                        'setStoreId',
                                                        'setAddedAt',
                                                        'setDescription',
                                                        'setQty',
                                                        'setName',
                                                        'setSku',
                                                        'setPrice',
                                                        'setVendor',
                                                        'setProductId'
                                                    ])
                                                    ->getMock();
            $this->wishItemFactoryMock = $this->getMockBuilder(
                \Arb\Wishlist\Model\Data\WishlistItemsFactory::class
            )
                                                                    ->disableOriginalConstructor()
                                                                    ->setMethods([
                                                                        'create',
                                                                        'getItems'
                                                                    ])
                                                                    ->getMock();
            
            $this->productFactoryMock = $this->getMockBuilder(
                \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory::class
            )
                                             ->disableOriginalConstructor()
                                             ->setMethods([
                                                 'create'
                                             ])
                                             ->getMock();
    
            $this->storeManagerMock = $this->getMockBuilder(\Magento\Store\Model\StoreManagerInterface::class)
                                           ->disableOriginalConstructor()
                                           ->setMethods([
                                               'getStore',
                                               'setIsSingleStoreModeAllowed',
                                               'hasSingleStore',
                                               'isSingleStoreMode',
                                               'getStores',
                                               'getWebsite',
                                               'getWebsites',
                                               'reinitStores',
                                               'getDefaultStoreView',
                                               'getGroup',
                                               'getGroups',
                                               'setCurrentStore'
                                               
                                           ])
                                           ->getMock();
        }
    
        /**
         * set product wishlist mock variables
         *
         * @return void
         */
        public function setProductWishlistMocks()
        {
            $this->StoreInterfaceMock = $this->getMockBuilder(\Magento\Store\Api\Data\StoreInterface::class)
                                                ->disableOriginalConstructor()
                                                ->setMethods([
                                                    'getId',
                                                    'setId',
                                                    'getCode',
                                                    'setCode',
                                                    'getName',
                                                    'setName',
                                                    'getWebsiteId',
                                                    'setWebsiteId',
                                                    'getStoreGroupId',
                                                    'setStoreGroupId',
                                                    'setIsActive',
                                                    'getIsActive',
                                                    'getExtensionAttributes',
                                                    'setExtensionAttributes',
                                                    "getBaseUrl"
                                                ])
                                                ->getMock();
            $this->wpCollectionMock = $this->getMockBuilder(WebkulCollection::class)
                                                ->disableOriginalConstructor()
                                                ->setMethods([
                                                    "getTable",
                                                    "addFieldToFilter",
                                                    "getSelect",
                                                    "join",
                                                    "joinLeft",
                                                    "addFieldToSelect",
                                                    "count",
                                                    "load",
                                                    "getData"
                                                ])
                                                ->getMock();
            $this->wpItemOptionCollectionFactoryMock = $this->getMockBuilder(\Magento\Wishlist\Model\ResourceModel\Item\Option\CollectionFactory::class)
                                        ->disableOriginalConstructor()
                                        ->setMethods([
                                            "create",
                                            "addItemFilter",
                                            "getProductIds",
                                            "getCollection"
                                        ])
                                        ->getMock();
            
            $this->wpItemOptionCollectionMock = $this->getMockBuilder(\Magento\Wishlist\Model\ResourceModel\Item\Option\Collection::class)
                                        ->disableOriginalConstructor()
                                        ->setMethods([
                                            "create",
                                            "addItemFilter",
                                            "getProductIds",
                                            "getData",
                                            "getCollection",
                                            "addFieldToFilter",
                                            "getIterator"
                                        ])
                                        ->getMock();
    
            $this->wpItemOptionMock = $this->getMockBuilder(\Magento\Wishlist\Model\ResourceModel\Item\Option::class)
                                        ->disableOriginalConstructor()
                                        ->setMethods([
                                            "create",
                                            "addItemFilter",
                                            "getProductIds",
                                            "getData",
                                            "getCollection"
                                        ])
                                        ->getMock();
    
            $this->wpCollectionMock->method('getSelect')->willReturnSelf();
            $this->wpCollectionMock->method('join')->willReturnSelf();
            $this->testObject = $this->objectManager->getObject(
                \Arb\Wishlist\Model\Wishlist::class,
                [
                    'wishlistFactory' => $this->wishlistFactoryMock,
                    'productRepository' => $this->prodRepMock,
                    'itemFactory' => $this->itemFactoryMock,
                    'wishlistItemsFactory' => $this->wishItemFactoryMock,
                    'productFactory' => $this->productFactoryMock,
                    'storeManager' => $this->storeManagerMock,
                    "sellerCollection"=>$this->wpCollectionMock,
                    "optionCollectionFactory" =>$this->wpItemOptionCollectionFactoryMock,
                    "tokenModelFactory"=>$this->tokenModelFactoryMock,
                    "request"=>$this->requestMock
                ]
            );
            $this->wishlistFactoryMock->method('create')->willReturn(
                $this->wishlistMock
            );
            $this->wishlistMock->method('loadByCustomerId')->will(
                $this->returnSelf()
            );
            $this->wishlistMock->method('getCollection')->will(
                $this->returnSelf()
            );
            $this->wishlistMock->method('addFieldToFilter')->will(
                $this->returnSelf()
            );
            $this->storeManagerMock->method('getStore')->willReturn(
                $this->StoreInterfaceMock
            );
            $this->StoreInterfaceMock->method('getBaseUrl')->willReturn(
                "http://localhost/magentotwo/"
            );
            $this->StoreInterfaceMock->method('getId')->willReturn(
                1
            );
            $this->productFactoryMock->method('create')->willReturn(
                $this->productMock
            );
            $this->productMock->method('addAttributeToSelect')->will(
                $this->returnSelf()
            );
            $this->productMock->method('addFieldToFilter')->will(
                $this->returnSelf()
            );
            $this->productMock->method('getSelect')->will(
                $this->returnSelf()
            );
    
            $this->productMock->method('join')->will(
                $this->returnSelf()
            );
    
            // $this->productMock->method('setPageSize')->will(
            //     $this->returnSelf()
            // );
    
            $this->wishlistMock->method('save')->will(
                $this->returnSelf()
            );
    
            // $this->wishlistMock->method('load')->will(
            //     $this->returnSelf()
            // );
        }
        
        /**
         * set wishlist items mock variables
         *
         * @return void
         */
        public function setItemMocks()
        {
            $this->productMock->method('getFirstItem')->willReturn(
                $this->productMock
            );
    
            $this->wishlistMock->method('getFirstItem')->will(
                $this->returnSelf()
            );
    
            $this->wishItemFactoryMock->method('create')->willReturn(
                $this->wishlistResponseItemsMock
            );
    
            $this->itemFactoryMock->method('create')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('load')->will(
                $this->returnSelf()
            );
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testProductNotFound()
        {
            $this->testObject->add('12345678', 'laptop',[]);
        }
    
        public function testAddProduct()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn(
                $this->productMock
            );
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $result = $this->testObject->add('12345678', 'laptop',[]);
    
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testAddProductNotFound()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn("");
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
    
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $result = $this->testObject->add('12345678', 'laptop',['test']);
    
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testAddProductNotFoundIsValid()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn("");
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
    
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("456");
            $result = $this->testObject->add('12345678', 'laptop',['test']);
    
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
    
        public function testAddProductNotFoundWithOptionData()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn($this->productMock);
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
    
            $this->itemMock->method('getSku')->willReturn(
                "sku"
            );
    
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $result = $this->testObject->add('12345678', 'laptop',['test']);
    
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testAddProductTokenNotFound()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn(
                $this->productMock
            );
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken df");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
    
            $result = $this->testObject->add('12345678', 'laptop',[]);
    
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testAddProductNotFoundBearer()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn(
                $this->productMock
            );
            $this->requestMock->method("getHeader")->willReturn("Bearers testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
    
            $result = $this->testObject->add('12345678', 'laptop',[]);
    
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testAddProductNotFoundCustomerId()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn(
                $this->productMock
            );
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
    
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("");
            $result = $this->testObject->add('678', 'laptop',[]);
    
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
         /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testAddProductNotFoundCustomerIdNotMatch()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn(
                $this->productMock
            );
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
    
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("1214");
            $result = $this->testObject->add('346', 'laptop',[]);
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
         /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testAddProductNotFoundCustomerIdRevoked()
        {
            $wishlistId = 1;
            $itemId = 2;
            $this->prodRepMock->method('get')->willReturn(
                $this->productMock
            );
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $this->wishlistMock->method('getWishlistId')->willReturn(
                $wishlistId
            );
            
            $this->wishlistMock->method('addNewItem')->willReturn(
                $this->itemMock
            );
    
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("345");
            $this->tokenModelFactoryMock->method("getRevoked")->willReturn(1);
            $result = $this->testObject->add('346', 'laptop',[]);
            $this->assertArrayHasKey('result', $result);
            $this->assertArrayHasKey('wishlist_id', $result['result']);
            $this->assertEquals($result['result']['wishlist_id'], $wishlistId);
            $this->assertArrayHasKey('item_id', $result['result']);
            $this->assertEquals($result['result']['item_id'], $itemId);
        }
    
        public function testGetWishlistItems()
        {
            $wishlistId = 1;
            
            $this->itemMock->method('getId')->willReturn(1);
            $this->itemMock->method('getProductId')->willReturn(1);
            $this->wpCollectionMock->method('addFieldToSelect')->willReturn([
                "mageproduct_id"=>"1",
                "seller_id"=>"1"
                ]);
            $this->wpCollectionMock->method('count')->willReturn(1);
            $this->wpCollectionMock->method('load')->willReturnSelf();
            $this->wpCollectionMock->method('getData')->willReturn([["data"=>1]]);
    
            $wishlistItems = [$this->itemMock];
            $this->wishlistMock->method('getId')->willReturn(
                $wishlistId
            );
            $this->wishlistMock->method('getItemCollection')->willReturn(
                $this->itemFactoryMock
            );
            $this->itemFactoryMock->method('setOrder')->willReturn(
                $this->itemMock
            );
            
            $this->itemFactoryMock->method('setPageSize')->willReturn($this->itemMock);
            $this->itemFactoryMock->method('setCurPage')->willReturn($this->itemMock);
            
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $itemIds = $this->itemFactoryMock->method('getItems')->willReturn([1,2]);
    
            
    
            $this->wpItemOptionCollectionFactoryMock->expects($this->any())
                ->method('create')
                ->willReturn($this->wpItemOptionCollectionMock);
    
            $this->wpItemOptionCollectionMock->expects($this->any())
                ->method('addItemFilter')
                ->willReturn($this->wpItemOptionCollectionMock);
    
            $this->wpItemOptionCollectionMock->expects($this->any())
                ->method('getData')
                ->willReturn([1,2]);
    
            $this->wpItemOptionCollectionMock->expects($this->any())
                ->method('getIterator')
                ->willReturn(new \ArrayIterator([$this->wpItemOptionCollectionMock]));
    
            $this->itemFactoryMock->expects($this->any())
                ->method('getIterator')
                ->willReturn(new \ArrayIterator([1,2]));
    
            $this->productMock->method('where')->will(
                $this->returnSelf()
            );
            $this->productMock->method('getTypeId')->willReturn('configurable');
            
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $restult = $this->testObject->getWishlistItems('12345678');
            $this->assertInternalType('array', $restult);
            $this->assertEquals(0, count($restult));
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testGetWishlistItemsException()
        {
            $this->testObject->getWishlistItems('1');
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testRemoveItemNotFound()
        {
            $customerId = "1";
            $itemId = 2;
            $this->testObject->remove($customerId, $itemId);
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testRemoveWishlistNotFound()
        {
            $customerId = "1";
            $itemId = 2;
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
            $this->testObject->remove($customerId, $itemId);
        }
    
        public function testRemove()
        {
            $customerId = "12345678";
            $itemId = 2;
            $wishlistItems = [$this->itemMock];
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
            $this->wishlistMock->method('getCustomerId')->willReturn(
                $customerId
            );
            $this->wishlistMock->method('getItemCollection')->willReturn(
                $wishlistItems
            );
    
            $this->wishlistMock->method('load')->will(
                $this->returnSelf()
            );
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $restult = $this->testObject->remove($customerId, $itemId);
            $this->assertInternalType('boolean', $restult);
            $this->assertEquals(true, $restult);
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testRemoveError()
        {
            $customerId = "12345678";
            $itemId = 2;
            $wishlistItems = [$this->itemMock];
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
            $this->wishlistMock->method('getCustomerId')->willReturn(
                $customerId
            );
            $this->wishlistMock->method('getItemCollection')->willReturn(
                $wishlistItems
            );
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken s");
            $restult = $this->testObject->remove($customerId, $itemId);
            $this->assertInternalType('boolean', $restult);
            $this->assertEquals(true, $restult);
        }
    
        /**
         * @expectedException \Magento\Framework\Exception\LocalizedException
         */
        public function testRemoveErrorId()
        {
            $customerId = "12345678";
            $itemId = 2;
            $wishlistItems = [$this->itemMock];
            $this->itemMock->method('getId')->willReturn("");
            $this->wishlistMock->method('getCustomerId')->willReturn(
                $customerId
            );
            $this->wishlistMock->method('getItemCollection')->willReturn(
                $wishlistItems
            );
    
            $this->wishlistMock->method('load')->will(
                $this->returnSelf()
            );
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $restult = $this->testObject->remove($customerId, $itemId);
            $this->assertInternalType('boolean', $restult);
            $this->assertEquals(true, $restult);
        }

        public function testRemoveErrorNoWishlist()
        {
            $customerId = "12345678";
            $itemId = 2;
            $wishlistItems = [$this->itemMock];
            $this->itemMock->method('getId')->willReturn(
                $itemId
            );
            $this->wishlistMock->method('getCustomerId')->willReturn(
                $customerId
            );
            $this->wishlistMock->method('getItemCollection')->willReturn(
                $wishlistItems
            );
    
            $this->wishlistMock->method('load')->willReturn("");
            $this->tokenModelFactoryMock->method("getCustomerId")->willReturn("12345678");
            $this->requestMock->method("getHeader")->willReturn("Bearer testetoken");
            $restult = $this->testObject->remove($customerId, $itemId);
            $this->assertInternalType('boolean', $restult);
            $this->assertEquals(true, $restult);
        }
    
        public function testGetMerchantData()
        {
            $productIds = ['1'=>'1'];
            $storeId = 0;
            $this->assertEquals([], []);
        }
    }