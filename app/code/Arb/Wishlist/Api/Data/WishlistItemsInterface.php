<?php
/**
 * This file file consist of interface for wishlist item array details
 *
 * @category Arb
 * @package Arb_Wishlist
 * @author Arb Magento Team
 *
 */
namespace Arb\Wishlist\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * class for wishlist interface
 */
interface WishlistItemsInterface
{
    const LABEL_ITEM_ID           = 'item_id';
    const LABEL_NAME              = 'name';
    const LABEL_PRICE             = 'price';
    const LABEL_SPECIALPRICE      = 'special_price';
    const LABEL_IMAGE             = 'image';
    const LABEL_STORE_NAME        = 'merchant_id';
    const LABEL_MERCHANT_ID       = 'merchant_name';
    const LABEL_SKU               = 'sku';
    const LABEL_OPTIONS           = 'option_data';
    /**
     * Get Wishlist Item Id
     *
     * @return int
     */
    public function getItemId();
    
    /**
     * Get Name
     *
     * @return string
     */
    public function getName();

    /**
     * Get Price
     *
     * @return string
     */
    public function getPrice();

    /**
     * Get Special Price
     *
     * @return string
     */
    public function getSpecialPrice();

    /**
     * Get Vendor
     *
     * @return string
     */
    public function getMerchantName();

    /**
     * Get Merchant Id
     *
     * @return string
     */
    public function getMerchantId();

    /**
     * Get Sku
     *
     * @return string
     */
    public function getSku();

    /**
     * Get Product image
     *
     * @return string
     */
    public function getImage();

    /**
     * Get Product Options
     *
     * @return string
     */
    public function getOptionData();

    /**
     * Set Wishlist Item Id
     *
     * @param int $wishlistItemId
     * @return $this
     */
    public function setItemId($wishlistItemId);
 
    /**
     * Set name
     *
     * @param  string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Set Price
     *
     * @param  string $price
     * @return $this
     */
    public function setPrice($price);

    /**
     * Set Price
     *
     * @param  string $price
     * @return $this
     */
    public function setSpecialPrice($price);

    /**
     * Set Vendor
     *
     * @param  string $store
     * @return $this
     */
    public function setMerchantName($store);
    
    /**
     * Set Merchant Id
     *
     * @param  string $merchantId
     * @return $this
     */
    public function setMerchantId($merchantId);

    /**
     * Set Sku
     *
     * @param  string $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * Set Product Image
     *
     * @param string $image
     * @return $this
     */
    public function setImage($image);

    /**
     * Set Product Options
     *
     * @param string $optionData
     * @return $this
     */
    public function setOptionData($optionData);
}
