<?php
/**
 * This file file consist of interface for wishlist add remove and listing
 *
 * @category Arb
 * @package Arb_Wishlist
 * @author Arb Magento Team
 *
 */
namespace Arb\Wishlist\Api;

/**
 * class for wishlist interface
 */
interface WishlistInterface
{
   /**
    * @param string $customerId customer Id
    * @param string $sku product sku
    * @param mixed $optionData product options
    * @return array
    * @throws \Magento\Framework\Exception\NoSuchEntityException
    */
    public function add($customerId, $sku, $optionData = []);

    /**
     * @param string $customerId customer id
     * @param int $itemId Wishlist Item id
     * @return boolean $result
     */
    public function remove($customerId, $itemId);

    /**
     * @param string $customerId customer id
     * @param int $currentPage
     * @param int $pageSize
     * @return \Arb\Wishlist\Api\Data\WishlistItemsInterface[] item list
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getWishlistItems($customerId, $currentPage = 1, $pageSize = 10);
}
