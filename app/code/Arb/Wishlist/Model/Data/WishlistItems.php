<?php
/**
 * Get and Set methods for wishlist items
 *
 * @category Arb
 * @package Arb_Wishlist
 * @author Arb Magento Team
 *
 */
namespace Arb\Wishlist\Model\Data;

use Arb\Wishlist\Api\Data\WishlistItemsInterface;

/**
 * class for wishlist items
 */
class WishlistItems extends \Magento\Framework\Api\AbstractExtensibleObject implements WishlistItemsInterface
{
    /**
     * @inheritDoc
     */
    public function getItemId()
    {
        return $this->_get(self::LABEL_ITEM_ID);
    }
  
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->_get(self::LABEL_NAME);
    }

    /**
     * @inheritDoc
     */
    public function getPrice()
    {
        return $this->_get(self::LABEL_PRICE);
    }

    /**
     * @inheritDoc
     */
    public function getSpecialPrice()
    {
        return $this->_get(self::LABEL_SPECIALPRICE);
    }

    /**
     * @inheritDoc
     */
    public function getMerchantName()
    {
        return $this->_get(self::LABEL_STORE_NAME);
    }

    /**
     * @inheritDoc
     */
    public function getMerchantId()
    {
        return $this->_get(self::LABEL_MERCHANT_ID);
    }
    
    /**
     * @inheritDoc
     */
    public function getSku()
    {
        return $this->_get(self::LABEL_SKU);
    }

    /**
     * @inheritDoc
     */
    public function getImage()
    {
        return $this->_get(self::LABEL_IMAGE);
    }
    
     /**
     * @inheritDoc
     */
    public function getOptionData()
    {
        return $this->_get(self::LABEL_OPTIONS);
    }

    /**
     * @inheritDoc
     */
    public function setItemId($wishlistItemId)
    {
        return $this->setData(self::LABEL_ITEM_ID, $wishlistItemId);
    }
  
    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        return $this->setData(self::LABEL_NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function setPrice($price)
    {
        return $this->setData(self::LABEL_PRICE, $price);
    }

    /**
     * @inheritDoc
     */
    public function setSpecialPrice($price)
    {
        return $this->setData(self::LABEL_SPECIALPRICE, $price);
    }

    /**
     * @inheritDoc
     */
    public function setMerchantName($store)
    {
        return $this->setData(self::LABEL_STORE_NAME, $store);
    }

    /**
     * @inheritDoc
     */
    public function setMerchantId($merchantId)
    {
        return $this->setData(self::LABEL_MERCHANT_ID, $merchantId);
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        return $this->setData(self::LABEL_SKU, $sku);
    }

    /**
     * @inheritDoc
     */
    public function setImage($image)
    {
        return $this->setData(self::LABEL_IMAGE, $image);
    }

    /**
     * @inheritDoc
     */
    public function setOptionData($optionData)
    {
        return $this->setData(self::LABEL_OPTIONS, $optionData);
    }
}
