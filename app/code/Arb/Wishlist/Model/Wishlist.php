<?php

/**
 * This file consist of class Wishlist which is used for adding, removing and return wishlist item .
 *
 * @category Arb
 * @package Arb_Wishlist
 * @author Arb Magento Team
 *
 */

namespace Arb\Wishlist\Model;

use \Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\Framework\Webapi\Request;

/**
 * class for wishlist
 */
class Wishlist implements \Arb\Wishlist\Api\WishlistInterface
{

    /**
     * @param \Magento\Wishlist\Model\WishlistFactory
     */
    private $wishlistFactory;

    /**
     * @param \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param \Magento\Wishlist\Model\ItemFactory
     */
    private $itemFactory;

    /**
     * @param \Arb\Wishlist\Model\Data\WishlistItemsFactory
     */
    private $wishlistItemsFactory;

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productFactory;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Webkul\Marketplace\Model\ResourceModel\Product\Collection
     */
    private $sellerCollection;

    /**
     * Product Ids array
     *
     * @var array
     */
    protected $_productIds = [];

    /**
     * class constructor
     *
     * @param \Magento\Wishlist\Model\WishlistFactory $wishlistFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Wishlist\Model\ItemFactory $itemFactory
     * @param \Arb\Wishlist\Model\Data\WishlistItemsFactory $wishlistItemsFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Webkul\Marketplace\Model\ResourceModel\Product\Collection $sellerCollection
     * @param TokenFactory $tokenModelFactory
     * @param Request $request
     */
    public function __construct(
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Wishlist\Model\ItemFactory $itemFactory,
        \Arb\Wishlist\Model\Data\WishlistItemsFactory $wishlistItemsFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\Marketplace\Model\ResourceModel\Product\Collection $sellerCollection,
        \Magento\Wishlist\Model\ResourceModel\Item\Option\CollectionFactory $optionCollectionFactory,
        TokenFactory $tokenModelFactory,
        Request $request,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable
    ) {
        $this->wishlistFactory = $wishlistFactory;
        $this->productRepository = $productRepository;
        $this->itemFactory = $itemFactory;
        $this->wishlistItemsFactory = $wishlistItemsFactory;
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->sellerCollection = $sellerCollection;
        $this->optionCollectionFactory = $optionCollectionFactory;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->request = $request;
        $this->configurable = $configurable;
    }

    /**
     * @param string $customerId customer id
     * @param string $sku product sku
     * @param mixed $optionData product option Data
     * @return array return wishlist id and item id
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function add($customerId, $sku, $optionData = [])
    {
        //Authorize User Token.
        $authorizationHeaderValue = $this->request->getHeader('Authorization');
        if (!$authorizationHeaderValue) {
            throw new LocalizedException(__('Token Invalid.'));
        }
        $isValidUser = $this->isValidUser($authorizationHeaderValue, $customerId);
        if (empty($isValidUser)) {
            throw new LocalizedException(__("Token Invalid."));
        }
        //End Authorization.
        $product = $this->productRepository->get($sku);
        if (!$product) {
            throw new LocalizedException(__('Requested order doesn\'t exist'));
        }
        $wishlist = $this->wishlistFactory->create()->loadByCustomerId($customerId, true);

        if ($optionData) {
            $options = [
                'product' => $product->getId(),
                'qty' => 1,
                'super_attribute' => $optionData,
                'parent_product_sku' => $product->getSku()
            ];

            $buyRequest = new \Magento\Framework\DataObject($options);

            $item = $wishlist->addNewItem($product, $buyRequest);
        } else {
            $item = $wishlist->addNewItem($product);
        }

        $wishlist->save();
        $response = [
            'wishlist_id' => $wishlist->getWishlistId(),
            'item_id' => $item->getId()
        ];
        return ['result' => $response];
    }

    /**
     * @param string $customerId customer id
     * @param int $itemId Wishlist Item id
     * @return boolean $result
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function remove($customerId, $itemId)
    {
        //Authorize User Token.
        $authorizationHeaderValue = $this->request->getHeader('Authorization');
        if (!$authorizationHeaderValue) {
            throw new LocalizedException(__('Token Invalid.'));
        }
        $isValidUser = $this->isValidUser($authorizationHeaderValue, $customerId);
        if (empty($isValidUser)) {
            throw new LocalizedException(__("Token Invalid."));
        }
        //End Authorization.
        $item = $this->itemFactory->create()->load($itemId);
        if (!$item->getId()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The requested Wish List Item doesn\'t exist.')
            );
        }
        $wishlistId = $item->getWishlistId();
        $wishlist = $this->wishlistFactory->create();
        $wishlist->load($wishlistId);

        if (!$wishlist || $wishlist->getCustomerId() != $customerId) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The requested Wish List doesn\'t exist.')
            );
        }
        $item->delete();
        $wishlist->save();
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getWishlistItems($customerId, $currentPage = 1, $pageSize = 10)
    {
        //Authorize User Token.
        $authorizationHeaderValue = $this->request->getHeader('Authorization');
        if (!$authorizationHeaderValue) {
            throw new LocalizedException(__('Token Invalid.'));
        }
        $isValidUser = $this->isValidUser($authorizationHeaderValue, $customerId);
        if (empty($isValidUser)) {
            throw new LocalizedException(__("Token Invalid."));
        }
        //End Authorization.
        $wishlist = $this->wishlistFactory->create()->loadByCustomerId($customerId, true);

        if ($wishlist->getId()) {
            $wishlistItems = $wishlist->getItemCollection();
            $wishlistItems->setOrder('added_at','DESC');
            $wishlistItems->setPageSize($pageSize)->setCurPage($currentPage);
            return $this->getItems($wishlistItems);
        } else {
            throw new LocalizedException(
                __('The requested Wish List doesn\'t exist.')
            );
        }
    }

    /**
     * return wishlist items array
     * @param \Magento\Wishlist\Model\ItemFactory[] $wishlistItems
     * @param int $pageSize
     * @param int $currentPage
     * @param int $customerId
     * @return array $items
     */
    private function getItems($wishlistItems)
    {
        $items = [];
        $itemsId = [];
        $options = [];
        $optionProducts = [];
        $itemProducts = [];
        $storeId = $this->storeManager->getStore()->getId();
        $itemIds = array_keys($wishlistItems->getItems());

        $optionCollection = $this->optionCollectionFactory->create();
        $optionCollection->addItemFilter($itemIds);

        $optionCollectionData = $optionCollection->getData();
        foreach ($optionCollectionData as $option) {
            if ($option['code'] == 'info_buyRequest') {
                $options[$option['wishlist_item_id']] = $option['value'];
            }
            $optionProducts[$option['wishlist_item_id']] = $option['product_id'];
        }

        foreach ($wishlistItems as $item) {
            $itemOptions[$item->getWishlistItemId()] = $options[$item->getWishlistItemId()];
            $itemProducts[$item->getWishlistItemId()] = $optionProducts[$item->getWishlistItemId()];
            $itemsId = $optionCollection->getProductIds();
        }

        $merchants = $this->getMerchantData($itemsId, $storeId);

        $products = $this->productFactory
            ->create()
            ->addAttributeToSelect([
                'name',
                'sku',
                'price',
                'special_price',
                'thumbnail',
                'id'
            ])
            ->addFieldToFilter('entity_id', ["in" => $itemsId]);
            
        foreach ($itemProducts as $addedItem) {
            foreach ($products as $product) {

                if ($product->getTypeId() == 'configurable') {
                    continue;
                }
                if ($addedItem == $product->getId()) {
                    
                    $merchantName = isset($merchants[$product->getId()]) ? $merchants[$product->getId()]['shop_title'] : '';
                    $merchantId = isset($merchants[$product->getId()]) ? $merchants[$product->getId()]['seller_id'] : 0;

                    $itemFactory = $this->wishlistItemsFactory->create();
                    $itemFactory->setItemId(array_search($product->getId(), $itemProducts));
                    $itemFactory->setName($product->getName());
                    $itemFactory->setSku($product->getSku());
                    $itemFactory->setPrice($product->getPrice());
                    $itemFactory->setSpecialPrice($product->getSpecialPrice());
                    $itemFactory->setImage($product->getThumbnail());
                    $itemFactory->setMerchantName($merchantName);
                    $itemFactory->setMerchantId($merchantId);
                    if (isset($options[$itemFactory->getItemId()]) && $options[$itemFactory->getItemId()] !== "[]") {
                        $itemFactory->setOptionData($options[$itemFactory->getItemId()]);
                        $product = $this->configurable->getParentIdsByChild($product->getId());
                        $product = $this->productRepository->getById($product[0]);
                        $itemFactory->setName($product->getName());
                        $itemFactory->setSku($product->getSku());
                    } else {
                        $itemFactory->setOptionData([]);
                    }
                    $items[] = $itemFactory;
                }
            }
        }
        return $items;
    }

    /**
     * get merchant data with respect to store & translations
     *
     * @param array $productIds
     * @param int $storeId
     * @return array
     */
    public function getMerchantData($productIds, $storeId)
    {
        //@codeCoverageIgnoreStart
        // code coverage will be get completed after the webkul team provides proper testcases for webkul model
        $productIds = implode(',', $productIds);

        $brandUserTable = $this->sellerCollection->getTable('marketplace_userdata');

        $brandCollection = $this->sellerCollection;

        $brandCollection->addFieldToFilter('main_table.mageproduct_id', ["in" => $productIds]);
        $brandCollection->addFieldToFilter('main_table.status', 1);

        $brandCollection->getSelect()->join(
            $brandUserTable . ' as band_store_default',
            'main_table.seller_id = band_store_default.seller_id
            AND band_store_default.store_id = 0',
            [
                'seller_id_default' => 'band_store_default.seller_id',
            ]
        );

        $brandCollection->getSelect()->joinLeft(
            $brandUserTable . ' as band_store',
            'main_table.seller_id = band_store.seller_id
            AND band_store.store_id = ' . $storeId,
            [
                'shop_title' => 'IF(band_store.shop_title != \'\', band_store.shop_title,
                        band_store_default.shop_title)',
                'store_id' => 'IF(band_store.store_id > 0, band_store.store_id,
                        band_store_default.store_id)'
            ]
        );

        $brandCollection->addFieldToSelect([
            'mageproduct_id',
            'seller_id'
        ]);

        $merchantData = [];
        if (count($brandCollection) >= 1) {
            foreach ($brandCollection->load() as $data) {
                $merchantData[$data['mageproduct_id']] = $data->getData();
            }
        }

        return $merchantData;
        //@codeCoverageIgnoreEnd
    }
    /**
     * Validate token for the user.
     *
     * @param string $authorizationHeaderValue
     * @param int $customerId
     * @return boolean
     */
    private function isValidUser($authorizationHeaderValue, $customerId)
    {
        $headerPieces = explode(" ", $authorizationHeaderValue);
        if (count($headerPieces) !== 2) {
            throw new LocalizedException(__('Token Invalid'));
        }
        $tokenType = strtolower($headerPieces[0]);
        if ($tokenType !== 'bearer') {
            throw new LocalizedException(__('Token Invalid'));
        }
        $customerToken = $headerPieces[1];
        //Validate Token
        $customerOuthModel = $this->tokenModelFactory->create();
        $customerOuthModel->load($customerToken, "token");
        $outhCustomerId = "";
        $outhCustomerId = $customerOuthModel->getCustomerId();
        if (empty($outhCustomerId)) {
            throw new LocalizedException(__('Token Invalid'));
        }
        if (!empty($customerOuthModel->getRevoked())) {
            throw new LocalizedException(__('Token Invalid'));
        }
        // Validating customer id associated with Token.
        if ($outhCustomerId != $customerId) {
            throw new LocalizedException(__('Token Invalid'));
        } else {
            return true;
        }
    }
}
