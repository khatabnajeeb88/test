<?php
/**
 * This file consist of class Products which is used to define single product and product listing.
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Model;

use Arb\API\Api\ProductsInterface;
use Arb\API\Helper\Product as ProductHelper;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductInterface as ProductParentInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\CatalogInventory\Helper\Stock;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\EntityManager\Operation\Read\ReadExtensions;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Webkul\Marketplace\Model\Product as WebkulProduct;
use Webkul\Marketplace\Model\ResourceModel\Seller\Collection as sellerCollection;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use \Magento\Catalog\Model\Api\SearchCriteria\ProductCollectionProcessor as SearchProcessor;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as MpProductCollection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;

/**
 * Consist of product and listing API funtions
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Products implements ProductsInterface
{
    /**
     * Inventory stock ID is used in following tables
     * inventory_stock
     * inventory_source
     * inventory_source_item
     * inventory_source_stock_link
     * This inventory stock ID stored in inventory_stock table where its default value is 1 and
     * This is linked with product SKUs in different inventory tables.
     */
    const DEFAULT_STOCK_ID = 1;

    /**
     * Product salable quantity allowed
     */
    const ALLOWED_SALABLE_QTY = 1;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var Product[]
     */
    protected $instances = [];

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $resourceModel;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Review model
     *
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $reviewFactory;

    /**
     * Review resource model
     *
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $reviewsColFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ReadExtensions
     */
    private $readExtensions;

    /**
     * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface
     */
    protected $stockItem;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * @var int
     */
    private $cacheLimit = 0;

    /**
     * @var SellerCollection
     */
    protected $sellerCollection;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var Stock
     */
    private $stockFilter;

    /**
     * @var MpProductCollection
     */
    private $mpProductCollectionFactory;

    /**
     * @var MpHelper
     */
    private $mpHelper;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * @var StockItemRepository
     */
    private $stockItemRepository;

    /**
     * @var StockItemCriteriaInterfaceFactory
     */
    private $stockItemCriteriaInterfaceFactory;

    /**
     * @var \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory
     */
    protected $voucherCollectionFactory;

    /**
     * Products Constructor
     *
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product $resourceModel
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionProductFactory
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param sellerCollection $sellerCollection
     * @param \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface $stockItem
     * @param \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Stock $stockFilter
     * @param MpProductCollection $mpProductCollectionFactory
     * @param MpHelper $mpHelper
     * @param ProductHelper $productHelper
     * @param \Magento\Framework\Serialize\Serializer\Json $serializer
     * @param ReadExtensions $readExtensions
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param StockItemRepository $stockItemRepository
     * @param StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory
     * @param integer $cacheLimit
     * @param \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $voucherCollectionFactory
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product $resourceModel,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionProductFactory,
        \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor,
        sellerCollection $sellerCollection,
        \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface $stockItem,
        \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory,
        CategoryRepositoryInterface $categoryRepository,
        Stock $stockFilter,
        MpProductCollection $mpProductCollectionFactory,
        MpHelper $mpHelper,
        ProductHelper $productHelper,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory $voucherCollectionFactory,
        ReadExtensions $readExtensions = null,
        CollectionProcessorInterface $collectionProcessor = null,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        StockItemRepository $stockItemRepository,
        StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory,
        $cacheLimit = 1000
    ) {
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionProductFactory;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->sellerCollection = $sellerCollection;
        $this->stockItem = $stockItem;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->voucherCollectionFactory  = $voucherCollectionFactory;
        $this->cacheLimit = (int) $cacheLimit;
        $this->readExtensions = $readExtensions ?: ObjectManager::getInstance()->get(ReadExtensions::class);
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
        $this->collectionProcessor = $collectionProcessor ?: ObjectManager::getInstance()->get(SearchProcessor::class);
        $this->categoryRepository = $categoryRepository;
        $this->stockFilter = $stockFilter;
        $this->mpProductCollectionFactory = $mpProductCollectionFactory;
        $this->mpHelper = $mpHelper;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productHelper = $productHelper;
        $this->stockItemRepository = $stockItemRepository;
        $this->stockItemCriteriaInterfaceFactory = $stockItemCriteriaInterfaceFactory;
    }

    /**
     * @inheritdoc
     */
    public function get($sku, $editMode = false, $storeId = null, $forceReload = false)
    {
        $cacheKey = $this->getCacheKey([$editMode, $storeId]);
        $cachedProduct = $this->getProductFromLocalCache($sku, $cacheKey);
        if ($cachedProduct === null || $forceReload) {
            $product = $this->productFactory->create();

            $productId = $this->resourceModel->getIdBySku($sku);
            if (!$productId) {
                throw new NoSuchEntityException(
                    __("The product that was requested doesn't exist. Verify the product and try again.")
                );
            }
            if ($editMode) {
                $product->setData('_edit_mode', true);
            }
            if ($storeId !== null) {
                $product->setData('store_id', $storeId);
            } else {
                $storeId = $this->storeManager->getStore()->getId();
            }
            $product->load($productId);
            $product = $this->getAttributeLabelForProducts($product);
            //Status 1 is for check voucher is approved and is_used 0 means voucher is available.
            $collection = $this->voucherCollectionFactory->create();
            $collection->addFieldToFilter('sku', $product->getSku())
                ->addFieldToFilter('is_used', 0)
                ->addFieldToFilter('status', 1);
            //Get Total vouchers available for the product.
            $voucherQty = $collection->getSize();

            $extensionattributes = $product->getExtensionAttributes();

            //Add saleble QTY in product detail API
            if ($product->getTypeId() !== 'configurable') {
                $stockQty = $this->stockItem->execute($product->getSku(), static::DEFAULT_STOCK_ID);
            } else {
                $configurableData = $this->getConfigurableProductData($product);
                $stockQty = $configurableData['stock_qty'];
                $productOptionsData = $this->prepareConfigurableOptionsData($product);
                $extensionattributes->setConfigurableProductChildren(json_encode($productOptionsData));
                $extensionattributes->setConfigurableProductPrice($configurableData['price']);
            }

            // setting values for product options so that not only index is visible in response
            if ($product->getTypeId() === 'configurable') {
                $productOptions = $extensionattributes->getConfigurableProductOptions();
                if (!empty($productOptions)) {
                    /** @var Attribute $productOption */
                    foreach ($productOptions as $productOption) {
                        $productOption->setData('use_default', 1);
                        $productOption->setValues($productOption->getOptions());
                    }
                }
            }

            $extensionattributes->setSalableQty($stockQty);
            $extensionattributes->setVoucherQty($voucherQty);
            $product->setExtensionAttributes($extensionattributes);
            $this->cacheProduct($cacheKey, $product);
            $cachedProduct = $product;
        }
        return $cachedProduct;
    }

    /**
     * Sets dropdown attribute label
     *
     * @param Product $product
     *
     * @return Product
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    // phpcs:disable Generic.Metrics.NestingLevel.TooHigh
    private function getAttributeLabelForProducts(Product $product)
    {
        $excludeAttributes = [];
        if (null != $product->getAttributes()) {
            $attributes = $product->getAttributes();

            foreach ($attributes as $attribute) {
                $data = [];
                if ($this->isVisibleOnFrontend($attribute, $excludeAttributes)) {
                    $code = $attribute->getAttributeCode();
                    $storeId = $this->storeManager->getStore()->getId();
                    $value = $product->getResource()->getAttributeRawValue($product->getId(), $code, (int)$storeId);
                    if ($attribute->getFrontendInput() == 'select' && (null != $attribute->getAttributeCode())) {
                        try {
                            if (is_string($value)) {
                                $value = $attribute->getSource()->getOptionText($value);
                                $attributeCheck = $product->getResource()->getAttribute($code);
                                if ($attributeCheck->usesSource()) {
                                    $optionId = $attributeCheck->getSource()->getOptionId($value);
                                }
                                if (is_string($value) && strlen($value)) {
                                    $data[$attribute->getAttributeCode()] = [
                                        'title' => $attribute->getStoreLabel(),
                                        'label' => __($value),
                                        'code' => $optionId,
                                        'visible_on_storefront' => $attribute->getIsVisibleOnFront(),
                                    ];
                                }

                                $product->setCustomAttribute($attribute->getAttributeCode(), $data);
                            }
                        } catch (\Exception $e) {
                            throw new LocalizedException(
                                __('Unable to display products'),
                                $e
                            );
                        }
                    } elseif ($attribute->getFrontendInput() !== 'multiselect' && !empty($value)) {
                        if ($value instanceof Phrase) {
                            $value = (string)$value;
                        }

                        if (is_string($value) && strlen($value)) {
                            $data[$attribute->getAttributeCode()] = [
                                'title' => __($attribute->getStoreLabel()),
                                'label' => __($value),
                                'visible_on_storefront' => $attribute->getIsVisibleOnFront()
                            ];
                        }

                        $product->setCustomAttribute($attribute->getAttributeCode(), $data);
                    }
                }
            }
        }
        return $product;
    }

    /**
     * Get key for cache
     *
     * @param array $data
     *
     * @return string
     */
    protected function getCacheKey($data)
    {
        $serializeData = [];
        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $serializeData[$key] = $value->getId();
            } else {
                $serializeData[$key] = $value;
            }
        }
        $serializeData = $this->serializer->serialize($serializeData);
        return sha1($serializeData);
    }

    /**
     * Gets product from the local cache by SKU.
     *
     * @param string $sku
     * @param string $cacheKey
     *
     * @return Product|null
     */
    private function getProductFromLocalCache(string $sku, string $cacheKey)
    {
        $preparedSku = $this->prepareSku($sku);

        return $this->instances[$preparedSku][$cacheKey] ?? null;
    }

    /**
     * Checks if attribute is visible on frontend
     *
     * @param \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute
     * @param array $excludeAttributes
     *
     * @return boolean
     */
    protected function isVisibleOnFrontend(
        \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute,
        array $excludeAttributes
    ) {
        return ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttributes));
    }

    /**
     * @inheritdoc
     */
    public function getById($productId, $editMode = false, $storeId = null, $forceReload = false)
    {
        $cacheKey = $this->getCacheKey([$editMode, $storeId]);
        if (!isset($this->instancesById[$productId][$cacheKey]) || $forceReload) {
            $product = $this->productFactory->create();
            if ($editMode) {
                $product->setData('_edit_mode', true);
            }
            if ($storeId !== null) {
                $product->setData('store_id', $storeId);
            }
            $product->load($productId);
            if (!$product->getId()) {
                throw new NoSuchEntityException(
                    __("The product that was requested doesn't exist. Verify the product and try again.")
                );
            }
            $this->cacheProduct($cacheKey, $product);
        }
        return $this->instancesById[$productId][$cacheKey];
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collectionVirtual = $this->prepareCollection($searchCriteria, 'virtual');
        $collectionSimple =$this->prepareCollection($searchCriteria, 'simple');

        $searchResult = $this->searchResultsFactory->create();

        $isPriceFiltered = false;
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if (in_array($filter->getField(), array('special_price', 'price'))) {
                    $isPriceFiltered = true;
                }
            }
        }

        $collectionsItemsMerged = array_merge(
            $collectionVirtual->getItems(),
            $collectionSimple->getItems());
        if ($isPriceFiltered) {
            $prices = $this->productHelper->getCorrectPrices($collectionsItemsMerged);

            $itemsToDelete = $this->getItemsToDelete($prices, $searchCriteria);
            if (!empty($itemsToDelete)) {
                $collectionsItemsMerged = $this->deleteItemsFromArray($collectionsItemsMerged, $itemsToDelete);
            }
        }

        // sorting
        if ($searchCriteria->getSortOrders()) {
            $collectionsItemsMerged = $this->productHelper->sortMergedProductCollectionItems(
                $collectionsItemsMerged,
                $searchCriteria
            );
        }

        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collectionsItemsMerged);
        $searchResult->setTotalCount(count($collectionsItemsMerged));
        return $searchResult;
    }

    /**
     * Preparing collection before merging
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @param string $type
     *
     * @return Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function prepareCollection(
        SearchCriteriaInterface $searchCriteria,
        string $type
    ) {
        $collection = $this->collectionFactory->create();

        if ($type === 'virtual') {
            $collection->addAttributeToFilter('type_id', Type::TYPE_VIRTUAL);
        } else {
            $collection->addAttributeToFilter(
                'type_id',
                array('in' => array(Type::TYPE_SIMPLE, Configurable::TYPE_CODE))
            );
        }

        $this->extensionAttributesJoinProcessor->process($collection);
        $storeId = $this->storeManager->getStore()->getId();
        // only required attributes are selected
        $collection->addAttributeToSelect(
            [
                'bestseller',           // shows if the product is best seller
                'price',                // returns the price of product
                'special_price',        // product special price
                'name',                 // product name to display in product listing
                'sku',                  // product unique sku identifier
                'image',                // product full size image for mobile team to display
                'thumbnail',            // product thumbnail size image for mobile team to identify
                'special_from_date',    // product special price from date
                'special_to_date'       // product special price to date  
            ]
        );
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        $this->collectionProcessor->process($searchCriteria, $collection);

        // get merchant data for product with proper store data
        $collection->getSelect()->join(
            ['mp'=>'marketplace_product'],
            'mp.mageproduct_id = e.entity_id',
            ['seller_id'=>'mp.seller_id']
        );
        $collection->getSelect()->join(
            ['stock'=>'cataloginventory_stock_item'],
            'stock.product_id = e.entity_id',
            ['notify_stock_qty '=>'stock.notify_stock_qty ']
        );

        $collection->getSelect()->join(
            ['band_user_default'=>'marketplace_userdata'],
            'band_user_default.seller_id = mp.seller_id
            AND band_user_default.store_id = 0',
            [
                'band_user_default.seller_id',
            ]
        );

        $collection->getSelect()->joinLeft(
            ['marketplace_userdata'],
            'marketplace_userdata.seller_id = mp.seller_id
             AND marketplace_userdata.store_id = ' . $storeId,
            [
                'marketplace_userdata.banner_pic',
                'marketplace_userdata.shop_title',
                'marketplace_userdata.logo_pic',
                'marketplace_userdata.store_id',
            ]
        );

        //stock status filter check for isSalable
        $collection->joinField(
            'qty',
            'cataloginventory_stock_status',
            'qty',
            'product_id=entity_id',
            '{{table}}.stock_id = 1 AND {{table}}.stock_status=1'
        );

        if ($type === 'virtual') {
            //check attached active vouchers for products to checkout
            $voucherQuery = new \Zend_Db_Expr('(SELECT arb_vouchers.sku FROM arb_vouchers
        WHERE status = 1 AND is_used = 0 AND voucher_expiry > now() GROUP BY sku)');
            $collection->getSelect()->join(
                ['at_voucher_sku' => $voucherQuery],
                'at_voucher_sku.sku = e.sku',
                []
            );
        }

        $collection->load();
        $collection->addCategoryIds();
        $this->addExtensionAttributes($collection);
        $this->getAttributeLabelForList($collection);
        return $collection;
    }

    /**
     * Deleting items from array
     *
     * @param array $items
     * @param array $itemsToDelete
     *
     * @return array
     */
    private function deleteItemsFromArray(array $items, array $itemsToDelete)
    {
        foreach ($items as $item => $value) {
            if (in_array($value->getId(), $itemsToDelete)) {
                unset($items[$item]);
            }
        }

        return $items;
    }

    /**
     * Filtering if items correct prices are in a price range
     *
     * @param array $prices
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    private function getItemsToDelete(array $prices, SearchCriteriaInterface $searchCriteria)
    {
        $itemsToDelete = [];
        $conditions = [];
        $minPrice = null;
        $maxPrice = null;
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'price') {
                    $conditions[$filter->getConditionType()] = $filter->getValue();
                }
            }
        }

        foreach ($conditions as $key => $value) {
            if ($key === 'gteq') {
                $minPrice = $value;
            }

            if ($key === 'lteq') {
                $maxPrice = $value;
            }
        }

        foreach ($prices as $key => $value) {
            if (!$this->isPriceInRange($value, $minPrice, $maxPrice)) {
                $itemsToDelete[] = $key;
            }
        }

        return $itemsToDelete;
    }

    /**
     * Checking if value (price) is between min and max
     *
     * @param string $value
     * @param string $min
     * @param string $max
     *
     * @return bool
     */
    private function isPriceInRange(string $value, string $min, string $max)
    {
        return ($min <= $value) && ($value <= $max);
    }

    /**
     * Sets dropdown label in array for loaded products
     *
     * @param Collection $collection
     *
     * @return void
     */
    protected function getAttributeLabelForList(Collection $collection)
    {
        foreach ($collection->getItems() as $product) {
            if (null != $product->getAttributes()) {
                $attributes = $product->getAttributes();
                // Removing product from collection if salable qty is less than 1
                if ($product->getTypeId() !== 'configurable') {
                    $stockQty = $this->stockItem->execute($product->getSku(), static::DEFAULT_STOCK_ID);
                } else {
                    $configurableData = $this->getConfigurableProductData($product);
                    $stockQty = $configurableData['stock_qty'];
                }
                if ($stockQty < static::ALLOWED_SALABLE_QTY) {
                    $collection->removeItemByKey($product->getId());
                }
                $this->processProductListing($attributes, $product);
            }

            $this->cacheProduct(
                $this->getCacheKey(
                    [
                        false,
                        $product->getStoreId(),
                    ]
                ),
                $product
            );
        }
    }

    /**
     * Prcess dropdown attributes from dropdown option id to label
     *
     * @param array $attributes
     * @param mixed $product
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function processProductListing($attributes, $product)
    {
        $excludeAttributes = [];
        foreach ($attributes as $attribute) {
            if ($this->isVisibleOnFrontend($attribute, $excludeAttributes)) {
                if ($attribute->getFrontendInput() == 'select' && (null != $attribute->getAttributeCode())) {
                    $code = $attribute->getAttributeCode();
                    // phpcs:disable Generic.Metrics.NestingLevel.TooHigh
                    try {
                        $value = $product->getResource()->getAttributeRawValue($product->getId(), $code, '1');
                        if (is_string($value)) {
                            $value = $attribute->getSource()->getOptionText($value);
                            $attributeCheck = $product->getResource()->getAttribute($code);
                            if ($attributeCheck->usesSource()) {
                                $optionId = $attributeCheck->getSource()->getOptionId($value);
                            }

                            $data = [];
                            if (is_string($value) && strlen($value)) {
                                $data[$attribute->getAttributeCode()] = [
                                    'title' => $attribute->getStoreLabel(),
                                    'label' => __($value),
                                    'code' => $optionId,
                                ];
                            }
                            $product->setCustomAttribute($attribute->getAttributeCode(), $data);
                        }
                    } catch (\Exception $e) {
                        throw new LocalizedException(
                            __('Unable to display products'),
                            $e
                        );
                    }
                }
            }
        }
    }

    /**
     * Add extension attributes to loaded items.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    private function addExtensionAttributes(Collection $collection)
    {
        foreach ($collection->getItems() as $item) {
            $this->readExtensions->execute($item);
            $this->processSellerListing($item);
        }
        return $collection;
    }

    /**
     * Add product to internal cache and truncate cache if it has more than cacheLimit elements.
     *
     * @param string $cacheKey
     * @param ProductParentInterface $product
     *
     * @return void
     */
    private function cacheProduct($cacheKey, ProductParentInterface $product)
    {
        $this->instancesById[$product->getId()][$cacheKey] = $product;
        $this->saveProductInLocalCache($product, $cacheKey);

        if ($this->cacheLimit && count($this->instances) > $this->cacheLimit) {
            $offset = round($this->cacheLimit / -2);
            $this->instancesById = array_slice($this->instancesById, $offset, null, true);
            $this->instances = array_slice($this->instances, $offset, null, true);
        }
    }

    /**
     * Saves product in the local cache by sku.
     *
     * @param Product $product
     * @param string $cacheKey
     *
     * @return void
     */
    private function saveProductInLocalCache(Product $product, string $cacheKey)
    {
        $preparedSku = $this->prepareSku($product->getSku());
        $this->instances[$preparedSku][$cacheKey] = $product;
    }

    /**
     * Converts SKU to lower case and trims.
     *
     * @param string $sku
     *
     * @return string
     */
    private function prepareSku(string $sku)
    {
        return mb_strtolower(trim($sku));
    }

    /**
     * function to get product merchant attributes and quentity
     *
     * @param Product $product
     *
     * @return void
     */
    protected function processSellerListing($product)
    {
        $extensionattributes = $product->getExtensionAttributes();
        if (null != $product) {
            if ($product->getTypeId() !== 'configurable') {
                $stockQty = $this->stockItem->execute($product->getSku(), static::DEFAULT_STOCK_ID);
            } else {
                $configurableData = $this->getConfigurableProductData($product);
                $stockQty = $configurableData['stock_qty'];
                $extensionattributes->setConfigurableProductPrice($configurableData['price']);
            }

            //add category name in PLP
            $storeId= $product->getData('store_id');
            $categoryList = $this->getProductCategories($product, $storeId);

            $extensionattributes->setProductQty($product->getQty());
            $extensionattributes->setMerchantId($product->getData('seller_id'));
            $extensionattributes->setMerchantStoreName($product->getData('shop_title'));
            $extensionattributes->setMerchantBanner($product->getData('banner_pic'));
            $extensionattributes->setProductCategories($categoryList);
            $extensionattributes->setMerchantLogo($product->getData('logo_pic'));
            $extensionattributes->setMerchantStoreId($product->getData('store_id'));
            $extensionattributes->setLowStockDate($this->getLowStockDate($product));
            $extensionattributes->setSalableQty($stockQty);
        }
        $product->setExtensionAttributes($extensionattributes);
    }

    /**
     * Getting brands in format specified by document
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getBrands()
    {
        $brands = [];
        $brandsValues = [];
        $storeId = $this->storeManager->getStore()->getId();
        $collection = $this->collectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addStoreFilter($storeId);
        $collection->addAttributeToFilter('status', Status::STATUS_ENABLED);
        $collection->setFlag('has_stock_status_filter', false);
        $this->stockFilter->addInStockFilterToCollection($collection);
        $products = $collection->getItems();

        /** @var Product $product */
        foreach ($products as $product) {
            $brandAttribute = $product->getCustomAttribute('brand');

            if ($brandAttribute === null) {
                continue;
            }

            $brandValue = $brandAttribute->getValue();
            if (!in_array($brandValue, $brandsValues)) {
                $brandsValues[] = $brandValue;

                $brands[] = [
                    'brand' => $brandValue,
                    'merchants' => $this->getProductMerchant($product),
                    'categories' => $this->getProductCategories($product, $storeId),
                ];
            } else {
                foreach ($brands as $key => $value) {
                    if ($value['brand'] === $brandValue) {
                        $brands[$key] = $this->updateBrand($product, $storeId, $value);
                        continue;
                    }
                }
            }

        }

        return $brands;
    }

    /**
     * Getting products sellers shop title
     *
     * @param Product $product
     *
     * @return array
     */
    private function getProductMerchant(Product $product)
    {
        $sellerProductColls = $this->mpProductCollectionFactory->create()
            ->addFieldToFilter(
                'mageproduct_id',
                $product->getId()
            );
        /** @var WebkulProduct $item */
        if (!empty($sellerProductColls)) {
            $item = $sellerProductColls->getFirstItem();
        }

        $sellerId = $item->getData('seller_id');

        $webkulMerchant = $this->mpHelper->getSellerCollectionObj($sellerId);
        $shopTitle = [];
        foreach ($webkulMerchant->getItems() as $merchant) {
            if (!empty($merchant->getShopTitle())) {
                $shopTitle[] = $merchant->getShopTitle();
            }
        }

        return $shopTitle;
    }

    /**
     * Getting products category names
     *
     * @param Product $product
     * @param string $storeId
     *
     * @return array
     * @throws NoSuchEntityException
     */
    private function getProductCategories(Product $product, $storeId)
    {
        $categories = [];
        $categoryIds = $product->getCategoryIds();
        foreach ($categoryIds as $categoryId) {
            $category = $this->categoryRepository->get($categoryId, $storeId);
            $categories[] = $category->getName();
        }

        return $categories;
    }

    /**
     * Updating brand array
     *
     * @param Product $product
     * @param string $storeId
     * @param array $brandArray
     *
     * @return array
     * @throws NoSuchEntityException
     */
    private function updateBrand(Product $product, string $storeId, array $brandArray)
    {
        $merchant = $this->getProductMerchant($product);
        $brandArray['merchants'] = array_values(array_unique(array_merge($merchant, $brandArray['merchants'])));

        $categories = $this->getProductCategories($product, $storeId);
        $brandArray['categories'] = array_values(array_unique(array_merge($categories, $brandArray['categories'])));

        return $brandArray;
    }

    /**
     * @param string $type
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     *
     * @throws LocalizedException
     */
    public function getLinkedProductsList(string $type, SearchCriteriaInterface $searchCriteria)
    {
        if (!in_array($type, static::LINKS)) {
            throw new LocalizedException(__('Invalid type.'));
        }

        $linkedData = [];
        $linkedIds = [];

        $productListSearchCriteria = $this->searchCriteriaBuilder
            ->create()
            ->setFilterGroups($searchCriteria->getFilterGroups());
        $productList = $this->productRepository->getList($productListSearchCriteria);

        foreach ($productList->getItems() as $product) {
            switch ($type) {
                case static::UPSELL:
                    $linkedProducts = $product->getUpSellProductIds();
                    break;
                case static::CROSSSELL:
                    $linkedProducts = $product->getCrossSellProductIds();
                    break;
                case static::RELATED:
                    $linkedProducts = $product->getRelatedProductIds();
                    break;
                default:
                    throw new LocalizedException(__('Invalid type.'));
            }

            foreach ($linkedProducts as $linkedProductId) {
                if (in_array($linkedProductId, $linkedIds)) {
                    continue;
                }
                $linkedIds[] = $linkedProductId;
                $linkedProduct = $this->productRepository->getById($linkedProductId);
                $stockStatus = $linkedProduct->getQuantityAndStockStatus();
                $linkedData[] = [
                    'sku' => $linkedProduct->getSku(),
                    'name' => $linkedProduct->getName(),
                    'link_type' => $type,
                    'linked_product_sku' => $product->getSku(),
                    'linked_product_type' => $product->getTypeId(),
                    'price' => $linkedProduct->getPrice(),
                    'special_price' => $linkedProduct->getSpecialPrice(),
                    'special_price_from_date' => $linkedProduct->getSpecialFromDate(),
                    'special_price_to_date' => $linkedProduct->getSpecialToDate(),
                    'image' => $linkedProduct->getImage(),
                    'stock' => $stockStatus ? (int)$stockStatus['qty'] : 0
                ];
            }
        }

        return $linkedData;
    }

    /**
     * Get Salable Quantity for Configurable Product consisting of its Childrens Salable Quantities
     *
     * @param Product $product
     *
     * @return array
     *
     * @throws InputException
     * @throws LocalizedException
     */
    private function getConfigurableProductData(Product $product)
    {
        $stockQty = 0;
        $extensionAttributes = $product->getExtensionAttributes();
        $childrenIds = $extensionAttributes->getConfigurableProductLinks();
        $childrenPrices = [];
        foreach ($childrenIds as $childrenId) {
            try {
                $childrenProduct = $this->productRepository->getById($childrenId);
                $childrenPrices[] = $childrenProduct->getFinalPrice();
            } catch (NoSuchEntityException $e) {
                continue;
            }
            $childrenStock = $this->stockItem->execute($childrenProduct->getSku(), static::DEFAULT_STOCK_ID);
            $stockQty += $childrenStock;
        }

        return ['stock_qty' => $stockQty, 'price' => min($childrenPrices)];
    }

    /**
     * Prepare array with data about Configurable Product children options
     *
     * @param Product $product
     *
     * @return array
     *
     * @throws InputException
     * @throws LocalizedException
     */
    private function prepareConfigurableOptionsData(Product $product)
    {
        $optionsArray = $product->getTypeInstance()->getConfigurableOptions($product);
        $productOptionsData = [];
        foreach ($optionsArray as $attributeId => $options) {
            foreach ($options as $option) {
                if (array_key_exists($option['sku'], $productOptionsData)) {
                    $productOptionsData[$option['sku']]['options'][$attributeId] = $option['value_index'];
                } else {
                    $product = $this->productRepository->get($option['sku']);

                    // child product images
                    $productImages = $product->getMediaGalleryImages();           
                    $childImage = [];
                    foreach ($productImages as $image) {
                        $childImage[] = $image->getFile();
                    }

                    $productOptionsData[$option['sku']] = [
                        'name' => $product->getName(),
                        'options' => [$attributeId => $option['value_index']],
                        'salable_qty' => $this->stockItem->execute($option['sku'], static::DEFAULT_STOCK_ID),
                        'low_stock_date' => $this->getLowStockDate($product),
                        'price' => $product->getPrice(),
                        'image_src' => $childImage,
                        'special_price' => $product->getSpecialPrice(),
                        'special_from_date' => $product->getSpecialFromDate(),
                        'special_to_date' => $product->getSpecialToDate()
                    ];
                }
            }
        }

        return $productOptionsData;
    }

    /**
     * @param ProductParentInterface $product
     *
     * @return string|null
     */
    private function getLowStockDate(ProductInterface $product)
    {
        $searchCriteria = $this->stockItemCriteriaInterfaceFactory->create();
        $searchCriteria->setProductsFilter($product->getId());
        $stockItemsList = $this->stockItemRepository->getList($searchCriteria);
        $stockItem = current($stockItemsList->getItems());

        return $stockItem ? $stockItem->getLowStockDate() : null;
    }
}
