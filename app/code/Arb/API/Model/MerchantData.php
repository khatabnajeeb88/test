<?php
/**
 * This file consist of class Merchant which is used to define product listing for merchant.
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */
namespace Arb\API\Model;

use Arb\API\Helper\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface as attributeProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface as storeManagerInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\Collection as sellerCollection;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as productFactory;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface as sourceDataBySku;
use Magento\Framework\App\ObjectManager;
use \Magento\Catalog\Model\Api\SearchCriteria\ProductCollectionProcessor as SearchProcessor;
use \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory;
use \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

/**
 * class to get merchant product data
 */
class MerchantData
{
    /**
     * Inventory stock ID is used in following tables
     * inventory_stock
     * inventory_source
     * inventory_source_item
     * inventory_source_stock_link
     * This inventory stock ID stored in inventory_stock table where its default value is 1 and
     * This is linked with product SKUs in different inventory tables.
     */
    const DEFAULT_STOCK_ID = 1;

    /**
     * Product salable quantity allowed
     */
    const ALLOWED_SALABLE_QTY = 1;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var SellerCollection
     */
    protected $sellerCollection;

    /**
     * merchant product Ids
     *
     * @var array
     */
    protected $productIds;

    /**
     * get smerchant data for given id
     *
     * @var [type]
     */
    protected $sellerData;

    /**
     * vendor country attribute code
     *
     * @var string
     */
    protected $vendorCountryAttr = 'vendor_country';

    /**
     * subcategory attribute
     *
     * @var string
     */
    protected $arbSubCategory = 'arb_subcategory';

    /**
     * get product collection class
     *
     * @var object
     */
    protected $prodCollFactory;

    /**
     * get product attrbutes
     *
     * @var [type]
     */
    protected $extnAttriJoinProc;

    /**
     * get product stock class
     *
     * @var [type]
     */
    protected $sourceDataBySku;

    /**
     * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * gallery images
     */
    protected $imageType = 'thumbnail';

    /**
     * @var Product
     */
    private $productHelper;

    /**
     * Merchant Construct
     *
     * @param storeManagerInterface $storeManager
     * @param sellerCollection $sellerCollFactory
     * @param productFactory $collProdFactory
     * @param sourceDataBySku $sourceDataBySku
     * @param attributeProcessor $extnAttriJoinProc
     * @param ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param Product $productHelper
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        storeManagerInterface $storeManager,
        sellerCollection $sellerCollFactory,
        productFactory $collProdFactory,
        sourceDataBySku $sourceDataBySku,
        attributeProcessor $extnAttriJoinProc,
        ProductSearchResultsInterfaceFactory $searchResultsFactory,
        Product $productHelper,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->storeManager = $storeManager;
        $this->sellerCollection = $sellerCollFactory;
        $this->prodCollFactory = $collProdFactory;
        $this->extnAttriJoinProc = $extnAttriJoinProc;
        $this->sourceDataBySku = $sourceDataBySku;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor ?: ObjectManager::getInstance()->get(SearchProcessor::class);
        $this->productHelper = $productHelper;
    }

    /**
     * get merchant listing data
     *
     * @param int $sellerId
     * @param Obj $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSellerProducts($sellerId, $searchCriteria)
    {
        try {
            $storeId = $this->storeManager->getStore()->getId();
            //get merchant data
            $this->getSellerData($sellerId, $storeId);
            //get merchant prodcut data
            $sellerProductsData = $this->getProductData($searchCriteria);

            return $sellerProductsData;
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Unable to display products'),
                $e
            );
        }
    }

    /**
     * Returns product data
     *
     * @param Obj $searchCriteria
     *
     * @return array
     * @throws LocalizedException
     * @throws InputException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getProductData($searchCriteria)
    {
        $collectionVirtual = $this->prepareCollection($searchCriteria, 'virtual');
        $collectionSimple = $this->prepareCollection($searchCriteria, 'simple');

        $collectionsItemsMerged = array_merge($collectionVirtual->getItems(), $collectionSimple->getItems());
        $collectionsItemsMerged = $this->productHelper->sortMergedProductCollectionItems(
            $collectionsItemsMerged,
            $searchCriteria
        );

        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collectionsItemsMerged);
        $searchResult->setTotalCount(count($collectionsItemsMerged));

        //add extra fields to products
        $collectionData = [];
        foreach ($searchResult->getItems() as $prod) {
            if ($prod->getTypeId() !== 'configurable') {
                $stockQty = $this->sourceDataBySku->execute($prod->getSku(), self::DEFAULT_STOCK_ID);
                if ($stockQty < self::ALLOWED_SALABLE_QTY) {
                    continue;
                }

                //set collection date
                $collectionData[$prod['entity_id']] = $prod->getData();
                //set product stock
                $collectionData[$prod['entity_id']]['stock'] = $prod->getQty();
                //set category data
                $collectionData[$prod['entity_id']]['category'] = $prod->getCategoryIds();
                //set  merchant data
                $collectionData[$prod['entity_id']]['merchant'] = $this->sellerData[$prod['entity_id']];

                // gallery image
                if ($prod->getThumbnail()) {
                    $collectionData[$prod['entity_id']]['media_gallery_entries'] = [
                        (object)['types' => [$this->imageType], 'file' => $prod->getThumbnail()]
                    ];
                    unset($collectionData[$prod['entity_id']][$this->imageType]);
                }
                //set country attribute data
                if ($prod[$this->vendorCountryAttr]) {
                    $collectionData[$prod['entity_id']][$this->vendorCountryAttr] = [
                        'id' => (int)$prod[$this->vendorCountryAttr],
                        'label' => $prod->getAttributeText($this->vendorCountryAttr),
                    ];
                }
                //set sub category attribute data
                if ($prod[$this->arbSubCategory]) {
                    $collectionData[$prod['entity_id']][$this->arbSubCategory] = [
                        'id' => (int)$prod[$this->arbSubCategory],
                        'label' => $prod->getAttributeText($this->arbSubCategory),
                    ];
                }
            }
        }

        return $collectionData;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param string $type
     *
     * @return Collection
     * @throws LocalizedException
     */
    private function prepareCollection(SearchCriteriaInterface $searchCriteria, string $type)
    {
        $collection = $this->prodCollFactory->create();

        if ($type === 'virtual') {
            $collection->addAttributeToFilter('type_id', Type::TYPE_VIRTUAL);
        } else {
            $collection->addAttributeToFilter(
                'type_id',
                array('in' => array(Type::TYPE_SIMPLE, Configurable::TYPE_CODE))
            );
        }

        $this->extnAttriJoinProc->process($collection);
        $collection->addAttributeToSelect(['sku','name','price','special_price',
            $this->imageType,$this->vendorCountryAttr, $this->arbSubCategory]);
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        //set filters for product listing
        $collection->addFieldToFilter('entity_id', ['in' => $this->productIds]);
        $collection->addFieldToFilter('status', 1);
        $collection->addFieldToFilter('visibility', ['in' => [2,4]]);

        //stock status filter check for isSalable
        $collection->joinField(
            'qty',
            'cataloginventory_stock_status',
            'qty',
            'product_id=entity_id',
            '{{table}}.stock_id = 1 AND {{table}}.stock_status=1'
        );

        //check attached active vouchers for products to checkout
        if ($type === 'virtual') {
            $voucherQuery = new \Zend_Db_Expr('(SELECT arb_vouchers.sku FROM arb_vouchers
        WHERE status = 1 AND is_used = 0 AND voucher_expiry > now() GROUP BY sku)');
            $collection->getSelect()->join(
                ['at_voucher_sku' => $voucherQuery],
                'at_voucher_sku.sku = e.sku',
                []
            );
        }

        //process the collection
        $this->collectionProcessor->process($searchCriteria, $collection);

        $collection->load();

        return $collection;
    }

    /**
     * get seller data by store
     *
     * @param integer $storeId
     * @return void
     */
    public function getSellerData($sellerId, $storeId = 0)
    {
        $brandProdTable = $this->sellerCollection->getTable('marketplace_product');

        $brandCollection = $this->sellerCollection;
        $brandCollection->addFieldToFilter('main_table.seller_id', $sellerId);
        $brandCollection->addFieldToFilter('main_table.store_id', $storeId);
        // If seller data doesn't exist for current store
        if (!$brandCollection->getSize()) {
            $brandCollection->clear()->getSelect()->reset(\Zend_Db_Select::WHERE);
            $brandCollection->addFieldToFilter('main_table.seller_id', $sellerId);
            $brandCollection->addFieldToFilter('main_table.store_id', 0);
        }

        $brandCollection->getSelect()->join(
            $brandProdTable . ' as band_prod_default',
            'main_table.seller_id = band_prod_default.seller_id
            AND band_prod_default.store_id = 0
            AND band_prod_default.status = 1',
            [
                'mageproduct_id_default' => 'band_prod_default.mageproduct_id',
                'product_store_id_default' => 'band_prod_default.store_id',
            ]
        );

        $brandCollection->getSelect()->joinLeft(
            $brandProdTable . ' as band_prod',
            'main_table.seller_id = band_prod.seller_id
            AND band_prod.store_id = ' . $storeId . '
            AND band_prod.status = 1',
            [
                'mageproduct_id' => 'band_prod.mageproduct_id',
                'product_store_id' => 'band_prod.store_id',
                'seller_mageproduct_id' => 'IF(band_prod.mageproduct_id > 0, band_prod.mageproduct_id,
                    band_prod_default.mageproduct_id)',
                'entity_id' => 'IF(band_prod.entity_id > 0, band_prod.entity_id, band_prod_default.entity_id)',
            ]
        );

        $brandCollection->addFieldToSelect([
            'seller_id',
            'banner_pic',
            'shop_title',
            'logo_pic',
            'country_pic',
            'company_description',
            'store_id',
            'contact_number',
        ]);

        $merchantData = [];
        $productIds = [];
        if (count($brandCollection) >= 1) {
            foreach ($brandCollection->load() as $data) {
                $merchantData[$data['seller_mageproduct_id']] = $data->getData();
                unset($data['mageproduct_id_default']);
                $productIds[] = $data['seller_mageproduct_id'];
            }
        }
        $this->productIds = $productIds;
        $this->sellerData = $merchantData;
    }
}
