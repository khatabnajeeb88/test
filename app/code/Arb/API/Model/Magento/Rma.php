<?php
/**
 * Overridden Magento RMA file to add getter/setter
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Model\Magento;

use Arb\API\Api\Magento\Rma\Data\RmaInterface;
use Magento\Rma\Model\Rma as ParentRma;

class Rma extends ParentRma implements RmaInterface
{
    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->getData('customer');
    }

    /**
     * @param $customerData
     *
     * @return mixed
     */
    public function setCustomer($customerData)
    {
        return $this->setData('customer', $customerData);
    }
}
