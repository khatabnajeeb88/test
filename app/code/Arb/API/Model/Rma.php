<?php
/**
 * This file consist of class Rma which exposes custom methods to API
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Model;

use Arb\API\Api\RmaInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Temando\Shipping\Model\Order\OrderItem;
use Webkul\MpRmaSystem\Helper\Data;
use Webkul\MpRmaSystem\Model\ResourceModel\Reasons\CollectionFactory as ReasonsCollection;
use Magento\Rma\Helper\Eav;
use Webkul\MpRmaSystem\Model\DetailsFactory;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Exception;
use Webkul\Marketplace\Model\OrdersRepository;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Webapi\Request;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\Sales\Model\OrderFactory;

class Rma implements RmaInterface
{
    /**
     * @var ReasonsCollection
     */
    private $reasonsCollection;

    /**
     * @var Eav
     */
    private $eav;

    /**
     * @var DetailsFactory
     */
    private $detailsFactory;

    /**
     * @var Data
     */
    private $rmaHelper;

    /**
     * @var OrderItemRepositoryInterface
     */
    private $orderItemRepository;

    /**
     * @var ProductCollection
     */
    private $collectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    protected $tokenModelFactory;

    protected $request;

    protected $orderFactory;

    const DEFAULT_STORE_ID = 0;

    /**
     * @param ReasonsCollection $reasonsCollection
     * @param Eav $eav
     * @param DetailsFactory $detailsFactory
     * @param Data $rmaHelper
     * @param Item $item
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param ProductCollection $collectionFactory
     * @param OrdersRepository $ordersRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param LoggerInterface $logger
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param TokenFactory $tokenModelFactory
     * @param Request $request
     * @param OrderFactory $orderFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ReasonsCollection $reasonsCollection,
        Eav $eav,
        DetailsFactory $detailsFactory,
        Data $rmaHelper,
        OrderItemRepositoryInterface $orderItemRepository,
        ProductCollection $collectionFactory,
        CustomerRepositoryInterface $customerRepository,
        LoggerInterface $logger,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        TokenFactory $tokenModelFactory,
        Request $request,
        OrderFactory $orderFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->reasonsCollection = $reasonsCollection;
        $this->eav = $eav;
        $this->detailsFactory = $detailsFactory;
        $this->rmaHelper = $rmaHelper;
        $this->orderItemRepository = $orderItemRepository;
        $this->collectionFactory = $collectionFactory;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->request = $request;
        $this->orderFactory = $orderFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Arb\API\Api\Magento\Rma\Data\RmaInterface $rmaDataObject
     *
     * @return array|string
     *
     * @throws LocalizedException
     */
    public function create(\Arb\API\Api\Magento\Rma\Data\RmaInterface $rmaDataObject)
    {

        $customerId = $rmaDataObject->getCustomerId();
        if ($customerId) {
            //check for logged in customer
            $authorizationHeaderValue = $this->request->getHeader('Authorization');
            if (!$authorizationHeaderValue) {
                throw new LocalizedException(__('Token Invalid.'));
            }
            
            $validCustomerId = $this->isValidUser($authorizationHeaderValue, $customerId);

            if (empty($validCustomerId)) {
                throw new LocalizedException(__("Token Invalid.User is not Valid"));
            }
        }

        $items = $rmaDataObject->getItems();
        $itemIds = [];
        $productIds = [];
        $totalQty = [];
        $reasonIds = [];
        $conditionIds = [];
        $allPrices = [];
        $orderItemIds = [];
        $sellerId = null;
        $rmaDataObject->setStoreId($this->storeManager->getStore()->getId());

        try
        {
            $order = $this->orderFactory->create()->load($rmaDataObject->getOrderId());
        }
        catch(Exception $e)
        {
            throw new LocalizedException(__('Order does not belongs to customer'));
        }

        if ($customerId)
        {
            if ($order->getCustomerId() != $validCustomerId)
            {
                throw new LocalizedException(__('Order does not belongs to customer'));
            }
        }else
        {
            $isGuest = $order->getCustomerIsGuest();
            if (!$isGuest)
            {
                throw new LocalizedException(__('Order does not belongs to customer'));
            }
        }
        
        $orderItems = $order->getAllItems();
        foreach ($orderItems as $orderItem) {
            $orderItemIds[] = $orderItem->getId();
        }

        foreach ($items as $item) {
            $itemId = $item->getOrderItemId();
            if (!in_array($itemId, $orderItemIds)) {
                throw new LocalizedException(__('Items are not part of this Order.'));
            }

            if (!$this->rmaHelper->isRmaAllowed($itemId, $rmaDataObject->getOrderId(), $item->getQtyRequested())) {
                throw new LocalizedException(__('Rma is not allowed.'));
            }

            /** @var OrderItem $orderItem */
            $orderItem = $this->orderItemRepository->get($itemId);

            if (!$sellerId) {
                $merchantItem = $this->collectionFactory
                ->create()
                ->addFieldToFilter('mageproduct_id', $orderItem->getProductId());

                $sellerId = $merchantItem->getFirstItem()->getSellerId();
            }

            $itemIds[] = $itemId;
            $totalQty[$itemId] = $item->getQtyRequested();
            $reasonIds[$itemId] = $item->getReason();
            $conditionIds[$itemId] = $item->getCondition();
            $allPrices[$itemId] = $orderItem->getPrice();
            $productIds[$itemId] = $orderItem->getProductId();
        }

        $customer = $rmaDataObject->getCustomer();

        $merchantRmaData = [
            'order_id' => $rmaDataObject->getOrderId(),
            'resolution_type' => 1, // hardcoded 'refund'
            'item_ids' => $itemIds,
            'total_qty' => $totalQty,
            'reason_ids' => $reasonIds,
            'condition_ids' => $conditionIds,
            'customer_name' => $customer['first_name'] . ' ' . $customer['last_name'],
            'order_status' => 1,
            'telephone' => $customer['phone'],
            'status' => Data::RMA_STATUS_PENDING,
            'seller_status' => Data::SELLER_STATUS_PENDING,
            'updated_date' => date('Y-m-d H:i:s'),
            'created_date' => date('Y-m-d H:i:s'),
            'additional_info' => '',
            'order_ref' => '#' . $rmaDataObject->getOrderIncrementId()
        ];

        $customerId = $rmaDataObject->getCustomerId();
        if ($customerId) {
            try {
                $customer = $this->customerRepository->getById($customerId);
                $merchantRmaData['customer_email'] = $customer->getEmail();
            } catch (NoSuchEntityException $e) {
                $this->logger->error($e->getMessage());
                throw new LocalizedException(__('No Customer with this Id.'));
            } catch (LocalizedException $e) {
                $this->logger->error($e->getMessage());
                throw new LocalizedException(__('No Customer with this Id.'));
            }

            $merchantRmaData['customer_id'] = $customerId;
        } else {
            $merchantRmaData['customer_email'] = '';
            $merchantRmaData['customer_id'] = 0;
        }

        if ($sellerId) {
            $merchantRmaData['seller_id'] = $sellerId;
            $merchantRmaData['seller'] = 1;
        } else {
            $merchantRmaData['seller'] = 0;
        }

        $rma = $this->detailsFactory->create();
        $rma->setData($merchantRmaData)->save();

        if (!$this->setItemsData($productIds, $reasonIds, $totalQty, $allPrices, $conditionIds, (int)$rma->getId())) {
            $rma->delete();
            throw new LocalizedException(__('Invalid request'));
        }

        $rmaInfo = $merchantRmaData;
        $rmaInfo['rma_id'] = $rma->getId();
        $details = [
            'type' => 0,
            'name' => $merchantRmaData['customer_name'],
            'rma' => $rmaInfo
        ];

        $this->rmaHelper->sendNewRmaEmail($details);

        $returnArray[] = [
            'increment_id' => sprintf('%08d', $rma->getId()),
            'entity_id' => (int)$rma->getId(),
            'order_id' => $rmaDataObject->getOrderId(),
            'order_increment_id' => $rmaDataObject->getOrderIncrementId(),
            'store_id' => $rmaDataObject->getStoreId(),
            'status' => __('Pending'),
            'created_at' => date('Y-m-d H:i:s')
        ];

        return $returnArray;
    }

    /**
     * Set Rma Items
     *
     * @param array $productIds
     * @param array $allReasons
     * @param array $allQtys
     * @param array $allPrices
     * @param array $itemConditions
     * @param int $rmaId
     *
     * @return bool
     */
    public function setItemsData(
        array $productIds,
        array $allReasons,
        array $allQtys,
        array $allPrices,
        array $itemConditions,
        int $rmaId
    ) {
        try {
            foreach ($productIds as $itemId => $productId) {
                $data = [];
                $data['rma_id'] = $rmaId;
                $data['item_id'] = $itemId;
                $data['product_id'] = $productId;
                $data['qty'] = $allQtys[$itemId];
                $data['price'] = $allPrices[$itemId];
                $data['reason_id'] = $allReasons[$itemId];
                $data['item_condition'] = $itemConditions[$itemId];
                $this->rmaHelper->saveItemData($data);
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getReasons()
    {
        $reasons = [];
        $storeId = (int)$this->storeManager->getStore()->getId();
       
        $collection = $this->reasonsCollection
        ->create()
        ->addFieldToFilter('status', 1)
        ->addFieldToFilter('store_id', array(
            array('finset'=> self::DEFAULT_STORE_ID),
            array('finset'=> $storeId),
        ));
        
        foreach ($collection as $reason) {
            $reasons[] = [
                'id' => $reason->getId(),
                'label' => __($reason->getReason())
            ];
        }
        return $reasons;
    }

    /**
     * @return array
     */
    public function getItemConditions()
    {
        $conditions = [];
        $eavConditions = $this->eav->getAttributeOptionValues('condition');

        foreach ($eavConditions as $key => $value) {
            $conditions[] = [
                'id' => $key,
                'label' => $value
            ];
        }

        return $conditions;
    }


    /**
     * Validate token for the user.
     *
     * @param string $authorizationHeaderValue
     * @param int $customerId
     * @return boolean
     */
    private function isValidUser($authorizationHeaderValue, $customerId)
    {
        $headerPieces = explode(" ", $authorizationHeaderValue);
        if (count($headerPieces) !== 2) {
            throw new LocalizedException(__('Token Invalid'));
        }
        $tokenType = strtolower($headerPieces[0]);
        if ($tokenType !== 'bearer') {//check if token is bearer
            throw new LocalizedException(__('Token Invalid'));
        }
        $customerToken = $headerPieces[1];
        //Validate Token
        $customerOuthModel = $this->tokenModelFactory->create();
        $customerOuthModel->load($customerToken, "token");
        $outhCustomerId = "";
        $outhCustomerId = $customerOuthModel->getCustomerId();
        if (empty($outhCustomerId)) {//check if passed token is customer token
            throw new LocalizedException(__('Token Invalid'));
        }
        if (!empty($customerOuthModel->getRevoked())) { //check if token is expired
            throw new LocalizedException(__('Token Invalid'));
        }
        // Validating customer id associated with Token.
        if ($outhCustomerId != $customerId) { // check if body data has same customer id as token
            throw new LocalizedException(__('Token Invalid'));
        } else {
            return $outhCustomerId;
        }
    }
}