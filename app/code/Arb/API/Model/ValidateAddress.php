<?php

/**
 * Validates customer input address city and merchant ids
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Model;

/**
 * API model class to validate customer city address
 */
class ValidateAddress implements \Arb\API\Api\ValidateAddressInterface
{
   
    /**
     * @inheritdoc
     */
    public function validateAddress($merchantIds, $city)
    {
        return !(empty($city));
    }
}
