<?php

namespace Arb\API\Test\Unit\Helper;

use Arb\API\Helper\Product;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Framework\Api\ExtensionAttributesInterface;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class ProductTest extends TestCase
{
    /**
     * Object to test
     *
     * @var Product
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->testObject = $objectManager->getObject(Product::class, []);
    }

    /**
     * @throws ReflectionException
     */
    public function testGetCorrectPrices()
    {
        $productItemFirst = $this->createMock(ProductModel::class);
        $productItemFirst->expects($this->once())->method('getTypeId')->willReturn('configurable');
        $productItemSecond = $this->createMock(ProductModel::class);
        $productItemSecond->expects($this->once())->method('getTypeId')->willReturn('simple');

        $extensionAttrMock = $this->getMockBuilder(ExtensionAttributesInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getConfigurableProductPrice'])
            ->getMock();

        $productItemFirst->expects($this->once())->method('getExtensionAttributes')->willReturn($extensionAttrMock);
        $extensionAttrMock->expects($this->once())->method('getConfigurableProductPrice')->willReturn(5);
        $productItemSecond->expects($this->once())->method('getPrice')->willReturn(4);

        $items = [$productItemFirst, $productItemSecond];
        $this->assertInternalType('array', $this->testObject->getCorrectPrices($items));
    }

    /**
     * @dataProvider sortMergedProductCollectionItemsProvider
     *
     * @param string $field
     * @param string $direction
     * @param int $expected
     *
     * @throws ReflectionException
     */
    public function testSortMergedProductCollectionItems(string $field, string $direction, int $expected)
    {
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);

        $sortOrderMock = $this->createMock(SortOrder::class);
        $searchCriteriaMock->expects($this->once())->method('getSortOrders')->willReturn([$sortOrderMock]);
        $sortOrderMock->expects($this->once())->method('getField')->willReturn($field);
        $sortOrderMock->expects($this->once())->method('getDirection')->willReturn($direction);
        $filterGroupMock = $this->createMock(FilterGroup::class);
        $searchCriteriaMock
            ->expects($this->exactly($expected))
            ->method('getFilterGroups')
            ->willReturn([$filterGroupMock]);
        $filterMock = $this->createMock(Filter::class);
        $filterGroupMock->expects($this->exactly($expected))->method('getFilters')->willReturn([$filterMock]);
        $filterMock->expects($this->exactly($expected))->method('getField')->willReturn('category_id');
        $filterMock->expects($this->exactly($expected))->method('getConditionType')->willReturn('eq');
        $filterMock->expects($this->exactly(2*$expected))->method('getValue')->willReturn(5);

        $items = [];
        $this->assertInternalType(
            'array',
            $this->testObject->sortMergedProductCollectionItems($items, $searchCriteriaMock)
        );
    }

    /**
     * @return array
     */
    public function sortMergedProductCollectionItemsProvider()
    {
        return [
            ['price', 'ASC', 0],
            ['position', 'DESC', 1],
            ['aaa', 'whatever', 0]
        ];
    }
}
