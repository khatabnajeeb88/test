<?php

/**
 * This file consist of PHPUnit test case for class Rma
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Test\Unit\Model;

use Arb\API\Api\Magento\Rma\Data\RmaInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Rma\Api\Data\ItemInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderItemSearchResultInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Psr\Log\LoggerInterface;
use Webkul\Marketplace\Model\Product as WebkulProduct;
use Webkul\MpRmaSystem\Helper\Data;
use Webkul\MpRmaSystem\Model\ResourceModel\Reasons\CollectionFactory as ReasonsCollection;
use Arb\API\Model\Rma;
use Webkul\MpRmaSystem\Model\ResourceModel\Reasons\Collection;
use Webkul\MpRmaSystem\Model\Reasons;
use ReflectionException;
use Magento\Rma\Helper\Eav;
use Webkul\Marketplace\Model\ResourceModel\Product\Collection as ProductCollection;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Webkul\MpRmaSystem\Model\Details;
use Webkul\MpRmaSystem\Model\DetailsFactory;
use Magento\Sales\Model\OrderFactory;
/**
 * @covers \Arb\API\Model\Rma
 */
class RmaTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ReasonsCollection
     */
    private $reasonsCollectionMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Eav
     */
    private $eavMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|DetailsFactory
     */
    private $detailsFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|Data
     */
    private $rmaHelperMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|OrderItemRepositoryInterface
     */
    private $orderItemRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ProductCollectionFactory
     */
    private $collectionFactoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|CustomerRepositoryInterface
     */
    private $customerRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|LoggerInterface
     */
    private $loggerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaBuilder
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|StoreManagerInterface
     */
    private $storeManagerMock;

    /**
     * Object to test
     *
     * @var Rma
     */
    private $testObject;

     /**
     * Mock orderFactory
     *
     * @var \Magento\Sales\Model\OrderFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $orderFactory;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->reasonsCollectionMock=$this->getMockBuilder(ReasonsCollection::class)
            ->setMethods([ "create","addFieldToFilter"])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        //  = $this->createMock(ReasonsCollection::class);
        $this->eavMock = $this->createMock(Eav::class);
        $this->detailsFactoryMock = $this->createMock(DetailsFactory::class);
        $this->rmaHelperMock = $this->createMock(Data::class);
        $this->orderItemRepositoryMock = $this->createMock(OrderItemRepositoryInterface::class);
        $this->collectionFactoryMock = $this->createMock(ProductCollectionFactory::class);
        $this->customerRepositoryMock = $this->createMock(CustomerRepositoryInterface::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->orderFactory = $this->getMockBuilder(\Magento\Sales\Model\OrderFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create','getCollection','addAddressFields','addAttributeToFilter',
                'getFirstItem','setBaseTotalDue','setCustomerEmail','setGrandTotal',
                'setCreatedAt','setIncrementId','setBaseGrandTotal','setItems',
                "setTotalItemCount","load","getId","getBaseGrandTotal","getPayment",
                "getBaseTotalDue","getCustomerEmail","getGrandTotal","getCreatedAt",
                "getIncrementId","getTotalItemCount","getItems","getData",
                "getCustomerFirstname","getCustomerLastname","getAllItems",
                "getStore","getCode","getBillingAddress","getTelephone","getCustomerIsGuest"
                ])
            ->getMock();
        $this->itemFactory = $this->getMockBuilder(\Magento\Sales\Model\Order\ItemFactory::class)
            ->disableOriginalConstructor()
            ->setMethods([ 'create','getVouchers',"getData","getId","getProductType"])
            ->getMock();
        $this->testObject = $objectManager->getObject(Rma::class, [
            'reasonsCollection' => $this->reasonsCollectionMock,
            'eav' => $this->eavMock,
            'detailsFactory' => $this->detailsFactoryMock,
            'rmaHelper' => $this->rmaHelperMock,
            'orderItemRepository' => $this->orderItemRepositoryMock,
            'collectionFactory' => $this->collectionFactoryMock,
            'customerRepository' => $this->customerRepositoryMock,
            'logger' => $this->loggerMock,
            'searchCriteriaBuilder' => $this->searchCriteriaBuilderMock,
            'storeManager' => $this->storeManagerMock,
            'orderFactory' => $this->orderFactory
        ]);

        $this->orderFactory->method('create')->will(
            $this->returnSelf()
        );
        $this->orderFactory->method('load')->will(
            $this->returnSelf()
        );


    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetReasons()
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|Reasons $reasonMock */
        $reasonMock = $this->getMockBuilder(Reasons::class)
            ->disableOriginalConstructor()
            ->setMethods(['getReason', 'getId'])
            ->getMock();
        $reasonMock
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);

        $reasonMock
            ->expects($this->any())
            ->method('getReason')
            ->willReturn('reason');
        /** @var PHPUnit_Framework_MockObject_MockObject|Collection $reasonsCollectionMock */
        // $reasonsCollectionMock = $this->createMock(ReasonsCollection::class);

        $reasonsCollectionMock = $this->getMockBuilder(ReasonsCollection::class)
        ->setMethods(["addFieldToFilter"])
        ->disableOriginalConstructor()
        ->getMockForAbstractClass();
        $reasonsCollectionMock
        ->expects($this->any(0))
        ->method('addFieldToFilter')
        ->willReturnSelf();
        $reasonsCollectionMock
        ->expects($this->any(1))
        ->method('addFieldToFilter')
        ->willReturn($reasonMock);
        $this->reasonsCollectionMock
        ->expects($this->once())
        ->method('create')
        ->willReturn($reasonsCollectionMock);
    
        $storeMock = $this->createMock(StoreInterface::class);
        $storeMock->expects($this->any())->method('getId')->willReturn('1');
        $this->storeManagerMock->method('getStore')->willReturn($storeMock);
        $getReasons = $this->testObject->getReasons();
        $this->assertInternalType('array', $getReasons);
    }

    /**
     * @return void
     */
    public function testGetItemConditions()
    {
        $conditionsArray = ['Unopened', 'Opened', 'Damaged', 'Cracked', 'Boiled', 'Half-eaten', 'Spoiled'];

        $this->eavMock
            ->expects($this->once())
            ->method('getAttributeOptionValues')
            ->willReturn($conditionsArray);

        $getItemConditions = $this->testObject->getItemConditions();
        $this->assertInternalType('array', $getItemConditions);
    }

    /**
     * @dataProvider setItemsDataProvider
     *
     * @param $productIds
     * @param $allReasons
     * @param $allQtys
     * @param $allPrices
     * @param $itemConditions
     * @param $rmaId
     * @param $expectedAssertion
     *
     * @return void
     */
    public function testSetItemsData(
        $productIds,
        $allReasons,
        $allQtys,
        $allPrices,
        $itemConditions,
        $rmaId,
        $expectedAssertion
    ) {
        $setItemsData = $this->testObject->setItemsData(
            $productIds,
            $allReasons,
            $allQtys,
            $allPrices,
            $itemConditions,
            $rmaId
        );

        $this->assertSame($setItemsData, $expectedAssertion);
    }

    /**
     * @return array
     */
    public function setItemsDataProvider()
    {
        return [
            [           // scenario 0: correct data, return true
                [0 => 1],
                [0],
                [0],
                [0],
                [0],
                1,
                true
            ],
            [           // scenario 1: incorrect data, return false
                [1 => 1],
                [0],
                [0],
                [0],
                [0],
                1,
                false
            ]
        ];
    }

    /**
     * @throws ReflectionException
     * @throws LocalizedException
     */
    public function testCreateNoItems()
    {
        $rmaDataObjectMock = $this->createMock(RmaInterface::class);
        $rmaDataObjectMock->expects($this->any())->method('getItems')->willReturn([]);
        $storeMock = $this->createMock(StoreInterface::class);

        $this->storeManagerMock->method('getStore')->willReturn($storeMock);
        $storeMock->expects($this->any())->method('getId')->willReturn('1');

        $searchCriteriaMock = $this->createMock(SearchCriteria::class);
        $this->searchCriteriaBuilderMock->expects($this->any())->method('addFilter')->willReturnSelf();
        $this->searchCriteriaBuilderMock->expects($this->any())->method('create')->willReturn($searchCriteriaMock);

        $orderSearchResultMock = $this->createMock(OrderItemSearchResultInterface::class);
        $this->orderItemRepositoryMock->expects($this->any())->method('getList')->willReturn($orderSearchResultMock);

        $rmaMock = $this->createMock(Details::class);
        $this->detailsFactoryMock->expects($this->any())->method('create')->willReturn($rmaMock);
        $rmaMock->expects($this->any())->method('setData')->willReturnSelf();

        $rmaDataObjectMock->expects($this->any())->method('setStoreId')->willReturn($rmaMock);
        $this->orderFactory->expects($this->any())->method('getCustomerIsGuest')->willReturnSelf(0);

        $items = [$this->itemFactory];
        
        $this->orderFactory->method('getAllItems')->willReturn(
            $items
        );
        $this->assertInternalType('array', $this->testObject->create($rmaDataObjectMock));
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @throws ReflectionException
     * @throws LocalizedException
     */
    public function testCreateItemsError()
    {
        $rmaDataObjectMock = $this->createMock(RmaInterface::class);
        $itemMock = $this->createMock(ItemInterface::class);

        $itemMock->method('getOrderItemId')->willReturn(1);

        $rmaDataObjectMock->expects($this->any())->method('getItems')->willReturn([$itemMock]);
        $storeMock = $this->createMock(StoreInterface::class);

        $this->storeManagerMock->method('getStore')->willReturn($storeMock);
        $storeMock->expects($this->once())->method('getId')->willReturn('1');
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);
        $this->searchCriteriaBuilderMock->expects($this->any())->method('addFilter')->willReturnSelf();
        $this->searchCriteriaBuilderMock->expects($this->any())->method('create')->willReturn($searchCriteriaMock);

        $this->orderItemRepositoryMock
            ->expects($this->any())
            ->method('getList')
            ->with($searchCriteriaMock)
            ->willReturn([]);

        $this->testObject->create($rmaDataObjectMock);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @throws ReflectionException
     * @throws LocalizedException
     */
    public function testCreateRmaError()
    {
        $rmaDataObjectMock = $this->createMock(RmaInterface::class);
        $itemMock = $this->createMock(ItemInterface::class);

        $orderItemMock = $this->getMockBuilder(OrderItemInterface::class)
            ->setMethods(['getId', "addFieldToFilter", "getAllSoldQty"])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $itemMock->method('getOrderItemId')->willReturn(2);

        $rmaDataObjectMock->expects($this->any())->method('getItems')->willReturn([$itemMock]);
        $storeMock = $this->createMock(StoreInterface::class);

        $this->storeManagerMock->method('getStore')->willReturn($storeMock);
        $storeMock->expects($this->once())->method('getId')->willReturn(2);
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);
        $this->searchCriteriaBuilderMock->expects($this->any())->method('addFilter')->willReturnSelf();
        $this->searchCriteriaBuilderMock->expects($this->any())->method('create')->willReturn($searchCriteriaMock);

        $this->orderItemRepositoryMock
            ->expects($this->any())
            ->method('getList')
            ->with($searchCriteriaMock)
            ->willReturn([$orderItemMock]);

        $orderItemMock->expects($this->any())->method('getId')->willReturn(2);

        $this->testObject->create($rmaDataObjectMock);
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @throws ReflectionException
     * @throws LocalizedException
     */
    public function testCreate()
    {
        $rmaDataObjectMock = $this->createMock(RmaInterface::class);
        $itemMock = $this->createMock(ItemInterface::class);

        $orderItemMock = $this->getMockBuilder(OrderItemInterface::class)
            ->setMethods(['getId', "addFieldToFilter", "getAllSoldQty"])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $itemMock->method('getOrderItemId')->willReturn(2);

        $rmaDataObjectMock->expects($this->once())->method('getItems')->willReturn([$itemMock]);
        $storeMock = $this->createMock(StoreInterface::class);

        $this->storeManagerMock->method('getStore')->willReturn($storeMock);
        $storeMock->expects($this->any())->method('getId')->willReturn(1);
        $searchCriteriaMock = $this->createMock(SearchCriteria::class);
        $this->searchCriteriaBuilderMock->expects($this->any())->method('addFilter')->willReturnSelf();
        $this->searchCriteriaBuilderMock->expects($this->any())->method('create')->willReturn($searchCriteriaMock);

        // $this->orderItemRepositoryMock
        //     ->expects($this->once())
        //     ->method('getList')
        //     ->with($searchCriteriaMock)
        //     ->willReturn([$orderItemMock]);

        // $orderItemMock->expects($this->once())->method('getId')->willReturn(2);
        $this->rmaHelperMock->method('isRmaAllowed')->willReturn(false);

        $collectionMock = $this->createMock(ProductCollection::class);
        $this->collectionFactoryMock->expects($this->any())->method('create')->willReturn($collectionMock);
        $collectionMock->expects($this->any())->method('addFieldToFilter')->willReturnSelf();


        $orderMock = $this->createMock(\Magento\Sales\Api\Data\OrderItemInterface::class);
        $this->orderItemRepositoryMock->expects($this->any())->method('get')->willReturn($orderMock);

        $mpProductMock = $this->createMock(WebkulProduct::class);
        $collectionMock->expects($this->any())->method('getFirstItem')->willReturn($mpProductMock);

        $rmaMock = $this->createMock(Details::class);
        $this->detailsFactoryMock->expects($this->any())->method('create')->willReturn($rmaMock);
        $rmaMock->expects($this->any())->method('setData')->willReturnSelf();
        $this->orderFactory->expects($this->any())->method('getCustomerIsGuest')->willReturnSelf(0);

        $items = [$this->itemFactory];
        
        $this->orderFactory->method('getAllItems')->willReturn(
            []
        );
        $this->assertInternalType('array', $this->testObject->create($rmaDataObjectMock));
    }
}
