<?php
/**
 * This file consist of PHPUnit test case for class MerchantData
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */
namespace Arb\API\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Arb\API\Helper\Product;
use Magento\Framework\DB\Select;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface as attributeProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface as storeManagerInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\Collection as sellerCollection;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as productFactory;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface as sourceDataBySku;
use \Magento\Catalog\Model\Api\SearchCriteria\ProductCollectionProcessor as SearchProcessor;
use \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory;
use \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use \Arb\API\Model\MerchantData;

/**
 * @covers \Arb\API\Model\MerchantData
 */
class MerchantDataTest extends TestCase
{
    /**
     * @var Product|MockObject
     */
    private $product;

    /**
     * Mock storeManager
     *
     * @var \Magento\Store\Model\StoreManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManager;

    /**
     * Mock sellerCollectionFactory
     *
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\Collection|PHPUnit_Framework_MockObject_MockObject
     */
    private $sellerCollectionFactory;

    /**
     * Mock collectionProductFactoryInstance
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection|PHPUnit_Framework_MockObject_MockObject
     */
    private $collectionProductFactoryInstance;

    /**
     * Mock collectionProductFactory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $collectionProductFactory;

    /**
     * Mock sourceDataBySku
     *
     * @var \Magento\InventoryCatalogAdminUi\Model\GetSourceItemsDataBySku|PHPUnit_Framework_MockObject_MockObject
     */
    private $sourceDataBySku;

    /**
     * Mock extensionAttributesJoinProcessor
     *
     * @var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $extensionAttributesJoinProcessor;

    /**
     * Webkul seller block class
     *
     * @var [type]
     */
    protected $sellerBlock;

    /**
     * attribute class object
     *
     * @var [type]
     */
    protected $eavAttribute;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\API\Model\MerchantData
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->storeManager = $this->getMockBuilder(\Magento\Store\Model\StoreManagerInterface::class)
        ->disableOriginalConstructor()
        ->setMethods(["getStore","getId"])
        ->getMockForAbstractClass();
        $this->searchCriteriaMock = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaInterface::class)
        ->disableOriginalConstructor()
        ->setMethods(["getSortOrders","getField","getDirection","setPageSize","getCurrentPage","getPageSize"])
        ->getMockForAbstractClass();
        $this->searchCriteriaMock->method("getSortOrders")->willReturn([$this->searchCriteriaMock]);
        $this->searchCriteriaMock->method("setPageSize")->willReturn(10);
        $this->searchCriteriaMock->method("getPageSize")->willReturn(10);
        $this->searchCriteriaMock->method("getCurrentPage")->willReturn(true);

        $this->collectionProductFactory = $this->getMockBuilder(productFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create',"load","addAttributeToFilter"])
            ->getMock();
        $this->sourceDataBySku = $this->createMock(
            \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface::class
        );
        $this->sourceDataBySku->method("execute")->willReturn("10");
        $this->extensionAttributesJoinProcessor = $this->createMock(
            \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface::class
        );
        
        $this->sellerBlock = $this->createMock(
            \Webkul\Marketplace\Block\Sellerlist::class
        );

        $this->eavAttribute = $this->createMock(
            \Magento\Eav\Model\Entity\Attribute::class
        );

        $this->productSearchResultsFactory = $this->getMockBuilder(ProductSearchResultsInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create",
                        "setSearchCriteria",
                        "setItems",
                        "setTotalCount",
                        "getItems"])
            ->getMockForAbstractClass();
        $this->collectionProcessorMock = $this->getMockBuilder(CollectionProcessorInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["process"])
            ->getMockForAbstractClass();
        $this->productHelperMock = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()
            ->setMethods(["sortMergedProductCollectionItems"])
            ->getMock();
        
        $this->product = $this->createPartialMock(
            Product::class,
            [
                'getId',
                'getSku',
                'setWebsiteIds',
                'getWebsiteIds',
                'load',
                'setData',
                'getStoreId',
                'getMediaGalleryEntries',
                'getExtensionAttributes',
                'getCategoryIds',
                'getAttributes',
                'getThumbnail',
                'getResource',
                'getAttributeRawValue',
                'setCustomAttribute'
            ]
        );
        $this->abstractDb = $this->objectManager->getCollectionMock(
            \Magento\Catalog\Model\ResourceModel\Product\Collection::class,
            [$this->product]
        );
        $this->selectMock = $this->createMock(Select::class);
       
        $this->seller = $this->createMock(\Webkul\Marketplace\Model\Seller::class);
        $this->sellerCollectionFactory = $this->objectManager->getCollectionMock(
            SellerCollection::class,
            [$this->seller]
        );
        $this->extensionAttributesJoinProcessor->method("process")->with($this->abstractDb)->willReturn($this->abstractDb);
        $this->testObject = $this->objectManager->getObject(
            MerchantData::class,
            [
                'storeManager' => $this->storeManager,
                'sellerCollection' => $this->sellerCollectionFactory,
                'prodCollFactory' => $this->collectionProductFactory,
                'sourceDataBySku' => $this->sourceDataBySku,
                'extnAttriJoinProc' => $this->extensionAttributesJoinProcessor,
                "searchResultsFactory"=>$this->productSearchResultsFactory,
                "productHelper"=>$this->productHelperMock,
                "collectionProcessor"=>$this->collectionProcessorMock
            ]
        );
    }
    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testGetSellerProductsExecption()
    {
        $this->storeManager->method('getStore')->willReturnSelf();
        $this->storeManager->method('getId')->willReturn(1);
        $mockError = new \Magento\Framework\Phrase('Unable to display products');
        $mockClass = new \Magento\Framework\Exception\LocalizedException($mockError);
        $this->sellerCollectionFactory->method('clear')->willThrowException($mockClass);
        $this->sellerCollectionFactory->method('getSelect')->willReturn($this->selectMock);
  
        $this->testObject->getSellerProducts(2, $this->searchCriteriaMock);
    }
    public function testGetProductData()
    {
        $this->abstractDb->method('getSelect')->willReturn($this->selectMock);
        $this->selectMock->method('join')->willReturnSelf();
        $this->product->method("getSku")->willReturn("teste_sku");
        $this->product->method("getThumbnail")->willReturn("teste_sku.jpg");
        $this->abstractDb->method('load')->willReturn([$this->product]);
        $this->collectionProductFactory->method('create')->willReturnSelf();
        $this->collectionProductFactory->method('addAttributeToFilter')->willReturn($this->abstractDb);
        $this->productSearchResultsFactory->method("create")->willReturnSelf();
        $this->productSearchResultsFactory->method("setSearchCriteria")->willReturnSelf();
        $this->productSearchResultsFactory->method("setItems")->willReturnSelf();
        $this->productSearchResultsFactory->method("setTotalCount")->willReturn(10);
        $this->productSearchResultsFactory->method("getItems")->willReturn([$this->product]);
//        $this->testObject->getProductData($this->searchCriteriaMock);
    }

    public function testGetSellerData()
    {
        $this->sellerCollectionFactory->expects($this->once())->method("getTable")->willReturn("marketplace_product");
        $this->sellerCollectionFactory->expects($this->once())->method("getSize")->willReturn(1);
        $this->sellerCollectionFactory->expects($this->exactly(2))->method('getSelect')->willReturn($this->selectMock);
        $this->sellerCollectionFactory->expects($this->once())->method('addFieldToSelect')->willReturnSelf();
        $this->testObject->getSellerData(1);
    }
}
