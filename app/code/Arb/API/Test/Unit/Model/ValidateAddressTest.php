<?php
namespace Arb\API\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @covers \Arb\API\Model\ValidateAddress
 */
class ValidateAddressTest extends TestCase
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\API\Model\ValidateAddress
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->testObject = $this->objectManager->getObject(
            \Arb\API\Model\ValidateAddress::class,
            [

            ]
        );
    }
    
    public function testValidateAddress()
    {
        $merchantIds = ['1','2','3'];
        $city = 'Riyadh';
        $this->assertNotEmpty($this->testObject->validateAddress($merchantIds, $city));
        $this->assertTrue($this->testObject->validateAddress($merchantIds, $city));
    }

    public function testInvalidAddress()
    {
        $merchantIds = ['1','2','3'];
        $city = '';
        $this->assertEmpty($this->testObject->validateAddress($merchantIds, $city));
        $this->assertFalse($this->testObject->validateAddress($merchantIds, $city));
    }
}
