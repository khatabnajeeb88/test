<?php
/**
 * Unit test case for class \Arb\API\Model\Products
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

declare (strict_types = 1);

namespace Arb\API\Test\Unit\Model;

use Arb\API\Api\ProductsInterface;
use Arb\API\Model\Products as ProductRepository;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Api\Data\ProductExtension;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product\Gallery\Processor;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Api\Data\StockItemCollectionInterface;
use Magento\CatalogInventory\Helper\Stock;
use Magento\CatalogInventory\Model\ResourceModel\Stock\Item\StockItemCriteria;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Api\AttributeInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\InventoryCatalogAdminUi\Model\GetSourceItemsDataBySku;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use PHPUnit\Framework\TestCase;
use \Arb\Vouchers\Model\ResourceModel\Vouchers\CollectionFactory as VouchersCollectionFactory;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use ReflectionException;
use stdClass;
use Webkul\Marketplace\Model\Product as WebkulProduct;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as MpProductCollection;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Webkul\Marketplace\Model\ResourceModel\Seller\Collection as SellerCollection;
use Webkul\Marketplace\Model\Rewrite\Catalog\ResourceModel\Product\Collection as WebkulCollection;
use Webkul\Marketplace\Model\Seller;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;

/**
 * Unit test case Class for Products
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ProductsTest extends TestCase
{
    /**
     * @var Product|MockObject
     */
    private $product;

    /**
     * @var ProductRepository
     */
    private $model;

    /**
     * @var Product|MockObject
     */
    private $resourceModel;

    /**
     * @var ProductFactory|MockObject
     */
    private $productFactory;

    /**
     * @var CollectionFactory|MockObject
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder|MockObject
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductSearchResultsInterfaceFactory|MockObject
     */
    private $searchResultsFactory;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var StoreManagerInterface|MockObject
     */
    private $storeManager;

    /**
     * @var CollectionProcessorInterface|MockObject
     */
    private $collectionProcessor;

    /**
     * @var Json|MockObject
     */
    private $serializerMock;

    /**
     * @var SellerCollection|MockObject
     */
    private $brandCollection;

    /**
     * @var GetSourceItemsDataBySku|MockObject
     */
    private $stockUpdate;

    /**
     * Product repository cache limit.
     *
     * @var int
     */
    private $cacheLimit = 2;

    /**
     * @var CategoryRepositoryInterface|MockObject
     */
    private $categoryRepository;

    /**
     * @var MpProductCollection|MockObject
     */
    private $webkulCollectionFactory;

    /**
     * @var MpHelper|MockObject
     */
    private $mpHelper;

    /**
     * @var StoreInterface|MockObject
     */
    private $storeMock;

    /**
     * @var ProductRepository|MockObject
     */
    private $productRepositoryMock;

    /**
     * @var StockItemRepository|MockObject
     */
    private $stockItemRepositoryMock;

    /**
     * @var StockItemCriteriaInterfaceFactory|MockObject
     */
    private $stockItemCriteriaInterfaceFactoryMock;

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function setUp()
    {
        $this->productFactory = $this->createPartialMock(
            ProductFactory::class,
            ['create', 'setData']
        );

        $this->vouchersCollectionFactory = $this->createPartialMock(
            VouchersCollectionFactory::class,
            ['create', 'addFieldToFilter','getSize']
        );
        $this->vouchersCollectionFactory->method("create")->willReturnSelf();
        $this->vouchersCollectionFactory->method("addFieldToFilter")->willReturnSelf();
        $this->vouchersCollectionFactory->method("getSize")->willReturn("1");
        $this->product = $this->createPartialMock(
            Product::class,
            [
                'getId',
                'getSku',
                'setWebsiteIds',
                'getWebsiteIds',
                'load',
                'setData',
                'getStoreId',
                'getMediaGalleryEntries',
                'getExtensionAttributes',
                'getCategoryIds',
                'getAttributes',
                'getResource',
                'getAttributeRawValue',
                'setCustomAttribute',
            ]
        );

        $this->collectionFactory = $this->createPartialMock(CollectionFactory::class, ['create']);
        $this->searchCriteriaBuilder = $this->createMock(SearchCriteriaBuilder::class);
        $this->searchResultsFactory = $this->createPartialMock(
            ProductSearchResultsInterfaceFactory::class,
            ['create']
        );
        $this->resourceModel = $this->createMock(\Magento\Catalog\Model\ResourceModel\Product::class);
        $this->objectManager = new ObjectManager($this);

        $this->storeManager = $this->getMockBuilder(StoreManagerInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMockForAbstractClass();
        $this->product
            ->method('getCategoryIds')
            ->willReturn([1, 2, 3, 4]);

        $this->storeMock = $this->getMockBuilder(StoreInterface::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMockForAbstractClass();
        $this->storeMock->expects($this->any())->method('getWebsiteId')->willReturn('1');
        $this->storeMock->expects($this->any())->method('getCode')->willReturn(Store::ADMIN_CODE);
        $this->storeManager->expects($this->any())->method('getStore')->willReturn($this->storeMock);

        $this->collectionProcessor = $this->getMockBuilder(CollectionProcessorInterface::class)
            ->getMock();
        $this->extensionAttributesJoinProcessor = $this->getMockBuilder(JoinProcessorInterface::class)
            ->getMock();

        $this->serializerMock = $this->getMockBuilder(Json::class)->getMock();
        $this->serializerMock->expects($this->any())
            ->method('unserialize')
            ->will(
                $this->returnCallback(
                    function ($value) {
                        return json_decode($value, true);
                    }
                )
            );

        $this->brandCollection = $this->createMock(SellerCollection::class);

        $this->stockUpdate = $this->createMock(GetProductSalableQtyInterface::class);
        $this->extentionApiMock = $this->createPartialMock(
            ProductExtension::class,
            [
                                    "setProductQty",
                                    "setMerchantId",
                                    "setMerchantStoreName",
                                    "setMerchantBanner",
                                    "setMerchantLogo",
                                    "setMerchantStoreId",
                                    "setSalableQty",
                                    "setVoucherQty",
                                    "setLowStockDate",
                                    "setProductCategories"
            ]
        );
        $this->product->method('getExtensionAttributes')->willReturn($this->extentionApiMock);

        $this->categoryRepository = $this->createMock(CategoryRepositoryInterface::class);
        $this->webkulCollectionFactory = $this->createPartialMock(
            MpProductCollection::class,
            [
                'addFieldToFilter',
                'addStoreFilter',
                'addFieldToSelect',
                'getSelect',
                'join',
                'create',
                'getSize',
                'where',
                'getFirstItem',
            ]);
        $this->mpHelper = $this->createMock(MpHelper::class);
        $this->productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);
        $this->stockItemRepositoryMock = $this->createMock(StockItemRepository::class);
        $this->stockItemCriteriaInterfaceFactoryMock = $this->createMock(StockItemCriteriaInterfaceFactory::class);

        $this->model = $this->objectManager->getObject(
            ProductRepository::class,
            [
                'productFactory' => $this->productFactory,
                'resourceModel' => $this->resourceModel,
                'storeManager' => $this->storeManager,
                'collectionProductFactory' => $this->collectionFactory,
                'extensionAttributesJoinProcessor' => $this->extensionAttributesJoinProcessor,
                'sellerCollection' => $this->brandCollection,
                'stockItem' => $this->stockUpdate,
                'searchResultsFactory' => $this->searchResultsFactory,
                'serializer' => $this->serializerMock,
                'collectionProcessor' => $this->collectionProcessor,
                'cacheLimit' => $this->cacheLimit,
                'categoryRepository' => $this->categoryRepository,
                'mpProductCollectionFactory' => $this->webkulCollectionFactory,
                'mpHelper' => $this->mpHelper,
                'productRepository' => $this->productRepositoryMock,
                'searchCriteriaBuilder' => $this->searchCriteriaBuilder,
                'stockItemRepository' => $this->stockItemRepositoryMock,
                'stockItemCriteriaInterfaceFactory' => $this->stockItemCriteriaInterfaceFactoryMock,
                'voucherCollectionFactory' => $this->vouchersCollectionFactory
            ]
        );
    }

    /**
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     * @expectedExceptionMessage The product that was requested doesn't exist. Verify the product and try again.
     */
    public function testGetAbsentProduct()
    {
        $this->productFactory->expects($this->once())->method('create')
            ->will($this->returnValue($this->product));
        $this->resourceModel->expects($this->once())->method('getIdBySku')->with('test_sku')
            ->will($this->returnValue(null));
        $this->productFactory->expects($this->never())->method('setData');
        $this->model->get('test_sku');
    }

    public function testCreateCreatesProduct()
    {
        $sku = 'test_sku';
        $this->productFactory->expects($this->once())->method('create')
            ->will($this->returnValue($this->product));
        $this->resourceModel->expects($this->once())->method('getIdBySku')->with($sku)
            ->will($this->returnValue('test_id'));
        $this->product->expects($this->once())->method('load')->with('test_id');
        $this->product->expects($this->any())->method('getSku')->willReturn($sku);
        $this->assertEquals($this->product, $this->model->get($sku));
    }

    public function testGetProductInEditMode()
    {
        $sku = 'test_sku';
        $this->productFactory->expects($this->once())->method('create')
            ->will($this->returnValue($this->product));
        $this->resourceModel->expects($this->once())->method('getIdBySku')->with($sku)
            ->will($this->returnValue('test_id'));
        $eavMock = $this->createMock(AbstractSource::class);
        $this->product->expects($this->once())->method('setData')->with('_edit_mode', true);
        $this->product->expects($this->once())->method('load')->with('test_id');
        $this->product->expects($this->any())->method('getSku')->willReturn($sku);
        $attributeMock = $this->createMock(AbstractAttribute::class);
        $this->product->method('getAttributes')->willReturn([$attributeMock]);
        $this->product->method('getResource')->willReturn($this->resourceModel);
        $this->resourceModel->method('getAttributeRawValue')->willReturn('Saudi');
        $this->resourceModel->method('getAttribute')->willReturn($attributeMock);
        $attributeMock->method('getFrontendInput')->willReturn('select');
        $attributeMock->method('getAttributeCode')->willReturn('vendor_country');
        $attributeMock->method('getIsVisibleOnFront')->willReturn(true);
        $attributeMock->method('getSource')->willReturn($eavMock);
        $eavMock->method('getOptionText')->willReturn('Saudi');
        $attributeMock->method('usesSource')->willReturn(true);
        $this->product->method('setCustomAttribute')->willReturn($this->product);
        $this->assertEquals($this->product, $this->model->get($sku, true));
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testGetProductInEditModeException()
    {
        $sku = 'test_sku';
        $this->productFactory->expects($this->once())->method('create')
            ->will($this->returnValue($this->product));
        $this->resourceModel->expects($this->once())->method('getIdBySku')->with($sku)
            ->will($this->returnValue('test_id'));
        $this->product->expects($this->once())->method('setData')->with('_edit_mode', true);
        $this->product->expects($this->once())->method('load')->with('test_id');
        $attributeMock = $this->createMock(AbstractAttribute::class);
        $this->product->method('getAttributes')->willReturn([$attributeMock]);
        $mockError = new Phrase('Unable to display products');
        $mockClass = new LocalizedException($mockError);
        $this->product->method('getResource')->willThrowException($mockClass);
        $this->resourceModel->method('getAttributeRawValue')->willReturn('Saudi');
        $this->resourceModel->method('getAttribute')->willReturn('Saudi');
        $attributeMock->method('getFrontendInput')->willReturn('select');
        $attributeMock->method('getAttributeCode')->willReturn('vendor_country');
        $attributeMock->method('getIsVisibleOnFront')->willReturn(true);
        $attributeMock->method('getSource')->willReturn(true);
        $this->product->method('setCustomAttribute')->willReturn($this->product);
        $this->assertEquals($this->product, $this->model->get($sku, true));
    }

    public function testGetBySkuWithSpace()
    {
        $trimmedSku = 'test_sku';
        $sku = 'test_sku ';
        $this->productFactory->expects($this->once())->method('create')
            ->will($this->returnValue($this->product));
        $this->resourceModel->expects($this->once())->method('getIdBySku')->with($sku)
            ->will($this->returnValue('test_id'));
        $this->product->expects($this->once())->method('load')->with('test_id');
        $this->product->expects($this->any())->method('getSku')->willReturn($trimmedSku);
        $this->assertEquals($this->product, $this->model->get($sku));
    }

    public function testGetWithSetStoreId()
    {
        $productId = 123;
        $sku = 'test-sku';
        $storeId = 7;
        $this->productFactory->expects($this->once())->method('create')->willReturn($this->product);
        $this->resourceModel->expects($this->once())->method('getIdBySku')->with($sku)->willReturn($productId);
        $this->product->expects($this->once())->method('setData')->with('store_id', $storeId);
        $this->product->expects($this->once())->method('load')->with($productId);
        $this->product->expects($this->once())->method('getId')->willReturn($productId);
        $this->product->expects($this->any())->method('getSku')->willReturn($sku);
        $this->assertSame($this->product, $this->model->get($sku, false, $storeId));
    }

    /**
     * Test forceReload parameter
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testGetForcedReload()
    {
        $sku = "sku";
        $id = "23";
        $editMode = false;
        $storeId = 0;

        $this->productFactory->expects($this->any())->method('create')
            ->will($this->returnValue($this->product));
        $this->product->expects($this->any())->method('load');
        $this->product->expects($this->any())->method('getId')->willReturn($sku);
        $this->resourceModel->expects($this->any())->method('getIdBySku')
            ->with($sku)->willReturn($id);
        $this->product->expects($this->any())->method('getSku')->willReturn($sku);
        $this->serializerMock->expects($this->exactly(3))->method('serialize');

        $this->assertEquals($this->product, $this->model->get($sku, $editMode, $storeId));
        //second invocation should just return from cache
        $this->assertEquals($this->product, $this->model->get($sku, $editMode, $storeId));
        //force reload
        $this->assertEquals($this->product, $this->model->get($sku, $editMode, $storeId, true));
    }

    /**
     * @param mixed $identifier
     * @param bool $editMode
     * @param mixed $storeId
     *
     * @return void
     *
     * @dataProvider cacheKeyDataProvider
     * @throws NoSuchEntityException
     */
    public function testGetByIdForCacheKeyGenerate($identifier, $editMode, $storeId)
    {
        $callIndex = 0;
        $this->productFactory->expects($this->once())->method('create')
            ->will($this->returnValue($this->product));
        if ($editMode) {
            $this->product->expects($this->at($callIndex))->method('setData')->with('_edit_mode', $editMode);
            ++$callIndex;
        }
        if ($storeId !== null) {
            $this->product->expects($this->at($callIndex))->method('setData')->with('store_id', $storeId);
        }
        $this->product->expects($this->once())->method('load')->with($identifier);
        $this->product->expects($this->atLeastOnce())->method('getId')->willReturn($identifier);
        $this->product->method('getSku')->willReturn('simple');
        $this->assertEquals($this->product, $this->model->getById($identifier, $editMode, $storeId));
        //Second invocation should just return from cache
        $this->assertEquals($this->product, $this->model->getById($identifier, $editMode, $storeId));
    }

    /**
     * Data provider for the key cache generator
     *
     * @return array
     * @throws ReflectionException
     */
    public function cacheKeyDataProvider()
    {
        $anyObject = $this->createPartialMock(stdClass::class, ['getId']);
        $anyObject->expects($this->any())
            ->method('getId')
            ->willReturn(123);

        return [
            [
                'identifier' => 'test-sku',
                'editMode' => false,
                'storeId' => null,
            ],
            [
                'identifier' => 25,
                'editMode' => false,
                'storeId' => null,
            ],
            [
                'identifier' => 25,
                'editMode' => true,
                'storeId' => null,
            ],
            [
                'identifier' => 'test-sku',
                'editMode' => true,
                'storeId' => null,
            ],
            [
                'identifier' => 25,
                'editMode' => true,
                'storeId' => $anyObject,
            ],
            [
                'identifier' => 'test-sku',
                'editMode' => true,
                'storeId' => $anyObject,
            ],
            [
                'identifier' => 25,
                'editMode' => false,
                'storeId' => $anyObject,
            ],
            [

                'identifier' => 'test-sku',
                'editMode' => false,
                'storeId' => $anyObject,
            ],
        ];
    }

    /**
     * Test for getById() method if we try to get products when cache is already filled and is reduced.
     *
     * @return void
     * @throws NoSuchEntityException
     */
    public function testGetByIdWhenCacheReduced()
    {
        $result = [];
        $expectedResult = [];
        $productsCount = $this->cacheLimit * 2;

        $productMocks = $this->getProductMocksForReducedCache($productsCount);
        $productFactoryInvMock = $this->productFactory->expects($this->exactly($productsCount))
            ->method('create');
        call_user_func_array([$productFactoryInvMock, 'willReturnOnConsecutiveCalls'], $productMocks);
        $this->serializerMock->expects($this->atLeastOnce())->method('serialize');

        for ($i = 1; $i <= $productsCount; $i++) {
            $product = $this->model->getById($i, false, 0);
            $result[] = $product->getId();
            $expectedResult[] = $i;
        }

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Get product mocks for testGetByIdWhenCacheReduced() method.
     *
     * @param int $productsCount
     * @return array
     */
    private function getProductMocksForReducedCache($productsCount)
    {
        $productMocks = [];

        for ($i = 1; $i <= $productsCount; $i++) {
            $productMock = $this->getMockBuilder(Product::class)
                ->disableOriginalConstructor()
                ->setMethods(
                    [
                        'getId',
                        'getSku',
                        'load',
                        'setData',
                    ]
                )
                ->getMock();
            $productMock->expects($this->once())->method('load');
            $productMock->expects($this->atLeastOnce())->method('getId')->willReturn($i);
            $productMock->expects($this->atLeastOnce())->method('getSku')->willReturn($i . uniqid());
            $productMocks[] = $productMock;
        }

        return $productMocks;
    }

    /**
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     * @expectedExceptionMessage The product that was requested doesn't exist. Verify the product and try again.
     */
    public function testGetByIdAbsentProduct()
    {
        $this->productFactory->expects($this->once())->method('create')
            ->will($this->returnValue($this->product));
        $this->product->expects($this->once())->method('load')->with('product_id');
        $this->product->expects($this->once())->method('getId')->willReturn(null);
        $this->model->getById('product_id');
    }

    public function testGetList()
    {
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);
        $collectionMock = $this->createMock(Collection::class);
        $eavMock = $this->createMock(AbstractSource::class);
        $attributeMock = $this->createMock(AbstractAttribute::class);
        $this->collectionFactory->expects($this->exactly(2))->method('create')->willReturn($collectionMock);
        $this->product->method('getSku')->willReturn('virtual');
        $collectionMock->expects($this->exactly(2))->method('addAttributeToSelect');
        $collectionMock->expects($this->any())->method('joinAttribute')->withConsecutive(
            ['status', 'catalog_product/status', 'entity_id', null, 'inner'],
            ['visibility', 'catalog_product/visibility', 'entity_id', null, 'inner']
        );

        //$selectQueryMock = $this->createMock(\Magento\Framework\DB\Select::class);

        $this->collectionProcessor->expects($this->exactly(2))
            ->method('process')
            ->with($searchCriteriaMock, $collectionMock);

        /** @var Select|MockObject $selectMock */
        $selectQueryMock = $this->getMockBuilder(Select::class)
            ->disableOriginalConstructor()
            ->getMock();

        $collectionMock->method('getSelect')->willReturn($selectQueryMock);
        $selectQueryMock->expects($this->any())->method('join')->withConsecutive(
            [
                ['mp' => 'marketplace_product'],
                'mp.mageproduct_id = e.entity_id',
                ['seller_id' => 'mp.seller_id']
            ],
            [
                ['stock'=>'cataloginventory_stock_item'],
                'stock.product_id = e.entity_id',
                ['notify_stock_qty '=>'stock.notify_stock_qty ']
            ],
            [
                ['band_user_default'=>'marketplace_userdata'],
                'band_user_default.seller_id = mp.seller_id
            AND band_user_default.store_id = 0',
                [
                'band_user_default.seller_id',
                ]
            ]
        )->willReturn($selectQueryMock);
        $collectionMock->expects($this->exactly(2))->method('load');
        $collectionMock->expects($this->exactly(2))->method('addCategoryIds');
        $collectionMock->expects($this->atLeastOnce())->method('getItems')->willReturn([$this->product]);
        $searchResultsMock = $this->createMock(ProductSearchResultsInterface::class);
        $searchResultsMock->expects($this->once())->method('setSearchCriteria')->with($searchCriteriaMock);
        $searchResultsMock->expects($this->once())->method('setItems');
        $this->product->method('getExtensionAttributes')->willReturn($this->extentionApiMock);
        $stock = [
            [
                'source_code' => 'default',
                'quantity' => 0.0,
                'status' => 1,
                'name' => 'Default Source',
                'source_status' => true,
            ],
        ];
        $this->stockUpdate->method('execute')->with('virtual')->willReturn("0.0");
        $this->extentionApiMock->method('setProductQty')->with($stock[0]['quantity']);
        $this->extentionApiMock->method('setSalableQty')->with($stock[0]['quantity']);
        $this->extentionApiMock->method('setVoucherQty')->with($stock[0]['quantity']);
        $this->product->method('getAttributes')->willReturn([$attributeMock]);
        $this->product->method('getResource')->willReturn($this->resourceModel);
        $this->resourceModel->method('getAttributeRawValue')->willReturn('Saudi');
        $this->resourceModel->method('getAttribute')->willReturn($attributeMock);
        $attributeMock->method('getFrontendInput')->willReturn('select');
        $attributeMock->method('getAttributeCode')->willReturn('vendor_country');
        $attributeMock->method('getIsVisibleOnFront')->willReturn(true);
        $attributeMock->method('getSource')->willReturn($eavMock);
        $eavMock->method('getOptionText')->willReturn('Saudi');
        $attributeMock->method('usesSource')->willReturn(true);
        $this->product->method('setCustomAttribute')->willReturn($this->product);
        $this->searchResultsFactory->expects($this->once())->method('create')->willReturn($searchResultsMock);

        $filterGroupMock = $this->createMock(FilterGroup::class);
        $searchCriteriaMock
            ->method('getFilterGroups')
            ->willReturn([$filterGroupMock]);
        $filterMock = $this->createMock(Filter::class);
        $filterGroupMock->expects($this->atLeastOnce())->method('getFilters')->willReturn([$filterMock]);
        $filterMock->expects($this->atLeastOnce())->method('getField')->willReturn('category_id');

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemCriteria $stockItemCriteriaMock */
        $stockItemCriteriaMock = $this->createMock(StockItemCriteria::class);

        $categoryMock = $this->createMock(CategoryInterface::class);
        $this->categoryRepository->method('get')->willReturn($categoryMock);
        $categoryMock->method('getName')->willReturn('some category name');

        $this->stockItemCriteriaInterfaceFactoryMock->method('create')->willReturn($stockItemCriteriaMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|StockItemCollectionInterface $stockItemListMock */
        $stockItemListMock = $this->createMock(StockItemCollectionInterface::class);

        $this->stockItemRepositoryMock->method('getList')->willReturn($stockItemListMock);
        $stockItemListMock->method('getItems')->willReturn([]);

        $this->assertEquals($searchResultsMock, $this->model->getList($searchCriteriaMock));
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testGetListExceptionError()
    {
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);
        $collectionMock = $this->createMock(Collection::class);
        $attributeMock = $this->createMock(AbstractAttribute::class);
        $this->collectionFactory->expects($this->once())->method('create')->willReturn($collectionMock);
        $this->product->method('getSku')->willReturn('virtual');
        $collectionMock->expects($this->once())->method('addAttributeToSelect');
        $collectionMock->expects($this->any())->method('joinAttribute')->withConsecutive(
            ['status', 'catalog_product/status', 'entity_id', null, 'inner'],
            ['visibility', 'catalog_product/visibility', 'entity_id', null, 'inner']
        );

        $this->collectionProcessor->expects($this->once())
            ->method('process')
            ->with($searchCriteriaMock, $collectionMock);
        /** @var Select|MockObject $selectMock */
        $selectQueryMock = $this->getMockBuilder(Select::class)
            ->disableOriginalConstructor()
            ->getMock();

        $collectionMock->method('getSelect')->willReturn($selectQueryMock);
        $selectQueryMock->expects($this->any())->method('join')->withConsecutive(
            [['mp' => 'marketplace_product'],
                'mp.mageproduct_id = e.entity_id',
                ['seller_id' => 'mp.seller_id']],
            [
                ['stock'=>'cataloginventory_stock_item'],
                'stock.product_id = e.entity_id',
                ['notify_stock_qty '=>'stock.notify_stock_qty ']
            ],
            [
                ['band_user_default'=>'marketplace_userdata'],
                'band_user_default.seller_id = mp.seller_id
            AND band_user_default.store_id = 0',
                [
                'band_user_default.seller_id',
                ]
            ]
        )->willReturn($selectQueryMock);

        $collectionMock->expects($this->once())->method('load');
        $collectionMock->expects($this->once())->method('addCategoryIds');
        $collectionMock->expects($this->atLeastOnce())->method('getItems')->willReturn([$this->product]);

        $extentionApiMock = $this->createPartialMock(
            \Magento\Framework\Api\ExtensionAttributesInterface::class,
            [
                'setProductQty',
                'setSalableQty'
            ]
        );

        $this->product->method('getExtensionAttributes')->willReturn($extentionApiMock);
        $stock = [
            [
                'source_code' => 'default',
                'quantity' => 0.0,
                'status' => 1,
                'name' => 'Default Source',
                'source_status' => true,
            ],
        ];

        $this->stockUpdate->method('execute')->with('virtual')->willReturn("0.0");
        $extentionApiMock->method('setProductQty')->with($stock[0]['quantity']);

        $stockItemCriteriaMock = $this->createPartialMock(
            \Magento\CatalogInventory\Api\StockItemCriteriaInterface::class,
            [
                'addCriteria',
                'setStockStatus',
                'setStockFilter',
                'setScopeFilter',
                'setProductsFilter',
                'setManagedFilter',
                'setQtyFilter',
                'getMapperInterfaceName',
                'addField',
                'addFilter',
                'addOrder',
                'setLimit',
                'removeField',
                'removeAllFields',
                'removeFilter',
                'removeAllFilters',
                'getCriteriaList',
                'getFilters',
                'getOrders',
                'getLimit',
                'getPart',
                'toArray',
                'reset',
            ]);
        $this->stockItemCriteriaInterfaceFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($stockItemCriteriaMock);
        $stockItemCollectionMock = $this->createPartialMock(
            \Magento\CatalogInventory\Api\Data\StockItemCollectionInterface::class,
            [
                'getItems',
                'setItems',
                'getSearchCriteria',
                'setSearchCriteria',
                'getTotalCount',
                'setTotalCount',
            ]);

        $this->stockItemRepositoryMock->expects($this->once())->method('getList')->willReturn($stockItemCollectionMock);

        $stockItemMock = $this->createMock(\Magento\CatalogInventory\Api\Data\StockItemInterface::class);
        $stockItemCollectionMock->expects($this->once())->method('getItems')->willReturn([$stockItemMock]);

        $categoryMock = $this->createMock(CategoryInterface::class);
        $this->categoryRepository->method('get')->willReturn($categoryMock);
        $categoryMock->method('getName')->willReturn('some category name');

        $extentionApiMock->method('setSalableQty')->with($stock[0]['quantity']);
        $searchResultsMock = $this->createMock(ProductSearchResultsInterface::class);
        $this->product->method('getAttributes')->willReturn([$attributeMock]);
        $mockError = new Phrase('Unable to display products');
        $mockClass = new LocalizedException($mockError);
        $this->product->method('getResource')->willThrowException($mockClass);
        $this->resourceModel->method('getAttributeRawValue')->willReturn('Saudi');
        $this->resourceModel->method('getAttribute')->willReturn('Saudi');
        $attributeMock->method('getFrontendInput')->willReturn('select');
        $attributeMock->method('getAttributeCode')->willReturn('vendor_country');
        $attributeMock->method('getIsVisibleOnFront')->willReturn(true);
        $this->product->method('setCustomAttribute')->willReturn($this->product);
        $this->assertEquals($searchResultsMock, $this->model->getList($searchCriteriaMock));
    }

    /**
     * @throws NoSuchEntityException
     * @throws ReflectionException
     */
    public function testGetBrands()
    {
        $storeId = $this->storeMock->method('getId')->willReturn(1);
        $this->storeManager->expects($this->once())->method('getStore')->willReturn($this->storeMock);

        $collectionMock = $this->createMock(WebkulCollection::class);
        $this->collectionFactory->expects($this->once())->method('create')->willReturn($collectionMock);

        $productMock = $this->createMock(Product::class);
        $collectionMock->expects($this->once())->method('getItems')->willReturn([$productMock]);

        $attributeMock = $this->createMock(AttributeInterface::class);
        $productMock->expects($this->once())->method('getCustomAttribute')->willReturn($attributeMock);
        $attributeMock->method('getValue')->willReturn('some brand');

        $mpProductMock = $this->createMock(WebkulProduct::class);
        $this->webkulCollectionFactory->method('create')->willReturnSelf();
        $this->webkulCollectionFactory->method('addStoreFilter')->with($storeId)->willReturnSelf();
        $this->webkulCollectionFactory->method('addFieldToFilter')->willReturnSelf();
        $this->webkulCollectionFactory->method('getFirstItem')->willReturn($mpProductMock);

        $sellerCollectionMock = $this->createMock(SellerCollection::class);
        $this->mpHelper->method('getSellerCollectionObj')->willReturn($sellerCollectionMock);

        $sellerMock = $this->createPartialMock(Seller::class, ['getShopTitle']);
        $sellerCollectionMock->method('getItems')->willReturn([$sellerMock]);
        $sellerMock->method('getShopTitle')->willReturn('some merchant');

        $productMock->method('getCategoryIds')->willReturn(['1']);
        $categoryMock = $this->createMock(CategoryInterface::class);
        $this->categoryRepository->method('get')->willReturn($categoryMock);
        $categoryMock->method('getName')->willReturn('some category name');

        $expectedReturn = [
            [
                'brand' => 'some brand',
                'merchants' => [
                    'some merchant',
                ],
                'categories' => [
                    'some category name',
                ],
            ],
        ];

        $this->assertEquals($expectedReturn, $this->model->getBrands());
    }

    /**
     * @return array
     */
    public function getLinkedProductsListProvider()
    {
        return [
            [
                ProductsInterface::UPSELL,      // link type
                'getUpSellProductIds'           // method name used for this link
            ],
            [
                ProductsInterface::CROSSSELL,   // link type
                'getCrossSellProductIds'        // method name used for this link
            ],
            [
                ProductsInterface::RELATED,     // link type
                'getRelatedProductIds'          // method name used for this link
            ]
        ];
    }

    /**
     * @dataProvider  getLinkedProductsListProvider
     *
     * @param $type
     * @param $methodName
     *
     * @return void
     *
     * @throws LocalizedException
     */
    public function testGetLinkedProductsList($type, $methodName)
    {
        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaInterface $searchCriteriaProductListMock */
        $searchCriteriaProductListMock = $this->createMock(SearchCriteriaInterface::class);

        $this->searchCriteriaBuilder
            ->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaProductListMock);

        $searchCriteriaProductListMock
            ->expects($this->once())
            ->method('setFilterGroups')
            ->willReturnSelf();

        /** @var PHPUnit_Framework_MockObject_MockObject|ProductSearchResultsInterface $productSearchResultMock */
        $productSearchResultMock = $this->createMock(ProductSearchResultsInterface::class);

        $this->productRepositoryMock
            ->expects($this->once())
            ->method('getList')
            ->willReturn($productSearchResultMock);

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $productMock */
        $productMock = $this->createMock(Product::class);

        $productArray = [$productMock];
        $productSearchResultMock
            ->expects($this->once())
            ->method('getItems')
            ->willReturn($productArray);

        $idsArray = [5, 10, 5];
        $productMock
            ->expects($this->exactly(count($productArray)))
            ->method($methodName)
            ->willReturn($idsArray);

        /** @var PHPUnit_Framework_MockObject_MockObject|Product $linkedProductMock */
        $linkedProductMock = $this->createMock(Product::class);

        $this->productRepositoryMock
            ->expects($this->exactly(count($productArray) * count(array_unique($idsArray))))
            ->method('getById')
            ->willReturn($linkedProductMock);

        $linkedProductMock
            ->expects($this->exactly(count($productArray) * count(array_unique($idsArray))))
            ->method('getQuantityAndStockStatus')
            ->willReturn(['qty' => 5]);

        /** @var PHPUnit_Framework_MockObject_MockObject|SearchCriteriaInterface $searchCriteriaMock */
        $searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);

        $getLinkedProductsList = $this->model->getLinkedProductsList($type, $searchCriteriaMock);
        $this->assertInternalType('array', $getLinkedProductsList);
        $this->assertEquals(count($getLinkedProductsList), count($productArray) * count(array_unique($idsArray)));
    }
}
