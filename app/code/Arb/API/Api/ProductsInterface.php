<?php
/**
 * This file file consist of interface for product details and listing
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Api;

/**
 * Product API interface
 */
interface ProductsInterface
{
    const UPSELL = 'upsell';
    const RELATED = 'related';
    const CROSSSELL = 'crosssell';

    const LINKS = [self::UPSELL, self::RELATED, self::CROSSSELL];

    /**
     * Get info about product by product SKU
     *
     * @param string $sku
     * @param bool $editMode
     * @param int|null $storeId
     * @param bool $forceReload
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($sku, $editMode = false, $storeId = null, $forceReload = false);

    /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Get brands
     *
     * @return array
     */
    public function getBrands();

    /**
     * Get linked products list
     *
     * @param string $type
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function getLinkedProductsList(string $type, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
