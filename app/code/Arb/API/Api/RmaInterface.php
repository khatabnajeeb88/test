<?php
/**
 * This file consist of interface for RMA
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Api;

/**
 * Rma API interface
 */
interface RmaInterface
{
    /**
     * Create RMA
     *
     * @param \Arb\API\Api\Magento\Rma\Data\RmaInterface $rmaDataObject
     *
     * @return array|string
     */
    public function create(\Arb\API\Api\Magento\Rma\Data\RmaInterface $rmaDataObject);

    /**
     * Get Return Reasons
     *
     * @return array
     */
    public function getReasons();

    /**
     * Get Item Conditions
     *
     * @return array
     */
    public function getItemConditions();
}
