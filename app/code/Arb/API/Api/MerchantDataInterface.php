<?php
/**
 * This file file consist of interface for merchant product details and listing
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Api;

/**
 * Product API interface
 */
interface MerchantDataInterface
{
  
  /**
   * function to get merchant product data
   *
   * @param integer $id
   * @param array $param
   * @return void
   */
    public function getSellerProducts($id, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
