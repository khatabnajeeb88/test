<?php
/**
 * Interface for validate address API
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Api;

/**
 * Validate address API
 */
interface ValidateAddressInterface
{
    /**
     * Returns if address is valif
     *
     * @param mixed $merchantIds
     * @param string $city
     * @return void
     */
    public function validateAddress($merchantIds, $city);
}
