<?php
/**
 * Overridden file of RMA Interface to add setter/getter
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Api\Magento\Rma\Data;

use Magento\Rma\Api\Data\RmaInterface as ParentInterface;

interface RmaInterface extends ParentInterface
{
    /**
     * @return mixed
     */
    public function getCustomer();

    /**
     * @param $data
     *
     * @return mixed
     */
    public function setCustomer($data);
}
