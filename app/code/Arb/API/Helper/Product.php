<?php
/**
 * This file consist of helper class Product which is used to process product collection data.
 *
 * @category Arb
 * @package Arb_API
 * @author Arb Magento Team
 *
 */

namespace Arb\API\Helper;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Product helper class
 */
class Product
{
    /**
     * Sorting field name
     *
     * @var string
     */
    private $sortBy = 'name';

    /**
     * Sorting direction
     *
     * @var string
     */
    private $direction = 'ASC';

    /**
     * Sorting merged virtual and simple/configurable products collection items
     *
     * @param array $items
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function sortMergedProductCollectionItems(array $items, SearchCriteriaInterface $searchCriteria)
    {
        foreach ($searchCriteria->getSortOrders() as $sortOrder) {
            $this->sortBy = $sortOrder->getField();
            $this->direction = $sortOrder->getDirection();
        }

        if ($this->sortBy === 'price') {
            $prices = $this->getCorrectPrices($items);

            if ($this->direction === 'ASC') {
                asort($prices);
            } else {
                arsort($prices);
            }

            $items = $this->sortIncludingSpecialPrice($items, $prices);
        } else if($this->sortBy === 'position') {
            // set the sort for category position set for products custom popularity sort
            $categoryId = 0;
            foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
                foreach ($filterGroup->getFilters() as $filter) {
                    if ($filter->getField() === 'category_id'
                        && $filter->getConditionType() === 'eq'
                        && is_numeric($filter->getValue())) {
                        $categoryId = $filter->getValue();
                    }
                }
            }

            if ($categoryId) {
                $getPositionData = [];
                $checkItem = [];

                foreach ($items as $item) {
                    if (!in_array($item['sku'], $checkItem)) {
                        $attributes = $item['extension_attributes'];
                        $productPosition = 0;
                        foreach ($attributes->getCategoryLinks() as $categoryData) {
                            if ($categoryData->getCategoryId() == $categoryId) {
                                $productPosition = $categoryData->getPosition();
                            }
                        }
                        if (!isset($getPositionData[$productPosition])) {
                            $getPositionData[$productPosition] = $item;
                        } else {
                            $getPositionData[count($checkItem)] = $item;
                        }

                        $checkItem[] = $item['sku'];
                    }

                }
                if ($this->direction === 'DESC') {
                    krsort($getPositionData);
                } else {
                    ksort($getPositionData);
                }
                $items = $getPositionData;
            }

        } else {
            usort($items, function ($first, $second) {
                $first = $first->getData($this->sortBy);
                $second = $second->getData($this->sortBy);
                if ($first) {
                    if (!is_numeric($first)) {
                        $first = strtolower($first);
                        $second = strtolower($second);
                    }

                    if ($this->direction === 'DESC') {
                        return $first < $second;
                    }

                    return $first > $second;
                } else {
                    return $first;
                }
            });
        }

        return $items;
    }

    /**
     * Getting the correct product price according to the special_price attribute
     *
     * @param array $items
     *
     * @return array
     */
    public function getCorrectPrices(array $items)
    {
        $correctPrice = [];
        /** @var \Magento\Catalog\Model\Product $item */
        foreach ($items as $item) {
            //check if configurable product exists
            if ($item->getTypeId() === 'configurable') {
                $correctPrice[$item->getId()] = $item->getExtensionAttributes()->getConfigurableProductPrice();
                continue;
            }

            $correctPrice[$item->getId()] = $item->getPrice();
        }
        
            return $correctPrice;
    }

    /**
     * @param array $collection
     * @param array $orderArray
     *
     * @return array
     */
    private function sortIncludingSpecialPrice(array $collection, array $orderArray)
    {
        $ordered = [];
        foreach ($orderArray as $key => $value) {
            foreach ($collection as $item) {
                if ((int)$item->getId() === $key) {
                    $ordered[] = $item;
                    break;
                }
            }
        }

        return $ordered;
    }
}
