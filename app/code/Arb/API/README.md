## Synopsis
This is module used for API customization and depends on module Magento_Eav, Magento_Catalog and Webkul_Marketplace.

### API List
1. Product listing page - Customized Magento OOB API to display lables for dropdown attributes.
2. Get product by SKU - Customized Magento OOB API to display lables for dropdown attributes.
3. Get products by merchant id - Custom API to get all products by merchant id.

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module dependency injection details in [di.xml](etc/di.xml).
4. Web API configuration details in [webapi.xml](etc/webapi.xml).



