<?php
/**
 * TokenManagement invalidate token api interface
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */

namespace Arb\TokenManagement\Api;

interface InvalidateTokenInterface
{
    /**
     * Invalidates customer token and logs out customer
     *
     * @api
     * @param int $customer_id
     * @return void
     */
    public function invalidateToken($customer_id);
}
