<?php
/**
 * TokenManagement get-token api interface
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */

namespace Arb\TokenManagement\Api;

interface GetTokenInterface
{
    /**
     * Returns customer token after ESB validation
     *
     * @api
     * @param string $cicnumber
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $phonenumber
     * @param string $sessionId
     * @param string $userId
     * @return array
     */
    public function getToken($cicnumber, $firstname, $lastname, $email, $phonenumber, $sessionId, $userId);
}
