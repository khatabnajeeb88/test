<?php

/**
 * Get token after ESB validation
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */

namespace Arb\TokenManagement\Model;

use Magento\Framework\Exception\LocalizedException;
use \Magento\Store\Model\ScopeInterface;

/**
 * GetToken class to perform ESB validation and customer generation
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class GetToken implements \Arb\TokenManagement\Api\GetTokenInterface
{
    /**
     * Default code for successful validation
     */
    const DEFAULT_SUCCESS_CODE = 'I000000';

    /**
     * Input string for generating random string
     */
    const RANDOM_STRING = 'abcdef0123456789';

    /**
     * SVC code for session validation
     */
    const SVCID = '0046';

    /**
     * SubSvcID code for session validation
     */
    const SUBSVCID = '0406';

    /**
     * Function ID code parameter for session validation
     */
    const FUNCID = '0001';

    /**
     * Message version request parameter
     */
    const MSGVER = '001';

    /**
     * Channel ID for session validation
     */
    const CHID = 'MOBILEWEB';

    /**
     * Sub channel parameter
     */
    const SUBCHID = 'MOBILE_BROWSER_BASED';

    /**
     * IP address to be used in ESB session validation
     */
    const IPADDR = '172.16.3.112';

    /**
     * OSID parameter for ESB validation request
     */
    const OSID = '03';

    /**
     * App ID for ESB request
     */
    const APPTYP = '620';

    /**
     * External system ID for ESB request
     */
    const EXTERNALSYSID = 'MAGENTO';
    
    /**
     * ARB customer group name
     */
    const CUSTOMER_GROUP_NAME = 'ARB users';

    /**
     * Magento default customer group id
     */
    const DEFAULT_CUSTOMER_GROUP_ID = 1;

    /**
     * Magento default email domain
     */
    const DEFAULT_DOMAIN = "@alrajhibank.com.sa";

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Integration\Model\Oauth\TokenFactory
     */
    protected $tokenModelFactory;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curl;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customerModel;

    /**
     * @var \Magento\Integration\Model\CustomerTokenService
     */
    protected $customerTokenServiceFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $random;

    /**
     * @var \Magento\Customer\Model\VisitorFactory
     */
    protected $customerVisitor;

    /**
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * Get token class constructor
     *
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerFactory
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Customer\Model\Customer $customerModel
     * @param \Magento\Integration\Model\CustomerTokenService $customerTokenServiceFactory
     * @param \Magento\Customer\Model\VisitorFactory
     * @param \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Integration\Model\CustomerTokenService $customerTokenServiceFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Math\Random $random,
        \Magento\Customer\Model\VisitorFactory $customerVisitor,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->encryptor = $encryptor;
        $this->customerSession = $customerSession;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->curl = $curl;
        $this->customerModel = $customerModel;
        $this->customerTokenServiceFactory = $customerTokenServiceFactory;
        $this->scopeConfig = $scopeConfig;
        $this->random = $random;
        $this->customerVisitor = $customerVisitor;
        $this->groupRepository = $groupRepository;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->_writer = new \Zend\Log\Writer\Stream(BP . "/var/log/Arb_ESB_Session_Log-".date('Ymd').".log");
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($this->_writer);
    }

    /**
     * Validate session and Generate Customer Token and return it.
     *
     * @param  string $cicnumber
     * @param  string $firstname
     * @param  string $lastname
     * @param  string $email
     * @param  string $phonenumber
     * @param  string $sessionId
     * @param  string $userId
     * @return array $result
     */
    public function getToken($cicnumber, $firstname, $lastname, $email, $phonenumber, $sessionId, $userId)
    {
        // Returning if session id validation failed
        $this->_logger->info('ESB Session validation intiated');
        $esbValidation =  $this->validateSessionId($cicnumber, $sessionId, $userId);
        if (!$esbValidation['success']) {
            //Log an error for future reference.
            $this->_logger->crit($esbValidation['error']);
            $return =  [
                'customer_id' => 0,
                'token' => null,
                'error' => $esbValidation['error']
            ];
            return [$return];
        }

        $customer = $this->isCustomerAvailable($cicnumber, $email, $phonenumber, $firstname, $lastname);
        if (!$customer) {
            $data = [];
            //Getting CIC number from Retail App.
            $data['cic'] = $cicnumber;
            $data['firstname'] = $firstname;
            $data['lastname'] = $lastname;
            //Adding Primary Email ID as per CIC Number.
            $data['email'] = $cicnumber.self::DEFAULT_DOMAIN;
            //Adding Customer Email ID as per retail app.
            $data['customer_email'] = !empty($email) ? $email : '' ;
            //Phone Number from retail app.
            $data['phone_number'] = $phonenumber;
            $customerId = $this->createCustomer($data);
            $customer = $this->customerModel->load($customerId);
            $this->_logger->info('Customer created with Customer ID '.$customerId);
        } else {
            $customerId = $customer->getId();
            $this->_logger->info('Customer is already exist with Customer ID '.$customerId);
        }

        $result = $this->getCustomerToken($customer);
        return [$result];
    }

    /**
     * Create customer programatically and returns customer id
     *
     * @param  array $data
     * @return int
     * @throws LocalizedException
     */
    private function createCustomer($data)
    {
        try {
            $storeId = $this->storeManager->getStore()->getId();
            $websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
            $customer = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($data['email']);
            $customer->setFirstname($data['firstname']);
            $customer->setLastname($data['lastname']);
            $customer->setCustomAttribute('cic', $data['cic']);
            $customer->setCustomAttribute('customer_email', $data['customer_email']);
            $customer->setCustomAttribute('phone_number', $data['phone_number']);
            $customer->setGroupId($this->getDefaultCustomerGroupId());
            $this->_logger->info(json_encode($data));
            return $this->customerRepository->save($customer)->getId();
        } catch (\Exception $e) {
            $this->_logger->crit('createCustomer Error : ' . $data['cic'] . ' === ' . $e->getMessage());
            throw new LocalizedException(
                __('Unable to create customer')
            );
        }
    }

    /**
     * Load customer by CIC number and returns customer id if exist
     *
     * @param string $cicNumber
     * @param string $email
     * @param string $phoneNumber
     * @param string $firstname
     * @param string $lastname
     * @return boolean|int
     * @throws LocalizedException
     */
    private function isCustomerAvailable($cicNumber, $email, $phoneNumber, $firstname, $lastname)
    {
        try {
            $cicEmail = $cicNumber.self::DEFAULT_DOMAIN;
            // checking if customer is available with CIC number
            $customer = $this->customerModel->setWebsiteId(1)->loadByEmail($cicEmail);

            $customerEmail = !empty($email) ? $email : '';

            if (empty($customer->getData())) {
                $this->_logger->info('Customer does not exist with CIC '.$cicNumber);
                return false; // If customer is not exist with CIC
            } else {            
                $customerData = $customer->getData();    

                $customerId = $customerData['entity_id'];                
                $existingCustomerFirstName = $customerData['firstname'];
                $existingCustomerLastName = $customerData['lastname'];
                $existingCustomerEmail = $customerData['email'];

                $customerData = $customer->getDataModel();
                $customerData->setId($customerId);
                $customerData->setFirstname($firstname);
                $customerData->setLastname($lastname);
                $customerData->setEmail($customer['email']);                
               
                $existingCustomerPhoneNumber = !empty($customerData->getCustomAttribute('phone_number'))? $customerData->getCustomAttribute('phone_number')->getValue() : "";
                //Get Email From the customer.
                $existingCustomerEmailId = !empty($customerData->getCustomAttribute('customer_email'))? $customerData->getCustomAttribute('customer_email')->getValue() : "";                
                
                $customerData->setCustomAttribute('phone_number', $phoneNumber);

                // Set Email if the email from retail App is different than existing email.
                // if email id is null then not allowed.
                if (($customerEmail != $existingCustomerEmailId) && !empty($customerEmail)) {
                    $customerData->setCustomAttribute('customer_email', $customerEmail);
                }

                //update customer only when there is any change in details
                if($existingCustomerFirstName != $firstname || $existingCustomerLastName != $lastname || $existingCustomerPhoneNumber != $phoneNumber || $existingCustomerEmailId != $email ) {
                    $customer->updateData($customerData);
                    $customer->save();
                }
               
                $this->_logger->info('Customer exist with CIC '.$cicNumber);
                return $customer;
            }
        } catch (\Exception $e) {
            $this->_logger->crit('isCustomerAvailable Error : ' . $cicNumber . ' === ' . $e->getMessage());
            throw new LocalizedException(
                __('Unable to get customer details by CIC')
            );
        }
    }

    /**
     * Returns customer token
     *
     * @param int $customerId
     * @return array
     */
    protected function getCustomerToken($customer)
    {
        $customerId = $customer->getId();
        $tokenModel = $this->tokenModelFactory->create()->loadByCustomerId($customerId);
        //Checking if customer token is available for customer
        if ($tokenModel->getToken()) {
            //Customer token revoke
            $this->customerTokenServiceFactory->revokeCustomerAccessToken($customerId);
            $this->_logger->info('Existing Token revoked for '.$customerId);
        }

        //Loggin customer session by customer object
        $this->customerSession->setCustomerAsLoggedIn($customer);
        $customerTokenObject = $this->tokenModelFactory->create();

        //Create customer token
        $customerToken = $customerTokenObject->createCustomerToken($customerId)->getToken();
        $this->addCustomerVisitor($this->customerSession->getSessionId(), $customerId);
        $this->_logger->info("Customer token created for customer id ". $customerId);
        return [
            'customer_id' => $customerId,
            'token' => $customerToken,
            'error' => null
        ];
    }

    /**
     * Adds login customer data in customer_visitor table
     *
     * @param  string $sessionId
     * @param  string $customerId
     * @return void
     */
    private function addCustomerVisitor($sessionId, $customerId)
    {
        $data = [
            'customer_id' => $customerId,
            'session_id' => $sessionId
        ];

        $customerVisitor = $this->customerVisitor->create();
        try {
            $customerVisitor->setData($data)->save();
        } catch (\Exception $e) {
            $this->_logger->crit('addCustomerVisitor : ' . $customerId . ' === ' . $e->getMessage());
            throw new LocalizedException(
                __('Unable to add customer visitor data')
            );
        }
    }

    /**
     * Returns customer group id
     *
     * @return int
     */
    private function getDefaultCustomerGroupId()
    {
        try {
            $this->searchCriteriaBuilder->addFilter('customer_group_code', self::CUSTOMER_GROUP_NAME);
            $customerGroups = $this->groupRepository->getList($this->searchCriteriaBuilder->create());
            if (empty($customerGroups->getItems())) {
                return self::DEFAULT_CUSTOMER_GROUP_ID;
            } else {
                $customerGroup = current($customerGroups->getItems());
                $groupId =  $customerGroup->getId();
                return (int) $groupId;
            }
        } catch (\Exception $e) {
            $this->_logger->crit('getDefaultCustomerGroupId : ' . $e->getMessage());
            return self::DEFAULT_CUSTOMER_GROUP_ID;
        }
    }

    /**
     * call SOAP Client for EBS API
     *
     * @param string $endPoint
     * @param string $xmlRequest
     * @return array $xmlResponseStatus
     */
    private function _callSoapClient($endPoint, $xmlRequest)
    {
        try {
            $return =
                [
                    'success' => false,
                    'error' => null
                ];
            //Curl request header set for SOAP API
            $this->curl->setHeaders(
                ['Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($xmlRequest)]
            );

            $this->curl->setOption(CURLOPT_RETURNTRANSFER, 1);
            $this->curl->post($endPoint, $xmlRequest);
            $xmlResponse = $this->curl->getBody();
            $this->_logger->info('validateSessionId Response: ' . $xmlResponse);
            // Convert XML response to Array
            $xmlResponse = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xmlResponse);
            $xml = simplexml_load_string($xmlResponse);
            $xmlResponseStatus = json_decode(json_encode((array) $xml), true);
           
            if (isset($xmlResponseStatus['soapenvBody']['userValidateSessionRs']['Hdr']['arbStatus'])) {
                $response = $xmlResponseStatus['soapenvBody']['userValidateSessionRs']['Hdr']['arbStatus'];
                if (isset($response['arbStatusCd']) && $response['arbStatusCd'] == self::DEFAULT_SUCCESS_CODE) {
                    $return['success'] = true;
                } else {
                    $return['error'] = isset($response['arbStatusDesc']) ? $response['arbStatusDesc'] : __('Invalid Response');
                }
            } else {
                $return['error'] = __('Invalid Request or Response');
            }
        } catch (\Exception $e) {
            //return SOAP Client Error
            $this->_logger->crit('_callSoapClient : ' . $e->getMessage());
            $return['error'] = $e->getMessage();
        }

        return $return;
    }

    /**
     * ESB session validation
     *
     * @param string $cicNumber
     * @param string $sessionId
     * @param string $userId
     * @return array
     */
    protected function validateSessionId($cicNumber, $sessionId, $userId)
    {
        $endPoint = $this->scopeConfig->getValue('esbtoken/token/url', ScopeInterface::SCOPE_STORE);
        $storeCode = $this->storeManager->getStore()->getCode();
        $languageCode = ($storeCode == 'ar_SA') ? 'AR' : 'EN';

        $xmlRequest = '<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope  xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
                xmlns:user="http://www.alrajhiwebservices.com/UserAuth" 
                xmlns:alr1="http://www.alrajhiwebservices.com/">
                <soapenv:Header/>
                <soapenv:Body>
                    <user:ValidateSessionRq>
                        <Hdr>
                            <alr1:Msg>
                                <alr1:RqID>' . $this->getRandomString() . '</alr1:RqID>
                                <alr1:SvcID>' . self::SVCID . '</alr1:SvcID>
                                <alr1:SubSvcID>' . self::SUBSVCID . '</alr1:SubSvcID>
                                <alr1:FuncID>' . self::FUNCID . '</alr1:FuncID>
                                <alr1:MsgTimestamp>' . $this->getCurrentDateTime() . '</alr1:MsgTimestamp>
                                <alr1:MsgVer>' . self::MSGVER . '</alr1:MsgVer>
                            </alr1:Msg>
                            <alr1:Agt>
                                <alr1:CICNum>' . $cicNumber . '</alr1:CICNum>
                                <alr1:UserLang>' . $languageCode . '</alr1:UserLang>
                            </alr1:Agt>
                            <alr1:Sys>
                                <alr1:ChID>' . self::CHID . '</alr1:ChID>
                                <alr1:SubChID>' . self::SUBCHID . '</alr1:SubChID>
                                <alr1:ExternalSysID>' . self::EXTERNALSYSID . '</alr1:ExternalSysID>
                                <alr1:SessionID>' . $sessionId . '</alr1:SessionID>
                                <alr1:LoginInfo>
                                    <alr1:IPAddr>' . self::IPADDR . '</alr1:IPAddr>
                                    <alr1:UserID>' . $userId . '</alr1:UserID>
                                </alr1:LoginInfo>
                                <alr1:OSID>' . self::OSID . '</alr1:OSID>
                                <alr1:SessionLang>' . $languageCode . '</alr1:SessionLang>
                                <alr1:AppTyp>' . self::APPTYP . '</alr1:AppTyp>
                            </alr1:Sys>
                        </Hdr>
                    </user:ValidateSessionRq>
                </soapenv:Body>
            </soapenv:Envelope>';
        $this->_logger->info('validateSessionId Request: ' . $xmlRequest);
        $xmlResponse = $this->_callSoapClient($endPoint, $xmlRequest);
        return $xmlResponse;
    }

    /**
     * Get Random String
     *
     * @param void
     * @return string
     */
    private function getRandomString()
    {
        $id = $this->random->getRandomString(8, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(4, self::RANDOM_STRING) . "-";
        $id .= $this->random->getRandomString(12, self::RANDOM_STRING);

        return $id;
    }

    /**
     * Returns current date time
     *
     * @return string
     */
    private function getCurrentDateTime()
    {
        return date("Y-m-d\TH:i:s", time());
    }
}
