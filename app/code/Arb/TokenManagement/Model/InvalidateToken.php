<?php
/**
 * Invalidates customer token
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */
namespace Arb\TokenManagement\Model;

use Magento\Framework\Exception\LocalizedException;

/**
 * InvalidateToken class to destroy customer session and invalidates customer token
 */
class InvalidateToken implements \Arb\TokenManagement\Api\InvalidateTokenInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Integration\Model\Oauth\TokenFactory
     */
    protected $tokenModelFactory;

    /**
     * @var \Magento\Integration\Model\CustomerTokenService
     */
    protected $customerTokenServiceFactory;

     /**
      * @var \Magento\Customer\Model\ResourceModel\Visitor\CollectionFactory
      */
    protected $visitorCollectionFactory;
    
    /**
     * @var \Magento\Framework\Session\SaveHandlerInterface
     */
    protected $saveHandler;

    /**
     * @var Magento\Framework\Webapi\Request
     */
    protected $webRequest;

    /**
     * @var Magento\Customer\Model\LoggerFactory
     */
    protected $logFactory;

    /**
     * Invalidate token and customer session
     *
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory
     * @param \Magento\Integration\Model\CustomerTokenService $customerTokenServiceFactory
     * @param \Magento\Customer\Model\ResourceModel\Visitor\CollectionFactory $visitorCollectionFactory
     * @param \Magento\Framework\Session\SaveHandlerInterface $saveHandler
     * @param \Magento\Framework\Webapi\Request $webRequest
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory,
        \Magento\Integration\Model\CustomerTokenService $customerTokenServiceFactory,
        \Magento\Customer\Model\ResourceModel\Visitor\CollectionFactory $visitorCollectionFactory,
        \Magento\Framework\Session\SaveHandlerInterface $saveHandler,
        \Magento\Framework\Webapi\Request $webRequest,
        \Magento\Customer\Model\LoggerFactory $logFactory
    ) {
        $this->customerSession = $customerSession;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->customerTokenServiceFactory = $customerTokenServiceFactory;
        $this->visitorCollectionFactory = $visitorCollectionFactory;
        $this->saveHandler = $saveHandler;
        $this->webRequest = $webRequest;
        $this->logFactory = $logFactory;
    }

    /**
     * @inheritdoc
     */
    public function invalidateToken($customer_id)
    {
        return  $this->revokeCustomerAccess($customer_id);
    }

    /**
     * Revokes customer session and token
     *
     * @param string $token
     * @param int $customerId
     * @return array
     */
    protected function revokeCustomerAccess($customerId)
    {
        $logger = $this->getLoggerFunction();
        $logger->info('revokeCustomerAccess : '.$customerId);

        $tokenModel = $this->tokenModelFactory->create()->loadByCustomerId($customerId);
        
        // validating header token with customer
        $authorizationHeaderValue = $this->webRequest->getHeader('Authorization');
        $headerPieces = explode(" ", $authorizationHeaderValue);
        $bearerToken = $headerPieces[1];
        
        //Checking if customer token is available for customer
        if ($tokenModel->getToken() == $bearerToken) {
            //Customer token revoke
            $this->customerTokenServiceFactory->revokeCustomerAccessToken($customerId);
        } else {
            return false;
        }

        try {
            $this->destroyCustomerSessions($customerId);
            $this->updateCustomerLoginLogs($customerId);
            return true;
        } catch (\Exception $e) {
            $logger = $this->getLoggerFunction();
            $logger->crit('revokeCustomerAccess : '.$customerId.' === '.$e->getMessage());
            throw new LocalizedException(
                __('Unable to revoke customer session')
            );
        }
    }

    /**
     * Destroy all active customer sessions by customer id
     * Customer sessions which should be deleted are collecting  from the "customer_visitor" table
     *
     * @param string|int $customerId
     * @return void
     */
    private function destroyCustomerSessions($customerId)
    {
        
        /** @var \Magento\Customer\Model\ResourceModel\Visitor\Collection $visitorCollection */
        $visitorCollection = $this->visitorCollectionFactory->create();
        $visitorCollection->addFieldToFilter('customer_id', $customerId);
        
        if ($visitorCollection->getSize() > 0) {
            /** @var \Magento\Customer\Model\Visitor $visitor */
            foreach ($visitorCollection->getItems() as $visitor) {
                $sessionId = $visitor->getSessionId();
                $this->customerSession->start();
                $this->saveHandler->destroy($sessionId);
                $this->customerSession->writeClose();
                $visitor->setData('last_visit_at', date('Y-m-d H:i:s'));
                $visitor->save();
            }
        }
    }

    /**
     * Updates customer log out time in customer_log table
     *
     * @param  int $customerId
     * @return void
     */
    private function updateCustomerLoginLogs($customerId)
    {
        $logger = $this->logFactory->create();
        $data = ['last_logout_at' => date('Y-m-d H:i:s')];
        $logger->log($customerId, $data);
    }

    /**
     * Returns log object
     *
     * @return \Zend\Log\Logger
     */
    protected function getLoggerFunction()
    {
            $this->_writer = new \Zend\Log\Writer\Stream(BP . "/var/log/Arb_ESB_Session_Log-".date('Ymd').".log");
            $this->_logger = new \Zend\Log\Logger();
            return $this->_logger->addWriter($this->_writer);
    }
}
