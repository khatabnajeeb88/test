<?php
namespace Arb\TokenManagement\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
/**
 * @covers \Arb\TokenManagement\Model\GetToken
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class GetTokenTest extends TestCase
{
    /**#@+
     * Sample customer data
     */
    const CUSTOMER_ID = 1;
    const CUSTOMER_EMAIL = 'CIC298927@alrajhibank.com.sa';
    const WEBSITE_ID = 1;
    /**
     * Mock customerRepository
     *
     * @var \Magento\Customer\Api\CustomerRepositoryInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerRepository;

    /**
     * Mock searchCriteriaBuilder
     *
     * @var \Magento\Framework\Api\SearchCriteriaBuilder|PHPUnit_Framework_MockObject_MockObject
     */
    private $searchCriteriaBuilder;

    /**
     * Mock storeManager
     *
     * @var \Magento\Store\Model\StoreManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManager;

    /**
     * Mock customerFactoryInstance
     *
     * @var \Magento\Customer\Api\Data\CustomerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerFactoryInstance;

    /**
     * Mock customerFactory
     *
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerFactory;

    /**
     * Mock encryptor
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $encryptor;

    /**
     * Mock customerSession
     *
     * @var \Magento\Customer\Model\Session|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerSession;

    /**
     * Mock tokenModelFactoryInstance
     *
     * @var \Magento\Integration\Model\Oauth\Token|PHPUnit_Framework_MockObject_MockObject
     */
    private $tokenModelFactoryInstance;

    /**
     * Mock tokenModelFactory
     *
     * @var \Magento\Integration\Model\Oauth\TokenFactory|PHPUnit_Framework_MockObject_MockObject
     */
    private $tokenModelFactory;

    /**
     * Mock curl
     *
     * @var \Magento\Framework\HTTP\Client\Curl|PHPUnit_Framework_MockObject_MockObject
     */
    private $curl;

    /**
     * Mock customerModel
     *
     * @var \Magento\Customer\Model\Customer|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerModel;

    /**
     * Mock customerTokenServiceFactory
     *
     * @var \Magento\Integration\Model\CustomerTokenService|PHPUnit_Framework_MockObject_MockObject
     */
    private $customerTokenServiceFactory;

    /**
     * Mock scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfig;

    /**
     * Mock random
     *
     * @var \Magento\Framework\Math\Random|PHPUnit_Framework_MockObject_MockObject
     */
    private $random;

    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Object to test
     *
     * @var \Arb\TokenManagement\Model\GetToken
     */
    private $testObject;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->customerRepository = $this->getMockBuilder(\Magento\Customer\Api\CustomerRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["getList","getItems","getId",
                "save","getEmail","getCustomAttribute",
                "setCustomAttribute","setGroupId","getValue","setFirstname","setLastname"])
            ->getMockForAbstractClass();
        $this->searchCriteriaBuilder = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(["addFilter","create"])
            ->getMockForAbstractClass();
        $this->searchCriteriaInterface = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["addFilter","create"])
            ->getMockForAbstractClass();
        $this->storeManager = $this->getMockBuilder(StoreManagerInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["getStore","getCode","getId","getWebsiteId"])
            ->getMockForAbstractClass();
        $this->storeManager->expects($this->any())->method('getStore')->willReturnSelf();
        $this->storeManager->expects($this->any())->method('getId')->willReturn(1);
        $this->storeManager->expects($this->any())->method('getWebsiteId')->willReturn(1);
        $this->customerFactoryInstance = $this->createMock(\Magento\Customer\Api\Data\CustomerInterface::class);
        $this->customerFactory = $this->createMock(\Magento\Customer\Api\Data\CustomerInterfaceFactory::class);
        $this->customerFactory->method('create')->willReturn($this->customerFactoryInstance);
        $this->encryptor = $this->createMock(\Magento\Framework\Encryption\EncryptorInterface::class);
        $this->customerSession = $this->createMock(\Magento\Customer\Model\Session::class);
        $this->tokenModelFactoryInstance = $this->createMock(\Magento\Integration\Model\Oauth\Token::class);

        $this->tokenModelFactory = $this->getMockBuilder(\Magento\Integration\Model\Oauth\TokenFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create","loadByCustomerId","getToken","createCustomerToken"])
            ->getMock();

        $this->curl = $this->getMockBuilder(\Magento\Framework\HTTP\Client\Curl::class)->getMock();
        $this->filterBuilder = $this->getMockBuilder(\Magento\Framework\Api\FilterBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(["setField","setValue","setConditionType","create"])
            ->getMock();
        $this->filterGroupBuilder = $this->getMockBuilder(\Magento\Framework\Api\Search\FilterGroupBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(["addFilter","create"])
            ->getMock();
        $this->customerVisitor = $this->getMockBuilder(\Magento\Customer\Model\VisitorFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create","setData","save"])
            ->getMock();
        //$this->customerModel = $this->createMock(\Magento\Customer\Model\Customer::class);
        $this->customerModel = $this->getMockBuilder(\Magento\Customer\Model\Customer::class)
            ->disableOriginalConstructor()
            ->setMethods([
                "setWebsiteId",
                "loadByEmail",
                "getData",
                "getDataModel",
                "setId", 
                "setFirstname", 
                "setLastname", 
                "setEmail", 
                "getCustomAttribute", 
                "setCustomAttribute", 
                "updateData", 
                "save",
                "load",
                "getId"
            ])
            ->getMockForAbstractClass();
        $this->groupRepository = $this->getMockBuilder(\Magento\Customer\Api\GroupRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["getList","getItems","getId"])
            ->getMockForAbstractClass();
        $this->customerTokenServiceFactory = $this->createMock(\Magento\Integration\Model\CustomerTokenService::class);
        $this->scopeConfig = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        $this->random = $this->createMock(\Magento\Framework\Math\Random::class);
        $this->filter = $this->createMock(\Magento\Framework\Api\Filter::class);
        $this->attribute = $this->getMockBuilder(\Magento\Framework\Api\AttributeInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(["getValue","setCustomAttribute","setGroupId"])
            ->getMockForAbstractClass();
        $this->customer = $this->getMockBuilder(Customer::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'load',
                    'getId',
                    'getEmail',
                    'getWebsiteId',
                    '__wakeup',
                    'setEmail',
                    'setWebsiteId',
                    'loadByEmail',
                ]
            )
            ->getMock();
         $this->getTokenMock = $this->objectManager->getObject(
            \Arb\TokenManagement\Model\GetToken::class,
            [
                'customerRepository' => $this->customerRepository,
                'searchCriteriaBuilder' => $this->searchCriteriaBuilder,
                'storeManager' => $this->storeManager,
                'customerFactory' => $this->customerFactory,
                'encryptor' => $this->encryptor,
                'customerSession' => $this->customerSession,
                'tokenModelFactory' => $this->tokenModelFactory,
                'curl' => $this->curl,
                'customerModel' => $this->customerModel,
                'customerTokenServiceFactory' => $this->customerTokenServiceFactory,
                'scopeConfig' => $this->scopeConfig,
                'random' => $this->random,
                "filterBuilder"=>$this->filterBuilder,
                "filterGroupBuilder"=>$this->filterGroupBuilder,
                "customerVisitor"=>$this->customerVisitor,
                "groupRepository"=>$this->groupRepository
            ]
        );
    }
   
    public function testGetToken()
    {
        $this->storeManager->method("getCode")->willReturn("ar_SA");
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->customerRepository->method("getList")->willReturnSelf();
        $this->customerRepository->method("getItems")->willReturn([$this->createMock(\Magento\Customer\Api\Data\CustomerInterface::class)]);
        $this->customerRepository->method("getId")->willReturn(30);
        $this->customerRepository->method("save")->willReturnSelf();
        $this->customerRepository->method("getCustomAttribute")->willReturnSelf();
        $this->customerRepository->method("getValue")->willReturn("");
        $this->customerRepository->method("getEmail")->willReturn("test@example.com");
        $this->groupRepository->method("getList")->willReturnSelf();
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("wdsakjgsaugkgksaj");
        $this->tokenModelFactory->method('createCustomerToken')->willReturnSelf();
        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);
        $this->customerVisitor->method('create')->willReturnSelf();
        $this->customerVisitor->method('setData')->willReturnSelf();
        $this->customerVisitor->method('save')->willReturnSelf();
        $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">      
         <userValidateSessionRs>
         <Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </Hdr>
         </userValidateSessionRs>      
   </soapenv:Body>
</soapenv:Envelope>';
        $this->curl->method("getBody")->willReturn($xml);
        $this->customerModel->expects($this->any())
            ->method('setWebsiteId')
            ->willReturn($this->customerModel);
        $this->customerModel->expects($this->any())
            ->method('loadByEmail')
            ->with(self::CUSTOMER_EMAIL)
            ->willReturn($this->customerModel);
        
        //  $this->getTokenMock->getToken(
        //     "CIC298927",
        //     "Tester 1",
        //     "Tester 2",
        //     "test@example.com",
        //     "216896321",
        //     "2396wsakjdgkjgsad",
        //     "10"
        // );
    }
    
     /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testGetTokenValidateWithoutError()
    {
        $this->storeManager->method("getCode")->willReturn("ar_SA");
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->customerRepository->method("getList")->willReturnSelf();
        $this->customerRepository->method("getItems")->willReturn([$this->createMock(\Magento\Customer\Api\Data\CustomerInterface::class)]);
        $this->customerRepository->method("getId")->willReturn(30);
        $this->customerRepository->method("save")->willReturnSelf();
        $this->groupRepository->method("getList")->willReturnSelf();
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("wdsakjgsaugkgksaj");
        $this->tokenModelFactory->method('createCustomerToken')->willReturnSelf();
        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);
        $this->customerVisitor->method('create')->willReturnSelf();
        $this->customerVisitor->method('setData')->willThrowException(new \Exception("invalid"));
        $this->customerModel->expects($this->any())
            ->method('setWebsiteId')
            ->willReturn($this->customerModel);
       $this->customerModel->expects($this->any())
            ->method('loadByEmail')
            ->with(self::CUSTOMER_EMAIL)
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('getData')
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('getDataModel')
            ->willReturn($this->customerModel);

        $this->customerModel->method('setId')->willReturnSelf();


        $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">      
         <userValidateSessionRs>
         <Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </Hdr>
         </userValidateSessionRs>      
   </soapenv:Body>
</soapenv:Envelope>';
        $this->curl->method("getBody")->willReturn($xml);
        $this->getTokenMock->getToken(
            "CIC298927",
            "Tester 1",
            "Tester 2",
            "test@example.com",
            "216896321",
            "2396wsakjdgkjgsad",
            "10"
        );
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @expectedExceptionMessage Unable to get customer details by CIC
     * @throws LocalizedException
     */
    public function testGetTokenValidateError()
    {
        $this->storeManager->method("getCode")->willReturn("ar_SA");
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->customerRepository->method("getList")->willReturnSelf();
        $this->customerRepository->method("getItems")->willReturn([$this->createMock(\Magento\Customer\Api\Data\CustomerInterface::class)]);
        $this->customerRepository->method("getId")->willReturn(30);
        $this->customerRepository->method("save")->willReturnSelf();
        $this->groupRepository->method("getList")->willReturnSelf();
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("wdsakjgsaugkgksaj");
        $this->tokenModelFactory->method('createCustomerToken')->willReturnSelf();
        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);
        $this->customerVisitor->method('create')->willReturnSelf();
        $this->customerVisitor->method('setData')->willThrowException(new \Exception("invalid"));
        $this->customerModel->expects($this->any())
            ->method('setWebsiteId')
            ->willReturn($this->customerModel);
       $this->customerModel->expects($this->any())
            ->method('loadByEmail')
            ->with(self::CUSTOMER_EMAIL)
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('getData')
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('getDataModel')
            ->willReturn($this->customerModel);

        $this->customerModel->method('setEmail')->with('dd');

        $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">      
         <userValidateSessionRs>
         <Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </Hdr>
         </userValidateSessionRs>      
   </soapenv:Body>
</soapenv:Envelope>';
        $this->curl->method("getBody")->willReturn($xml);
        $this->getTokenMock->getToken(
            "CIC298927",
            "Tester 1",
            "Tester 2",
            "test@example.com",
            "216896321",
            "2396wsakjdgkjgsad",
            "10"
        );
    }

    public function testCallSoapClient()
    {
        $this->storeManager->method("getCode")->willReturn("ar_SA");
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->customerRepository->method("getList")->willReturnSelf();
        $this->customerRepository->method("getItems")->willReturn([$this->createMock(\Magento\Customer\Api\Data\CustomerInterface::class)]);
        $this->customerRepository->method("getId")->willReturn(30);
        $this->customerRepository->method("save")->willReturnSelf();
        $this->groupRepository->method("getList")->willReturnSelf();
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("wdsakjgsaugkgksaj");
        $this->tokenModelFactory->method('createCustomerToken')->willReturnSelf();
        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);
        $this->customerVisitor->method('create')->willReturnSelf();
        $this->customerVisitor->method('setData')->willReturnSelf();
        $this->customerVisitor->method('save')->willReturnSelf();
        $this->curl->method("getBody")->willThrowException(new \Exception("invalid"));
        $this->getTokenMock->getToken(
            "CIC298927",
            "Tester 1",
            "Tester 2",
            "test@example.com",
            "216896321",
            "2396wsakjdgkjgsad",
            "10"
        );
    }
    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @expectedExceptionMessage Unable to create customer
     * @throws LocalizedException
     */
    public function testCreateCustomer()
    {
        $this->storeManager->method("getCode")->willReturn("ar_SA");
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->customerRepository->method("getList")->willReturnSelf();
        $this->customerRepository->method("getItems")->willReturn([$this->createMock(\Magento\Customer\Api\Data\CustomerInterface::class)]);
        $this->customerRepository->method("getId")->willReturn(30);
        $this->customerRepository->method("save")->willThrowException(new \Exception("invalid"));

        $this->searchResultMock = $this->getMockBuilder(\Magento\Customer\Api\Data\GroupSearchResultsInterface::class)
            ->getMockForAbstractClass();

        $this->groupRepository->method("getList")->willReturn($this->searchResultMock);


        $groupTest = $this->getMockBuilder(\Magento\Customer\Api\Data\GroupInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getCode', 'getId'])
            ->getMockForAbstractClass();
        $groupTest->expects($this->any())->method('getCode')->willReturn(__('NOT LOGGED IN'));
        $groupTest->expects($this->any())->method('getId')->willReturn('0');
        $groups = [$groupTest];

        $this->searchResultMock->expects($this->any())->method('getItems')->willReturn($groups);

        
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("wdsakjgsaugkgksaj");
        $this->tokenModelFactory->method('createCustomerToken')->willReturnSelf();
        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);
        $this->customerVisitor->method('create')->willReturnSelf();
        $this->customerVisitor->method('setData')->willReturnSelf();
        $this->customerVisitor->method('save')->willReturnSelf();
       
        $this->customerModel->expects($this->any())
            ->method('setWebsiteId')
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('load')
            ->with(30)
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('loadByEmail')
            ->with(self::CUSTOMER_EMAIL)
            ->willReturn($this->customerModel);
        $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">      
         <userValidateSessionRs>
         <Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </Hdr>
         </userValidateSessionRs>      
   </soapenv:Body>
</soapenv:Envelope>';
        $this->curl->method("getBody")->willReturn($xml);
        $this->getTokenMock->getToken(
            "CIC298927",
            "Tester 1",
            "Tester 2",
            "test@example.com",
            "216896321",
            "2396wsakjdgkjgsad",
            "10"
        );
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     * @expectedExceptionMessage Unable to create customer
     * @throws LocalizedException
     */
    public function testCreateCustomerWithError()
    {
        $this->storeManager->method("getCode")->willReturn("ar_SA");
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->customerRepository->method("getList")->willReturnSelf();
        $this->customerRepository->method("getItems")->willReturn([$this->createMock(\Magento\Customer\Api\Data\CustomerInterface::class)]);
        $this->customerRepository->method("getId")->willReturn(30);
        $this->customerRepository->method("save")->willThrowException(new \Exception("invalid"));

        $this->searchResultMock = $this->getMockBuilder(\Magento\Customer\Api\Data\GroupSearchResultsInterface::class)
            ->getMockForAbstractClass();

        $this->groupRepository->method("getList")->with([])->willReturn($this->searchResultMock);


        $groupTest = $this->getMockBuilder(\Magento\Customer\Api\Data\GroupInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getCode', 'getId'])
            ->getMockForAbstractClass();
        $groupTest->expects($this->any())->method('getCode')->willReturn(__('NOT LOGGED IN'));
        $groupTest->expects($this->any())->method('getId')->willReturn('0');
        $groups = [$groupTest];

        $this->searchResultMock->expects($this->any())->method('getItems')->willReturn($groups);

        
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("wdsakjgsaugkgksaj");
        $this->tokenModelFactory->method('createCustomerToken')->willReturnSelf();
        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);
        $this->customerVisitor->method('create')->willReturnSelf();
        $this->customerVisitor->method('setData')->willReturnSelf();
        $this->customerVisitor->method('save')->willReturnSelf();
       
        $this->customerModel->expects($this->any())
            ->method('setWebsiteId')
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('load')
            ->with(30)
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('loadByEmail')
            ->with(self::CUSTOMER_EMAIL)
            ->willReturn($this->customerModel);
        $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">      
         <userValidateSessionRs>
         <Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </Hdr>
         </userValidateSessionRs>      
   </soapenv:Body>
</soapenv:Envelope>';
        $this->curl->method("getBody")->willReturn($xml);
        $this->getTokenMock->getToken(
            "CIC298927",
            "Tester 1",
            "Tester 2",
            "test@example.com",
            "216896321",
            "2396wsakjdgkjgsad",
            "10"
        );
    }

    public function testGetTokenNoItems()
    {
        $this->storeManager->method("getCode")->willReturn("ar_SA");
        $this->searchCriteriaBuilder->method("addFilter")->willReturnSelf();
        $this->searchCriteriaBuilder->method("create")->willReturn($this->searchCriteriaInterface);
        $this->customerRepository->method("getList")->willReturnSelf();
        $this->customerRepository->method("getItems")->willReturn("");
        $this->customerRepository->method("getId")->willReturn(30);
        $this->customerRepository->method("save")->willReturnSelf();
        $this->groupRepository->method("getList")->willReturnSelf();
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("wdsakjgsaugkgksaj");
        $this->tokenModelFactory->method('createCustomerToken')->willReturnSelf();
        $this->filterBuilder->method('setField')->willReturnSelf();
        $this->filterBuilder->method('setValue')->willReturnSelf();
        $this->filterBuilder->method('setConditionType')->willReturnSelf();
        $this->filterBuilder->method('create')->willReturn($this->filter);
        $this->filterGroupBuilder->method('addFilter')->willReturnSelf();
        $this->filterGroupBuilder->method('create')->willReturn([$this->filterGroupBuilder]);
        $this->customerVisitor->method('create')->willReturnSelf();
        $this->customerVisitor->method('setData')->willReturnSelf();
        $this->customerVisitor->method('save')->willReturnSelf();
        $this->customerModel->expects($this->any())
            ->method('setWebsiteId')
            ->willReturn($this->customerModel);
        
        $this->customerModel->expects($this->any())
            ->method('load')
            ->with(30)
            ->willReturn($this->customerModel);

        $this->customerModel->expects($this->any())
            ->method('loadByEmail')
            ->with(self::CUSTOMER_EMAIL)
            ->willReturn($this->customerModel);
        $xml = '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body xmlns:ejad="http://www.ejada.com/" xmlns:arb="http://www.alrajhiwebservices.com/">      
         <userValidateSessionRs>
         <Hdr>
            <arb:Status>
               <arb:StatusCd>I000000</arb:StatusCd>
            </arb:Status>
            <arb:RqID>3a3c3005-dee7-41d1-9379-805a4743e346</arb:RqID>
         </Hdr>
         </userValidateSessionRs>      
   </soapenv:Body>
</soapenv:Envelope>';
        $this->curl->method("getBody")->willReturn($xml);
        $this->getTokenMock->getToken(
            "CIC298927",
            "Tester 1",
            "Tester 2",
            "test@example.com",
            "216896321",
            "2396wsakjdgkjgsad",
            "10"
        );
    }
}
