<?php
/**
 * Invalidates customer token
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */
namespace Arb\TokenManagement\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\Integration\Model\CustomerTokenService;
use Magento\Customer\Model\ResourceModel\Visitor\CollectionFactory;
use Magento\Framework\Session\SaveHandlerInterface;
use Magento\Framework\Webapi\Request;
use Arb\TokenManagement\Model\InvalidateToken;
use Magento\Customer\Model\LoggerFactory;

/**
 * @covers \Arb\TokenManagement\Model\InvalidateToken
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class InvalidateTokenTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->customerSession = $this->createMock(Session::class);
        $this->customerTokenService = $this->createMock(CustomerTokenService::class);
        $this->collectionFactory = $this->getMockBuilder(CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create","addFieldToFilter","getSize","getItems","getSessionId","setData","save"])
            ->getMock();
        $this->collectionFactory->method('create')->willReturnSelf();
        $this->collectionFactory->method('addFieldToFilter')->willReturnSelf();
        $this->saveHandlerInterface = $this->createMock(SaveHandlerInterface::class);
        $this->webRequest = $this->createMock(Request::class);
        $this->tokenModelFactory = $this->getMockBuilder(TokenFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create","loadByCustomerId","getToken","createCustomerToken"])
            ->getMock();
        $this->loggerFactory = $this->getMockBuilder(LoggerFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(["create","log"])
            ->getMock();
        $this->invalidateTokenMock = $this->objectManager->getObject(
            InvalidateToken::class,
            [
                'customerSession' => $this->customerSession,
                'tokenModelFactory' => $this->tokenModelFactory,
                'customerTokenServiceFactory' => $this->customerTokenService,
                'visitorCollectionFactory' => $this->collectionFactory,
                'saveHandler' => $this->saveHandlerInterface,
                'webRequest' => $this->webRequest,
                'logFactory' => $this->loggerFactory
            ]
        );
    }
    public function testInvalidateToken()
    {
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("lkf84195ptlzitkcloulxy5wiykkvs42");
        $this->collectionFactory->method('getSize')->willReturn(10);
        $this->collectionFactory->method('getItems')->willReturn([$this->collectionFactory]);
        $this->collectionFactory->method('getSessionId')->willReturn("sessionId");
        $this->collectionFactory->method('setData')->willReturnSelf();
        $this->collectionFactory->method('save')->willReturnSelf();
        $this->loggerFactory->method('create')->willReturnSelf();
        $this->loggerFactory->method('log')->willReturnSelf();
        $this->webRequest->method('getHeader')->willReturn("Bearer lkf84195ptlzitkcloulxy5wiykkvs42");
        $this->invalidateTokenMock->invalidateToken("30");
    }

    public function testInvalidateTokenFailed()
    {
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("teste");
        $this->collectionFactory->method('getSize')->willReturn(10);
        $this->webRequest->method('getHeader')->willReturn("Bearer lkf84195ptlzitkcloulxy5wiykkvs42");
        $this->invalidateTokenMock->invalidateToken("30");
    }

    /**
     * @expectedException \Magento\Framework\Exception\LocalizedException
     */
    public function testInvalidateTokenException()
    {
        $this->tokenModelFactory->method('create')->willReturnSelf();
        $this->tokenModelFactory->method('loadByCustomerId')->willReturnSelf();
        $this->tokenModelFactory->method('getToken')->willReturn("lkf84195ptlzitkcloulxy5wiykkvs42");
        $this->collectionFactory->method('getSize')->willReturn(10);
        $this->webRequest->method('getHeader')->willReturn("Bearer lkf84195ptlzitkcloulxy5wiykkvs42");
        $this->invalidateTokenMock->invalidateToken("30");
    }
}
