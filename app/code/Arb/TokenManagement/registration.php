<?php
/**
 * TokenManagement module registration file
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */

// @codeCoverageIgnoreStart
/** it is a default magento module registration code so no code coverage
 * is required as default magneto also not providing it
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_TokenManagement',
    __DIR__
);

// @codeCoverageIgnoreEnd
