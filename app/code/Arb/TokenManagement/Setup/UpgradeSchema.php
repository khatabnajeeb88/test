<?php
/**
 * @category    Arb
 * @package     Arb_TokenManagement
 * @author Arb Magento Team
 */
namespace Arb\TokenManagement\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the customer_entity_varchar DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->addIndex($setup);
        }
    }

    /**
     * Add a new index in customer_entity_varchar table
     * @codeCoverageIgnore
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addIndex(SchemaSetupInterface $setup)
    {

        $setup->getConnection()->addIndex(
            $setup->getTable('customer_entity_varchar'),
            $setup->getIdxName(
                $setup->getTable('customer_entity_varchar'), 
                ['entity_id'], 
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            ),
            ['entity_id'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
        );

        $setup->getConnection()->addIndex(
            $setup->getTable('customer_entity_varchar'),
            $setup->getIdxName(
                $setup->getTable('customer_entity_varchar'), 
                ['value'], 
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            ),
            ['value'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
        );

        return $this;
    }

   
}
