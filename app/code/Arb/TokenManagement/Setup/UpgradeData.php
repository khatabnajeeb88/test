<?php
/**
 * customer attribute and token authentication extension setup
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */
namespace Arb\TokenManagement\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Customer\Api\CustomerMetadataInterface;

/**
 * This class Upgrades customer attribute
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * customer attribute for customer_email - secondary email id
     */
    const CUSTOMER_EMAIL = 'customer_email';

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    
    /**
     * @var Config;
     */
    private $eavConfig;

    /**
     * UpgradeData constructor
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig       = $eavConfig;
    }
    
    /**
     * Upgrades table schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        /**
         * Create customer attributes
         */
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            if (!$eavSetup->getAttributeId(Customer::ENTITY, self::CUSTOMER_EMAIL)) {
                $attributeCode = self::CUSTOMER_EMAIL;
                $eavSetup->addAttribute(CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER, self::CUSTOMER_EMAIL, [
                'label' => 'Customer Email',
                'required' => false,
                'user_defined' => 1,
                'system' => 0,
                'position' => 50,
                'input' => 'text',
                'note' => 'Customer Email'
                ]);

                $eavSetup->addAttributeToSet(
                    CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
                    CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
                    null,
                    $attributeCode
                );
            }
        }
    }
}
