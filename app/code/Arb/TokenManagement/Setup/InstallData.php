<?php
/**
 * customer attribute and token authentication extension setup
 *
 * @category Arb
 * @package Arb_TokenManagement
 * @author Arb Magento Team
 *
 */
namespace Arb\TokenManagement\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Model\GroupFactory;

/**
 * This class installs customer attribute and token authentication extented table
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * customer attribute for CIC number
     */
    const CUSTOMER_CIC_NUMBER = 'cic';

    /**
     * customer attribute for phone number
     */
    const CUSTOMER_PHONE_NUMBER = 'phone_number';

    /**
     * ARB customer group name
     */
    const CUSTOMER_GROUP_NAME = 'ARB users';

    /**
     * Default customer group to assigned to tax group
     */
    const DEFAULT_CUSTOMER_TAX_GROUP = '3';

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    
    /**
     * @var Config;
     */
    private $eavConfig;

    /**
     *
     * @var GroupFactory
     */
    protected $groupFactory;

    /**
     * InstallData constructor
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig,
        GroupFactory $groupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig       = $eavConfig;
        $this->groupFactory = $groupFactory;
    }
    
    /**
     * Installs table schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        /**
         * Create customer attributes
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if (!$eavSetup->getAttributeId(Customer::ENTITY, self::CUSTOMER_PHONE_NUMBER)) {
            $attributeCode = self::CUSTOMER_PHONE_NUMBER;
            $eavSetup->addAttribute(CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER, self::CUSTOMER_PHONE_NUMBER, [
                'label' => 'Phone Number',
                'required' => true,
                'user_defined' => 1,
                'system' => 0,
                'position' => 100,
                'input' => 'text',
                'note' => 'Customer phone number'
            ]);
    
            $eavSetup->addAttributeToSet(
                CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
                CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
                null,
                $attributeCode
            );
    
            $phoneNumber = $this->eavConfig->getAttribute(
                CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
                $attributeCode
            );
            $phoneNumber->setData('used_in_forms', [
                'adminhtml_customer',
                'customer_account_create',
                'customer_account_edit'
            ]);
            $phoneNumber->getResource()->save($phoneNumber);
        }
        if (!$eavSetup->getAttributeId(Customer::ENTITY, self::CUSTOMER_CIC_NUMBER)) {
            $attributeCode = self::CUSTOMER_CIC_NUMBER;
            $eavSetup->addAttribute(CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER, self::CUSTOMER_CIC_NUMBER, [
                'label' => 'CIC Number',
                'required' => false,
                'user_defined' => 1,
                'system' => 0,
                'position' => 50,
                'input' => 'text',
                'note' => 'Customer CIC number'
            ]);
    
            $eavSetup->addAttributeToSet(
                CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
                CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
                null,
                $attributeCode
            );
        }

        // Create the new group
        $setup->startSetup();
        /** @var \Magento\Customer\Model\Group $group */
        $group = $this->groupFactory->create();
        $group->setCode(self::CUSTOMER_GROUP_NAME)
            ->setTaxClassId(self::DEFAULT_CUSTOMER_TAX_GROUP)
            ->save();

        $setup->endSetup();
    }
}
