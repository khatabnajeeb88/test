<?php
/**
 * Url redirection in frontend.
 *
 * @category    Arb
 * @package     Arb_UrlRestriction
 * @author      Arb Magento Team
 */
namespace Arb\UrlRestriction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Arb\UrlRestriction\Helper\Data as RedirectFrontendHelper;

/**
 * Url redirection in frontend.
 */
class RedirectFrontend implements ObserverInterface
{

    /**
     * @var redirect
     */
    protected $redirect;
    
    /**
     * @var redirectFrontendHelper
     */
    private $redirectFrontendHelper;
    
    /**
     * @var request
     */
    private $request;
    
    /**
     * Bocked Modules Name
     */
    const BLOCKED_MODULES = ['cms', 'catalog', 'checkout', 'catalogsearch', 'contact', 'search', 'sales'];
    
    /**
     * Bocked Action Name
     */
    const BLOCKED_MODULES_ACTION = ['edit', 'editPost', 'create', 'createPost'];

    /**
     * RedirectFrontend constructor.
     *
     * @param RedirectInterface $redirect
     * @param RedirectFrontendHelper $redirectFrontendHelper
     * @param RequestInterface $request
     */
    public function __construct(
        RedirectInterface $redirect,
        RedirectFrontendHelper $redirectFrontendHelper,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->redirect = $redirect;
        $this->redirectFrontendHelper = $redirectFrontendHelper;
        $this->request = $request;
    }
    
    /**
     * Redirect frontend pages to Marketplace login.
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
            
        // Get the module name
        $moduleName = $this->request->getModuleName();
        // Get the action name
        $actionName = $this->request->getActionName();
        // Get the controller name
        $controllerName = $this->request->getControllerName();
        //Restricts product to access from web.
        if ($this->redirectFrontendHelper->getConfigValue('urlrestriction/general/enable')) {
            if (in_array($moduleName, self::BLOCKED_MODULES) || ($moduleName == 'customer' && in_array($actionName, self::BLOCKED_MODULES_ACTION)) || ($moduleName == 'marketplace' && $actionName == 'view' && $controllerName == 'catalog')) {
                $controller = $observer->getControllerAction();
                $this->redirect->redirect($controller->getResponse(), 'marketplace/account/login');
            }
        }
        
        return $this;
    }
}
