<?php
/**
 * Url redirection helper.
 *
 * @category    Arb
 * @package     Arb_UrlRestriction
 * @author      Arb Magento Team
 */
namespace Arb\UrlRestriction\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Url redirection helper.
 */
class Data extends AbstractHelper
{

    /**
     * Get value from config
     * @return mixed
     */
    public function getConfigValue($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_WEBSITE
        );
    }
}
