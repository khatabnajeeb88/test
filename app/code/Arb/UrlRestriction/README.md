## Synopsis
This module is for redirect all the modules like 'cms', 'catalog', 'checkout', 'customer', 'catalogsearch', 'contact', 'search', 'sales' to marketplace login page except marketplace pages.

### Module configuration
1. Package details [composer.json](composer.json).
2. Module configuration details (sequence) in [module.xml](etc/module.xml).
3. Module configuration available through Stores->Configuration [system.xml](etc/adminhtml/system.xml)
4. ACL configuration [acl.xml](etc/acl.xml)
5. New Observer created for `controller_action_predispatch`
