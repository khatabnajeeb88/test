<?php
/**
 * Url Restriction override registration file
 *
 * @category Arb
 * @package Arb_UrlRestriction
 * @author Arb Magento Team
 *
 */
// @codeCoverageIgnoreStart
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Arb_UrlRestriction',
    __DIR__
);
// @codeCoverageIgnoreEnd
