<?php
/**
 * RedirectFrontendTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_UrlRestriction
 * @author Arb Magento Team
 *
 */
namespace Arb\UrlRestriction\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Event\Observer;
use Arb\UrlRestriction\Observer\RedirectFrontend;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Arb\UrlRestriction\Helper\Data as RedirectFrontendHelper;
use \Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;

/**
 * Class RedirectFrontendTest for testing  UrlRestriction class
 * @covers \Arb\UrlRestriction\Observer\RedirectFrontend
 */
class RedirectFrontendTest extends TestCase
{
    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->redirectInterface = $this->getMockForAbstractClass(RedirectInterface::class);
        $this->requestInterface = $this->getMockBuilder(RequestInterface::class)
            ->setMethods(["getModuleName",
                        "getActionName",
                        "getControllerName"])
            ->disableOriginalConstructor()->getMockForAbstractClass();
        $this->redirectFrontendHelper = $this->createMock(RedirectFrontendHelper::class);
        $this->observerMock = $this->createPartialMock(
            Observer::class,
            ['getEvent',"getControllerAction","getResponse"]
        );
        $this->observerMock->method("getControllerAction")->willReturnSelf();
        $responseMock = $this->createMock(ResponseInterface::class);
        $this->observerMock->method("getResponse")->willReturn($responseMock);
        $this->redirectFrontend = $this->objectManager->getObject(RedirectFrontend::class, [
            "redirect"=>$this->redirectInterface,
            "redirectFrontendHelper"=>$this->redirectFrontendHelper,
            "request"=>$this->requestInterface
        ]);
    }

    public function testExecute()
    {
        $this->requestInterface->method("getModuleName")->willReturn("customer");
        $this->requestInterface->method("getActionName")->willReturn("create");
        $this->requestInterface->method("getControllerName")->willReturn("catalog");
        $this->redirectInterface->method("redirect")->willReturn("create");
        $this->redirectFrontendHelper->method("getConfigValue")->willReturn(true);
        $this->redirectFrontend->execute($this->observerMock);
    }
}
