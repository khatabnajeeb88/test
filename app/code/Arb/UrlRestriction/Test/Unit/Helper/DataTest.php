<?php
/**
 * DataTest PHPUnit test file
 *
 * @category Arb
 * @package Arb_UrlRestriction
 * @author Arb Magento Team
 *
 */
namespace Arb\UrlRestriction\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Arb\UrlRestriction\Helper\Data as ArbHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class DataTest for testing  UrlRestriction class
 * @covers \Arb\UrlRestriction\Helper\Data
 */
class DataTest extends TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ObjectManager
     */
    private $model;

    /**
     * Main set up method
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->context = $this->createMock(Context::class);
        $this->_scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);
        $this->context->expects($this->any())->method('getScopeConfig')->willReturn($this->_scopeConfigMock);
        $this->_arbHelper = $this->objectManager->getObject(ArbHelper::class, []);
    }

    public function testGetConfigValue()
    {
        $this->_arbHelper->getConfigValue("testpath");
    }
}
